
public class sampleSmartBullet extends BaseProjectile {

  private ref<IComponent> m_meshComponent;

  private edit EffectRef m_effect;

  [Default(sampleSmartBullet, 0))]
  private Float m_countTime;

  private Float m_startVelocity;

  private edit Float m_lifetime;

  private edit Float m_bendTimeRatio;

  private edit Float m_bendFactor;

  [Default(sampleSmartBullet, false))]
  private edit Bool m_useParabolicPhase;

  private edit Float m_parabolicVelocityMin;

  private edit Float m_parabolicVelocityMax;

  private edit Float m_parabolicDuration;

  private edit Vector4 m_parabolicGravity;

  private ref<SpiralControllerParams> m_spiralParams;

  private Bool m_useSpiralParams;

  [Default(sampleSmartBullet, true))]
  private Bool m_alive;

  [Default(sampleSmartBullet, false))]
  private Bool m_hit;

  private CName m_trailName;

  public ref<StatsSystem> m_statsSystem;

  public EntityID m_weaponID;

  public ref<ParabolicTrajectoryParams> m_parabolicPhaseParams;

  public ref<FollowCurveTrajectoryParams> m_followPhaseParams;

  public ref<LinearTrajectoryParams> m_linearPhaseParams;

  public Bool m_targeted;

  public Bool m_trailStarted;

  public ESmartBulletPhase m_phase;

  public Float m_timeInPhase;

  private Float m_randStartVelocity;

  private Float m_smartGunMissDelay;

  private Float m_smartGunHitProbability;

  private Float m_smartGunMissRadius;

  private Float m_randomWeaponMissChance;

  private Float m_randomTargetMissChance;

  private Bool m_readyToMiss;

  [Default(sampleSmartBullet, false))]
  private edit Bool m_stopAndDropOnTargetingDisruption;

  private Bool m_shouldStopAndDrop;

  private EntityID m_targetID;

  private EntityID m_ignoredTargetID;

  private wref<GameObject> m_owner;

  private wref<GameObject> m_weapon;

  private Vector4 m_startPosition;

  private Bool m_hasExploded;

  private ref<IAttack> m_attack;

  private ref<BulletCollisionEvaluator> m_BulletCollisionEvaluator;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "MeshComponent", "IComponent", true);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    OnTakeControl(ri);
    this.m_meshComponent = GetComponent(ri, "MeshComponent");
    this.m_spiralParams = new SpiralControllerParams();
    this.m_spiralParams.rampUpDistanceStart = 0.4000000059604645;
    this.m_spiralParams.rampUpDistanceEnd = 1;
    this.m_spiralParams.rampDownDistanceStart = 7.5;
    this.m_spiralParams.rampDownDistanceEnd = 5;
    this.m_spiralParams.rampDownFactor = 1;
    this.m_spiralParams.randomizePhase = true;
  }

  protected cb Bool OnInitialize(ref<gameprojectileSetUpEvent> eventData) {
    Float velVariance;
    this.m_projectileComponent.ToggleAxisRotation(true);
    this.m_projectileComponent.AddAxisRotation(new Vector4(0,1,0,0), 100);
    this.m_owner = eventData.owner;
    this.m_weapon = eventData.weapon;
    this.m_BulletCollisionEvaluator = new BulletCollisionEvaluator();
    this.m_projectileComponent.SetCollisionEvaluator(this.m_BulletCollisionEvaluator);
    if(ToBool(this.m_weapon)) {
      this.m_statsSystem = GetStatsSystem(WeakRefToRef(this.m_weapon).GetGame());
      this.m_weaponID = WeakRefToRef(this.m_weapon).GetEntityID();
      this.m_attack = Cast(WeakRefToRef(this.m_weapon)).GetCurrentAttack();
      this.m_useSpiralParams = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunAddSpiralTrajectory) > 0;
      this.m_spiralParams.radius = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunSpiralRadius);
      this.m_spiralParams.cycleTimeMin = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunSpiralCycleTimeMin);
      this.m_spiralParams.cycleTimeMax = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunSpiralCycleTimeMax);
      this.m_spiralParams.randomizeDirection = Cast(this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunSpiralRandomizeDirection));
      if(WeakRefToRef(eventData.owner).IsNPC()) {
        this.m_startVelocity = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunNPCProjectileVelocity);
      } else {
        if(WeakRefToRef(eventData.owner).IsPlayer()) {
          this.m_startVelocity = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunPlayerProjectileVelocity);
        };
      };
      velVariance = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunProjectileVelocityVariance);
      this.m_randStartVelocity = RandRangeF(this.m_startVelocity - this.m_startVelocity * velVariance, this.m_startVelocity + this.m_startVelocity * velVariance);
      this.m_randStartVelocity = MaxF(1, this.m_randStartVelocity);
      this.m_smartGunHitProbability = ClampF(this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunHitProbability), 0, 1);
      if(ToBool(this.m_owner) && ToBool(this.m_statsSystem)) {
        this.m_smartGunHitProbability *= 1 + this.m_statsSystem.GetStatValue(Cast(WeakRefToRef(this.m_owner).GetEntityID()), gamedataStatType.SmartGunHitProbabilityMultiplier);
        this.m_smartGunHitProbability = ClampF(this.m_smartGunHitProbability, 0, 1);
      };
      this.m_smartGunMissDelay = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunMissDelay);
      this.m_smartGunMissRadius = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.SmartGunMissRadius);
      this.m_smartGunMissRadius = this.m_smartGunMissRadius * RandRangeF(0.6600000262260437, 1);
    } else {
      this.m_spiralParams.enabled = false;
      this.m_spiralParams.radius = 0.009999999776482582;
      this.m_spiralParams.cycleTimeMin = 0.10000000149011612;
      this.m_spiralParams.cycleTimeMax = 0.10000000149011612;
      this.m_randStartVelocity = 50;
      this.m_startVelocity = 50;
      this.m_useSpiralParams = false;
    };
    SetCurrentDamageTrailName();
  }

  private final void StartTrailEffect() {
    if(!this.m_trailStarted) {
      StartEffectEvent(this, this.m_trailName, true);
      this.m_trailStarted = true;
    };
  }

  private final void Reset() {
    this.m_countTime = 0;
    this.m_alive = true;
    this.m_hit = false;
    this.m_timeInPhase = 0;
    this.m_phase = ESmartBulletPhase.Init;
    this.m_targeted = false;
    this.m_trailStarted = false;
    this.m_readyToMiss = false;
    this.m_hasExploded = false;
    this.m_targetID = EMPTY_ENTITY_ID();
    this.m_ignoredTargetID = EMPTY_ENTITY_ID();
  }

  protected cb Bool OnShoot(ref<gameprojectileShootEvent> eventData) {
    ref<LinearTrajectoryParams> linearParams;
    linearParams = new LinearTrajectoryParams();
    Reset();
    this.m_targeted = false;
    this.m_startPosition = eventData.startPoint;
    SetupCommonParams(eventData.weaponVelocity);
    this.m_followPhaseParams = new FollowCurveTrajectoryParams();
    StartNextPhase();
    this.m_projectileComponent.SetOnCollisionAction(gameprojectileOnCollisionAction.Stop);
  }

  private final void SetCurrentDamageTrailName() {
    Float cachedThreshold;
    this.m_weaponID = WeakRefToRef(this.m_owner).GetEntityID();
    cachedThreshold = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.PhysicalDamage);
    this.m_trailName = "trail";
    if(this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.ThermalDamage) > cachedThreshold) {
      cachedThreshold = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.ThermalDamage);
      this.m_trailName = "trail_thermal";
    };
    if(this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.ElectricDamage) > cachedThreshold) {
      cachedThreshold = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.ElectricDamage);
      this.m_trailName = "trail_electric";
    };
    if(this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.ChemicalDamage) > cachedThreshold) {
      cachedThreshold = this.m_statsSystem.GetStatValue(Cast(this.m_weaponID), gamedataStatType.ChemicalDamage);
      this.m_trailName = "trail_chemical";
    };
  }

  protected cb Bool OnShootTarget(ref<gameprojectileShootTargetEvent> eventData) {
    WorldTransform slotTransform;
    ref<SlotComponent> slotComponent;
    wref<Entity> targetEntity;
    Reset();
    this.m_targeted = true;
    this.m_startPosition = eventData.startPoint;
    this.m_randomWeaponMissChance = RandF();
    this.m_randomTargetMissChance = RandF();
    if(ToBool(eventData.params.trackedTargetComponent)) {
      this.m_targetID = WeakRefToRef(WeakRefToRef(eventData.params.trackedTargetComponent).GetEntity()).GetEntityID();
    };
    SetupCommonParams(eventData.weaponVelocity);
    this.m_followPhaseParams = new FollowCurveTrajectoryParams();
    this.m_followPhaseParams.targetComponent = eventData.params.trackedTargetComponent;
    this.m_followPhaseParams.targetPosition = eventData.params.targetPosition;
    slotComponent = Cast(WeakRefToRef(eventData.params.trackedTargetComponent));
    if(ToBool(slotComponent)) {
      slotComponent.GetSlotTransform("Head", slotTransform);
      this.m_followPhaseParams.offset = ToVector4(GetWorldPosition(slotTransform)) - GetTranslation(WeakRefToRef(eventData.params.trackedTargetComponent).GetLocalToWorld());
    };
    if(ToBool(this.m_followPhaseParams.targetComponent)) {
      targetEntity = WeakRefToRef(this.m_followPhaseParams.targetComponent).GetEntity();
      if(ToBool(targetEntity)) {
        this.m_targetID = WeakRefToRef(targetEntity).GetEntityID();
        if(GetInitialDistanceToTarget() > 4 && this.m_randomTargetMissChance < ClampF(this.m_statsSystem.GetStatValue(Cast(this.m_targetID), gamedataStatType.SmartTargetingDisruptionProbability), 0, 1)) {
          DisableTargetCollisions(this.m_targetID);
        };
      };
    };
    this.m_followPhaseParams.startVelocity = this.m_randStartVelocity;
    this.m_followPhaseParams.offsetInPlane = eventData.params.smartGunSpreadOnHitPlane;
    this.m_followPhaseParams.returnTimeMargin = 0;
    this.m_followPhaseParams.accuracy = 0.009999999776482582;
    this.m_followPhaseParams.bendTimeRatio = this.m_bendTimeRatio;
    this.m_followPhaseParams.bendFactor = this.m_bendFactor;
    this.m_followPhaseParams.interpolationTimeRatio = 0.10000000149011612;
    this.m_followPhaseParams.offset += eventData.params.hitPlaneOffset;
    StartNextPhase();
    this.m_projectileComponent.SetOnCollisionAction(gameprojectileOnCollisionAction.Stop);
  }

  private final void SetupCommonParams(Vector4 weaponVel) {
    Float randVel;
    randVel = RandRangeF(this.m_parabolicVelocityMin, this.m_parabolicVelocityMax);
    this.m_parabolicPhaseParams = GetAccelVelParabolicParams(this.m_parabolicGravity, Length(weaponVel) + randVel);
    this.m_linearPhaseParams = new LinearTrajectoryParams();
    this.m_linearPhaseParams.startVel = this.m_randStartVelocity;
    this.m_spiralParams.enabled = false;
    this.m_projectileComponent.SetSpiral(this.m_spiralParams);
  }

  private final void StartPhase(ESmartBulletPhase phase) {
    ESmartBulletPhase prevPhase;
    ref<SmartGunMissParams_Record> missParamsRecord;
    ref<SpiralControllerParams> missSpiral;
    Transform desiredTransform;
    Matrix localToWorld;
    Bool leftSideMiss;
    Float randomYaw;
    Float randomPitch;
    EulerAngles rotation;
    if(this.m_phase != phase) {
      prevPhase = this.m_phase;
      this.m_phase = phase;
      this.m_projectileComponent.ClearTrajectories();
      this.m_timeInPhase = 0;
      if(this.m_phase == ESmartBulletPhase.Parabolic) {
        this.m_projectileComponent.AddParabolic(this.m_parabolicPhaseParams);
        this.m_projectileComponent.LockOrientation(true);
      } else {
        if(this.m_phase == ESmartBulletPhase.Follow) {
          this.m_projectileComponent.AddFollowCurve(this.m_followPhaseParams);
          this.m_projectileComponent.LockOrientation(false);
          if(this.m_useSpiralParams) {
            this.m_spiralParams.enabled = true;
            this.m_projectileComponent.SetSpiral(this.m_spiralParams);
          };
          StartTrailEffect();
        } else {
          if(this.m_phase == ESmartBulletPhase.Linear) {
            this.m_projectileComponent.AddLinear(this.m_linearPhaseParams);
            this.m_projectileComponent.LockOrientation(false);
            StartTrailEffect();
          } else {
            if(this.m_phase == ESmartBulletPhase.Miss) {
              missParamsRecord = GetSmartGunMissParamsRecord("SmartGun.SmartGunMissParams_Default");
              if(this.m_shouldStopAndDrop) {
                this.m_parabolicPhaseParams = GetAccelVelParabolicParams(new Vector4(0,0,missParamsRecord.Gravity(),0), 0);
                this.m_projectileComponent.AddParabolic(this.m_parabolicPhaseParams);
                this.m_projectileComponent.LockOrientation(true);
                this.m_shouldStopAndDrop = false;
              } else {
                localToWorld = this.m_projectileComponent.GetLocalToWorld();
                leftSideMiss = RandRange(0, 2) == 0;
                randomYaw = leftSideMiss ? RandRangeF(missParamsRecord.AreaToIgnoreHalfYaw(), missParamsRecord.MaxMissAngleYaw()) : RandRangeF(-missParamsRecord.AreaToIgnoreHalfYaw(), missParamsRecord.MinMissAngleYaw());
                randomPitch = RandRangeF(missParamsRecord.MinMissAnglePitch(), missParamsRecord.MaxMissAnglePitch());
                rotation = GetRotation(localToWorld);
                rotation.Yaw += randomYaw;
                rotation.Pitch += randomPitch;
                SetPosition(desiredTransform, GetTranslation(localToWorld));
                SetOrientationEuler(desiredTransform, rotation);
                this.m_projectileComponent.SetDesiredTransform(desiredTransform);
                if(this.m_useSpiralParams) {
                  missSpiral = new SpiralControllerParams();
                  missSpiral.enabled = true;
                  missSpiral.rampUpDistanceStart = missParamsRecord.SpiralRampUpDistanceStart();
                  missSpiral.rampUpDistanceEnd = missParamsRecord.SpiralRampUpDistanceEnd();
                  missSpiral.radius = missParamsRecord.SpiralRadius();
                  missSpiral.cycleTimeMin = missParamsRecord.SpiralCycleTimeMin();
                  missSpiral.cycleTimeMax = missParamsRecord.SpiralCycleTimeMax();
                  missSpiral.rampDownDistanceStart = missParamsRecord.SpiralRampDownDistanceStart();
                  missSpiral.rampDownDistanceEnd = missParamsRecord.SpiralRampDownDistanceEnd();
                  missSpiral.rampDownFactor = missParamsRecord.SpiralRampDownFactor();
                  missSpiral.randomizePhase = missParamsRecord.SpiralRandomizePhase();
                  this.m_projectileComponent.SetSpiral(missSpiral);
                  this.m_linearPhaseParams.startVel = this.m_linearPhaseParams.startVel * RandRangeF(0.30000001192092896, 0.5);
                };
                this.m_projectileComponent.AddLinear(this.m_linearPhaseParams);
                this.m_projectileComponent.LockOrientation(false);
              };
              StartTrailEffect();
            };
          };
        };
      };
      if(prevPhase == ESmartBulletPhase.Parabolic) {
        StartEffectEvent(this, "ignition", true);
      };
    };
  }

  private final void StartNextPhase() {
    if(this.m_phase == ESmartBulletPhase.Init) {
      if(this.m_useParabolicPhase) {
        StartPhase(ESmartBulletPhase.Parabolic);
      } else {
        if(this.m_targeted) {
          StartPhase(ESmartBulletPhase.Follow);
        } else {
          StartPhase(ESmartBulletPhase.Linear);
        };
      };
    } else {
      if(this.m_phase == ESmartBulletPhase.Parabolic) {
        if(this.m_targeted) {
          StartPhase(ESmartBulletPhase.Follow);
        } else {
          StartPhase(ESmartBulletPhase.Linear);
        };
      } else {
        if(this.m_phase == ESmartBulletPhase.Follow) {
          if(this.m_readyToMiss) {
            StartPhase(ESmartBulletPhase.Miss);
          } else {
            StartPhase(ESmartBulletPhase.Linear);
          };
        };
      };
    };
  }

  protected cb Bool OnTick(ref<gameprojectileTickEvent> eventData) {
    this.m_countTime += eventData.deltaTime;
    this.m_timeInPhase += eventData.deltaTime;
    if(this.m_alive && this.m_phase == ESmartBulletPhase.Follow) {
      UpdateReadyToMiss();
      if(this.m_readyToMiss) {
        StartNextPhase();
      };
    };
    if(this.m_alive && this.m_phase == ESmartBulletPhase.Parabolic && this.m_timeInPhase >= this.m_parabolicDuration) {
      StartNextPhase();
    };
    if(this.m_countTime >= this.m_lifetime) {
      BulletRelease();
    } else {
      if(this.m_countTime >= 0.066600002348423) {
        this.m_meshComponent.Toggle(true);
      };
    };
  }

  protected cb Bool OnCollision(ref<gameprojectileHitEvent> projectileHitEvent) {
    ref<GameObject> gameObj;
    ref<Attack_Record> explosionAttackRecord;
    CName explosionAttackName;
    Bool targetHasJammer;
    ref<gameprojectileHitEvent> damageHitEvent;
    gameprojectileHitInstance hitInstance;
    Int32 i;
    targetHasJammer = false;
    damageHitEvent = new gameprojectileHitEvent();
    i = 0;
    while(i < Size(projectileHitEvent.hitInstances)) {
      hitInstance = projectileHitEvent.hitInstances[i];
      if(this.m_alive) {
        gameObj = Cast(WeakRefToRef(hitInstance.hitObject));
        targetHasJammer = ToBool(gameObj) && gameObj.HasTag("jammer");
        if(!targetHasJammer) {
          Push(damageHitEvent.hitInstances, hitInstance);
        };
        if(!gameObj.HasTag("bullet_no_destroy") && this.m_BulletCollisionEvaluator.HasReportedStopped() && hitInstance.position == this.m_BulletCollisionEvaluator.GetStoppedPosition()) {
          BulletRelease();
          if(!this.m_hasExploded && ToBool(this.m_attack)) {
            this.m_hasExploded = true;
            explosionAttackName = this.m_attack.GetParameterName(Cast("explosionAttack"));
            explosionAttackRecord = GetAttackRecord(Create(NameToString(explosionAttackName)));
            if(ToBool(explosionAttackRecord) && IsValid(explosionAttackRecord.GetID())) {
              SpawnExplosionAttack(explosionAttackRecord, RefToWeakRef(Cast(WeakRefToRef(this.m_weapon))), this.m_owner, RefToWeakRef(this), hitInstance.position, 0.05000000074505806);
            };
          };
          if(!targetHasJammer && !gameObj.HasTag("MeatBag")) {
            this.m_countTime = 0;
            this.m_alive = false;
            this.m_hit = true;
          } else {
            i += 1;
          };
        } else {
        };
      };
      i += 1;
    };
    if(Size(damageHitEvent.hitInstances) > 0) {
      DealDamage(damageHitEvent);
    };
  }

  protected cb Bool OnAcceleratedMovement(ref<gameprojectileAcceleratedMovementEvent> eventData)

  protected cb Bool OnLinearMovement(ref<gameprojectileLinearMovementEvent> eventData) {
    if(!ToBool(this.m_owner) || !WeakRefToRef(this.m_owner).IsPlayer()) {
      GetAudioSystem(this.GetGame()).TriggerFlyby(this.GetWorldPosition(), this.GetWorldForward(), this.m_projectileSpawnPoint, WeakRefToRef(this.m_weapon));
    };
  }

  private final void DealDamage(ref<gameprojectileHitEvent> eventData) {
    ref<EffectInstance> damageEffect;
    damageEffect = this.m_projectileComponent.GetGameEffectInstance();
    SetVariant(damageEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.projectileHitEvent, ToVariant(eventData));
    damageEffect.Run();
  }

  protected cb Bool OnFollowSuccess(ref<gameprojectileFollowEvent> eventData) {
    if(!this.m_hit) {
      StartNextPhase();
    };
  }

  private final void BulletRelease() {
    this.m_meshComponent.Toggle(false);
    BreakEffectLoopEvent(this, this.m_trailName);
    Release();
  }

  private final void UpdateReadyToMiss() {
    Float targetMissProbability;
    Vector4 targetPosition;
    Float distanceToTarget;
    Matrix projectileLocalToWorld;
    ref<SmartBulletDeflectedEvent> bulletDeflectedEvent;
    ref<WeaponItem_Record> weaponRecord;
    TweakDBID ammoID;
    CName audioEventName;
    this.m_readyToMiss = false;
    if(GetInitialDistanceToTarget() > 4) {
      this.m_readyToMiss = this.m_randomWeaponMissChance > this.m_smartGunHitProbability;
      if(!this.m_readyToMiss && IsDefined(this.m_targetID)) {
        targetMissProbability = ClampF(this.m_statsSystem.GetStatValue(Cast(this.m_targetID), gamedataStatType.SmartTargetingDisruptionProbability), 0, 1);
        if(this.m_randomTargetMissChance < targetMissProbability) {
          projectileLocalToWorld = this.m_projectileComponent.GetLocalToWorld();
          targetPosition = ToBool(this.m_followPhaseParams.targetComponent) ? GetTranslation(WeakRefToRef(this.m_followPhaseParams.targetComponent).GetLocalToWorld()) : this.m_followPhaseParams.targetPosition;
          distanceToTarget = Length(targetPosition - GetTranslation(projectileLocalToWorld));
          if(distanceToTarget < this.m_smartGunMissRadius) {
            this.m_readyToMiss = true;
            this.m_shouldStopAndDrop = this.m_stopAndDropOnTargetingDisruption;
            bulletDeflectedEvent = new SmartBulletDeflectedEvent();
            bulletDeflectedEvent.localToWorld = projectileLocalToWorld;
            bulletDeflectedEvent.instigator = this.m_owner;
            bulletDeflectedEvent.weapon = this.m_weapon;
            weaponRecord = GetWeaponItemRecord(GetTDBID(this.m_weapon));
            ammoID = WeakRefToRef(weaponRecord.Ammo()).GetID();
            audioEventName = "unknown_bullet_type";
            if(ammoID == "Ammo.SmartHighMissile") {
              audioEventName = "w_gun_flyby_smart_large_jammed";
            } else {
              if(ammoID == "Ammo.SmartLowMissile") {
                audioEventName = "w_gun_flyby_smart_small_jammed";
              } else {
                if(ammoID == "Ammo.SmartSplitMissile") {
                  audioEventName = "w_gun_flyby_smart_shotgun_jammed";
                };
              };
            };
            PlaySoundEvent(this, audioEventName);
            WeakRefToRef(WeakRefToRef(this.m_followPhaseParams.targetComponent).GetEntity()).QueueEvent(bulletDeflectedEvent);
          };
        };
      };
      if(this.m_readyToMiss) {
        DisableTargetCollisions(this.m_targetID);
      } else {
        EnableTargetCollisions(this.m_targetID);
      };
    };
  }

  private final void EnableTargetCollisions(EntityID targetID) {
    if(IsDefined(targetID) && targetID == this.m_ignoredTargetID) {
      this.m_projectileComponent.RemoveIgnoredEntity(this.m_ignoredTargetID);
      this.m_ignoredTargetID = EMPTY_ENTITY_ID();
    };
  }

  private final void DisableTargetCollisions(EntityID targetID) {
    if(IsDefined(targetID) && targetID != this.m_ignoredTargetID) {
      this.m_ignoredTargetID = targetID;
      this.m_projectileComponent.AddIgnoredEntity(this.m_ignoredTargetID);
    };
  }

  private final Float GetInitialDistanceToTarget() {
    Vector4 targetPosition;
    Float distanceToTarget;
    targetPosition = GetTranslation(WeakRefToRef(this.m_followPhaseParams.targetComponent).GetLocalToWorld());
    distanceToTarget = Length(targetPosition - this.m_startPosition);
    return distanceToTarget;
  }
}
