
public class SampleDeviceClassPS extends GameObjectPS {

  [Default(SampleDeviceClassPS, 0))]
  protected persistent Int32 m_counter;

  public final EntityNotificationType OnActionInt(ref<ActionInt> evt) {
    this.m_counter += 1;
    Log("sample counter: " + IntToString(this.m_counter));
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final ref<ActionInt> GetAction_ActionInt() {
    ref<ActionInt> action;
    return action;
  }

  public final array<ref<DeviceAction>> GetActions() {
    array<ref<DeviceAction>> arr;
    Push(arr, GetAction_ActionInt());
    return arr;
  }
}

public class PSD_DetectorPS extends DeviceComponentPS {

  [Default(PSD_DetectorPS, 0))]
  protected persistent Int32 m_counter;

  [Default(PSD_DetectorPS, false))]
  protected persistent Bool m_toggle;

  protected persistent EntityID m_lastEntityID;

  protected persistent PersistentID m_lastPersistentID;

  protected persistent CName m_name;

  public final EntityID GetLastEntityID() {
    return this.m_lastEntityID;
  }

  public final PersistentID GetLastPersistentID() {
    return this.m_lastPersistentID;
  }

  public final CName GetName() {
    return this.m_name;
  }

  public final Int32 ReadTheCounter() {
    return this.m_counter;
  }

  public final EntityNotificationType OnBumpTheCounter(ref<SampleBumpEvent> evt) {
    this.m_counter += evt.m_amount;
    Log("sample counter: " + IntToString(this.m_counter));
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnBumpTheCounter(ref<ActionInt> evt) {
    this.m_counter += 1;
    Log("sample counter: " + IntToString(this.m_counter) + "  " + ToDebugString(this.m_lastEntityID));
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnLogAction(ref<ActionBool> evt) {
    Bool boolValue;
    CName nameOnFalse;
    CName nameOnTrue;
    if(evt.prop.typeName == "Bool") {
      GetProperty_Bool(evt.prop, boolValue, nameOnFalse, nameOnTrue);
      this.m_toggle = boolValue;
      if(this.m_toggle == true) {
        this.m_counter += 2;
      };
    };
    Log("sample counter: " + IntToString(this.m_counter) + " ## " + NameToString(evt.prop.name) + ": " + BoolToString(FromVariant(evt.prop.first)));
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final ref<ActionInt> GetAction_BumpTheCounter() {
    ref<ActionInt> action;
    return action;
  }

  public final ref<ActionBool> GetAction_Log() {
    ref<ActionBool> action;
    return action;
  }

  public void GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    ref<DeviceAction> action;
    Log("Getting Actions!");
    Push(outActions, GetAction_BumpTheCounter());
    action = GetAction_Log();
    if(IsInRange(context.clearance, action.clearanceLevel)) {
      Push(outActions, action);
    };
  }
}

public class PSD_Detector extends DeviceComponent {

  public final void LogID() {
    Log(ToDebugString(Cast(GetPS()).GetLastEntityID()));
    Log(ToDebugString(Cast(GetPS()).GetLastPersistentID()));
    Log(NameToString(Cast(GetPS()).GetName()));
  }
}

public class PSD_Trigger extends GameObject {

  public NodeRef m_ref;

  [Default(PSD_Trigger, PSD_DetectorPS))]
  public CName m_className;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "interaction", "InteractionComponent", true);
  }

  protected cb Bool OnInteraction(ref<InteractionChoiceEvent> interaction) {
    EntityID targetEntityID;
    ref<GameComponentPS> objPS;
    array<ref<DeviceAction>> actions;
    ref<DeviceAction> action;
    ref<ActionBool> actionBool;
    Int32 i;
    Int32 j;
    array<ref<DeviceActionProperty>> propertyArr;
    GetActionsContext context;
    i = 0;
    j = 0;
    targetEntityID = Cast(ResolveNodeRefWithEntityID(this.m_ref, GetEntityID()));
    objPS = Cast(GetPersistencySystem(GetGame()).GetConstAccessToPSObject(Cast(targetEntityID), this.m_className));
    Cast(objPS).GetActions(actions, context);
    action = Cast(objPS).GetAction_BumpTheCounter();
    actionBool = Cast(objPS).GetAction_Log();
    Log("Device PS: " + NameToString(this.m_className) + ", number of actions: " + IntToString(Size(actions)));
    i = 0;
    while(i < Size(actions)) {
      propertyArr = actions[i].GetProperties();
      j = 0;
      while(j < Size(propertyArr)) {
        if(propertyArr[j].typeName == "Bool") {
          propertyArr[j].first = ToVariant(!FromVariant(propertyArr[j].first));
        };
        j += 1;
      };
      GetPersistencySystem(GetGame()).QueuePSDeviceEvent(actions[i]);
      i += 1;
    };
  }
}

public class Slave_Test extends GameObject {

  public ref<PSD_Detector> deviceComponent;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "detector", "PSD_Detector", true);
    RequestComponent(ri, "interaction", "InteractionComponent", true);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.deviceComponent = Cast(GetComponent(ri, "detector"));
  }

  protected cb Bool OnInteraction(ref<InteractionChoiceEvent> interaction) {
    this.deviceComponent.LogID();
  }
}

public class Master_Test extends GameObject {

  public ref<MasterDeviceComponent> deviceComponent;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "master", "MasterDeviceComponent", true);
    RequestComponent(ri, "interaction", "InteractionComponent", true);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.deviceComponent = Cast(GetComponent(ri, "master"));
  }

  protected cb Bool OnInteraction(ref<InteractionChoiceEvent> interaction) {
    array<ref<DeviceAction>> actions;
    Int32 i;
    Int32 temp;
    Int32 j;
    array<ref<DeviceActionProperty>> propertyArr;
    GetActionsContext context;
    context.clearance = this.deviceComponent.clearance;
    i = 0;
    j = 0;
    Log("Works");
    this.deviceComponent.GetActionsOfConnectedDevices(actions, context);
    Log(IntToString(Size(actions)));
    i = 0;
    while(i < Size(actions)) {
      Log(NameToString(actions[i].actionName));
      propertyArr = actions[i].GetProperties();
      j = 0;
      while(j < Size(propertyArr)) {
        if(propertyArr[j].typeName == "Bool") {
          propertyArr[j].first = ToVariant(!FromVariant(propertyArr[j].first));
        };
        j += 1;
      };
      GetPersistencySystem(GetGame()).QueuePSDeviceEvent(actions[i]);
      i += 1;
    };
  }

  protected cb Bool OnSlaveChanged(ref<PSDeviceChangedEvent> evt) {
    Log(ToDebugString(GetEntityID()) + " notified by " + ToDebugString(evt.persistentID) + " of class " + NameToString(evt.className));
  }
}
