
public static exec void LogPlayerPositionAndName(GameInstance gameInstance) {
  ref<GameObject> playerObject;
  Vector4 worldPosition;
  playerObject = GetPlayerSystem(gameInstance).GetLocalPlayerMainGameObject();
  if(ToBool(playerObject)) {
    worldPosition = playerObject.GetWorldPosition();
    Log("Player Position:: " + ToString(worldPosition));
    Log("Player Name:: " + NameToString(playerObject.GetName()));
  };
}

public static exec void ParameterTest1(GameInstance gameInstance, String param1) {
  Log("param1:: " + param1);
}

public static exec void ParameterTest5(GameInstance gameInstance, String param1, String param2, String param3, String param4, String param5) {
  Log("param1:: " + param1);
  Log("param2:: " + param2);
  Log("param3:: " + param3);
  Log("param4:: " + param4);
  Log("param5:: " + param5);
}

public static exec void ToIntTest(GameInstance gameInstance, String toInt) {
  Int32 fromString;
  fromString = StringToInt(toInt);
  fromString += 100;
  Log(IntToString(fromString));
}
