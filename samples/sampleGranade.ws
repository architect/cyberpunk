
public class sampleGranade extends BaseProjectile {

  private Float m_countTime;

  private edit Float m_energyLossFactor;

  private edit Float m_startVelocity;

  private edit Float m_grenadeLifetime;

  private edit Float m_gravitySimulation;

  [Default(sampleGranade, trail))]
  private CName m_trailEffectName;

  [Default(sampleGranade, true))]
  private Bool m_alive;

  protected cb Bool OnInitialize(ref<gameprojectileSetUpEvent> eventData) {
    this.m_projectileComponent.SetEnergyLossFactor(this.m_energyLossFactor, this.m_energyLossFactor);
  }

  private final void StartTrailEffect() {
    ref<entSpawnEffectEvent> spawnEffectEvent;
    spawnEffectEvent = new entSpawnEffectEvent();
    spawnEffectEvent.effectName = this.m_trailEffectName;
    QueueEvent(spawnEffectEvent);
  }

  private final void Reset() {
    this.m_countTime = 0;
    this.m_alive = true;
  }

  protected cb Bool OnShoot(ref<gameprojectileShootEvent> eventData) {
    Reset();
    StartTrailEffect();
  }

  protected cb Bool OnShootTarget(ref<gameprojectileShootTargetEvent> eventData) {
    Float angle;
    Vector4 accel;
    Vector4 target;
    ref<ParabolicTrajectoryParams> parabolicParams;
    Reset();
    this.m_projectileComponent.ClearTrajectories();
    angle = 45;
    accel = new Vector4(0,0,-this.m_gravitySimulation,0);
    target = eventData.params.targetPosition;
    parabolicParams = GetAccelTargetAngleParabolicParams(accel, target, angle);
    this.m_projectileComponent.AddParabolic(parabolicParams);
    StartTrailEffect();
  }

  protected cb Bool OnTick(ref<gameprojectileTickEvent> eventData) {
    ref<EffectInstance> explosion;
    this.m_countTime += eventData.deltaTime;
    if(this.m_alive) {
      if(this.m_countTime > this.m_grenadeLifetime) {
        explosion = this.m_projectileComponent.GetGameEffectInstance();
        SetVector(explosion.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, eventData.position);
        SetFloat(explosion.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, 3);
        explosion.Run();
        PlayExplosionSound();
        this.m_alive = false;
        this.m_countTime = 0;
      };
    } else {
      if(this.m_countTime > 0.5) {
        Release();
      };
    };
  }

  protected cb Bool OnCollision(ref<gameprojectileHitEvent> eventData)

  private final void PlayExplosionSound() {
    ref<SoundPlayEvent> audioEvent;
    audioEvent = new SoundPlayEvent();
    audioEvent.soundName = "Play_grenade";
    QueueEvent(audioEvent);
  }
}
