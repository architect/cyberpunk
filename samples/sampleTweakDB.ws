
public static exec void TweakDBTest() {
  ref<LootTable_Record> lootTableRecord;
  ref<LootItem_Record> lootItemRecord;
  array<wref<LootItem_Record>> lootItemList;
  ref<Item_Record> itemRecord;
  Log(FloatToString(GetFloat("Scripts.Item.A")));
  Log(FloatToString(GetFloat("Scripts.Item.C", 10)));
  Log(IntToString(GetInt("Scripts.Item.Int")));
  Log(IntToString(GetInt("Scripts.Item.In2t", 1999)));
  Log(GetString("Scripts.Item.String"));
  Log(GetString("Scripts.Item.Stng", "DefaultValue"));
  lootTableRecord = GetLootTableRecord("LootTables.testSingleItem");
  lootTableRecord.LootItems(lootItemList);
  lootItemRecord = WeakRefToRef(lootItemList[0]);
  itemRecord = WeakRefToRef(lootItemRecord.ItemID());
  Log(itemRecord.FriendlyName());
}
