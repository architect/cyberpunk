
public class sampleBullet extends BaseProjectile {

  private ref<IComponent> m_meshComponent;

  private Float m_countTime;

  private edit Float m_startVelocity;

  private edit Float m_lifetime;

  private ref<BulletCollisionEvaluator> m_BulletCollisionEvaluator;

  [Default(sampleBullet, true))]
  private Bool m_alive;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "MeshComponent", "IComponent", true);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    OnTakeControl(ri);
    this.m_meshComponent = GetComponent(ri, "MeshComponent");
  }

  private final void Reset() {
    this.m_countTime = 0;
    this.m_alive = true;
    this.m_meshComponent.Toggle(true);
  }

  protected cb Bool OnInitialize(ref<gameprojectileSetUpEvent> eventData) {
    ref<LinearTrajectoryParams> linearParams;
    linearParams = new LinearTrajectoryParams();
    OnInitialize(eventData);
    linearParams.startVel = this.m_startVelocity;
    this.m_projectileComponent.AddLinear(linearParams);
    this.m_BulletCollisionEvaluator = new BulletCollisionEvaluator();
    this.m_projectileComponent.SetCollisionEvaluator(this.m_BulletCollisionEvaluator);
  }

  private final void StartTrailEffect() {
    this.m_projectileComponent.SpawnTrailVFX();
    PlaySoundEvent(this, "Time_Dilation_Bullet_Trails_bullets_normal");
  }

  protected cb Bool OnShoot(ref<gameprojectileShootEvent> eventData) {
    this.m_BulletCollisionEvaluator.SetWeaponParams(eventData.params);
    Reset();
    StartTrailEffect();
  }

  protected cb Bool OnShootTarget(ref<gameprojectileShootTargetEvent> eventData) {
    this.m_BulletCollisionEvaluator.SetWeaponParams(eventData.params);
    Reset();
    StartTrailEffect();
  }

  protected cb Bool OnTick(ref<gameprojectileTickEvent> eventData) {
    this.m_countTime += eventData.deltaTime;
    if(this.m_countTime > this.m_lifetime || !this.m_alive) {
      Release();
    };
    this.m_projectileComponent.LogDebugVariable("Lifetime", FloatToString(this.m_countTime));
  }

  protected cb Bool OnCollision(ref<gameprojectileHitEvent> projectileHitEvent) {
    ref<GameObject> object;
    Bool isUserPlayer;
    ref<gameprojectileHitEvent> damageHitEvent;
    gameprojectileHitInstance hitInstance;
    Int32 i;
    damageHitEvent = new gameprojectileHitEvent();
    isUserPlayer = WeakRefToRef(this.m_user) == GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject();
    i = 0;
    while(i < Size(projectileHitEvent.hitInstances)) {
      hitInstance = projectileHitEvent.hitInstances[i];
      object = Cast(WeakRefToRef(hitInstance.hitObject));
      if(this.m_alive && !isUserPlayer && object.HasTag("ignore_player_bullets")) {
        Push(damageHitEvent.hitInstances, hitInstance);
        if(!object.HasTag("bullet_no_destroy") && this.m_BulletCollisionEvaluator.HasReportedStopped() && hitInstance.position == this.m_BulletCollisionEvaluator.GetStoppedPosition()) {
          this.m_countTime = 0;
          this.m_alive = false;
          this.m_meshComponent.Toggle(false);
          this.m_projectileComponent.ClearTrajectories();
        } else {
          i += 1;
        };
      } else {
      };
      i += 1;
    };
    if(Size(damageHitEvent.hitInstances) > 0) {
      DealDamage(damageHitEvent);
    };
  }

  private final void DealDamage(ref<gameprojectileHitEvent> eventData) {
    ref<EffectInstance> damageEffect;
    damageEffect = this.m_projectileComponent.GetGameEffectInstance();
    SetVariant(damageEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.projectileHitEvent, ToVariant(eventData));
    damageEffect.Run();
  }
}

public class BulletCollisionEvaluator extends gameprojectileScriptCollisionEvaluator {

  [Default(BulletCollisionEvaluator, false))]
  private Bool m_hasStopped;

  private Vector4 m_stoppedPosition;

  private gameprojectileWeaponParams m_weaponParams;

  [Default(BulletCollisionEvaluator, false))]
  private Bool m_isExplodingBullet;

  public final void SetIsExplodingBullet(Bool isExplodingBullet) {
    this.m_isExplodingBullet = isExplodingBullet;
  }

  public final void SetWeaponParams(gameprojectileWeaponParams params) {
    this.m_weaponParams = params;
  }

  public final Bool HasReportedStopped() {
    return this.m_hasStopped;
  }

  public final Vector4 GetStoppedPosition() {
    return this.m_stoppedPosition;
  }

  protected gameprojectileOnCollisionAction EvaluateCollision(gameprojectileOnCollisionAction defaultOnCollisionAction, ref<CollisionEvaluatorParams> params) {
    Bool validAngle;
    Bool validBounces;
    Bool validRand;
    validAngle = false;
    validBounces = false;
    validRand = false;
    if(!this.m_isExplodingBullet && params.isPiercableSurface) {
      return gameprojectileOnCollisionAction.Pierce;
    };
    if(ToBool(Cast(WeakRefToRef(params.target))) || ToBool(Cast(WeakRefToRef(params.target)))) {
      this.m_hasStopped = true;
      this.m_stoppedPosition = params.position;
      return gameprojectileOnCollisionAction.Stop;
    };
    if(this.m_isExplodingBullet && params.projectilePenetration == "Any" || params.isTechPiercing && params.isPiercableSurface) {
      return gameprojectileOnCollisionAction.Pierce;
    };
    validAngle = this.m_weaponParams.ricochetData.minAngle < params.angle && this.m_weaponParams.ricochetData.maxAngle > params.angle;
    validBounces = params.numBounces < Cast(this.m_weaponParams.ricochetData.count);
    validRand = RandRangeF(0, 1) < this.m_weaponParams.ricochetData.chance;
    if(!validAngle || !validBounces || !validRand) {
      this.m_hasStopped = true;
      this.m_stoppedPosition = params.position;
      return gameprojectileOnCollisionAction.Stop;
    };
    return gameprojectileOnCollisionAction.Bounce;
  }
}
