
public class AnimationsConstructor extends IScriptable {

  private Float m_duration;

  private inkanimInterpolationType m_type;

  private inkanimInterpolationMode m_mode;

  private Bool m_isAdditive;

  public final void SetGenericSettings(Float animDuration, inkanimInterpolationType animType, inkanimInterpolationMode animMode, Bool isAdditive) {
    this.m_duration = animDuration;
    this.m_type = animType;
    this.m_mode = animMode;
    this.m_isAdditive = isAdditive;
  }

  public final ref<inkAnimMargin> NewMarginInterpolator(inkMargin startMargin, inkMargin endMargin) {
    ref<inkAnimMargin> newInterpolator;
    newInterpolator = new inkAnimMargin();
    newInterpolator.SetStartMargin(startMargin);
    newInterpolator.SetEndMargin(endMargin);
    newInterpolator.SetDuration(this.m_duration);
    newInterpolator.SetType(this.m_type);
    newInterpolator.SetMode(this.m_mode);
    newInterpolator.SetIsAdditive(this.m_isAdditive);
    return newInterpolator;
  }

  public final ref<inkAnimSize> NewSizeInterpolator(Vector2 startSize, Vector2 endSize) {
    ref<inkAnimSize> newInterpolator;
    newInterpolator = new inkAnimSize();
    newInterpolator.SetStartSize(startSize);
    newInterpolator.SetEndSize(endSize);
    newInterpolator.SetDuration(this.m_duration);
    newInterpolator.SetType(this.m_type);
    newInterpolator.SetMode(this.m_mode);
    newInterpolator.SetIsAdditive(this.m_isAdditive);
    return newInterpolator;
  }

  public final ref<inkAnimRotation> NewRotationInterpolator(Float startRotation, Float endRotation) {
    ref<inkAnimRotation> newInterpolator;
    newInterpolator = new inkAnimRotation();
    newInterpolator.SetStartRotation(startRotation);
    newInterpolator.SetEndRotation(endRotation);
    newInterpolator.SetDuration(this.m_duration);
    newInterpolator.SetType(this.m_type);
    newInterpolator.SetMode(this.m_mode);
    newInterpolator.SetIsAdditive(this.m_isAdditive);
    return newInterpolator;
  }

  public final ref<inkAnimColor> NewColorInterpolator(HDRColor startColor, HDRColor endColor) {
    ref<inkAnimColor> newInterpolator;
    newInterpolator = new inkAnimColor();
    newInterpolator.SetStartColor(startColor);
    newInterpolator.SetEndColor(endColor);
    newInterpolator.SetDuration(this.m_duration);
    newInterpolator.SetType(this.m_type);
    newInterpolator.SetMode(this.m_mode);
    newInterpolator.SetIsAdditive(this.m_isAdditive);
    return newInterpolator;
  }
}
