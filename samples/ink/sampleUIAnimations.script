
public class sampleUIAnimationController extends inkLogicController {

  private ref<inkAnimDef> m_rotation_anim;

  private ref<inkAnimDef> m_size_anim;

  private ref<inkAnimDef> m_color_anim;

  private ref<inkAnimDef> m_alpha_anim;

  private ref<inkAnimProxy> m_rotation_anim_proxy;

  private ref<inkAnimProxy> m_size_anim_proxy;

  private ref<inkAnimProxy> m_color_anim_proxy;

  private ref<inkAnimProxy> m_alpha_anim_proxy;

  private wref<inkWidget> m_rotation_widget;

  private wref<inkWidget> m_size_widget;

  private wref<inkWidget> m_color_widget;

  private wref<inkWidget> m_alpha_widget;

  private Uint32 m_iteration_counter;

  private Bool m_is_paused;

  private Bool m_is_stoped;

  private Bool m_playReversed;

  protected cb Bool OnInitialize() {
    this.m_iteration_counter = 3;
    this.m_is_paused = false;
    this.m_is_stoped = true;
    this.m_playReversed = false;
    PrepareDefinitions();
    this.m_rotation_widget = GetWidget("simple_animations/simple_rotation_anim/simple_rotation_example");
    this.m_size_widget = GetWidget("simple_animations/simple_size_anim/simple_size_example");
    this.m_color_widget = GetWidget("simple_animations/simple_color_anim/simple_color_example");
    this.m_alpha_widget = GetWidget("simple_animations/simple_alpha_anim/simple_alpha_example");
    OnPlayPingPongLoop(GetRootWidget());
  }

  public final void OnStopAnimation(wref<inkWidget> widget) {
    this.m_is_paused = false;
    this.m_is_stoped = true;
    this.m_rotation_anim_proxy.Stop();
    this.m_size_anim_proxy.Stop();
    this.m_color_anim_proxy.Stop();
    this.m_alpha_anim_proxy.Stop();
  }

  public final void OnPauseResumeAnimation(wref<inkWidget> widget) {
    ref<inkWidget> widgetHandle;
    ref<inkCompoundWidget> root;
    ref<inkText> label;
    widgetHandle = WeakRefToRef(widget);
    root = Cast(widgetHandle);
    label = Cast(WeakRefToRef(root.GetWidget("pause_button_text")));
    this.m_is_paused = !this.m_is_paused;
    this.m_is_stoped = false;
    if(this.m_is_paused == true) {
      label.SetText("RESUME");
      this.m_rotation_anim_proxy.Pause();
      this.m_size_anim_proxy.Pause();
      this.m_color_anim_proxy.Pause();
      this.m_alpha_anim_proxy.Pause();
    } else {
      label.SetText("PAUSE");
      this.m_rotation_anim_proxy.Resume();
      this.m_size_anim_proxy.Resume();
      this.m_color_anim_proxy.Resume();
      this.m_alpha_anim_proxy.Resume();
    };
  }

  public final void OnPlay(wref<inkWidget> widget) {
    this.m_is_paused = false;
    this.m_is_stoped = false;
    WeakRefToRef(this.m_rotation_widget).PlayAnimation(this.m_rotation_anim);
    WeakRefToRef(this.m_size_widget).PlayAnimation(this.m_size_anim);
    WeakRefToRef(this.m_color_widget).PlayAnimation(this.m_color_anim);
    WeakRefToRef(this.m_alpha_widget).PlayAnimation(this.m_alpha_anim);
  }

  public final void OnPlayCycleLoop(wref<inkWidget> widget) {
    inkAnimOptions options;
    this.m_is_paused = false;
    this.m_is_stoped = false;
    options.playReversed = this.m_playReversed;
    options.executionDelay = 0;
    options.loopType = inkanimLoopType.Cycle;
    options.loopCounter = this.m_iteration_counter;
    WeakRefToRef(this.m_rotation_widget).PlayAnimationWithOptions(this.m_rotation_anim, options);
    WeakRefToRef(this.m_size_widget).PlayAnimationWithOptions(this.m_size_anim, options);
    WeakRefToRef(this.m_color_widget).PlayAnimationWithOptions(this.m_color_anim, options);
    WeakRefToRef(this.m_alpha_widget).PlayAnimationWithOptions(this.m_alpha_anim, options);
  }

  public final void OnPlayPingPongLoop(wref<inkWidget> widget) {
    inkAnimOptions options;
    this.m_is_paused = false;
    this.m_is_stoped = false;
    options.playReversed = this.m_playReversed;
    options.executionDelay = 0;
    options.loopType = inkanimLoopType.PingPong;
    options.loopCounter = this.m_iteration_counter;
    WeakRefToRef(this.m_rotation_widget).PlayAnimationWithOptions(this.m_rotation_anim, options);
    WeakRefToRef(this.m_size_widget).PlayAnimationWithOptions(this.m_size_anim, options);
    WeakRefToRef(this.m_color_widget).PlayAnimationWithOptions(this.m_color_anim, options);
    WeakRefToRef(this.m_alpha_widget).PlayAnimationWithOptions(this.m_alpha_anim, options);
  }

  private final void PrepareDefinitions() {
    ref<inkAnimRotation> rotationInterpolator;
    ref<inkAnimSize> sizeInterpolator;
    ref<inkAnimColor> colorInterpolator;
    ref<inkAnimTransparency> alphaInterpolator;
    ref<inkAnimToggleVisibilityEvent> blinkEvent;
    this.m_rotation_anim = new inkAnimDef();
    rotationInterpolator = new inkAnimRotation();
    rotationInterpolator.SetStartRotation(0);
    rotationInterpolator.SetEndRotation(180);
    rotationInterpolator.SetDuration(3);
    rotationInterpolator.SetType(inkanimInterpolationType.Linear);
    rotationInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_rotation_anim.AddInterpolator(rotationInterpolator);
    blinkEvent = new inkAnimToggleVisibilityEvent();
    blinkEvent.SetStartTime(1.5);
    this.m_rotation_anim.AddEvent(blinkEvent);
    this.m_size_anim = new inkAnimDef();
    sizeInterpolator = new inkAnimSize();
    sizeInterpolator.SetStartSize(new Vector2(32,32));
    sizeInterpolator.SetEndSize(new Vector2(16,16));
    sizeInterpolator.SetDuration(3);
    sizeInterpolator.SetType(inkanimInterpolationType.Linear);
    sizeInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_size_anim.AddInterpolator(sizeInterpolator);
    this.m_color_anim = new inkAnimDef();
    colorInterpolator = new inkAnimColor();
    colorInterpolator.SetStartColor(new HDRColor(1,1,1,1));
    colorInterpolator.SetEndColor(new HDRColor(1,0,0,1));
    colorInterpolator.SetDuration(3);
    colorInterpolator.SetType(inkanimInterpolationType.Linear);
    colorInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_color_anim.AddInterpolator(colorInterpolator);
    this.m_alpha_anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(0.20000000298023224);
    alphaInterpolator.SetDuration(3);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_anim.AddInterpolator(alphaInterpolator);
  }
}
