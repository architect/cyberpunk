
public static void InitializeScripts() {
  Log("InitializeScripts");
  StatsEffectsEnumToTDBID(-1);
}

public static void LogDamage(String str) {
  LogChannel("Damage", str);
}

public static void LogDM(String str) {
  LogChannel("DevelopmentManager", str);
}

public static void LogItems(String str) {
  LogChannel("Items", str);
}

public static void LogStats(String str) {
  LogChannel("Stats", str);
}

public static void LogStatPools(String str) {
  LogChannel("StatPools", str);
}

public static void LogStrike(String str) {
  LogChannel("Strike", str);
}

public static void LogItemManager(String str) {
  LogChannel("ItemManager", str);
}

public static void LogScanner(String str) {
  LogChannel("Scanner", str);
}

public static void LogAI(String str) {
  LogChannel("AI", str);
}

public static void LogAIError(String str) {
  LogChannelError("AI", str);
  ReportFailure(str);
}

public static void LogAIWarning(String str) {
  LogChannelWarning("AI", str);
}

public static void LogAICover(String str) {
  LogChannel("AICover", str);
}

public static void LogAICoverWarning(String str) {
  LogChannelError("AICover", str);
}

public static void LogAICoverError(String str) {
  LogChannelWarning("AICover", str);
}

public static void LogPuppet(String str) {
  LogChannel("Puppet", str);
}

public static void LogUI(String str) {
  LogChannel("UI", str);
}

public static void LogUIWarning(String str) {
  LogChannelWarning("UI", str);
}

public static void LogUIError(String str) {
  LogChannelError("UI", str);
}

public static void LogVehicles(String str) {
  LogChannel("Vehicles", str);
}

public static void LogTargetManager(String str, CName type?) {
  CName channelName;
  channelName = "TargetManager";
  FindProperLog(channelName, type, str);
}

public static void LogDevices(String str, ELogType type?) {
  CName channelName;
  if(IsFinal()) {
    return ;
  };
  channelName = "Device";
  FindProperLog(channelName, type, str);
}

public static void LogDevices(ref<IScriptable> object, String str, ELogType type?) {
  String extendedString;
  String address;
  String id;
  const ref<ScriptableDeviceComponentPS> devicePS;
  const ref<Device> deviceObj;
  const ref<ScriptedPuppetPS> puppetPS;
  GameInstance gameInstance;
  Int32 isOverride;
  String tooltip;
  String deviceSpecificTags;
  array<ref<SecurityAreaControllerPS>> securityAreas;
  return ;
}

public static void LogDevices(ref<SecuritySystemControllerPS> object, ref<SecuritySystemInput> input, String str, ELogType type?) {
  String prefix;
  String message;
  if(IsFinal()) {
    return ;
  };
  prefix = "SecuritySystemInput [ Frame: " + IntToString(Cast(GetFrameNumber(object.GetGameInstance()))) + " @" + " | ID #" + input.GetID() + " ]";
  message = prefix + " " + str;
  LogDevices(object, message, type);
}

public static void LogDevices(ref<SecuritySystemControllerPS> object, Int32 id, String str, ELogType type?) {
  String prefix;
  String message;
  if(IsFinal()) {
    return ;
  };
  prefix = "Most recent accepted ID [ Frame: " + IntToString(Cast(GetFrameNumber(object.GetGameInstance()))) + " @" + id + " ]";
  message = prefix + " " + str;
  LogDevices(object, message, type);
}

public static void LogDevices(ref<SecurityAreaControllerPS> object, Int32 id, String str, ELogType type?) {
  String prefix;
  String message;
  if(IsFinal()) {
    return ;
  };
  prefix = "[ Frame: " + IntToString(Cast(GetFrameNumber(object.GetGameInstance()))) + " #" + id + " ]";
  message = prefix + " " + str;
  LogDevices(object, message, type);
}

public static void FindProperLog(CName channelName, ELogType logType, String message) {
  switch(logType) {
    case ELogType.WARNING:
      LogChannelWarning(channelName, message);
      break;
    case ELogType.ERROR:
      LogChannelError(channelName, message);
      break;
    default:
      LogChannel(channelName, message);
  };
}

public static void FindProperLog(CName channelName, CName logType, String message) {
  if(logType == "Warning" || logType == "warning" || logType == "w") {
    LogChannelWarning(channelName, message);
  } else {
    if(logType == "Error" || logType == "error" || logType == "e") {
      LogChannelError(channelName, message);
    } else {
      LogChannel(channelName, message);
    };
  };
}

public static Bool IsNameValid(CName n) {
  return n != "" && n != "" && n != "0";
}

public static Bool IsStringValid(String n) {
  return n != "" && n != "None" && n != "0";
}

public static void LogAssert(Bool condition, String text) {
  if(!condition) {
    LogChannel("ASSERT", text);
  };
}

public static exec void CastEnum() {
  EDeviceStatus enumState;
  Int32 value;
  enumState = EDeviceStatus.DISABLED;
  value = ToInt(enumState);
  switch(value) {
    case -2:
      Log("Disabled " + IntToString(value));
      break;
    case -1:
      Log("Unpowered " + IntToString(value));
      break;
    case 0:
      Log("Off " + IntToString(value));
      break;
    case 1:
      Log("On " + IntToString(value));
      break;
    default:
      Log("wtf " + IntToString(value));
  };
}

public static exec void GetFunFact() {
  Int32 RNG;
  RNG = RandRange(0, 23);
  switch(RNG) {
    case 0:
      Log("Duck vaginas are spiral shaped with dead ends.");
      break;
    case 1:
      Log("Plural of axolotl is axolotls");
      break;
    case 2:
      Log("In the UK, it is illegal to eat mince pies on Christmas Day!");
      break;
    case 3:
      Log("Pteronophobia is the fear of being tickled by feathers!");
      break;
    case 4:
      Log("When hippos are upset, their sweat turns red.");
      break;
    case 5:
      Log("The average woman uses her height in lipstick every 5 years.");
      break;
    case 6:
      Log(" Cherophobia is the fear of fun");
      break;
    case 7:
      Log("If Pinokio says “My Nose Will Grow Now”, it would cause a paradox. ");
      break;
    case 8:
      Log("Billy goats urinate on their own heads to smell more attractive to females.");
      break;
    case 9:
      Log("The person who invented the Frisbee was cremated and made into a frisbee after he died!");
      break;
    case 10:
      Log("If you consistently fart for 6 years & 9 months, enough gas is produced to create the energy of an atomic bomb!");
      break;
    case 11:
      Log("McDonalds calls frequent buyers of their food “heavy users.");
      break;
    case 12:
      Log("Guinness Book of Records holds the record for being the book most often stolen from Public Libraries.");
      break;
    case 13:
      Log("In Romania it is illegal to performe pantimime as it is considered to be higly offensive");
    case 14:
      Log("Banging your head against a wall can burn 150 calories per hour");
    case 15:
      Log("Crocodile poop used to be used as a contraception");
      break;
    case 16:
      Log("In Finland they have an official tournament for peaple riding on a fake horses");
      break;
    case 17:
      Log("The Vatican City is the country that drinks the most wine per capita at 74 liters per citizen per year.");
      break;
    case 18:
      Log("It's possible to lead a cow upstairs...but not downstairs.");
      break;
    case 19:
      Log("There's a chance you won't get a fun fact from GetFunFact");
      break;
    case 20:
      Log("For every non-porn webpage, there are five porn pages.");
      break;
    case 21:
      Log("At any given time, at least 0,7% of earth population is drunk.");
      break;
    case 22:
      Log(" You can’t say happiness without saying penis.");
      break;
    default:
      Log("No fact for you. Ha ha!");
  };
}

public static Bool ProcessCompare(EComparisonType comparator, Float valA, Float valB) {
  switch(comparator) {
    case EComparisonType.Greater:
      return valA > valB;
    case EComparisonType.GreaterOrEqual:
      return valA >= valB;
    case EComparisonType.Equal:
      return valA == valB;
    case EComparisonType.NotEqual:
      return valA != valB;
    case EComparisonType.Less:
      return valA < valB;
    case EComparisonType.LessOrEqual:
      return valA < valB;
  };
}

public static exec void DetectCycles() {
  DetectScriptableCycles();
}

public static void ArrayOfScriptedPuppetsAppend(out array<ref<ScriptedPuppet>> to, array<ref<ScriptedPuppet>> from) {
  Int32 i;
  i = 0;
  while(i < Size(from)) {
    Push(to, from[i]);
    i += 1;
  };
}

public static String ManyCNamesIntoSingleString(array<CName> names, String separator?) {
  String outcomeString;
  Int32 i;
  i = 0;
  while(i < Size(names)) {
    if(i == Size(names) - 1) {
      outcomeString += NameToString(names[i]);
    } else {
      outcomeString += NameToString(names[i]) + separator;
    };
    i += 1;
  };
  return outcomeString;
}

public static Bool IsInRange(Int32 value, Int32 a, Int32 b) {
  Int32 min;
  Int32 max;
  if(a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  };
  if(value >= min && value < max) {
    return true;
  };
  return false;
}

public static Bool IsInRange(Float value, Float a, Float b) {
  Float min;
  Float max;
  if(a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  };
  if(value >= min && value < max) {
    return true;
  };
  return false;
}
