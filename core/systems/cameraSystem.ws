
public class CameraSystemHelper extends IScriptable {

  public final static Bool IsInCameraFrustum(wref<GameObject> obj, Float objHeight, Float objRadius) {
    Vector4 targetPos;
    ref<CameraSystem> cameraSys;
    Vector4 offset;
    Transform cameraTransform;
    Vector4 cameraToTarget;
    Vector4 projectedMin;
    Vector4 projectedMax;
    Bool betweenXPoints;
    Bool betweenYPoints;
    Vector4 bestProjected;
    if(!ToBool(obj)) {
      return false;
    };
    cameraSys = GetCameraSystem(WeakRefToRef(obj).GetGame());
    targetPos = WeakRefToRef(obj).GetWorldPosition();
    targetPos.Z += objHeight * 0.5;
    if(!cameraSys.GetActiveCameraWorldTransform(cameraTransform)) {
      return false;
    };
    cameraToTarget = GetPosition(cameraTransform) - targetPos;
    cameraToTarget.Z = 0;
    cameraToTarget.W = 0;
    offset = Cross(cameraToTarget, new Vector4(0,0,1,0));
    offset.W = 0;
    offset = Normalize(offset) * objRadius;
    offset.Z += objHeight * 0.5;
    projectedMin = new Vector4(1,1,0,0);
    projectedMax = -projectedMin;
    bestProjected = projectedMin;
    HandlePairOfCorners(cameraSys, targetPos, offset, ToScriptRef(projectedMin), ToScriptRef(projectedMax), ToScriptRef(bestProjected));
    offset.X = -offset.X;
    HandlePairOfCorners(cameraSys, targetPos, offset, ToScriptRef(projectedMin), ToScriptRef(projectedMax), ToScriptRef(bestProjected));
    offset.Y = -offset.Y;
    HandlePairOfCorners(cameraSys, targetPos, offset, ToScriptRef(projectedMin), ToScriptRef(projectedMax), ToScriptRef(bestProjected));
    offset.X = -offset.X;
    HandlePairOfCorners(cameraSys, targetPos, offset, ToScriptRef(projectedMin), ToScriptRef(projectedMax), ToScriptRef(bestProjected));
    betweenXPoints = projectedMin.X > 0 ? projectedMax.X < 0 : projectedMax.X > 0;
    betweenYPoints = projectedMin.Y > 0 ? projectedMax.Y < 0 : projectedMax.Y > 0;
    if(betweenXPoints || bestProjected.X < 1 && betweenYPoints || bestProjected.Y < 1) {
      return true;
    };
    return false;
  }

  public final static void HandlePairOfCorners(ref<CameraSystem> cameraSys, Vector4 center, Vector4 offset, ref<Vector4> projectedMin, ref<Vector4> projectedMax, ref<Vector4> bestProjected) {
    Vector4 tmp;
    Vector4 tmpProjected;
    tmp = center + offset;
    tmpProjected = cameraSys.ProjectPoint(tmp);
    projectedMin = ToScriptRef(MinVector2D(FromScriptRef(projectedMin), tmpProjected));
    projectedMax = ToScriptRef(MaxVector2D(FromScriptRef(projectedMax), tmpProjected));
    bestProjected = ToScriptRef(MinAbsVector2D(FromScriptRef(bestProjected), tmpProjected));
    tmp = center - offset;
    tmpProjected = cameraSys.ProjectPoint(tmp);
    projectedMin = ToScriptRef(MinVector2D(FromScriptRef(projectedMin), tmpProjected));
    projectedMax = ToScriptRef(MaxVector2D(FromScriptRef(projectedMax), tmpProjected));
    bestProjected = ToScriptRef(MinAbsVector2D(FromScriptRef(bestProjected), tmpProjected));
  }

  private final static Vector4 MinVector2D(Vector4 a, Vector4 b) {
    a.X = MinF(a.X, b.X);
    a.Y = MinF(a.Y, b.Y);
    return a;
  }

  private final static Vector4 MaxVector2D(Vector4 a, Vector4 b) {
    a.X = MaxF(a.X, b.X);
    a.Y = MaxF(a.Y, b.Y);
    return a;
  }

  private final static Vector4 MinAbsVector2D(Vector4 a, Vector4 b) {
    a.X = MinF(AbsF(a.X), AbsF(b.X));
    a.Y = MinF(AbsF(a.Y), AbsF(b.Y));
    return a;
  }
}
