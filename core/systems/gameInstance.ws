
public static Bool CanLog() {
  return true;
}

public static Int64 GetDamageSystemLogFlags() {
  Int64 flags;
  if(!CanLog()) {
    return 0;
  };
  flags = EnumGetMax("damageSystemLogFlags") * 2 - 1;
  return flags;
}
