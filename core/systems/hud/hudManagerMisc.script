
public static Bool OperatorEqual(ref<ModuleInstance> hudInstance1, ref<ModuleInstance> hudInstance2) {
  if(ToBool(hudInstance1) && ToBool(hudInstance2)) {
    return hudInstance1.GetEntityID() == hudInstance2.GetEntityID();
  };
  return false;
}

public static Bool OperatorGreaterEqual(ActiveMode activeMode1, ActiveMode activeMode2) {
  if(ToInt(activeMode1) >= ToInt(activeMode2)) {
    return true;
  };
  return false;
}

public static Bool OperatorLessEqual(ActiveMode activeMode1, ActiveMode activeMode2) {
  if(ToInt(activeMode1) < ToInt(activeMode2)) {
    return true;
  };
  return false;
}

public class HUDManagerRequest extends ScriptableSystemRequest {

  public EntityID ownerID;

  public final const Bool IsValid() {
    if(IsDefined(this.ownerID)) {
      return true;
    };
    return false;
  }
}

public class HUDManagerRegistrationRequest extends HUDManagerRequest {

  public Bool isRegistering;

  public HUDActorType type;

  public final void SetProperties(ref<GameObject> owner, Bool shouldRegister) {
    this.ownerID = owner.GetEntityID();
    this.isRegistering = shouldRegister;
    if(ToBool(Cast(owner))) {
      this.type = HUDActorType.PUPPET;
    } else {
      if(ToBool(Cast(owner))) {
        this.type = HUDActorType.BODY_DISPOSAL_DEVICE;
      } else {
        if(ToBool(Cast(owner))) {
          this.type = HUDActorType.DEVICE;
        } else {
          if(ToBool(Cast(owner))) {
            this.type = HUDActorType.VEHICLE;
          } else {
            if(ToBool(Cast(owner))) {
              this.type = HUDActorType.ITEM;
            } else {
              if(ToBool(owner)) {
                this.type = HUDActorType.GAME_OBJECT;
              } else {
                this.type = HUDActorType.UNINITIALIZED;
              };
            };
          };
        };
      };
    };
  }
}

public class RefreshActorRequest extends HUDManagerRequest {

  private ref<HUDActorUpdateData> actorUpdateData;

  private array<wref<HUDModule>> requestedModules;

  public final static ref<RefreshActorRequest> Construct(EntityID requesterID, ref<HUDActorUpdateData> updateData?, array<wref<HUDModule>> suggestedModules?) {
    ref<RefreshActorRequest> request;
    Int32 i;
    request = new RefreshActorRequest();
    request.ownerID = requesterID;
    if(ToBool(updateData)) {
      request.actorUpdateData = updateData;
    };
    i = 0;
    while(i < Size(suggestedModules)) {
      if(ToBool(suggestedModules[i])) {
        Push(request.requestedModules, suggestedModules[i]);
      };
      i += 1;
    };
    return request;
  }

  public final const ref<HUDActorUpdateData> GetActorUpdateData() {
    return this.actorUpdateData;
  }

  public final const array<wref<HUDModule>> GetRequestedModules() {
    return this.requestedModules;
  }
}

public class HUDActor extends IScriptable {

  private EntityID entityID;

  [Default(DEVICE_Actor, HUDActorType.DEVICE))]
  [Default(GAMEOBJECT_Actor, HUDActorType.GAME_OBJECT))]
  [Default(PUPPET_ACtor, HUDActorType.PUPPET))]
  [Default(VEHICLE_Actor, HUDActorType.VEHICLE))]
  private HUDActorType type;

  private HUDActorStatus status;

  private ActorVisibilityStatus visibility;

  private array<wref<HUDModule>> activeModules;

  private Bool isRevealed;

  private Bool isTagged;

  private HUDClueData clueData;

  private Bool isRemotelyAccessed;

  private Bool canOpenScannerInfo;

  private Bool isInIconForcedVisibilityRange;

  private Bool isIconForcedVisibleThroughWalls;

  [Default(HUDActor, true))]
  private Bool shouldRefreshQHack;

  public final static void Construct(ref<HUDActor> self, EntityID entityID, HUDActorType type, HUDActorStatus status, ActorVisibilityStatus visibility) {
    self.entityID = entityID;
    self.type = type;
    self.status = status;
    self.visibility = visibility;
  }

  public final void UpdateActorData(ref<HUDActorUpdateData> updateData) {
    if(updateData == null) {
      return ;
    };
    if(updateData.updateVisibility) {
      this.visibility = updateData.visibilityValue;
    };
    if(updateData.updateIsRevealed) {
      this.isRevealed = updateData.isRevealedValue;
    };
    if(updateData.updateIsTagged) {
      this.isTagged = updateData.isTaggedValue;
    };
    if(updateData.updateClueData) {
      this.clueData = updateData.clueDataValue;
    };
    if(updateData.updateIsRemotelyAccessed) {
      this.isRemotelyAccessed = updateData.isRemotelyAccessedValue;
    };
    if(updateData.updateCanOpenScannerInfo) {
      this.canOpenScannerInfo = updateData.canOpenScannerInfoValue;
    };
    if(updateData.updateIsInIconForcedVisibilityRange) {
      this.isInIconForcedVisibilityRange = updateData.isInIconForcedVisibilityRangeValue;
    };
    if(updateData.updateIsIconForcedVisibleThroughWalls) {
      this.isIconForcedVisibleThroughWalls = updateData.isIconForcedVisibleThroughWallsValue;
    };
  }

  public final void AddModule(ref<HUDModule> module) {
    Int32 i;
    i = 0;
    while(i < Size(this.activeModules)) {
      if(WeakRefToRef(this.activeModules[i]) == module) {
        return ;
      };
      i += 1;
    };
    Push(this.activeModules, RefToWeakRef(module));
  }

  public final void RemoveModule(ref<HUDModule> module) {
    Int32 i;
    i = 0;
    while(i < Size(this.activeModules)) {
      if(WeakRefToRef(this.activeModules[i]) == module) {
        Erase(this.activeModules, i);
        return ;
      };
      i += 1;
    };
  }

  public final void SetStatus(HUDActorStatus newStatus) {
    this.status = newStatus;
  }

  public final void SetRemotelyAccessed(Bool value) {
    this.isRemotelyAccessed = value;
  }

  public final void SetRevealed(Bool value) {
    this.isRevealed = value;
  }

  public final void SetTagged(Bool value) {
    this.isTagged = value;
  }

  public final void SetClue(Bool value) {
    this.clueData.isClue = value;
  }

  public final void SetClueGroup(CName value) {
    this.clueData.clueGroupID = value;
  }

  public final void SetCanOpenScannerInfo(Bool value) {
    this.canOpenScannerInfo = value;
  }

  public final void SetIsInIconForcedVisibilityRange(Bool value) {
    this.isInIconForcedVisibilityRange = value;
  }

  public final void SetIsIconForcedVisibileThroughWalls(Bool value) {
    this.isIconForcedVisibleThroughWalls = value;
  }

  public final void SetShouldRefreshQHack(Bool value) {
    this.shouldRefreshQHack = value;
  }

  public final const EntityID GetEntityID() {
    return this.entityID;
  }

  public final const HUDActorType GetType() {
    return this.type;
  }

  public final const HUDActorStatus GetStatus() {
    return this.status;
  }

  public final const ActorVisibilityStatus GetVisibility() {
    return this.visibility;
  }

  public final const Bool IsRevealed() {
    return this.isRevealed;
  }

  public final const Bool IsTagged() {
    return this.isTagged;
  }

  public final const Bool IsClue() {
    return this.clueData.isClue;
  }

  public final const Bool IsGrouppedClue() {
    return IsNameValid(this.clueData.clueGroupID);
  }

  public final const Bool IsRemotelyAccessed() {
    return this.isRemotelyAccessed;
  }

  public final const Bool CanOpenScannerInfo() {
    return this.canOpenScannerInfo;
  }

  public final const Bool IsInIconForcedVisibilityRange() {
    return this.isInIconForcedVisibilityRange;
  }

  public final const Bool IsIconForcedVisibileThroughWalls() {
    return this.isIconForcedVisibleThroughWalls;
  }

  public final const Bool GetShouldRefreshQHack() {
    return this.shouldRefreshQHack;
  }

  public final const array<wref<HUDModule>> GetActiveModules() {
    return this.activeModules;
  }
}
