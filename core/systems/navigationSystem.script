
public class NavigationSystem extends IScriptable {

  public final native const ref<NavigationPath> CalculatePath(Vector4 startPoint, Vector4 endPoint, NavGenAgentSize agentSize, Float findPointTolerance, ref<NavigationCostModCircle> costModCircle?)

  public final native ref<NavigationFindWallResult> FindWallInLine(Vector4 startPoint, Vector4 endPoint, NavGenAgentSize agentSize, Float findPointTolerance)

  public final native const NavigationFindPointResult FindPointInSphere(Vector4 center, Float radius, NavGenAgentSize agentSize, Bool heightDetail)

  public final native const NavigationFindPointResult FindPointInBox(Vector4 center, Vector4 extents, NavGenAgentSize agentSize, Bool heightDetail)

  public final native const Bool FindPursuitPoint(Vector4 pos, Vector4 dir, Float radius, Bool navVisCheck, NavGenAgentSize agentSize, out Vector4 destination)

  public final native const Bool FindPursuitPointRange(Vector4 pos, Vector4 dir, Float radiusMin, Float radiusMax, Bool navVisCheck, NavGenAgentSize agentSize, out Vector4 destination)

  public final native const Bool FindPursuitPointsRange(Vector4 pos, Vector4 dir, Float radiusMin, Float radiusMax, Int32 count, Bool navVisCheck, NavGenAgentSize agentSize, out array<Vector4> results)

  public final native const Bool FindNavmeshPointAwayFromReferencePoint(Vector4 pos, Vector4 refPos, Float distance, NavGenAgentSize agentSize, out Vector4 destination, Float distanceTolerance?, Float angleTolerance?)

  public final native ref<NavigationObstacle> AddObstacle(Vector4 position, Float radius, Float height, NavGenAgentSize agentSize)

  public final native void RemoveObstacle(ref<NavigationObstacle> obstacle)

  public final const Bool IsPointOnNavmesh(Vector4 point, Float tolerance?) {
    NavigationFindPointResult pointResults;
    if(tolerance < 0) {
      tolerance = 0.5;
    };
    pointResults = FindPointInSphere(point, tolerance, NavGenAgentSize.Human, false);
    if(pointResults.status != worldNavigationRequestStatus.OK) {
      return false;
    };
    return true;
  }

  public final const Bool IsPointOnNavmesh(Vector4 point, Vector4 tolerance) {
    NavigationFindPointResult pointResults;
    pointResults = FindPointInBox(point, tolerance, NavGenAgentSize.Human, false);
    if(pointResults.status != worldNavigationRequestStatus.OK) {
      return false;
    };
    return true;
  }

  public final const Bool IsPointOnNavmesh(Vector4 point, Vector4 tolerance, out Vector4 navmeshPoint) {
    NavigationFindPointResult pointResults;
    pointResults = FindPointInBox(point, tolerance, NavGenAgentSize.Human, false);
    navmeshPoint = pointResults.point;
    if(pointResults.status != worldNavigationRequestStatus.OK) {
      return false;
    };
    return true;
  }

  public final const Vector4 GetNearestNavmeshPointBelow(Vector4 origin, Float querySphereRadius, Int32 numberOfSpheres) {
    Int32 i;
    NavigationFindPointResult pointResults;
    Vector4 point;
    i = 0;
    while(i < numberOfSpheres) {
      pointResults = FindPointInSphere(origin, querySphereRadius, NavGenAgentSize.Human, false);
      if(pointResults.status != worldNavigationRequestStatus.OK) {
        origin.Z -= querySphereRadius;
      } else {
        point = pointResults.point;
        goto 258;
      };
      i += 1;
    };
    return point;
  }

  public final const Bool GetNearestNavmeshPointBehind(wref<Entity> origin, Float querySphereRadius, Int32 numberOfSpheres, out Vector4 point, Bool checkPathToOrigin?) {
    Int32 i;
    NavigationFindPointResult pointResults;
    Vector4 originPosition;
    Vector4 originHeading;
    Vector4 center;
    ref<NavigationPath> navigationPath;
    if(!ToBool(origin)) {
      false;
    };
    originPosition = WeakRefToRef(origin).GetWorldPosition();
    originHeading = WeakRefToRef(origin).GetWorldForward();
    i = 1;
    while(i < numberOfSpheres) {
      center = originPosition - originHeading * querySphereRadius * Cast(i);
      pointResults = FindPointInSphere(center, querySphereRadius, NavGenAgentSize.Human, false);
      if(pointResults.status != worldNavigationRequestStatus.OK) {
      } else {
        point = pointResults.point;
        if(checkPathToOrigin) {
          navigationPath = CalculatePath(point, WeakRefToRef(origin).GetWorldPosition(), NavGenAgentSize.Human, 0.5);
          if(Size(navigationPath.path) < 0) {
          } else {
            return true;
          };
        };
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const Bool GetFurthestNavmeshPointBehind(wref<Entity> origin, Float querySphereRadius, Int32 numberOfSpheres, out Vector4 point, Vector4 offsetFromOrigin?, Bool checkPathToOrigin?, CName ratioCurveName?) {
    Int32 i;
    NavigationFindPointResult pointResults;
    Vector4 originPosition;
    Vector4 originHeading;
    Vector4 center;
    ref<NavigationPath> navigationPath;
    ref<StatsDataSystem> statsDataSystem;
    Float pathLength;
    Float directLength;
    Float ratio;
    if(!ToBool(origin)) {
      false;
    };
    statsDataSystem = GetStatsDataSystem(Cast(WeakRefToRef(origin)).GetGame());
    originPosition = WeakRefToRef(origin).GetWorldPosition() + offsetFromOrigin;
    originHeading = WeakRefToRef(origin).GetWorldForward();
    i = numberOfSpheres;
    while(i > 0) {
      center = originPosition - originHeading * querySphereRadius * Cast(i);
      pointResults = FindPointInSphere(center, querySphereRadius, NavGenAgentSize.Human, false);
      if(pointResults.status != worldNavigationRequestStatus.OK) {
      } else {
        point = pointResults.point;
        if(checkPathToOrigin) {
          navigationPath = CalculatePath(point, WeakRefToRef(origin).GetWorldPosition(), NavGenAgentSize.Human, 0.5);
          if(Size(navigationPath.path) < 0) {
          } else {
            if(ratioCurveName != "" && ToBool(statsDataSystem)) {
              directLength = Length(originPosition - point);
              pathLength = navigationPath.CalculateLength();
              ratio = statsDataSystem.GetValueFromCurve("pathLengthToDirectDistancesRatio", pathLength, ratioCurveName);
              if(ratio > 1 && pathLength > ratio * directLength) {
              } else {
                return true;
              };
            };
            return true;
          };
        };
        return true;
      };
      i -= 1;
    };
    return false;
  }

  public final const Bool TryToFindNavmeshPointAroundPoint(Vector4 originPosition, Quaternion originOrientation, Vector4 probeDimensions, Int32 numberOfSpheres, Float sphereDistanceFromOrigin, out Vector4 point, Bool checkPathToOrigin?) {
    Int32 i;
    NavigationFindPointResult pointResults;
    Vector4 currentCheckPosition;
    Float currentAngle;
    Float currentAngleRad;
    Quaternion quat;
    ref<NavigationPath> navigationPath;
    i = numberOfSpheres;
    while(i > 0) {
      SetIdentity(quat);
      currentAngleRad = Deg2Rad(currentAngle);
      SetZRot(quat, currentAngleRad);
      quat = originOrientation * quat;
      currentCheckPosition = originPosition + GetForward(quat) * sphereDistanceFromOrigin;
      pointResults = FindPointInBox(currentCheckPosition, probeDimensions, NavGenAgentSize.Human, false);
      currentAngle += Cast(360 / numberOfSpheres);
      if(pointResults.status != worldNavigationRequestStatus.OK) {
      } else {
        point = pointResults.point;
        if(checkPathToOrigin) {
          navigationPath = CalculatePath(point, originPosition, NavGenAgentSize.Human, 0.5);
          if(Size(navigationPath.path) < 0) {
          } else {
            return true;
          };
        };
        return true;
      };
      i -= 1;
    };
    return false;
  }

  public final const Bool HasPathForward(wref<GameObject> sourceObject, Float distance) {
    Vector4 originPoint;
    NavigationFindPointResult originPointNavmeshResult;
    Vector4 destinationPoint;
    NavigationFindPointResult destinationPointNavmeshResult;
    ref<NavigationPath> navigationPath;
    originPoint = WeakRefToRef(sourceObject).GetWorldPosition();
    destinationPoint = originPoint + Normalize(WeakRefToRef(sourceObject).GetWorldForward()) * distance;
    originPointNavmeshResult = FindPointInBox(originPoint, new Vector4(0.20000000298023224,0.20000000298023224,0.75,1), NavGenAgentSize.Human, false);
    destinationPointNavmeshResult = FindPointInBox(destinationPoint, new Vector4(0.20000000298023224,0.20000000298023224,0.75,1), NavGenAgentSize.Human, false);
    if(originPointNavmeshResult.status == worldNavigationRequestStatus.OK && destinationPointNavmeshResult.status == worldNavigationRequestStatus.OK) {
      navigationPath = CalculatePath(originPointNavmeshResult.point, destinationPointNavmeshResult.point, NavGenAgentSize.Human, 0.5);
      if(Size(navigationPath.path) > 0) {
        return true;
      };
    };
    return false;
  }

  public final static Bool HasPathFromAtoB(GameInstance game, Vector4 originPoint, Vector4 targetPoint) {
    ref<NavigationPath> navigationPath;
    if(!IsValid(game)) {
      return false;
    };
    navigationPath = GetNavigationSystem(game).CalculatePath(originPoint, targetPoint, NavGenAgentSize.Human, 0.5);
    if(Size(navigationPath.path) < 0) {
      return false;
    };
    return true;
  }

  public final const Bool IsOnGround(ref<GameObject> target, Float queryLength?) {
    Vector4 startingPoint;
    ref<GeometryDescriptionQuery> geometryDescription;
    QueryFilter staticQueryFilter;
    ref<GeometryDescriptionResult> geometryDescriptionResult;
    if(queryLength == 0) {
      queryLength = 0.20000000298023224;
    };
    startingPoint = target.GetWorldPosition() + target.GetWorldUp() * 0.10000000149011612;
    AddGroup(staticQueryFilter, "Static");
    geometryDescription = new GeometryDescriptionQuery();
    geometryDescription.refPosition = startingPoint;
    geometryDescription.refDirection = new Vector4(0,0,-1,0);
    geometryDescription.filter = staticQueryFilter;
    geometryDescription.primitiveDimension = new Vector4(0.10000000149011612,0.10000000149011612,queryLength,0);
    geometryDescriptionResult = GetSpatialQueriesSystem(target.GetGame()).GetGeometryDescriptionSystem().QueryExtents(geometryDescription);
    return geometryDescriptionResult.queryStatus == worldgeometryDescriptionQueryStatus.OK;
  }
}

public static exec void TestNavigationSystem(GameInstance gameInstance) {
  Vector4 start;
  Vector4 end;
  ref<NavigationPath> path;
  ref<NavigationFindWallResult> fw;
  start.X = 0;
  start.Y = 0;
  start.Z = 0;
  end.X = 20;
  end.Y = 20;
  end.Z = 0;
  path = GetNavigationSystem(gameInstance).CalculatePath(start, end, NavGenAgentSize.Human, 1);
  Log(ToString(path.path[0]));
  Log(ToString(path.path[1]));
  Log(ToString(path.CalculateLength()));
  fw = GetNavigationSystem(gameInstance).FindWallInLine(start, end, NavGenAgentSize.Human, 1);
  Log(ToString(fw.status));
  Log(ToString(fw.isHit));
  Log(ToString(fw.hitPosition.X));
  Log(ToString(fw.hitPosition.Y));
  Log(ToString(fw.hitPosition.Z));
}
