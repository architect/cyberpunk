
public class ScriptStatsListener extends IStatsListener {

  public void OnStatChanged(StatsObjectID ownerID, gamedataStatType statType, Float diff, Float total)

  public void OnGodModeChanged(EntityID ownerID, gameGodModeType newType)

  public final native void SetStatType(gamedataStatType statType)
}

public class StatsSystemHelper extends IScriptable {

  public final static Bool GetDetailedStatInfo(ref<GameObject> obj, gamedataStatType statType, out gameStatDetailedData statData) {
    array<gameStatDetailedData> detailsArray;
    Int32 i;
    detailsArray = GetStatsSystem(obj.GetGame()).GetStatDetails(Cast(obj.GetEntityID()));
    i = 0;
    while(i < Size(detailsArray)) {
      if(detailsArray[i].statType == statType) {
        statData = detailsArray[i];
        return true;
      };
      i += 1;
    };
    return false;
  }
}

public static Float GetDamageValue(ref<GameObject> object, gamedataDamageType damageType) {
  gamedataStatType statType;
  StatsObjectID objectID;
  Float DPS;
  Float magazineCapacity;
  Float attacksPerSec;
  Float reloadTime;
  Float numShotsPerCycle;
  Float numAttacksPerMagazine;
  Float cycleTime;
  Float projectilesPerShot;
  ref<StatsSystem> statsSystem;
  ref<DamageType_Record> damageRecord;
  Float baseDamage;
  Float finalDamage;
  statsSystem = GetStatsSystem(object.GetGame());
  damageRecord = WeakRefToRef(statsSystem.GetDamageRecordFromType(damageType));
  statType = GetStatTypeFromDamageTypeRecord(damageRecord);
  objectID = Cast(object.GetEntityID());
  DPS = statsSystem.GetStatValue(objectID, statType);
  reloadTime = statsSystem.GetStatValue(objectID, gamedataStatType.ReloadTime);
  magazineCapacity = statsSystem.GetStatValue(objectID, gamedataStatType.MagazineCapacity);
  numShotsPerCycle = statsSystem.GetStatValue(objectID, gamedataStatType.NumShotsToFire);
  cycleTime = statsSystem.GetStatValue(objectID, gamedataStatType.CycleTime);
  projectilesPerShot = statsSystem.GetStatValue(objectID, gamedataStatType.ProjectilesPerShot);
  numAttacksPerMagazine = projectilesPerShot * numShotsPerCycle * magazineCapacity;
  if(numAttacksPerMagazine < 0) {
    return DPS;
  };
  attacksPerSec = projectilesPerShot * numShotsPerCycle * magazineCapacity / cycleTime * magazineCapacity + reloadTime;
  baseDamage = DPS / attacksPerSec;
  return baseDamage;
}

public static gamedataStatType GetStatTypeFromDamageTypeRecord(ref<DamageType_Record> damageRecord) {
  gamedataStatType statType;
  ref<Stat_Record> associatedStat;
  statType = gamedataStatType.Invalid;
  if(ToBool(damageRecord)) {
    associatedStat = WeakRefToRef(damageRecord.AssociatedStat());
    if(ToBool(associatedStat)) {
      statType = ToEnum(Cast(EnumValueFromString("gamedataStatType", associatedStat.EnumName())));
    };
  };
  return statType;
}
