
public class TargetFilterResult extends IScriptable {

  public native EntityID hitEntId;

  public native wref<IComponent> hitComponent;

  public void OnReset()

  public void OnClone(out ref<TargetFilterResult> cloneDestination)
}

public class TargetFilter_Script extends TargetFilter {

  public final native Uint64 GetFilterMask()

  public final native QueryFilter GetFilter()

  public final native void SetFilter(QueryFilter queryFilter)

  public final native Bool TestFilterMask(Uint64 mask)

  public final native void GetResult(ref<TargetFilterResult> destination)

  public void PreFilter()

  public void Filter(TargetHitInfo hitInfo, ref<TargetFilterResult> workingState)

  public void PostFilter()

  public ref<TargetFilterResult> CreateFilterResult() {
    return null;
  }
}
