
public class GameSessionDataSystem extends ScriptableSystem {

  private array<ref<GameSessionDataModule>> m_gameSessionDataModules;

  private final const ref<GameSessionDataModule> GetModule(EGameSessionDataType dataType) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_gameSessionDataModules)) {
      if(this.m_gameSessionDataModules[i].GetModuleType() == dataType) {
        return this.m_gameSessionDataModules[i];
      };
      i += 1;
    };
    return null;
  }

  private void OnAttach() {
    Initialize();
  }

  private void OnDetach() {
    Uninitialize();
  }

  private final void Initialize() {
    ref<CameraDeadBodySessionDataModule> cameraDeadBodyModule;
    ref<CameraTagEnemyLimitDataModule> cameraTagLimitModule;
    cameraDeadBodyModule = new CameraDeadBodySessionDataModule();
    cameraDeadBodyModule.Initialize();
    Push(this.m_gameSessionDataModules, cameraDeadBodyModule);
    cameraTagLimitModule = new CameraTagEnemyLimitDataModule();
    cameraTagLimitModule.Initialize();
    Push(this.m_gameSessionDataModules, cameraTagLimitModule);
  }

  private final void Uninitialize() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_gameSessionDataModules)) {
      this.m_gameSessionDataModules[i].Uninitialize();
      i += 1;
    };
  }

  public final static void AddDataEntryRequest(GameInstance context, EGameSessionDataType dataType, Variant data) {
    ref<GameSessionDataSystem> gameSessionDataSystem;
    ref<DataEntryRequest> dataEntryRequest;
    gameSessionDataSystem = Cast(GetScriptableSystemsContainer(context).Get("GameSessionDataSystem"));
    if(ToBool(gameSessionDataSystem)) {
      dataEntryRequest = new DataEntryRequest();
      dataEntryRequest.dataType = dataType;
      dataEntryRequest.data = data;
      gameSessionDataSystem.QueueRequest(dataEntryRequest);
    };
  }

  private final void OnDataEntryRequest(ref<DataEntryRequest> request) {
    if(IsDataValid(request.dataType, request.data)) {
      GetModule(request.dataType).AddEntry(request.data);
    };
    if(!IsFinal()) {
      RefreshDebug();
    };
  }

  public final static Bool CheckDataRequest(GameInstance context, EGameSessionDataType dataType, Variant dataHelper) {
    ref<GameSessionDataSystem> gameSessionDataSystem;
    ref<GameSessionDataModule> module;
    gameSessionDataSystem = Cast(GetScriptableSystemsContainer(context).Get("GameSessionDataSystem"));
    if(ToBool(gameSessionDataSystem)) {
      module = gameSessionDataSystem.GetModule(dataType);
      if(ToBool(module)) {
        return module.CheckData(dataHelper);
      };
      return false;
    };
    return false;
  }

  protected final Bool IsDataValid(EGameSessionDataType dataType, Variant data) {
    ref<GameSessionDataModule> module;
    module = GetModule(dataType);
    if(ToBool(module)) {
      return module.IsDataValid(data);
    };
    return false;
  }

  private final void RefreshDebug() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_gameSessionDataModules)) {
      this.m_gameSessionDataModules[i].RefreshDebug(GetGameInstance());
      i += 1;
    };
  }
}

public class GameSessionDataModule extends IScriptable {

  protected EGameSessionDataType m_moduleType;

  public void Initialize()

  public void Uninitialize()

  public final const EGameSessionDataType GetModuleType() {
    return this.m_moduleType;
  }

  public const Bool IsDataValid(Variant data) {
    return false;
  }

  public void AddEntry(Variant data)

  public const Bool CheckData(Variant data) {
    return false;
  }

  public void RefreshDebug(GameInstance context)
}

public class CameraDeadBodySessionDataModule extends GameSessionDataModule {

  public array<ref<CameraDeadBodyInternalData>> m_cameraDeadBodyData;

  public void Initialize() {
    this.m_moduleType = EGameSessionDataType.CameraDeadBody;
  }

  public const Bool IsDataValid(Variant data) {
    ref<CameraDeadBodyData> castedData;
    castedData = FromVariant(data);
    if(!ToBool(castedData)) {
      return false;
    };
    if(IsDefined(castedData.ownerID) && IsDefined(castedData.bodyID)) {
      return true;
    };
    return false;
  }

  public void AddEntry(Variant data) {
    Int32 i;
    ref<CameraDeadBodyData> castedData;
    ref<CameraDeadBodyInternalData> newEntry;
    castedData = FromVariant(data);
    if(!ToBool(castedData)) {
      return ;
    };
    i = 0;
    while(i < Size(this.m_cameraDeadBodyData)) {
      if(this.m_cameraDeadBodyData[i].m_ownerID == castedData.ownerID) {
        this.m_cameraDeadBodyData[i].AddEntry(castedData.bodyID);
        return ;
      };
      i += 1;
    };
    newEntry = new CameraDeadBodyInternalData();
    newEntry.m_ownerID = castedData.ownerID;
    newEntry.AddEntry(castedData.bodyID);
    Push(this.m_cameraDeadBodyData, newEntry);
  }

  public const Bool CheckData(Variant data) {
    ref<CameraDeadBodyData> castedData;
    Int32 i;
    castedData = FromVariant(data);
    i = 0;
    while(i < Size(this.m_cameraDeadBodyData)) {
      if(this.m_cameraDeadBodyData[i].m_ownerID == castedData.ownerID) {
        return this.m_cameraDeadBodyData[i].ContainsEntry(castedData.bodyID);
      };
      i += 1;
    };
    return false;
  }

  public void RefreshDebug(GameInstance context) {
    Int32 i;
    Int32 i1;
    SDOSink sink;
    sink = GetScriptsDebugOverlaySystem(context).CreateSink();
    SetRoot(sink, "GameSessionData/CameraDeadBody");
    i = 0;
    while(i < Size(this.m_cameraDeadBodyData)) {
      i1 = 0;
      while(i1 < Size(this.m_cameraDeadBodyData[i].m_bodyIDs)) {
        PushString(sink, ToDebugString(this.m_cameraDeadBodyData[i].m_ownerID) + "/" + i1, ToDebugString(this.m_cameraDeadBodyData[i].m_bodyIDs[i1]));
        i1 += 1;
      };
      i += 1;
    };
  }
}

public class CameraDeadBodyInternalData extends IScriptable {

  public EntityID m_ownerID;

  public array<EntityID> m_bodyIDs;

  public final void AddEntry(EntityID entryID) {
    if(!Contains(this.m_bodyIDs, entryID)) {
      Push(this.m_bodyIDs, entryID);
    };
  }

  public final Bool ContainsEntry(EntityID entryID) {
    return Contains(this.m_bodyIDs, entryID);
  }
}

public class CameraTagEnemyLimitDataModule extends GameSessionDataModule {

  [Default(CameraTagEnemyLimitDataModule, 5))]
  public Int32 m_cameraLimit;

  public array<wref<SurveillanceCamera>> m_cameraList;

  public void Initialize() {
    this.m_moduleType = EGameSessionDataType.CameraTagLimit;
  }

  public void Uninitialize()

  public const Bool IsDataValid(Variant data) {
    ref<CameraTagLimitData> castedData;
    castedData = FromVariant(data);
    if(!ToBool(castedData)) {
      return false;
    };
    if(ToBool(castedData.object)) {
      return true;
    };
    return false;
  }

  public void AddEntry(Variant data) {
    ref<CameraTagLimitData> castedData;
    castedData = FromVariant(data);
    CleanupNulls();
    if(castedData.add) {
      if(!Contains(this.m_cameraList, castedData.object)) {
        if(Size(this.m_cameraList) == this.m_cameraLimit) {
          SendCameraTagLockEvent(0);
          Erase(this.m_cameraList, 0);
        };
        Push(this.m_cameraList, castedData.object);
      };
    } else {
      if(Remove(this.m_cameraList, castedData.object)) {
      };
    };
  }

  private final void CleanupNulls() {
    Int32 i;
    i = Size(this.m_cameraList) - 1;
    while(i >= 0) {
      if(WeakRefToRef(this.m_cameraList[i]) == null) {
        Erase(this.m_cameraList, i);
      };
      i -= 1;
    };
  }

  private final void SendCameraTagLockEvent(Int32 index) {
    ref<CameraTagLockEvent> evt;
    evt = new CameraTagLockEvent();
    evt.isLocked = true;
    WeakRefToRef(this.m_cameraList[index]).QueueEvent(evt);
  }

  public const Bool CheckData(Variant data) {
    if(ToBool(FromVariant(data))) {
      return Contains(this.m_cameraList, RefToWeakRef(FromVariant(data)));
    };
    return false;
  }

  public void RefreshDebug(GameInstance context) {
    Int32 i;
    SDOSink sink;
    sink = GetScriptsDebugOverlaySystem(context).CreateSink();
    SetRoot(sink, "GameSessionData/CameraTagLimit");
    PushInt32(sink, "Limit", this.m_cameraLimit);
    i = 0;
    while(i < this.m_cameraLimit) {
      if(i < Size(this.m_cameraList)) {
        PushString(sink, "-" + i, ToDebugString(WeakRefToRef(this.m_cameraList[i]).GetEntityID()));
      } else {
        PushString(sink, "-" + i, "NONE");
      };
      i += 1;
    };
  }
}
