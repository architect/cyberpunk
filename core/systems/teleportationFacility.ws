
public static exec void TeleportPlayerToPosition(GameInstance gi, String xStr, String yStr, String zStr) {
  ref<GameObject> playerPuppet;
  Vector4 position;
  EulerAngles rotation;
  playerPuppet = GetPlayerSystem(gi).GetLocalPlayerMainGameObject();
  position.X = StringToFloat(xStr);
  position.Y = StringToFloat(yStr);
  position.Z = StringToFloat(zStr);
  GetTeleportationFacility(gi).Teleport(playerPuppet, position, rotation);
}
