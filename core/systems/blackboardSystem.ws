
public class BlackBoardRequestEvent extends Event {

  protected ref<IBlackboard> m_blackBoard;

  protected gameScriptedBlackboardStorage m_storageClass;

  protected CName m_entryTag;

  public final void PassBlackBoardReference(ref<IBlackboard> newBlackbord, CName blackBoardName) {
    this.m_blackBoard = newBlackbord;
    this.m_entryTag = blackBoardName;
  }

  public final ref<IBlackboard> GetBlackboardReference() {
    return this.m_blackBoard;
  }

  public final void SetStorageType(gameScriptedBlackboardStorage storageType) {
    this.m_storageClass = storageType;
  }

  public final gameScriptedBlackboardStorage GetStorageType() {
    return this.m_storageClass;
  }

  public final CName GetEntryTag() {
    return this.m_entryTag;
  }
}
