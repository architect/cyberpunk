
public abstract class AIActionTransactionSystem extends IScriptable {

  public final static array<wref<NPCEquipmentItem_Record>> ChooseSingleItemsSetFromPool(Int32 powerLevel, Uint32 seed, wref<NPCEquipmentItemPool_Record> itemPool) {
    Float randomVal;
    array<wref<NPCEquipmentItemsPoolEntry_Record>> possibleItems;
    Int32 i;
    Int32 poolSize;
    Float weightSum;
    Float accumulator;
    wref<NPCEquipmentItemsPoolEntry_Record> tempPoolEntry;
    array<wref<NPCEquipmentItem_Record>> results;
    accumulator = 0;
    poolSize = WeakRefToRef(itemPool).GetPoolCount();
    i = 0;
    while(i < poolSize) {
      tempPoolEntry = WeakRefToRef(itemPool).GetPoolItem(i);
      if(powerLevel >= WeakRefToRef(tempPoolEntry).MinLevel()) {
        Push(possibleItems, tempPoolEntry);
        weightSum += WeakRefToRef(tempPoolEntry).Weight();
      };
      i += 1;
    };
    randomVal = RandNoiseF(Cast(seed), 0, weightSum);
    i = 0;
    while(i < Size(possibleItems)) {
      accumulator += WeakRefToRef(possibleItems[i]).Weight();
      if(randomVal < accumulator) {
        WeakRefToRef(possibleItems[i]).Items(results);
        return results;
      };
      i += 1;
    };
    return results;
  }

  public final static void CalculateEquipmentItems(wref<ScriptedPuppet> puppet, CName equipmentGroupName, out array<wref<NPCEquipmentItem_Record>> items, Int32 powerLevel?) {
    wref<Character_Record> characterRecord;
    wref<NPCEquipmentGroup_Record> equipmentGroupRecord;
    characterRecord = RefToWeakRef(GetCharacterRecord(WeakRefToRef(puppet).GetRecordID()));
    if(!ToBool(characterRecord) || !IsNameValid(equipmentGroupName)) {
      return ;
    };
    if(equipmentGroupName == "PrimaryEquipment") {
      equipmentGroupRecord = WeakRefToRef(characterRecord).PrimaryEquipment();
    } else {
      if(equipmentGroupName == "SecondaryEquipment") {
        equipmentGroupRecord = WeakRefToRef(characterRecord).SecondaryEquipment();
      };
    };
    if(!ToBool(equipmentGroupRecord)) {
      return ;
    };
    CalculateEquipmentItems(puppet, equipmentGroupRecord, items, powerLevel);
  }

  public final static void CalculateEquipmentItems(wref<ScriptedPuppet> puppet, wref<NPCEquipmentGroup_Record> equipmentGroupRecord, out array<wref<NPCEquipmentItem_Record>> items, Int32 powerLevel) {
    EntityID id;
    Uint32 seed;
    Int32 i;
    Int32 x;
    Int32 itemsCount;
    wref<NPCEquipmentGroupEntry_Record> entry;
    ref<StatsSystem> statSys;
    Uint32 groupID;
    Uint64 bitsMask;
    array<wref<NPCEquipmentItem_Record>> itemsSet;
    id = WeakRefToRef(puppet).GetEntityID();
    bitsMask = Cast(PowF(2, 32)) - 1;
    if(!ToBool(equipmentGroupRecord)) {
      return ;
    };
    seed = GetHash(id);
    groupID = Cast(ToNumber(WeakRefToRef(equipmentGroupRecord).GetID()) & bitsMask);
    seed = seed ^ groupID;
    if(powerLevel < 0) {
      statSys = GetStatsSystem(WeakRefToRef(puppet).GetGame());
      powerLevel = Cast(statSys.GetStatValue(Cast(WeakRefToRef(puppet).GetEntityID()), gamedataStatType.PowerLevel));
    };
    itemsCount = WeakRefToRef(equipmentGroupRecord).GetEquipmentItemsCount();
    i = 0;
    while(i < itemsCount) {
      entry = WeakRefToRef(equipmentGroupRecord).GetEquipmentItemsItem(i);
      if(ToBool(Cast(WeakRefToRef(entry)))) {
        Push(items, RefToWeakRef(Cast(WeakRefToRef(entry))));
      } else {
        seed += 1;
        itemsSet = ChooseSingleItemsSetFromPool(powerLevel, seed, RefToWeakRef(Cast(WeakRefToRef(entry))));
        x = 0;
        while(x < Size(itemsSet)) {
          Push(items, itemsSet[x]);
          x += 1;
        };
      };
      i += 1;
    };
  }

  public final static Bool ShouldPerformEquipmentCheck(wref<ScriptedPuppet> obj, CName equipmentGroup) {
    wref<Character_Record> characterRecord;
    characterRecord = RefToWeakRef(GetCharacterRecord(WeakRefToRef(obj).GetRecordID()));
    if(!ToBool(characterRecord)) {
      return false;
    };
    if(!ToBool(obj) || !IsNameValid(equipmentGroup)) {
      return false;
    };
    if(equipmentGroup == "PrimaryEquipment") {
      if(ToBool(WeakRefToRef(characterRecord).PrimaryEquipment())) {
        return true;
      };
    } else {
      if(equipmentGroup == "SecondaryEquipment") {
        if(ToBool(WeakRefToRef(characterRecord).SecondaryEquipment())) {
          return true;
        };
      };
    };
    return false;
  }

  public final static Bool CheckEquipmentGroupForEquipment(ScriptExecutionContext context, wref<AIItemCond_Record> condition) {
    Int32 i;
    Int32 a;
    Int32 itemsInInventory;
    wref<Character_Record> characterRecord;
    ref<Item_Record> item;
    Int32 itemsCount;
    Int32 tagCount;
    Bool checkTag;
    ItemID itemID;
    array<wref<NPCEquipmentItem_Record>> items;
    characterRecord = RefToWeakRef(GetCharacterRecord(GetOwner(context).GetRecordID()));
    CalculateEquipmentItems(RefToWeakRef(Cast(GetOwner(context))), WeakRefToRef(condition).EquipmentGroup(), items);
    itemsCount = Size(items);
    if(itemsCount > 0 && !WeakRefToRef(condition).CheckAllItemsInEquipmentGroup()) {
      itemsCount = 1;
    };
    i = 0;
    while(i < itemsCount) {
      item = WeakRefToRef(WeakRefToRef(items[i]).Item());
      if(!ToBool(item)) {
      } else {
        if(ToBool(WeakRefToRef(condition).ItemType()) && WeakRefToRef(WeakRefToRef(condition).ItemType()).Name() != "") {
          if(WeakRefToRef(item.ItemType()).Type() != WeakRefToRef(WeakRefToRef(condition).ItemType()).Type()) {
          } else {
            if(WeakRefToRef(WeakRefToRef(condition).ItemCategory()).Name() != "") {
              if(WeakRefToRef(item.ItemCategory()) != WeakRefToRef(WeakRefToRef(condition).ItemCategory())) {
              } else {
                if(WeakRefToRef(condition).ItemTag() != "") {
                  tagCount = item.GetTagsCount();
                  a = 0;
                  while(a < tagCount) {
                    if(item.GetTagsItem(a) == WeakRefToRef(condition).ItemTag()) {
                      checkTag = true;
                    } else {
                      a += 1;
                    };
                  };
                  if(!checkTag) {
                  } else {
                    return true;
                  };
                };
                return true;
              };
            };
            if(WeakRefToRef(condition).ItemTag() != "") {
              tagCount = item.GetTagsCount();
              a = 0;
              while(a < tagCount) {
                if(item.GetTagsItem(a) == WeakRefToRef(condition).ItemTag()) {
                  checkTag = true;
                } else {
                  a += 1;
                };
              };
              if(!checkTag) {
              } else {
                return true;
              };
            };
            return true;
          };
        };
        if(WeakRefToRef(WeakRefToRef(condition).ItemCategory()).Name() != "") {
          if(WeakRefToRef(item.ItemCategory()) != WeakRefToRef(WeakRefToRef(condition).ItemCategory())) {
          } else {
            if(WeakRefToRef(condition).ItemTag() != "") {
              tagCount = item.GetTagsCount();
              a = 0;
              while(a < tagCount) {
                if(item.GetTagsItem(a) == WeakRefToRef(condition).ItemTag()) {
                  checkTag = true;
                } else {
                  a += 1;
                };
              };
              if(!checkTag) {
              } else {
                return true;
              };
            };
            return true;
          };
        };
        if(WeakRefToRef(condition).ItemTag() != "") {
          tagCount = item.GetTagsCount();
          a = 0;
          while(a < tagCount) {
            if(item.GetTagsItem(a) == WeakRefToRef(condition).ItemTag()) {
              checkTag = true;
            } else {
              a += 1;
            };
          };
          if(!checkTag) {
          } else {
            return true;
          };
        };
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static Bool CheckSlotsForEquipment(ScriptExecutionContext context, CName equipmentGroup) {
    array<NPCItemToEquip> itemsToEquip;
    Int32 i;
    Int32 itemsInSlots;
    wref<Character_Record> characterRecord;
    characterRecord = RefToWeakRef(GetCharacterRecord(GetOwner(context).GetRecordID()));
    switch(equipmentGroup) {
      case "PrimaryEquipment":
        if(!GetEquipment(context, true, itemsToEquip)) {
          return false;
        };
        break;
      case "SecondaryEquipment":
        if(!GetEquipment(context, false, itemsToEquip)) {
          return false;
        };
        break;
      default:
    };
    i = 0;
    while(i < Size(itemsToEquip)) {
      if(GetTransactionSystem(GetOwner(context).GetGame()).HasItemInSlot(GetOwner(context), itemsToEquip[i].slotID, itemsToEquip[i].itemID)) {
        itemsInSlots += 1;
      };
      i += 1;
    };
    if(itemsInSlots > 0) {
      return true;
    };
    return false;
  }

  public final static Bool GetEquipment(ScriptExecutionContext context, Bool checkPrimaryEquipment, out array<NPCItemToEquip> itemsList) {
    wref<ScriptedPuppet> puppet;
    wref<Character_Record> characterRecord;
    NPCItemToEquip item;
    ref<NPCEquipmentItem_Record> itemRecord;
    Int32 itemCount;
    ItemID itemID;
    Int32 i;
    wref<NPCEquipmentGroup_Record> equipmentGroup;
    array<wref<NPCEquipmentItem_Record>> items;
    TweakDBID slotId;
    puppet = RefToWeakRef(Cast(GetOwner(context)));
    if(!ToBool(puppet)) {
      return false;
    };
    characterRecord = RefToWeakRef(GetCharacterRecord(WeakRefToRef(puppet).GetRecordID()));
    if(!ToBool(characterRecord)) {
      return false;
    };
    if(checkPrimaryEquipment) {
      equipmentGroup = WeakRefToRef(characterRecord).PrimaryEquipment();
    } else {
      equipmentGroup = WeakRefToRef(characterRecord).SecondaryEquipment();
    };
    CalculateEquipmentItems(puppet, equipmentGroup, items, -1);
    itemCount = Size(items);
    i = 0;
    while(i < itemCount) {
      itemRecord = WeakRefToRef(items[i]);
      if(ToBool(itemRecord.OnBodySlot())) {
        slotId = WeakRefToRef(itemRecord.OnBodySlot()).GetID();
      };
      GetItemID(puppet, itemRecord.Item(), slotId, itemID);
      if(GetTransactionSystem(GetOwner(context).GetGame()).HasItem(GetOwner(context), itemID)) {
        item.itemID = itemID;
        item.slotID = WeakRefToRef(itemRecord.EquipSlot()).GetID();
        item.bodySlotID = slotId;
        Push(itemsList, item);
      };
      i += 1;
    };
    return Size(itemsList) > 0;
  }

  public final static Bool GetEquipmentWithCondition(ScriptExecutionContext context, Bool checkPrimaryEquipment, Bool checkForUnequip, out array<NPCItemToEquip> itemsList) {
    wref<ScriptedPuppet> puppet;
    GameInstance game;
    ref<TransactionSystem> transactionSystem;
    wref<Character_Record> characterRecord;
    NPCItemToEquip item;
    array<wref<NPCEquipmentItem_Record>> items;
    array<wref<NPCEquipmentItem_Record>> primaryItems;
    ItemID itemID;
    ItemID primaryItemID;
    array<wref<AIActionCondition_Record>> conditions;
    array<wref<AIActionCondition_Record>> primaryConditions;
    Int32 i;
    Int32 k;
    Int32 z;
    wref<NPCEquipmentGroup_Record> equipmentGroup;
    array<NPCItemToEquip> primaryItemsToEquip;
    array<NPCItemToEquip> secondaryItemsToEquip;
    ref<IBlackboard> BBoard;
    array<ItemID> lastEquippedItems;
    Float lastUnequipTimestamp;
    Variant itemsVariant;
    TweakDBID bodySlotId;
    ref<NPCEquipmentItem_Record> currentItem;
    TweakDBID defaultID;
    puppet = RefToWeakRef(Cast(GetOwner(context)));
    if(!ToBool(puppet)) {
      return false;
    };
    game = WeakRefToRef(puppet).GetGame();
    characterRecord = RefToWeakRef(GetCharacterRecord(WeakRefToRef(puppet).GetRecordID()));
    if(!ToBool(characterRecord)) {
      return false;
    };
    transactionSystem = GetTransactionSystem(game);
    if(checkPrimaryEquipment) {
      equipmentGroup = WeakRefToRef(characterRecord).PrimaryEquipment();
    } else {
      equipmentGroup = WeakRefToRef(characterRecord).SecondaryEquipment();
    };
    CalculateEquipmentItems(puppet, equipmentGroup, items, -1);
    i = 0;
    while(i < Size(items)) {
      currentItem = WeakRefToRef(items[i]);
      if(ToBool(currentItem.OnBodySlot())) {
        bodySlotId = WeakRefToRef(currentItem.OnBodySlot()).GetID();
      };
      if(ToBool(currentItem.Item()) && ToBool(currentItem.EquipSlot()) && GetItemID(puppet, currentItem.Item(), bodySlotId, itemID)) {
        Clear(conditions);
        if(!checkForUnequip && !transactionSystem.HasItemInSlot(WeakRefToRef(puppet), WeakRefToRef(currentItem.EquipSlot()).GetID(), itemID) && transactionSystem.HasItem(WeakRefToRef(puppet), itemID)) {
          currentItem.EquipCondition(conditions);
        } else {
          if(checkForUnequip && transactionSystem.HasItemInSlot(WeakRefToRef(puppet), WeakRefToRef(currentItem.EquipSlot()).GetID(), itemID) || GetTDBID(transactionSystem.GetItemInSlot(WeakRefToRef(puppet), "AttachmentSlots.WeaponRight").GetItemID()) == "Items.Npc_fists_wounded") {
            currentItem.UnequipCondition(conditions);
          } else {
          };
        };
        if(checkForUnequip && Size(conditions) == 0 && checkPrimaryEquipment && GetTDBID(transactionSystem.GetItemInSlot(WeakRefToRef(puppet), "AttachmentSlots.WeaponRight").GetItemID()) != "Items.Npc_fists_wounded") {
        } else {
          if(!checkForUnequip) {
            BBoard = WeakRefToRef(puppet).GetAIControllerComponent().GetActionBlackboard();
            if(ToBool(BBoard)) {
              itemsVariant = BBoard.GetVariant(GetAllBlackboardDefs().AIAction.ownerLastEquippedItems);
              if(IsValid(itemsVariant)) {
                lastEquippedItems = FromVariant(itemsVariant);
              };
              lastUnequipTimestamp = BBoard.GetFloat(GetAllBlackboardDefs().AIAction.ownerLastUnequipTimestamp);
            };
            if(Contains(lastEquippedItems, itemID) && ToFloat(GetSimTime(game)) < lastUnequipTimestamp + 3) {
            } else {
              if(Size(conditions) > 0 && !CheckActionConditions(context, ToScriptRef(conditions))) {
              } else {
                if(!checkForUnequip && Size(conditions) == 0 && Size(itemsList) > 0) {
                  k = 0;
                  while(k < Size(itemsList)) {
                    if(itemsList[k].slotID == WeakRefToRef(currentItem.EquipSlot()).GetID()) {
                    };
                    k += 1;
                  };
                };
                if(!checkPrimaryEquipment && checkForUnequip) {
                  GetEquipmentWithCondition(context, true, false, primaryItemsToEquip);
                  GetEquipmentWithCondition(context, false, false, secondaryItemsToEquip);
                  if(Size(primaryItemsToEquip) == 0 && Size(secondaryItemsToEquip) == 0) {
                  } else {
                    if(Size(primaryItemsToEquip) > 0 && transactionSystem.HasItemInSlot(WeakRefToRef(puppet), primaryItemsToEquip[0].slotID, primaryItemsToEquip[0].itemID) || Size(secondaryItemsToEquip) > 0 && transactionSystem.HasItemInSlot(WeakRefToRef(puppet), secondaryItemsToEquip[0].slotID, secondaryItemsToEquip[0].itemID)) {
                    } else {
                      CalculateEquipmentItems(puppet, WeakRefToRef(characterRecord).PrimaryEquipment(), primaryItems, -1);
                      z = 0;
                      while(z < Size(primaryItems)) {
                        GetItemID(puppet, WeakRefToRef(primaryItems[z]).Item(), ToBool(WeakRefToRef(primaryItems[z]).OnBodySlot()) ? WeakRefToRef(WeakRefToRef(primaryItems[z]).OnBodySlot()).GetID() : defaultID, primaryItemID);
                        if(itemID == primaryItemID) {
                          WeakRefToRef(primaryItems[z]).EquipCondition(primaryConditions);
                          if(CheckActionConditions(context, ToScriptRef(primaryConditions))) {
                          };
                        };
                        z += 1;
                      };
                      item.itemID = itemID;
                      item.slotID = WeakRefToRef(currentItem.EquipSlot()).GetID();
                      item.bodySlotID = bodySlotId;
                      Push(itemsList, item);
                    };
                  };
                };
                item.itemID = itemID;
                item.slotID = WeakRefToRef(currentItem.EquipSlot()).GetID();
                item.bodySlotID = bodySlotId;
                Push(itemsList, item);
              };
            };
          };
          if(Size(conditions) > 0 && !CheckActionConditions(context, ToScriptRef(conditions))) {
          } else {
            if(!checkForUnequip && Size(conditions) == 0 && Size(itemsList) > 0) {
              k = 0;
              while(k < Size(itemsList)) {
                if(itemsList[k].slotID == WeakRefToRef(currentItem.EquipSlot()).GetID()) {
                };
                k += 1;
              };
            };
            if(!checkPrimaryEquipment && checkForUnequip) {
              GetEquipmentWithCondition(context, true, false, primaryItemsToEquip);
              GetEquipmentWithCondition(context, false, false, secondaryItemsToEquip);
              if(Size(primaryItemsToEquip) == 0 && Size(secondaryItemsToEquip) == 0) {
              } else {
                if(Size(primaryItemsToEquip) > 0 && transactionSystem.HasItemInSlot(WeakRefToRef(puppet), primaryItemsToEquip[0].slotID, primaryItemsToEquip[0].itemID) || Size(secondaryItemsToEquip) > 0 && transactionSystem.HasItemInSlot(WeakRefToRef(puppet), secondaryItemsToEquip[0].slotID, secondaryItemsToEquip[0].itemID)) {
                } else {
                  CalculateEquipmentItems(puppet, WeakRefToRef(characterRecord).PrimaryEquipment(), primaryItems, -1);
                  z = 0;
                  while(z < Size(primaryItems)) {
                    GetItemID(puppet, WeakRefToRef(primaryItems[z]).Item(), ToBool(WeakRefToRef(primaryItems[z]).OnBodySlot()) ? WeakRefToRef(WeakRefToRef(primaryItems[z]).OnBodySlot()).GetID() : defaultID, primaryItemID);
                    if(itemID == primaryItemID) {
                      WeakRefToRef(primaryItems[z]).EquipCondition(primaryConditions);
                      if(CheckActionConditions(context, ToScriptRef(primaryConditions))) {
                      };
                    };
                    z += 1;
                  };
                  item.itemID = itemID;
                  item.slotID = WeakRefToRef(currentItem.EquipSlot()).GetID();
                  item.bodySlotID = bodySlotId;
                  Push(itemsList, item);
                };
              };
            };
            item.itemID = itemID;
            item.slotID = WeakRefToRef(currentItem.EquipSlot()).GetID();
            item.bodySlotID = bodySlotId;
            Push(itemsList, item);
          };
        };
      } else {
      };
      i += 1;
    };
    return Size(itemsList) > 0;
  }

  public final static Bool GetDefaultEquipment(ScriptExecutionContext context, wref<Character_Record> characterRecord, Bool checkForUnequip, out array<NPCItemToEquip> itemsList) {
    array<wref<NPCEquipmentItem_Record>> items;
    NPCItemToEquip item;
    ItemID itemID;
    Int32 i;
    array<NPCItemToEquip> primaryItemsToEquip;
    ref<NPCEquipmentItem_Record> defaultItem;
    Bool sendData;
    TweakDBID onBodySlotID;
    CalculateEquipmentItems(RefToWeakRef(Cast(GetOwner(context))), WeakRefToRef(characterRecord).SecondaryEquipment(), items, -1);
    sendData = false;
    GetEquipmentWithCondition(context, true, false, primaryItemsToEquip);
    if(checkForUnequip) {
      if(Size(primaryItemsToEquip) > 0 && !GetTransactionSystem(GetOwner(context).GetGame()).HasItemInSlot(GetOwner(context), primaryItemsToEquip[0].slotID, primaryItemsToEquip[0].itemID)) {
        if(!GetTransactionSystem(GetOwner(context).GetGame()).HasItemInSlot(GetOwner(context), primaryItemsToEquip[0].slotID, primaryItemsToEquip[0].itemID)) {
          sendData = true;
        };
      };
    };
    if(sendData) {
      i = Size(items) - 1;
      if(ToBool(WeakRefToRef(items[i]).OnBodySlot())) {
        onBodySlotID = WeakRefToRef(WeakRefToRef(items[i]).OnBodySlot()).GetID();
      };
      GetItemID(RefToWeakRef(Cast(GetOwner(context))), WeakRefToRef(items[i]).Item(), onBodySlotID, itemID);
      if(Size(items) > 0 && GetTransactionSystem(GetOwner(context).GetGame()).HasItem(GetOwner(context), itemID)) {
        item.itemID = itemID;
        item.slotID = WeakRefToRef(WeakRefToRef(items[i]).EquipSlot()).GetID();
      } else {
        defaultItem = WeakRefToRef(WeakRefToRef(characterRecord).DefaultEquipment());
        GetItemID(RefToWeakRef(Cast(GetOwner(context))), defaultItem.Item(), WeakRefToRef(defaultItem.OnBodySlot()).GetID(), itemID);
        if(!GetTransactionSystem(GetOwner(context).GetGame()).HasItem(GetOwner(context), itemID)) {
          GetTransactionSystem(GetOwner(context).GetGame()).GiveItem(GetOwner(context), itemID, 1);
        };
        item.itemID = itemID;
        item.slotID = WeakRefToRef(defaultItem.EquipSlot()).GetID();
      };
      if(!checkForUnequip || GetTransactionSystem(GetOwner(context).GetGame()).HasItemInSlot(GetOwner(context), item.slotID, item.itemID)) {
        Push(itemsList, item);
        return true;
      };
    };
    return false;
  }

  public final static Bool GetOnBodyEquipment(wref<ScriptedPuppet> obj, out array<NPCItemToEquip> itemsToEquip) {
    wref<Character_Record> characterRecord;
    wref<NPCEquipmentGroup_Record> equipmentGroup;
    NPCItemToEquip itemToEquip;
    array<wref<NPCEquipmentItem_Record>> items;
    ItemID itemID;
    Int32 i;
    if(!ToBool(obj)) {
      return false;
    };
    characterRecord = RefToWeakRef(GetCharacterRecord(WeakRefToRef(obj).GetRecordID()));
    if(!ToBool(characterRecord)) {
      return false;
    };
    equipmentGroup = WeakRefToRef(characterRecord).PrimaryEquipment();
    if(!ToBool(equipmentGroup)) {
      return false;
    };
    CalculateEquipmentItems(obj, equipmentGroup, items, -1);
    i = 0;
    while(i < Size(items)) {
      if(ToBool(WeakRefToRef(items[i]).Item()) && GetItemIDFromRecord(WeakRefToRef(items[i]).Item(), itemID)) {
        itemToEquip.itemID = itemID;
        if(ToBool(WeakRefToRef(items[i]).OnBodySlot())) {
          itemToEquip.bodySlotID = WeakRefToRef(WeakRefToRef(items[i]).OnBodySlot()).GetID();
        };
        Push(itemsToEquip, itemToEquip);
      };
      i += 1;
    };
    equipmentGroup = WeakRefToRef(characterRecord).SecondaryEquipment();
    Clear(items);
    CalculateEquipmentItems(obj, equipmentGroup, items, -1);
    i = 0;
    while(i < Size(items)) {
      if(ToBool(WeakRefToRef(items[i]).Item()) && GetItemIDFromRecord(WeakRefToRef(items[i]).Item(), itemID)) {
        itemToEquip.itemID = itemID;
        if(ToBool(WeakRefToRef(items[i]).OnBodySlot())) {
          itemToEquip.bodySlotID = WeakRefToRef(WeakRefToRef(items[i]).OnBodySlot()).GetID();
        };
        Push(itemsToEquip, itemToEquip);
      };
      i += 1;
    };
    return Size(itemsToEquip) > 0;
  }

  public final static Bool GetOnBodyEquipmentRecords(wref<ScriptedPuppet> obj, out array<wref<NPCEquipmentItem_Record>> outEquipmentRecords) {
    wref<Character_Record> characterRecord;
    wref<NPCEquipmentGroup_Record> equipmentGroup;
    array<wref<NPCEquipmentItem_Record>> items;
    Int32 i;
    if(!ToBool(obj)) {
      return false;
    };
    characterRecord = RefToWeakRef(GetCharacterRecord(WeakRefToRef(obj).GetRecordID()));
    if(!ToBool(characterRecord)) {
      return false;
    };
    equipmentGroup = WeakRefToRef(characterRecord).PrimaryEquipment();
    if(!ToBool(equipmentGroup)) {
      return false;
    };
    CalculateEquipmentItems(obj, equipmentGroup, items, -1);
    i = 0;
    while(i < Size(items)) {
      if(ToBool(WeakRefToRef(items[i]).Item()) && ToBool(WeakRefToRef(items[i]).OnBodySlot())) {
        Push(outEquipmentRecords, items[i]);
      };
      i += 1;
    };
    equipmentGroup = WeakRefToRef(characterRecord).SecondaryEquipment();
    Clear(items);
    CalculateEquipmentItems(obj, equipmentGroup, items, -1);
    i = 0;
    while(i < Size(items)) {
      if(ToBool(WeakRefToRef(items[i]).Item()) && ToBool(WeakRefToRef(items[i]).OnBodySlot())) {
        Push(outEquipmentRecords, items[i]);
      };
      i += 1;
    };
    return Size(outEquipmentRecords) > 0;
  }

  public final static Bool GetItemsBodySlot(wref<ScriptedPuppet> owner, ItemID itemID, out TweakDBID onBodySlotID) {
    array<wref<NPCEquipmentItem_Record>> equipmentRecords;
    Int32 i;
    if(!GetOnBodyEquipmentRecords(owner, equipmentRecords)) {
      return false;
    };
    i = 0;
    while(i < Size(equipmentRecords)) {
      if(WeakRefToRef(WeakRefToRef(equipmentRecords[i]).Item()).GetID() == GetTDBID(itemID)) {
        onBodySlotID = WeakRefToRef(WeakRefToRef(equipmentRecords[i]).OnBodySlot()).GetID();
        return IsValid(onBodySlotID);
      };
      i += 1;
    };
    return false;
  }

  public final static Bool GetItemID(wref<ScriptedPuppet> obj, wref<Item_Record> itemRecord, TweakDBID onBodySlotID, out ItemID itemID) {
    ref<ItemObject> itemObj;
    if(!ToBool(itemRecord)) {
      return false;
    };
    if(ToBool(obj) && IsValid(onBodySlotID)) {
      itemObj = GetTransactionSystem(WeakRefToRef(obj).GetGame()).GetItemInSlot(WeakRefToRef(obj), onBodySlotID);
      if(ToBool(itemObj)) {
        itemID = itemObj.GetItemID();
        if(GetTDBID(itemID) == WeakRefToRef(itemRecord).GetID()) {
          return true;
        };
      };
    };
    itemID = CreateQuery(WeakRefToRef(itemRecord).GetID());
    return IsValid(itemID);
  }

  public final static Bool GetItemIDFromRecord(wref<Item_Record> itemRecord, out ItemID itemID) {
    if(!ToBool(itemRecord)) {
      return false;
    };
    itemID = CreateQuery(WeakRefToRef(itemRecord).GetID());
    return IsValid(itemID);
  }

  public final static Bool GetFirstItemID(wref<GameObject> owner, CName itemTag, out ItemID itemID) {
    array<wref<gameItemData>> itemList;
    if(itemTag != "") {
      GetTransactionSystem(WeakRefToRef(owner).GetGame()).GetItemListByTag(WeakRefToRef(owner), itemTag, itemList);
    } else {
      GetTransactionSystem(WeakRefToRef(owner).GetGame()).GetItemList(WeakRefToRef(owner), itemList);
    };
    if(Size(itemList) > 0) {
      itemID = WeakRefToRef(itemList[0]).GetID();
      return true;
    };
    return false;
  }

  public final static Bool GetFirstItemID(wref<GameObject> owner, wref<ItemType_Record> itemType, CName itemTag, out ItemID itemID) {
    array<wref<gameItemData>> itemList;
    Int32 i;
    if(itemTag != "") {
      GetTransactionSystem(WeakRefToRef(owner).GetGame()).GetItemListByTag(WeakRefToRef(owner), itemTag, itemList);
    } else {
      GetTransactionSystem(WeakRefToRef(owner).GetGame()).GetItemList(WeakRefToRef(owner), itemList);
    };
    i = 0;
    while(i < Size(itemList)) {
      if(WeakRefToRef(GetItemRecord(GetTDBID(WeakRefToRef(itemList[i]).GetID())).ItemType()) == WeakRefToRef(itemType)) {
        itemID = WeakRefToRef(itemList[i]).GetID();
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static Bool GetFirstItemID(wref<GameObject> owner, wref<ItemCategory_Record> itemCategory, CName itemTag, out ItemID itemID) {
    array<wref<gameItemData>> itemList;
    Int32 i;
    if(itemTag != "") {
      GetTransactionSystem(WeakRefToRef(owner).GetGame()).GetItemListByTag(WeakRefToRef(owner), itemTag, itemList);
    } else {
      GetTransactionSystem(WeakRefToRef(owner).GetGame()).GetItemList(WeakRefToRef(owner), itemList);
    };
    i = 0;
    while(i < Size(itemList)) {
      if(WeakRefToRef(GetItemRecord(GetTDBID(WeakRefToRef(itemList[i]).GetID())).ItemCategory()) == WeakRefToRef(itemCategory)) {
        itemID = WeakRefToRef(itemList[i]).GetID();
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static Bool IsSlotEmptySpawningItem(wref<GameObject> owner, TweakDBID slotID) {
    if(!ToBool(owner)) {
      return false;
    };
    if(!IsValid(slotID)) {
      return false;
    };
    return GetTransactionSystem(WeakRefToRef(owner).GetGame()).IsSlotEmptySpawningItem(WeakRefToRef(owner), slotID);
  }

  public final static Bool DoesItemMeetRequirements(ItemID weaponItemID, ref<AIItemCond_Record> condition, wref<WeaponEvolution_Record> evolution) {
    wref<WeaponItem_Record> weaponRecord;
    Int32 triggerModeCount;
    triggerModeCount = condition.GetTriggerModesCount();
    if(!ToBool(evolution) && triggerModeCount == 0) {
      return true;
    };
    weaponRecord = RefToWeakRef(Cast(GetItemRecord(GetTDBID(weaponItemID))));
    if(ToBool(weaponRecord)) {
      if(ToBool(evolution) && WeakRefToRef(WeakRefToRef(weaponRecord).Evolution()) != WeakRefToRef(evolution)) {
        return false;
      };
      if(triggerModeCount > 0) {
        if(!ToBool(WeakRefToRef(weaponRecord).PrimaryTriggerMode()) || !condition.TriggerModesContains(WeakRefToRef(weaponRecord).PrimaryTriggerMode())) {
          return false;
        };
      };
      return true;
    };
    return false;
  }
}
