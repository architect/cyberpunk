
public static exec void Slowmo(GameInstance gameInstance) {
  GetTimeSystem(gameInstance).SetTimeDilation("consoleCommand", 0.10000000149011612);
}

public static exec void Noslowmo(GameInstance gameInstance) {
  GetTimeSystem(gameInstance).UnsetTimeDilation("consoleCommand");
}

public static exec void SetTimeDilation(GameInstance gameInstance, String amount) {
  Float famount;
  famount = StringToFloat(amount);
  if(famount > 0) {
    GetTimeSystem(gameInstance).SetTimeDilation("consoleCommand", famount);
  } else {
    GetTimeSystem(gameInstance).UnsetTimeDilation("consoleCommand");
  };
}
