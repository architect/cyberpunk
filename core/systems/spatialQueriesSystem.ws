
public class SpatialQueriesHelper extends IScriptable {

  public final static Bool HasSpaceInFront(wref<GameObject> sourceObject, Float groundClearance, Float areaWidth, Float areaLength, Float areaHeight) {
    Bool hasSpace;
    hasSpace = HasSpaceInFront(sourceObject, WeakRefToRef(sourceObject).GetWorldForward(), groundClearance, areaWidth, areaLength, areaHeight);
    return hasSpace;
  }

  public final static Bool HasSpaceInFront(wref<GameObject> sourceObject, Vector4 queryDirection, Float groundClearance, Float areaWidth, Float areaLength, Float areaHeight) {
    Bool overlapSuccessStatic;
    Bool overlapSuccessVehicle;
    TraceResult fitTestOvelap;
    Vector4 queryPosition;
    Vector4 boxDimensions;
    EulerAngles boxOrientation;
    queryDirection.Z = 0;
    queryDirection = Normalize(queryDirection);
    boxDimensions.X = areaWidth * 0.5;
    boxDimensions.Y = areaLength * 0.5;
    boxDimensions.Z = areaHeight * 0.5;
    queryPosition = WeakRefToRef(sourceObject).GetWorldPosition();
    queryPosition.Z += boxDimensions.Z + groundClearance;
    queryPosition += boxDimensions.Y * queryDirection;
    boxOrientation = ToEulerAngles(BuildFromDirectionVector(queryDirection));
    overlapSuccessStatic = GetSpatialQueriesSystem(WeakRefToRef(sourceObject).GetGame()).Overlap(boxDimensions, queryPosition, boxOrientation, "Static", fitTestOvelap);
    overlapSuccessVehicle = GetSpatialQueriesSystem(WeakRefToRef(sourceObject).GetGame()).Overlap(boxDimensions, queryPosition, boxOrientation, "Vehicle", fitTestOvelap);
    return !overlapSuccessStatic && !overlapSuccessVehicle;
  }

  public final static Bool GetFloorAngle(wref<GameObject> sourceObject, out Float floorAngle) {
    Vector4 startPosition;
    Vector4 endPosition;
    Bool raycastSuccess;
    TraceResult raycastResult;
    startPosition = WeakRefToRef(sourceObject).GetWorldPosition() + new Vector4(0,0,0.10000000149011612,0);
    endPosition = WeakRefToRef(sourceObject).GetWorldPosition() + new Vector4(0,0,-0.30000001192092896,0);
    if(GetSpatialQueriesSystem(WeakRefToRef(sourceObject).GetGame()).SyncRaycastByCollisionGroup(startPosition, endPosition, "Static", raycastResult, true, false)) {
      floorAngle = GetAngleBetween(Cast(raycastResult.normal), WeakRefToRef(sourceObject).GetWorldUp());
      return true;
    };
    return false;
  }
}
