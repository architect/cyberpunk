
public class SelectorController extends inkLogicController {

  [Default(SelectorController, Panel/Label))]
  public edit CName m_labelPath;

  [Default(SelectorController, Panel/Value))]
  public edit CName m_valuePath;

  [Default(SelectorController, Panel/LeftArrow))]
  public edit CName m_leftArrowPath;

  [Default(SelectorController, Panel/RightArrow))]
  public edit CName m_rightArrowPath;

  protected wref<inkText> m_label;

  protected wref<inkText> m_value;

  protected wref<inkWidget> m_leftArrow;

  protected wref<inkWidget> m_rightArrow;

  protected wref<inkButtonController> m_rightArrowButton;

  protected wref<inkButtonController> m_leftArrowButton;

  public final native void AddValues(array<String> values)

  public final native void AddValue(String value)

  public final native void Clear()

  public final native array<String> GetValues()

  public final native Int32 GetValuesCount()

  public final native Bool IsCyclical()

  public final native Int32 GetCurrIndex()

  public final native void SetCurrIndex(Int32 index)

  public final native void SetCurrIndexWithDirection(Int32 index, inkSelectorChangeDirection changeDirection)

  public final native Int32 Next()

  public final native void Prior()

  public final void SetLabel(String label) {
    if(ToBool(this.m_label)) {
      WeakRefToRef(this.m_label).SetText(label);
    };
  }

  protected cb Bool OnInitialize() {
    if(IsNameValid(this.m_labelPath)) {
      this.m_label = RefToWeakRef(Cast(WeakRefToRef(GetWidget(this.m_labelPath))));
    };
    this.m_value = RefToWeakRef(Cast(WeakRefToRef(GetWidget(this.m_valuePath))));
    this.m_leftArrow = GetWidget(this.m_leftArrowPath);
    if(ToBool(this.m_leftArrow)) {
      WeakRefToRef(this.m_leftArrow).RegisterToCallback("OnRelease", this, "OnLeft");
      this.m_leftArrowButton = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(this.m_leftArrow).GetControllerByType("inkButtonController"))));
    };
    this.m_rightArrow = GetWidget(this.m_rightArrowPath);
    if(ToBool(this.m_rightArrow)) {
      WeakRefToRef(this.m_rightArrow).RegisterToCallback("OnRelease", this, "OnRight");
      this.m_rightArrowButton = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(this.m_rightArrow).GetControllerByType("inkButtonController"))));
    };
  }

  protected cb Bool OnUpdateValue(String value, Int32 index, inkSelectorChangeDirection changeDirection) {
    Bool isCyclical;
    Int32 valuesCount;
    Bool hasMoreThanOneValue;
    valuesCount = GetValuesCount();
    hasMoreThanOneValue = valuesCount > 1;
    isCyclical = IsCyclical();
    if(ToBool(this.m_value)) {
      WeakRefToRef(this.m_value).SetText(value);
    };
    if(hasMoreThanOneValue) {
      if(!isCyclical && ToBool(this.m_leftArrowButton)) {
        WeakRefToRef(this.m_leftArrowButton).SetEnabled(GetCurrIndex() != 0);
      };
      if(!isCyclical && ToBool(this.m_rightArrowButton)) {
        WeakRefToRef(this.m_rightArrowButton).SetEnabled(GetCurrIndex() != valuesCount - 1);
      };
    } else {
      WeakRefToRef(this.m_leftArrowButton).SetEnabled(false);
      WeakRefToRef(this.m_rightArrowButton).SetEnabled(false);
    };
  }

  protected cb Bool OnLeft(ref<inkPointerEvent> e) {
    if(e.IsAction("click")) {
      Prior();
    };
  }

  protected cb Bool OnRight(ref<inkPointerEvent> e) {
    if(e.IsAction("click")) {
      Next();
    };
  }
}
