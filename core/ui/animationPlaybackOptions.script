
public static inkAnimOptions GetAnimOptions(Bool playReversed?, Float executionDelay?, inkanimLoopType loopType?, Uint32 loopCounter?, Bool loopInfinite?, CName fromMarker?, CName toMarker?, Bool oneSegment?) {
  inkAnimOptions animOptions;
  animOptions.playReversed = playReversed;
  animOptions.executionDelay = executionDelay;
  animOptions.loopType = loopType;
  animOptions.loopCounter = loopCounter;
  animOptions.loopInfinite = loopInfinite;
  animOptions.fromMarker = fromMarker;
  animOptions.toMarker = toMarker;
  animOptions.oneSegment = oneSegment;
  return animOptions;
}

public static inkAnimOptions GetAnimOptionsInfiniteLoop(inkanimLoopType loopType) {
  inkAnimOptions animOptions;
  animOptions.loopType = loopType;
  animOptions.loopInfinite = true;
  return animOptions;
}

public static inkAnimOptions GetAnimOptionsInfiniteLoopFinish() {
  inkAnimOptions animOptions;
  animOptions.loopType = inkanimLoopType.;
  animOptions.loopInfinite = false;
  return animOptions;
}

public class WidgetAnimationManager extends IScriptable {

  private array<SWidgetAnimationData> m_animations;

  public final void Initialize(array<SWidgetAnimationData> animations) {
    this.m_animations = animations;
  }

  public final const array<SWidgetAnimationData> GetAnimations() {
    return this.m_animations;
  }

  public final void UpdateAnimationsList(CName animName, ref<PlaybackOptionsUpdateData> updateData) {
    SWidgetAnimationData animationData;
    if(!ToBool(updateData)) {
      return ;
    };
    if(!HasAnimation(animName)) {
      animationData.m_animationName = animName;
      animationData.m_playbackOptions = updateData.m_playbackOptions;
      Push(this.m_animations, animationData);
    };
  }

  public final const Bool HasAnimation(CName animName) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(this.m_animations[i].m_animationName == animName) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final void CleanAllAnimationsChachedData() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(this.m_animations[i].m_animProxy != null) {
        UnregisterAllCallbacks(this.m_animations[i]);
      };
      i += 1;
    };
  }

  public final void TriggerAnimations(ref<inkLogicController> owner) {
    ref<inkAnimProxy> currentProxy;
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(!IsNameValid(this.m_animations[i].m_animationName)) {
      } else {
        if(this.m_animations[i].m_animProxy == null || !this.m_animations[i].m_lockWhenActive || this.m_animations[i].m_animProxy.IsFinished() || !this.m_animations[i].m_animProxy.IsPaused() && !this.m_animations[i].m_animProxy.IsPlaying()) {
          if(this.m_animations[i].m_animProxy != null) {
            UnregisterAllCallbacks(this.m_animations[i]);
          };
          currentProxy = owner.PlayLibraryAnimation(this.m_animations[i].m_animationName, this.m_animations[i].m_playbackOptions);
          this.m_animations[i].m_animProxy = currentProxy;
          RegisterAllCallbacks(owner, this.m_animations[i]);
        };
      };
      i += 1;
    };
  }

  public final void TriggerAnimations(ref<inkGameController> owner) {
    ref<inkAnimProxy> currentProxy;
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(!IsNameValid(this.m_animations[i].m_animationName)) {
      } else {
        if(this.m_animations[i].m_animProxy == null || !this.m_animations[i].m_lockWhenActive || this.m_animations[i].m_animProxy.IsFinished() || !this.m_animations[i].m_animProxy.IsPaused() && !this.m_animations[i].m_animProxy.IsPlaying()) {
          if(this.m_animations[i].m_animProxy != null) {
            UnregisterAllCallbacks(this.m_animations[i]);
          };
          currentProxy = owner.PlayLibraryAnimation(this.m_animations[i].m_animationName, this.m_animations[i].m_playbackOptions);
          this.m_animations[i].m_animProxy = currentProxy;
          RegisterAllCallbacks(owner, this.m_animations[i]);
        };
      };
      i += 1;
    };
  }

  public final void TriggerAnimationByName(ref<inkLogicController> owner, CName animName, EInkAnimationPlaybackOption playbackOption, ref<inkWidget> targetWidget?, ref<PlaybackOptionsUpdateData> playbackOptionsOverrideData?) {
    ref<inkAnimProxy> currentProxy;
    inkAnimOptions playbackOptionsData;
    SWidgetAnimationData animData;
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(!IsNameValid(this.m_animations[i].m_animationName)) {
      } else {
        if(this.m_animations[i].m_animationName == animName) {
          if(playbackOption == EInkAnimationPlaybackOption.PLAY) {
            if(this.m_animations[i].m_animProxy == null || !this.m_animations[i].m_lockWhenActive || this.m_animations[i].m_animProxy.IsFinished() || !this.m_animations[i].m_animProxy.IsPaused() && !this.m_animations[i].m_animProxy.IsPlaying()) {
              if(ToBool(playbackOptionsOverrideData)) {
                playbackOptionsData = playbackOptionsOverrideData.m_playbackOptions;
              } else {
                playbackOptionsData = this.m_animations[i].m_playbackOptions;
              };
              if(this.m_animations[i].m_animProxy != null) {
                ResolveActiveAnimDataPlaybackState(this.m_animations[i], EInkAnimationPlaybackOption.STOP);
              };
              if(ToBool(targetWidget)) {
                currentProxy = owner.PlayLibraryAnimationOnAutoSelectedTargets(this.m_animations[i].m_animationName, targetWidget, playbackOptionsData);
              } else {
                currentProxy = owner.PlayLibraryAnimation(this.m_animations[i].m_animationName, playbackOptionsData);
              };
              this.m_animations[i].m_animProxy = currentProxy;
              RegisterAllCallbacks(owner, this.m_animations[i]);
            };
          } else {
            if(this.m_animations[i].m_animProxy != null) {
              animData = this.m_animations[i];
              if(ToBool(playbackOptionsOverrideData)) {
                animData.m_playbackOptions = playbackOptionsOverrideData.m_playbackOptions;
              };
              ResolveActiveAnimDataPlaybackState(animData, playbackOption);
            };
          };
        } else {
          i += 1;
        };
      };
    };
  }

  public final void TriggerAnimationByName(ref<inkGameController> owner, CName animName, EInkAnimationPlaybackOption playbackOption, ref<inkWidget> targetWidget?, ref<PlaybackOptionsUpdateData> playbackOptionsOverrideData?) {
    ref<inkAnimProxy> currentProxy;
    inkAnimOptions playbackOptionsData;
    SWidgetAnimationData animData;
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(!IsNameValid(this.m_animations[i].m_animationName)) {
      } else {
        if(this.m_animations[i].m_animationName == animName) {
          if(playbackOption == EInkAnimationPlaybackOption.PLAY) {
            if(this.m_animations[i].m_animProxy == null || !this.m_animations[i].m_lockWhenActive || this.m_animations[i].m_animProxy.IsFinished() || !this.m_animations[i].m_animProxy.IsPaused() && !this.m_animations[i].m_animProxy.IsPlaying()) {
              if(ToBool(playbackOptionsOverrideData)) {
                playbackOptionsData = playbackOptionsOverrideData.m_playbackOptions;
              } else {
                playbackOptionsData = this.m_animations[i].m_playbackOptions;
              };
              if(this.m_animations[i].m_animProxy != null) {
                ResolveActiveAnimDataPlaybackState(this.m_animations[i], EInkAnimationPlaybackOption.STOP);
              };
              if(ToBool(targetWidget)) {
                currentProxy = owner.PlayLibraryAnimationOnAutoSelectedTargets(this.m_animations[i].m_animationName, targetWidget, playbackOptionsData);
              } else {
                currentProxy = owner.PlayLibraryAnimation(this.m_animations[i].m_animationName, playbackOptionsData);
              };
              this.m_animations[i].m_animProxy = currentProxy;
              RegisterAllCallbacks(owner, this.m_animations[i]);
            };
          } else {
            if(this.m_animations[i].m_animProxy != null) {
              animData = this.m_animations[i];
              if(ToBool(playbackOptionsOverrideData)) {
                animData.m_playbackOptions = playbackOptionsOverrideData.m_playbackOptions;
              };
              ResolveActiveAnimDataPlaybackState(animData, playbackOption);
            };
          };
        } else {
          i += 1;
        };
      };
    };
  }

  private final void ResolveActiveAnimDataPlaybackState(SWidgetAnimationData animData, EInkAnimationPlaybackOption requestedState) {
    if(animData.m_animProxy == null) {
      return ;
    };
    if(requestedState == EInkAnimationPlaybackOption.STOP) {
      animData.m_animProxy.Stop(false);
      UnregisterAllCallbacks(animData);
    } else {
      if(requestedState == EInkAnimationPlaybackOption.PAUSE) {
        animData.m_animProxy.Pause();
      } else {
        if(requestedState == EInkAnimationPlaybackOption.RESUME) {
          animData.m_animProxy.Resume();
        } else {
          if(requestedState == EInkAnimationPlaybackOption.CONTINUE) {
            animData.m_animProxy.Continue(animData.m_playbackOptions);
          } else {
            if(requestedState == EInkAnimationPlaybackOption.GO_TO_START) {
              animData.m_animProxy.GotoStartAndStop(false);
              UnregisterAllCallbacks(animData);
            } else {
              if(requestedState == EInkAnimationPlaybackOption.GO_TO_END) {
                animData.m_animProxy.GotoEndAndStop(false);
                UnregisterAllCallbacks(animData);
              };
            };
          };
        };
      };
    };
  }

  public final void UnregisterAllCallbacks(SWidgetAnimationData animData) {
    if(animData.m_animProxy != null) {
      if(IsNameValid(animData.m_onFinish)) {
        animData.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnFinish);
      };
      if(IsNameValid(animData.m_onStart)) {
        animData.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnStart);
      };
      if(IsNameValid(animData.m_onPasue)) {
        animData.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnPause);
      };
      if(IsNameValid(animData.m_onResume)) {
        animData.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnResume);
      };
      if(IsNameValid(animData.m_onStartLoop)) {
        animData.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnStartLoop);
      };
      if(IsNameValid(animData.m_onEndLoop)) {
        animData.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnEndLoop);
      };
    };
    CleanProxyData(animData);
  }

  public final void RegisterAllCallbacks(ref<IScriptable> owner, SWidgetAnimationData animData) {
    if(animData.m_animProxy != null) {
      if(IsNameValid(animData.m_onFinish)) {
        animData.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, owner, animData.m_onFinish);
      };
      if(IsNameValid(animData.m_onStart)) {
        animData.m_animProxy.RegisterToCallback(inkanimEventType.OnStart, owner, animData.m_onStart);
      };
      if(IsNameValid(animData.m_onPasue)) {
        animData.m_animProxy.RegisterToCallback(inkanimEventType.OnPause, owner, animData.m_onPasue);
      };
      if(IsNameValid(animData.m_onResume)) {
        animData.m_animProxy.RegisterToCallback(inkanimEventType.OnResume, owner, animData.m_onResume);
      };
      if(IsNameValid(animData.m_onStartLoop)) {
        animData.m_animProxy.RegisterToCallback(inkanimEventType.OnStartLoop, owner, animData.m_onStartLoop);
      };
      if(IsNameValid(animData.m_onEndLoop)) {
        animData.m_animProxy.RegisterToCallback(inkanimEventType.OnEndLoop, owner, animData.m_onEndLoop);
      };
    };
  }

  public final void ResolveCallback(ref<IScriptable> owner, ref<inkAnimProxy> animProxy, inkanimEventType eventType) {
    ref<inkAnimProxy> currentProxy;
    Int32 i;
    if(animProxy == null) {
      return ;
    };
    i = 0;
    while(i < Size(this.m_animations)) {
      if(this.m_animations[i].m_animProxy == animProxy) {
        if(eventType == inkanimEventType.OnFinish) {
          UnregisterAllCallbacks(this.m_animations[i]);
          this.m_animations[i].m_animProxy = null;
        } else {
          animProxy.UnregisterFromCallback(eventType, owner, GetAnimationCallbackName(this.m_animations[i], eventType));
        };
      };
      i += 1;
    };
  }

  private final CName GetAnimationCallbackName(SWidgetAnimationData animData, inkanimEventType eventType) {
    CName returnValue;
    if(eventType == inkanimEventType.OnStart) {
      animData.m_onStart;
    } else {
      if(eventType == inkanimEventType.OnFinish) {
        animData.m_onFinish;
      } else {
        if(eventType == inkanimEventType.OnPause) {
          animData.m_onPasue;
        } else {
          if(eventType == inkanimEventType.OnResume) {
            animData.m_onResume;
          } else {
            if(eventType == inkanimEventType.OnStartLoop) {
              animData.m_onStartLoop;
            } else {
              if(eventType == inkanimEventType.OnEndLoop) {
                animData.m_onEndLoop;
              };
            };
          };
        };
      };
    };
    return returnValue;
  }

  private final void CleanProxyData(SWidgetAnimationData animData) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_animations)) {
      if(this.m_animations[i].m_animProxy == animData.m_animProxy) {
        this.m_animations[i].m_animProxy = null;
      } else {
        i += 1;
      };
    };
  }
}
