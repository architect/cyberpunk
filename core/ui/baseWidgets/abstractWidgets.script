
public abstract class inkWidget extends IScriptable {

  public final native CName GetName()

  public final native void SetName(CName widgetName)

  public final native wref<inkLogicController> GetController()

  public final native wref<inkLogicController> GetControllerByType(CName controllerType)

  public final native wref<inkLogicController> GetControllerByBaseType(CName controllerType)

  public final native array<wref<inkLogicController>> GetControllers()

  public final native array<wref<inkLogicController>> GetControllersByType(CName controllerType)

  public final native Int32 GetNumControllers()

  public final native Int32 GetNumControllersOfType(CName controllerType)

  public final native ResRef GetStylePath()

  public final native void SetStyle(ResRef styleResPath)

  public final native CName GetState()

  public final native void SetState(CName state)

  public final static CName DefaultState() {
    return "Default";
  }

  public final native Bool IsVisible()

  public final native void SetVisible(Bool visible)

  public final native Bool IsInteractive()

  public final native void SetInteractive(Bool value)

  public final native void SetLayout(inkWidgetLayout layout)

  public final native Bool GetAffectsLayoutWhenHidden()

  public final native void SetAffectsLayoutWhenHidden(Bool affectsLayoutWhenHidden)

  public final native inkMargin GetMargin()

  public final native void SetMargin(inkMargin margin)

  public final void SetMargin(Float left, Float top, Float right, Float bottom) {
    SetMargin(new inkMargin(left,top,right,bottom));
  }

  public final void UpdateMargin(Float left, Float top, Float right, Float bottom) {
    inkMargin currentMargin;
    currentMargin = GetMargin();
    currentMargin.left += left;
    currentMargin.top += top;
    currentMargin.right += right;
    currentMargin.bottom += bottom;
    SetMargin(currentMargin);
  }

  public final native inkMargin GetPadding()

  public final native void SetPadding(inkMargin padding)

  public final void SetPadding(Float left, Float top, Float right, Float bottom) {
    SetPadding(new inkMargin(left,top,right,bottom));
  }

  public final native inkEHorizontalAlign GetHAlign()

  public final native void SetHAlign(inkEHorizontalAlign hAlign)

  public final native inkEVerticalAlign GetVAlign()

  public final native void SetVAlign(inkEVerticalAlign vAlign)

  public final native inkEAnchor GetAnchor()

  public final native void SetAnchor(inkEAnchor anchor)

  public final native Vector2 GetAnchorPoint()

  public final native void SetAnchorPoint(Vector2 anchorPoint)

  public final void SetAnchorPoint(Float x, Float y) {
    SetAnchorPoint(new Vector2(x,y));
  }

  public final native inkESizeRule GetSizeRule()

  public final native void SetSizeRule(inkESizeRule sizeRule)

  public final native Float GetSizeCoefficient()

  public final native void SetSizeCoefficient(Float sizeCoefficient)

  public final native Bool GetFitToContent()

  public final native void SetFitToContent(Bool fitToContent)

  public final native Vector2 GetSize()

  public final native void SetSize(Vector2 size)

  public final void SetSize(Float width, Float height) {
    SetSize(new Vector2(width,height));
  }

  public final Float GetWidth() {
    Vector2 size;
    size = GetSize();
    return size.X;
  }

  public final Float GetHeight() {
    Vector2 size;
    size = GetSize();
    return size.Y;
  }

  public final void SetWidth(Float width) {
    SetSize(width, GetHeight());
  }

  public final void SetHeight(Float height) {
    SetSize(GetWidth(), height);
  }

  public final native Vector2 GetDesiredSize()

  public final Float GetDesiredWidth() {
    Vector2 size;
    size = GetDesiredSize();
    return size.X;
  }

  public final Float GetDesiredHeight() {
    Vector2 size;
    size = GetDesiredSize();
    return size.Y;
  }

  public final native HDRColor GetTintColor()

  public final native void SetTintColor(HDRColor color)

  public final void SetTintColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
    SetTintColor(new Color(r,g,b,a));
  }

  public final void SetTintColor(Color color) {
    SetTintColor(ToHDRColorDirect(color));
  }

  public final native Float GetOpacity()

  public final native void SetOpacity(Float opacity)

  public final native Vector2 GetRenderTransformPivot()

  public final native void SetRenderTransformPivot(Vector2 pivot)

  public final void SetRenderTransformPivot(Float x, Float y) {
    SetRenderTransformPivot(new Vector2(x,y));
  }

  public final native void SetScale(Vector2 scale)

  public final native Vector2 GetScale()

  public final native void SetShear(Vector2 shear)

  public final native Vector2 GetShear()

  public final native void SetRotation(Float angleInDegrees)

  public final native Float GetRotation()

  public final native void SetTranslation(Vector2 translationVector)

  public final native Vector2 GetTranslation()

  public final native void ChangeTranslation(Vector2 translationVector)

  public final void SetTranslation(Float x, Float y) {
    SetTranslation(new Vector2(x,y));
  }

  public final native ref<inkAnimProxy> PlayAnimation(ref<inkAnimDef> animationDefinition)

  public final native ref<inkAnimProxy> PlayAnimationWithOptions(ref<inkAnimDef> animationDefinition, inkAnimOptions playbackOptions)

  public final native void StopAllAnimations()

  public final native void CallCustomCallback(CName eventName)

  public final native void RegisterToCallback(CName eventName, ref<IScriptable> object, CName functionName)

  public final native void UnregisterFromCallback(CName eventName, ref<IScriptable> object, CName functionName)

  public final native void SetEffectEnabled(inkEffectType effectType, CName effectName, Bool enabled)

  public final native Bool GetEffectEnabled(inkEffectType effectType, CName effectName)

  public final native Float GetEffectParamValue(inkEffectType effectType, CName effectName, CName paramName)

  public final native Float SetEffectParamValue(inkEffectType effectType, CName effectName, CName paramName, Float paramValue)

  public final native Bool HasUserDataObject(CName userDataTypeName)

  public final native Uint32 GetUserDataObjectCount(CName userDataTypeName)

  public final native ref<inkUserData> GetUserData(CName userDataTypeName)

  public final native array<ref<inkUserData>> GetUserDataArray(CName userDataTypeName)

  public final native void GatherUserData(CName userDataTypeName, array<ref<inkUserData>> userDataCollection)

  public final native Bool BindProperty(CName propertyName, CName stylePath)

  public final native Bool UnbindProperty(CName propertyName)

  public final native void Reparent(wref<inkCompoundWidget> newParent, Int32 index?)
}

public abstract class inkCompoundWidget extends inkWidget {

  public final native Int32 GetNumChildren()

  public final native wref<inkWidget> AddChild(CName widgetTypeName)

  public final native void AddChildWidget(wref<inkWidget> widget)

  public final native wref<inkWidget> GetWidgetByPath(inkWidgetPath path)

  public final native wref<inkWidget> GetWidgetByIndex(Int32 index)

  public final wref<inkWidget> GetWidget(inkWidgetPath path) {
    return GetWidgetByPath(path);
  }

  public final wref<inkWidget> GetWidget(Int32 index) {
    return GetWidgetByIndex(index);
  }

  public final native wref<inkWidget> GetWidgetByPathName(CName widgetNamePath)

  public final wref<inkWidget> GetWidget(CName path) {
    return GetWidgetByPathName(path);
  }

  public final native void RemoveChild(wref<inkWidget> childWidget)

  public final native void RemoveChildByIndex(Int32 index)

  public final native void RemoveChildByName(CName widgetName)

  public final native void RemoveAllChildren()

  public final native void ReorderChild(wref<inkWidget> childWidget, Int32 newIndex)

  public final native inkEChildOrder GetChildOrder()

  public final native void SetChildOrder(inkEChildOrder newOrder)

  public final native inkMargin GetChildMargin()

  public final native void SetChildMargin(inkMargin newMargin)

  public final native Vector2 GetChildPosition(wref<inkWidget> widget)

  public final native Vector2 GetChildSize(wref<inkWidget> widget)
}
