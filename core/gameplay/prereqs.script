
public class IScriptablePrereq extends IPrereq {

  protected const Bool IsOnRegisterSupported() {
    return true;
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context)

  protected void Initialize(TweakDBID record)

  protected const void OnApplied(ref<PrereqState> state, GameInstance game, ref<IScriptable> context)
}

public class DevelopmentCheckPrereq extends IScriptablePrereq {

  protected edit Float requiredLevel;

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    tweakID = recordID;
    Append(tweakID, ".requiredLevel");
    this.requiredLevel = GetFloat(tweakID);
  }
}

public class SkillCheckPrereqState extends PrereqState {

  public final const gamedataProficiencyType GetSkillToCheck() {
    return Cast(GetPrereq()).GetSkillToCheck();
  }

  public final void UpdateSkillCheckPrereqData(ref<GameObject> obj, Int32 newLevel) {
    Bool checkPassed;
    checkPassed = Cast(GetPrereq()).IsFulfilled(obj.GetGame(), GetContext());
    OnChanged(checkPassed);
  }
}

public class SkillCheckPrereq extends DevelopmentCheckPrereq {

  protected edit gamedataProficiencyType skillToCheck;

  public final const gamedataProficiencyType GetSkillToCheck() {
    return this.skillToCheck;
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<PlayerPuppet> player;
    ref<SkillCheckPrereqState> castedState;
    ref<ModifySkillCheckPrereq> request;
    castedState = Cast(state);
    player = Cast(context);
    if(ToBool(player)) {
      request = new ModifySkillCheckPrereq();
      request.Set(RefToWeakRef(player), true, castedState);
      GetScriptableSystemsContainer(game).Get("PlayerDevelopmentSystem").QueueRequest(request);
      return IsFulfilled(game, context);
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<PlayerPuppet> player;
    ref<SkillCheckPrereqState> castedState;
    ref<ModifySkillCheckPrereq> request;
    castedState = Cast(state);
    player = Cast(context);
    if(ToBool(player)) {
      request = new ModifySkillCheckPrereq();
      request.Set(RefToWeakRef(player), false, castedState);
      GetScriptableSystemsContainer(game).Get("PlayerDevelopmentSystem").QueueRequest(request);
    };
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    Int32 skillLevel;
    ref<PlayerPuppet> player;
    player = GetPlayer(game);
    skillLevel = Cast(GetScriptableSystemsContainer(game).Get("PlayerDevelopmentSystem")).GetProficiencyLevel(player, this.skillToCheck);
    return Cast(skillLevel) >= this.requiredLevel;
  }

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    CName type;
    Initialize(recordID);
    tweakID = recordID;
    Append(tweakID, ".skillToCheck");
    type = GetCName(tweakID);
    this.skillToCheck = ToEnum(Cast(EnumValueFromName("gamedataProficiencyType", type)));
  }
}

public class StatCheckPrereqState extends PrereqState {

  public final const gamedataStatType GetStatToCheck() {
    return Cast(GetPrereq()).GetStatToCheck();
  }

  public final void UpdateStatCheckPrereqData(ref<GameObject> obj, Float newValue) {
    Bool checkPassed;
    checkPassed = Cast(GetPrereq()).IsFulfilled(obj.GetGame(), GetContext());
    OnChanged(checkPassed);
  }
}

public class StatCheckPrereq extends DevelopmentCheckPrereq {

  protected edit gamedataStatType statToCheck;

  public final const gamedataStatType GetStatToCheck() {
    return this.statToCheck;
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<PlayerPuppet> player;
    ref<StatCheckPrereqState> castedState;
    ref<ModifyStatCheckPrereq> request;
    castedState = Cast(state);
    player = Cast(context);
    if(ToBool(player)) {
      request = new ModifyStatCheckPrereq();
      request.Set(RefToWeakRef(player), true, castedState);
      GetScriptableSystemsContainer(GetGameInstance()).Get("PlayerDevelopmentSystem").QueueRequest(request);
      return true;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<PlayerPuppet> player;
    ref<StatCheckPrereqState> castedState;
    ref<ModifyStatCheckPrereq> request;
    castedState = Cast(state);
    player = Cast(context);
    if(ToBool(player)) {
      request = new ModifyStatCheckPrereq();
      request.Set(RefToWeakRef(player), false, castedState);
      GetScriptableSystemsContainer(GetGameInstance()).Get("PlayerDevelopmentSystem").QueueRequest(request);
    };
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    Float statValue;
    ref<GameObject> player;
    StatsObjectID playerID;
    player = GetPlayer(game);
    statValue = GetStatsSystem(game).GetStatValue(Cast(player.GetEntityID()), this.statToCheck);
    return statValue >= this.requiredLevel;
  }

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    CName type;
    Initialize(recordID);
    tweakID = recordID;
    Append(tweakID, ".statToCheck");
    type = GetCName(tweakID);
    this.statToCheck = ToEnum(Cast(EnumValueFromName("gamedataStatType", type)));
  }
}

public class NPCRevealedPrereq extends IScriptablePrereq {

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<PuppetListener> listener;
    ref<NPCPuppet> npcOwner;
    ref<NPCRevealedPrereqState> castedState;
    castedState = Cast(state);
    if(ToBool(Cast(context))) {
      npcOwner = Cast(context);
    };
    castedState.m_listener = new PuppetListener();
    castedState.m_listener.RegisterOwner(castedState);
    AddListener(npcOwner, castedState.m_listener);
    return npcOwner.IsRevealed();
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<NPCRevealedPrereqState> castedState;
    castedState = Cast(state);
    RemoveListener(Cast(context), castedState.m_listener);
    castedState.m_listener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<NPCPuppet> npcOwner;
    if(ToBool(Cast(context))) {
      npcOwner = Cast(context);
    };
    return npcOwner.IsRevealed();
  }
}

public class GameObjectRevealedRedPrereq extends IScriptablePrereq {

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<GameObjectListener> listener;
    ref<GameObject> owner;
    ref<GameObjectRevealedRedPrereqState> castedState;
    castedState = Cast(state);
    if(ToBool(Cast(context))) {
      owner = Cast(context);
    };
    castedState.m_listener = new GameObjectListener();
    castedState.m_listener.RegisterOwner(castedState);
    AddListener(owner, castedState.m_listener);
    return owner.ShouldEnableOutlineRed();
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<GameObjectRevealedRedPrereqState> castedState;
    castedState = Cast(state);
    RemoveListener(Cast(context), castedState.m_listener);
    castedState.m_listener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> owner;
    if(ToBool(Cast(context))) {
      owner = Cast(context);
    };
    return owner.ShouldEnableOutlineRed();
  }
}

public class GameObjectRevealedGreenPrereq extends IScriptablePrereq {

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<GameObjectListener> listener;
    ref<GameObject> owner;
    ref<GameObjectRevealedGreenPrereqState> castedState;
    castedState = Cast(state);
    if(ToBool(Cast(context))) {
      owner = Cast(context);
    };
    castedState.m_listener = new GameObjectListener();
    castedState.m_listener.RegisterOwner(castedState);
    AddListener(owner, castedState.m_listener);
    return owner.ShouldEnableOutlineGreen();
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<GameObjectRevealedGreenPrereqState> castedState;
    castedState = Cast(state);
    RemoveListener(Cast(context), castedState.m_listener);
    castedState.m_listener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> owner;
    if(ToBool(Cast(context))) {
      owner = Cast(context);
    };
    return owner.ShouldEnableOutlineGreen();
  }
}

public class RevealAccessPointPrereq extends IScriptablePrereq {

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<GameObjectListener> listener;
    ref<GameObject> owner;
    ref<RevealAccessPointPrereqState> castedState;
    castedState = Cast(state);
    if(ToBool(Cast(context))) {
      owner = Cast(context);
    };
    castedState.m_listener = new GameObjectListener();
    castedState.m_listener.RegisterOwner(castedState);
    AddListener(owner, castedState.m_listener);
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<RevealAccessPointPrereqState> castedState;
    castedState = Cast(state);
    RemoveListener(Cast(context), castedState.m_listener);
    castedState.m_listener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<AccessPoint> accessPoint;
    accessPoint = Cast(context);
    if(ToBool(accessPoint)) {
      return accessPoint.IsRevealed();
    };
    return false;
  }
}

public class NPCDeadPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<NPCPuppet> npcOwner;
    ref<StatPoolsSystem> statPoolsSystem;
    Float health;
    StatsObjectID npcOwnerID;
    statPoolsSystem = GetStatPoolsSystem(game);
    if(ToBool(Cast(context))) {
      npcOwner = Cast(context);
      npcOwnerID = Cast(npcOwner.GetEntityID());
      health = statPoolsSystem.GetStatPoolValue(npcOwnerID, gamedataStatPoolType.Health);
      if(health < 0) {
        return true;
      };
    };
    return false;
  }
}

public class NPCIncapacitatedPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<NPCPuppet> npcOwner;
    gamedataNPCHighLevelState state;
    npcOwner = Cast(context);
    if(ToBool(npcOwner)) {
      state = npcOwner.GetHighLevelStateFromBlackboard();
      if(state == gamedataNPCHighLevelState.Unconscious || state == gamedataNPCHighLevelState.Dead || IsDefeated(npcOwner) || IsNanoWireHacked(npcOwner)) {
        return true;
      };
    };
    return false;
  }
}

public class NPCGrappledByPlayerPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<NPCPuppet> npcOwner;
    MountingInfo mountingInfo;
    CName mountingSlotName;
    Bool isNPCMounted;
    npcOwner = Cast(context);
    mountingInfo = GetMountingFacility(npcOwner.GetGame()).GetMountingInfoSingleWithObjects(npcOwner);
    isNPCMounted = IsDefined(mountingInfo.childId);
    mountingSlotName = mountingInfo.slotId.id;
    if(ToBool(Cast(context)) && isNPCMounted && mountingSlotName == "grapple") {
      return true;
    };
    return false;
  }
}

public class SinglePlayerPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    return !GetRuntimeInfo(game).IsMultiplayer();
  }
}

public class NPCNotMountedToVehiclePrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<NPCPuppet> npcOwner;
    Bool isNPCMounted;
    npcOwner = Cast(context);
    isNPCMounted = IsMountedToVehicle(game, npcOwner.GetEntityID());
    if(ToBool(Cast(context)) && !isNPCMounted) {
      return true;
    };
    return false;
  }
}

public class NPCIsHumanoidPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<NPCPuppet> npcOwner;
    gamedataNPCHighLevelState state;
    npcOwner = Cast(context);
    if(ToBool(npcOwner)) {
      return npcOwner.GetNPCType() == gamedataNPCType.Human;
    };
    return true;
  }
}

public class PuppetNotBossPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<ScriptedPuppet> puppet;
    Bool isBoss;
    puppet = Cast(context);
    if(ToBool(puppet)) {
      isBoss = puppet.IsBoss();
    } else {
      isBoss = false;
    };
    return !isBoss;
  }
}

public class NotReplacerPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      if(!playerControlledObject.IsReplacer()) {
        return true;
      };
    };
    return false;
  }
}

public class NotJohnnyReplacerPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      if(!playerControlledObject.IsJohnnyReplacer()) {
        return true;
      };
    };
    return false;
  }
}

public class NotVRReplacerPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      if(!playerControlledObject.IsVRReplacer()) {
        return true;
      };
    };
    return false;
  }
}

public class PlayerDeadPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<PlayerPuppet> playerOwner;
    playerOwner = Cast(context);
    if(ToBool(playerOwner)) {
      if(playerOwner.IsDead()) {
        return true;
      };
    };
    return false;
  }
}

public class PuppetIncapacitatedPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<gamePuppetBase> puppet;
    puppet = Cast(context);
    if(ToBool(puppet)) {
      return puppet.IsIncapacitated();
    };
    return false;
  }
}

public class PlayerNotCarryingPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> blackboard;
    Bool playerCarrying;
    ref<GameObject> playerControlledObject;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      blackboard = GetBlackboardSystem(game).GetLocalInstanced(playerControlledObject.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
      playerCarrying = blackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.Carrying);
    } else {
      playerCarrying = false;
    };
    return !playerCarrying;
  }
}

public class PlayerNotGrapplingPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    ref<IBlackboard> blackboard;
    Bool playerGrappling;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      blackboard = GetBlackboardSystem(game).GetLocalInstanced(playerControlledObject.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
      playerGrappling = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == ToInt(gamePSMTakedown.EnteringGrapple) || blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == ToInt(gamePSMTakedown.Grapple) || blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == ToInt(gamePSMTakedown.Takedown);
    } else {
      playerGrappling = false;
    };
    return !playerGrappling;
  }
}

public class DisableAllWorldInteractionsNotEnabledPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    Bool isDisablingRequested;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      isDisablingRequested = HasRestriction(RefToWeakRef(playerControlledObject), "NoWorldInteractions");
    } else {
      isDisablingRequested = false;
    };
    return !isDisablingRequested;
  }
}

public class DisableAllVehicleInteractionsNotEnabledPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    Bool isDisablingRequested;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    if(ToBool(playerControlledObject)) {
      isDisablingRequested = HasRestriction(RefToWeakRef(playerControlledObject), "VehicleNoInteraction");
    } else {
      isDisablingRequested = false;
    };
    return !isDisablingRequested;
  }
}

public class PlayerHasTakedownWeaponEquippedPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    ref<WeaponObject> weaponObj;
    ref<Item_Record> record;
    ItemID itemID;
    array<CName> tags;
    Int32 i;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    weaponObj = Cast(GetTransactionSystem(playerControlledObject.GetGame()).GetItemInSlot(playerControlledObject, "AttachmentSlots.WeaponRight"));
    itemID = weaponObj.GetItemID();
    if(ToBool(playerControlledObject) && IsValid(itemID)) {
      record = GetItemRecord(GetTDBID(itemID));
      tags = record.Tags();
      i = 0;
      while(i < Size(tags)) {
        if(tags[i] == "TakedownWeapon") {
          return true;
        };
        i += 1;
      };
    };
    return false;
  }
}

public class PlayerHasMantisBladesEquippedPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    ref<WeaponObject> weaponObj;
    String friendlyName;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    weaponObj = Cast(GetTransactionSystem(playerControlledObject.GetGame()).GetItemInSlot(playerControlledObject, "AttachmentSlots.WeaponRight"));
    if(ToBool(playerControlledObject) && ToBool(weaponObj)) {
      friendlyName = GetItemRecord(GetTDBID(weaponObj.GetItemID())).FriendlyName();
      if(friendlyName == "mantis_blade") {
        return true;
      };
    };
    return false;
  }
}

public class IsNpcMountedInSlotPrereqState extends PrereqState {

  public ref<gameScriptedPrereqPSChangeListenerWrapper> psListener;

  protected final void OnMountingStateChanged() {
    ref<IsNpcMountedInSlotPrereq> prereq;
    prereq = Cast(GetPrereq());
    OnChanged(prereq.IsFulfilled(GetGameInstance(), GetContext()));
  }
}

public class IsNpcMountedInSlotPrereq extends IScriptablePrereq {

  protected CName slotName;

  protected Bool isCheckInverted;

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    tweakID = recordID;
    Append(tweakID, ".slotname");
    this.slotName = GetCName(tweakID);
    tweakID = recordID;
    Append(tweakID, ".isCheckInverted");
    this.isCheckInverted = GetBool(tweakID);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsNpcMountedInSlotPrereqState> castedState;
    PersistentID persistentId;
    const ref<VehicleObject> vehicle;
    vehicle = Cast(context);
    castedState = Cast(state);
    if(ToBool(vehicle)) {
      persistentId = CreatePersistentID(vehicle.GetEntityID(), vehicle.GetPSClassName());
      castedState.psListener = CreateListener(game, persistentId, RefToWeakRef(castedState));
      return false;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsNpcMountedInSlotPrereqState> castedState;
    castedState = Cast(state);
    castedState.psListener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    const ref<PlayerPuppet> player;
    const ref<VehicleObject> vehicle;
    EAIAttitude attitude;
    MountingSlotId mountingSlotID;
    player = Cast(GetPlayerSystem(game).GetLocalPlayerMainGameObject());
    vehicle = Cast(context);
    if(ToBool(player) && ToBool(vehicle)) {
      if(vehicle.GetVehiclePS().IsSlotOccupiedByNPC(this.slotName)) {
        if(IsSlotOccupiedByActivePassenger(game, vehicle.GetEntityID(), this.slotName)) {
          return !this.isCheckInverted;
        };
      };
    };
    return this.isCheckInverted;
  }
}

public class CanPlayerHijackMountedNpcPrereqState extends PrereqState {

  public ref<gameScriptedPrereqMountingListenerWrapper> mountingListener;

  protected final void OnMountingStateChanged() {
    ref<CanPlayerHijackMountedNpcPrereq> prereq;
    prereq = Cast(GetPrereq());
    OnChanged(prereq.IsFulfilled(GetGameInstance(), GetContext()));
  }
}

public class CanPlayerHijackMountedNpcPrereq extends IScriptablePrereq {

  protected CName slotName;

  protected Bool isCheckInverted;

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    tweakID = recordID;
    Append(tweakID, ".slotname");
    this.slotName = GetCName(tweakID);
    tweakID = recordID;
    Append(tweakID, ".isCheckInverted");
    this.isCheckInverted = GetBool(tweakID);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<CanPlayerHijackMountedNpcPrereqState> castedState;
    const ref<VehicleObject> vehicle;
    vehicle = Cast(context);
    castedState = Cast(state);
    if(ToBool(vehicle)) {
      castedState.mountingListener = CreateVehicleListener(game, vehicle.GetEntityID(), RefToWeakRef(castedState));
      return false;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<CanPlayerHijackMountedNpcPrereqState> castedState;
    castedState = Cast(state);
    castedState.mountingListener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    const ref<PlayerPuppet> player;
    const ref<VehicleObject> vehicle;
    EAIAttitude attitude;
    MountingSlotId mountingSlotID;
    player = Cast(GetPlayerSystem(game).GetLocalPlayerMainGameObject());
    vehicle = Cast(context);
    if(ToBool(player) && ToBool(vehicle)) {
      if(IsSlotOccupiedByActivePassenger(game, vehicle.GetEntityID(), this.slotName)) {
        mountingSlotID.id = this.slotName;
        GetAttitudeOfPassenger(game, vehicle.GetEntityID(), mountingSlotID, attitude);
        if(attitude == EAIAttitude.AIA_Neutral) {
          return !this.isCheckInverted;
        };
      };
    };
    return this.isCheckInverted;
  }
}

public class IsNpcPlayingMountingAnimationPrereqState extends PrereqState {

  public ref<gameScriptedPrereqPSChangeListenerWrapper> psListener;

  protected final void OnPSStateChanged() {
    ref<IsNpcPlayingMountingAnimationPrereq> prereq;
    prereq = Cast(GetPrereq());
    OnChanged(prereq.IsFulfilled(GetGameInstance(), GetContext()));
  }
}

public class IsNpcPlayingMountingAnimationPrereq extends IScriptablePrereq {

  protected CName slotName;

  protected Bool isCheckInverted;

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    tweakID = recordID;
    Append(tweakID, ".slotname");
    this.slotName = GetCName(tweakID);
    tweakID = recordID;
    Append(tweakID, ".isCheckInverted");
    this.isCheckInverted = GetBool(tweakID);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsNpcPlayingMountingAnimationPrereqState> castedState;
    PersistentID persistentId;
    const ref<VehicleObject> vehicle;
    vehicle = Cast(context);
    castedState = Cast(state);
    if(ToBool(vehicle)) {
      persistentId = CreatePersistentID(vehicle.GetEntityID(), vehicle.GetPSClassName());
      castedState.psListener = CreateListener(game, persistentId, RefToWeakRef(castedState));
      return false;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsNpcPlayingMountingAnimationPrereqState> castedState;
    castedState = Cast(state);
    castedState.psListener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    const ref<PlayerPuppet> player;
    const ref<VehicleObject> vehicle;
    EAIAttitude attitude;
    MountingSlotId mountingSlotID;
    player = Cast(GetPlayerSystem(game).GetLocalPlayerMainGameObject());
    vehicle = Cast(context);
    if(ToBool(player) && ToBool(vehicle)) {
      if(IsSlotOccupied(game, vehicle.GetEntityID(), this.slotName)) {
        if(!vehicle.GetVehiclePS().IsSlotOccupiedByNPC(this.slotName)) {
          return !this.isCheckInverted;
        };
      };
    };
    return this.isCheckInverted;
  }
}

public class IsVehicleDoorLockedState extends PrereqState {

  public ref<gameScriptedPrereqPSChangeListenerWrapper> psListener;

  protected final void OnPSStateChanged() {
    ref<IsVehicleDoorLocked> prereq;
    prereq = Cast(GetPrereq());
    OnChanged(prereq.IsFulfilled(GetGameInstance(), GetContext()));
  }
}

public class IsVehicleDoorLocked extends IScriptablePrereq {

  protected CName slotName;

  protected Bool isCheckInverted;

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    tweakID = recordID;
    Append(tweakID, ".slotname");
    this.slotName = GetCName(tweakID);
    tweakID = recordID;
    Append(tweakID, ".isCheckInverted");
    this.isCheckInverted = GetBool(tweakID);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsVehicleDoorLockedState> castedState;
    PersistentID persistentId;
    const ref<VehicleObject> vehicle;
    vehicle = Cast(context);
    castedState = Cast(state);
    if(ToBool(vehicle)) {
      persistentId = CreatePersistentID(vehicle.GetEntityID(), vehicle.GetPSClassName());
      castedState.psListener = CreateListener(game, persistentId, RefToWeakRef(castedState));
      return false;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsVehicleDoorLockedState> castedState;
    castedState = Cast(state);
    castedState.psListener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    const ref<PlayerPuppet> player;
    const ref<VehicleObject> vehicle;
    EVehicleDoor doorEnum;
    player = Cast(GetPlayerSystem(game).GetLocalPlayerMainGameObject());
    vehicle = Cast(context);
    if(ToBool(player) && ToBool(vehicle)) {
      vehicle.GetVehiclePS().GetVehicleDoorEnum(doorEnum, this.slotName);
      if(vehicle.GetVehiclePS().GetDoorInteractionState(doorEnum) == VehicleDoorInteractionState.Locked) {
        return !this.isCheckInverted;
      };
    };
    return this.isCheckInverted;
  }
}

public class IsVehicleDoorQuestLockedState extends PrereqState {

  public ref<gameScriptedPrereqPSChangeListenerWrapper> psListener;

  protected final void OnPSStateChanged() {
    ref<IsVehicleDoorQuestLocked> prereq;
    prereq = Cast(GetPrereq());
    OnChanged(prereq.IsFulfilled(GetGameInstance(), GetContext()));
  }
}

public class IsVehicleDoorQuestLocked extends IScriptablePrereq {

  protected CName slotName;

  protected Bool isCheckInverted;

  protected void Initialize(TweakDBID recordID) {
    TweakDBID tweakID;
    tweakID = recordID;
    Append(tweakID, ".slotname");
    this.slotName = GetCName(tweakID);
    tweakID = recordID;
    Append(tweakID, ".isCheckInverted");
    this.isCheckInverted = GetBool(tweakID);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsVehicleDoorQuestLockedState> castedState;
    PersistentID persistentId;
    const ref<VehicleObject> vehicle;
    vehicle = Cast(context);
    castedState = Cast(state);
    if(ToBool(vehicle)) {
      persistentId = CreatePersistentID(vehicle.GetEntityID(), vehicle.GetPSClassName());
      castedState.psListener = CreateListener(game, persistentId, RefToWeakRef(castedState));
      return false;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IsVehicleDoorQuestLockedState> castedState;
    castedState = Cast(state);
    castedState.psListener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    const ref<PlayerPuppet> player;
    const ref<VehicleObject> vehicle;
    EVehicleDoor doorEnum;
    player = Cast(GetPlayerSystem(game).GetLocalPlayerMainGameObject());
    vehicle = Cast(context);
    if(ToBool(player) && ToBool(vehicle)) {
      vehicle.GetVehiclePS().GetVehicleDoorEnum(doorEnum, this.slotName);
      if(vehicle.GetVehiclePS().GetDoorInteractionState(doorEnum) == VehicleDoorInteractionState.QuestLocked) {
        return !this.isCheckInverted;
      };
    };
    return this.isCheckInverted;
  }
}

public class PlayerHasNanoWiresEquippedPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> playerControlledObject;
    ref<WeaponObject> weaponObj;
    String friendlyName;
    playerControlledObject = GetPlayerSystem(game).GetLocalPlayerControlledGameObject();
    weaponObj = Cast(GetTransactionSystem(playerControlledObject.GetGame()).GetItemInSlot(playerControlledObject, "AttachmentSlots.WeaponRight"));
    if(ToBool(playerControlledObject) && ToBool(weaponObj)) {
      friendlyName = GetItemRecord(GetTDBID(weaponObj.GetItemID())).FriendlyName();
      if(friendlyName == "mono_wires") {
        return true;
      };
    };
    return false;
  }
}

public class IsMultiplayerGamePrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    return IsMultiplayer();
  }
}

public class PlayerHasCPOMissionDataPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<PlayerPuppet> playerPuppet;
    playerPuppet = Cast(context);
    if(ToBool(playerPuppet)) {
      return playerPuppet.HasCPOMissionData();
    };
    return false;
  }
}

public class SelectedForMultiplayerChoiceDialog extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<Entity> entity;
    String factName;
    if(!GetRuntimeInfo(game).IsMultiplayer()) {
      return true;
    };
    entity = Cast(context);
    factName = GetSceneSystem(game).GetPeerIdDialogChoiceFactName();
    return GetQuestsSystem(game).GetFactStr(factName) == Cast(entity.GetControllingPeerID());
  }
}

public class PlayerCanTakeCPOMissionDataPrereq extends InteractionScriptedCondition {

  public const Bool Test(wref<GameObject> activatorObject, wref<GameObject> hotSpotObject, wref<HotSpotLayerDefinition> hotSpotLayer) {
    const ref<PlayerPuppet> currentDataOwner;
    const ref<PlayerPuppet> receivingPlayer;
    currentDataOwner = Cast(WeakRefToRef(hotSpotObject));
    receivingPlayer = Cast(WeakRefToRef(activatorObject));
    if(ToBool(currentDataOwner) && ToBool(receivingPlayer)) {
      if(!currentDataOwner.m_CPOMissionDataState.m_ownerDecidesOnTransfer && currentDataOwner.HasCPOMissionData() && !receivingPlayer.HasCPOMissionData()) {
        return true;
      };
    };
    return false;
  }
}

public class PlayerCanGiveCPOMissionDataPrereq extends InteractionScriptedCondition {

  public const Bool Test(wref<GameObject> activatorObject, wref<GameObject> hotSpotObject, wref<HotSpotLayerDefinition> hotSpotLayer) {
    const ref<PlayerPuppet> currentDataOwner;
    const ref<PlayerPuppet> receivingPlayer;
    currentDataOwner = Cast(WeakRefToRef(activatorObject));
    receivingPlayer = Cast(WeakRefToRef(hotSpotObject));
    if(ToBool(currentDataOwner) && ToBool(receivingPlayer)) {
      if(currentDataOwner.m_CPOMissionDataState.m_ownerDecidesOnTransfer && currentDataOwner.HasCPOMissionData() && !receivingPlayer.HasCPOMissionData()) {
        return true;
      };
    };
    return false;
  }
}

public class AccessPointHasCPOMissionDataPrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<CPOMissionDataAccessPoint> device;
    device = Cast(context);
    if(ToBool(device)) {
      return device.HasDataToDownload();
    };
    return false;
  }
}

public class AccessPointIsBlocked extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<CPOMissionDevice> device;
    device = Cast(context);
    if(ToBool(device)) {
      return device.IsBlocked();
    };
    return false;
  }
}

public class IsScannerTarget extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    wref<GameObject> object;
    ref<IBlackboard> blackBoard;
    EntityID entityID;
    object = RefToWeakRef(Cast(context));
    if(ToBool(object)) {
      blackBoard = GetBlackboardSystem(game).Get(GetAllBlackboardDefs().UI_Scanner);
      entityID = blackBoard.GetEntityID(GetAllBlackboardDefs().UI_Scanner.ScannedObject);
      return entityID == WeakRefToRef(object).GetEntityID();
    };
    return false;
  }
}

public class AccessPointCompatibleWithUser extends InteractionScriptedCondition {

  public final const Bool Test(wref<GameObject> activatorObject, wref<GameObject> hotSpotObject, wref<HotSpotLayerDefinition> hotSpotLayer) {
    const ref<CPOMissionDataAccessPoint> device;
    const ref<PlayerPuppet> playerPuppet;
    device = Cast(WeakRefToRef(hotSpotObject));
    playerPuppet = Cast(WeakRefToRef(activatorObject));
    if(ToBool(device) && ToBool(playerPuppet)) {
      if(device.GetCompatibleDeviceName() != "" || playerPuppet.GetCompatibleCPOMissionDeviceName() != "") {
        return device.GetCompatibleDeviceName() == playerPuppet.GetCompatibleCPOMissionDeviceName();
      };
      return true;
    };
    return false;
  }
}

public class PlayerControlsDevicePrereq extends IScriptablePrereq {

  [Default(PlayerControlsDevicePrereq, true))]
  private Bool m_inverse;

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    if(this.m_inverse) {
      return !Cast(GetScriptableSystemsContainer(game).Get("TakeOverControlSystem")).IsDeviceControlled();
    };
    return Cast(GetScriptableSystemsContainer(game).Get("TakeOverControlSystem")).IsDeviceControlled();
  }

  protected void Initialize(TweakDBID record) {
    TweakDBID tweakID;
    tweakID = record;
    Append(tweakID, ".invert");
    this.m_inverse = GetBool(tweakID);
  }
}

public class PlayerNotInBraindancePrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    return !GetSceneSystem(game).GetScriptInterface().IsRewindableSectionActive();
  }
}

public static exec void EffectorOn(GameInstance gi, String record) {
  TweakDBID tdbid;
  tdbid = Create(record);
  if(IsValid(tdbid)) {
    GetEffectorSystem(gi).ApplyEffector(GetPlayer(gi).GetEntityID(), RefToWeakRef(GetPlayer(gi)), tdbid);
  };
}

public static exec void EffectorOnW(GameInstance gi, String record) {
  TweakDBID tdbid;
  ref<PlayerPuppet> player;
  wref<WeaponObject> wpn;
  tdbid = Create(record);
  if(IsValid(tdbid)) {
    player = GetPlayer(gi);
    wpn = GetActiveWeapon(player);
    GetEffectorSystem(gi).ApplyEffector(WeakRefToRef(wpn).GetEntityID(), wpn, tdbid);
  };
}

public static exec void EffectorOff(GameInstance gi, String record) {
  GetEffectorSystem(gi).RemoveEffector(GetPlayer(gi).GetEntityID(), Create(record));
}

public class CPOMissionPlayerVoted extends InteractionScriptedCondition {

  public const Bool Test(wref<GameObject> activatorObject, wref<GameObject> hotSpotObject, wref<HotSpotLayerDefinition> hotSpotLayer) {
    const ref<CPOVotingDevice> device;
    const ref<PlayerPuppet> playerPuppet;
    device = Cast(WeakRefToRef(hotSpotObject));
    playerPuppet = Cast(WeakRefToRef(activatorObject));
    if(ToBool(device) && ToBool(playerPuppet)) {
      if(device.GetCompatibleDeviceName() != "") {
        return playerPuppet.GetCPOMissionVoted(device.GetCompatibleDeviceName());
      };
    };
    return false;
  }
}

public class CPOMissionPlayerNotVoted extends CPOMissionPlayerVoted {

  public const Bool Test(wref<GameObject> activatorObject, wref<GameObject> hotSpotObject, wref<HotSpotLayerDefinition> hotSpotLayer) {
    return !Test(activatorObject, hotSpotObject, hotSpotLayer);
  }
}

public class PuppetMortalPrereq extends IScriptablePrereq {

  public Bool m_invert;

  protected void Initialize(TweakDBID record) {
    TweakDBID tweakID;
    tweakID = record;
    Append(tweakID, ".invert");
    this.m_invert = GetBool(tweakID);
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<ScriptedPuppet> puppet;
    puppet = Cast(context);
    if(!ToBool(puppet)) {
      return this.m_invert ? true : false;
    };
    if(GetGodModeSystem(puppet.GetGame()).HasGodMode(puppet.GetEntityID(), gameGodModeType.Immortal)) {
      return this.m_invert ? true : false;
    };
    if(GetGodModeSystem(puppet.GetGame()).HasGodMode(puppet.GetEntityID(), gameGodModeType.Invulnerable)) {
      return this.m_invert ? true : false;
    };
    return this.m_invert ? false : true;
  }
}
