
public static exec void SpawnTestEffect(GameInstance gameInstance) {
  ref<EffectInstance> effect;
  Vector4 pos;
  pos.X = 0;
  pos.Y = 0;
  pos.Z = 0;
  effect = GetGameEffectSystem(gameInstance).CreateEffectStatic("test_effect", "explosion", GetPlayer(gameInstance));
  SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, pos);
  effect.Run();
}

public abstract class EffectDataHelper extends IScriptable {

  public final static void FillMeleeEffectData(EffectData effectData, Vector4 colliderBoxSize, Float duration, Vector4 position, Quaternion rotation, Vector4 direction, Float range) {
    SetVector(effectData, GetAllBlackboardDefs().EffectSharedData.box, colliderBoxSize);
    SetFloat(effectData, GetAllBlackboardDefs().EffectSharedData.duration, duration);
    SetVector(effectData, GetAllBlackboardDefs().EffectSharedData.position, position);
    SetQuat(effectData, GetAllBlackboardDefs().EffectSharedData.rotation, rotation);
    SetVector(effectData, GetAllBlackboardDefs().EffectSharedData.forward, direction);
    SetFloat(effectData, GetAllBlackboardDefs().EffectSharedData.range, range);
    SetFloat(effectData, GetAllBlackboardDefs().EffectSharedData.radius, range);
  }
}
