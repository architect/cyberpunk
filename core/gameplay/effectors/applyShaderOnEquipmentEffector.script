
public class ApplyShaderOnEquipmentEffector extends Effector {

  private array<wref<ItemObject>> m_items;

  private array<ref<EffectInstance>> m_effects;

  private String m_overrideMaterialName;

  private CName m_overrideMaterialTag;

  private ref<EffectInstance> m_effectInstance;

  private wref<GameObject> m_owner;

  private ref<EffectInstance> m_ownerEffect;

  protected void Initialize(TweakDBID record, GameInstance game, TweakDBID parentRecord) {
    this.m_overrideMaterialName = GetString(record + ".overrideMaterialName", "");
    this.m_overrideMaterialTag = GetCName(record + ".overrideMaterialTag", "");
  }

  protected void ActionOn(ref<GameObject> owner) {
    Int32 i;
    ref<TransactionSystem> ts;
    array<TweakDBID> slots;
    wref<ItemObject> item;
    this.m_owner = RefToWeakRef(owner);
    ts = GetTransactionSystem(owner.GetGame());
    slots = GetAttachmentSlotsForEquipment();
    i = 0;
    while(i < Size(slots)) {
      item = RefToWeakRef(ts.GetItemInSlot(owner, slots[i]));
      if(ToBool(item)) {
        this.m_effectInstance = GetGameEffectSystem(WeakRefToRef(this.m_owner).GetGame()).CreateEffectStatic(Cast(this.m_overrideMaterialName), this.m_overrideMaterialTag, WeakRefToRef(this.m_owner));
        if(ToBool(this.m_effectInstance) && IsStringValid(this.m_overrideMaterialName)) {
          SetBool(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.enable, true);
          SetEntity(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.entity, item);
          this.m_effectInstance.Run();
          Push(this.m_effects, this.m_effectInstance);
          Push(this.m_items, item);
        };
      };
      i += 1;
    };
    this.m_effectInstance = GetGameEffectSystem(WeakRefToRef(this.m_owner).GetGame()).CreateEffectStatic(Cast(this.m_overrideMaterialName), this.m_overrideMaterialTag, WeakRefToRef(this.m_owner));
    if(ToBool(this.m_effectInstance) && IsStringValid(this.m_overrideMaterialName)) {
      SetBool(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.enable, true);
      SetEntity(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.entity, this.m_owner);
      this.m_effectInstance.Run();
      Push(this.m_effects, this.m_effectInstance);
    };
  }

  protected void Uninitialize(GameInstance game) {
    Int32 i;
    ref<TransactionSystem> ts;
    array<TweakDBID> slots;
    wref<ItemObject> item;
    ts = GetTransactionSystem(WeakRefToRef(this.m_owner).GetGame());
    slots = GetAttachmentSlotsForEquipment();
    i = 0;
    while(i < Size(slots)) {
      item = RefToWeakRef(ts.GetItemInSlot(WeakRefToRef(this.m_owner), slots[i]));
      if(ToBool(item)) {
        this.m_effectInstance = GetGameEffectSystem(WeakRefToRef(this.m_owner).GetGame()).CreateEffectStatic(Cast(this.m_overrideMaterialName), this.m_overrideMaterialTag, WeakRefToRef(this.m_owner));
        if(ToBool(this.m_effectInstance) && IsStringValid(this.m_overrideMaterialName)) {
          SetBool(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.enable, false);
          SetEntity(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.entity, item);
          this.m_effectInstance.Run();
        };
      };
      i += 1;
    };
    this.m_effectInstance = GetGameEffectSystem(WeakRefToRef(this.m_owner).GetGame()).CreateEffectStatic(Cast(this.m_overrideMaterialName), this.m_overrideMaterialTag, WeakRefToRef(this.m_owner));
    if(ToBool(this.m_effectInstance) && IsStringValid(this.m_overrideMaterialName)) {
      SetBool(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.enable, false);
      SetEntity(this.m_effectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.entity, this.m_owner);
      this.m_effectInstance.Run();
    };
  }

  private final const array<TweakDBID> GetAttachmentSlotsForEquipment() {
    array<TweakDBID> slots;
    Push(slots, "AttachmentSlots.Underwear");
    Push(slots, "AttachmentSlots.Chest");
    Push(slots, "AttachmentSlots.Torso");
    Push(slots, "AttachmentSlots.Head");
    Push(slots, "AttachmentSlots.Face");
    Push(slots, "AttachmentSlots.Legs");
    Push(slots, "AttachmentSlots.Feet");
    Push(slots, "AttachmentSlots.RightArm");
    return slots;
  }
}
