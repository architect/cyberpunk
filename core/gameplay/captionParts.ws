
public class InteractionChoiceCaptionScriptPart extends InteractionChoiceCaptionPart {

  protected const gamedataChoiceCaptionPartType GetPartType() {
    return gamedataChoiceCaptionPartType.Invalid;
  }
}

public static String GetCaptionTagsFromArray(array<ref<InteractionChoiceCaptionPart>> argList) {
  Int32 i;
  String toRet;
  String preLoc;
  String postLoc;
  gamedataChoiceCaptionPartType currType;
  toRet = "";
  i = 0;
  while(i < Size(argList)) {
    currType = argList[i].GetType();
    if(currType == gamedataChoiceCaptionPartType.Tag) {
      preLoc = Cast(argList[i]).content;
      postLoc = GetLocalizedText(preLoc);
      toRet = postLoc + " " + toRet;
    };
    i = i + 1;
  };
  return toRet;
}
