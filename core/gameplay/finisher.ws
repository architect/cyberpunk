
public static void PlayFinisher(GameInstance gameInstance)

public static void PlayFinisherSingle(GameInstance gameInstance) {
  ref<EffectInstance> gameEffectInstance;
  ref<PlayerPuppet> player;
  player = GetPlayer(gameInstance);
  gameEffectInstance = GetGameEffectSystem(gameInstance).CreateEffectStatic("playFinisher", "playFinisherSingle", player);
  SetVector(gameEffectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, player.GetWorldPosition() + new Vector4(0,0,1,1));
  SetVector(gameEffectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, player.GetWorldForward());
  SetFloat(gameEffectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, 20);
  SetFloat(gameEffectInstance.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, 20);
  gameEffectInstance.Run();
}
