
public class TemporalPrereqDelayCallback extends DelayCallback {

  protected wref<TemporalPrereqState> m_state;

  public void Call() {
    WeakRefToRef(this.m_state).CallbackRecall();
  }

  public final void RegisterState(ref<PrereqState> state) {
    this.m_state = RefToWeakRef(Cast(state));
  }
}

public class TemporalPrereqState extends PrereqState {

  public ref<DelaySystem> m_delaySystem;

  public ref<TemporalPrereqDelayCallback> m_callback;

  public Float m_lapsedTime;

  public DelayID m_delayID;

  public void RegisterDealyCallback(Float delayTime) {
    this.m_delayID = this.m_delaySystem.DelayCallback(this.m_callback, delayTime);
  }

  public void CallbackRecall() {
    Bool newState;
    const ref<TemporalPrereq> prereq;
    prereq = Cast(GetPrereq());
    newState = this.m_lapsedTime >= prereq.m_totalDuration;
    OnChanged(newState);
    this.m_lapsedTime += prereq.m_totalDuration;
    if(!IsFulfilled()) {
      RegisterDealyCallback(prereq.m_totalDuration);
    };
  }
}

public class TemporalPrereq extends IScriptablePrereq {

  public Float m_totalDuration;

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<TemporalPrereqState> castedState;
    castedState = Cast(state);
    castedState.m_delaySystem = GetDelaySystem(game);
    castedState.m_callback = new TemporalPrereqDelayCallback();
    castedState.m_callback.RegisterState(castedState);
    castedState.m_lapsedTime = 0;
    castedState.RegisterDealyCallback(0);
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<TemporalPrereqState> castedState;
    castedState = Cast(state);
    GetDelaySystem(game).CancelCallback(castedState.m_delayID);
    castedState.m_callback = null;
  }

  protected void Initialize(TweakDBID recordID) {
    Float randRange;
    this.m_totalDuration = GetFloat(recordID + ".duration", 0);
    randRange = GetFloat(recordID + ".randRange", 0);
    if(randRange > 0) {
      this.m_totalDuration = RandRangeF(this.m_totalDuration - randRange, this.m_totalDuration + randRange);
    };
  }
}

public class PlayerVehicleStatePrereq extends IScriptablePrereq {

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> bboard;
    Bool playerVehState;
    ref<PlayerPuppet> playerPuppet;
    playerPuppet = Cast(GetPlayerSystem(game).GetLocalPlayerMainGameObject());
    if(ToBool(playerPuppet)) {
      bboard = GetBlackboardSystem(game).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
      playerVehState = bboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle) == 0;
    } else {
      playerVehState = true;
    };
    return playerVehState;
  }
}
