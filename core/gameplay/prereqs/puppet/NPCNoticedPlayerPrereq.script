
public class EntityNoticedPlayerPrereqState extends PrereqState {

  public wref<GameObject> m_owner;

  public Uint32 m_listenerInt;

  protected cb Bool OnStateUpdate(Uint32 value) {
    Bool checkPassed;
    const ref<EntityNoticedPlayerPrereq> prereq;
    prereq = Cast(GetPrereq());
    checkPassed = prereq.Evaluate(WeakRefToRef(this.m_owner), value);
    OnChanged(checkPassed);
  }
}

public class EntityNoticedPlayerPrereq extends IScriptablePrereq {

  private Bool m_isPlayerNoticed;

  [Default(EntityNoticedPlayerPrereq, 1))]
  private Uint32 m_valueToListen;

  public final const Bool Evaluate(ref<GameObject> owner, Uint32 value) {
    if(this.m_isPlayerNoticed) {
      if(value >= this.m_valueToListen) {
        return true;
      };
    } else {
      if(value != this.m_valueToListen) {
        return true;
      };
    };
    return false;
  }

  protected void Initialize(TweakDBID recordID) {
    this.m_isPlayerNoticed = GetBool(recordID + ".isPlayerNoticed", false);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> bb;
    ref<EntityNoticedPlayerPrereqState> castedState;
    wref<PlayerPuppet> player;
    castedState = Cast(state);
    castedState.m_owner = RefToWeakRef(Cast(context));
    player = RefToWeakRef(Cast(context));
    if(ToBool(player)) {
      bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      castedState.m_listenerInt = bb.RegisterListenerUint(GetAllBlackboardDefs().PlayerPerkData.EntityNoticedPlayer, castedState, "OnStateUpdate");
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> bb;
    ref<EntityNoticedPlayerPrereqState> castedState;
    wref<PlayerPuppet> player;
    player = RefToWeakRef(Cast(context));
    if(ToBool(player)) {
      bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      castedState = Cast(state);
      bb.UnregisterListenerUint(GetAllBlackboardDefs().PlayerPerkData.EntityNoticedPlayer, castedState.m_listenerInt);
    };
  }

  protected const void OnApplied(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    wref<PlayerPuppet> player;
    ref<EntityNoticedPlayerPrereqState> castedState;
    ref<IBlackboard> bb;
    player = RefToWeakRef(Cast(context));
    if(ToBool(player)) {
      bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      castedState = Cast(state);
      castedState.OnChanged(IsFulfilled(WeakRefToRef(player).GetGame(), context));
    };
  }

  protected const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> bb;
    wref<PlayerPuppet> player;
    Bool checkPassed;
    player = RefToWeakRef(Cast(context));
    bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
    checkPassed = Evaluate(WeakRefToRef(player), bb.GetUint(GetAllBlackboardDefs().PlayerPerkData.EntityNoticedPlayer));
    return checkPassed;
  }
}
