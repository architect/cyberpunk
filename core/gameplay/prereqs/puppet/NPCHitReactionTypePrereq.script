
public class NPCHitReactionTypePrereq extends IScriptablePrereq {

  public animHitReactionType m_hitReactionType;

  public Bool m_invert;

  protected void Initialize(TweakDBID recordID) {
    String str;
    str = GetString(recordID + ".hitReactionType", "");
    this.m_hitReactionType = ToEnum(Cast(EnumValueFromString("animHitReactionType", str)));
    this.m_invert = GetBool(recordID + ".invert", false);
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<PuppetListener> listener;
    ref<ScriptedPuppet> puppet;
    ref<NPCHitReactionTypePrereqState> castedState;
    puppet = Cast(context);
    if(ToBool(puppet)) {
      castedState = Cast(state);
      castedState.m_listener = new PuppetListener();
      castedState.m_listener.RegisterOwner(castedState);
      AddListener(puppet, castedState.m_listener);
      return false;
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<ScriptedPuppet> puppet;
    ref<NPCHitReactionTypePrereqState> castedState;
    puppet = Cast(context);
    castedState = Cast(state);
    if(ToBool(puppet) && ToBool(castedState.m_listener)) {
      RemoveListener(puppet, castedState.m_listener);
    };
    castedState.m_listener = null;
  }

  public const Bool IsFulfilled(GameInstance game, ref<IScriptable> context) {
    ref<GameObject> owner;
    ref<ScriptedPuppet> targetPuppet;
    ref<HitReactionComponent> hitReactionComponent;
    owner = Cast(context);
    targetPuppet = Cast(owner);
    hitReactionComponent = targetPuppet.GetHitReactionComponent();
    if(!ToBool(targetPuppet)) {
      return false;
    };
    return EvaluateCondition(hitReactionComponent.GetHitReactionData().hitType);
  }

  public final const Bool EvaluateCondition(Int32 hitType) {
    if(hitType != ToInt(this.m_hitReactionType)) {
      return this.m_invert ? true : false;
    };
    return this.m_invert ? false : true;
  }
}
