
public class DismembermentTriggeredPrereqState extends PrereqState {

  public wref<GameObject> m_owner;

  public Uint32 m_listenerInt;

  protected cb Bool OnStateUpdate(Uint32 value) {
    Bool checkPassed;
    const ref<DismembermentTriggeredPrereq> prereq;
    prereq = Cast(GetPrereq());
    checkPassed = prereq.Evaluate(WeakRefToRef(this.m_owner), value);
    if(checkPassed) {
      OnChangedRepeated(false);
    };
  }
}

public class DismembermentTriggeredPrereq extends IScriptablePrereq {

  [Default(DismembermentTriggeredPrereq, 0))]
  public Uint32 m_currValue;

  public final const Bool Evaluate(ref<GameObject> owner, Uint32 value) {
    Bool checkPassed;
    checkPassed = value != this.m_currValue;
    return checkPassed;
  }

  protected const Bool OnRegister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> bb;
    ref<DismembermentTriggeredPrereqState> castedState;
    wref<PlayerPuppet> player;
    castedState = Cast(state);
    castedState.m_owner = RefToWeakRef(Cast(context));
    player = RefToWeakRef(Cast(context));
    if(ToBool(player)) {
      bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      castedState.m_listenerInt = bb.RegisterListenerUint(GetAllBlackboardDefs().PlayerPerkData.DismembermentInstigated, castedState, "OnStateUpdate");
    };
    return false;
  }

  protected const void OnUnregister(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    ref<IBlackboard> bb;
    ref<DismembermentTriggeredPrereqState> castedState;
    wref<PlayerPuppet> player;
    player = RefToWeakRef(Cast(context));
    if(ToBool(player)) {
      bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      castedState = Cast(state);
      bb.UnregisterListenerUint(GetAllBlackboardDefs().PlayerPerkData.DismembermentInstigated, castedState.m_listenerInt);
    };
  }

  protected const void OnApplied(ref<PrereqState> state, GameInstance game, ref<IScriptable> context) {
    wref<PlayerPuppet> player;
    ref<DismembermentTriggeredPrereqState> castedState;
    ref<IBlackboard> bb;
    player = RefToWeakRef(Cast(context));
    if(ToBool(player)) {
      bb = GetBlackboardSystem(WeakRefToRef(player).GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      castedState = Cast(state);
      castedState.OnChanged(Evaluate(WeakRefToRef(player), bb.GetUint(GetAllBlackboardDefs().PlayerPerkData.DismembermentInstigated)));
    };
  }
}
