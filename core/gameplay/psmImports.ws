
public class StateGameScriptInterface extends StateScriptInterface {

  public final native const Variant GetStateVectorParameter(physicsStateValue stateVectorParameter)

  public final native Bool SetStateVectorParameter(physicsStateValue stateVectorParameter, Variant value)

  public final native const Bool Overlap(Vector4 primitiveDimension, Vector4 position, EulerAngles rotation, CName collisionGroup?, out TraceResult result)

  public final native const Bool OverlapWithCollisionFilter(Vector4 primitiveDimension, Vector4 position, EulerAngles rotation, QueryFilter collisionGroup?, out TraceResult result)

  public final native const array<TraceResult> OverlapMultiple(Vector4 primitiveDimension, Vector4 position, EulerAngles rotation, CName collisionGroup?)

  public final native const TraceResult RayCast(Vector4 start, Vector4 end, CName collisionGroup?)

  public final native const TraceResult RayCastWithCollisionFilter(Vector4 start, Vector4 end, QueryFilter collisionGroup?)

  public final native const array<TraceResult> RayCastMultiple(Vector4 start, Vector4 end, CName collisionGroup?)

  public final native const Bool Sweep(Vector4 primitiveDimension, Vector4 position, EulerAngles rotation, Vector4 direction, Float distance, CName collisionGroup?, Bool assumeInitialPositionClear?, out TraceResult result)

  public final native const Bool SweepWithCollisionFilter(Vector4 primitiveDimension, Vector4 position, EulerAngles rotation, Vector4 direction, Float distance, QueryFilter collisionGroup?, Bool assumeInitialPositionClear?, out TraceResult result)

  public final native const array<TraceResult> SweepMultiple(Vector4 primitiveDimension, Vector4 position, EulerAngles rotation, Vector4 direction, Float distance, CName collisionGroup?)

  public final native const array<ControllerHit> GetCollisionReport()

  public final native const Bool IsOnGround()

  public final native const Bool IsOnMovingPlatform()

  public final native const Bool CanCapsuleFit(Float capsuleHeight, Float capsuleRadius)

  public final native const SecureFootingResult HasSecureFooting()

  public final native const Float GetActionPrevStateTime(CName actionName)

  public final native const Float GetActionStateTime(CName actionName)

  public final native const Float GetActionValue(CName actionName)

  public final native const Bool IsActionJustPressed(CName actionName)

  public final native const Bool IsActionJustReleased(CName actionName)

  public final native const Bool IsActionJustHeld(CName actionName)

  public final native const Bool IsAxisChangeAction(CName actionName)

  public final native const Bool IsRelativeChangeAction(CName actionName)

  public final native const Uint32 GetActionPressCount(CName actionName)

  public final const Bool IsActionJustTapped(CName actionName) {
    if(!IsActionJustReleased(actionName)) {
      return false;
    };
    if(GetActionPrevStateTime(actionName) > 0.20000000298023224) {
      return false;
    };
    return true;
  }

  public final native Bool SetComponentVisibility(CName actionName, Bool visibility)

  public final native ref<GameObject> GetObjectFromComponent(ref<IPlacedComponent> targetingComponent)

  public final native const Vector4 TransformInvPointFromObject(Vector4 point, ref<GameObject> object?)

  public final native Bool ActivateCameraSetting(CName settingId)

  public final native Bool SetCameraTimeDilationCurve(CName curveName)

  public final native const Transform GetCameraWorldTransform()

  public final native Bool TEMP_WeaponStopFiring()

  public final native const Bool IsTriggerModeActive(gamedataTriggerMode triggerMode)

  public final native Bool SetAnimationParameterInt(CName key, Int32 value)

  public final native Bool SetAnimationParameterFloat(CName key, Float value)

  public final native Bool SetAnimationParameterBool(CName key, Bool value)

  public final native Bool SetAnimationParameterVector(CName key, Vector4 value)

  public final native Bool SetAnimationParameterQuaternion(CName key, Quaternion value)

  public final native Bool SetAnimationParameterFeature(CName key, ref<AnimFeature> value, ref<GameObject> owner?)

  public final native Bool PushAnimationEvent(CName eventName)

  public final native const Bool IsSceneAnimationActive()

  public final native const Bool IsMoveInputConsiderable()

  public final native const Float GetInputHeading()

  public final native const Float GetOwnerStateVectorParameterFloat(physicsStateValue parameterType)

  public final native const Vector4 GetOwnerStateVectorParameterVector(physicsStateValue parameterType)

  public final native const Vector4 GetOwnerMovingDirection()

  public final native const Vector4 GetOwnerForward()

  public final native const Transform GetOwnerTransform()

  public final native const Bool RayCastNotPlayer(Vector4 start, Vector4 end)

  public final native const Bool MeetsPrerequisites(TweakDBID prereqName)

  public final native const ItemID GetItemIdInSlot(TweakDBID slotName)

  public final native const Bool CanEquipItem(ref<StateContext> stateContext)

  public final native const Bool IsMountedToObject(ref<GameObject> object?)

  public final native const Bool IsDriverInVehicle(ref<GameObject> child?, ref<GameObject> parent?)

  public final native const Bool IsPassengerInVehicle(ref<GameObject> child?, ref<GameObject> parent?)

  public final native const MountingInfo GetMountingInfo(ref<GameObject> child)

  public final native const gameMountingSlotRole GetRoleForSlot(MountingSlotId slot, ref<GameObject> parent, CName occupantSlotComponentName?)

  public final native const Bool GetWaterLevel(Vector4 puppetPosition, Vector4 referencePosition, out Float waterLevel)

  public final native const Bool IsEntityInCombat(EntityID objectId?)

  public final native const Bool CanEnterInteraction(ref<StateContext> stateContext)

  public final native const void RequestWeaponEquipOnServer(TweakDBID slotName, ItemID itemId)
}

public class MountEventData extends IScriptable {

  public native CName slotName;

  public native EntityID mountParentEntityId;

  public native Bool isInstant;

  public native CName entryAnimName;

  public native Transform initialTransformLS;

  public native ref<MountEventOptions> mountEventOptions;

  public native Bool ignoreHLS;

  public final Bool IsTransitionForced() {
    if(this.slotName == "trunk_body") {
      return true;
    };
    return false;
  }
}
