
public static TargetSearchFilter TSF_NPC() {
  TargetSearchFilter tsf;
  tsf = TSF_And(TSF_All(TSFMV.Obj_Puppet || TSFMV.St_Alive), TSF_Not(TSFMV.Obj_Player));
  return tsf;
}

public static TargetSearchFilter TSF_EnemyNPC() {
  TargetSearchFilter tsf;
  tsf = TSF_And(TSF_All(TSFMV.Obj_Puppet || TSFMV.Att_Hostile || TSFMV.St_Alive), TSF_Not(TSFMV.Obj_Player));
  return tsf;
}

public static TargetSearchFilter TSF_NpcOrDevice() {
  TargetSearchFilter tsf;
  tsf = TSF_And(TSF_Any(TSFMV.Obj_Puppet || TSFMV.Obj_Device || TSFMV.Obj_Sensor), TSF_Not(TSFMV.Obj_Player), TSF_Not(TSFMV.Att_Friendly));
  return tsf;
}

public static TargetSearchFilter TSF_Quickhackable() {
  TargetSearchFilter tsf;
  tsf = TSF_And(TSF_All(TSFMV.St_QuickHackable), TSF_Not(TSFMV.Obj_Player), TSF_Not(TSFMV.Att_Friendly), TSF_Any(TSFMV.Sp_Aggressive || TSFMV.Obj_Device));
  return tsf;
}

public static TargetSearchQuery TSQ_ALL() {
  TargetSearchQuery tsq;
  return tsq;
}

public static TargetSearchQuery TSQ_NPC() {
  TargetSearchQuery tsq;
  tsq.searchFilter = TSF_NPC();
  return tsq;
}

public static TargetSearchQuery TSQ_EnemyNPC() {
  TargetSearchQuery tsq;
  tsq.searchFilter = TSF_EnemyNPC();
  return tsq;
}

public static TargetSearchQuery TSQ_NpcOrDevice() {
  TargetSearchQuery tsq;
  tsq.searchFilter = TSF_NpcOrDevice();
  return tsq;
}
