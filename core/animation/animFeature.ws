
public class AnimFeature_StatusEffect extends AnimFeature {

  public edit Int32 state;

  [Default(AnimFeature_StatusEffect, -1.f))]
  public edit Float duration;

  [Default(AnimFeature_StatusEffect, 1))]
  public edit Int32 variation;

  public edit Int32 direction;

  [Default(AnimFeature_StatusEffect, -1))]
  public edit Int32 impactDirection;

  public edit Bool knockdown;

  public edit Bool stunned;

  [Default(AnimFeature_StatusEffect, false))]
  public edit Bool playImpact;

  public final void Clear() {
    this.state = 0;
    this.impactDirection = -1;
    this.knockdown = false;
    this.stunned = false;
    this.playImpact = false;
  }
}
