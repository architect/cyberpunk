
public static ref<RagdollActivationRequestEvent> CreateRagdollActivationRequestEvent(entragdollActivationRequestType activationType, CName filterDataOverride, Bool applyPowerPose, Bool applyMomentum, CName debugSourceName) {
  ref<RagdollActivationRequestEvent> evt;
  evt = new RagdollActivationRequestEvent();
  evt.data.type = activationType;
  evt.data.applyPowerPose = applyPowerPose;
  evt.data.applyMomentum = applyMomentum;
  evt.data.filterDataOverride = filterDataOverride;
  evt.DebugSetSourceName(debugSourceName);
  return evt;
}

public static ref<RagdollActivationRequestEvent> CreateRagdollEvent(CName debugSourceName) {
  return CreateRagdollActivationRequestEvent(entragdollActivationRequestType.Default, "", true, true, debugSourceName);
}

public static ref<RagdollActivationRequestEvent> CreateForceRagdollEvent(CName debugSourceName) {
  return CreateRagdollActivationRequestEvent(entragdollActivationRequestType.Forced, "", true, true, debugSourceName);
}

public static ref<RagdollActivationRequestEvent> CreateForceRagdollWithCustomFilterDataEvent(CName customFilterData, CName debugSourceName) {
  return CreateRagdollActivationRequestEvent(entragdollActivationRequestType.Forced, customFilterData, true, true, debugSourceName);
}

public static ref<RagdollActivationRequestEvent> CreateForceRagdollNoPowerPoseEvent(CName debugSourceName) {
  return CreateRagdollActivationRequestEvent(entragdollActivationRequestType.Forced, "", false, true, debugSourceName);
}

public static ref<RagdollDisableEvent> CreateDisableRagdollEvent() {
  ref<RagdollDisableEvent> evt;
  evt = new RagdollDisableEvent();
  return evt;
}

public static ref<DisableRagdollComponentEvent> CreateDisableRagdollComponentEvent() {
  ref<DisableRagdollComponentEvent> evt;
  evt = new DisableRagdollComponentEvent();
  return evt;
}

public static ref<RagdollApplyImpulseEvent> CreateRagdollApplyImpulseEvent(Vector4 worldPos, Vector4 imuplseVal, Float influenceRadius) {
  ref<RagdollApplyImpulseEvent> evt;
  evt = new RagdollApplyImpulseEvent();
  evt.worldImpulsePos = worldPos;
  evt.worldImpulseValue = imuplseVal;
  evt.influenceRadius = influenceRadius;
  return evt;
}
