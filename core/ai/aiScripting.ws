
public abstract class AIBehaviorScript extends IScriptable {

  protected final ref<ScriptedPuppet> GetPuppet(ScriptExecutionContext context) {
    return Cast(GetOwner(context));
  }

  protected final GameInstance GetGame(ScriptExecutionContext context) {
    return GetOwner(context).GetGame();
  }
}

public class AIBehaviorScriptBase extends IScriptable {

  public final native String ToString()

  public String GetDescription(ScriptExecutionContext context) {
    return ToString();
  }

  public final static ref<ScriptedPuppet> GetPuppet(ScriptExecutionContext context) {
    return Cast(GetOwner(context));
  }

  public final static ref<NPCPuppet> GetNPCPuppet(ScriptExecutionContext context) {
    return Cast(GetOwner(context));
  }

  public final static GameInstance GetGame(ScriptExecutionContext context) {
    return GetOwner(context).GetGame();
  }

  public final static Float GetAITime(ScriptExecutionContext context) {
    return ToFloat(GetAITime(context));
  }

  public final static ref<HitReactionComponent> GetHitReactionComponent(ScriptExecutionContext context) {
    return GetPuppet(context).GetHitReactionComponent();
  }

  public final static ref<AIHumanComponent> GetAIComponent(ScriptExecutionContext context) {
    return GetPuppet(context).GetAIControllerComponent();
  }

  public final static Float GetStatPoolValue(ScriptExecutionContext context, gamedataStatPoolType statPoolType) {
    StatsObjectID ownerID;
    ownerID = Cast(GetOwner(context).GetEntityID());
    return GetStatPoolsSystem(GetOwner(context).GetGame()).GetStatPoolValue(ownerID, statPoolType, false);
  }

  public final static Float GetStatPoolPercentage(ScriptExecutionContext context, gamedataStatPoolType statPoolType) {
    StatsObjectID ownerID;
    ownerID = Cast(GetOwner(context).GetEntityID());
    return GetStatPoolsSystem(GetOwner(context).GetGame()).GetStatPoolValue(ownerID, statPoolType, true);
  }

  public final static ref<GameObject> GetCombatTarget(ScriptExecutionContext context) {
    return WeakRefToRef(GetArgumentObject(context, "CombatTarget"));
  }

  public final static ref<GameObject> GetCompanion(ScriptExecutionContext context) {
    return WeakRefToRef(GetArgumentObject(context, "Companion"));
  }

  public final static gamedataNPCUpperBodyState GetUpperBodyState(ScriptExecutionContext context) {
    return ToEnum(GetPuppet(context).GetPuppetStateBlackboard().GetInt(GetAllBlackboardDefs().PuppetState.UpperBody));
  }
}

public class AIbehaviorconditionScript extends AIBehaviorScriptBase {

  protected void Activate(ScriptExecutionContext context)

  protected void Deactivate(ScriptExecutionContext context)

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    return Cast(false);
  }

  protected AIbehaviorConditionOutcomes CheckOnEvent(ScriptExecutionContext context, ref<AIEvent> behaviorEvent) {
    return Check(context);
  }

  public final native Uint16 ListenToSignal(ScriptExecutionContext context, CName signalName)

  public final native void StopListeningToSignal(ScriptExecutionContext context, CName signalName, Uint16 callbackId)
}

public static AIbehaviorConditionOutcomes Cast(Bool value) {
  if(value) {
    return AIbehaviorConditionOutcomes.True;
  };
  return AIbehaviorConditionOutcomes.False;
}

public static Bool Cast(AIbehaviorConditionOutcomes value) {
  return value == AIbehaviorConditionOutcomes.True;
}

public class AIbehaviortaskScript extends AIBehaviorScriptBase {

  protected void Activate(ScriptExecutionContext context)

  protected void Deactivate(ScriptExecutionContext context)

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  protected void ChildCompleted(ScriptExecutionContext context, AIbehaviorCompletionStatus status)

  public final static native void CutSelector(ScriptExecutionContext context)
}

public class AIbehaviorexpressionScript extends AIBehaviorScriptBase {

  public final native void MarkDirty(ref<ScriptExecutionContext> context)

  protected Bool OnBehaviorCallback(CName cbName, ScriptExecutionContext context) {
    MarkDirty(ToScriptRef(context));
    return true;
  }
}
