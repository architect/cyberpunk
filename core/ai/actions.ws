
public static ActionAnimationSlideParams GetActionAnimationSlideParams(AIActionSlideParams slideParams) {
  ActionAnimationSlideParams resultParams;
  resultParams.distance = slideParams.distance;
  resultParams.directionAngle = slideParams.directionAngle;
  resultParams.finalRotationAngle = 0;
  resultParams.offsetToTarget = slideParams.offset;
  resultParams.offsetAroundTarget = 0;
  resultParams.slideToTarget = slideParams.slideToTarget;
  resultParams.duration = slideParams.duration;
  resultParams.positionSpeed = 1;
  resultParams.rotationSpeed = 180;
  resultParams.maxSlidePositionDistance = 3;
  resultParams.maxSlideRotationAngle = 90;
  resultParams.slideStartDelay = 0;
  resultParams.usePositionSlide = true;
  resultParams.useRotationSlide = true;
  resultParams.maxTargetVelocity = 0;
  return resultParams;
}

public static ActionAnimationSlideParams GetActionAnimationSlideParams(ref<AIActionSlideData_Record> slideRecord) {
  ActionAnimationSlideParams resultParams;
  resultParams.distance = slideRecord.Distance();
  resultParams.directionAngle = slideRecord.DirectionAngle();
  resultParams.finalRotationAngle = slideRecord.FinalRotationAngle();
  resultParams.offsetToTarget = slideRecord.OffsetToTarget();
  resultParams.offsetAroundTarget = slideRecord.OffsetAroundTarget();
  resultParams.slideToTarget = ToBool(slideRecord.Target());
  resultParams.duration = slideRecord.Duration();
  resultParams.positionSpeed = slideRecord.PositionSpeed();
  resultParams.rotationSpeed = slideRecord.RotationSpeed();
  resultParams.slideStartDelay = slideRecord.SlideStartDelay();
  resultParams.usePositionSlide = slideRecord.UsePositionSlide();
  resultParams.useRotationSlide = slideRecord.UseRotationSlide();
  resultParams.maxSlidePositionDistance = slideRecord.Distance();
  resultParams.zAlignmentThreshold = slideRecord.ZAlignmentCollisionThreshold();
  resultParams.maxTargetVelocity = 0;
  resultParams.maxSlideRotationAngle = 135;
  return resultParams;
}
