
public static String OperatorAdd(String s, EAIAttitude att) {
  return s + EnumValueToString("EAIAttitude", ToInt(att));
}

public static String OperatorAdd(EAIAttitude att, String s) {
  return EnumValueToString("EAIAttitude", ToInt(att)) + s;
}

public static EAIAttitude Max(EAIAttitude a, EAIAttitude b) {
  Int32 ai;
  Int32 bi;
  ai = ToInt(a);
  bi = ToInt(b);
  return ToEnum(Max(ai, bi));
}
