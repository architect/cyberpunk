
public class SetGlitchOnUIEvent extends Event {

  [Attrib(rangeMax, "1.f")]
  [Attrib(rangeMin, "0.f")]
  public edit Float intensity;

  public final String GetFriendlyDescription() {
    return "Set Glitch On UI";
  }
}

public static exec void ForceOutline(GameInstance gameInstance, String isGreen, String opacity) {
  ref<TargetingSystem> targetSystem;
  ref<GameObject> target;
  EulerAngles distance;
  ref<DebugOutlineEvent> dbgOutlineEvent;
  Bool shouldBeGreen;
  Float opacityValue;
  dbgOutlineEvent = new DebugOutlineEvent();
  shouldBeGreen = StringToBool(isGreen);
  opacityValue = StringToFloat(opacity);
  targetSystem = GetTargetingSystem(gameInstance);
  target = targetSystem.GetObjectClosestToCrosshair(RefToWeakRef(GetPlayerSystem(gameInstance).GetLocalPlayerMainGameObject()), distance, TSQ_NPC());
  if(shouldBeGreen) {
    dbgOutlineEvent.type = EOutlineType.GREEN;
  } else {
    dbgOutlineEvent.type = EOutlineType.RED;
  };
  dbgOutlineEvent.opacity = opacityValue;
  dbgOutlineEvent.requester = GetPlayerSystem(gameInstance).GetLocalPlayerMainGameObject().GetEntityID();
  target.QueueEvent(dbgOutlineEvent);
}

public static exec void PlayRumble(GameInstance gameInstance, String presetName) {
  CName rumbleName;
  rumbleName = GetCName(Create("rumble.local." + presetName));
  PlaySound(Cast(GetPlayerSystem(gameInstance).GetLocalPlayerMainGameObject()), rumbleName);
}

public class OutlineRequest extends IScriptable {

  private CName m_requester;

  private Bool m_shouldAdd;

  private Float m_outlineDuration;

  private OutlineData m_outlineData;

  public final static ref<OutlineRequest> CreateRequest(CName requester, Bool shouldAdd, OutlineData data, Float exepectedDuration?) {
    ref<OutlineRequest> newRequest;
    newRequest = new OutlineRequest();
    newRequest.m_requester = requester;
    newRequest.m_shouldAdd = shouldAdd;
    if(exepectedDuration == 0) {
      exepectedDuration = 0.10000000149011612;
    };
    newRequest.m_outlineDuration = exepectedDuration;
    if(data.outlineStrength < 0.20000000298023224) {
      data.outlineStrength = 0.20000000298023224;
    };
    newRequest.m_outlineData = data;
    return newRequest;
  }

  public final const CName GetRequester() {
    return this.m_requester;
  }

  public final const Bool ShouldAdd() {
    return this.m_shouldAdd;
  }

  public final const OutlineData GetData() {
    return this.m_outlineData;
  }

  public final const EOutlineType GetRequestType() {
    return this.m_outlineData.outlineType;
  }

  public final const Float GetRequestStrength() {
    return this.m_outlineData.outlineStrength;
  }

  public final const Float GetOutlineDuration() {
    return this.m_outlineDuration;
  }

  public final void UpdateData(OutlineData newData) {
    this.m_outlineData = newData;
  }

  public final void SetShouldAdd(Bool b) {
    this.m_shouldAdd = b;
  }
}

public class OutlineRequestManager extends IScriptable {

  private array<ref<OutlineRequest>> m_requestsList;

  private wref<GameObject> m_owner;

  private Bool m_isBlocked;

  private array<ref<OutlineRequest>> m_dbgRequests;

  public final void BlockRequests() {
    this.m_isBlocked = true;
  }

  public final void Initialize(wref<GameObject> owner) {
    this.m_owner = owner;
  }

  public final void PushRequest(ref<OutlineRequest> freshRequest) {
    if(this.m_isBlocked) {
      return ;
    };
    if(DoesRequestAlreadyExist(freshRequest)) {
      ProcessExistingRequest(freshRequest);
      return ;
    };
    AddNewRequest(freshRequest);
  }

  public final void ClearAllRequests() {
    Clear(this.m_requestsList);
  }

  public final Bool HasAnyOutlineRequest() {
    if(Size(this.m_requestsList) > 0) {
      return true;
    };
    return false;
  }

  public final Bool HasOutlineOfType(EOutlineType type) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_requestsList)) {
      if(this.m_requestsList[i].ShouldAdd() && this.m_requestsList[i].GetRequestType() == type) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final Float FindStrongestRequest() {
    Int32 i;
    Float currentValue;
    Float highestValue;
    highestValue = 0;
    i = 0;
    while(i < Size(this.m_requestsList)) {
      currentValue = this.m_requestsList[i].GetRequestStrength();
      if(currentValue > highestValue) {
        highestValue = currentValue;
      };
      i += 1;
    };
    return highestValue;
  }

  private final Bool DoesRequestAlreadyExist(ref<OutlineRequest> requestToCheck) {
    Int32 index;
    index = FindRequestIndex(requestToCheck);
    if(index == -1) {
      return false;
    };
    return true;
  }

  private final Int32 FindRequestIndex(ref<OutlineRequest> request) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_requestsList)) {
      if(this.m_requestsList[i].GetRequester() == request.GetRequester()) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final void ProcessExistingRequest(ref<OutlineRequest> freshRequest) {
    Int32 index;
    index = FindRequestIndex(freshRequest);
    if(!freshRequest.ShouldAdd()) {
      RemoveRequest(index);
      return ;
    };
    ModifyRequestData(freshRequest.GetData(), index);
  }

  private final void AddNewRequest(ref<OutlineRequest> freshRequest) {
    Push(this.m_requestsList, freshRequest);
  }

  private final void RemoveRequest(Int32 index) {
    Erase(this.m_requestsList, index);
  }

  private final void ModifyRequestData(OutlineData newData, Int32 index) {
    this.m_requestsList[index].UpdateData(newData);
  }

  private final void HandleRequestSuppression(ref<OutlineRequest> request) {
    ref<SuppressOutlineEvent> suppressEvent;
    Float suppressAfter;
    suppressAfter = request.GetOutlineDuration();
    suppressEvent = new SuppressOutlineEvent();
    suppressEvent.requestToSuppress = request;
    GetDelaySystem(WeakRefToRef(this.m_owner).GetGame()).DelayEvent(this.m_owner, suppressEvent, suppressAfter);
  }
}

public class GameObjectListener extends IScriptable {

  public ref<PrereqState> prereqOwner;

  public Bool e3HackBlock;

  public final Bool RegisterOwner(ref<PrereqState> owner) {
    if(!ToBool(this.prereqOwner)) {
      this.prereqOwner = owner;
      return true;
    };
    return false;
  }

  public final void ModifyOwner(ref<PrereqState> owner) {
    this.prereqOwner = owner;
  }

  public final void E3BlockHack() {
    this.e3HackBlock = true;
  }

  public final void OnRedOutlineStateChanged(Bool isRevealed) {
    if(ToBool(Cast(this.prereqOwner))) {
      if(isRevealed && this.e3HackBlock) {
        return ;
      };
      this.prereqOwner.OnChanged(isRevealed);
    };
  }

  public final void OnGreenOutlineStateChanged(Bool isOn) {
    if(ToBool(Cast(this.prereqOwner))) {
      if(isOn && this.e3HackBlock) {
        return ;
      };
      this.prereqOwner.OnChanged(isOn);
    };
  }

  public final void OnRevealAccessPoint(Bool shouldReveal) {
    if(ToBool(Cast(this.prereqOwner))) {
      this.prereqOwner.OnChanged(shouldReveal);
    };
  }

  public final void OnStatusEffectTrigger(Bool shouldTrigger) {
    if(ToBool(Cast(this.prereqOwner))) {
      this.prereqOwner.OnChanged(shouldTrigger);
    };
  }
}

public class GameObject extends GameEntity {

  [Attrib(category, "HUD Manager")]
  protected Bool m_forceRegisterInHudManager;

  protected array<ref<GameObjectListener>> m_prereqListeners;

  protected array<ref<StatusEffectTriggerListener>> m_statusEffectListeners;

  protected ref<OutlineRequestManager> m_outlineRequestsManager;

  protected Int32 m_outlineFadeCounter;

  protected Bool m_fadeOutStarted;

  private Float m_lastEngineTime;

  private Float m_accumulatedTimePasssed;

  protected ref<ScanningComponent> m_scanningComponent;

  protected ref<VisionModeComponent> m_visionComponent;

  protected Bool m_isHighlightedInFocusMode;

  protected ref<StatusEffectComponent> m_statusEffectComponent;

  private ref<OutlineRequest> m_lastFrameGreen;

  private ref<OutlineRequest> m_lastFrameRed;

  protected Bool m_markAsQuest;

  protected Bool m_e3HighlightHackStarted;

  private Bool m_e3ObjectRevealed;

  protected EntityID m_forceHighlightSource;

  protected ref<WorkspotMapperComponent> m_workspotMapper;

  protected ref<StimBroadcasterComponent> m_stimBroadcaster;

  protected ref<SlotComponent> m_uiSlotComponent;

  protected ref<SquadMemberBaseComponent> m_squadMemberComponent;

  private ref<SourceShootComponent> m_sourceShootComponent;

  private ref<TargetShootComponent> m_targetShootComponent;

  protected array<DamageHistoryEntry> m_receivedDamageHistory;

  [Default(GameObject, false))]
  protected Bool m_forceDefeatReward;

  [Default(GameObject, false))]
  protected Bool m_killRewardDisabled;

  [Default(GameObject, false))]
  protected Bool m_willDieSoon;

  private Bool m_isScannerDataDirty;

  private Bool m_hasVisibilityForcedInAnimSystem;

  protected Bool m_isDead;

  public final native const CName GetName()

  public final native const GameInstance GetGame()

  public final native void RegisterInputListener(ref<IScriptable> listener, CName name?)

  public final native void RegisterInputListenerWithOwner(ref<IScriptable> listener, CName name?)

  public final native void UnregisterInputListener(ref<IScriptable> listener, CName name?)

  public final native void GetCurveValue(out Float x, out Float y, CName curveName, Bool isDebug)

  public final native const Bool IsSelectedForDebugging()

  public final native String GetTracedActionName()

  public final native const Bool IsPlayerControlled()

  public final native wref<GameObject> GetOwner()

  public final native const CName GetCurrentContext()

  public final native const Bool PlayerLastUsedPad()

  public final native const Bool PlayerLastUsedKBM()

  public final native Bool TriggerEvent(CName eventName, ref<IScriptable> data?, Int32 flags?)

  protected native const ref<GameObjectPS> GetPS()

  protected final native const ref<GameObjectPS> GetBasePS()

  public final native const Bool HasTag(CName tag)

  protected final native void EnableTransformUpdates(Bool enable)

  protected cb Bool OnDeviceLinkRequest(ref<DeviceLinkRequest> evt) {
    ref<DeviceLinkComponentPS> link;
    link = CreateAndAcquireDeviceLink(GetGame(), GetEntityID());
    if(ToBool(link)) {
      GetPersistencySystem(GetGame()).QueuePSEvent(link.GetID(), link.GetClassName(), evt);
    };
  }

  public const ref<DeviceLinkComponentPS> GetDeviceLink() {
    return AcquireDeviceLink(GetGame(), GetEntityID());
  }

  protected void OnTransformUpdated()

  public final const PersistentID GetPersistentID() {
    return Cast(GetEntityID());
  }

  public final const PSOwnerData GetPSOwnerData() {
    PSOwnerData psOwnerData;
    psOwnerData.id = GetPersistentID();
    psOwnerData.className = GetClassName();
    return psOwnerData;
  }

  public const CName GetPSClassName() {
    return GetPS().GetClassName();
  }

  protected void SendEventToDefaultPS(ref<Event> evt) {
    ref<GameObjectPS> persistentState;
    persistentState = GetPS();
    if(persistentState == null) {
      if(!IsFinal()) {
        LogError("[SendEventToDefaultPS] Unable to send event, there is no presistent state on that entity " + ToString(GetEntityID()));
      };
      return ;
    };
    GetPersistencySystem(GetGame()).QueuePSEvent(persistentState.GetID(), persistentState.GetClassName(), evt);
  }

  public const Bool IsConnectedToSecuritySystem() {
    return false;
  }

  public const ref<SecuritySystemControllerPS> GetSecuritySystem() {
    return null;
  }

  public const Bool IsTargetTresspassingMyZone(ref<GameObject> target) {
    return false;
  }

  public final static void AddListener(ref<GameObject> obj, ref<GameObjectListener> listener) {
    ref<AddOrRemoveListenerForGOEvent> evt;
    evt = new AddOrRemoveListenerForGOEvent();
    evt.listener = listener;
    evt.shouldAdd = true;
    obj.QueueEvent(evt);
  }

  public final static void RemoveListener(ref<GameObject> obj, ref<GameObjectListener> listener) {
    ref<AddOrRemoveListenerForGOEvent> evt;
    evt = new AddOrRemoveListenerForGOEvent();
    evt.listener = listener;
    evt.shouldAdd = false;
    obj.QueueEvent(evt);
  }

  protected cb Bool OnAddOrRemoveListenerForGameObject(ref<AddOrRemoveListenerForGOEvent> evt) {
    if(evt.shouldAdd) {
      Push(this.m_prereqListeners, evt.listener);
    } else {
      Remove(this.m_prereqListeners, evt.listener);
    };
  }

  public final static void AddStatusEffectTriggerListener(ref<GameObject> target, ref<StatusEffectTriggerListener> listener) {
    ref<AddStatusEffectListenerEvent> evt;
    evt = new AddStatusEffectListenerEvent();
    evt.listener = listener;
    target.QueueEvent(evt);
  }

  public final static void RemoveStatusEffectTriggerListener(ref<GameObject> target, ref<StatusEffectTriggerListener> listener) {
    ref<RemoveStatusEffectListenerEvent> evt;
    evt = new RemoveStatusEffectListenerEvent();
    evt.listener = listener;
    target.QueueEvent(evt);
  }

  protected cb Bool OnAddStatusEffectTriggerListener(ref<AddStatusEffectListenerEvent> evt) {
    Push(this.m_statusEffectListeners, evt.listener);
  }

  protected cb Bool OnRemoveStatusEffectTriggerListener(ref<RemoveStatusEffectListenerEvent> evt) {
    Remove(this.m_statusEffectListeners, evt.listener);
    GetStatPoolsSystem(GetGame()).RequestUnregisteringListener(Cast(GetEntityID()), evt.listener.m_statPoolType, evt.listener);
  }

  public final native const String GetDisplayName()

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "vision", "gameVisionModeComponent", false);
    RequestComponent(ri, "scanning", "gameScanningComponent", false);
    RequestComponent(ri, "workspotMapper", "WorkspotMapperComponent", false);
    RequestComponent(ri, "StimBroadcaster", "StimBroadcasterComponent", false);
    RequestComponent(ri, "UI_Slots", "SlotComponent", false);
    RequestComponent(ri, "SquadMember", "SquadMemberBaseComponent", false);
    RequestComponent(ri, "StatusEffect", "gameStatusEffectComponent", false);
    RequestComponent(ri, "sourceShootComponent", "gameSourceShootComponent", false);
    RequestComponent(ri, "targetShootComponent", "gameTargetShootComponent", false);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.m_scanningComponent = Cast(GetComponent(ri, "scanning"));
    this.m_visionComponent = Cast(GetComponent(ri, "vision"));
    this.m_workspotMapper = Cast(GetComponent(ri, "workspotMapper"));
    this.m_stimBroadcaster = Cast(GetComponent(ri, "StimBroadcaster"));
    this.m_uiSlotComponent = Cast(GetComponent(ri, "UI_Slots"));
    this.m_squadMemberComponent = Cast(GetComponent(ri, "SquadMember"));
    this.m_statusEffectComponent = Cast(GetComponent(ri, "StatusEffect"));
    this.m_sourceShootComponent = Cast(GetComponent(ri, "sourceShootComponent"));
    this.m_targetShootComponent = Cast(GetComponent(ri, "targetShootComponent"));
  }

  protected cb Bool OnGameAttached() {
    ref<GameAttachedEvent> evt;
    EGameplayRole role;
    evt = new GameAttachedEvent();
    evt.isGameplayRelevant = IsGameplayRelevant();
    evt.displayName = GetDisplayName();
    evt.contentScale = GetContentScale();
    if(ShouldSendGameAttachedEventToPS()) {
      SendEventToDefaultPS(evt);
    };
    if(ShouldRegisterToHUD()) {
      RegisterToHUDManager(true);
      RestoreRevealState();
      if(IsTaggedinFocusMode()) {
        TagObject(RefToWeakRef(this));
      };
    };
  }

  protected cb Bool OnDetach() {
    if(ShouldRegisterToHUD() && GetHudManager().IsRegistered(GetEntityID())) {
      RegisterToHUDManager(false);
    };
    if(this.m_hasVisibilityForcedInAnimSystem) {
      ClearForcedVisibilityInAnimSystem();
    };
  }

  public final const Bool ShouldForceRegisterInHUDManager() {
    return this.m_forceRegisterInHudManager;
  }

  public const Bool ShouldRegisterToHUD() {
    if(this.m_forceRegisterInHudManager || HasAnyClue() || ToBool(this.m_visionComponent) && this.m_visionComponent.HasDefaultHighlight()) {
      return true;
    };
    return false;
  }

  protected final void RegisterToHUDManager(Bool shouldRegister) {
    ref<HUDManager> hudManager;
    ref<HUDManagerRegistrationRequest> register;
    hudManager = GetHudManager();
    if(ToBool(hudManager)) {
      register = new HUDManagerRegistrationRequest();
      register.SetProperties(this, shouldRegister);
      hudManager.QueueRequest(register);
    };
  }

  protected final void RequestHUDRefresh(ref<HUDActorUpdateData> updateData?) {
    ref<RefreshActorRequest> request;
    request = Construct(GetEntityID(), updateData);
    GetHudManager().QueueRequest(request);
  }

  protected final void RequestHUDRefresh(EntityID targetID, ref<HUDActorUpdateData> updateData?) {
    ref<RefreshActorRequest> request;
    request = Construct(targetID, updateData);
    GetHudManager().QueueRequest(request);
  }

  public final const Bool CanScanThroughWalls() {
    Float statValue;
    ref<PlayerPuppet> player;
    player = GetPlayer(GetGame());
    if(ToBool(player)) {
      if(player.HasAutoReveal()) {
        return true;
      };
      statValue = GetStatsSystem(GetGame()).GetStatValue(Cast(player.GetEntityID()), gamedataStatType.AutoReveal);
    };
    return statValue > 0;
  }

  public final const Bool IsScannerDataDirty() {
    return this.m_isScannerDataDirty;
  }

  public final void SetScannerDirty(Bool dirty) {
    this.m_isScannerDataDirty = dirty;
  }

  public const Bool CanRevealRemoteActionsWheel() {
    return false;
  }

  public const Bool IsInitialized() {
    return true;
  }

  public const Bool IsLogicReady() {
    return true;
  }

  public const Bool ShouldReactToTarget(EntityID targetID) {
    return false;
  }

  public const ref<SenseComponent> GetSensesComponent() {
    return null;
  }

  public const ref<AttitudeAgent> GetAttitudeAgent() {
    return null;
  }

  public const TweakDBID GetScannerAttitudeTweak() {
    EAIAttitude attitude;
    ref<PlayerPuppet> playerPuppet;
    TweakDBID recordID;
    playerPuppet = Cast(GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject());
    attitude = GetAttitudeTowards(playerPuppet);
    switch(attitude) {
      case EAIAttitude.AIA_Friendly:
        recordID = "scanning_devices.attitude_friendly";
        break;
      case EAIAttitude.AIA_Neutral:
        recordID = "scanning_devices.attitude_neutral";
        break;
      case EAIAttitude.AIA_Hostile:
        recordID = "scanning_devices.attitude_hostile";
    };
    return recordID;
  }

  public final static EAIAttitude GetAttitudeTowards(ref<GameObject> first, ref<GameObject> second) {
    ref<AttitudeAgent> fa;
    ref<AttitudeAgent> fb;
    if(first == null || second == null) {
      return EAIAttitude.AIA_Neutral;
    };
    fa = first.GetAttitudeAgent();
    fb = second.GetAttitudeAgent();
    if(fa != null && fb != null) {
      return fa.GetAttitudeTowards(fb);
    };
    return EAIAttitude.AIA_Neutral;
  }

  public final const EAIAttitude GetAttitudeTowards(ref<GameObject> target) {
    ref<AttitudeAgent> fa;
    ref<AttitudeAgent> fb;
    fa = this.GetAttitudeAgent();
    if(ToBool(target)) {
      fb = target.GetAttitudeAgent();
    };
    if(fa != null && fb != null) {
      return fa.GetAttitudeTowards(fb);
    };
    return EAIAttitude.AIA_Neutral;
  }

  public final static EAIAttitude GetAttitudeBetween(ref<GameObject> first, ref<GameObject> second) {
    return GetAttitudeTowards(first, second);
  }

  public final static Bool IsFriendlyTowardsPlayer(wref<GameObject> obj) {
    if(!ToBool(obj)) {
      return false;
    };
    if(GetAttitudeTowards(WeakRefToRef(obj), GetPlayerSystem(WeakRefToRef(obj).GetGame()).GetLocalPlayerMainGameObject()) == EAIAttitude.AIA_Friendly) {
      return true;
    };
    if(GetAttitudeTowards(WeakRefToRef(obj), GetPlayerSystem(WeakRefToRef(obj).GetGame()).GetLocalPlayerControlledGameObject()) == EAIAttitude.AIA_Friendly) {
      return true;
    };
    return false;
  }

  public final const Bool IsHostile() {
    ref<PlayerPuppet> playerPuppet;
    EAIAttitude attitude;
    playerPuppet = Cast(GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject());
    attitude = GetAttitudeTowards(playerPuppet);
    if(attitude == EAIAttitude.AIA_Hostile) {
      return true;
    };
    return false;
  }

  public final static void ChangeAttitudeToHostile(wref<GameObject> owner, wref<GameObject> target) {
    ref<AttitudeAgent> attitudeOwner;
    ref<AttitudeAgent> attitudeTarget;
    if(!ToBool(owner) || !ToBool(target)) {
      return ;
    };
    attitudeOwner = WeakRefToRef(owner).GetAttitudeAgent();
    attitudeTarget = WeakRefToRef(target).GetAttitudeAgent();
    if(!ToBool(attitudeOwner) || !ToBool(attitudeTarget)) {
      return ;
    };
    if(attitudeOwner.GetAttitudeTowards(attitudeTarget) != EAIAttitude.AIA_Hostile) {
      attitudeOwner.SetAttitudeTowards(attitudeTarget, EAIAttitude.AIA_Hostile);
    };
  }

  public final static void ChangeAttitudeToNeutral(wref<GameObject> owner, wref<GameObject> target) {
    ref<AttitudeAgent> attitudeOwner;
    ref<AttitudeAgent> attitudeTarget;
    if(!ToBool(owner) || !ToBool(target)) {
      return ;
    };
    attitudeOwner = WeakRefToRef(owner).GetAttitudeAgent();
    attitudeTarget = WeakRefToRef(target).GetAttitudeAgent();
    if(!ToBool(attitudeOwner) || !ToBool(attitudeTarget)) {
      return ;
    };
    if(attitudeOwner.GetAttitudeTowards(attitudeTarget) != EAIAttitude.AIA_Neutral) {
      attitudeOwner.SetAttitudeTowards(attitudeTarget, EAIAttitude.AIA_Neutral);
    };
  }

  public const ref<TargetTrackerComponent> GetTargetTrackerComponent() {
    return null;
  }

  public final static TweakDBID GetTDBID(wref<GameObject> object) {
    TweakDBID instigatorID;
    ref<ScriptedPuppet> puppet;
    ref<Device> device;
    ref<ItemObject> item;
    puppet = Cast(WeakRefToRef(object));
    if(ToBool(puppet)) {
      return puppet.GetRecordID();
    };
    device = Cast(WeakRefToRef(object));
    if(ToBool(device)) {
      return device.GetTweakDBRecord();
    };
    item = Cast(WeakRefToRef(object));
    if(ToBool(item)) {
      return GetTDBID(item.GetItemID());
    };
    return undefined();
  }

  public final static ref<WeaponObject> GetActiveWeapon(wref<GameObject> object) {
    ref<WeaponObject> weapon;
    weapon = Cast(GetTransactionSystem(WeakRefToRef(object).GetGame()).GetItemInSlot(WeakRefToRef(object), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      return weapon;
    };
    weapon = Cast(GetTransactionSystem(WeakRefToRef(object).GetGame()).GetItemInSlot(WeakRefToRef(object), "AttachmentSlots.WeaponLeft"));
    if(ToBool(weapon)) {
      return weapon;
    };
    return weapon;
  }

  public final static Int32 StartCooldown(ref<GameObject> self, CName cooldownName, Float cooldownDuration) {
    ref<ICooldownSystem> cs;
    RegisterNewCooldownRequest cdRequest;
    if(cooldownDuration < 0 || !IsNameValid(cooldownName)) {
      return -1;
    };
    if(cooldownDuration == 0) {
      RemoveCooldown(self, cooldownName);
      return -1;
    };
    cs = GetCooldownSystem(self);
    cdRequest.cooldownName = cooldownName;
    cdRequest.duration = cooldownDuration;
    cdRequest.owner = RefToWeakRef(self);
    return cs.Register(ToScriptRef(cdRequest));
  }

  public final static void RemoveCooldown(ref<GameObject> self, CName cooldownName) {
    ref<ICooldownSystem> cs;
    Int32 cid;
    if(!IsNameValid(cooldownName)) {
      return ;
    };
    cs = GetCooldownSystem(self);
    cid = cs.GetCIDByOwnerAndName(RefToWeakRef(self), cooldownName);
    if(cs.DoesCooldownExist(cid)) {
      cs.Remove(cid);
    };
  }

  public final static Bool IsCooldownActive(ref<GameObject> self, CName cooldownName, Int32 id?) {
    ref<ICooldownSystem> cs;
    cs = GetCooldownSystem(self);
    if(!ToBool(cs)) {
      return false;
    };
    if(id > 0) {
      return cs.DoesCooldownExist(id);
    };
    id = cs.GetCIDByOwnerAndName(RefToWeakRef(self), cooldownName);
    return cs.DoesCooldownExist(id);
  }

  public final static Float GetTargetAngleInFloat(ref<GameObject> target, ref<GameObject> owner) {
    Vector4 localHitDirection;
    Float forwardLocalToWorldAngle;
    Float finalHitDirectionCalculationFloat;
    forwardLocalToWorldAngle = Heading(owner.GetWorldForward());
    localHitDirection = RotByAngleXY(target.GetWorldForward(), forwardLocalToWorldAngle);
    finalHitDirectionCalculationFloat = Heading(localHitDirection) + 180;
    return finalHitDirectionCalculationFloat;
  }

  public final static Int32 GetTargetAngleInInt(ref<GameObject> target, ref<GameObject> owner) {
    Vector4 localHitDirection;
    Float forwardLocalToWorldAngle;
    Float finalHitDirectionCalculationFloat;
    forwardLocalToWorldAngle = Heading(target.GetWorldForward());
    localHitDirection = RotByAngleXY(owner.GetWorldForward(), forwardLocalToWorldAngle);
    finalHitDirectionCalculationFloat = Heading(localHitDirection) + 180;
    if(finalHitDirectionCalculationFloat > 225 && finalHitDirectionCalculationFloat < 275.5) {
      return 1;
    };
    if(finalHitDirectionCalculationFloat > 135 && finalHitDirectionCalculationFloat < 225) {
      return 2;
    };
    if(finalHitDirectionCalculationFloat > 85 && finalHitDirectionCalculationFloat < 135) {
      return 3;
    };
    return 4;
  }

  public final static Int32 GetAttackAngleInInt(ref<gameHitEvent> hitEvent, Int32 hitSource?) {
    if(hitSource == 0) {
      return GetLocalAngleForDirectionInInt(hitEvent.hitDirection, WeakRefToRef(hitEvent.target));
    };
    return GetTargetAngleInInt(WeakRefToRef(hitEvent.attackData.GetSource()), WeakRefToRef(hitEvent.target));
  }

  public final static Int32 GetLocalAngleForDirectionInInt(Vector4 direction, ref<GameObject> owner) {
    Vector4 localHitDirection;
    Float forwardLocalToWorldAngle;
    Int32 finalHitDirectionCalculationInt;
    forwardLocalToWorldAngle = Heading(owner.GetWorldForward());
    localHitDirection = RotByAngleXY(direction, forwardLocalToWorldAngle);
    finalHitDirectionCalculationInt = RoundMath(Heading(localHitDirection) + 180 / 90);
    return finalHitDirectionCalculationInt;
  }

  public final static Float GetAttackAngleInFloat(ref<gameHitEvent> hitEvent) {
    Vector4 localHitDirection;
    Float forwardLocalToWorldAngle;
    Float finalHitDirectionCalculationfloat;
    forwardLocalToWorldAngle = Heading(WeakRefToRef(hitEvent.target).GetWorldForward());
    localHitDirection = RotByAngleXY(hitEvent.hitDirection, forwardLocalToWorldAngle);
    finalHitDirectionCalculationfloat = Heading(localHitDirection) + 180;
    return finalHitDirectionCalculationfloat;
  }

  public final static void ApplyModifierGroup(ref<GameObject> self, Uint64 modifierGroupID) {
    StatsObjectID objectID;
    objectID = Cast(self.GetEntityID());
    GetStatsSystem(self.GetGame()).ApplyModifierGroup(objectID, modifierGroupID);
  }

  public final static void RemoveModifierGroup(ref<GameObject> self, Uint64 modifierGroupID) {
    StatsObjectID objectID;
    objectID = Cast(self.GetEntityID());
    GetStatsSystem(self.GetGame()).RemoveModifierGroup(objectID, modifierGroupID);
  }

  public final static DelayID PlayVoiceOver(ref<GameObject> self, CName voName, CName debugInitialContext, Float delay?, EntityID answeringEntityID?, Bool canPlayInVehicle?) {
    ref<SoundPlayVo> evt;
    DelayID delayID;
    evt = new SoundPlayVo();
    if(!ToBool(self)) {
      return delayID;
    };
    if(IsMountedToVehicle(self.GetGame(), RefToWeakRef(self)) && !canPlayInVehicle) {
      return delayID;
    };
    if(IsServer()) {
      return delayID;
    };
    if(IsNameValid(voName)) {
      evt.voContext = voName;
      if(IsMultiplayer()) {
        evt.ignoreFrustumCheck = true;
        evt.ignoreDistanceCheck = true;
      };
      evt.debugInitialContext = debugInitialContext;
      evt.answeringEntityId = answeringEntityID;
      if(delay < 0) {
        self.QueueEvent(evt);
      } else {
        delayID = GetDelaySystem(self.GetGame()).DelayEvent(RefToWeakRef(self), evt, delay);
      };
    };
    return delayID;
  }

  public final static void PlaySound(ref<GameObject> self, CName eventName, CName emitterName?) {
    EntityID objectID;
    objectID = self.GetEntityID();
    if(!IsDefined(objectID)) {
      GetAudioSystem(self.GetGame()).Play(eventName, objectID, emitterName);
    } else {
      PlaySoundEvent(self, eventName);
    };
  }

  public final static void PlaySoundWithParams(ref<GameObject> self, CName eventName, CName emitterName?, audioAudioEventFlags flag?, audioEventActionType type?) {
    EntityID objectID;
    objectID = self.GetEntityID();
    if(!IsDefined(objectID)) {
      GetAudioSystem(self.GetGame()).Play(eventName, objectID, emitterName);
    } else {
      PlaySoundEventWithParams(self, eventName, flag, type);
    };
  }

  public final static void StopSound(ref<GameObject> self, CName eventName, CName emitterName?) {
    EntityID objectID;
    objectID = self.GetEntityID();
    if(!IsDefined(objectID)) {
      GetAudioSystem(self.GetGame()).Stop(eventName, objectID, emitterName);
    } else {
      StopSoundEvent(self, eventName);
    };
  }

  public final static void AudioSwitch(ref<GameObject> self, CName switchName, CName switchValue, CName emitterName?) {
    EntityID objectID;
    objectID = self.GetEntityID();
    GetAudioSystem(self.GetGame()).Switch(switchName, switchValue, objectID, emitterName);
  }

  public final static void AudioParameter(ref<GameObject> self, CName parameterName, Float parameterValue, CName emitterName?) {
    EntityID objectID;
    objectID = self.GetEntityID();
    GetAudioSystem(self.GetGame()).Parameter(parameterName, parameterValue, objectID, emitterName);
  }

  public final static void PlaySoundEvent(ref<GameObject> self, CName eventName) {
    ref<AudioEvent> evt;
    evt = new AudioEvent();
    if(!IsNameValid(eventName)) {
      return ;
    };
    evt.eventName = eventName;
    self.QueueEvent(evt);
  }

  public final static void PlaySoundEventWithParams(ref<GameObject> self, CName eventName, audioAudioEventFlags flag?, audioEventActionType type?) {
    ref<AudioEvent> evt;
    evt = new AudioEvent();
    if(!IsNameValid(eventName)) {
      return ;
    };
    evt.eventName = eventName;
    evt.eventFlags = flag;
    evt.eventType = type;
    self.QueueEvent(evt);
  }

  public final static void StopSoundEvent(ref<GameObject> self, CName eventName) {
    ref<SoundStopEvent> evt;
    evt = new SoundStopEvent();
    if(!IsNameValid(eventName)) {
      return ;
    };
    evt.soundName = eventName;
    self.QueueEvent(evt);
  }

  public final static void PlayMetadataEvent(ref<GameObject> self, CName eventName) {
    ref<AudioEvent> evt;
    evt = new AudioEvent();
    evt.eventFlags = audioAudioEventFlags.Metadata;
    evt.eventName = eventName;
    self.QueueEvent(evt);
  }

  public final static void SetAudioSwitch(ref<GameObject> self, CName switchName, CName switchValue) {
    ref<SoundSwitchEvent> evt;
    evt = new SoundSwitchEvent();
    evt.switchName = switchName;
    evt.switchValue = switchValue;
    self.QueueEvent(evt);
  }

  public final static void SetAudioParameter(ref<GameObject> self, CName paramName, Float paramValue) {
    ref<SoundParameterEvent> evt;
    evt = new SoundParameterEvent();
    evt.parameterName = paramName;
    evt.parameterValue = paramValue;
    self.QueueEvent(evt);
  }

  public final native void QueueReplicatedEvent(ref<Event> evt)

  public final void OnEventReplicated(ref<Event> evt) {
    QueueEvent(evt);
  }

  public final static void ActivateEffectAction(wref<GameObject> obj, gamedataFxActionType actionType, CName fxName, ref<worldEffectBlackboard> fxBlackboard?) {
    switch(actionType) {
      case gamedataFxActionType.Start:
        StartEffectEvent(WeakRefToRef(obj), fxName, false, fxBlackboard);
        break;
      case gamedataFxActionType.BreakLoop:
        BreakEffectLoopEvent(WeakRefToRef(obj), fxName);
        break;
      case gamedataFxActionType.Kill:
        StopEffectEvent(WeakRefToRef(obj), fxName);
        break;
      default:
    };
  }

  public final static void StartEffectEvent(ref<GameObject> self, CName effectName, Bool shouldPersist?, ref<worldEffectBlackboard> blackboard?) {
    ref<entSpawnEffectEvent> evt;
    ref<ItemObject> item;
    if(!IsNameValid(effectName)) {
      return ;
    };
    evt = new entSpawnEffectEvent();
    evt.effectName = effectName;
    evt.persistOnDetach = shouldPersist;
    evt.blackboard = blackboard;
    item = Cast(self);
    ToBool(item) ? item.QueueEventToChildItems(evt) : self.QueueEvent(evt);
  }

  public final static void StartReplicatedEffectEvent(ref<GameObject> self, CName effectName, Bool shouldPersist?, Bool breakAllOnDestroy?) {
    ref<entSpawnEffectEvent> evt;
    evt = new entSpawnEffectEvent();
    if(IsNameValid(effectName)) {
      evt.effectName = effectName;
      evt.persistOnDetach = shouldPersist;
      evt.breakAllOnDestroy = breakAllOnDestroy;
      self.QueueEvent(evt);
      self.QueueReplicatedEvent(evt);
    };
  }

  public final static void BreakEffectLoopEvent(ref<GameObject> self, CName effectName) {
    ref<entBreakEffectLoopEvent> evt;
    ref<ItemObject> item;
    if(!IsNameValid(effectName)) {
      return ;
    };
    evt = new entBreakEffectLoopEvent();
    evt.effectName = effectName;
    item = Cast(self);
    ToBool(item) ? item.QueueEventToChildItems(evt) : self.QueueEvent(evt);
  }

  public final static void BreakReplicatedEffectLoopEvent(ref<GameObject> self, CName effectName) {
    ref<entBreakEffectLoopEvent> evt;
    evt = new entBreakEffectLoopEvent();
    if(IsNameValid(effectName)) {
      evt.effectName = effectName;
      self.QueueEvent(evt);
      self.QueueReplicatedEvent(evt);
    };
  }

  public final static void StopEffectEvent(ref<GameObject> self, CName effectName) {
    ref<entKillEffectEvent> evt;
    ref<ItemObject> item;
    if(!IsNameValid(effectName)) {
      return ;
    };
    evt = new entKillEffectEvent();
    evt.effectName = effectName;
    item = Cast(self);
    ToBool(item) ? item.QueueEventToChildItems(evt) : self.QueueEvent(evt);
  }

  public final static void StopReplicatedEffectEvent(ref<GameObject> self, CName effectName) {
    ref<entKillEffectEvent> evt;
    evt = new entKillEffectEvent();
    evt.effectName = effectName;
    self.QueueEvent(evt);
    self.QueueReplicatedEvent(evt);
  }

  public final static void StopEffectEvent(ref<GameObject> self, EntityID id, CName effectName) {
    ref<entKillEffectEvent> evt;
    evt = new entKillEffectEvent();
    evt.effectName = effectName;
    self.QueueEventForEntityID(id, evt);
  }

  public final static void SetMeshAppearanceEvent(ref<GameObject> self, CName appearance) {
    ref<entAppearanceEvent> evt;
    ref<ForceReactivateHighlightsEvent> reactivateHighLightEvt;
    evt = new entAppearanceEvent();
    evt.appearanceName = appearance;
    self.QueueEvent(evt);
    if(self.IsHighlightedInFocusMode()) {
      reactivateHighLightEvt = new ForceReactivateHighlightsEvent();
      self.QueueEvent(reactivateHighLightEvt);
    };
  }

  public final void PassUpdate(Float dt) {
    Update(dt);
  }

  protected void Update(Float dt)

  protected cb Bool OnStatusEffectApplied(ref<ApplyStatusEffectEvent> evt) {
    ApplyStatusEffectPackages(evt);
    StartStatusEffectVFX(evt);
    StartStatusEffectSFX(evt);
    HandleICEBreakerUpdate(evt);
  }

  private final void HandleICEBreakerUpdate(ref<ApplyStatusEffectEvent> evt) {
    if(evt.staticData.GetID() == "MinigameAction.ICEBrokenMinigameMinor" || evt.staticData.GetID() == "MinigameAction.ICEBrokenMinigameMedium" || evt.staticData.GetID() == "MinigameAction.ICEBrokenMinigameMajor" || evt.staticData.GetID() == "MinigameAction.ICEBrokenMinigamePlacide") {
      RequestRefreshQuickhackMenu(GetGame(), GetEntityID());
    };
  }

  protected void ApplyStatusEffectPackages(ref<ApplyStatusEffectEvent> evt) {
    Int32 i;
    array<wref<GameplayLogicPackage_Record>> packages;
    Uint32 stackCount;
    evt.staticData.Packages(packages);
    stackCount = evt.stackCount;
    i = 0;
    while(i < Size(packages)) {
      GetGameplayLogicPackageSystem(GetGame()).ApplyPackages(RefToWeakRef(this), RefToWeakRef(Cast(FindEntityByID(GetGame(), evt.instigatorEntityID))), WeakRefToRef(packages[i]).GetID(), stackCount);
      i += 1;
    };
  }

  protected void StartStatusEffectVFX(ref<ApplyStatusEffectEvent> evt) {
    Int32 i;
    array<wref<StatusEffectFX_Record>> vfxList;
    evt.staticData.VFX(vfxList);
    i = 0;
    while(i < Size(vfxList)) {
      if(evt.isNewApplication || WeakRefToRef(vfxList[i]).ShouldReapply()) {
        StartEffectEvent(this, WeakRefToRef(vfxList[i]).Name());
      };
      i += 1;
    };
  }

  protected void StartStatusEffectSFX(ref<ApplyStatusEffectEvent> evt) {
    Int32 i;
    array<wref<StatusEffectFX_Record>> sfxList;
    evt.staticData.SFX(sfxList);
    i = 0;
    while(i < Size(sfxList)) {
      if(evt.isNewApplication || WeakRefToRef(sfxList[i]).ShouldReapply()) {
        PlaySound(this, WeakRefToRef(sfxList[i]).Name());
      };
      i += 1;
    };
  }

  protected cb Bool OnStatusEffectRemoved(ref<RemoveStatusEffect> evt) {
    RemoveStatusEffectPackages(evt);
    StopStatusEffectVFX(evt);
    StopStatusEffectSFX(evt);
  }

  protected void RemoveStatusEffectPackages(ref<RemoveStatusEffect> evt) {
    Int32 i;
    array<wref<GameplayLogicPackage_Record>> packages;
    Uint32 stackCount;
    evt.staticData.Packages(packages);
    stackCount = evt.stackCount;
    i = 0;
    while(i < Size(packages)) {
      if(WeakRefToRef(packages[i]).Stackable() || evt.isFinalRemoval) {
        GetGameplayLogicPackageSystem(GetGame()).RemovePackages(RefToWeakRef(this), WeakRefToRef(packages[i]).GetID(), stackCount);
      };
      i += 1;
    };
  }

  protected void StopStatusEffectVFX(ref<RemoveStatusEffect> evt) {
    Int32 i;
    array<wref<StatusEffectFX_Record>> vfxList;
    evt.staticData.VFX(vfxList);
    i = 0;
    while(i < Size(vfxList)) {
      if(evt.isFinalRemoval) {
        BreakEffectLoopEvent(this, WeakRefToRef(vfxList[i]).Name());
      };
      i += 1;
    };
  }

  protected void StopStatusEffectSFX(ref<RemoveStatusEffect> evt) {
    Int32 i;
    array<wref<StatusEffectFX_Record>> sfxList;
    evt.staticData.SFX(sfxList);
    i = 0;
    while(i < Size(sfxList)) {
      if(evt.isFinalRemoval) {
        StopSound(this, WeakRefToRef(sfxList[i]).Name());
      };
      i += 1;
    };
  }

  protected cb Bool OnHit(ref<gameHitEvent> evt) {
    ref<StimuliEvent> hitStim;
    SetScannerDirty(true);
    ProcessDamagePipeline(evt);
    hitStim = new StimuliEvent();
    hitStim.name = "HitStim";
    QueueEvent(hitStim);
  }

  protected cb Bool OnVehicleHit(ref<gameVehicleHitEvent> evt) {
    AttackInitContext attackContext;
    ref<IAttack> attack;
    attackContext.record = GetAttackRecord("Attacks.VehicleCollision");
    attackContext.instigator = evt.attackData.GetInstigator();
    attackContext.source = evt.attackData.GetSource();
    attack = Create(attackContext);
    evt.attackData.SetAttackDefinition(attack);
    evt.attackData.AddFlag(hitFlag.FriendlyFire, "vehicle_collision");
    GetDamageSystem(GetGame()).StartPipeline(evt);
    if(IsAlive(this)) {
      PlayVoiceOver(this, "vo_any_damage_hit", "Scripts:OnHit");
      OnHit(RefToWeakRef(Cast(this)), evt);
    };
  }

  protected cb Bool OnHitProjection(ref<gameProjectedHitEvent> evt) {
    GetDamageSystem(GetGame()).StartProjectionPipeline(evt);
  }

  protected cb Bool OnAttitudeChanged(ref<AttitudeChangedEvent> evt) {
    SetScannerDirty(true);
  }

  protected void ProcessDamagePipeline(ref<gameHitEvent> evt) {
    GetDamageSystem(GetGame()).StartPipeline(evt);
  }

  public void ReactToHitProcess(ref<gameHitEvent> hitEvent) {
    gameGodModeType targetGodMode;
    if(hitEvent.attackData.HasFlag(hitFlag.WasBlocked) || hitEvent.attackData.HasFlag(hitFlag.WasDeflected)) {
      OnHitBlockedOrDeflected(hitEvent);
    };
    GetImmortality(WeakRefToRef(hitEvent.target), targetGodMode);
    if(WeakRefToRef(hitEvent.target).IsPlayer() && targetGodMode == gameGodModeType.Invulnerable || hitEvent.attackData.HasFlag(hitFlag.DealNoDamage)) {
      return ;
    };
    OnHitUI(hitEvent);
    if(hitEvent.attackData.HasFlag(hitFlag.DisableNPCHitReaction) && !WeakRefToRef(hitEvent.target).IsPlayer()) {
      return ;
    };
    OnHitAnimation(hitEvent);
    OnHitSounds(hitEvent);
    OnHitVFX(hitEvent);
  }

  protected void OnHitBlockedOrDeflected(ref<gameHitEvent> hitEvent)

  protected void OnHitAnimation(ref<gameHitEvent> hitEvent)

  protected void OnHitUI(ref<gameHitEvent> hitEvent) {
    array<DamageInfo> dmgInfos;
    if(IsClient()) {
      return ;
    };
    dmgInfos = GetDamageSystem(GetGame()).ConvertHitDataToDamageInfo(hitEvent);
    DisplayHitUI(dmgInfos);
  }

  public void DisplayHitUI(array<DamageInfo> dmgInfos) {
    Int32 i;
    i = 0;
    while(i < Size(dmgInfos)) {
      GetTargetingSystem(GetGame()).GetPuppetBlackboardUpdater().AddDamageInfo(dmgInfos[i]);
      i += 1;
    };
  }

  public void DisplayKillUI(KillInfo killInfo) {
    GetTargetingSystem(GetGame()).GetPuppetBlackboardUpdater().AddKillInfo(killInfo);
  }

  protected void OnHitSounds(ref<gameHitEvent> hitEvent) {
    if(hitEvent.attackData.HasFlag(hitFlag.DisableSounds)) {
      return ;
    };
  }

  protected void OnHitVFX(ref<gameHitEvent> hitEvent)

  protected cb Bool OnDamageReceived(ref<gameDamageReceivedEvent> evt) {
    DamageHistoryEntry damageHistoryEvt;
    wref<GameObject> instigator;
    ref<DamageInflictedEvent> damageInflictedEvent;
    instigator = evt.hitEvent.attackData.GetInstigator();
    if(WeakRefToRef(instigator).IsControlledByAnyPeer()) {
      if(GetStatPoolsSystem(WeakRefToRef(evt.hitEvent.target).GetGame()).GetStatPoolValue(Cast(WeakRefToRef(evt.hitEvent.target).GetEntityID()), gamedataStatPoolType.Health, false) < 0) {
        TryPlayEnemyKilledChatter(WeakRefToRef(instigator));
      } else {
        TryPlayEnemyDamagedChatter(WeakRefToRef(instigator));
      };
    };
    if(evt.totalDamageReceived > 0) {
      if(Size(this.m_receivedDamageHistory) > 0) {
        if(this.m_receivedDamageHistory[Size(this.m_receivedDamageHistory) - 1].frameReceived < GetFrameNumber(GetGame())) {
          Clear(this.m_receivedDamageHistory);
        };
      };
      damageHistoryEvt.hitEvent = evt.hitEvent;
      damageHistoryEvt.frameReceived = GetFrameNumber(GetGame());
      damageHistoryEvt.timestamp = ToFloat(GetEngineTime(GetGame()));
      damageHistoryEvt.totalDamageReceived = evt.totalDamageReceived;
      damageHistoryEvt.source = evt.hitEvent.attackData.GetInstigator();
      damageHistoryEvt.target = evt.hitEvent.target;
      damageHistoryEvt.healthAtTheTime = GetStatPoolsSystem(GetGame()).GetStatPoolValue(Cast(GetEntityID()), gamedataStatPoolType.Health, false);
      Push(this.m_receivedDamageHistory, damageHistoryEvt);
    };
    TryCreate(instigator);
    if(ToBool(instigator)) {
      damageInflictedEvent = new DamageInflictedEvent();
      WeakRefToRef(instigator).QueueEvent(damageInflictedEvent);
    };
  }

  public void Record1DamageInHistory(ref<GameObject> source) {
    DamageHistoryEntry damageHistoryEvt;
    if(Size(this.m_receivedDamageHistory) > 0) {
      if(this.m_receivedDamageHistory[Size(this.m_receivedDamageHistory) - 1].frameReceived < GetFrameNumber(GetGame())) {
        Clear(this.m_receivedDamageHistory);
      };
    };
    damageHistoryEvt.frameReceived = GetFrameNumber(GetGame());
    damageHistoryEvt.timestamp = ToFloat(GetEngineTime(GetGame()));
    damageHistoryEvt.totalDamageReceived = 1;
    damageHistoryEvt.source = RefToWeakRef(source);
    damageHistoryEvt.target = RefToWeakRef(this);
    damageHistoryEvt.healthAtTheTime = 1;
    Push(this.m_receivedDamageHistory, damageHistoryEvt);
  }

  protected cb Bool OnRecord1DamageInHistoryEvent(ref<Record1DamageInHistoryEvent> evt) {
    Record1DamageInHistory(WeakRefToRef(evt.source));
  }

  public final void FindAndRewardKiller(gameKillType killType, wref<GameObject> instigator?) {
    array<PlayerTotalDamageAgainstHealth> playerDamageData;
    array<wref<GameObject>> validKillerPool;
    array<wref<GameObject>> reserveKillerPool;
    Bool isAnyDamageNonlethal;
    Int32 i;
    Int32 p;
    Int32 randomInt;
    if(Size(this.m_receivedDamageHistory) > 0) {
      i = 0;
      while(i < Size(this.m_receivedDamageHistory)) {
        if(WeakRefToRef(this.m_receivedDamageHistory[i].source) != null) {
          if(!Contains(reserveKillerPool, this.m_receivedDamageHistory[i].source)) {
            Push(reserveKillerPool, this.m_receivedDamageHistory[i].source);
            Resize(playerDamageData, Size(reserveKillerPool));
            p = Size(reserveKillerPool) - 1;
            playerDamageData[p].player = this.m_receivedDamageHistory[i].source;
            playerDamageData[p].totalDamage = this.m_receivedDamageHistory[i].totalDamageReceived;
            playerDamageData[p].targetHealth = this.m_receivedDamageHistory[i].healthAtTheTime;
          } else {
            p = FindFirst(reserveKillerPool, this.m_receivedDamageHistory[i].source);
            playerDamageData[p].totalDamage += this.m_receivedDamageHistory[i].totalDamageReceived;
            if(playerDamageData[p].targetHealth > this.m_receivedDamageHistory[i].healthAtTheTime) {
              playerDamageData[p].targetHealth = this.m_receivedDamageHistory[i].healthAtTheTime;
            };
          };
        };
        if(ToBool(this.m_receivedDamageHistory[i].hitEvent)) {
          if(this.m_receivedDamageHistory[i].hitEvent.attackData.HasFlag(hitFlag.Nonlethal)) {
            isAnyDamageNonlethal = true;
          };
        };
        i += 1;
      };
      i = 0;
      while(i < Size(playerDamageData)) {
        if(playerDamageData[i].totalDamage >= playerDamageData[i].targetHealth) {
          Push(validKillerPool, playerDamageData[i].player);
        };
        i += 1;
      };
      if(Size(validKillerPool) > 0) {
        randomInt = RandRange(0, Size(validKillerPool));
        RewardKiller(validKillerPool[randomInt], killType, isAnyDamageNonlethal);
        CheckIfPreventionShouldReact(validKillerPool);
      } else {
        if(Size(reserveKillerPool) > 0) {
          randomInt = RandRange(0, Size(reserveKillerPool));
          RewardKiller(reserveKillerPool[randomInt], killType, isAnyDamageNonlethal);
          CheckIfPreventionShouldReact(reserveKillerPool);
        };
      };
    } else {
      if(ToBool(instigator)) {
        RewardKiller(instigator, killType, isAnyDamageNonlethal);
        Push(validKillerPool, instigator);
        CheckIfPreventionShouldReact(validKillerPool);
      };
    };
  }

  protected void RewardKiller(wref<GameObject> killer, gameKillType killType, Bool isAnyDamageNonlethal) {
    ref<KillRewardEvent> killRewardEvt;
    if(this.m_killRewardDisabled) {
      return ;
    };
    if(this.m_willDieSoon && killType == gameKillType.Normal) {
      return ;
    };
    killRewardEvt = new KillRewardEvent();
    killRewardEvt.victim = RefToWeakRef(this);
    if(this.m_forceDefeatReward) {
      killRewardEvt.killType = gameKillType.Defeat;
    } else {
      if(this.m_willDieSoon) {
        killRewardEvt.killType = gameKillType.Normal;
      } else {
        killRewardEvt.killType = killType;
      };
    };
    WeakRefToRef(killer).QueueEvent(killRewardEvt);
  }

  public final void ForceDefeatReward(Bool value) {
    this.m_forceDefeatReward = value;
  }

  public final void DisableKillReward(Bool value) {
    this.m_killRewardDisabled = value;
  }

  protected cb Bool OnChangeRewardSettingsEvent(ref<ChangeRewardSettingsEvent> evt) {
    ForceDefeatReward(evt.forceDefeatReward);
    DisableKillReward(evt.disableKillReward);
  }

  protected cb Bool OnWillDieSoonEventEvent(ref<WillDieSoonEvent> evt) {
    this.m_willDieSoon = true;
  }

  private final void CheckIfPreventionShouldReact(array<wref<GameObject>> damageDealers) {
    Int32 i;
    if(ShouldPreventionSystemReactToKill(RefToWeakRef(Cast(this)))) {
      i = 0;
      while(i < Size(damageDealers)) {
        if(WeakRefToRef(damageDealers[i]).IsPlayer()) {
          CreateNewDamageRequest(GetGame(), this, 1);
          return ;
        };
        i += 1;
      };
    };
  }

  public const Bool IsVehicle() {
    return false;
  }

  public const Bool IsPuppet() {
    return false;
  }

  public const Bool IsPlayer() {
    return false;
  }

  public const Bool IsReplacer() {
    return false;
  }

  public const Bool IsVRReplacer() {
    return false;
  }

  public const Bool IsJohnnyReplacer() {
    return false;
  }

  public const Bool IsNPC() {
    return false;
  }

  public const Bool IsContainer() {
    return false;
  }

  public const Bool IsShardContainer() {
    return false;
  }

  public const Bool IsDevice() {
    return false;
  }

  public const Bool IsSensor() {
    return false;
  }

  public const Bool IsTurret() {
    return false;
  }

  public const Bool IsActive() {
    return false;
  }

  public const Bool IsPrevention() {
    return false;
  }

  public const Bool IsDropPoint() {
    return false;
  }

  public const Bool IsDrone() {
    return false;
  }

  protected final const Bool IsItem() {
    return Cast(this) != null;
  }

  public const Bool IsDead() {
    if(GetStatPoolsSystem(GetGame()).GetStatPoolValue(Cast(GetEntityID()), gamedataStatPoolType.Health, false) < 0) {
      return true;
    };
    return false;
  }

  public const Bool IsDeadNoStatPool() {
    return IsDead();
  }

  public void UpdateAdditionalScanningData() {
    GameObjectScanStats stats;
    ref<IBlackboard> bb;
    stats.scannerData.entityName = GetDisplayName();
    bb = GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().UI_Scanner);
    if(ToBool(bb)) {
      bb.SetVariant(GetAllBlackboardDefs().UI_Scanner.scannerObjectStats, ToVariant(stats));
      bb.SignalVariant(GetAllBlackboardDefs().UI_Scanner.scannerObjectStats);
    };
  }

  protected cb Bool OnOutlineItemRequestEvent(ref<OutlineItemRequestEvent> evt)

  protected cb Bool OnOutlineRequestEvent(ref<OutlineRequestEvent> evt) {
    if(!this.m_e3HighlightHackStarted) {
      this.m_e3HighlightHackStarted = true;
      StartUpdate();
    };
    if(IsDead()) {
      return false;
    };
    if(evt.outlineRequest.GetRequestType() == EOutlineType.RED) {
      this.m_lastFrameRed = evt.outlineRequest;
    } else {
      if(evt.outlineRequest.GetRequestType() == EOutlineType.GREEN) {
        this.m_lastFrameGreen = evt.outlineRequest;
      };
    };
  }

  private final void StartUpdate() {
    FireSingleE3Tick();
  }

  private final void FireSingleE3Tick() {
    ref<FakeUpdateEvent> fakeUpdate;
    fakeUpdate = new FakeUpdateEvent();
    QueueEvent(fakeUpdate);
  }

  protected cb Bool OnFakeUpdate(ref<FakeUpdateEvent> evt) {
    if(this.m_e3HighlightHackStarted) {
      QueueEvent(evt);
      EvaluateLastFrameRequest();
      ClearLastFrame();
    };
  }

  private final void EvaluateLastFrameRequest() {
    if(!ToBool(this.m_outlineRequestsManager)) {
      this.m_outlineRequestsManager = new OutlineRequestManager();
      this.m_outlineRequestsManager.Initialize(RefToWeakRef(this));
    };
    if(IsDead()) {
      return ;
    };
    if(this.m_lastFrameGreen.ShouldAdd()) {
      this.m_outlineRequestsManager.PushRequest(this.m_lastFrameGreen);
    } else {
      if(this.m_lastFrameRed.ShouldAdd()) {
        this.m_outlineRequestsManager.PushRequest(this.m_lastFrameRed);
      } else {
        Suppress();
      };
    };
    SetUpProperOutline();
  }

  private final void ClearLastFrame() {
    this.m_lastFrameGreen = null;
    this.m_lastFrameRed = null;
  }

  private final void Suppress() {
    if(IsDead()) {
      return ;
    };
    this.m_outlineRequestsManager.ClearAllRequests();
  }

  protected cb Bool OnSuppressOutlineEvent(ref<SuppressOutlineEvent> evt) {
    ref<OutlineRequest> newRequest;
    newRequest = new OutlineRequest();
    if(IsDead()) {
      return false;
    };
    newRequest = CreateRequest(evt.requestToSuppress.GetRequester(), false, evt.requestToSuppress.GetData());
    this.m_outlineRequestsManager.PushRequest(newRequest);
    SetUpProperOutline();
  }

  private final void SetUpProperOutline() {
    Bool isGreen;
    Int32 i;
    ref<HighlightOpacityEvent> opacityEvent;
    Int32 lisSize;
    lisSize = Size(this.m_prereqListeners);
    opacityEvent = new HighlightOpacityEvent();
    isGreen = this.m_outlineRequestsManager.HasOutlineOfType(EOutlineType.GREEN);
    if(IsDead()) {
      return ;
    };
    if(isGreen) {
      i = 0;
      while(i < Size(this.m_prereqListeners)) {
        this.m_prereqListeners[i].OnRedOutlineStateChanged(false);
        this.m_prereqListeners[i].OnGreenOutlineStateChanged(true);
        i += 1;
      };
      opacityEvent.opacity = 1;
      QueueEvent(opacityEvent);
    } else {
      if(this.m_outlineRequestsManager.HasOutlineOfType(EOutlineType.RED)) {
        i = 0;
        while(i < Size(this.m_prereqListeners)) {
          this.m_prereqListeners[i].OnGreenOutlineStateChanged(false);
          this.m_prereqListeners[i].OnRedOutlineStateChanged(true);
          i += 1;
        };
        opacityEvent.opacity = 1;
        QueueEvent(opacityEvent);
      } else {
        if(!this.m_outlineRequestsManager.HasAnyOutlineRequest()) {
          i = 0;
          while(i < Size(this.m_prereqListeners)) {
            this.m_prereqListeners[i].OnGreenOutlineStateChanged(false);
            this.m_prereqListeners[i].OnRedOutlineStateChanged(false);
            i += 1;
          };
          QueueEvent(opacityEvent);
        };
      };
    };
  }

  protected final void FadeOutOutlines() {
    ref<FadeOutOutlinesUpdate> fadeOutUpdate;
    ref<OutlineRequest> lastRequest;
    OutlineData data;
    Int32 i;
    if(!ToBool(this.m_outlineRequestsManager) || !this.m_outlineRequestsManager.HasAnyOutlineRequest() || this.m_fadeOutStarted) {
      return ;
    };
    fadeOutUpdate = new FadeOutOutlinesUpdate();
    lastRequest = new OutlineRequest();
    data.outlineType = EOutlineType.RED;
    data.outlineStrength = 1;
    lastRequest = CreateRequest("lastRequest", true, data);
    this.m_fadeOutStarted = true;
    this.m_outlineRequestsManager.ClearAllRequests();
    this.m_outlineRequestsManager.PushRequest(lastRequest);
    this.m_outlineRequestsManager.BlockRequests();
    i = 0;
    while(i < Size(this.m_prereqListeners)) {
      this.m_prereqListeners[i].OnGreenOutlineStateChanged(false);
      this.m_prereqListeners[i].OnRedOutlineStateChanged(true);
      i += 1;
    };
    GetDelaySystem(GetGame()).TickOnEvent(RefToWeakRef(this), fadeOutUpdate, 5.5);
  }

  protected cb Bool OnFadeOutOutlinesUpdate(ref<FadeOutOutlinesUpdate> evt) {
    ref<HighlightOpacityEvent> opacityEvent;
    Float strongestOutline;
    Int32 i;
    Float acumulatedTimePassed;
    Bool isDead;
    this.m_e3HighlightHackStarted = false;
    isDead = IsDead();
    strongestOutline = this.m_outlineRequestsManager.FindStrongestRequest();
    this.m_outlineFadeCounter += 1;
    this.m_accumulatedTimePasssed += CalculateRealTimePassed();
    acumulatedTimePassed = this.m_accumulatedTimePasssed;
    if(this.m_accumulatedTimePasssed >= 5.5 || evt.state == gameTickableEventState.LastTick) {
      this.m_outlineRequestsManager.ClearAllRequests();
      i = 0;
      while(i < Size(this.m_prereqListeners)) {
        this.m_prereqListeners[i].OnGreenOutlineStateChanged(false);
        this.m_prereqListeners[i].OnRedOutlineStateChanged(false);
        this.m_prereqListeners[i].E3BlockHack();
        i += 1;
      };
      this.m_accumulatedTimePasssed = 0;
      this.m_e3HighlightHackStarted = false;
      opacityEvent = new HighlightOpacityEvent();
      opacityEvent.opacity = 0;
      QueueEvent(opacityEvent);
      return false;
    };
    opacityEvent = new HighlightOpacityEvent();
    opacityEvent.opacity = CalculateOpacity(strongestOutline);
    if(opacityEvent.opacity < 0.20000000298023224) {
      Log("success");
    };
    QueueEvent(opacityEvent);
  }

  private final Float CalculateRealTimePassed() {
    Float newTime;
    Float deltaTime;
    newTime = ToFloat(GetEngineTime(GetGame()));
    if(this.m_lastEngineTime != 0) {
      deltaTime = newTime - this.m_lastEngineTime;
    };
    this.m_lastEngineTime = newTime;
    return deltaTime;
  }

  protected cb Bool OnForceFadeOutlineEventForWeapon(ref<ForceFadeOutlineEventForWeapon> evt) {
    Log("GameObject \ ForceFadeOutlineEventForWeapon " + ToDebugString(GetEntityID()));
    FadeOutOutlines();
  }

  protected final Int32 CalculateAmountOfTicks() {
    return FloorF(30 * 5.5);
  }

  protected final Float CalculateOpacity(Float outlineStrength) {
    Float progress;
    Float result;
    progress = this.m_accumulatedTimePasssed / 5.5;
    result = 1 - progress / outlineStrength;
    return result;
  }

  public final Bool ShouldEnableOutlineRed() {
    return this.m_outlineRequestsManager.HasOutlineOfType(EOutlineType.RED);
  }

  public final Bool ShouldEnableOutlineGreen() {
    return this.m_outlineRequestsManager.HasOutlineOfType(EOutlineType.GREEN);
  }

  protected cb Bool OnDebugOutlineEvent(ref<DebugOutlineEvent> evt) {
    ref<OutlineRequestEvent> outlineRequestEvent;
    ref<OutlineRequest> outlineRequest;
    OutlineData data;
    outlineRequestEvent = new OutlineRequestEvent();
    outlineRequest = new OutlineRequest();
    data.outlineType = evt.type;
    data.outlineStrength = evt.opacity;
    outlineRequest = CreateRequest("debug", true, data);
    outlineRequestEvent.outlineRequest = outlineRequest;
    outlineRequestEvent.flag = false;
    QueueEvent(outlineRequestEvent);
  }

  protected cb Bool OnScanningModeChanged(ref<ScanningModeEvent> evt) {
    GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().UI_Scanner).SetVariant(GetAllBlackboardDefs().UI_Scanner.ScannerMode, ToVariant(evt));
  }

  protected cb Bool OnScanningLookedAt(ref<ScanningLookAtEvent> evt) {
    if(evt.state) {
      PurgeScannerBlackboard();
      SetScannerDirty(true);
    };
  }

  protected cb Bool OnLookedAtEvent(ref<LookedAtEvent> evt)

  protected cb Bool OnPulseEvent(ref<gameVisionModeUpdateVisuals> evt) {
    if(IsPlayer() || IsItem() || GetNetworkSystem().IsPingLinksLimitReached() || GetNetworkSystem().HasActivePing(GetEntityID()) || this.m_e3ObjectRevealed) {
      return false;
    };
    if(evt.pulse) {
    };
  }

  protected void PulseNetwork(Bool revealNetworkAtEnd) {
    ref<StartPingingNetworkRequest> request;
    Float duration;
    this.m_e3ObjectRevealed = true;
    if(GetQuestsSystem(GetGame()).GetFact("pingingNetworkDisabled") > 0) {
      return ;
    };
    request = new StartPingingNetworkRequest();
    duration = GetNetworkSystem().GetSpacePingDuration();
    request.source = RefToWeakRef(this);
    request.fxResource = GetFxResourceByKey("pingNetworkLink");
    request.duration = duration;
    request.pingType = EPingType.SPACE;
    request.fakeLinkType = ELinkType.FREE;
    request.revealNetworkAtEnd = revealNetworkAtEnd;
    GetNetworkSystem().QueueRequest(request);
  }

  public final const ref<TakeOverControlSystem> GetTakeOverControlSystem() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("TakeOverControlSystem"));
  }

  public final const ref<FocusModeTaggingSystem> GetTaggingSystem() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("FocusModeTaggingSystem"));
  }

  public final static void TagObject(wref<GameObject> obj) {
    ref<TagObjectRequest> request;
    if(!ToBool(obj) || !WeakRefToRef(obj).CanBeTagged()) {
      return ;
    };
    request = new TagObjectRequest();
    request.object = obj;
    WeakRefToRef(obj).GetTaggingSystem().QueueRequest(request);
  }

  public final static void UntagObject(wref<GameObject> obj) {
    ref<UnTagObjectRequest> request;
    if(!ToBool(obj)) {
      return ;
    };
    request = new UnTagObjectRequest();
    request.object = obj;
    WeakRefToRef(obj).GetTaggingSystem().QueueRequest(request);
  }

  public const Bool CanBeTagged() {
    return true;
  }

  protected cb Bool OnTagObjectEvent(ref<TagObjectEvent> evt) {
    if(evt.isTagged) {
      TagObject(RefToWeakRef(this));
    } else {
      UntagObject(RefToWeakRef(this));
    };
  }

  public const ref<FocusForcedHighlightData> GetDefaultHighlight() {
    ref<FocusForcedHighlightData> highlight;
    EFocusOutlineType outline;
    ref<VehicleObject> vehicle;
    if(IsBraindanceBlocked() || IsPhotoModeBlocked()) {
      return null;
    };
    outline = GetCurrentOutline();
    highlight = new FocusForcedHighlightData();
    highlight.sourceID = GetEntityID();
    highlight.sourceName = GetClassName();
    highlight.priority = EPriority.Low;
    highlight.outlineType = outline;
    vehicle = Cast(GetEntity(this.GetEntity()));
    if(IsQuest()) {
      highlight.highlightType = EFocusForcedHighlightType.QUEST;
      highlight.outlineType = EFocusOutlineType.QUEST;
    } else {
      if(ToBool(vehicle) && !vehicle.IsPlayerMounted()) {
        highlight.highlightType = EFocusForcedHighlightType.INTERACTION;
        highlight.outlineType = EFocusOutlineType.INTERACTION;
      } else {
        if(IsTaggedinFocusMode()) {
          highlight.highlightType = EFocusForcedHighlightType.INTERACTION;
          highlight.outlineType = EFocusOutlineType.INTERACTION;
        } else {
          highlight = null;
        };
      };
    };
    return highlight;
  }

  protected final void UpdateDefaultHighlight() {
    ref<ForceUpdateDefaultHighlightEvent> updateHighlightEvt;
    updateHighlightEvt = new ForceUpdateDefaultHighlightEvent();
    QueueEvent(updateHighlightEvt);
  }

  public const EFocusOutlineType GetCurrentOutline() {
    return EFocusOutlineType.INVALID;
  }

  public final const EFocusForcedHighlightType GetDefaultHighlightType() {
    ref<FocusForcedHighlightData> data;
    data = GetDefaultHighlight();
    if(data != null) {
      return data.highlightType;
    };
    return EFocusForcedHighlightType.INVALID;
  }

  public final const Bool HasHighlight(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType) {
    if(!ToBool(this.m_visionComponent)) {
      return false;
    };
    return this.m_visionComponent.HasHighlight(highlightType, outlineType);
  }

  public final const Bool HasOutlineOrFill(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType) {
    if(!ToBool(this.m_visionComponent)) {
      return false;
    };
    return this.m_visionComponent.HasOutlineOrFill(highlightType, outlineType);
  }

  public final const Bool HasHighlight(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType, EntityID sourceID) {
    if(!ToBool(this.m_visionComponent)) {
      return false;
    };
    return this.m_visionComponent.HasHighlight(highlightType, outlineType, sourceID);
  }

  public final const Bool HasHighlight(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType, EntityID sourceID, CName sourceName) {
    if(!ToBool(this.m_visionComponent)) {
      return false;
    };
    return this.m_visionComponent.HasHighlight(highlightType, outlineType, sourceID, sourceName);
  }

  public final const Bool HasRevealRequest(gameVisionModeSystemRevealIdentifier data) {
    if(!ToBool(this.m_visionComponent)) {
      return false;
    };
    return this.m_visionComponent.HasRevealRequest(data);
  }

  protected final void CancelForcedVisionAppearance(ref<FocusForcedHighlightData> data) {
    ref<ForceVisionApperanceEvent> evt;
    evt = new ForceVisionApperanceEvent();
    evt.forcedHighlight = data;
    evt.apply = false;
    QueueEvent(evt);
  }

  protected final void ForceVisionAppearance(ref<FocusForcedHighlightData> data) {
    ref<ForceVisionApperanceEvent> evt;
    evt = new ForceVisionApperanceEvent();
    evt.forcedHighlight = data;
    evt.apply = true;
    QueueEvent(evt);
  }

  public final static void ForceVisionAppearance(ref<GameObject> self, ref<FocusForcedHighlightData> data) {
    ref<ForceVisionApperanceEvent> evt;
    evt = new ForceVisionApperanceEvent();
    evt.forcedHighlight = data;
    evt.apply = true;
    self.QueueEvent(evt);
  }

  public final static ref<FocusForcedHighlightData> SetFocusForcedHightlightData(EFocusOutlineType outType, EFocusForcedHighlightType highType, EPriority prio, EntityID id, CName className) {
    ref<FocusForcedHighlightData> newData;
    newData = new FocusForcedHighlightData();
    newData.outlineType = outType;
    newData.highlightType = highType;
    newData.priority = prio;
    newData.sourceID = id;
    newData.sourceName = className;
    return newData;
  }

  protected final void SendReactivateHighlightEvent() {
    ref<ForceReactivateHighlightsEvent> evt;
    evt = new ForceReactivateHighlightsEvent();
    QueueEvent(evt);
  }

  public const array<wref<GameObject>> GetObjectToForwardHighlight() {
    array<wref<GameObject>> emptyArray;
    return emptyArray;
  }

  protected cb Bool OnHUDInstruction(ref<HUDInstruction> evt) {
    if(evt.highlightInstructions.GetState() == InstanceState.ON) {
      this.m_isHighlightedInFocusMode = true;
    } else {
      if(evt.highlightInstructions.WasProcessed()) {
        this.m_isHighlightedInFocusMode = false;
      };
    };
  }

  protected final void TryOpenQuickhackMenu(Bool shouldOpen) {
    if(shouldOpen) {
      shouldOpen = CanRevealRemoteActionsWheel();
    };
    SendQuickhackCommands(shouldOpen);
  }

  protected void SendQuickhackCommands(Bool shouldOpen)

  protected final void SendForceRevealObjectEvent(Bool reveal, CName reason, EntityID instigatorID?, Float lifetime?, Float delay?) {
    ref<RevealObjectEvent> evt;
    evt = new RevealObjectEvent();
    evt.reveal = reveal;
    evt.reason.reason = reason;
    evt.reason.sourceEntityId = instigatorID;
    evt.lifetime = lifetime;
    if(delay > 0) {
      GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), evt, delay, true);
    } else {
      this.QueueEvent(evt);
    };
  }

  public final static void SendForceRevealObjectEvent(ref<GameObject> self, Bool reveal, CName reason, EntityID instigatorID?, Float lifetime?, Float delay?) {
    self.SendForceRevealObjectEvent(reveal, reason, instigatorID, lifetime, delay);
  }

  private final void RestoreRevealState() {
    ref<RestoreRevealStateEvent> evt;
    if(IsObjectRevealed()) {
      evt = new RestoreRevealStateEvent();
      QueueEvent(evt);
    };
  }

  public const Bool IsHighlightedInFocusMode() {
    return this.m_isHighlightedInFocusMode;
  }

  public final const Bool IsScanned() {
    if(this.m_scanningComponent != null) {
      return this.m_scanningComponent.IsScanned();
    };
    return false;
  }

  public final const braindanceVisionMode GetBraindanceLayer() {
    if(ToBool(this.m_scanningComponent)) {
      return this.m_scanningComponent.GetBraindanceLayer();
    };
    return braindanceVisionMode.Default;
  }

  public final const Bool IsObjectRevealed() {
    if(this.m_visionComponent == null) {
      return false;
    };
    return this.m_visionComponent.IsRevealed();
  }

  protected final ref<FastTravelSystem> GetFastTravelSystem() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("FastTravelSystem"));
  }

  protected final const ref<NetworkSystem> GetNetworkSystem() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("NetworkSystem"));
  }

  public const Bool CanOverrideNetworkContext() {
    return false;
  }

  public const Bool IsAccessPoint() {
    return false;
  }

  protected void StartPingingNetwork()

  protected void StopPingingNetwork()

  public const CName GetNetworkLinkSlotName(out WorldTransform transform) {
    return "";
  }

  public const CName GetNetworkLinkSlotName() {
    return "";
  }

  public const CName GetRoleMappinSlotName() {
    return "roleMappin";
  }

  public const CName GetQuickHackIndicatorSlotName() {
    return "uploadBar";
  }

  public const CName GetPhoneCallIndicatorSlotName() {
    return "phoneCall";
  }

  public const Bool IsNetworkLinkDynamic() {
    return false;
  }

  public const Vector4 GetNetworkBeamEndpoint() {
    Vector4 beamPos;
    beamPos = GetWorldPosition();
    return beamPos;
  }

  public const Bool IsNetworkKnownToPlayer() {
    return false;
  }

  public const Bool CanPlayerUseQuickHackVulnerability(TweakDBID data) {
    return false;
  }

  public const Bool IsConnectedToBackdoorDevice() {
    return false;
  }

  public const Bool IsInIconForcedVisibilityRange() {
    return false;
  }

  public void EvaluateMappinsVisualState() {
    ref<EvaluateMappinsVisualStateEvent> evt;
    evt = new EvaluateMappinsVisualStateEvent();
    QueueEvent(evt);
  }

  public const Bool IsGameplayRelevant() {
    EGameplayRole role;
    role = DeterminGameplayRole();
    return role != EGameplayRole. && role != EGameplayRole.UnAssigned;
  }

  public const Bool ShouldSendGameAttachedEventToPS() {
    return true;
  }

  public const TweakDBID GetContentScale() {
    TweakDBID id;
    return id;
  }

  public const Bool IsGameplayRoleValid(EGameplayRole role) {
    return true;
  }

  public const EGameplayRole DeterminGameplayRole() {
    if(ToBool(this.m_scanningComponent) && this.m_scanningComponent.IsAnyClueEnabled()) {
      return EGameplayRole.Clue;
    };
    return EGameplayRole.;
  }

  public const EMappinVisualState DeterminGameplayRoleMappinVisuaState(SDeviceMappinData data) {
    if(HasAnyClue() && IsClueInspected()) {
      return EMappinVisualState.Inactive;
    };
    return EMappinVisualState.Default;
  }

  public const Float DeterminGameplayRoleMappinRange(SDeviceMappinData data) {
    return 0;
  }

  protected cb Bool OnGameplayRoleChangeNotification(ref<GameplayRoleChangeNotification> evt) {
    if(evt.newRole == EGameplayRole. && evt.oldRole != EGameplayRole.) {
      RequestHUDRefresh();
      RegisterToHUDManager(false);
    } else {
      if(evt.newRole != EGameplayRole. && evt.oldRole == EGameplayRole.) {
        RegisterToHUDManager(true);
      };
    };
  }

  public const Bool IsHackingPlayer() {
    return false;
  }

  public const Bool IsQuickHackAble() {
    return false;
  }

  public const Bool IsQuickHacksExposed() {
    return false;
  }

  public const Bool IsBreached() {
    return false;
  }

  public const Bool IsBackdoor() {
    return false;
  }

  public const Bool IsActiveBackdoor() {
    return false;
  }

  public const Bool IsBodyDisposalPossible() {
    return false;
  }

  public const Bool IsControllingDevices() {
    return false;
  }

  public const Bool HasAnySlaveDevices() {
    return false;
  }

  public const Bool IsFastTravelPoint() {
    return false;
  }

  public const Bool IsExplosive() {
    return false;
  }

  public const Bool HasImportantInteraction() {
    return false;
  }

  public const Bool HasAnyDirectInteractionActive() {
    return false;
  }

  public const Bool ShouldEnableRemoteLayer() {
    return false;
  }

  public const Bool IsTechie() {
    return false;
  }

  public const Bool IsSolo() {
    return false;
  }

  public const Bool IsNetrunner() {
    return false;
  }

  public final const Bool IsAnyPlaystyleValid() {
    return IsTechie() || IsSolo() || IsNetrunner();
  }

  public const Bool IsHackingSkillCheckActive() {
    return false;
  }

  public const Bool IsDemolitionSkillCheckActive() {
    return false;
  }

  public const Bool IsEngineeringSkillCheckActive() {
    return false;
  }

  public const Bool CanPassEngineeringSkillCheck() {
    return false;
  }

  public const Bool CanPassDemolitionSkillCheck() {
    return false;
  }

  public const Bool CanPassHackingSkillCheck() {
    return false;
  }

  public const Bool HasDirectActionsActive() {
    return false;
  }

  public const Bool HasActiveDistraction() {
    return false;
  }

  public const Bool HasActiveQuickHackUpload() {
    return false;
  }

  public const Bool IsInvestigating() {
    return false;
  }

  public const Bool IsInvestigatingObject(ref<GameObject> targetID) {
    return false;
  }

  public final const Bool IsTaggedinFocusMode() {
    return GetVisionModeSystem(GetGame()).GetScanningController().IsTagged(this);
  }

  public const Bool IsQuest() {
    return this.m_markAsQuest;
  }

  protected cb Bool OnSetAsQuestImportantEvent(ref<SetAsQuestImportantEvent> evt) {
    ToggleQuestImportance(evt.IsImportant());
  }

  protected final void ToggleQuestImportance(Bool isImportant) {
    if(IsQuest() != isImportant) {
      MarkAsQuest(isImportant);
      RequestHUDRefresh();
    };
  }

  protected void MarkAsQuest(Bool isQuest) {
    this.m_markAsQuest = isQuest;
  }

  public final const Bool IsGrouppedClue() {
    return ToBool(this.m_scanningComponent) && this.m_scanningComponent.IsActiveClueLinked();
  }

  public final const Bool HasAnyClue() {
    return ToBool(this.m_scanningComponent) && this.m_scanningComponent.HasAnyClue();
  }

  public final const Bool IsClueInspected() {
    return ToBool(this.m_scanningComponent) && this.m_scanningComponent.IsClueInspected();
  }

  public final const LinkedFocusClueData GetLinkedClueData(Int32 clueIndex) {
    LinkedFocusClueData linkedClueData;
    FocusClueDefinition clue;
    if(this.m_scanningComponent != null) {
      this.m_scanningComponent.GetLinkedClueData(clueIndex, linkedClueData);
    };
    return linkedClueData;
  }

  public final const Int32 GetAvailableClueIndex() {
    if(this.m_scanningComponent != null) {
      return this.m_scanningComponent.GetAvailableClueIndex();
    };
    return -1;
  }

  protected final void PurgeScannerBlackboard() {
    wref<IBlackboard> scannerBlackboard;
    scannerBlackboard = RefToWeakRef(GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().UI_ScannerModules));
    if(ToBool(scannerBlackboard)) {
      WeakRefToRef(scannerBlackboard).ClearAllFields(false);
    };
  }

  protected cb Bool OnlinkedClueTagEvent(ref<linkedClueTagEvent> evt) {
    if(evt.tag) {
      TagObject(RefToWeakRef(this));
    } else {
      UntagObject(RefToWeakRef(this));
    };
  }

  public const Bool CompileScannerChunks() {
    wref<IBlackboard> scannerBlackboard;
    ref<ScannerName> nameChunk;
    String displayName;
    Bool hasValidDisplayName;
    scannerBlackboard = RefToWeakRef(GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().UI_ScannerModules));
    if(ToBool(scannerBlackboard)) {
      displayName = GetDisplayName();
      hasValidDisplayName = IsStringValid(displayName);
      if(!hasValidDisplayName) {
        displayName = "";
      };
      if(hasValidDisplayName || ToBool(this.m_scanningComponent) && this.m_scanningComponent.IsAnyClueEnabled() || this.m_scanningComponent.HasValidObjectDescription()) {
        nameChunk = new ScannerName();
        nameChunk.Set(displayName);
        WeakRefToRef(scannerBlackboard).SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerName, ToVariant(nameChunk), true);
      };
      WeakRefToRef(scannerBlackboard).SetInt(GetAllBlackboardDefs().UI_ScannerModules.ObjectType, ToInt(ScannerObjectType.GENERIC), true);
      return true;
    };
    return false;
  }

  protected void FillObjectDescription(out array<ScanningTooltipElementDef> arr) {
    Int32 i;
    ref<ObjectScanningDescription> objectDescription;
    array<TweakDBID> customDescriptionsIDS;
    TweakDBID gameplayDescriptionID;
    ScanningTooltipElementDef objectData;
    objectDescription = this.m_scanningComponent.GetObjectDescription();
    if(objectDescription == null) {
      return ;
    };
    customDescriptionsIDS = objectDescription.GetCustomDesriptions();
    gameplayDescriptionID = objectDescription.GetGameplayDesription();
    if(IsValid(gameplayDescriptionID)) {
      objectData.recordID = gameplayDescriptionID;
      Push(arr, objectData);
    };
    if(Size(customDescriptionsIDS) != 0) {
      i = 0;
      while(i < Size(customDescriptionsIDS)) {
        objectData.recordID = customDescriptionsIDS[i];
        Push(arr, objectData);
        i += 1;
      };
    };
  }

  public array<ScanningTooltipElementDef> GetScannableObjects() {
    Int32 clueIndex;
    ScanningTooltipElementDef conclusionData;
    array<ScanningTooltipElementDef> arr;
    if(this.m_scanningComponent != null) {
      clueIndex = this.m_scanningComponent.GetAvailableClueIndex();
      if(clueIndex >= 0) {
        arr = this.m_scanningComponent.GetScannableDataForSingleClueByIndex(clueIndex, conclusionData);
        ResolveFocusClueExtendedDescription(clueIndex);
        ResolveFocusClueConclusion(clueIndex, conclusionData);
      };
      if(this.m_scanningComponent.IsObjectDescriptionEnabled()) {
        FillObjectDescription(arr);
      };
      if(IsScannerDataDirty()) {
        CompileScannerChunks();
        SetScannerDirty(false);
      };
    };
    return arr;
  }

  public const Bool ShouldShowScanner() {
    if(!ToBool(this.m_scanningComponent)) {
      return false;
    };
    if(GetHudManager().IsBraindanceActive() && !this.m_scanningComponent.IsBraindanceClue()) {
      return false;
    };
    if(this.m_scanningComponent.IsBraindanceBlocked() || this.m_scanningComponent.IsPhotoModeBlocked()) {
      return false;
    };
    if(!this.m_scanningComponent.HasValidObjectDescription() && !this.m_scanningComponent.IsAnyClueEnabled() || IsScaningCluesBlocked()) {
      return false;
    };
    return true;
  }

  public const Bool IsScaningCluesBlocked() {
    if(ToBool(this.m_scanningComponent)) {
      return this.m_scanningComponent.IsScanningCluesBlocked();
    };
    return false;
  }

  public final const Bool IsBraindanceBlocked() {
    if(ToBool(this.m_scanningComponent)) {
      return this.m_scanningComponent.IsBraindanceBlocked();
    };
    return false;
  }

  public final const Bool IsPhotoModeBlocked() {
    return GetPhotoModeSystem(GetGame()).IsPhotoModeActive();
  }

  private final void ResolveFocusClueExtendedDescription(Int32 clueIndex) {
    Int32 i;
    Int32 k;
    array<ClueRecordData> clueRecords;
    if(this.m_scanningComponent != null) {
      clueRecords = this.m_scanningComponent.GetExtendedClueRecords(clueIndex);
      i = 0;
      while(i < Size(clueRecords)) {
        if(!clueRecords[i].wasInspected && clueRecords[i].percentage >= this.m_scanningComponent.GetScanningProgress()) {
          ResolveFacts(clueRecords[i].facts);
          this.m_scanningComponent.SetClueExtendedDescriptionAsInspected(clueIndex, i);
        };
        i += 1;
      };
    };
  }

  private final void ResolveFocusClueConclusion(Int32 clueIndex, ScanningTooltipElementDef conclusionData) {
    FocusClueDefinition clue;
    if(this.m_scanningComponent != null) {
      if(clueIndex < 0) {
        return ;
      };
      if(!IsValid(conclusionData.recordID)) {
        return ;
      };
      if(!this.m_scanningComponent.WasConclusionShown(clueIndex) && conclusionData.timePct >= this.m_scanningComponent.GetScanningProgress()) {
        clue = this.m_scanningComponent.GetClueByIndex(clueIndex);
        ResolveFacts(clue.facts);
        this.m_scanningComponent.SetConclusionAsShown(clueIndex);
      };
    };
  }

  protected final void ResolveFacts(array<SFactOperationData> facts) {
    Int32 i;
    i = 0;
    while(i < Size(facts)) {
      if(IsNameValid(facts[i].factName)) {
        if(facts[i].operationType == EMathOperationType.Add) {
          AddFact(GetGame(), facts[i].factName, facts[i].factValue);
        } else {
          SetFactValue(GetGame(), facts[i].factName, facts[i].factValue);
        };
      };
      i += 1;
    };
  }

  public final const ref<FocusCluesSystem> GetFocusClueSystem() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("FocusCluesSystem"));
  }

  public final const Bool IsAnyClueEnabled() {
    if(ToBool(this.m_scanningComponent)) {
      return this.m_scanningComponent.IsAnyClueEnabled();
    };
    return false;
  }

  protected final const Bool IsCurrentTarget() {
    ref<GameObject> lookedAtObect;
    lookedAtObect = GetTargetingSystem(GetGame()).GetLookAtObject(RefToWeakRef(Cast(GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject())));
    if(lookedAtObect == null) {
      return false;
    };
    return lookedAtObect.GetEntityID() == GetEntityID();
  }

  protected final const Bool IsCurrentlyScanned() {
    ref<IBlackboard> blackBoard;
    EntityID entityID;
    blackBoard = GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().UI_Scanner);
    entityID = blackBoard.GetEntityID(GetAllBlackboardDefs().UI_Scanner.ScannedObject);
    return GetEntityID() == entityID;
  }

  public const NodeRef GetFreeWorkspotRefForAIAction(gamedataWorkspotActionType aiAction) {
    NodeRef worskpotRef;
    if(this.m_workspotMapper != null) {
      worskpotRef = this.m_workspotMapper.GetFreeWorkspotRefForAIAction(aiAction);
    };
    return worskpotRef;
  }

  public const ref<WorkspotEntryData> GetFreeWorkspotDataForAIAction(gamedataWorkspotActionType aiAction) {
    ref<WorkspotEntryData> worskpotData;
    if(this.m_workspotMapper != null) {
      worskpotData = this.m_workspotMapper.GetFreeWorkspotDataForAIAction(aiAction);
    };
    return worskpotData;
  }

  public const Bool HasFreeWorkspotForInvestigation() {
    ref<WorkspotEntryData> worskpotData;
    if(this.m_workspotMapper != null) {
      worskpotData = this.m_workspotMapper.GetFreeWorkspotDataForAIAction(gamedataWorkspotActionType.DeviceInvestigation);
    };
    return worskpotData != null;
  }

  public const Int32 GetFreeWorkspotsCountForAIAction(gamedataWorkspotActionType aiAction) {
    Int32 numberOfWorkspots;
    if(this.m_workspotMapper != null) {
      numberOfWorkspots = this.m_workspotMapper.GetFreeWorkspotsCountForAIAction(aiAction);
    };
    return numberOfWorkspots;
  }

  public const Int32 GetNumberOfWorkpotsForAIAction(gamedataWorkspotActionType aiAction) {
    Int32 numberOfWorkspots;
    if(this.m_workspotMapper != null) {
      numberOfWorkspots = this.m_workspotMapper.GetNumberOfWorkpotsForAIAction(aiAction);
    };
    return numberOfWorkspots;
  }

  public final const Int32 GetTotalCountOfInvestigationSlots() {
    Int32 count;
    count = GetNumberOfWorkpotsForAIAction(gamedataWorkspotActionType.DeviceInvestigation);
    if(count == 0) {
      count = 1;
    };
    return count;
  }

  public final const ref<StimBroadcasterComponent> GetStimBroadcasterComponent() {
    return this.m_stimBroadcaster;
  }

  public final const ref<SlotComponent> GetUISlotComponent() {
    return this.m_uiSlotComponent;
  }

  public final const ref<SquadMemberBaseComponent> GetSquadMemberComponent() {
    return this.m_squadMemberComponent;
  }

  public final const ref<StatusEffectComponent> GetStatusEffectComponent() {
    return this.m_statusEffectComponent;
  }

  public final const ref<SourceShootComponent> GetSourceShootComponent() {
    return this.m_sourceShootComponent;
  }

  public final const ref<TargetShootComponent> GetTargetShootComponent() {
    return this.m_targetShootComponent;
  }

  public final native void ReplicateAnimFeature(ref<GameObject> obj, CName inputName, ref<AnimFeature> value)

  public final void OnAnimFeatureReplicated(CName inputName, ref<AnimFeature> value) {
    ApplyFeature(this, inputName, value);
  }

  public final native void ReplicateAnimEvent(ref<GameObject> obj, CName eventName)

  public final void OnAnimEventReplicated(CName eventName) {
    PushEvent(this, eventName);
  }

  public final native void ReplicateInputFloat(ref<GameObject> obj, CName inputName, Float value)

  public final native void ReplicateInputBool(ref<GameObject> obj, CName inputName, Bool value)

  public final native void ReplicateInputInt(ref<GameObject> obj, CName inputName, Int32 value)

  public final native void ReplicateInputVector(ref<GameObject> obj, CName inputName, Vector4 value)

  public const Vector4 GetPlaystyleMappinLocalPos() {
    Vector4 pos;
    return pos;
  }

  public const Vector4 GetPlaystyleMappinSlotWorldPos() {
    return GetWorldPosition();
  }

  public const WorldTransform GetPlaystyleMappinSlotWorldTransform() {
    WorldTransform transform;
    SetPosition(transform, GetWorldPosition());
    SetOrientation(transform, GetWorldOrientation());
    return transform;
  }

  public const FxResource GetFxResourceByKey(CName key) {
    FxResource resource;
    return resource;
  }

  protected cb Bool OnDelayPrereqEvent(ref<DelayPrereqEvent> evt) {
    evt.m_state.UpdatePrereq();
  }

  protected cb Bool OnTriggerAttackEffectorWithDelay(ref<TriggerAttackEffectorWithDelay> evt) {
    if(ToBool(evt.attack)) {
      evt.attack.StartAttack();
    };
  }

  protected cb Bool OnToggleOffMeshConnections(ref<ToggleOffMeshConnections> evt) {
    if(evt.enable) {
      EnableOffMeshConnections(evt.affectsPlayer, evt.affectsNPCs);
    } else {
      DisableOffMeshConnections(evt.affectsPlayer, evt.affectsNPCs);
    };
  }

  protected void EnableOffMeshConnections(Bool player, Bool npc)

  protected void DisableOffMeshConnections(Bool player, Bool npc)

  protected cb Bool OnPhysicalDestructionEvent(ref<PhysicalDestructionEvent> evt) {
    if(ToBool(this.m_stimBroadcaster)) {
      this.m_stimBroadcaster.TriggerSingleBroadcast(RefToWeakRef(this), gamedataStimType.SoundDistraction);
    };
  }

  public final const ref<HUDManager> GetHudManager() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("HUDManager"));
  }

  protected final void TriggerMenuEvent(CName eventName) {
    CName currentEventName;
    ref<IBlackboard> blackboard;
    blackboard = GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().MenuEventBlackboard);
    if(ToBool(blackboard)) {
      currentEventName = blackboard.GetName(GetAllBlackboardDefs().MenuEventBlackboard.MenuEventToTrigger);
      if(IsNameValid(currentEventName)) {
        blackboard.SetName(GetAllBlackboardDefs().MenuEventBlackboard.MenuEventToTrigger, "");
      };
      blackboard.SetName(GetAllBlackboardDefs().MenuEventBlackboard.MenuEventToTrigger, eventName);
    };
  }

  public const Vector4 GetAcousticQuerryStartPoint() {
    return GetWorldPosition();
  }

  public const Bool CanBeInvestigated() {
    return true;
  }

  public final static Bool IsVehicle(wref<GameObject> object) {
    return Cast(WeakRefToRef(object)) != null;
  }

  public final const ref<PreventionSystem> GetPreventionSystem() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("PreventionSystem"));
  }

  public const gamedataQuality GetLootQuality() {
    return gamedataQuality.Invalid;
  }

  public const Bool GetIsIconic() {
    return false;
  }

  public final const ref<AnimationSystemForcedVisibilityManager> GetAnimationSystemForcedVisibilityManager() {
    return Cast(GetScriptableSystemsContainer(GetGame()).Get("AnimationSystemForcedVisibilityManager"));
  }

  public final static void ToggleForcedVisibilityInAnimSystemEvent(ref<GameObject> owner, CName sourceName, Bool isVisibe, Float transitionTime?) {
    ref<ToggleVisibilityInAnimSystemEvent> evt;
    if(owner == null || !IsNameValid(sourceName)) {
      return ;
    };
    evt = new ToggleVisibilityInAnimSystemEvent();
    evt.isVisible = isVisibe;
    evt.sourceName = sourceName;
    evt.transitionTime = transitionTime;
    owner.QueueEvent(evt);
  }

  protected final void ToggleForcedVisibilityInAnimSystem(CName sourceName, Bool isVisibe, Float transitionTime?, EntityID entityID?, Bool forcedVisibleOnlyInFrustum?) {
    ref<ToggleVisibilityInAnimSystemRequest> request;
    if(!IsNameValid(sourceName)) {
      return ;
    };
    request = new ToggleVisibilityInAnimSystemRequest();
    request.isVisible = isVisibe;
    request.sourceName = sourceName;
    request.transitionTime = transitionTime;
    request.forcedVisibleOnlyInFrustum = forcedVisibleOnlyInFrustum;
    if(IsDefined(entityID)) {
      request.entityID = entityID;
    } else {
      request.entityID = GetEntityID();
    };
    this.m_hasVisibilityForcedInAnimSystem = isVisibe;
    GetAnimationSystemForcedVisibilityManager().QueueRequest(request);
  }

  protected final void ClearForcedVisibilityInAnimSystem() {
    ref<ClearVisibilityInAnimSystemRequest> request;
    request = new ClearVisibilityInAnimSystemRequest();
    request.entityID = GetEntityID();
    GetAnimationSystemForcedVisibilityManager().QueueRequest(request);
    this.m_hasVisibilityForcedInAnimSystem = false;
  }

  protected final Bool HasVisibilityForcedInAnimSystem() {
    return this.m_hasVisibilityForcedInAnimSystem || GetAnimationSystemForcedVisibilityManager().HasVisibilityForced(GetEntityID());
  }

  protected cb Bool OnToggleVisibilityInAnimSystemEvent(ref<ToggleVisibilityInAnimSystemEvent> evt) {
    ToggleForcedVisibilityInAnimSystem(evt.sourceName, evt.isVisible, evt.transitionTime);
  }

  protected cb Bool OnSetGlitchOnUIEvent(ref<SetGlitchOnUIEvent> evt) {
    ref<AdvertGlitchEvent> glitchEvt;
    glitchEvt = new AdvertGlitchEvent();
    glitchEvt.SetShouldGlitch(evt.intensity);
    QueueEvent(glitchEvt);
  }

  protected cb Bool OnCustomUIAnimationEvent(ref<CustomUIAnimationEvent> evt) {
    evt.ownerID = GetEntityID();
    GetUISystem(GetGame()).QueueEvent(evt);
  }

  protected cb Bool OnSmartGunLockEvent(ref<SmartGunLockEvent> evt) {
    if(evt.lockedOnByPlayer) {
      SendForceRevealObjectEvent(evt.locked, "SmartGunLock");
    };
  }
}

public static String OperatorAdd(String s, ref<GameObject> o) {
  return s + o.GetDisplayName();
}

public static String OperatorAdd(ref<GameObject> o, String s) {
  return o.GetDisplayName() + s;
}
