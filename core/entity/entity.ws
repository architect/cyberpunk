
public class Entity extends IScriptable {

  public final native const EntityGameInterface GetEntity()

  public final native void QueueEvent(ref<Event> evt)

  public final native const EntityID GetEntityID()

  public final native Bool QueueEventForNodeID(GlobalNodeRef nodeID, ref<Event> evt)

  public final native Bool QueueEventForEntityID(EntityID entityID, ref<Event> evt)

  public final native const Bool CanServiceEvent(CName evtName)

  public final native const Bool IsReplicated()

  public final native const Uint32 GetControllingPeerID()

  public final native const Bool MatchVisualTag(CName visualTag)

  public final native const Bool MatchVisualTags(array<CName> visualTags)

  public final native const Bool IsControlledByAnyPeer()

  public final native const Bool IsControlledByLocalPeer()

  public final native const Bool IsControlledByAnotherClient()

  public native const Bool IsAttached()

  protected final native const ref<IComponent> FindComponentByName(CName componentName)

  public final native void PrefetchAppearanceChange(CName newAppearanceName)

  public final native void ScheduleAppearanceChange(CName newAppearanceName)

  public final native const CName GetCurrentAppearanceName()

  public void OnInspectorDebugDraw(out ref<InfoBox> box)

  public final native const Vector4 GetWorldPosition()

  public final native const Quaternion GetWorldOrientation()

  public final native const Float GetWorldYaw()

  public final native const Vector4 GetWorldForward()

  public final native const Vector4 GetWorldRight()

  public final native const Vector4 GetWorldUp()

  public final const WorldTransform GetWorldTransform() {
    WorldTransform worldTransform;
    WorldPosition worldPosition;
    SetVector4(worldPosition, GetWorldPosition());
    SetWorldPosition(worldTransform, worldPosition);
    SetOrientation(worldTransform, GetWorldOrientation());
    return worldTransform;
  }
}
