
public final class gameDoorComponent extends IComponent {

  public final native Bool IsInteractible()

  public final native Bool IsAutomatic()

  public final native Bool IsPhysical()

  public final native Bool IsOpen()

  public final native Bool IsLocked()

  public final native Bool IsSealed()

  public final native Bool IsOffline()

  public final native Float GetOpeningSpeed()

  public final native Bool SetOpen(Bool newVal)

  public final native Bool SetLocked(Bool newVal)

  public final native Bool SetSealed(Bool newVal)

  public final native Bool SetOffline(Bool newVal)

  public final void ToggleOpen() {
    SetOpen(!IsOpen());
  }
}
