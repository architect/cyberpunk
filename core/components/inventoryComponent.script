
public class InventoryScriptCallback extends IScriptable {

  public native ItemID itemID;

  public void OnItemNotification(ItemID itemID, wref<gameItemData> itemData)

  public void OnItemAdded(ItemID itemID, wref<gameItemData> itemData, Bool flaggedAsSilent)

  public void OnItemRemoved(ItemID itemID, Int32 difference, Int32 currentQuantity)

  public void OnItemQuantityChanged(ItemID itemID, Int32 diff, Uint32 total, Bool flaggedAsSilent)

  public void OnItemExtracted(ItemID itemID)

  public void OnPartAdded(ItemID itemID, ItemID partID)

  public void OnPartRemoved(ItemID partID, ItemID formerItemID)
}

public final class Inventory extends GameComponent {

  public final native Bool IsAccessible()

  public final native Bool ReinitializeStatsOnAllItems()

  public final gameinteractionsELootChoiceType IsChoiceAvailable(ref<ItemAction_Record> itemActionRecord, ref<GameObject> requester, EntityID ownerEntID, ItemID itemID) {
    ref<BaseItemAction> action;
    wref<gameItemData> itemData;
    GetActionsContext emptyContext;
    itemData = GetItemData(requester.GetGame(), Cast(WeakRefToRef(GetEntity())), itemID);
    action = SetupItemAction(requester.GetGame(), RefToWeakRef(requester), itemData, itemActionRecord.GetID(), false);
    if(action.IsVisible(emptyContext)) {
      return gameinteractionsELootChoiceType.Available;
    };
    return gameinteractionsELootChoiceType.Invisible;
  }

  protected final cb Bool OnLootAllEvent(ref<OnLootAllEvent> evt) {
    ref<GameObject> gameObject;
    gameObject = Cast(WeakRefToRef(GetEntity()));
    GetAudioSystem(gameObject.GetGame()).PlayLootAllSound();
  }

  protected final cb Bool OnInteractionUsed(ref<InteractionChoiceEvent> evt) {
    Int32 itemQuantity;
    LootChoiceActionWrapper lootActionWrapper;
    ref<GameObject> gameObject;
    ref<StimBroadcasterComponent> broadcaster;
    gameObject = Cast(WeakRefToRef(GetEntity()));
    lootActionWrapper = Unwrap(evt);
    if(IsValid(lootActionWrapper)) {
      if(IsIllegal(lootActionWrapper)) {
        broadcaster = WeakRefToRef(evt.activator).GetStimBroadcasterComponent();
        if(ToBool(broadcaster)) {
          broadcaster.TriggerSingleBroadcast(RefToWeakRef(gameObject), gamedataStimType.IllegalInteraction);
        };
      };
      if(ConsumeItem(RefToWeakRef(gameObject), evt)) {
        GetAudioSystem(gameObject.GetGame()).PlayItemActionSound(lootActionWrapper.action, GetItemData(gameObject.GetGame(), gameObject, lootActionWrapper.itemId));
        return false;
      };
      if(lootActionWrapper.action == "Learn") {
        GetTransactionSystem(gameObject.GetGame()).GiveItem(WeakRefToRef(evt.activator), lootActionWrapper.itemId, 1);
        LearnItem(evt.activator, lootActionWrapper.itemId, false);
      };
      if(IsHandledByCode(lootActionWrapper) == false) {
        GetTransactionSystem(gameObject.GetGame()).RemoveItem(gameObject, lootActionWrapper.itemId, 1);
      };
      GetAudioSystem(gameObject.GetGame()).PlayItemActionSound(lootActionWrapper.action, GetItemData(gameObject.GetGame(), gameObject, lootActionWrapper.itemId));
    };
  }
}

public class gameLootObject extends GameObject {

  protected Bool m_isInIconForcedVisibilityRange;

  protected CName m_activeQualityRangeInteraction;

  protected cb Bool OnInteractionActivated(ref<InteractionEvent> choiceEvent)

  protected final Bool IsQualityRangeInteractionLayer(CName layerTag) {
    return layerTag == "QualityRange_Short" || layerTag == "QualityRange_Medium" || layerTag == "QualityRange_Max";
  }

  protected final void SetQualityRangeInteractionLayerState(Bool enable) {
    ref<InteractionSetEnableEvent> evt;
    if(IsNameValid(this.m_activeQualityRangeInteraction)) {
      evt = new InteractionSetEnableEvent();
      evt.enable = enable;
      evt.layer = this.m_activeQualityRangeInteraction;
      QueueEvent(evt);
    };
  }

  protected final void ResolveQualityRangeInteractionLayer(wref<gameItemData> itemData?) {
    CName currentLayer;
    Bool isQuest;
    gamedataQuality lootQuality;
    if(IsNameValid(this.m_activeQualityRangeInteraction)) {
      SetQualityRangeInteractionLayerState(false);
    };
    if(WeakRefToRef(itemData) == null) {
      return ;
    };
    lootQuality = GetItemDataQuality(itemData);
    isQuest = WeakRefToRef(itemData).HasTag("Quest");
    if(lootQuality != gamedataQuality.Invalid && lootQuality != gamedataQuality.Random) {
      if(isQuest) {
        currentLayer = "QualityRange_Max";
      } else {
        if(lootQuality == gamedataQuality.Common) {
          currentLayer = "QualityRange_Short";
        } else {
          if(lootQuality == gamedataQuality.Uncommon) {
            currentLayer = "QualityRange_Medium";
          } else {
            if(lootQuality == gamedataQuality.Rare) {
              currentLayer = "QualityRange_Medium";
            } else {
              if(lootQuality == gamedataQuality.Epic) {
                currentLayer = "QualityRange_Max";
              } else {
                if(lootQuality == gamedataQuality.Legendary) {
                  currentLayer = "QualityRange_Max";
                } else {
                  if(lootQuality == gamedataQuality.Iconic) {
                    currentLayer = "QualityRange_Max";
                  };
                };
              };
            };
          };
        };
      };
    } else {
      currentLayer = "";
    };
    this.m_activeQualityRangeInteraction = currentLayer;
    SetQualityRangeInteractionLayerState(true);
  }

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "Collider", "entColliderComponent", false);
  }

  public const Bool IsInIconForcedVisibilityRange() {
    return this.m_isInIconForcedVisibilityRange;
  }
}

public final class gameItemDropObject extends gameLootObject {

  protected Bool m_wasItemInitialized;

  public final native const EntityID GetItemEntityID()

  public final native const wref<ItemObject> GetItemObject()

  protected final void OnItemEntitySpawned(EntityID entID) {
    SetQualityRangeInteractionLayerState(true);
    EvaluateLootQualityEvent(entID);
    RequestHUDRefresh();
  }

  protected final cb Bool OnGameAttached() {
    if(ShouldRegisterToHUD()) {
      RegisterToHUDManager(true);
    };
  }

  protected final cb Bool OnInteractionActivated(ref<InteractionEvent> choiceEvent) {
    ref<HUDActorUpdateData> actorUpdateData;
    OnInteractionActivated(choiceEvent);
    if(choiceEvent.eventType == gameinteractionsEInteractionEventType.EIET_activate) {
      if(WeakRefToRef(choiceEvent.activator).IsPlayer()) {
        if(IsQualityRangeInteractionLayer(choiceEvent.layerData.tag)) {
          this.m_isInIconForcedVisibilityRange = true;
          actorUpdateData = new HUDActorUpdateData();
          actorUpdateData.updateIsInIconForcedVisibilityRange = true;
          actorUpdateData.isInIconForcedVisibilityRangeValue = true;
          RequestHUDRefresh(actorUpdateData);
        };
      };
    } else {
      if(IsQualityRangeInteractionLayer(choiceEvent.layerData.tag) && WeakRefToRef(choiceEvent.activator).IsPlayer()) {
        this.m_isInIconForcedVisibilityRange = false;
        actorUpdateData = new HUDActorUpdateData();
        actorUpdateData.updateIsInIconForcedVisibilityRange = true;
        actorUpdateData.isInIconForcedVisibilityRangeValue = false;
        RequestHUDRefresh(actorUpdateData);
      };
    };
  }

  public final const Bool IsEmpty() {
    return !IsDefined(GetItemEntityID());
  }

  public final const Bool ShouldRegisterToHUD() {
    return true;
  }

  protected final cb Bool OnHUDInstruction(ref<HUDInstruction> evt) {
    if(this.m_wasItemInitialized || !IsEmpty()) {
      QueueEventForEntityID(GetItemEntityID(), evt);
    };
  }

  protected final cb Bool OnItemRemovedEvent(ref<ItemBeingRemovedEvent> evt) {
    ref<ItemLootedEvent> evtToSend;
    RegisterToHUDManager(false);
    if(!IsEmpty()) {
      evtToSend = new ItemLootedEvent();
      QueueEventForEntityID(GetItemEntityID(), evtToSend);
    };
    SetQualityRangeInteractionLayerState(false);
  }

  protected final cb Bool OnItemAddedEvent(ref<ItemAddedEvent> evt) {
    wref<gameItemData> itemData;
    this.m_wasItemInitialized = true;
    itemData = GetTransactionSystem(GetGame()).GetItemData(this, evt.itemID);
    ResolveQualityRangeInteractionLayer(itemData);
  }

  private final void EvaluateLootQualityEvent(EntityID target) {
    ref<EvaluateLootQualityEvent> evt;
    if(IsDefined(target)) {
      evt = new EvaluateLootQualityEvent();
      GetPersistencySystem(GetGame()).QueueEntityEvent(target, evt);
    };
  }
}
