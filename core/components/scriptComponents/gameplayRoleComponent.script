
public class SetGameplayRoleEvent extends Event {

  public EGameplayRole gameplayRole;

  public final String GetFriendlyDescription() {
    return "Set Gameplay Role";
  }
}

public class ToggleGameplayMappinVisibilityEvent extends Event {

  public Bool isHidden;

  public final String GetFriendlyDescription() {
    return "Toggle Gameplay Mappin Visibility";
  }
}

public class GameplayRoleComponent extends ScriptableComponent {

  [Attrib(category, "Gameplay Role")]
  [Default(GameplayRoleComponent, EGameplayRole.UnAssigned))]
  private EGameplayRole m_gameplayRole;

  [Attrib(category, "Gameplay Role")]
  [Default(GameplayRoleComponent, true))]
  private Bool m_autoDeterminGameplayRole;

  [Default(GameplayRoleComponent, EMappinDisplayMode.MINIMALISTIC))]
  private EMappinDisplayMode m_mappinsDisplayMode;

  [Default(GameplayRoleComponent, false))]
  private Bool m_displayAllRolesAsGeneric;

  [Default(GameplayRoleComponent, true))]
  private Bool m_alwaysCreateMappinAsDynamic;

  private array<SDeviceMappinData> m_mappins;

  [Default(GameplayRoleComponent, 0.04f))]
  private Float m_offsetValue;

  private Bool m_isBeingScanned;

  private Bool m_isCurrentTarget;

  private Bool m_isShowingMappins;

  private Bool m_isHighlightedInFocusMode;

  [Default(GameplayRoleComponent, EGameplayRole.UnAssigned))]
  private EGameplayRole m_currentGameplayRole;

  private Bool m_isGameplayRoleInitialized;

  private Bool m_isForceHidden;

  private Bool m_isForcedVisibleThroughWalls;

  protected final void OnGameAttach() {
    this.m_currentGameplayRole = this.m_gameplayRole;
    DeterminGamplayRole();
    InitializeQuickHackIndicator();
    InitializePhoneCallIndicator();
  }

  protected final void OnGameDetach() {
    UnregisterAllMappins();
  }

  protected cb Bool OnSetGameplayRole(ref<SetGameplayRoleEvent> evt) {
    this.m_gameplayRole = evt.gameplayRole;
    SetCurrentGameplayRoleWithNotification(evt.gameplayRole);
    ReEvaluateGameplayRole();
  }

  protected cb Bool OnSetCurrentGameplayRole(ref<SetCurrentGameplayRoleEvent> evt) {
    SetCurrentGameplayRoleWithNotification(evt.gameplayRole);
    ReEvaluateGameplayRole();
  }

  protected cb Bool OnReEvaluateGameplayRole(ref<EvaluateGameplayRoleEvent> evt) {
    if(!IsGameplayRoleStatic() || evt.force) {
      if(evt.force) {
        this.m_currentGameplayRole = this.m_gameplayRole;
      };
      ReEvaluateGameplayRole();
    };
  }

  private final void SetCurrentGameplayRoleWithNotification(EGameplayRole role) {
    ref<GameplayRoleChangeNotification> evt;
    if(this.m_currentGameplayRole != role) {
      evt = new GameplayRoleChangeNotification();
      evt.oldRole = this.m_currentGameplayRole;
      evt.newRole = role;
      GetOwner().QueueEvent(evt);
    };
    this.m_currentGameplayRole = role;
  }

  protected cb Bool OnLookedAtEvent(ref<LookedAtEvent> evt) {
    this.m_isCurrentTarget = evt.isLookedAt;
  }

  protected cb Bool OnScanningLookedAt(ref<ScanningLookAtEvent> evt) {
    this.m_isBeingScanned = evt.state;
  }

  private final const Bool IsHighlightedInFocusMode() {
    return this.m_isHighlightedInFocusMode;
  }

  protected cb Bool OnLogicReady(ref<SetLogicReadyEvent> evt) {
    RequestHUDRefresh();
  }

  protected cb Bool OnHUDInstruction(ref<HUDInstruction> evt) {
    if(evt.braindanceInstructions.GetState() == InstanceState.ON) {
      if(GetOwner().IsBraindanceBlocked() || GetOwner().IsPhotoModeBlocked()) {
        this.m_isHighlightedInFocusMode = false;
        HideRoleMappins();
        return false;
      };
    };
    this.m_isForcedVisibleThroughWalls = evt.iconsInstruction.isForcedVisibleThroughWalls;
    if(evt.iconsInstruction.GetState() == InstanceState.ON) {
      this.m_isHighlightedInFocusMode = true;
      ShowRoleMappins();
    } else {
      if(evt.highlightInstructions.WasProcessed()) {
        this.m_isHighlightedInFocusMode = false;
        HideRoleMappins();
      };
    };
  }

  protected cb Bool OnUploadProgressStateChanged(ref<UploadProgramProgressEvent> evt) {
    ref<GameplayRoleMappinData> visualData;
    wref<ChoiceCaptionIconPart_Record> iconRecord;
    visualData = new GameplayRoleMappinData();
    visualData.statPoolType = evt.statPoolType;
    if(evt.state == EUploadProgramState.STARTED) {
      visualData.m_mappinVisualState = EMappinVisualState.Default;
      visualData.m_duration = evt.duration;
      visualData.m_progressBarType = evt.progressBarType;
      visualData.m_progressBarContext = evt.progressBarContext;
      visualData.m_visibleThroughWalls = true;
      if(evt.progressBarContext == EProgressBarContext.QuickHack) {
        iconRecord = evt.action.GetInteractionIcon();
        if(ToBool(iconRecord)) {
          visualData.m_textureID = WeakRefToRef(WeakRefToRef(iconRecord).TexturePartID()).GetID();
        };
        ActivateQuickHackIndicator(visualData);
      } else {
        if(evt.progressBarContext == EProgressBarContext.PhoneCall) {
          iconRecord = evt.iconRecord;
          if(ToBool(iconRecord)) {
            visualData.m_textureID = WeakRefToRef(WeakRefToRef(iconRecord).TexturePartID()).GetID();
          };
          ActivatePhoneCallIndicator(visualData);
        };
      };
    } else {
      if(evt.state == EUploadProgramState.COMPLETED) {
        if(evt.progressBarContext == EProgressBarContext.QuickHack) {
          DeactivateQuickHackIndicator();
        } else {
          if(evt.progressBarContext == EProgressBarContext.PhoneCall) {
            DeactivatePhoneCallIndicator();
          };
        };
      };
    };
  }

  protected cb Bool OnPerformedAction(ref<PerformedAction> evt) {
    ref<ScriptableDeviceAction> action;
    action = Cast(evt.m_action);
    EvaluateMappins();
  }

  private final void ActivateQuickHackIndicator(ref<GameplayRoleMappinData> visualData) {
    HideRoleMappins();
    ToggleMappin(gamedataMappinVariant.QuickHackVariant, true, true, visualData);
  }

  private final void DeactivateQuickHackIndicator() {
    ToggleMappin(gamedataMappinVariant.QuickHackVariant, false);
    RequestHUDRefresh();
  }

  protected cb Bool OnDeactivateQuickHackIndicator(ref<DeactivateQuickHackIndicatorEvent> evt) {
    DeactivateQuickHackIndicator();
  }

  private final void ActivatePhoneCallIndicator(ref<GameplayRoleMappinData> visualData) {
    ToggleMappin(gamedataMappinVariant.PhoneCallVariant, true, true, visualData);
  }

  private final void DeactivatePhoneCallIndicator() {
    ToggleMappin(gamedataMappinVariant.PhoneCallVariant, false);
  }

  protected cb Bool OnEvaluateMappinVisualStateEvent(ref<EvaluateMappinsVisualStateEvent> evt) {
    if(this.m_isShowingMappins) {
      HideRoleMappins();
      ShowRoleMappins();
    };
  }

  protected cb Bool OnShowSingleMappin(ref<ShowSingleMappinEvent> evt) {
    ShowSingleMappin(evt.index);
  }

  protected cb Bool OnHideSingleMappin(ref<HideSingleMappinEvent> evt) {
    HideSingleMappin(evt.index);
  }

  private final void DeterminGamplayRole() {
    if(this.m_autoDeterminGameplayRole && this.m_currentGameplayRole == EGameplayRole.UnAssigned) {
      this.m_currentGameplayRole = GetOwner().DeterminGameplayRole();
    };
    if(this.m_currentGameplayRole != EGameplayRole. && this.m_currentGameplayRole != EGameplayRole.UnAssigned) {
      InitializeGamepleyRoleMappin();
    };
  }

  private final void InitializeQuickHackIndicator() {
    SDeviceMappinData mappin;
    mappin.mappinType = "Mappins.DeviceMappinDefinition";
    mappin.enabled = false;
    mappin.active = false;
    mappin.permanent = true;
    mappin.checkIfIsTarget = false;
    mappin.mappinVariant = gamedataMappinVariant.QuickHackVariant;
    mappin.gameplayRole = EGameplayRole.;
    AddMappin(mappin);
  }

  private final void InitializePhoneCallIndicator() {
    SDeviceMappinData mappin;
    mappin.mappinType = "Mappins.DeviceMappinDefinition";
    mappin.enabled = false;
    mappin.active = false;
    mappin.permanent = true;
    mappin.checkIfIsTarget = false;
    mappin.mappinVariant = gamedataMappinVariant.PhoneCallVariant;
    mappin.gameplayRole = EGameplayRole.;
    AddMappin(mappin);
  }

  private final void InitializeGamepleyRoleMappin() {
    if(GetOwner().IsAttached()) {
      if(this.m_currentGameplayRole == EGameplayRole.UnAssigned || this.m_currentGameplayRole == EGameplayRole.) {
        this.m_currentGameplayRole = GetOwner().DeterminGameplayRole();
      };
      this.m_isGameplayRoleInitialized = AddMappin(GetMappinDataForGamepleyRole(this.m_currentGameplayRole));
    };
  }

  private final const SDeviceMappinData GetMappinDataForGamepleyRole(EGameplayRole role) {
    SDeviceMappinData mappin;
    if(role != EGameplayRole. && role != EGameplayRole.UnAssigned || !HasMappin(role)) {
      mappin.enabled = false;
      mappin.active = false;
      mappin.range = 35;
      mappin.mappinVariant = GetCurrentMappinVariant(role);
      mappin.mappinType = "Mappins.DeviceMappinDefinition";
    };
    mappin.gameplayRole = role;
    return mappin;
  }

  private final const gamedataMappinVariant GetCurrentMappinVariant(EGameplayRole role) {
    gamedataMappinVariant mappinVariant;
    if(this.m_displayAllRolesAsGeneric) {
      mappinVariant = gamedataMappinVariant.GenericRoleVariant;
    } else {
      if(this.m_mappinsDisplayMode == EMappinDisplayMode.PLAYSTYLE) {
        mappinVariant = GetPlaystyleMappinVariant();
      } else {
        if(this.m_mappinsDisplayMode == EMappinDisplayMode.ROLE) {
          mappinVariant = GetRoleMappinVariant(role);
        } else {
          if(this.m_mappinsDisplayMode == EMappinDisplayMode.MINIMALISTIC) {
            mappinVariant = GetMinimalisticMappinVariant();
          };
        };
      };
    };
    return mappinVariant;
  }

  private final const gamedataMappinVariant GetMinimalisticMappinVariant() {
    gamedataMappinVariant mappinVariant;
    if(GetOwner().IsAnyClueEnabled()) {
      mappinVariant = gamedataMappinVariant.FocusClueVariant;
    } else {
      if(GetOwner().IsContainer()) {
        mappinVariant = gamedataMappinVariant.LootVariant;
      } else {
        if(!GetOwner().IsActive()) {
          mappinVariant = gamedataMappinVariant.Invalid;
        } else {
          if(GetOwner().IsNPC()) {
            if(GetOwner().IsInvestigating() || GetOwner().HasHighlight(EFocusForcedHighlightType.INVALID, EFocusOutlineType.DISTRACTION)) {
              mappinVariant = gamedataMappinVariant.EffectDistractVariant;
            } else {
              if(GetOwner().IsHackingPlayer()) {
                mappinVariant = gamedataMappinVariant.NetrunnerVariant;
              } else {
                if(GetOwner().IsActiveBackdoor()) {
                  mappinVariant = gamedataMappinVariant.EffectControlNetworkVariant;
                } else {
                  mappinVariant = gamedataMappinVariant.Invalid;
                };
              };
            };
          } else {
            if(GetOwner().IsBodyDisposalPossible()) {
              mappinVariant = gamedataMappinVariant.EffectHideBodyVariant;
            } else {
              if(GetOwner().IsActiveBackdoor()) {
                mappinVariant = gamedataMappinVariant.EffectControlNetworkVariant;
              } else {
                if(GetOwner().IsExplosive()) {
                  mappinVariant = gamedataMappinVariant.EffectExplodeLethalVariant;
                } else {
                  if(ToBool(Cast(GetOwner())) && !GetOwner().IsQuickHackAble()) {
                    mappinVariant = gamedataMappinVariant.EffectHideBodyVariant;
                  } else {
                    if(GetOwner().HasImportantInteraction()) {
                      mappinVariant = gamedataMappinVariant.ImportantInteractionVariant;
                    } else {
                      if(GetOwner().IsControllingDevices()) {
                        mappinVariant = gamedataMappinVariant.EffectControlOtherDeviceVariant;
                      } else {
                        if(GetOwner().HasAnyDirectInteractionActive()) {
                          mappinVariant = gamedataMappinVariant.GenericRoleVariant;
                        } else {
                          mappinVariant = gamedataMappinVariant.Invalid;
                        };
                      };
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
    return mappinVariant;
  }

  private final const gamedataMappinVariant GetPlaystyleMappinVariant() {
    gamedataMappinVariant mappinVariant;
    if(GetOwner().IsAnyClueEnabled()) {
      mappinVariant = gamedataMappinVariant.FocusClueVariant;
    } else {
      if(GetOwner().IsNetrunner() && GetOwner().IsSolo() && GetOwner().IsTechie()) {
        mappinVariant = gamedataMappinVariant.NetrunnerSoloTechieVariant;
      } else {
        if(GetOwner().IsNetrunner() && GetOwner().IsSolo()) {
          mappinVariant = gamedataMappinVariant.NetrunnerSoloVariant;
        } else {
          if(GetOwner().IsNetrunner() && GetOwner().IsTechie()) {
            mappinVariant = gamedataMappinVariant.NetrunnerTechieVariant;
          } else {
            if(GetOwner().IsSolo() && GetOwner().IsTechie()) {
              mappinVariant = gamedataMappinVariant.SoloTechieVariant;
            } else {
              if(GetOwner().IsNetrunner()) {
                mappinVariant = gamedataMappinVariant.NetrunnerVariant;
              } else {
                if(GetOwner().IsTechie()) {
                  mappinVariant = gamedataMappinVariant.TechieVariant;
                } else {
                  if(GetOwner().IsSolo()) {
                    mappinVariant = gamedataMappinVariant.SoloVariant;
                  } else {
                    mappinVariant = gamedataMappinVariant.GenericRoleVariant;
                  };
                };
              };
            };
          };
        };
      };
    };
    return mappinVariant;
  }

  private final const gamedataMappinVariant GetRoleMappinVariant(EGameplayRole role) {
    gamedataMappinVariant mappinVariant;
    if(GetOwner().IsAnyClueEnabled()) {
      mappinVariant = gamedataMappinVariant.FocusClueVariant;
    } else {
      switch(role) {
        case EGameplayRole.Alarm:
          mappinVariant = gamedataMappinVariant.EffectAlarmVariant;
          break;
        case EGameplayRole.ControlNetwork:
          mappinVariant = gamedataMappinVariant.EffectControlNetworkVariant;
          break;
        case EGameplayRole.ControlOtherDevice:
          mappinVariant = gamedataMappinVariant.EffectControlOtherDeviceVariant;
          break;
        case EGameplayRole.ControlSelf:
          mappinVariant = gamedataMappinVariant.EffectControlSelfVariant;
          break;
        case EGameplayRole.CutPower:
          mappinVariant = gamedataMappinVariant.EffectCutPowerVariant;
          break;
        case EGameplayRole.Distract:
          mappinVariant = gamedataMappinVariant.EffectDistractVariant;
          break;
        case EGameplayRole.DropPoint:
          mappinVariant = gamedataMappinVariant.EffectDropPointVariant;
          break;
        case EGameplayRole.ExplodeLethal:
          mappinVariant = gamedataMappinVariant.EffectExplodeLethalVariant;
          break;
        case EGameplayRole.ExplodeNoneLethal:
          mappinVariant = gamedataMappinVariant.EffectExplodeNonLethalVariant;
          break;
        case EGameplayRole.Fall:
          mappinVariant = gamedataMappinVariant.EffectFallVariant;
          break;
        case EGameplayRole.FastTravel:
          mappinVariant = gamedataMappinVariant.FastTravelVariant;
          break;
        case EGameplayRole.GrantInformation:
          mappinVariant = gamedataMappinVariant.EffectGrantInformationVariant;
          break;
        case EGameplayRole.Clue:
          mappinVariant = gamedataMappinVariant.FocusClueVariant;
          break;
        case EGameplayRole.HazardWarning:
          mappinVariant = gamedataMappinVariant.HazardWarningVariant;
          break;
        case EGameplayRole.HideBody:
          mappinVariant = gamedataMappinVariant.EffectHideBodyVariant;
          break;
        case EGameplayRole.Loot:
          mappinVariant = gamedataMappinVariant.EffectLootVariant;
          break;
        case EGameplayRole.OpenPath:
          mappinVariant = gamedataMappinVariant.EffectOpenPathVariant;
          break;
        case EGameplayRole.Push:
          mappinVariant = gamedataMappinVariant.EffectPushVariant;
          break;
        case EGameplayRole.ServicePoint:
          mappinVariant = gamedataMappinVariant.EffectServicePointVariant;
          break;
        case EGameplayRole.Shoot:
          mappinVariant = gamedataMappinVariant.EffectShootVariant;
          break;
        case EGameplayRole.SpreadGas:
          mappinVariant = gamedataMappinVariant.EffectSpreadGasVariant;
          break;
        case EGameplayRole.StoreItems:
          mappinVariant = gamedataMappinVariant.EffectStoreItemsVariant;
          break;
        case EGameplayRole.GenericRole:
          mappinVariant = gamedataMappinVariant.GenericRoleVariant;
          break;
        default:
          mappinVariant = gamedataMappinVariant.Invalid;
      };
    };
    return mappinVariant;
  }

  private final Bool HasOffscreenArrow() {
    if(GetOwner().IsNPC() && GetOwner().IsInvestigating() || GetOwner().HasHighlight(EFocusForcedHighlightType.INVALID, EFocusOutlineType.DISTRACTION)) {
      return true;
    };
    return false;
  }

  private final void ReEvaluateGameplayRole() {
    Bool isShowingMappins;
    isShowingMappins = this.m_isShowingMappins;
    ClearAllRoleMappins();
    DeterminGamplayRole();
    if(isShowingMappins) {
      ShowRoleMappins();
    };
    if(GetCurrentGameplayRole() == EGameplayRole.) {
      UpdateDefaultHighlight();
    };
  }

  private final ref<MappinSystem> GetMappinSystem() {
    return GetMappinSystem(GetOwner().GetGame());
  }

  private void EvaluateMappins() {
    Int32 i;
    Bool isRoleValid;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].gameplayRole != EGameplayRole. && this.m_mappins[i].gameplayRole != EGameplayRole.UnAssigned) {
        isRoleValid = GetOwner().IsGameplayRoleValid(this.m_mappins[i].gameplayRole);
        ToggleMappin(i, isRoleValid);
      };
      i += 1;
    };
  }

  private final void EvaluatePositions() {
    EAxisType currentAxis;
    Vector4 currentOffset;
    Vector4 currentPos;
    Vector4 rootPos;
    Float offsetValue;
    WorldTransform slotTransform;
    Int32 i;
    Int32 direction;
    direction = 0;
    slotTransform = GetOwner().GetPlaystyleMappinSlotWorldTransform();
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(!this.m_mappins[i].enabled) {
      } else {
        if(direction != 0) {
          direction *= -1;
          offsetValue += this.m_offsetValue * Cast(direction);
        } else {
          if(direction == 0) {
            offsetValue = 0;
            direction = 1;
          };
        };
        currentOffset = this.m_mappins[i].offset;
        currentOffset.X = currentOffset.X + offsetValue;
        currentPos = ToVector4(TransformPoint(slotTransform, currentOffset));
        this.m_mappins[i].position = currentPos;
        currentOffset = new Vector4(0,0,0,0);
        currentPos = new Vector4(0,0,0,0);
      };
      i += 1;
    };
  }

  private final EAxisType GetNextAxis(EAxisType currentAxis) {
    EAxisType nextAxis;
    Int32 axisValue;
    if(ToInt(currentAxis) < 3) {
      axisValue += 1;
      nextAxis = ToEnum(axisValue);
    } else {
      nextAxis = ToEnum(0);
    };
    return nextAxis;
  }

  public final void ShowRoleMappins() {
    Int32 i;
    MappinData mappinData;
    ref<GameplayRoleMappinData> visualData;
    Bool shouldUpdate;
    Bool shouldUpdateVariant;
    gamedataMappinVariant currentVariant;
    NewMappinID invalidID;
    if(!GetOwner().IsLogicReady()) {
      return ;
    };
    if(HasActiveMappin(gamedataMappinVariant.QuickHackVariant)) {
      return ;
    };
    if(IsForceHidden()) {
      return ;
    };
    if(!this.m_isGameplayRoleInitialized) {
      InitializeGamepleyRoleMappin();
    };
    EvaluateMappins();
    if(!this.m_alwaysCreateMappinAsDynamic) {
      EvaluatePositions();
    };
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(!this.m_mappins[i].enabled) {
      } else {
        if(this.m_mappins[i].gameplayRole == EGameplayRole. || this.m_mappins[i].gameplayRole == EGameplayRole.UnAssigned) {
        } else {
          if(this.m_mappins[i].gameplayRole == EGameplayRole.Loot && GetSceneSystem(GetOwner().GetGame()).GetScriptInterface().IsRewindableSectionActive()) {
          } else {
            this.m_isShowingMappins = true;
            visualData = CreateRoleMappinData(this.m_mappins[i]);
            if(!CompareRoleMappinsData(visualData, this.m_mappins[i].visualStateData)) {
              shouldUpdate = true;
            };
            currentVariant = GetCurrentMappinVariant(this.m_mappins[i].gameplayRole);
            if(currentVariant != this.m_mappins[i].mappinVariant) {
              this.m_mappins[i].mappinVariant = currentVariant;
              shouldUpdate = true;
              shouldUpdateVariant = true;
            };
            if(this.m_mappins[i].id != invalidID && shouldUpdate) {
              UpdateSingleMappinData(i, visualData, shouldUpdateVariant);
              return ;
            };
            if(this.m_mappins[i].id == invalidID) {
              ShowSingleMappin(i);
            } else {
              if(this.m_mappins[i].id != invalidID) {
                ActivateSingleMappin(i);
              };
            };
            i += 1;
          };
        };
      };
    };
  }

  private final ref<GameplayRoleMappinData> CreateRoleMappinData(SDeviceMappinData data) {
    ref<GameplayRoleMappinData> roleMappinData;
    roleMappinData = new GameplayRoleMappinData();
    roleMappinData.m_mappinVisualState = GetOwner().DeterminGameplayRoleMappinVisuaState(data);
    roleMappinData.m_isTagged = GetOwner().IsTaggedinFocusMode();
    roleMappinData.m_isQuest = GetOwner().IsQuest() || GetOwner().IsAnyClueEnabled() && !GetOwner().IsClueInspected();
    roleMappinData.m_visibleThroughWalls = this.m_isForcedVisibleThroughWalls || GetOwner().IsObjectRevealed() || IsCurrentTarget();
    roleMappinData.m_range = GetOwner().DeterminGameplayRoleMappinRange(data);
    roleMappinData.m_isCurrentTarget = IsCurrentTarget();
    roleMappinData.m_gameplayRole = this.m_currentGameplayRole;
    roleMappinData.m_braindanceLayer = GetOwner().GetBraindanceLayer();
    roleMappinData.m_quality = GetOwner().GetLootQuality();
    roleMappinData.m_isIconic = GetOwner().GetIsIconic();
    roleMappinData.m_hasOffscreenArrow = HasOffscreenArrow();
    roleMappinData.m_isScanningCluesBlocked = GetOwner().IsAnyClueEnabled() && GetOwner().IsScaningCluesBlocked();
    roleMappinData.m_textureID = GetIconIdForMappinVariant(data.mappinVariant);
    return roleMappinData;
  }

  private final Bool CompareRoleMappinsData(ref<GameplayRoleMappinData> data1, ref<GameplayRoleMappinData> data2) {
    if(data1 == null && data2 != null) {
      return false;
    };
    if(data1 != null && data2 == null) {
      return false;
    };
    if(data1.m_isTagged != data2.m_isTagged) {
      return false;
    };
    if(data1.m_mappinVisualState != data2.m_mappinVisualState) {
      return false;
    };
    if(data1.m_visibleThroughWalls != data2.m_visibleThroughWalls) {
      return false;
    };
    if(data1.m_isCurrentTarget != data2.m_isCurrentTarget) {
      return false;
    };
    if(data1.m_isQuest != data2.m_isQuest) {
      return false;
    };
    if(data1.m_textureID != data2.m_textureID) {
      return false;
    };
    if(data1.m_quality != data2.m_quality) {
      return false;
    };
    if(data1.m_isScanningCluesBlocked != data2.m_isScanningCluesBlocked) {
      return false;
    };
    if(data1.m_gameplayRole != data2.m_gameplayRole) {
      return false;
    };
    if(data1.m_braindanceLayer != data2.m_braindanceLayer) {
      return false;
    };
    return true;
  }

  private final TweakDBID GetIconIdForMappinVariant(gamedataMappinVariant mappinVariant) {
    TweakDBID id;
    if(mappinVariant == gamedataMappinVariant.NPCVariant) {
      id = "MappinIcons.NPCMappin";
    } else {
      if(mappinVariant == gamedataMappinVariant.FastTravelVariant) {
        id = "MappinIcons.FastTravelMappin";
      } else {
        if(mappinVariant == gamedataMappinVariant.DistractVariant) {
          id = "MappinIcons.DistractMappin";
        } else {
          if(mappinVariant == gamedataMappinVariant.LootVariant) {
            if(GetOwner().IsShardContainer()) {
              id = "MappinIcons.ShardMappin";
            } else {
              if(GetOwner().IsQuest()) {
                id = "MappinIcons.QuestMappin";
              } else {
                id = "MappinIcons.LootMappin";
              };
            };
          } else {
            if(mappinVariant == gamedataMappinVariant.EffectExplodeLethalVariant) {
              id = "MappinIcons.ExplosiveDevice";
            } else {
              if(mappinVariant == gamedataMappinVariant.EffectDropPointVariant) {
                id = "MappinIcons.DropPointMappin";
              } else {
                if(mappinVariant == gamedataMappinVariant.FocusClueVariant) {
                  id = "MappinIcons.ClueMappin";
                } else {
                  if(mappinVariant == gamedataMappinVariant.PhoneCallVariant) {
                    id = "MappinIcons.PhoneCallMappin";
                  } else {
                    if(mappinVariant == gamedataMappinVariant.EffectControlNetworkVariant) {
                      id = "MappinIcons.BackdoorDeviceMappin";
                    } else {
                      if(mappinVariant == gamedataMappinVariant.EffectControlOtherDeviceVariant) {
                        id = "MappinIcons.ControlPanleDeviceMappin";
                      } else {
                        if(mappinVariant == gamedataMappinVariant.NetrunnerVariant) {
                          id = "MappinIcons.EnemyNetrunnerMappin";
                        } else {
                          if(mappinVariant == gamedataMappinVariant.EffectHideBodyVariant) {
                            id = "MappinIcons.HideBodyMappin";
                          } else {
                            if(mappinVariant == gamedataMappinVariant.ImportantInteractionVariant) {
                              if(GetOwner().IsQuickHackAble()) {
                                id = "MappinIcons.HackableDeviceMappin";
                              } else {
                                id = "MappinIcons.InteractiveDeviceMappin";
                              };
                            } else {
                              if(mappinVariant == gamedataMappinVariant.GenericRoleVariant) {
                                id = "MappinIcons.GenericDeviceMappin";
                              };
                            };
                          };
                        };
                      };
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
    return id;
  }

  public final void HideRoleMappins() {
    Int32 i;
    NewMappinID invalidID;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].gameplayRole == EGameplayRole. && this.m_mappins[i].gameplayRole == EGameplayRole.UnAssigned) {
      } else {
        if(this.m_mappins[i].permanent && this.m_mappins[i].active) {
        } else {
          if(this.m_mappins[i].active || !this.m_mappins[i].active && this.m_mappins[i].id != invalidID) {
            this.m_isShowingMappins = false;
            DeactivateSingleMappin(i);
          };
        };
      };
      i += 1;
    };
  }

  private final void ClearAllRoleMappins() {
    Int32 i;
    i = Size(this.m_mappins) - 1;
    while(i >= 0) {
      if(this.m_mappins[i].gameplayRole != EGameplayRole. && this.m_mappins[i].gameplayRole != EGameplayRole.UnAssigned) {
        if(this.m_mappins[i].active) {
          HideSingleMappin(i);
        };
        Erase(this.m_mappins, i);
      };
      i -= 1;
    };
  }

  public final void UnregisterAllRoleMappins() {
    Int32 i;
    this.m_isShowingMappins = false;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].gameplayRole == EGameplayRole. || this.m_mappins[i].gameplayRole == EGameplayRole.UnAssigned) {
      } else {
        if(this.m_mappins[i].active) {
          HideSingleMappin(i);
        };
      };
      i += 1;
    };
  }

  protected cb Bool OnUnregisterAllMappinsEvent(ref<UnregisterAllMappinsEvent> evt) {
    UnregisterAllMappins();
  }

  public final void UnregisterAllMappins() {
    Int32 i;
    NewMappinID invalidID;
    this.m_isShowingMappins = false;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].active || this.m_mappins[i].id != invalidID) {
        HideSingleMappin(i);
      };
      i += 1;
    };
  }

  private final void HideSingleMappin_Event(Int32 index) {
    ref<HideSingleMappinEvent> evt;
    evt = new HideSingleMappinEvent();
    evt.index = index;
    QueueEntityEvent(evt);
  }

  private final void HideSingleMappin(Int32 index) {
    NewMappinID invalidID;
    GetMappinSystem().UnregisterMappin(this.m_mappins[index].id);
    this.m_mappins[index].id = invalidID;
    this.m_mappins[index].active = false;
    if(!IsFinal()) {
      LogDevices(GetOwner(), "MAPPIN " + ToString(this.m_mappins[index].gameplayRole) + " HIDDEN");
    };
  }

  private final void DeactivateSingleMappin(Int32 index) {
    this.m_mappins[index].active = false;
    GetMappinSystem().SetMappinActive(this.m_mappins[index].id, false);
    if(!IsFinal()) {
      LogDevices(GetOwner(), "MAPPIN " + ToString(this.m_mappins[index].gameplayRole) + " HIDDEN");
    };
  }

  private final void ShowSingleMappin_Event(Int32 index) {
    ref<ShowSingleMappinEvent> evt;
    if(index < 0 || index > Size(this.m_mappins) - 1) {
      return ;
    };
    this.m_mappins[index].active = true;
    evt = new ShowSingleMappinEvent();
    evt.index = index;
    QueueEntityEvent(evt);
  }

  private final void ShowSingleMappin(Int32 index, ref<GameplayRoleMappinData> visualData) {
    MappinData mappinData;
    CName slotname;
    if(index < 0 || index > Size(this.m_mappins) - 1) {
      return ;
    };
    mappinData.mappinType = this.m_mappins[index].mappinType;
    mappinData.variant = this.m_mappins[index].mappinVariant;
    mappinData.active = true;
    mappinData.debugCaption = this.m_mappins[index].caption;
    mappinData.scriptData = visualData;
    mappinData.visibleThroughWalls = visualData.m_visibleThroughWalls;
    this.m_mappins[index].active = true;
    this.m_mappins[index].visualStateData = visualData;
    if(IsMappinDynamic()) {
      if(this.m_mappins[index].mappinVariant == gamedataMappinVariant.PhoneCallVariant) {
        slotname = GetOwner().GetPhoneCallIndicatorSlotName();
      } else {
        if(this.m_mappins[index].mappinVariant == gamedataMappinVariant.QuickHackVariant) {
          slotname = GetOwner().GetQuickHackIndicatorSlotName();
        } else {
          slotname = GetOwner().GetRoleMappinSlotName();
        };
      };
      if(IsNameValid(slotname)) {
        this.m_mappins[index].id = GetMappinSystem().RegisterMappinWithObject(mappinData, RefToWeakRef(GetOwner()), slotname);
      } else {
        this.m_mappins[index].id = GetMappinSystem().RegisterMappin(mappinData, this.m_mappins[index].position);
      };
    } else {
      this.m_mappins[index].id = GetMappinSystem().RegisterMappin(mappinData, this.m_mappins[index].position);
    };
    GetMappinSystem().SetMappinActive(this.m_mappins[index].id, true);
    if(!IsFinal()) {
      LogDevices(GetOwner(), "MAPPIN " + ToString(this.m_mappins[index].gameplayRole) + " SHOWN");
    };
  }

  private final void ShowSingleMappin(Int32 index) {
    MappinData mappinData;
    ref<GameplayRoleMappinData> visualData;
    CName slotname;
    Vector3 worldOffset;
    if(index < 0 || index > Size(this.m_mappins) - 1) {
      return ;
    };
    visualData = CreateRoleMappinData(this.m_mappins[index]);
    mappinData.mappinType = this.m_mappins[index].mappinType;
    mappinData.variant = this.m_mappins[index].mappinVariant;
    mappinData.active = true;
    mappinData.debugCaption = this.m_mappins[index].caption;
    mappinData.scriptData = visualData;
    mappinData.visibleThroughWalls = visualData.m_visibleThroughWalls;
    this.m_mappins[index].active = true;
    this.m_mappins[index].visualStateData = visualData;
    if(IsMappinDynamic()) {
      if(this.m_mappins[index].mappinVariant == gamedataMappinVariant.PhoneCallVariant) {
        slotname = GetOwner().GetPhoneCallIndicatorSlotName();
      } else {
        if(this.m_mappins[index].mappinVariant == gamedataMappinVariant.QuickHackVariant) {
          slotname = GetOwner().GetQuickHackIndicatorSlotName();
        } else {
          slotname = GetOwner().GetRoleMappinSlotName();
        };
      };
      if(this.m_mappins[index].mappinVariant == gamedataMappinVariant.LootVariant && GetOwner().IsNPC()) {
        worldOffset = new Vector3(0,0,0.1599999964237213);
      };
      this.m_mappins[index].id = GetMappinSystem().RegisterMappinWithObject(mappinData, RefToWeakRef(GetOwner()), slotname, worldOffset);
    } else {
      this.m_mappins[index].id = GetMappinSystem().RegisterMappin(mappinData, this.m_mappins[index].position);
    };
    GetMappinSystem().SetMappinActive(this.m_mappins[index].id, true);
    if(!IsFinal()) {
      LogDevices(GetOwner(), "MAPPIN " + ToString(this.m_mappins[index].gameplayRole) + " SHOWN");
    };
  }

  private final void UpdateSingleMappinData(Int32 index, ref<GameplayRoleMappinData> visualData, Bool shouldUpdateVariant) {
    if(index < 0 || index > Size(this.m_mappins) - 1) {
      return ;
    };
    if(shouldUpdateVariant) {
      HideSingleMappin(index);
      ShowSingleMappin(index, visualData);
      return ;
    };
    this.m_mappins[index].visualStateData = visualData;
    GetMappinSystem().SetMappinScriptData(this.m_mappins[index].id, visualData);
  }

  private final void ActivateSingleMappin(Int32 index) {
    this.m_mappins[index].active = true;
    GetMappinSystem().SetMappinActive(this.m_mappins[index].id, true);
    if(!IsFinal()) {
      LogDevices(GetOwner(), "MAPPIN " + ToString(this.m_mappins[index].gameplayRole) + " HIDDEN");
    };
  }

  public final const Bool HasActiveMappin(gamedataMappinVariant mappinVariant) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].mappinVariant == mappinVariant && this.m_mappins[i].active) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const Bool HasMappin(gamedataMappinVariant mappinVariant) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].mappinVariant == mappinVariant) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const Bool HasMappin(SDeviceMappinData data) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].mappinVariant == data.mappinVariant && this.m_mappins[i].mappinType == data.mappinType && this.m_mappins[i].caption == data.caption) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const Bool HasMappin(EGameplayRole gameplayRole) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].gameplayRole == gameplayRole) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final Bool IsMappinDataValid(SDeviceMappinData mappinData) {
    if(mappinData.mappinVariant != gamedataMappinVariant.Invalid && IsValid(mappinData.mappinType)) {
      return true;
    };
    return false;
  }

  private final Bool IsMappinDynamic() {
    return this.m_alwaysCreateMappinAsDynamic || GetOwner().IsNetworkLinkDynamic();
  }

  private final const Bool IsCurrentTarget() {
    return this.m_isBeingScanned || this.m_isCurrentTarget;
  }

  public final void ToggleMappin(gamedataMappinVariant mappinVariant, Bool enable, Bool show, ref<GameplayRoleMappinData> visualData) {
    Int32 i;
    Int32 index;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].mappinVariant == mappinVariant) {
        this.m_mappins[i].enabled = enable;
        if(!enable) {
          if(this.m_mappins[i].active) {
            HideSingleMappin(i);
          };
        } else {
          if(show && !this.m_mappins[i].active) {
            EvaluatePositions();
            ShowSingleMappin(i, visualData);
          };
        };
      };
      i += 1;
    };
  }

  public final void ToggleMappin(gamedataMappinVariant mappinVariant, Bool enable, Bool show) {
    Int32 i;
    Int32 index;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].mappinVariant == mappinVariant) {
        this.m_mappins[i].enabled = enable;
        if(!enable) {
          if(this.m_mappins[i].active) {
            HideSingleMappin(i);
          };
        } else {
          if(show && !this.m_mappins[i].active) {
            EvaluatePositions();
            ShowSingleMappin(i);
          };
        };
      };
      i += 1;
    };
  }

  public final void ToggleMappin(gamedataMappinVariant mappinVariant, Bool enable) {
    Int32 i;
    Int32 index;
    i = 0;
    while(i < Size(this.m_mappins)) {
      if(this.m_mappins[i].mappinVariant == mappinVariant) {
        this.m_mappins[i].enabled = enable;
        if(!enable) {
          if(this.m_mappins[i].active) {
            HideSingleMappin(i);
          };
        };
      };
      i += 1;
    };
  }

  public final void ToggleMappin(Int32 mappinIndex, Bool enable) {
    if(mappinIndex > Size(this.m_mappins)) {
      return ;
    };
    this.m_mappins[mappinIndex].enabled = enable;
    if(!enable) {
      if(this.m_mappins[mappinIndex].active) {
        HideSingleMappin(mappinIndex);
      };
    };
  }

  public final Bool AddMappin(SDeviceMappinData data) {
    if(IsMappinDataValid(data) && !HasMappin(data)) {
      Push(this.m_mappins, data);
      return true;
    };
    return false;
  }

  public final const EGameplayRole GetCurrentGameplayRole() {
    return this.m_currentGameplayRole;
  }

  public final const Bool IsGameplayRoleStatic() {
    return this.m_gameplayRole != EGameplayRole.UnAssigned;
  }

  protected final void UpdateDefaultHighlight() {
    ref<ForceUpdateDefaultHighlightEvent> updateHighlightEvt;
    updateHighlightEvt = new ForceUpdateDefaultHighlightEvent();
    GetOwner().QueueEvent(updateHighlightEvt);
  }

  private final Bool IsForceHidden() {
    return this.m_isForceHidden;
  }

  private final void SetForceHidden(Bool isHidden) {
    this.m_isForceHidden = isHidden;
    if(isHidden) {
      HideRoleMappins();
    } else {
      RequestHUDRefresh();
    };
  }

  protected cb Bool OnToggleGameplayMappinVisibilityEvent(ref<ToggleGameplayMappinVisibilityEvent> evt) {
    SetForceHidden(evt.isHidden);
  }

  private final void RequestHUDRefresh() {
    ref<RefreshActorRequest> request;
    ref<GameObject> owner;
    ref<HUDManager> hudManager;
    array<wref<HUDModule>> modules;
    owner = GetOwner();
    if(ToBool(owner)) {
      hudManager = owner.GetHudManager();
    };
    if(!ToBool(hudManager)) {
      return ;
    };
    Push(modules, RefToWeakRef(hudManager.GetIconsModule()));
    request = Construct(owner.GetEntityID(), modules);
    hudManager.QueueRequest(request);
  }
}
