
public class InspectableObjectComponentPS extends GameComponentPS {

  private persistent Bool m_isStarted;

  private persistent Bool m_isFinished;

  private array<ref<ObjectInspectListener>> m_listeners;

  public final const Bool IsState(questObjectInspectEventType state) {
    if(state == questObjectInspectEventType.Started && this.m_isStarted) {
      return true;
    };
    if(state == questObjectInspectEventType.Finished && this.m_isFinished) {
      return true;
    };
    return false;
  }

  public final EntityNotificationType OnRegisterListener(ref<InspectListenerEvent> evt) {
    if(evt.register) {
      Push(this.m_listeners, evt.listener);
    } else {
      Remove(this.m_listeners, evt.listener);
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final EntityNotificationType OnSetState(ref<SetInspectStateEvent> evt) {
    if(evt.state == questObjectInspectEventType.Started) {
      SetStarted();
    } else {
      if(evt.state == questObjectInspectEventType.Finished) {
        SetFinished();
      };
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final void SetStarted() {
    this.m_isStarted = true;
    NotifyListeners(questObjectInspectEventType.Started);
  }

  public final void SetFinished() {
    this.m_isFinished = true;
    NotifyListeners(questObjectInspectEventType.Finished);
  }

  private final void NotifyListeners(questObjectInspectEventType state) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_listeners)) {
      this.m_listeners[i].OnInspect(state);
      i += 1;
    };
  }
}

public class InspectableObjectComponent extends ScriptableComponent {

  public CName m_factToAdd;

  public String m_itemID;

  [Default(InspectableObjectComponent, 0.5f))]
  public Float m_offset;

  [Default(InspectableObjectComponent, 0.25f))]
  public Float m_adsOffset;

  [Default(InspectableObjectComponent, 2.f))]
  public Float m_timeToScan;

  [Default(InspectableObjectComponent, AttachmentSlots.Inspect))]
  private String m_slot;

  protected const ref<InspectableObjectComponentPS> GetPS() {
    return Cast(GetBasePS());
  }

  private final void InspectObject(ref<GameObject> activator) {
    ref<InspectionTriggerEvent> evt;
    evt = new InspectionTriggerEvent();
    evt.inspectedObjID = GetOwner().GetEntityID();
    evt.item = this.m_itemID;
    evt.offset = this.m_offset;
    evt.adsOffset = this.m_adsOffset;
    evt.timeToScan = this.m_timeToScan;
    activator.QueueEvent(evt);
    SetInspectableObjectState(false);
    SetFactValue(GetOwner().GetGame(), this.m_factToAdd, 1);
    GetPS().SetStarted();
  }

  protected cb Bool OnInspectEvent(ref<ObjectInspectEvent> evt) {
    SetInspectableObjectState(evt.showItem);
  }

  private final void GiveInspectableItem(ref<GameObject> activator) {
    ref<ObjectInspectEvent> inspectEvt;
    ref<TransactionSystem> transSystem;
    transSystem = GetTransactionSystem(GetOwner().GetGame());
    transSystem.GiveItem(activator, FromTDBID(Create(this.m_itemID)), 1);
    inspectEvt = new ObjectInspectEvent();
    inspectEvt.showItem = false;
    GetOwner().QueueEvent(inspectEvt);
    GetPS().SetFinished();
  }

  protected cb Bool OnInspectItem(ref<InspectItemInspectionEvent> evt) {
    InspectObject(WeakRefToRef(evt.owner));
  }

  protected cb Bool OnLootItem(ref<InspectItemInspectionEvent> evt) {
    GiveInspectableItem(WeakRefToRef(evt.owner));
  }

  private final void SetInspectableObjectState(Bool b) {
    ref<InspectDummy> owner;
    ref<InteractionSetEnableEvent> state;
    owner = Cast(GetOwner());
    state = new InteractionSetEnableEvent();
    state.enable = b;
    owner.QueueEvent(state);
    owner.m_mesh.Toggle(b);
  }
}
