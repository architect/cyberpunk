
public class EffectExecutor_GameObjectOutline extends EffectExecutor_Scripted {

  public edit EOutlineType m_outlineType;

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<Entity> target;
    ref<OutlineRequestEvent> evt;
    OutlineData data;
    CName id;
    if(this.m_outlineType == EOutlineType.RED) {
      id = "EffectExecutor_GameObjectOutline_RED";
    } else {
      id = "EffectExecutor_GameObjectOutline_GREEN";
    };
    target = GetTarget(applierCtx);
    evt = new OutlineRequestEvent();
    data.outlineType = this.m_outlineType;
    data.outlineStrength = 1;
    evt.outlineRequest = CreateRequest(id, true, data);
    target.QueueEvent(evt);
    return true;
  }
}

public class AddTargetToHighlightEvent extends Event {

  public CombatTarget m_target;

  public final void Create(ref<ScriptedPuppet> puppet) {
    this.m_target.m_puppet = RefToWeakRef(puppet);
    this.m_target.m_hasTime = false;
  }

  public final void Create(ref<ScriptedPuppet> puppet, Float highlightTime) {
    this.m_target.m_puppet = RefToWeakRef(puppet);
    this.m_target.m_hasTime = true;
    this.m_target.m_highlightTime = highlightTime;
  }
}

public class CombatHUDManager extends ScriptableComponent {

  public Bool m_isRunning;

  public array<CombatTarget> m_targets;

  [Default(CombatHUDManager, 1.0f))]
  public Float m_interval;

  public Float m_timeSinceLastUpdate;

  private final void OnAddTargetToHighlightEvent(ref<AddTargetToHighlightEvent> evt) {
    EAIAttitude att;
    ref<RevealRequestEvent> revealEvent;
    ref<RemoveTargetFromHighlightEvent> removeTargetEvent;
    revealEvent = new RevealRequestEvent();
    revealEvent.CreateRequest(true, GetOwner().GetEntityID());
    if(!TargetExists(WeakRefToRef(evt.m_target.m_puppet))) {
      WeakRefToRef(evt.m_target.m_puppet).QueueEvent(revealEvent);
      Push(this.m_targets, evt.m_target);
      if(evt.m_target.m_hasTime) {
        removeTargetEvent = new RemoveTargetFromHighlightEvent();
        removeTargetEvent.m_target = evt.m_target.m_puppet;
        GetDelaySystem().DelayEvent(RefToWeakRef(GetOwner()), removeTargetEvent, evt.m_target.m_highlightTime);
      };
    };
  }

  private final void OnRemoveTargetFromHighlightEvent(ref<RemoveTargetFromHighlightEvent> evt) {
    ref<RevealRequestEvent> revealEvent;
    revealEvent = new RevealRequestEvent();
    revealEvent.CreateRequest(false, GetOwner().GetEntityID());
    if(TargetExists(WeakRefToRef(evt.m_target))) {
      RemoveTarget(evt.m_target);
      WeakRefToRef(evt.m_target).QueueEvent(revealEvent);
    };
  }

  private final Bool TargetExists(ref<ScriptedPuppet> puppet) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_targets)) {
      if(WeakRefToRef(this.m_targets[i].m_puppet) == puppet) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final void OnToggleChargeHighlightEvent(ref<ToggleChargeHighlightEvent> evt) {
    if(evt.m_active) {
      HandleChargeMode();
    } else {
      ClearHUD();
    };
  }

  private final void RemoveTarget(wref<ScriptedPuppet> target) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_targets)) {
      if(WeakRefToRef(this.m_targets[i].m_puppet) == WeakRefToRef(target)) {
        Erase(this.m_targets, i);
      } else {
        i += 1;
      };
    };
  }

  private final void ClearHUD() {
    ref<RevealRequestEvent> revealRequestEvent;
    Int32 i;
    revealRequestEvent = new RevealRequestEvent();
    revealRequestEvent.CreateRequest(false, GetOwner().GetEntityID());
    i = 0;
    while(i < Size(this.m_targets)) {
      GetOwner().QueueEventForEntityID(WeakRefToRef(this.m_targets[i].m_puppet).GetEntityID(), revealRequestEvent);
      i += 1;
    };
    Clear(this.m_targets);
  }

  private final void DetermineProperHandlingMode(ref<WeaponObject> activeWeapon) {
    ref<TriggerMode_Record> triggerMode;
    gamedataTriggerMode triggerType;
    triggerMode = activeWeapon.GetCurrentTriggerMode();
    triggerType = triggerMode.Type();
    switch(triggerType) {
      case gamedataTriggerMode.Charge:
        HandleChargeMode();
    };
  }

  private final void HandleChargeMode() {
    ref<HudEnhancer_Record> distanceRecord;
    Float distance;
    ref<EffectInstance> effectCone;
    ref<EffectInstance> effectRaycast;
    Int32 i;
    Vector4 aimPosition;
    Vector4 aimForward;
    Float coneAngle;
    GetTargetingSystem().GetDefaultCrosshairData(RefToWeakRef(GetOwner()), aimPosition, aimForward);
    distanceRecord = GetHudEnhancerRecord("HudEnhancer.ChargeWeapon");
    distance = distanceRecord.Distance();
    coneAngle = GetFloat("HudEnhancer.ChargeWeapon.coneAngle");
    effectCone = GetGameEffectSystem().CreateEffectStatic("weaponShoot", "pierce_preview_cone", GetOwner());
    SetVector(effectCone.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, aimPosition);
    SetVector(effectCone.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, aimForward);
    SetFloat(effectCone.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, distance);
    SetFloat(effectCone.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.angle, coneAngle);
    effectRaycast = GetGameEffectSystem().CreateEffectStatic("weaponShoot", "pierce_preview_raycast", GetOwner());
    SetVector(effectRaycast.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, aimPosition);
    SetVector(effectRaycast.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, aimForward);
    SetFloat(effectRaycast.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, distance);
    SetBool(effectRaycast.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.fallback_weaponPierce, true);
    SetFloat(effectRaycast.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.fallback_weaponPierceChargeLevel, 0);
  }
}
