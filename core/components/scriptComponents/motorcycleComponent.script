
public class MotorcycleComponent extends VehicleComponent {

  protected cb Bool OnVehicleParkedEvent(ref<VehicleParkedEvent> evt) {
    if(evt.park) {
      ParkBike();
    } else {
      UnParkBike();
    };
  }

  protected cb Bool OnMountingEvent(ref<MountingEvent> evt) {
    OnMountingEvent(evt);
    WeakRefToRef(GetVehicle()).PhysicsWakeUp();
    UnParkBike();
    PickUpBike();
  }

  protected cb Bool OnUnmountingEvent(ref<UnmountingEvent> evt) {
    ref<KnockOverBikeEvent> knockOverBike;
    Float currentSpeed;
    OnUnmountingEvent(evt);
    currentSpeed = WeakRefToRef(GetVehicle()).GetCurrentSpeed();
    if(currentSpeed >= 3) {
      knockOverBike = new KnockOverBikeEvent();
      WeakRefToRef(GetVehicle()).QueueEvent(knockOverBike);
    } else {
      ParkBike();
    };
  }

  private final void ParkBike() {
    Float currentTiltAngle;
    Float desiredTiltAngle;
    wref<Vehicle_Record> record;
    wref<VehicleDataPackage_Record> vehicleDataPackage;
    currentTiltAngle = Cast(WeakRefToRef(GetVehicle())).GetCustomTargetTilt();
    record = WeakRefToRef(GetVehicle()).GetRecord();
    vehicleDataPackage = WeakRefToRef(record).VehDataPackage();
    desiredTiltAngle = WeakRefToRef(vehicleDataPackage).ParkingAngle();
    if(!Cast(WeakRefToRef(GetVehicle())).IsTiltControlEnabled()) {
      return ;
    };
    if(currentTiltAngle == 0 && !IsVehicleOccupied(WeakRefToRef(GetVehicle()).GetGame(), GetVehicle())) {
      Cast(WeakRefToRef(GetVehicle())).SetCustomTargetTilt(desiredTiltAngle);
      PushEvent(WeakRefToRef(GetVehicle()), "toPark");
      PushEvent(WeakRefToRef(GetVehicle()), "readyModeEnd");
      WeakRefToRef(GetVehicle()).PhysicsWakeUp();
    };
  }

  private final void UnParkBike() {
    Cast(WeakRefToRef(GetVehicle())).SetCustomTargetTilt(0);
    PushEvent(WeakRefToRef(GetVehicle()), "unPark");
  }

  private final void PickUpBike() {
    if(!Cast(WeakRefToRef(GetVehicle())).IsTiltControlEnabled()) {
      Cast(WeakRefToRef(GetVehicle())).EnableTiltControl(true);
    };
  }

  protected cb Bool OnKnockOverBikeEvent(ref<KnockOverBikeEvent> evt) {
    MountingInfo mountInfo;
    ref<PhysicalImpulseEvent> bikeImpulseEvent;
    Vector4 tempVec4;
    mountInfo = GetMountingFacility(WeakRefToRef(GetVehicle()).GetGame()).GetMountingInfoSingleWithObjects(WeakRefToRef(GetVehicle()));
    if(evt.forceKnockdown) {
      if(Cast(WeakRefToRef(GetVehicle())).IsTiltControlEnabled()) {
        UnParkBike();
        Cast(WeakRefToRef(GetVehicle())).EnableTiltControl(false);
      };
    } else {
      if(!IsVehicleOccupied(WeakRefToRef(GetVehicle()).GetGame(), GetVehicle())) {
        if(Cast(WeakRefToRef(GetVehicle())).IsTiltControlEnabled()) {
          UnParkBike();
          Cast(WeakRefToRef(GetVehicle())).EnableTiltControl(false);
        };
      };
    };
    if(evt.applyDirectionalForce) {
      bikeImpulseEvent = new PhysicalImpulseEvent();
      bikeImpulseEvent.radius = 1;
      tempVec4 = WeakRefToRef(GetVehicle()).GetWorldPosition();
      bikeImpulseEvent.worldPosition.X = tempVec4.X;
      bikeImpulseEvent.worldPosition.Y = tempVec4.Y;
      bikeImpulseEvent.worldPosition.Z = tempVec4.Z + 0.5;
      tempVec4 = GetRight(WeakRefToRef(GetVehicle()).GetWorldTransform());
      tempVec4 *= WeakRefToRef(GetVehicle()).GetTotalMass() * 3.799999952316284;
      bikeImpulseEvent.worldImpulse = Vector4To3(tempVec4);
      WeakRefToRef(GetVehicle()).QueueEvent(bikeImpulseEvent);
    };
  }
}
