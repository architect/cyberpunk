
public class LightComponent extends IVisualComponent {

  public final native void SetTemperature(Float temperature)

  public final native void SetColor(Color color)

  public final native void SetRadius(Float radius)

  public final native void SetIntensity(Float intensity)

  public final native void SetFlickerParams(Float strength, Float period, Float offset)

  protected cb Bool OnForceFlicker(ref<FlickerEvent> evt) {
    SetFlickerParams(evt.strength, evt.duration, evt.offset);
  }

  protected cb Bool OnToggleLight(ref<ToggleLightEvent> evt) {
    Toggle(evt.toggle);
  }

  protected cb Bool OnToggleLightByName(ref<ToggleLightByNameEvent> evt) {
    String fullComponentName;
    fullComponentName = NameToString(GetName());
    if(StrContains(fullComponentName, NameToString(evt.componentName))) {
      Toggle(evt.toggle);
    };
  }
}
