
public class gamestateMachineComponent extends gamePlayerControlledComponent {

  public final native void AddStateMachine(CName stateMachineName, StateMachineInstanceData instanceData, wref<Entity> owner, Bool tryHotSwap?)

  public final native void RemoveStateMachine(StateMachineIdentifier stateMachineIdentifier)

  public final native Bool IsStateMachinePresent(StateMachineIdentifier stateMachineIdentifier)

  public final native StateSnapshotsContainer GetSnapshotContainer()

  protected cb Bool OnStartTakedownEvent(ref<StartTakedownEvent> startTakedownEvent) {
    wref<Entity> owner;
    ref<PSMAddOnDemandStateMachine> addEvent;
    ref<LocomotionTakedownInitData> initData;
    StateMachineInstanceData instanceData;
    ref<Record1DamageInHistoryEvent> record1HitDamage;
    initData = new LocomotionTakedownInitData();
    addEvent = new PSMAddOnDemandStateMachine();
    record1HitDamage = new Record1DamageInHistoryEvent();
    initData.target = startTakedownEvent.target;
    initData.slideTime = startTakedownEvent.slideTime;
    initData.actionName = startTakedownEvent.actionName;
    instanceData.initData = initData;
    addEvent.stateMachineName = "LocomotionTakedown";
    addEvent.instanceData = instanceData;
    owner = GetEntity();
    WeakRefToRef(owner).QueueEvent(addEvent);
    if(ToBool(startTakedownEvent.target)) {
      record1HitDamage.source = RefToWeakRef(Cast(WeakRefToRef(owner)));
      WeakRefToRef(startTakedownEvent.target).QueueEvent(record1HitDamage);
    };
  }

  protected cb Bool OnRipOff(ref<RipOff> evt) {
    wref<GameObject> owner;
    ref<PSMAddOnDemandStateMachine> addEvent;
    ref<TurretInitData> initData;
    StateMachineInstanceData instanceData;
    addEvent = new PSMAddOnDemandStateMachine();
    initData = new TurretInitData();
    owner = RefToWeakRef(Cast(WeakRefToRef(GetEntity())));
    initData.turret = RefToWeakRef(Cast(FindEntityByID(WeakRefToRef(owner).GetGame(), evt.GetRequesterID())));
    instanceData.initData = initData;
    addEvent.stateMachineName = "Turret";
    addEvent.instanceData = instanceData;
    WeakRefToRef(owner).QueueEvent(addEvent);
  }

  protected cb Bool OnStartMountingEvent(ref<MountingEvent> mountingEvent) {
    MountingRelationship relationship;
    ref<Entity> owner;
    relationship = mountingEvent.relationship;
    owner = WeakRefToRef(GetEntity());
    switch(relationship.relationshipType) {
      case gameMountingRelationshipType.Parent:
        MountFromParent(mountingEvent, owner);
        break;
      case gameMountingRelationshipType.Child:
        MountAsChild(mountingEvent, owner);
        break;
      default:
    };
  }

  protected cb Bool OnStartUnmountingEvent(ref<UnmountingEvent> unmountingEvent) {
    MountingRelationship relationship;
    ref<Entity> owner;
    relationship = unmountingEvent.relationship;
    owner = WeakRefToRef(GetEntity());
    switch(relationship.relationshipType) {
      case gameMountingRelationshipType.Parent:
        UnmountFromParent(unmountingEvent, owner);
        break;
      case gameMountingRelationshipType.Child:
        UnmountChild(unmountingEvent, owner);
        break;
      default:
    };
  }

  protected final void MountFromParent(ref<MountingEvent> mountingEvent, ref<Entity> ownerEntity) {
    ref<VehicleTransitionInitData> initData;
    MountingRelationship relationship;
    gameMountingObjectType otherObjectType;
    StateMachineInstanceData instanceData;
    wref<GameObject> otherObject;
    initData = new VehicleTransitionInitData();
    relationship = mountingEvent.relationship;
    otherObjectType = relationship.otherMountableType;
    otherObject = RelationshipGetOtherObject(relationship);
    switch(otherObjectType) {
      case gameMountingObjectType.Vehicle:
        if(mountingEvent.request.mountData.mountEventOptions.silentUnmount) {
          return ;
        };
        initData.instant = mountingEvent.request.mountData.isInstant;
        initData.entityID = mountingEvent.request.mountData.mountEventOptions.entityID;
        initData.alive = mountingEvent.request.mountData.mountEventOptions.alive;
        initData.occupiedByNeutral = mountingEvent.request.mountData.mountEventOptions.occupiedByNeutral;
        instanceData.initData = initData;
        AddStateMachine("Vehicle", instanceData, otherObject);
        break;
      case gameMountingObjectType.Object:
        break;
      case gameMountingObjectType.Puppet:
        break;
      case gameMountingObjectType.Platform:
        break;
      case gameMountingObjectType.Invalid:
        break;
      default:
    };
  }

  protected final void MountAsChild(ref<MountingEvent> mountingEvent, ref<Entity> ownerEntity) {
    MountingRelationship relationship;
    gameMountingObjectType otherObjectType;
    ref<CarriedObjectData> initData;
    StateMachineInstanceData instanceData;
    wref<GameObject> otherObject;
    StateMachineIdentifier stateMachineIdentifier;
    relationship = mountingEvent.relationship;
    otherObjectType = relationship.otherMountableType;
    initData = new CarriedObjectData();
    otherObject = RelationshipGetOtherObject(relationship);
    stateMachineIdentifier.definitionName = "LocomotionTakedown";
    switch(otherObjectType) {
      case gameMountingObjectType.Vehicle:
        break;
      case gameMountingObjectType.Puppet:
      case gameMountingObjectType.Object:
        if(WeakRefToRef(otherObject) != null) {
          if(!IsStateMachinePresent(stateMachineIdentifier)) {
            initData.instant = mountingEvent.request.mountData.isInstant;
            instanceData.initData = initData;
            AddStateMachine("CarriedObject", instanceData, otherObject);
          };
        };
        break;
      case gameMountingObjectType.Platform:
        break;
      case gameMountingObjectType.Invalid:
        break;
      default:
    };
  }

  protected final void UnmountFromParent(ref<UnmountingEvent> unmountingEvent, ref<Entity> ownerEntity) {
    MountingRelationship relationship;
    gameMountingObjectType otherObjectType;
    StateMachineIdentifier stateMachineIdentifier;
    Bool silentUnmount;
    relationship = unmountingEvent.relationship;
    otherObjectType = relationship.otherMountableType;
    silentUnmount = unmountingEvent.request.mountData.mountEventOptions.silentUnmount;
    stateMachineIdentifier.definitionName = "Vehicle";
    switch(otherObjectType) {
      case gameMountingObjectType.Vehicle:
        if(!silentUnmount) {
          RemoveStateMachine(stateMachineIdentifier);
        };
        break;
      case gameMountingObjectType.Object:
        break;
      case gameMountingObjectType.Puppet:
        break;
      case gameMountingObjectType.Platform:
        break;
      case gameMountingObjectType.Invalid:
        break;
      default:
    };
  }

  protected final void UnmountChild(ref<UnmountingEvent> unmountingEvent, ref<Entity> ownerEntity) {
    MountingRelationship relationship;
    gameMountingObjectType otherObjectType;
    StateMachineIdentifier stateMachineIdentifier;
    relationship = unmountingEvent.relationship;
    otherObjectType = relationship.otherMountableType;
    stateMachineIdentifier.definitionName = "CarriedObject";
    switch(otherObjectType) {
      case gameMountingObjectType.Vehicle:
        break;
      case gameMountingObjectType.Puppet:
      case gameMountingObjectType.Object:
        RemoveStateMachine(stateMachineIdentifier);
        break;
      case gameMountingObjectType.Platform:
        break;
      case gameMountingObjectType.Invalid:
        break;
      default:
    };
  }
}
