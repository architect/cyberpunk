
public class AnimationControllerComponent extends IComponent {

  private final native void PushEvent(CName eventName)

  private final native void SetInputFloat(CName inputName, Float value)

  private final native void SetInputInt(CName inputName, Int32 value)

  private final native void SetInputBool(CName inputName, Bool value)

  private final native void SetInputQuaternion(CName inputName, Quaternion value)

  private final native void SetInputVector(CName inputName, Vector4 value)

  private final native void SetUsesSleepMode(Bool allowSleepState)

  private final native void ScheduleFastForward()

  public final native Bool PreloadAnimations(CName streamingContextName, Bool highPriority)

  private final native void ApplyFeature(CName inputName, ref<AnimFeature> value)

  public final native const Float GetAnimationDuration(CName animationName)

  protected cb Bool OnSetInputVectorEvent(ref<AnimInputSetterVector> evt) {
    SetInputVector(evt.key, evt.value);
  }

  public final static void ApplyFeature(ref<GameObject> obj, CName inputName, ref<AnimFeature> value, Float delay?) {
    ref<AnimInputSetterAnimFeature> evt;
    ref<ItemObject> item;
    evt = new AnimInputSetterAnimFeature();
    evt.key = inputName;
    evt.value = value;
    evt.delay = delay;
    obj.QueueEvent(evt);
    item = Cast(obj);
    if(ToBool(item)) {
      item.QueueEventToChildItems(evt);
    };
  }

  public final static void ApplyFeatureToReplicate(ref<GameObject> obj, CName inputName, ref<AnimFeature> value, Float delay?) {
    ApplyFeature(obj, inputName, value, delay);
    obj.ReplicateAnimFeature(obj, inputName, value);
  }

  public final static void ApplyFeatureToReplicateOnHeldItems(ref<GameObject> obj, CName inputName, ref<AnimFeature> value, Float delay?) {
    ref<ItemObject> leftItem;
    ref<ItemObject> rightItem;
    leftItem = GetTransactionSystem(obj.GetGame()).GetItemInSlot(obj, "AttachmentSlots.WeaponLeft");
    rightItem = GetTransactionSystem(obj.GetGame()).GetItemInSlot(obj, "AttachmentSlots.WeaponRight");
    if(ToBool(leftItem)) {
      ApplyFeature(leftItem, inputName, value, delay);
      leftItem.ReplicateAnimFeature(leftItem, inputName, value);
    };
    if(ToBool(rightItem)) {
      ApplyFeature(rightItem, inputName, value, delay);
      rightItem.ReplicateAnimFeature(rightItem, inputName, value);
    };
  }

  public final static void PushEvent(ref<GameObject> obj, CName eventName) {
    ref<AnimExternalEvent> evt;
    ref<ItemObject> item;
    evt = new AnimExternalEvent();
    evt.name = eventName;
    obj.QueueEvent(evt);
    item = Cast(obj);
    if(ToBool(item)) {
      item.QueueEventToChildItems(evt);
    };
  }

  public final static void PushEventToObjAndHeldItems(ref<GameObject> obj, CName eventName) {
    ref<ItemObject> item;
    if(!ToBool(obj)) {
      return ;
    };
    PushEvent(obj, eventName);
    item = GetTransactionSystem(obj.GetGame()).GetItemInSlot(obj, "AttachmentSlots.WeaponLeft");
    if(ToBool(item)) {
      PushEvent(item, eventName);
    };
    item = GetTransactionSystem(obj.GetGame()).GetItemInSlot(obj, "AttachmentSlots.WeaponRight");
    if(ToBool(item)) {
      PushEvent(item, eventName);
    };
  }

  public final static void PushEventToReplicate(ref<GameObject> obj, CName eventName) {
    PushEvent(obj, eventName);
    obj.ReplicateAnimEvent(obj, eventName);
  }

  public final static void SetInputFloat(ref<GameObject> obj, CName inputName, Float value) {
    ref<AnimInputSetterFloat> evt;
    evt = new AnimInputSetterFloat();
    evt.key = inputName;
    evt.value = value;
    obj.QueueEvent(evt);
  }

  public final static void SetInputFloatToReplicate(ref<GameObject> obj, CName inputName, Float value) {
    SetInputFloat(obj, inputName, value);
    obj.ReplicateInputFloat(obj, inputName, value);
  }

  public final static void SetInputBool(ref<GameObject> obj, CName inputName, Bool value) {
    ref<AnimInputSetterBool> evt;
    evt = new AnimInputSetterBool();
    evt.key = inputName;
    evt.value = value;
    obj.QueueEvent(evt);
  }

  public final static void SetInputBoolToReplicate(ref<GameObject> obj, CName inputName, Bool value) {
    SetInputBool(obj, inputName, value);
    obj.ReplicateInputBool(obj, inputName, value);
  }

  public final static void SetInputInt(ref<GameObject> obj, CName inputName, Int32 value) {
    ref<AnimInputSetterInt> evt;
    evt = new AnimInputSetterInt();
    evt.key = inputName;
    evt.value = value;
    obj.QueueEvent(evt);
  }

  public final static void SetInputIntToReplicate(ref<GameObject> obj, CName inputName, Int32 value) {
    SetInputInt(obj, inputName, value);
    obj.ReplicateInputInt(obj, inputName, value);
  }

  public final static void SetInputVector(ref<GameObject> obj, CName inputName, Vector4 value) {
    ref<AnimInputSetterVector> evt;
    evt = new AnimInputSetterVector();
    evt.key = inputName;
    evt.value = value;
    obj.QueueEvent(evt);
  }

  public final static void SetInputVectorToReplicate(ref<GameObject> obj, CName inputName, Vector4 value) {
    SetInputVector(obj, inputName, value);
    obj.ReplicateInputVector(obj, inputName, value);
  }

  public final static void SetUsesSleepMode(ref<GameObject> obj, Bool state) {
    ref<AnimInputSetterUsesSleepMode> evt;
    evt = new AnimInputSetterUsesSleepMode();
    evt.value = state;
    obj.QueueEvent(evt);
  }

  public final static void SetAnimWrapperWeight(ref<GameObject> obj, CName key, Float value) {
    ref<AnimWrapperWeightSetter> evt;
    ref<ItemObject> item;
    if(!IsNameValid(key)) {
      return ;
    };
    evt = new AnimWrapperWeightSetter();
    evt.key = key;
    evt.value = value;
    obj.QueueEvent(evt);
    item = Cast(obj);
    if(ToBool(item)) {
      item.QueueEventToChildItems(evt);
    };
  }

  public final static void SetAnimWrapperWeightOnOwnerAndItems(ref<GameObject> owner, CName key, Float value) {
    ref<ItemObject> item;
    SetAnimWrapperWeight(owner, key, value);
    item = GetTransactionSystem(owner.GetGame()).GetItemInSlot(owner, "AttachmentSlots.WeaponRight");
    if(ToBool(item)) {
      SetAnimWrapperWeight(item, key, value);
    };
    item = GetTransactionSystem(owner.GetGame()).GetItemInSlot(owner, "AttachmentSlots.WeaponLeft");
    if(ToBool(item)) {
      SetAnimWrapperWeight(item, key, value);
    };
  }
}
