
public class HitData_Base extends HitShapeUserData {

  public edit CName m_hitShapeTag;

  public edit CName m_bodyPartStatPoolName;

  public edit HitShape_Type m_hitShapeType;

  public final const Bool IsWeakspot() {
    return this.m_hitShapeType == HitShape_Type.InternalWeakSpot || this.m_hitShapeType == HitShape_Type.ExternalWeakSpot;
  }
}

public class HitShapeUserDataBase extends HitShapeUserData {

  public edit CName m_hitShapeTag;

  public edit EHitShapeType m_hitShapeType;

  public edit EHitReactionZone m_hitReactionZone;

  public edit EAIDismembermentBodyPart m_dismembermentPart;

  public edit Bool m_isProtectionLayer;

  public edit Bool m_isInternalWeakspot;

  public edit Float m_hitShapeDamageMod;

  public final static Float GetHitShapeDamageMod(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitShapeDamageMod;
  }

  public final static Bool IsProtectionLayer(ref<HitShapeUserDataBase> userData) {
    return userData.m_isProtectionLayer;
  }

  public final static Bool IsInternalWeakspot(ref<HitShapeUserDataBase> userData) {
    return userData.m_isInternalWeakspot;
  }

  public final const Bool IsHead() {
    return this.m_hitShapeTag == "Head";
  }

  public final const EHitShapeType GetShapeType() {
    return this.m_hitShapeType;
  }

  public final static void DisableHitShape(wref<GameObject> gameObj, CName shapeName, Bool hierarchical) {
    ref<ToggleHitShapeEvent> hitShapeEvent;
    hitShapeEvent = new ToggleHitShapeEvent();
    hitShapeEvent.enable = false;
    hitShapeEvent.hitShapeName = shapeName;
    hitShapeEvent.hierarchical = hierarchical;
    WeakRefToRef(gameObj).QueueEvent(hitShapeEvent);
  }

  public final static void EnableHitShape(wref<GameObject> gameObj, CName shapeName, Bool hierarchical) {
    ref<ToggleHitShapeEvent> hitShapeEvent;
    hitShapeEvent = new ToggleHitShapeEvent();
    hitShapeEvent.enable = true;
    hitShapeEvent.hitShapeName = shapeName;
    hitShapeEvent.hierarchical = hierarchical;
    WeakRefToRef(gameObj).QueueEvent(hitShapeEvent);
  }

  public final static EHitReactionZone GetHitReactionZone(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone;
  }

  public final static gameDismBodyPart GetDismembermentBodyPart(ref<HitShapeUserDataBase> userData) {
    String str;
    gameDismBodyPart dismBodyPart;
    str = EnumValueToString("EAIDismembermentBodyPart", Cast(ToInt(userData.m_dismembermentPart)));
    dismBodyPart = ToEnum(Cast(EnumValueFromString("gameDismBodyPart", str)));
    return dismBodyPart;
  }

  public final static Bool IsHitReactionZoneHead(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.Head;
  }

  public final static Bool IsHitReactionZoneTorso(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.Abdomen || userData.m_hitReactionZone == EHitReactionZone.ChestLeft || userData.m_hitReactionZone == EHitReactionZone.ChestRight;
  }

  public final static Bool IsHitReactionZoneLeftArm(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.ArmLeft || userData.m_hitReactionZone == EHitReactionZone.HandLeft;
  }

  public final static Bool IsHitReactionZoneRightArm(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.ArmRight || userData.m_hitReactionZone == EHitReactionZone.ArmRight;
  }

  public final static Bool IsHitReactionZoneRightLeg(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.LegRight;
  }

  public final static Bool IsHitReactionZoneLeftLeg(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.LegLeft;
  }

  public final static Bool IsHitReactionZoneLeg(ref<HitShapeUserDataBase> userData) {
    return userData.m_hitReactionZone == EHitReactionZone.LegLeft || userData.m_hitReactionZone == EHitReactionZone.LegRight;
  }

  public final static Bool IsHitReactionZoneLimb(ref<HitShapeUserDataBase> userData) {
    return IsHitReactionZoneLeftArm(userData) || IsHitReactionZoneRightArm(userData) || IsHitReactionZoneRightLeg(userData) || IsHitReactionZoneLeftLeg(userData);
  }
}
