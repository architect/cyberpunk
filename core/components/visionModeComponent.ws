
public class RevealQuestTargetEvent extends Event {

  public edit CName sourceName;

  [Default(RevealQuestTargetEvent, ERevealDurationType.TEMPORARY))]
  public edit ERevealDurationType durationType;

  [Default(RevealQuestTargetEvent, true))]
  public edit Bool reveal;

  [Default(RevealQuestTargetEvent, 4.0f))]
  public Float timeout;

  public final String GetFriendlyDescription() {
    return "Reveal Quest Target";
  }
}

public class ToggleForcedHighlightEvent extends Event {

  public edit CName sourceName;

  public inline edit ref<HighlightEditableData> highlightData;

  public edit EToggleOperationType operation;

  public final String GetFriendlyDescription() {
    return "Toggle Forced Highlight";
  }
}

public class SetPersistentForcedHighlightEvent extends Event {

  public edit CName sourceName;

  public inline edit ref<HighlightEditableData> highlightData;

  public edit EToggleOperationType operation;

  public final String GetFriendlyDescription() {
    return "Set Persitent Forced Highlight";
  }
}

public class SetDefaultHighlightEvent extends Event {

  public inline edit ref<HighlightEditableData> highlightData;

  public final String GetFriendlyDescription() {
    return "Set Default Highlight";
  }
}

public class FocusForcedHighlightPersistentData extends IScriptable {

  private persistent EntityID sourceID;

  private persistent CName sourceName;

  private persistent EFocusForcedHighlightType highlightType;

  private persistent EFocusOutlineType outlineType;

  private persistent EPriority priority;

  private persistent Float inTransitionTime;

  private persistent Float outTransitionTime;

  private persistent Bool isRevealed;

  private persistent VisionModePatternType patternType;

  public final void Initialize(ref<FocusForcedHighlightData> data) {
    if(data == null) {
      return ;
    };
    this.sourceID = data.sourceID;
    this.sourceName = data.sourceName;
    this.highlightType = data.highlightType;
    this.outlineType = data.outlineType;
    this.priority = data.priority;
    this.inTransitionTime = data.inTransitionTime;
    this.outTransitionTime = data.outTransitionTime;
    this.isRevealed = data.isRevealed;
    this.patternType = data.patternType;
  }

  public final const ref<FocusForcedHighlightData> GetData() {
    ref<FocusForcedHighlightData> data;
    data = new FocusForcedHighlightData();
    data.sourceID = this.sourceID;
    data.sourceName = this.sourceName;
    data.highlightType = this.highlightType;
    data.outlineType = this.outlineType;
    data.priority = this.priority;
    data.inTransitionTime = this.inTransitionTime;
    data.outTransitionTime = this.outTransitionTime;
    data.isRevealed = this.isRevealed;
    data.patternType = this.patternType;
    data.isSavable = true;
    return data;
  }
}

public class FocusForcedHighlightData extends IScriptable {

  public EntityID sourceID;

  public CName sourceName;

  public EFocusForcedHighlightType highlightType;

  [Default(FocusForcedHighlightData, EFocusOutlineType.INVALID))]
  public EFocusOutlineType outlineType;

  public EPriority priority;

  [Default(FocusForcedHighlightData, 0.5f))]
  public Float inTransitionTime;

  [Default(FocusForcedHighlightData, 2f))]
  public Float outTransitionTime;

  public ref<HighlightInstance> hudData;

  public Bool isRevealed;

  public Bool isSavable;

  public VisionModePatternType patternType;

  public final Bool IsValid() {
    return IsNameValid(this.sourceName) || IsDefined(this.sourceID) && this.highlightType != EFocusForcedHighlightType.INVALID || this.outlineType != EFocusOutlineType.INVALID;
  }

  public final void InitializeWithHudInstruction(ref<HighlightInstance> data) {
    this.hudData = data;
    this.isRevealed = data.isRevealed;
    if(data.instant) {
      this.inTransitionTime = 0;
      this.outTransitionTime = 0;
    };
  }

  private final Int32 GetFillColorIndex() {
    switch(this.highlightType) {
      case EFocusForcedHighlightType.INTERACTION:
        return 2;
      case EFocusForcedHighlightType.IMPORTANT_INTERACTION:
        return 5;
      case EFocusForcedHighlightType.WEAKSPOT:
        return 6;
      case EFocusForcedHighlightType.QUEST:
        return 1;
      case EFocusForcedHighlightType.DISTRACTION:
        return 3;
      case EFocusForcedHighlightType.CLUE:
        return 4;
      case EFocusForcedHighlightType.NPC:
        return 0;
      case EFocusForcedHighlightType.AOE:
        return 7;
      case EFocusForcedHighlightType.ITEM:
        return 5;
      case EFocusForcedHighlightType.HOSTILE:
        return 7;
      case EFocusForcedHighlightType.FRIENDLY:
        return 4;
      case EFocusForcedHighlightType.NEUTRAL:
        return 2;
      case EFocusForcedHighlightType.HACKABLE:
        return 4;
      case EFocusForcedHighlightType.ENEMY_NETRUNNER:
        return 6;
      case EFocusForcedHighlightType.BACKDOOR:
        return 5;
      default:
        return 0;
    };
  }

  private final Int32 GetOutlineColorIndex() {
    switch(this.outlineType) {
      case EFocusOutlineType.HOSTILE:
        return 2;
      case EFocusOutlineType.FRIENDLY:
        return 1;
      case EFocusOutlineType.NEUTRAL:
        return 3;
      case EFocusOutlineType.ITEM:
        return 6;
      case EFocusOutlineType.INTERACTION:
        return 3;
      case EFocusOutlineType.IMPORTANT_INTERACTION:
        return 6;
      case EFocusOutlineType.QUEST:
        return 5;
      case EFocusOutlineType.CLUE:
        return 1;
      case EFocusOutlineType.DISTRACTION:
        return 7;
      case EFocusOutlineType.AOE:
        return 2;
      case EFocusOutlineType.HACKABLE:
        return 1;
      case EFocusOutlineType.WEAKSPOT:
        return 4;
      case EFocusOutlineType.ENEMY_NETRUNNER:
        return 4;
      case EFocusOutlineType.BACKDOOR:
        return 6;
      default:
        return 0;
    };
  }

  public final VisionAppearance GetVisionApperance() {
    VisionAppearance apperance;
    apperance.patternType = this.patternType;
    if(this.hudData == null) {
      apperance.fill = GetFillColorIndex();
      apperance.outline = GetOutlineColorIndex();
    } else {
      if(this.hudData.context == HighlightContext.FILL) {
        apperance.fill = GetFillColorIndex();
      } else {
        if(this.hudData.context == HighlightContext.OUTLINE) {
          apperance.outline = GetOutlineColorIndex();
        } else {
          if(this.hudData.context == HighlightContext.FULL) {
            apperance.fill = GetFillColorIndex();
            apperance.outline = GetOutlineColorIndex();
          };
        };
      };
      apperance.showThroughWalls = this.hudData.isRevealed;
    };
    switch(this.highlightType) {
      case EFocusForcedHighlightType.QUEST:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.INTERACTION:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.IMPORTANT_INTERACTION:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.WEAKSPOT:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.DISTRACTION:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.CLUE:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.NPC:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.AOE:
        apperance.showThroughWalls = false;
        break;
      case EFocusForcedHighlightType.ITEM:
        apperance.showThroughWalls = false;
        break;
      default:
    };
    return apperance;
  }
}

public final class gameVisionModeComponentPS extends GameComponentPS {

  private persistent ref<FocusForcedHighlightPersistentData> m_storedHighlightData;

  public final void StoreHighlightData(ref<FocusForcedHighlightData> data) {
    if(ToBool(data)) {
      this.m_storedHighlightData = new FocusForcedHighlightPersistentData();
      this.m_storedHighlightData.Initialize(data);
    } else {
      this.m_storedHighlightData = null;
    };
  }

  public final const ref<FocusForcedHighlightData> GetStoredHighlightData() {
    if(this.m_storedHighlightData != null) {
      return this.m_storedHighlightData.GetData();
    };
    return null;
  }

  private final EntityNotificationType OnSetPersistentForcedHighlightEvent(ref<SetPersistentForcedHighlightEvent> evt) {
    ref<FocusForcedHighlightData> highlight;
    if(evt.operation == EToggleOperationType.REMOVE) {
      StoreHighlightData(null);
    } else {
      highlight = new FocusForcedHighlightData();
      highlight.sourceID = ExtractEntityID(GetID());
      highlight.sourceName = evt.sourceName;
      highlight.highlightType = evt.highlightData.highlightType;
      highlight.outlineType = evt.highlightData.outlineType;
      highlight.inTransitionTime = evt.highlightData.inTransitionTime;
      highlight.outTransitionTime = evt.highlightData.outTransitionTime;
      highlight.priority = evt.highlightData.priority;
      highlight.isRevealed = evt.highlightData.isRevealed;
      highlight.isSavable = true;
      StoreHighlightData(highlight);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }
}

public final class VisionModeComponent extends GameComponent {

  private inline ref<HighlightEditableData> m_defaultHighlightData;

  private array<ref<FocusForcedHighlightData>> m_forcedHighlights;

  private ref<FocusForcedHighlightData> m_activeForcedHighlight;

  private ref<FocusForcedHighlightData> m_currentDefaultHighlight;

  private array<gameVisionModeSystemRevealIdentifier> m_activeRevealRequests;

  private Bool m_isFocusModeActive;

  private Bool m_wasCleanedUp;

  public final native void SetHiddenInVisionMode(Bool hidden, gameVisionModeType type)

  protected final void OnGameAttach() {
    GetVisionModeSystem().GetDelayedRevealEntries(GetOwner().GetEntityID(), this.m_activeRevealRequests);
    AddForcedHighlight(GetMyPS().GetStoredHighlightData());
  }

  protected final void OnGameDetach() {
    if(ToBool(this.m_activeForcedHighlight) && this.m_activeForcedHighlight.isSavable) {
      GetMyPS().StoreHighlightData(this.m_activeForcedHighlight);
    };
  }

  protected final cb Bool OnRestoreRevealEvent(ref<RestoreRevealStateEvent> evt) {
    RestoreReveal();
  }

  private final void RestoreReveal() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_activeRevealRequests)) {
      if(i == 0) {
        SendRevealStateChangedEvent(ERevealState.STARTED, this.m_activeRevealRequests[i]);
      } else {
        SendRevealStateChangedEvent(ERevealState.CONTINUE, this.m_activeRevealRequests[i]);
      };
      i += 1;
    };
  }

  private final const ref<gameVisionModeComponentPS> GetMyPS() {
    return Cast(GetPS());
  }

  private final const ref<GameObject> GetOwner() {
    return Cast(WeakRefToRef(GetEntity()));
  }

  private final void AddForcedHighlight(ref<FocusForcedHighlightData> data) {
    if(data == null || !data.IsValid()) {
      return ;
    };
    if(!HasForcedHighlightOnStack(data)) {
      Push(this.m_forcedHighlights, data);
    };
    if(Size(this.m_forcedHighlights) > 0) {
      EvaluateForcedHighLightsStack();
    };
  }

  private final void RemoveForcedHighlight(ref<FocusForcedHighlightData> data, Bool ignoreStackEvaluation?) {
    Int32 i;
    Bool evaluate;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(this.m_forcedHighlights[i].sourceID == data.sourceID) {
        if(this.m_forcedHighlights[i].sourceName == data.sourceName) {
          if(this.m_forcedHighlights[i].highlightType == data.highlightType && this.m_forcedHighlights[i].outlineType == data.outlineType) {
            this.m_forcedHighlights[i] = null;
            Erase(this.m_forcedHighlights, i);
            evaluate = true;
          } else {
            i += 1;
          };
        } else {
        };
      };
      i += 1;
    };
    if(evaluate && !ignoreStackEvaluation) {
      EvaluateForcedHighLightsStack();
    };
  }

  private final void EvaluateForcedHighLightsStack() {
    Int32 i;
    ref<FocusForcedHighlightData> currentForcedHighlight;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(currentForcedHighlight == null || currentForcedHighlight != null && ToInt(this.m_forcedHighlights[i].priority) >= ToInt(currentForcedHighlight.priority)) {
        currentForcedHighlight = this.m_forcedHighlights[i];
      };
      i += 1;
    };
    UpdateActiveForceHighlight(currentForcedHighlight);
  }

  protected final cb Bool OnAIAction(ref<AIEvent> evt) {
    if(evt.name == "NewWeaponEquipped") {
      ForwardHighlightToSlaveEntity(this.m_activeForcedHighlight, true);
    };
  }

  private final void UpdateActiveForceHighlight(ref<FocusForcedHighlightData> data) {
    if(data != this.m_activeForcedHighlight) {
      if(data != null) {
        ForceVisionAppearance(data);
        this.m_activeForcedHighlight = data;
      } else {
        if(this.m_activeForcedHighlight != null) {
          CancelForcedVisionAppearance(this.m_activeForcedHighlight.outTransitionTime);
          this.m_activeForcedHighlight = null;
        };
      };
    };
  }

  private final void ReactivateForceHighlight() {
    if(this.m_activeForcedHighlight != null) {
      ForceVisionAppearance(this.m_activeForcedHighlight);
    };
  }

  private final Bool HasForcedHighlightOnStack(ref<FocusForcedHighlightData> data) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(this.m_forcedHighlights[i].sourceID == data.sourceID) {
        if(this.m_forcedHighlights[i].sourceName == data.sourceName) {
          if(this.m_forcedHighlights[i].highlightType == data.highlightType && this.m_forcedHighlights[i].outlineType == data.outlineType) {
            return true;
          };
        };
      };
      i += 1;
    };
    return false;
  }

  private final void ForceVisionAppearance(ref<FocusForcedHighlightData> data) {
    VisionAppearance appearance;
    appearance = data.GetVisionApperance();
    if(IsRevealed() || data.isRevealed) {
      data.isRevealed = true;
      appearance.showThroughWalls = true;
    } else {
      data.isRevealed = false;
      appearance.showThroughWalls = false;
    };
    GetVisionModeSystem(GetOwner().GetGame()).ForceVisionAppearance(GetOwner(), appearance, data.inTransitionTime);
    ForwardHighlightToSlaveEntity(data, true);
  }

  private final void CancelForcedVisionAppearance(Float transitionTime) {
    GetVisionModeSystem(GetOwner().GetGame()).CancelForceVisionAppearance(GetOwner(), transitionTime);
    ForwardHighlightToSlaveEntity(this.m_activeForcedHighlight, false);
  }

  private final void PulseObject() {
    VisionAppearance emptyAppearance;
    ref<FocusForcedHighlightData> targetHighlight;
    ref<HighlightInstance> data;
    if(this.m_activeForcedHighlight == null) {
      data = new HighlightInstance();
      data.SetContext(HighlightContext.FULL, false, false);
      targetHighlight = GetDefaultHighlight();
      if(targetHighlight != null) {
        GetVisionModeSystem(GetOwner().GetGame()).RequestPulse(GetOwner(), emptyAppearance, targetHighlight.GetVisionApperance(), 0.4000000059604645, 1);
      };
    };
  }

  private final const ref<FocusForcedHighlightData> GetDefaultHighlight(ref<HighlightInstance> data?) {
    ref<FocusForcedHighlightData> highlight;
    if(GetOwner().IsBraindanceBlocked() || GetOwner().IsPhotoModeBlocked()) {
      return null;
    };
    if(this.m_defaultHighlightData != null) {
      highlight = new FocusForcedHighlightData();
      highlight.sourceID = GetOwner().GetEntityID();
      highlight.sourceName = GetOwner().GetClassName();
      highlight.outlineType = this.m_defaultHighlightData.outlineType;
      highlight.highlightType = this.m_defaultHighlightData.highlightType;
      highlight.priority = this.m_defaultHighlightData.priority;
      highlight.inTransitionTime = this.m_defaultHighlightData.inTransitionTime;
      highlight.outTransitionTime = this.m_defaultHighlightData.outTransitionTime;
    } else {
      highlight = GetOwner().GetDefaultHighlight();
      if(data != null) {
        highlight.InitializeWithHudInstruction(data);
      };
    };
    return highlight;
  }

  public final void ToggleRevealObject(Bool reveal, Bool forced?) {
    if(reveal) {
      if(this.m_activeForcedHighlight != null && !this.m_activeForcedHighlight.isRevealed) {
        if(forced) {
          this.m_activeForcedHighlight.isRevealed = reveal;
        };
        CancelForcedVisionAppearance(0.30000001192092896);
        ReactivateForceHighlight();
      };
    } else {
      if(this.m_activeForcedHighlight != null && this.m_activeForcedHighlight.isRevealed) {
        if(forced) {
          this.m_activeForcedHighlight.isRevealed = reveal;
        };
        CancelForcedVisionAppearance(0.30000001192092896);
        ReactivateForceHighlight();
      };
    };
  }

  private final Bool IsTagged() {
    return GetOwner().IsTaggedinFocusMode();
  }

  private final Int32 AddRevealRequest(gameVisionModeSystemRevealIdentifier data) {
    if(!HasRevealRequest(data)) {
      Push(this.m_activeRevealRequests, data);
      return Size(this.m_activeRevealRequests) - 1;
    };
    return -1;
  }

  private final void RemoveRevealRequest(gameVisionModeSystemRevealIdentifier data) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_activeRevealRequests)) {
      if(this.m_activeRevealRequests[i].reason == data.reason && this.m_activeRevealRequests[i].sourceEntityId == data.sourceEntityId) {
        Erase(this.m_activeRevealRequests, i);
      } else {
        i += 1;
      };
    };
  }

  public final Bool HasRevealRequest(gameVisionModeSystemRevealIdentifier data) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_activeRevealRequests)) {
      if(this.m_activeRevealRequests[i].reason == data.reason && this.m_activeRevealRequests[i].sourceEntityId == data.sourceEntityId) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const Int32 GetRevealRequestIndex(gameVisionModeSystemRevealIdentifier data) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_activeRevealRequests)) {
      if(IsRequestTheSame(this.m_activeRevealRequests[i], data)) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final const Bool IsRequestTheSame(gameVisionModeSystemRevealIdentifier request1, gameVisionModeSystemRevealIdentifier request2) {
    if(request1.reason == request2.reason && request1.sourceEntityId == request2.sourceEntityId) {
      return true;
    };
    return false;
  }

  public final const Bool IsRevealed() {
    return Size(this.m_activeRevealRequests) > 0;
  }

  private final Bool IsRevealRequestIndexValid(Int32 index) {
    if(index >= 0 && index < Size(this.m_activeRevealRequests)) {
      return true;
    };
    return false;
  }

  private final void UpdateDefaultHighlight(ref<FocusForcedHighlightData> data) {
    if(data == null) {
      if(this.m_currentDefaultHighlight != null) {
        RemoveForcedHighlight(this.m_currentDefaultHighlight);
        this.m_currentDefaultHighlight = null;
      };
    } else {
      if(this.m_currentDefaultHighlight == null) {
        AddForcedHighlight(data);
        this.m_currentDefaultHighlight = data;
      } else {
        if(this.m_currentDefaultHighlight != null && !CompareHighlightData(this.m_currentDefaultHighlight, data)) {
          RemoveForcedHighlight(this.m_currentDefaultHighlight, true);
          AddForcedHighlight(data);
          this.m_currentDefaultHighlight = data;
        };
      };
    };
  }

  private final void RequestHUDRefresh() {
    ref<RefreshActorRequest> request;
    request = new RefreshActorRequest();
    request.ownerID = GetOwner().GetEntityID();
    GetOwner().GetHudManager().QueueRequest(request);
  }

  private final ref<VisionModeSystem> GetVisionModeSystem() {
    return GetVisionModeSystem(GetOwner().GetGame());
  }

  private final void SendRevealStateChangedEvent(ERevealState state, gameVisionModeSystemRevealIdentifier reason) {
    ref<RevealStateChangedEvent> evt;
    ref<RevealStatusNotification> request;
    ref<HUDManager> hudManager;
    if(state != ERevealState.CONTINUE) {
      hudManager = GetOwner().GetHudManager();
      if(ToBool(hudManager)) {
        request = new RevealStatusNotification();
        request.ownerID = GetOwner().GetEntityID();
        if(state == ERevealState.STARTED) {
          request.isRevealed = true;
        } else {
          if(state == ERevealState.STOPPED) {
            request.isRevealed = false;
          };
        };
        hudManager.QueueRequest(request);
      };
    };
    evt = new RevealStateChangedEvent();
    evt.state = state;
    evt.reason = reason;
    if(ToBool(this.m_activeForcedHighlight)) {
      evt.transitionTime = this.m_activeForcedHighlight.outTransitionTime;
    };
    GetOwner().QueueEvent(evt);
  }

  private final Bool ClearAllReavealRequests() {
    Int32 i;
    Bool evaluate;
    gameVisionModeSystemRevealIdentifier lastRequest;
    i = Size(this.m_activeRevealRequests) - 1;
    while(i >= 0) {
      if(this.m_activeRevealRequests[i].reason != "tag") {
        evaluate = true;
        if(i == 0) {
          lastRequest = this.m_activeRevealRequests[i];
        } else {
          Erase(this.m_activeRevealRequests, i);
          i -= 1;
        };
      } else {
      };
      i -= 1;
    };
    if(evaluate) {
      RevealObject(false, lastRequest, 0);
    };
    return evaluate;
  }

  private final Bool ClearForcedHighlights() {
    Int32 i;
    Bool evaluate;
    i = Size(this.m_forcedHighlights) - 1;
    while(i >= 0) {
      if(this.m_forcedHighlights[i] != this.m_currentDefaultHighlight) {
        Erase(this.m_forcedHighlights, i);
        evaluate = true;
      };
      i -= 1;
    };
    if(evaluate) {
      EvaluateForcedHighLightsStack();
    };
    return evaluate;
  }

  private final Bool CompareHighlightData(ref<FocusForcedHighlightData> data1, ref<FocusForcedHighlightData> data2) {
    if(data1.patternType != data2.patternType) {
      return false;
    };
    if(data1.sourceID != data2.sourceID || data1.sourceName != data2.sourceName) {
      return false;
    };
    if(data1.highlightType != data2.highlightType || data1.outlineType != data2.outlineType) {
      return false;
    };
    if(ToBool(data1.hudData) && !ToBool(data2.hudData) || ToBool(data2.hudData) && !ToBool(data1.hudData)) {
      return false;
    };
    if(ToBool(data1.hudData) && ToBool(data2.hudData)) {
      if(data1.hudData.context != data2.hudData.context) {
        return false;
      };
      if(data1.hudData.isRevealed != data2.hudData.isRevealed) {
        return false;
      };
    };
    return true;
  }

  protected final void ForwardHighlightToSlaveEntity(ref<FocusForcedHighlightData> data, Bool apply) {
    ref<ForceVisionApperanceEvent> evt;
    array<wref<GameObject>> objectsToHighlight;
    Int32 i;
    objectsToHighlight = GetOwner().GetObjectToForwardHighlight();
    if(Size(objectsToHighlight) < 0) {
      return ;
    };
    evt = new ForceVisionApperanceEvent();
    evt.forcedHighlight = data;
    evt.apply = apply;
    evt.forceCancel = true;
    evt.ignoreStackEvaluation = apply;
    i = 0;
    while(i < Size(objectsToHighlight)) {
      GetOwner().QueueEventForEntityID(WeakRefToRef(objectsToHighlight[i]).GetEntityID(), evt);
      i += 1;
    };
  }

  protected final cb Bool OnForceVisionApperance(ref<ForceVisionApperanceEvent> evt) {
    ref<ResponseEvent> responseEvt;
    gameVisionModeSystemRevealIdentifier reason;
    if(evt.forceCancel) {
      if(this.m_activeForcedHighlight != null) {
        RemoveForcedHighlight(this.m_activeForcedHighlight, evt.ignoreStackEvaluation);
      } else {
        CancelForcedVisionAppearance(0);
      };
    };
    if(evt.forcedHighlight != null) {
      if(evt.apply) {
        if(evt.forcedHighlight.isRevealed) {
          reason.reason = evt.forcedHighlight.sourceName;
          reason.sourceEntityId = evt.forcedHighlight.sourceID;
          RevealObject(true, reason, 0);
        };
        AddForcedHighlight(evt.forcedHighlight);
      } else {
        if(evt.forcedHighlight.isRevealed) {
          reason.reason = evt.forcedHighlight.sourceName;
          reason.sourceEntityId = evt.forcedHighlight.sourceID;
          RevealObject(false, reason, 0);
        };
        RemoveForcedHighlight(evt.forcedHighlight, evt.ignoreStackEvaluation);
      };
      if(ToBool(evt.responseData)) {
        responseEvt = new ResponseEvent();
        responseEvt.responseData = evt.responseData;
        GetOwner().QueueEventForEntityID(evt.forcedHighlight.sourceID, responseEvt);
      };
    };
  }

  protected final cb Bool OnRevealObject(ref<RevealObjectEvent> evt) {
    RevealObject(evt.reveal, evt.reason, evt.lifetime);
  }

  protected final cb Bool OnVisionRevealExpiredEvent(ref<gameVisionRevealExpiredEvent> evt) {
    RevealObject(false, evt.revealId, 0);
  }

  private final void RevealObject(Bool reveal, gameVisionModeSystemRevealIdentifier reason, Float lifetime) {
    Int32 index;
    Bool isIndexValid;
    Float cachedLifetime;
    DelayID emptyDelayID;
    ref<RevealObjectEvent> evt;
    ref<VisionModeSystem> visonModeSystem;
    if(reveal) {
      visonModeSystem = GetVisionModeSystem();
      index = GetRevealRequestIndex(reason);
      if(IsRevealRequestIndexValid(index) && visonModeSystem.IsDelayedRevealInProgress(GetOwner().GetEntityID(), reason)) {
        visonModeSystem.UnregisterDelayedReveal(GetOwner().GetEntityID(), reason);
        RemoveRevealRequest(reason);
      };
      if(!IsRevealed()) {
        SendRevealStateChangedEvent(ERevealState.STARTED, reason);
      } else {
        SendRevealStateChangedEvent(ERevealState.CONTINUE, reason);
      };
      index = AddRevealRequest(reason);
      if(lifetime > 0) {
        if(!IsRevealRequestIndexValid(index)) {
          return ;
        };
        RemoveRevealWithDelay(reason, lifetime);
        return ;
      };
    } else {
      index = GetRevealRequestIndex(reason);
      if(!IsRevealRequestIndexValid(index)) {
        return ;
      };
      if(lifetime > 0) {
        RemoveRevealWithDelay(reason, lifetime);
        return ;
      };
      RemoveRevealRequest(reason);
      if(!IsRevealed()) {
        SendRevealStateChangedEvent(ERevealState.STOPPED, reason);
      } else {
        SendRevealStateChangedEvent(ERevealState.CONTINUE, reason);
      };
    };
  }

  private final void RemoveRevealWithDelay(gameVisionModeSystemRevealIdentifier reason, Float lifetime) {
    GetVisionModeSystem().RegisterDelayedReveal(GetOwner().GetEntityID(), reason, lifetime);
  }

  protected final cb Bool OnForceReactivateHighlights(ref<ForceReactivateHighlightsEvent> evt) {
    ReactivateForceHighlight();
  }

  protected final cb Bool OnHUDInstruction(ref<HUDInstruction> evt) {
    ref<FocusForcedHighlightData> highlight;
    if(evt.braindanceInstructions.GetState() == InstanceState.ON) {
      if(GetOwner().IsBraindanceBlocked() || GetOwner().IsPhotoModeBlocked()) {
        UpdateDefaultHighlight(null);
        ToggleRevealObject(false);
        return false;
      };
    };
    if(evt.highlightInstructions.GetState() == InstanceState.ON) {
      highlight = GetDefaultHighlight(evt.highlightInstructions);
      UpdateDefaultHighlight(highlight);
      ToggleRevealObject(evt.highlightInstructions.isRevealed);
    } else {
      if(evt.highlightInstructions.WasProcessed()) {
        UpdateDefaultHighlight(null);
        ToggleRevealObject(false);
      };
    };
  }

  protected final cb Bool OnPulseEvent(ref<gameVisionModeUpdateVisuals> evt) {
    if(evt.pulse) {
      PulseObject();
    };
  }

  protected final cb Bool OnDeath(ref<gameDeathEvent> evt) {
    if(!this.m_wasCleanedUp) {
      CleanUp();
    };
  }

  protected final cb Bool OnDefeated(ref<DefeatedEvent> evt) {
    if(!this.m_wasCleanedUp) {
      CleanUp();
    };
  }

  private final void CleanUp() {
    ClearAllReavealRequests();
    ClearForcedHighlights();
  }

  protected final cb Bool OnForceUpdateDefultHighlight(ref<ForceUpdateDefaultHighlightEvent> evt) {
    RequestHUDRefresh();
  }

  protected final cb Bool OnSetForcedDefaultHighlight(ref<SetDefaultHighlightEvent> evt) {
    this.m_defaultHighlightData = evt.highlightData;
    RequestHUDRefresh();
  }

  protected final cb Bool OnRevealQuestTargetEvent(ref<RevealQuestTargetEvent> evt) {
    gameVisionModeSystemRevealIdentifier revealData;
    Float duration;
    revealData.reason = evt.sourceName;
    if(evt.durationType == ERevealDurationType.TEMPORARY && evt.reveal == true) {
      duration = evt.timeout;
    } else {
      duration = 0;
    };
    RevealObject(evt.reveal, revealData, duration);
  }

  protected final cb Bool OnSetPersistentForcedHighlightEvent(ref<SetPersistentForcedHighlightEvent> evt) {
    ToggleForcedHighlight(evt.sourceName, evt.highlightData, evt.operation);
  }

  protected final cb Bool OnToggleForcedHighlightEvent(ref<ToggleForcedHighlightEvent> evt) {
    ToggleForcedHighlight(evt.sourceName, evt.highlightData, evt.operation);
  }

  private final void ToggleForcedHighlight(CName sourceName, ref<HighlightEditableData> highlightData, EToggleOperationType operation) {
    ref<FocusForcedHighlightData> highlight;
    gameVisionModeSystemRevealIdentifier reason;
    highlight = new FocusForcedHighlightData();
    highlight.sourceID = GetOwner().GetEntityID();
    highlight.sourceName = sourceName;
    highlight.highlightType = highlightData.highlightType;
    highlight.outlineType = highlightData.outlineType;
    highlight.inTransitionTime = highlightData.inTransitionTime;
    highlight.outTransitionTime = highlightData.outTransitionTime;
    highlight.priority = highlightData.priority;
    highlight.isSavable = true;
    if(operation == EToggleOperationType.ADD) {
      AddForcedHighlight(highlight);
      if(highlightData.isRevealed) {
        reason.reason = sourceName;
        reason.sourceEntityId = GetOwner().GetEntityID();
        RevealObject(true, reason, 0);
      };
    } else {
      RemoveForcedHighlight(highlight);
      if(highlightData.isRevealed) {
        reason.reason = sourceName;
        reason.sourceEntityId = GetOwner().GetEntityID();
        RevealObject(false, reason, 0);
      };
    };
  }

  public final const Bool HasDefaultHighlight() {
    if(GetOwner().IsBraindanceBlocked() || GetOwner().IsPhotoModeBlocked()) {
      return false;
    };
    if(ToBool(this.m_defaultHighlightData)) {
      return true;
    };
    return GetOwner().GetDefaultHighlight() != null;
  }

  public final const Bool HasOutlineOrFill(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(this.m_forcedHighlights[i].highlightType == highlightType || this.m_forcedHighlights[i].outlineType == outlineType) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const Bool HasHighlight(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(this.m_forcedHighlights[i].highlightType == highlightType && this.m_forcedHighlights[i].outlineType == outlineType) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const Bool HasHighlight(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType, EntityID sourceID) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(this.m_forcedHighlights[i].highlightType == highlightType && this.m_forcedHighlights[i].outlineType == outlineType && this.m_forcedHighlights[i].sourceID == sourceID) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const Bool HasHighlight(EFocusForcedHighlightType highlightType, EFocusOutlineType outlineType, EntityID sourceID, CName sourceName) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forcedHighlights)) {
      if(this.m_forcedHighlights[i].highlightType == highlightType && this.m_forcedHighlights[i].outlineType == outlineType && this.m_forcedHighlights[i].sourceID == sourceID && this.m_forcedHighlights[i].sourceName == sourceName) {
        return true;
      };
      i += 1;
    };
    return false;
  }
}
