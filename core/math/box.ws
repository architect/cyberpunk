
public struct Box {

  public native Vector4 Min;

  public native Vector4 Max;

  public final static Vector4 GetSize(Box box) {
    return box.Max - box.Min;
  }

  public final static Vector4 GetExtents(Box box) {
    return box.Max - box.Min * 0.5;
  }

  public final static Float GetRange(Box box) {
    Vector4 size;
    size = GetExtents(box);
    if(size.X > size.Y && size.X > size.Z) {
      return size.X;
    };
    if(size.Y > size.Z) {
      return size.Y;
    };
    return size.Z;
  }
}
