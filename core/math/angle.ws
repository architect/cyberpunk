
public static Float AngleNormalize180(Float a) {
  if(a >= -180 && a < 180) {
    return a;
  };
  if(a < -360 || a > 360) {
    a = AngleNormalize(a);
  };
  if(a > 180) {
    a -= 360;
  } else {
    if(a < -180) {
      a += 360;
    };
  };
  return a;
}

public static Float LerpAngleF(Float alpha, Float a, Float b) {
  return a + AngleDistance(b, a) * alpha;
}
