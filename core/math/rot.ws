
public static EulerAngles GetOppositeRotation180(EulerAngles rot) {
  EulerAngles ret;
  ret.Pitch = AngleNormalize180(rot.Pitch + 180);
  ret.Yaw = AngleNormalize180(rot.Yaw + 180);
  ret.Roll = AngleNormalize180(rot.Roll + 180);
  return ret;
}
