
public static Float Pi() {
  return 3.1415927410125732;
}

public static Float HalfPi() {
  return 1.5707963705062866;
}

public static Float SgnF(Float a) {
  if(a > 0) {
    return 1;
  };
  return -1;
}

public static Float ModF(Float a, Float b) {
  if(b < 0 || a < 0) {
    return 0;
  };
  return a - Cast(FloorF(a / b)) * b;
}

public static Float ProportionalClampF(Float inMin, Float inMax, Float v, Float outMin, Float outMax) {
  Float lerp;
  Float inputRange;
  inputRange = inMax - inMin;
  v = ClampF(v, inMin, inMax);
  if(AbsF(inputRange) > 0.0005000000237487257) {
    lerp = v - inMin / inputRange;
  } else {
    lerp = 0;
  };
  v = LerpF(lerp, outMin, outMax);
  return v;
}

public static Int32 RoundMath(Float f) {
  if(f == 0) {
    return Cast(f);
  };
  if(f > 0) {
    if(f - Cast(FloorF(f)) >= 0.5) {
      return CeilF(f);
    };
    return FloorF(f);
  };
  if(f + Cast(FloorF(f)) >= -0.5) {
    return FloorF(f);
  };
  return CeilF(f);
}

public static Float RoundTo(Float f, Int32 decimal) {
  Int32 i;
  Int32 digit;
  Float ret;
  Bool isNeg;
  if(decimal < 0) {
    decimal = 0;
  };
  ret = Cast(FloorF(AbsF(f)));
  isNeg = false;
  if(f < 0) {
    isNeg = true;
    f *= -1;
  };
  f -= ret;
  i = 0;
  while(i < decimal) {
    f *= 10;
    digit = FloorF(f);
    ret += Cast(digit) / PowF(10, Cast(i + 1));
    f -= Cast(digit);
    i += 1;
  };
  if(isNeg) {
    ret *= -1;
  };
  return ret;
}

public static Bool FloatIsEqual(Float f, Float to) {
  return AbsF(f - to) < 0.0000019999999949504854;
}
