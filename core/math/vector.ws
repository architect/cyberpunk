
public struct Vector4 {

  public native Float X;

  public native Float Y;

  public native Float Z;

  public native Float W;

  public final static native Float Dot2D(Vector4 a, Vector4 b)

  public final static native Float Dot(Vector4 a, Vector4 b)

  public final static native Vector4 Cross(Vector4 a, Vector4 b)

  public final static native Float Length2D(Vector4 a)

  public final static native Float LengthSquared(Vector4 a)

  public final static native Float Length(Vector4 a)

  public final static native Vector4 Normalize2D(Vector4 a)

  public final static native Vector4 Normalize(Vector4 a)

  public final static native Vector4 Rand2D()

  public final static native Vector4 Rand()

  public final static Vector4 RandRing(Float minRadius, Float maxRadius) {
    Float r;
    Float angle;
    r = RandRangeF(minRadius, maxRadius);
    angle = RandRangeF(0, 6.2831854820251465);
    return new Vector4(r * CosF(angle),r * SinF(angle),0,1);
  }

  public final static Vector4 RandCone(Float coneDir, Float coneAngle, Float minRadius, Float maxRadius) {
    Float r;
    Float angle;
    Float angleMin;
    Float angleMax;
    r = RandRangeF(minRadius, maxRadius);
    angleMin = Deg2Rad(coneDir - coneAngle * 0.5 + 90);
    angleMax = Deg2Rad(coneDir + coneAngle * 0.5 + 90);
    angle = RandRangeF(angleMin, angleMax);
    return new Vector4(r * CosF(angle),r * SinF(angle),0,1);
  }

  public final static Vector4 RandRingStatic(Int32 seed, Float minRadius, Float maxRadius) {
    Float r;
    Float angle;
    r = RandNoiseF(seed, maxRadius, minRadius);
    angle = RandNoiseF(seed, 6.2831854820251465);
    return new Vector4(r * CosF(angle),r * SinF(angle),0,1);
  }

  public final static native Vector4 Mirror(Vector4 dir, Vector4 normal)

  public final static native Float Distance(Vector4 from, Vector4 to)

  public final static native Float DistanceSquared(Vector4 from, Vector4 to)

  public final static native Float Distance2D(Vector4 from, Vector4 to)

  public final static native Float DistanceSquared2D(Vector4 from, Vector4 to)

  public final static native Float DistanceToEdge(Vector4 point, Vector4 a, Vector4 b)

  public final static native Vector4 NearestPointOnEdge(Vector4 point, Vector4 a, Vector4 b)

  public final static native EulerAngles ToRotation(Vector4 dir)

  public final static native Float Heading(Vector4 dir)

  public final static native Vector4 FromHeading(Float heading)

  public final static native Vector4 Transform(Matrix m, Vector4 point)

  public final static native Vector4 TransformDir(Matrix m, Vector4 point)

  public final static native Vector4 TransformH(Matrix m, Vector4 point)

  public final static native Float GetAngleBetween(Vector4 from, Vector4 to)

  public final static native Float GetAngleDegAroundAxis(Vector4 dirA, Vector4 dirB, Vector4 axis)

  public final static native Vector4 ProjectPointToPlane(Vector4 p1, Vector4 p2, Vector4 p3, Vector4 toProject)

  public final static native Vector4 RotateAxis(Vector4 vector, Vector4 axis, Float angle)

  public final static Vector4 RotByAngleXY(Vector4 vec, Float angleDeg) {
    Vector4 ret;
    Float angle;
    angle = Deg2Rad(angleDeg);
    ret = vec;
    ret.X = vec.X * CosF(-angle) - vec.Y * SinF(-angle);
    ret.Y = vec.X * SinF(-angle) + vec.Y * CosF(-angle);
    return ret;
  }

  public final static Vector4 Interpolate(Vector4 v1, Vector4 v2, Float ratio) {
    Vector4 dir;
    dir = v2 - v1;
    return v1 + dir * ratio;
  }

  public final static String ToString(Vector4 vec) {
    return FloatToString(vec.X) + " " + FloatToString(vec.Y) + " " + FloatToString(vec.Z) + " " + FloatToString(vec.W);
  }

  public final static String ToStringPrec(Vector4 vec, Int32 precision) {
    return FloatToStringPrec(vec.X, precision) + " " + FloatToStringPrec(vec.Y, precision) + " " + FloatToStringPrec(vec.Z, precision) + " " + FloatToStringPrec(vec.W, precision);
  }

  public final static void Zero(out Vector4 self) {
    self.X = 0;
    self.Y = 0;
    self.Z = 0;
    self.W = 0;
  }

  public final static Bool IsZero(Vector4 self) {
    return self.X == 0 && self.Y == 0 && self.Z == 0 && self.W == 0;
  }

  public final static Bool IsXYZZero(Vector4 self) {
    return self.X < 0.0010000000474974513 && self.Y < 0.0010000000474974513 && self.Z < 0.0010000000474974513;
  }

  public final static Bool IsFloatZero(Vector4 self) {
    return self.X < 0.0010000000474974513 && self.Y < 0.0010000000474974513 && self.Z < 0.0010000000474974513 && self.W < 0.0010000000474974513;
  }

  public final static Bool IsXYZFloatZero(Vector4 self) {
    return self.X < 0.0010000000474974513 && self.Y < 0.0010000000474974513 && self.Z < 0.0010000000474974513;
  }

  public final static Vector4 EmptyVector() {
    Vector4 vec;
    return vec;
  }

  public final static Vector4 ClampLength(Vector4 self, Float min, Float max) {
    Float length;
    length = Length(self);
    length = ClampF(length, min, max);
    return Normalize(self) * length;
  }

  public final static Vector4 Vector3To4(Vector3 v3) {
    Vector4 v4;
    v4.X = v3.X;
    v4.Y = v3.Y;
    v4.Z = v3.Z;
    return v4;
  }

  public final static Vector3 Vector4To3(Vector4 v4) {
    Vector3 v3;
    v3.X = v4.X;
    v3.Y = v4.Y;
    v3.Z = v4.Z;
    return v3;
  }
}

public static String VectorToString(Vector4 vec) {
  String str;
  str = "x: " + FloatToString(vec.X) + " y: " + FloatToString(vec.Y) + " z: " + FloatToString(vec.Z);
  return str;
}

public static Vector4 Cast(Vector3 v3) {
  Vector4 v4;
  v4.X = v3.X;
  v4.Y = v3.Y;
  v4.Z = v3.Z;
  v4.W = 0;
  return v4;
}

public static Vector3 Cast(Vector4 v4) {
  Vector3 v3;
  v3.X = v4.X;
  v3.Y = v4.Y;
  v3.Z = v4.Z;
  return v3;
}
