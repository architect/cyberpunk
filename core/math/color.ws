
public struct Color {

  public native Uint8 Red;

  public native Uint8 Green;

  public native Uint8 Blue;

  public native Uint8 Alpha;

  public final static HDRColor ToHDRColorDirect(Color color) {
    return new HDRColor(Cast(color.Red) / 255,Cast(color.Green) / 255,Cast(color.Blue) / 255,Cast(color.Alpha) / 255);
  }
}
