
public struct Transform {

  public native Vector4 position;

  public native Quaternion orientation;

  public final static Transform Create(Vector4 position, Quaternion orientation?) {
    Transform t;
    t.position = position;
    t.orientation = orientation;
    return t;
  }

  public final static native Vector4 TransformPoint(Transform xform, Vector4 v)

  public final static native Vector4 TransformVector(Transform xform, Vector4 v)

  public final static native EulerAngles ToEulerAngles(Transform xform)

  public final static native Matrix ToMatrix(Transform xform)

  public final static native Vector4 GetForward(Transform xform)

  public final static native Vector4 GetRight(Transform xform)

  public final static native Vector4 GetUp(Transform xform)

  public final static native Float GetPitch(Transform xform)

  public final static native Float GetYaw(Transform xform)

  public final static native Float GetRoll(Transform xform)

  public final static native void SetIdentity(Transform xform)

  public final static native void SetInverse(Transform xform)

  public final static native Transform GetInverse(Transform xform)

  public final static native Vector4 GetPosition(Transform xform)

  public final static native Quaternion GetOrientation(Transform xform)

  public final static native void SetPosition(Transform xform, Vector4 v)

  public final static native void SetOrientation(Transform xform, Quaternion quat)

  public final static native void SetOrientationEuler(Transform xform, EulerAngles euler)

  public final static native void SetOrientationFromDir(Transform xform, Vector4 direction)
}
