
public static String NoTrailZeros(Float f) {
  String tmp;
  tmp = FloatToString(f);
  if(StrFindFirst(tmp, ",") >= 0 || StrFindFirst(tmp, ".") >= 0) {
    while(StrEndsWith(tmp, "0")) {
      tmp = StrLeft(tmp, StrLen(tmp) - 1);
    };
  };
  if(StrEndsWith(tmp, ",") || StrEndsWith(tmp, ".")) {
    tmp = StrLeft(tmp, StrLen(tmp) - 1);
  };
  return tmp;
}

public static String NoTrailZerosStr(String str) {
  if(StrFindFirst(str, ",") >= 0 || StrFindFirst(str, ".") >= 0) {
    while(StrEndsWith(str, "0")) {
      str = StrLeft(str, StrLen(str) - 1);
    };
  };
  if(StrEndsWith(str, ",") || StrEndsWith(str, ".")) {
    str = StrLeft(str, StrLen(str) - 1);
  };
  return str;
}

public static String StrUpperFirst(String str, Int32 lenght?) {
  String left;
  String right;
  if(lenght < 1) {
    lenght = 1;
  };
  left = StrLeft(str, lenght);
  right = StrAfterFirst(str, left);
  left = StrUpper(left);
  return left + right;
}

public static String BoolToString(Bool value) {
  String tmp;
  if(value) {
    tmp = "TRUE";
  } else {
    tmp = "FALSE";
  };
  return tmp;
}

public static Bool StringToBool(String s) {
  if(StrLower(s) == "true") {
    return true;
  };
  return false;
}

public static String SpaceFill(String str, Int32 length, ESpaceFillMode mode?, String fillChar?) {
  Int32 strLen;
  Int32 i;
  Int32 addLeft;
  Int32 addRight;
  Int32 fillLen;
  fillLen = StrLen(fillChar);
  if(fillLen == 0) {
    fillChar = " ";
  } else {
    if(fillLen > 1) {
      fillChar = StrChar(0);
    };
  };
  strLen = StrLen(str);
  if(strLen >= length) {
    return str;
  };
  if(mode == ESpaceFillMode.JustifyLeft) {
    addLeft = 0;
    addRight = length - strLen;
  } else {
    if(mode == ESpaceFillMode.JustifyRight) {
      addLeft = length - strLen;
      addRight = 0;
    } else {
      if(mode == ESpaceFillMode.JustifyCenter) {
        addLeft = FloorF(Cast(length) - Cast(strLen) / 2);
        addRight = length - strLen - addLeft;
      };
    };
  };
  i = 0;
  while(i < addLeft) {
    str = fillChar + str;
    i += 1;
  };
  i = 0;
  while(i < addRight) {
    str += fillChar;
    i += 1;
  };
  return str;
}

public static Bool StrStartsWith(String str, String subStr) {
  return StrFindFirst(str, subStr) == 0;
}

public static Bool StrContains(String str, String subStr) {
  return StrFindFirst(str, subStr) >= 0;
}

public static String OperatorMultiply(String a, Int32 count) {
  Int32 i;
  String result;
  String bit;
  bit = a;
  i = 0;
  while(i < count) {
    result = result + bit;
    i += 1;
  };
  return result;
}

public static String OperatorAdd(String s, Int32 i) {
  return s + IntToString(i);
}

public static String OperatorAdd(Int32 i, String s) {
  return IntToString(i) + s;
}

public static String OperatorAdd(String s, Float f) {
  return s + NoTrailZeros(f);
}

public static String OperatorAdd(Float f, String s) {
  return NoTrailZeros(f) + s;
}

public static String OperatorAdd(String s, Bool b) {
  return s + BoolToString(b);
}

public static String OperatorAdd(Bool b, String s) {
  return BoolToString(b) + s;
}

public static CName OperatorAdd(CName n1, CName n2) {
  String s1;
  String s2;
  s1 = NameToString(n1);
  s2 = NameToString(n2);
  return StringToName(s1 + s2);
}
