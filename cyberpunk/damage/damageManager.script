
public class DamageManager extends IScriptable {

  public final static void PreAttack(wref<AttackData> attackData) {
    ProcessDodgeOpportunity(attackData);
  }

  public final static void ModifyHitData(ref<gameHitEvent> hitEvent) {
    ref<AttackData> attackData;
    ref<StatusEffectSystem> statusEffectsSystem;
    ref<StatPoolsSystem> statPoolSystem;
    gamedataAttackType attackType;
    Float chargeVal;
    attackData = hitEvent.attackData;
    statusEffectsSystem = GetStatusEffectSystem(WeakRefToRef(hitEvent.target).GetGame());
    statPoolSystem = GetStatPoolsSystem(WeakRefToRef(hitEvent.target).GetGame());
    if(!ToBool(GetScriptedPuppetTarget(hitEvent))) {
      LogAI("No scripted puppet has been found /!\");
      return ;
    };
    if(Size(hitEvent.hitRepresentationResult.hitShapes) > 0 && IsProtectionLayer(GetHitShape(hitEvent))) {
      attackData.AddFlag(hitFlag.DamageNullified, "ProtectionLayer");
    };
    if(ToBool(attackData.GetWeapon()) && GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame()).GetStatValue(Cast(WeakRefToRef(attackData.GetWeapon()).GetEntityID()), gamedataStatType.CanSilentKill) > 0) {
      attackData.AddFlag(hitFlag.SilentKillModifier, "CanSilentKill");
    };
    if(WeakRefToRef(attackData.GetInstigator()).IsPlayer()) {
      if(!IsPlayerInCombat(attackData)) {
        attackData.AddFlag(hitFlag.StealthHit, "Player attacked from out of combat");
      };
    };
    if(statusEffectsSystem.HasStatusEffect(WeakRefToRef(hitEvent.target).GetEntityID(), "BaseStatusEffect.Defeated")) {
      attackData.AddFlag(hitFlag.Defeated, "Defeated");
    };
    if(statPoolSystem.HasActiveStatPool(Cast(WeakRefToRef(attackData.GetWeapon()).GetEntityID()), gamedataStatPoolType.WeaponCharge)) {
      chargeVal = statPoolSystem.GetStatPoolValue(Cast(WeakRefToRef(attackData.GetWeapon()).GetEntityID()), gamedataStatPoolType.WeaponCharge, false);
      if(chargeVal >= 100) {
        attackData.AddFlag(hitFlag.WeaponFullyCharged, "Charge Weapon");
      };
    };
    if(IsMelee(attackType) && chargeVal > 0) {
      if(!IsStrongMelee(attackType)) {
        chargeVal = MinF(chargeVal, 20);
      };
      hitEvent.attackComputed.MultAttackValue(LerpF(chargeVal / 100, 1, 2));
      attackData.AddFlag(hitFlag.DoNotTriggerFinisher, "Charge Weapon");
    };
    if(!IsBullet(attackType) && !IsExplosion(attackType)) {
      ProcessDefensiveState(hitEvent);
    };
    return ;
  }

  private final static void ProcessDodgeOpportunity(wref<AttackData> attackData) {
    ref<StimuliEvent> aiEvent;
    EulerAngles angleDist;
    wref<PlayerPuppet> playerPuppet;
    wref<ScriptedPuppet> targetPuppet;
    wref<WeaponObject> weapon;
    gamedataItemType weaponType;
    if(!WeakRefToRef(WeakRefToRef(attackData).GetInstigator()).IsPlayer()) {
      return ;
    };
    playerPuppet = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(attackData).GetInstigator())));
    if(WeakRefToRef(playerPuppet) != null) {
      targetPuppet = RefToWeakRef(Cast(GetTargetingSystem(WeakRefToRef(playerPuppet).GetGame()).GetObjectClosestToCrosshair(playerPuppet, angleDist, TSQ_NPC())));
    } else {
      targetPuppet = null;
    };
    if(ToBool(targetPuppet)) {
      weapon = WeakRefToRef(attackData).GetWeapon();
      if(ToBool(weapon)) {
        weaponType = WeakRefToRef(GetWeaponItemRecord(GetTDBID(WeakRefToRef(weapon).GetItemID())).ItemType()).Type();
      };
      if(!IsAlive(WeakRefToRef(targetPuppet))) {
        return ;
      };
      aiEvent = new StimuliEvent();
      if(IsQuickMelee(WeakRefToRef(attackData).GetAttackType()) || weaponType != gamedataItemType.Cyb_NanoWires && weaponType != gamedataItemType.Wea_Handgun && weaponType != gamedataItemType.Wea_Revolver && weaponType != gamedataItemType.Wea_Rifle && weaponType != gamedataItemType.Wea_LightMachineGun && weaponType != gamedataItemType.Wea_SubmachineGun) {
        if(Distance(WeakRefToRef(targetPuppet).GetWorldPosition(), WeakRefToRef(playerPuppet).GetWorldPosition()) > 4) {
          return ;
        };
        aiEvent.name = "MeleeDodgeOpportunity";
        WeakRefToRef(targetPuppet).QueueEvent(aiEvent);
      };
    };
  }

  public final static Bool IsValidDirectionToDefendMeleeAttack(Vector4 attackerForward, Vector4 defenderForward) {
    Float finalHitDirection;
    finalHitDirection = GetAngleBetween(attackerForward, defenderForward);
    return finalHitDirection > 135;
  }

  private final static void ProcessDefensiveState(ref<gameHitEvent> hitEvent) {
    ref<AttackData> attackData;
    Vector4 pos;
    StatsObjectID targetID;
    ref<StatsSystem> statSystem;
    ref<AIEvent> hitAIEvent;
    gamedataAttackType attackType;
    wref<GameObject> attackSource;
    attackData = hitEvent.attackData;
    targetID = Cast(WeakRefToRef(hitEvent.target).GetEntityID());
    attackSource = attackData.GetSource();
    hitAIEvent = new AIEvent();
    statSystem = GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame());
    if(IsMelee(hitEvent.attackData.GetAttackType()) && statSystem.GetStatValue(targetID, gamedataStatType.IsBlocking) == 1 || statSystem.GetStatValue(targetID, gamedataStatType.IsDeflecting) == 1) {
      if(IsValidDirectionToDefendMeleeAttack(WeakRefToRef(attackSource).GetWorldForward(), WeakRefToRef(hitEvent.target).GetWorldForward())) {
        attackType = attackData.GetAttackType();
        if(statSystem.GetStatValue(targetID, gamedataStatType.IsDeflecting) == 1 && attackType != gamedataAttackType.QuickMelee) {
          attackData.AddFlag(hitFlag.WasDeflected, "Parry");
          PushEvent(WeakRefToRef(attackSource), "myAttackParried");
          hitAIEvent.name = "MyAttackParried";
          WeakRefToRef(attackSource).QueueEvent(hitAIEvent);
          if(WeakRefToRef(hitEvent.target).IsPlayer()) {
            SendNameEventToPSM("successfulDeflect", hitEvent);
          };
        } else {
          if(statSystem.GetStatValue(targetID, gamedataStatType.IsBlocking) == 1 || statSystem.GetStatValue(targetID, gamedataStatType.IsDeflecting) == 1 && attackType == gamedataAttackType.QuickMelee) {
            attackData.AddFlag(hitFlag.WasBlocked, "Block");
            PushEvent(WeakRefToRef(attackSource), "myAttackBlocked");
            SendActionSignal(RefToWeakRef(Cast(WeakRefToRef(attackSource))), "BlockSignal", 0.30000001192092896);
            hitAIEvent.name = "MyAttackBlocked";
            WeakRefToRef(attackSource).QueueEvent(hitAIEvent);
            DealStaminaDamage(hitEvent, targetID, statSystem);
          };
        };
      };
    } else {
      SendActionSignal(RefToWeakRef(Cast(WeakRefToRef(attackSource))), "HitSignal", 0.30000001192092896);
      hitAIEvent.name = "MyAttackHit";
      WeakRefToRef(attackSource).QueueEvent(hitAIEvent);
    };
  }

  protected final static void SendNameEventToPSM(CName eventName, ref<gameHitEvent> hitEvent) {
    ref<AttackData> attackData;
    ref<PSMPostponedParameterBool> psmEvent;
    ref<PlayerPuppet> player;
    ref<ItemObject> playerWeapon;
    ref<EquipmentSystem> es;
    attackData = hitEvent.attackData;
    player = Cast(WeakRefToRef(hitEvent.target));
    es = Cast(GetScriptableSystemsContainer(player.GetGame()).Get("EquipmentSystem"));
    playerWeapon = es.GetActiveWeaponObject(player, gamedataEquipmentArea.Weapon);
    psmEvent = new PSMPostponedParameterBool();
    psmEvent.id = eventName;
    psmEvent.value = true;
    player.QueueEvent(psmEvent);
    player.QueueEventForEntityID(playerWeapon.GetEntityID(), psmEvent);
  }

  public final static void PostProcess(ref<gameHitEvent> hitEvent)

  public final static void CalculateSourceModifiers(ref<gameHitEvent> hitEvent) {
    Float tempStat;
    if(WeakRefToRef(hitEvent.attackData.GetInstigator()).IsPlayer()) {
      if(Cast(WeakRefToRef(hitEvent.target)).IsMechanical()) {
        tempStat = GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame()).GetStatValue(Cast(WeakRefToRef(hitEvent.attackData.GetInstigator()).GetEntityID()), gamedataStatType.BonusDamageAgainstMechanicals);
        if(!FloatIsEqual(tempStat, 0)) {
          hitEvent.attackComputed.MultAttackValue(1 + tempStat);
        };
      };
      if(Cast(WeakRefToRef(hitEvent.target)).GetNPCRarity() == gamedataNPCRarity.Elite) {
        tempStat = GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame()).GetStatValue(Cast(WeakRefToRef(hitEvent.attackData.GetInstigator()).GetEntityID()), gamedataStatType.BonusDamageAgainstElites);
        if(!FloatIsEqual(tempStat, 0)) {
          hitEvent.attackComputed.MultAttackValue(1 + tempStat);
        };
      };
      if(IsMelee(hitEvent.attackData.GetAttackType()) && HasStatusEffect(hitEvent.attackData.GetInstigator(), "BaseStatusEffect.BerserkPlayerBuff")) {
        tempStat = GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame()).GetStatValue(Cast(WeakRefToRef(hitEvent.attackData.GetInstigator()).GetEntityID()), gamedataStatType.BerserkMeleeDamageBonus);
        if(!FloatIsEqual(tempStat, 0)) {
          hitEvent.attackComputed.MultAttackValue(1 + tempStat * 0.009999999776482582);
        };
      };
    };
  }

  public final static void CalculateTargetModifiers(ref<gameHitEvent> hitEvent) {
    Float tempStat;
    if(IsExplosion(hitEvent.attackData.GetAttackType())) {
      tempStat = tempStat = GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame()).GetStatValue(Cast(WeakRefToRef(hitEvent.target).GetEntityID()), gamedataStatType.DamageReductionExplosion);
      if(!FloatIsEqual(tempStat, 0)) {
        hitEvent.attackComputed.MultAttackValue(1 - tempStat);
      };
    };
    if(IsDoT(hitEvent.attackData.GetAttackType())) {
      tempStat = tempStat = GetStatsSystem(WeakRefToRef(hitEvent.target).GetGame()).GetStatValue(Cast(WeakRefToRef(hitEvent.target).GetEntityID()), gamedataStatType.DamageReductionDamageOverTime);
      if(!FloatIsEqual(tempStat, 0)) {
        hitEvent.attackComputed.MultAttackValue(1 - tempStat);
      };
    };
  }

  public final static void CalculateGlobalModifiers(ref<gameHitEvent> hitEvent)

  private final static ref<ScriptedPuppet> GetScriptedPuppetTarget(ref<gameHitEvent> hitEvent) {
    return Cast(WeakRefToRef(hitEvent.target));
  }

  protected final static void DealStaminaDamage(ref<gameHitEvent> hitEvent, StatsObjectID targetID, ref<StatsSystem> statSystem) {
    ref<WeaponObject> weapon;
    Float staminaDamageValue;
    weapon = WeakRefToRef(hitEvent.attackData.GetWeapon());
    staminaDamageValue = statSystem.GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.StaminaDamage);
    GetStatPoolsSystem(WeakRefToRef(hitEvent.target).GetGame()).RequestChangingStatPoolValue(targetID, gamedataStatPoolType.Stamina, -staminaDamageValue, hitEvent.attackData.GetInstigator(), false, false);
  }
}
