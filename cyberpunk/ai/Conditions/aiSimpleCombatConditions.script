
public class SimpleCombatConditon extends AIbehaviorconditionScript {

  public Bool m_firstCoverEvaluationDone;

  public ref<GameplayAbility_Record> m_takeCoverAbility;

  public ref<GameplayAbility_Record> m_quickhackAbility;

  protected void Activate(ScriptExecutionContext context) {
    this.m_takeCoverAbility = GetGameplayAbilityRecord("Ability.CanUseCovers");
    this.m_quickhackAbility = GetGameplayAbilityRecord("Ability.CanQuickhack");
  }

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    ref<GameObject> player;
    wref<SquadScriptInterface> squadInterface;
    array<wref<Entity>> squadMembers;
    ref<ScriptedPuppet> squadMember;
    ref<AIHumanComponent> aiComponent;
    Int32 activeCommandCount;
    Int32 squadMatesInSimpleCombat;
    ref<ScriptedPuppet> puppet;
    Vector4 playerPos;
    Int32 membersCount;
    Float distanceSqr;
    Float minDistSqr1;
    Float minDistSqr2;
    Int32 minDist1Index;
    Int32 minDist2Index;
    Int32 i;
    puppet = GetPuppet(context);
    minDistSqr1 = 999999;
    minDistSqr2 = 999999;
    minDist1Index = -1;
    minDist2Index = -1;
    player = GetPlayerSystem(GetOwner(context).GetGame()).GetLocalPlayerControlledGameObject();
    if(!ToBool(player)) {
      return Cast(true);
    };
    if(!puppet.IsActive()) {
      return Cast(false);
    };
    if(IsPlayerCompanion(RefToWeakRef(puppet))) {
      return Cast(false);
    };
    if(AnimationInProgress(RefToWeakRef(puppet))) {
      return Cast(false);
    };
    if(CheckAbility(RefToWeakRef(puppet), RefToWeakRef(this.m_takeCoverAbility))) {
      if(!this.m_firstCoverEvaluationDone && GetCoverBlackboard(puppet).GetBool(GetAllBlackboardDefs().AICover.firstCoverEvaluationDone)) {
        this.m_firstCoverEvaluationDone = true;
      };
      if(!this.m_firstCoverEvaluationDone) {
        return Cast(false);
      };
      if(HasAvailableCover(context) && !IsCurrentlyInSmartObject(puppet)) {
        return Cast(false);
      };
      if(IsCurrentlyInCoverAttackAction(puppet)) {
        return Cast(false);
      };
    };
    if(CheckAbility(RefToWeakRef(puppet), RefToWeakRef(this.m_quickhackAbility))) {
      return Cast(false);
    };
    aiComponent = puppet.GetAIControllerComponent();
    if(ToBool(aiComponent)) {
      if(aiComponent.IsCommandActive("AICommand")) {
        activeCommandCount = aiComponent.GetActiveCommandsCount();
        if(aiComponent.IsCommandActive("AIInjectCombatTargetCommand")) {
          activeCommandCount -= 1;
        };
        if(aiComponent.IsCommandActive("AIInjectCombatThreatCommand")) {
          activeCommandCount -= 1;
        };
        if(activeCommandCount > 0) {
          return Cast(false);
        };
      };
    };
    squadInterface = RefToWeakRef(puppet.GetSquadMemberComponent().MySquad(AISquadType.Combat));
    if(!ToBool(squadInterface)) {
      return Cast(true);
    };
    squadMembers = WeakRefToRef(squadInterface).ListMembersWeak();
    playerPos = player.GetWorldPosition();
    membersCount = Size(squadMembers);
    i = 0;
    while(i < membersCount) {
      squadMember = Cast(WeakRefToRef(squadMembers[i]));
      if(!ToBool(squadMember)) {
      } else {
        if(HasOrder(RefToWeakRef(squadMember), "SimpleCombat")) {
          squadMatesInSimpleCombat += 1;
        };
        distanceSqr = DistanceSquared(WeakRefToRef(squadMembers[i]).GetWorldPosition(), playerPos);
        if(distanceSqr < minDistSqr2) {
          if(distanceSqr < minDistSqr1) {
            minDistSqr1 = distanceSqr;
            minDist1Index = i;
          } else {
            minDistSqr2 = distanceSqr;
            minDist2Index = i;
          };
        };
      };
      i += 1;
    };
    if(minDist1Index >= 0 && WeakRefToRef(squadMembers[minDist1Index]) == puppet || minDist2Index >= 0 && WeakRefToRef(squadMembers[minDist2Index]) == puppet) {
      return Cast(false);
    };
    if(membersCount - squadMatesInSimpleCombat < 3 && !HasOrder(RefToWeakRef(puppet), "SimpleCombat")) {
      return Cast(false);
    };
    return Cast(true);
  }

  private final Bool AnimationInProgress(wref<ScriptedPuppet> puppet) {
    if(WeakRefToRef(puppet).GetMovePolicesComponent().IsOnOffMeshLink()) {
      return true;
    };
    if(GetCoverManager(WeakRefToRef(puppet).GetGame()).IsEnteringOrLeavingCover(WeakRefToRef(puppet))) {
      return true;
    };
    return false;
  }

  public final static Bool HasAvailableCover(ScriptExecutionContext context) {
    wref<ScriptedPuppet> puppet;
    wref<MultiSelectCovers> msc;
    gamedataAIRingType currentRing;
    Int32 i;
    puppet = RefToWeakRef(Cast(GetOwner(context)));
    if(!ToBool(puppet)) {
      return false;
    };
    msc = Cast(GetArgumentScriptable(context, "MultiCoverID"));
    if(!ToBool(msc)) {
      return false;
    };
    currentRing = GetCurrentSquadRing(RefToWeakRef(Cast(GetOwner(context))));
    if(currentRing == gamedataAIRingType.Invalid) {
      return true;
    };
    i = 0;
    while(i < Size(WeakRefToRef(msc).selectedCovers)) {
      if(!WeakRefToRef(msc).coversUseLOS[i]) {
      } else {
        if(currentRing != WeakRefToRef(msc).coverRingTypes[i]) {
        } else {
          if(WeakRefToRef(msc).selectedCovers[i] > 0) {
            return true;
          };
        };
      };
      i += 1;
    };
    return false;
  }
}
