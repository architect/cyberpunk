
public class UseCoverCommandTask extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  protected wref<AIUseCoverCommand> m_currentCommand;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIUseCoverCommand> typedCommand;
    GlobalNodeRef coverGlobalRef;
    Uint64 coverID;
    ref<CoverManager> cm;
    ref<AIHumanComponent> aiComponent;
    Uint64 tmpID;
    ref<CoverDemandHolder> coverDemandHolder;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    aiComponent = GetPuppet(context).GetAIControllerComponent();
    if(!ToBool(typedCommand)) {
      if(ToBool(this.m_currentCommand)) {
        CancelCommand(context, typedCommand, aiComponent);
      };
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    if(typedCommand == WeakRefToRef(this.m_currentCommand)) {
      if(typedCommand.oneTimeSelection && GetCurrentCoverID(context, coverID) && coverID == GetArgumentUint64(context, "CommandCoverID")) {
        CancelCommand(context, typedCommand, aiComponent);
        if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
          aiComponent.StopExecutingCommand(typedCommand, true);
        };
      };
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    cm = GetCoverManager(GetGame(context));
    if(!ToBool(cm)) {
      LogAIError("CoverManager is NULL in UseCoverCommandTask!");
    } else {
      coverDemandHolder = cm.GetDemandCoverHolder(typedCommand.coverNodeRef);
      if(ToBool(coverDemandHolder)) {
        coverID = coverDemandHolder.GetCoverID();
      };
    };
    if(coverID == 0 || !cm.IsCoverValid(coverID)) {
      DebugLog(context, "AIUseCoverCommand", "Trying to select invalid cover or cover with ID == 0!");
      CancelCommand(context, typedCommand, aiComponent);
      if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
        aiComponent.StopExecutingCommand(typedCommand, true);
      };
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    tmpID = GetArgumentUint64(context, "CommandCoverID");
    if(tmpID != coverID) {
      SetArgumentUint64(context, "CommandCoverID", coverID);
      cm.NotifyBehaviourCoverArgumentChanged(GetOwner(context), "CommandCoverID", tmpID, coverID);
    };
    GetPuppet(context).GetAIControllerComponent().GetCoverBlackboard().SetVariant(GetAllBlackboardDefs().AICover.commandExposureMethods, ToVariant(typedCommand.limitToTheseExposureMethods));
    SetArgumentBool(context, "StopCover", false);
    SetArgumentName(context, "ForcedEntryAnimation", typedCommand.forcedEntryAnimation);
    this.m_currentCommand = RefToWeakRef(typedCommand);
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  private final void Deactivate(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIUseCoverCommand> typedCommand;
    ref<AIHumanComponent> aiComponent;
    if(!ToBool(this.m_currentCommand)) {
      return ;
    };
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    Get(RefToWeakRef(Cast(GetOwner(context))), aiComponent);
    if(!ToBool(typedCommand)) {
      CancelCommand(context, typedCommand, aiComponent);
    } else {
      if(GetArgumentBool(context, "StopCover")) {
        if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing && ToBool(aiComponent)) {
          aiComponent.StopExecutingCommand(typedCommand, true);
        };
        CancelCommand(context, typedCommand, aiComponent);
      };
    };
  }

  protected final void CancelCommand(ScriptExecutionContext context, ref<AIUseCoverCommand> typedCommand, ref<AIHumanComponent> aiComponent) {
    Uint64 tmpID;
    tmpID = GetArgumentUint64(context, "CommandCoverID");
    if(tmpID != 0) {
      SetArgumentUint64(context, "CommandCoverID", 0);
      GetCoverManager(GetOwner(context).GetGame()).NotifyBehaviourCoverArgumentChanged(GetOwner(context), "CommandCoverID", tmpID, 0);
    };
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
    if(ToBool(this.m_currentCommand)) {
      this.m_currentCommand = null;
    };
    if(ToBool(aiComponent)) {
      aiComponent.GetCoverBlackboard().SetVariant(GetAllBlackboardDefs().AICover.commandExposureMethods, ToVariant(null));
    };
  }
}

public class UseCoverCommandHandler extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  protected wref<AIUseCoverCommand> m_currentCommand;

  private final void Activate(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIUseCoverCommand> typedCommand;
    this.m_currentCommand = null;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    if(ToBool(typedCommand)) {
      this.m_currentCommand = RefToWeakRef(typedCommand);
    };
  }

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIUseCoverCommand> typedCommand;
    ref<AIHumanComponent> aiComponent;
    Bool waitBeforeExit;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    if(!ToBool(typedCommand)) {
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    if(ToBool(this.m_currentCommand)) {
      waitBeforeExit = GetCoverManager(GetGame(context)).IsEnteringOrLeavingCover(GetOwner(context));
      if(typedCommand == WeakRefToRef(this.m_currentCommand)) {
        aiComponent = GetPuppet(context).GetAIControllerComponent();
        if(aiComponent.GetCoverBlackboard().GetBool(GetAllBlackboardDefs().AICover.commandCoverOverride) && !waitBeforeExit) {
          aiComponent.GetCoverBlackboard().SetBool(GetAllBlackboardDefs().AICover.commandCoverOverride, false);
          return AIbehaviorUpdateOutcome.SUCCESS;
        };
        return AIbehaviorUpdateOutcome.IN_PROGRESS;
      };
      if(!waitBeforeExit) {
        return AIbehaviorUpdateOutcome.SUCCESS;
      };
      aiComponent = GetPuppet(context).GetAIControllerComponent();
      aiComponent.GetCoverBlackboard().SetBool(GetAllBlackboardDefs().AICover.commandCoverOverride, true);
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    this.m_currentCommand = RefToWeakRef(typedCommand);
    return AIbehaviorUpdateOutcome.SUCCESS;
  }

  private final Bool WaitBeforeExit(ScriptExecutionContext context) {
    ref<AIHumanComponent> aiComponent;
    aiComponent = GetPuppet(context).GetAIControllerComponent();
    if(GetCoverManager(GetGame(context)).IsEnteringOrLeavingCover(GetOwner(context))) {
      return true;
    };
    if(aiComponent.GetCoverBlackboard().GetBool(GetAllBlackboardDefs().AICover.currentlyExposed)) {
      return true;
    };
    return false;
  }
}
