
public class AimAtTargetCommandTask extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  protected wref<AIAimAtTargetCommand> m_currentCommand;

  protected Float m_activationTimeStamp;

  protected Float m_commandDuration;

  protected wref<GameObject> m_target;

  protected EntityID m_targetID;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIAimAtTargetCommand> typedCommand;
    GlobalNodeRef globalRef;
    wref<GameObject> target;
    array<EntityID> targetIDs;
    ref<TargetTrackerComponent> targetTrackerComponent;
    ref<AttitudeAgent> attitudeOwner;
    ref<AttitudeAgent> attitudeTarget;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    if(typedCommand == WeakRefToRef(this.m_currentCommand)) {
      if(ToBool(this.m_currentCommand)) {
        if(!IsCommandCombatTargetValid(context, "AIAimAtTargetCommand")) {
          CancelCommand(context);
          if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
            GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, true);
          };
        } else {
          if(IsDefined(this.m_targetID) && !ToBool(this.m_target)) {
            CancelCommand(context);
            DebugLog(context, "InjectCombatThreatCommand", "Canceling command, entity streamed out");
            if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
              GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, false);
            };
          } else {
            if(this.m_commandDuration > 0 && ToFloat(GetAITime(context)) > this.m_activationTimeStamp + this.m_commandDuration) {
              CancelCommand(context);
              DebugLog(context, "AIAimAtTargetCommand", "Canceling command, duration expired");
              if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
                GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, true);
              };
            };
          };
        };
      };
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    this.m_currentCommand = RefToWeakRef(typedCommand);
    this.m_commandDuration = typedCommand.duration;
    this.m_activationTimeStamp = ToFloat(GetAITime(context));
    if(!GetGameObjectFromEntityReference(typedCommand.targetOverridePuppetRef, GetOwner(context).GetGame(), target)) {
      globalRef = ResolveNodeRef(typedCommand.targetOverrideNodeRef, Cast(GetRoot()));
      target = RefToWeakRef(Cast(FindEntityByID(GetGame(context), Cast(globalRef))));
    };
    this.m_target = target;
    this.m_targetID = Cast(globalRef);
    if(IsDefined(this.m_targetID) && !ToBool(this.m_target)) {
      CancelCommand(context);
      DebugLog(context, "AIAimAtTargetCommand", "Canceling command, entity streamed out");
      if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
        GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, false);
      };
    } else {
      if(!SetCommandCombatTarget(context, target, ToInt(PersistenceSource.CommandAimWithWeapon))) {
        CancelCommand(context);
        DebugLog(context, "AIAimAtTargetCommand", "Canceling command, unable to set CommandCombatTarget");
        if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
          GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, true);
        };
      };
    };
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  private final void Deactivate(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIAimAtTargetCommand> typedCommand;
    if(!ToBool(this.m_currentCommand)) {
      return ;
    };
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    if(!ToBool(typedCommand)) {
      CancelCommand(context);
    };
  }

  protected final void CancelCommand(ScriptExecutionContext context) {
    ClearCommandCombatTarget(context, ToInt(PersistenceSource.CommandAimWithWeapon));
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
    this.m_activationTimeStamp = 0;
    this.m_commandDuration = 0;
    this.m_currentCommand = null;
    this.m_target = null;
    this.m_targetID = WeakRefToRef(this.m_target).GetEntityID();
  }
}

public class AimAtTargetCommandCleanup extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  private final void Deactivate(ScriptExecutionContext context) {
    ClearCommandCombatTarget(context, ToInt(PersistenceSource.CommandAimWithWeapon));
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
  }
}

public class AimAtTargetCommandHandler extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  protected wref<AIAimAtTargetCommand> m_currentCommand;

  private final void Activate(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIAimAtTargetCommand> typedCommand;
    this.m_currentCommand = null;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    if(ToBool(typedCommand)) {
      this.m_currentCommand = RefToWeakRef(typedCommand);
    };
  }

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    wref<AIAimAtTargetCommand> typedCommand;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = RefToWeakRef(Cast(rawCommand));
    if(!ToBool(typedCommand)) {
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    if(ToBool(this.m_currentCommand)) {
      if(WeakRefToRef(typedCommand) == WeakRefToRef(this.m_currentCommand)) {
        return AIbehaviorUpdateOutcome.IN_PROGRESS;
      };
      return AIbehaviorUpdateOutcome.SUCCESS;
    };
    this.m_currentCommand = typedCommand;
    return AIbehaviorUpdateOutcome.SUCCESS;
  }
}
