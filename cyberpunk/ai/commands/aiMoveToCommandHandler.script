
public class AIMoveToCommandHandler extends AICommandHandlerBase {

  protected inline edit ref<AIArgumentMapping> m_outIsDynamicMove;

  protected inline edit ref<AIArgumentMapping> m_outMovementTarget;

  protected inline edit ref<AIArgumentMapping> m_outMovementTargetPos;

  protected inline edit ref<AIArgumentMapping> m_outRotateEntityTowardsFacingTarget;

  protected inline edit ref<AIArgumentMapping> m_outFacingTarget;

  protected inline edit ref<AIArgumentMapping> m_outMovementType;

  protected inline edit ref<AIArgumentMapping> m_outIgnoreNavigation;

  protected inline edit ref<AIArgumentMapping> m_outUseStart;

  protected inline edit ref<AIArgumentMapping> m_outUseStop;

  protected inline edit ref<AIArgumentMapping> m_outDesiredDistanceFromTarget;

  protected inline edit ref<AIArgumentMapping> m_outFinishWhenDestinationReached;

  protected AIbehaviorUpdateOutcome UpdateCommand(ScriptExecutionContext context, ref<AICommand> command) {
    gamedataNPCHighLevelState currentHighLevelState;
    ref<AIMoveToCommand> typedCommand;
    ref<gamePuppet> owner;
    Bool isDynamicMove;
    typedCommand = Cast(command);
    if(!ToBool(typedCommand)) {
      LogAIError("Argument 'inCommand' has invalid type. Expected AIMoveToCommand, got " + ToString(command.GetClassName()) + ".");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    owner = GetOwner(context);
    isDynamicMove = IsEntity(typedCommand.movementTarget);
    SetMappingValue(context, this.m_outIsDynamicMove, ToVariant(isDynamicMove));
    if(isDynamicMove) {
      SetMappingValue(context, this.m_outMovementTarget, ToVariant(RefToWeakRef(Cast(GetEntity(typedCommand.movementTarget)))));
    } else {
      if(IsEmpty(typedCommand.movementTarget)) {
        SetMappingValue(context, this.m_outMovementTargetPos, ToVariant(GetInvalidPos()));
      } else {
        SetMappingValue(context, this.m_outMovementTargetPos, ToVariant(GetWorldPosition(typedCommand.movementTarget)));
      };
    };
    if(!SetMappingValue(context, this.m_outRotateEntityTowardsFacingTarget, ToVariant(typedCommand.rotateEntityTowardsFacingTarget))) {
      LogAIError("Failed to set 'Out Rotate Entity Towards Facing Target' argument mapping.");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(IsEntity(typedCommand.facingTarget)) {
      if(!SetMappingValue(context, this.m_outFacingTarget, ToVariant(RefToWeakRef(Cast(GetEntity(typedCommand.facingTarget)))))) {
        LogAIError("Failed to set 'Out Facing Target' argument mapping.");
        return AIbehaviorUpdateOutcome.FAILURE;
      };
    };
    if(Cast(GetOwner(context)).GetNPCType() == gamedataNPCType.Drone) {
      SetEnumMappingValue(context, this.m_outMovementType, ToInt(moveMovementType.Walk));
      SetLocomotionWrappers(Cast(GetOwner(context)), EnumValueToName("moveMovementType", ToInt(typedCommand.movementType)));
    } else {
      if(typedCommand.movementType == moveMovementType.Run) {
        currentHighLevelState = ToEnum(Cast(GetOwner(context)).GetPuppetStateBlackboard().GetInt(GetAllBlackboardDefs().PuppetState.HighLevel));
        if(currentHighLevelState == gamedataNPCHighLevelState.Combat && !SetEnumMappingValue(context, this.m_outMovementType, ToInt(moveMovementType.Sprint))) {
          LogAIError("Failed to set 'Out Movement Type' argument mapping.");
          return AIbehaviorUpdateOutcome.FAILURE;
        };
        if(!SetEnumMappingValue(context, this.m_outMovementType, ToInt(typedCommand.movementType))) {
          LogAIError("Failed to set 'Out Movement Type' argument mapping.");
          return AIbehaviorUpdateOutcome.FAILURE;
        };
      } else {
        if(!SetEnumMappingValue(context, this.m_outMovementType, ToInt(typedCommand.movementType))) {
          LogAIError("Failed to set 'Out Movement Type' argument mapping.");
          return AIbehaviorUpdateOutcome.FAILURE;
        };
      };
    };
    if(!SetMappingValue(context, this.m_outIgnoreNavigation, ToVariant(typedCommand.ignoreNavigation))) {
      LogAIError("Failed to set 'Out Ignore Navigation' argument mapping.");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(!SetMappingValue(context, this.m_outUseStart, ToVariant(typedCommand.useStart))) {
      LogAIError("Failed to set 'Out Use Start' argument mapping.");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(!SetMappingValue(context, this.m_outUseStop, ToVariant(typedCommand.useStop))) {
      LogAIError("Failed to set 'Out Use Stop' argument mapping.");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(!SetMappingValue(context, this.m_outDesiredDistanceFromTarget, ToVariant(typedCommand.desiredDistanceFromTarget))) {
      LogAIError("Failed to set 'Out Desired Distance From Target' argument mapping.");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(!SetMappingValue(context, this.m_outFinishWhenDestinationReached, ToVariant(typedCommand.finishWhenDestinationReached))) {
      LogAIError("Failed to set 'Out Finish When Destination Reached' argument mapping.");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(typedCommand.alwaysUseStealth) {
      ChangeHighLevelState(GetOwner(context), gamedataNPCHighLevelState.Stealth);
    };
    return AIbehaviorUpdateOutcome.SUCCESS;
  }
}

public class AIMoveOnSplineCommandHandler extends AICommandHandlerBase {

  protected inline edit ref<AIArgumentMapping> m_outSpline;

  protected inline edit ref<AIArgumentMapping> m_outMovementType;

  protected inline edit ref<AIArgumentMapping> m_outRotateTowardsFacingTarget;

  protected inline edit ref<AIArgumentMapping> m_outFacingTarget;

  protected inline edit ref<AIArgumentMapping> m_outSnapToTerrain;

  protected AIbehaviorUpdateOutcome UpdateCommand(ScriptExecutionContext context, ref<AICommand> command) {
    ref<AIMoveOnSplineCommand> typedCommand;
    gamedataNPCHighLevelState currentHighLevelState;
    typedCommand = Cast(command);
    if(!ToBool(typedCommand)) {
      LogAIError("Argument 'inCommand' has invalid type. Expected AIMoveOnSplineCommand, got " + ToString(command.GetClassName()) + ".");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(GetPuppet(context).GetNPCType() == gamedataNPCType.Drone) {
      SetLocomotionWrappers(GetPuppet(context), EnumValueToName("moveMovementType", ToInt(typedCommand.movementType.movementType)));
      typedCommand.movementType.movementType = moveMovementType.Walk;
    } else {
      if(typedCommand.movementType.movementType == moveMovementType.Run) {
        currentHighLevelState = ToEnum(Cast(GetOwner(context)).GetPuppetStateBlackboard().GetInt(GetAllBlackboardDefs().PuppetState.HighLevel));
        if(currentHighLevelState == gamedataNPCHighLevelState.Combat) {
          typedCommand.movementType.movementType = moveMovementType.Sprint;
        } else {
          typedCommand.movementType.movementType = moveMovementType.Walk;
        };
      };
    };
    if(typedCommand.alwaysUseStealth) {
      ChangeHighLevelState(GetOwner(context), gamedataNPCHighLevelState.Stealth);
    };
    SetMappingValue(context, this.m_outSpline, ToVariant(typedCommand.spline));
    SetMappingValue(context, this.m_outMovementType, ToVariant(typedCommand.movementType));
    SetMappingValue(context, this.m_outRotateTowardsFacingTarget, ToVariant(typedCommand.rotateEntityTowardsFacingTarget));
    SetMappingValue(context, this.m_outFacingTarget, ToVariant(null));
    SetMappingValue(context, this.m_outSnapToTerrain, ToVariant(typedCommand.snapToTerrain));
    return AIbehaviorUpdateOutcome.SUCCESS;
  }
}

public class AIMoveRotateToCommandHandler extends AICommandHandlerBase {

  protected inline edit ref<AIArgumentMapping> m_target;

  protected inline edit ref<AIArgumentMapping> m_angleTolerance;

  protected inline edit ref<AIArgumentMapping> m_angleOffset;

  protected inline edit ref<AIArgumentMapping> m_speed;

  protected AIbehaviorUpdateOutcome UpdateCommand(ScriptExecutionContext context, ref<AICommand> command) {
    ref<AIRotateToCommand> typedCommand;
    typedCommand = Cast(command);
    if(!ToBool(typedCommand)) {
      LogAIError("Argument 'inCommand' has invalid type. Expected AIMoveRotateToCommand, got " + ToString(command.GetClassName()) + ".");
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    SetMappingValue(context, this.m_target, ToVariant(GetWorldPosition(typedCommand.target)));
    SetMappingValue(context, this.m_angleTolerance, ToVariant(typedCommand.angleTolerance));
    SetMappingValue(context, this.m_angleOffset, ToVariant(typedCommand.angleOffset));
    SetMappingValue(context, this.m_speed, ToVariant(typedCommand.speed));
    return AIbehaviorUpdateOutcome.SUCCESS;
  }
}

public class AIMoveCommandsDelegate extends ScriptBehaviorDelegate {

  protected inline edit wref<AIAnimMoveOnSplineCommand> m_animMoveOnSplineCommand;

  private NodeRef spline;

  private Bool useStart;

  private Bool useStop;

  private Bool reverse;

  private CName controllerSetupName;

  private Float blendTime;

  private Float globalInBlendTime;

  private Float globalOutBlendTime;

  private Bool turnCharacterToMatchVelocity;

  private CName customStartAnimationName;

  private CName customMainAnimationName;

  private CName customStopAnimationName;

  private Bool startSnapToTerrain;

  private Bool mainSnapToTerrain;

  private Bool stopSnapToTerrain;

  private Float startSnapToTerrainBlendTime;

  private Float stopSnapToTerrainBlendTime;

  private ref<AIMoveOnSplineCommand> m_moveOnSplineCommand;

  private wref<GameObject> strafingTarget;

  private moveMovementType movementType;

  private Bool ignoreNavigation;

  private Bool startFromClosestPoint;

  private Bool useCombatState;

  private Bool useAlertedState;

  private Float noWaitToEndDistance;

  private Float noWaitToEndCompanionDistance;

  [Default(AIMoveCommandsDelegate, 9999999.0f))]
  private Float lowestCompanionDistanceToEnd;

  [Default(AIMoveCommandsDelegate, 9999999.0f))]
  private Float previousCompanionDistanceToEnd;

  private Float maxCompanionDistanceOnSpline;

  private wref<GameObject> companion;

  private Bool ignoreLineOfSightCheck;

  private wref<GameObject> shootingTarget;

  private Float minSearchAngle;

  private Float maxSearchAngle;

  private Float desiredDistance;

  private Float deadZoneRadius;

  private Bool shouldBeInFrontOfCompanion;

  private Bool useMatchForSpeedForPlayer;

  private wref<GameObject> lookAtTarget;

  private Float distanceToCompanion;

  private Vector4 splineEndPoint;

  private Bool hasSplineEndPoint;

  private wref<PlayerPuppet> m_playerCompanion;

  private Float m_firstWaitingDemandTimestamp;

  private Bool useOffMeshLinkReservation;

  private Bool sprint;

  private Bool run;

  private Bool waitForCompanion;

  private ref<AIFollowTargetCommand> m_followTargetCommand;

  private Bool stopWhenDestinationReached;

  private Bool teleportToTarget;

  private Bool shouldTeleportNow;

  private Vector4 teleportDestination;

  private Bool matchTargetSpeed;

  public final Bool DoStartAnimMoveOnSpline() {
    ref<AIAnimMoveOnSplineCommand> c;
    c = WeakRefToRef(this.m_animMoveOnSplineCommand);
    this.spline = c.spline;
    this.useStart = c.useStart;
    this.useStop = c.useStop;
    this.controllerSetupName = c.controllerSetupName;
    this.blendTime = c.blendTime;
    this.globalInBlendTime = c.globalInBlendTime;
    this.globalOutBlendTime = c.globalOutBlendTime;
    this.turnCharacterToMatchVelocity = c.turnCharacterToMatchVelocity;
    this.customStartAnimationName = c.customStartAnimationName;
    this.customMainAnimationName = c.customMainAnimationName;
    this.customStopAnimationName = c.customStopAnimationName;
    this.startSnapToTerrain = c.startSnapToTerrain;
    this.mainSnapToTerrain = c.mainSnapToTerrain;
    this.stopSnapToTerrain = c.stopSnapToTerrain;
    this.startSnapToTerrainBlendTime = c.startSnapToTerrainBlendTime;
    this.stopSnapToTerrainBlendTime = c.stopSnapToTerrainBlendTime;
    return true;
  }

  public final Bool DoEndAnimMoveOnSpline() {
    this.m_animMoveOnSplineCommand = null;
    return true;
  }

  public final Bool GetRotateEntity(ScriptExecutionContext context) {
    return this.m_moveOnSplineCommand.rotateEntityTowardsFacingTarget;
  }

  public final Bool DoStartMoveOnSpline(ScriptExecutionContext context) {
    ref<AIMoveOnSplineCommand> cmd;
    gamedataNPCHighLevelState currentHighLevelState;
    wref<ScriptedPuppet> puppetOwner;
    cmd = this.m_moveOnSplineCommand;
    this.spline = cmd.spline;
    this.strafingTarget = cmd.facingTarget;
    puppetOwner = RefToWeakRef(Cast(GetOwner(context)));
    if(!ToBool(puppetOwner)) {
      return false;
    };
    ResetActionSignal(puppetOwner, "WaitForCompanion");
    if(WeakRefToRef(puppetOwner).GetNPCType() == gamedataNPCType.Drone) {
      SetLocomotionWrappers(WeakRefToRef(puppetOwner), EnumValueToName("moveMovementType", ToInt(cmd.movementType.movementType)));
      cmd.movementType.movementType = moveMovementType.Walk;
    } else {
      if(cmd.movementType.movementType == moveMovementType.Run) {
        currentHighLevelState = ToEnum(WeakRefToRef(puppetOwner).GetPuppetStateBlackboard().GetInt(GetAllBlackboardDefs().PuppetState.HighLevel));
        if(currentHighLevelState == gamedataNPCHighLevelState.Combat) {
          cmd.movementType.movementType = moveMovementType.Sprint;
        } else {
          cmd.movementType.movementType = moveMovementType.Walk;
        };
      };
    };
    if(cmd.alwaysUseStealth) {
      ChangeHighLevelState(GetOwner(context), gamedataNPCHighLevelState.Stealth);
    };
    this.movementType = Resolve(cmd.movementType, WeakRefToRef(puppetOwner));
    this.ignoreNavigation = cmd.ignoreNavigation;
    this.startFromClosestPoint = cmd.startFromClosestPoint;
    this.mainSnapToTerrain = cmd.snapToTerrain;
    this.useCombatState = cmd.useCombatState;
    this.useAlertedState = cmd.useAlertedState;
    this.noWaitToEndDistance = cmd.noWaitToEndDistance;
    this.noWaitToEndCompanionDistance = cmd.noWaitToEndCompanionDistance;
    this.useStart = cmd.useStart;
    this.useStop = cmd.useStop;
    this.reverse = cmd.reverse;
    this.useOffMeshLinkReservation = cmd.useOMLReservation;
    this.companion = cmd.companion;
    this.desiredDistance = cmd.desiredDistance;
    this.deadZoneRadius = cmd.deadZoneRadius;
    this.matchTargetSpeed = cmd.catchUpWithCompanion;
    this.teleportToTarget = cmd.teleportToCompanion;
    this.maxCompanionDistanceOnSpline = cmd.maxCompanionDistanceOnSpline;
    this.ignoreLineOfSightCheck = cmd.ignoreLineOfSightCheck;
    this.minSearchAngle = cmd.minSearchAngle;
    this.maxSearchAngle = cmd.maxSearchAngle;
    if(WeakRefToRef(this.companion) != WeakRefToRef(cmd.shootingTarget)) {
      this.shootingTarget = cmd.shootingTarget;
    } else {
      this.shootingTarget = null;
    };
    this.lookAtTarget = cmd.lookAtTarget;
    this.hasSplineEndPoint = false;
    if(ToBool(this.companion)) {
      this.hasSplineEndPoint = GetEndPointOfSpline(GetOwner(context).GetGame(), this.spline, this.splineEndPoint);
    };
    this.m_playerCompanion = RefToWeakRef(Cast(WeakRefToRef(this.companion)));
    if(ToBool(this.m_playerCompanion) && cmd.useMatchForSpeedForPlayer && this.desiredDistance < 0) {
      this.useMatchForSpeedForPlayer = true;
    } else {
      this.useMatchForSpeedForPlayer = false;
    };
    if(this.desiredDistance < 0) {
      this.shouldBeInFrontOfCompanion = true;
    } else {
      this.shouldBeInFrontOfCompanion = false;
    };
    return true;
  }

  public final Bool DoEndMoveOnSpline() {
    return true;
  }

  public final Bool DoFindClosestPointOnSpline(ScriptExecutionContext context) {
    if(!GetClosestPointOnSpline(GetOwner(context).GetGame(), this.spline, GetOwner(context).GetWorldPosition(), this.teleportDestination)) {
      return false;
    };
    return true;
  }

  public final Bool DoFindStartOfTheSpline(ScriptExecutionContext context) {
    if(!GetStartPointOfSpline(GetOwner(context).GetGame(), this.spline, this.teleportDestination)) {
      return false;
    };
    return true;
  }

  public final Bool DoFindEndOfTheSpline(ScriptExecutionContext context) {
    return false;
  }

  public final Bool GetIsMoveToSplineNeeded(ScriptExecutionContext context) {
    if(IsSplineStartRecalculated(context, this.spline)) {
      return false;
    };
    if(ArePositionsEqual(GetOwner(context).GetWorldPosition(), this.teleportDestination)) {
      return false;
    };
    return true;
  }

  private final void OnWalkingOnSpline(ScriptExecutionContext context, Bool success, Bool isCompanionProgressing) {
    if(success || !isCompanionProgressing) {
      this.m_firstWaitingDemandTimestamp = -1;
    } else {
      if(this.m_firstWaitingDemandTimestamp < 0) {
        this.m_firstWaitingDemandTimestamp = ToFloat(GetSimTime(GetOwner(context).GetGame()));
      };
    };
  }

  private final Bool ShouldBeWaitingDelayed(ScriptExecutionContext context) {
    return this.m_firstWaitingDemandTimestamp > 0 && ToFloat(GetSimTime(GetOwner(context).GetGame())) - this.m_firstWaitingDemandTimestamp < 4;
  }

  public final Bool DoUpdateDistanceToCompanionOnSpline(ScriptExecutionContext context) {
    ref<ScriptedPuppet> owner;
    ref<GameObject> companionHandle;
    Float distanceToDestination;
    Float companionDistance;
    Vector4 closestPointOnSpline;
    Float incline;
    Float absoluteDistToCompanion;
    Float distanceToSpline;
    Bool isCompanionProgressingOnSpline;
    ref<MovePoliciesComponent> movePoliciesComponent;
    companionHandle = WeakRefToRef(this.companion);
    owner = Cast(GetOwner(context));
    movePoliciesComponent = owner.GetMovePolicesComponent();
    if(!ToBool(companionHandle) || !ToBool(owner)) {
      this.distanceToCompanion = 0;
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    if(!IsOnTheSpline(owner, 0.5)) {
      this.distanceToCompanion = 0;
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    if(!ToBool(companionHandle) || !ToBool(owner) || !GetClosestPointOnSpline(GetOwner(context).GetGame(), this.spline, companionHandle.GetWorldPosition(), closestPointOnSpline)) {
      this.distanceToCompanion = 0;
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    if(movePoliciesComponent.IsOnStairs()) {
      OnWalkingOnSpline(context, true, true);
      return true;
    };
    if(this.maxCompanionDistanceOnSpline > 0 && !IsOnTheSpline(Cast(companionHandle), 3) && this.lowestCompanionDistanceToEnd > 5) {
      OnWalkingOnSpline(context, false, isCompanionProgressingOnSpline);
      if(!ShouldBeWaitingDelayed(context)) {
        SetWaitForCompanion(owner, true);
      };
      return true;
    };
    if(!CalculateDistanceToEndFrom(GetOwner(context).GetGame(), this.spline, GetOwner(context).GetWorldPosition(), distanceToDestination)) {
      this.distanceToCompanion = 0;
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    if(!CalculateDistanceToEndFrom(GetOwner(context).GetGame(), this.spline, companionHandle.GetWorldPosition(), companionDistance)) {
      this.distanceToCompanion = 0;
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    incline = owner.GetMovePolicesComponent().GetInclineAngle();
    absoluteDistToCompanion = Length(owner.GetWorldPosition() - companionHandle.GetWorldPosition());
    distanceToSpline = Distance(closestPointOnSpline, WeakRefToRef(this.companion).GetWorldPosition());
    if(distanceToSpline > AbsF(this.desiredDistance)) {
      companionDistance += distanceToSpline;
    };
    this.distanceToCompanion = distanceToDestination - companionDistance;
    isCompanionProgressingOnSpline = this.previousCompanionDistanceToEnd > companionDistance;
    this.previousCompanionDistanceToEnd = companionDistance;
    this.lowestCompanionDistanceToEnd = MinF(this.lowestCompanionDistanceToEnd, companionDistance);
    if(this.matchTargetSpeed) {
      DoUpdateSpeed(context);
    };
    if(IsCooldownActive(owner, "WaitForCompanion")) {
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    if(companionDistance < 0.10000000149011612) {
      OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
      return true;
    };
    if(this.desiredDistance < 0) {
      if(AbsF(incline) > 5) {
        OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
        return true;
      };
      if(DontWaitToCompanionNearEnd(owner, distanceToDestination, companionDistance)) {
        OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
        return true;
      };
    };
    if(this.distanceToCompanion < this.desiredDistance) {
      OnWalkingOnSpline(context, false, isCompanionProgressingOnSpline);
      if(!ShouldBeWaitingDelayed(context)) {
        SetWaitForCompanion(owner, true);
      };
      return true;
    };
    OnWalkingOnSpline(context, true, isCompanionProgressingOnSpline);
    return true;
  }

  private final void DoUpdateSpeed(ScriptExecutionContext context) {
    if(ToBool(this.m_playerCompanion)) {
      if(GetCurrentLocomotionState(this.m_playerCompanion) == gamePSMLocomotionStates.Sprint) {
        this.sprint = true;
        this.run = false;
        return ;
      };
    };
    if(this.distanceToCompanion >= GetSprintSpeedDistance(context)) {
      this.sprint = true;
      this.run = false;
    } else {
      this.sprint = false;
      this.run = false;
    };
  }

  public final Bool DoUpdateWaitForCompanionOnSpline(ScriptExecutionContext context) {
    ref<ScriptedPuppet> owner;
    ref<GameObject> companionHandle;
    Vector4 companionPosition;
    Float distanceToDestination;
    Float companionDistance;
    Vector4 closestPointOnSplineCompanion;
    Float absoluteDistToCompanion;
    Float companionDistanceSqFromSplineEnd;
    Float tolerance;
    Float distanceToSpline;
    ref<MovePoliciesComponent> movePoliciesComponent;
    companionHandle = WeakRefToRef(this.companion);
    companionPosition = companionHandle.GetWorldPosition();
    owner = Cast(GetOwner(context));
    movePoliciesComponent = owner.GetMovePolicesComponent();
    if(!ToBool(companionHandle) || !ToBool(owner) || !GetClosestPointOnSpline(GetOwner(context).GetGame(), this.spline, companionHandle.GetWorldPosition(), closestPointOnSplineCompanion)) {
      this.distanceToCompanion = 0;
      return true;
    };
    if(this.maxCompanionDistanceOnSpline > 0 && !IsOnTheSpline(Cast(companionHandle), 3) && this.lowestCompanionDistanceToEnd > 5) {
      this.distanceToCompanion = 0;
      return true;
    };
    if(!CalculateDistanceToEndFrom(GetOwner(context).GetGame(), this.spline, GetOwner(context).GetWorldPosition(), distanceToDestination)) {
      this.distanceToCompanion = 0;
      return true;
    };
    if(!CalculateDistanceToEndFrom(GetOwner(context).GetGame(), this.spline, companionHandle.GetWorldPosition(), companionDistance)) {
      this.distanceToCompanion = 0;
      return true;
    };
    distanceToSpline = Distance(closestPointOnSplineCompanion, companionHandle.GetWorldPosition());
    if(distanceToSpline > AbsF(this.desiredDistance)) {
      companionDistance += distanceToSpline;
    };
    absoluteDistToCompanion = Length(owner.GetWorldPosition() - companionHandle.GetWorldPosition());
    this.distanceToCompanion = distanceToDestination - companionDistance;
    this.lowestCompanionDistanceToEnd = MinF(this.lowestCompanionDistanceToEnd, companionDistance);
    if(movePoliciesComponent.IsOnStairs()) {
      SetWaitForCompanion(owner, false);
      return true;
    };
    if(IsCooldownActive(owner, "WaitForCompanion")) {
      return true;
    };
    if(distanceToDestination >= companionDistance) {
      if(absoluteDistToCompanion < 2) {
        return true;
      };
    };
    if(DontWaitToCompanionNearEnd(owner, distanceToDestination, companionDistance)) {
      return true;
    };
    if(this.shouldBeInFrontOfCompanion) {
      tolerance = this.deadZoneRadius;
    } else {
      tolerance = -this.deadZoneRadius;
    };
    if(this.distanceToCompanion > this.desiredDistance + tolerance) {
      SetWaitForCompanion(owner, false);
      return true;
    };
    return true;
  }

  private final Bool DontWaitToCompanionNearEnd(ref<ScriptedPuppet> owner, Float distanceToDestination, Float companionDistance) {
    if(this.noWaitToEndDistance > 0) {
      if(distanceToDestination < this.noWaitToEndDistance) {
        if(this.noWaitToEndCompanionDistance > 0) {
          if(companionDistance < this.noWaitToEndCompanionDistance) {
            SetWaitForCompanion(owner, false);
            return true;
          };
        } else {
          SetWaitForCompanion(owner, false);
          return true;
        };
      };
    };
    return false;
  }

  private final Bool IsOnTheSpline(ref<ScriptedPuppet> target, Float tolerance) {
    Vector4 closestPointOnSpline;
    Float distanceToSpline;
    if(!ToBool(target) || !GetClosestPointOnSpline(target.GetGame(), this.spline, target.GetWorldPosition(), closestPointOnSpline)) {
      return false;
    };
    distanceToSpline = Length(target.GetWorldPosition() - closestPointOnSpline);
    if(distanceToSpline >= tolerance) {
      return false;
    };
    return true;
  }

  private final void SetWaitForCompanion(ref<ScriptedPuppet> owner, Bool value) {
    if(IsCooldownActive(owner, "WaitForCompanion")) {
      return ;
    };
    if(this.waitForCompanion != value) {
      if(value) {
        SendActionSignal(RefToWeakRef(owner), "WaitForCompanion", -1);
      } else {
        ResetActionSignal(RefToWeakRef(owner), "WaitForCompanion");
      };
      this.waitForCompanion = value;
      StartCooldown(owner, "WaitForCompanion", 2);
    };
  }

  public final Bool DoEndTeleportToCompanionOnSpline() {
    this.startFromClosestPoint = true;
    return true;
  }

  public final Bool DoStartWaitForCompanion() {
    this.useStart = false;
    return true;
  }

  public final Bool DoEndWaitForCompanion() {
    this.startFromClosestPoint = true;
    return true;
  }

  public final Bool SelectSplineTeleportTarget(ScriptExecutionContext context) {
    ref<ScriptedPuppet> owner;
    Vector4 center;
    Vector4 adjustedCenter;
    Float radius;
    NavigationFindPointResult findResult;
    owner = Cast(GetOwner(context));
    radius = this.desiredDistance;
    center = WeakRefToRef(this.companion).GetWorldPosition() - WeakRefToRef(this.companion).GetWorldForward() * radius * 1.100000023841858;
    adjustedCenter = owner.GetMovePolicesComponent().GetClosestPointToPath(center);
    findResult = GetNavigationSystem(GetOwner(context).GetGame()).FindPointInSphere(adjustedCenter, radius, NavGenAgentSize.Human, false);
    if(findResult.status != worldNavigationRequestStatus.OK) {
      return false;
    };
    this.teleportDestination = findResult.point;
    return true;
  }

  public final Float GetSprintSpeedDistance(ScriptExecutionContext context) {
    TweakDBID actionId;
    Float result;
    Float tolerance;
    if(ToInt(this.movementType) == ToInt(moveMovementType.Sprint)) {
      return -99999;
    };
    if(this.shouldBeInFrontOfCompanion) {
      actionId = "IdleActions.MoveOnSplineWithCompanionParams.moveToFrontSprintSpeedDistance";
      tolerance = -6;
    } else {
      actionId = "IdleActions.MoveOnSplineWithCompanionParams.catchUpSprintSpeedDistance";
      tolerance = 6;
    };
    result = GetFloat(actionId, 12);
    if(this.sprint) {
      result += tolerance;
    };
    return result;
  }

  public final Float GetRunSpeedDistance(ScriptExecutionContext context) {
    TweakDBID actionId;
    Float result;
    Float tolerance;
    if(ToInt(this.movementType) == ToInt(moveMovementType.Run)) {
      return -99999;
    };
    if(this.shouldBeInFrontOfCompanion) {
      actionId = "IdleActions.MoveOnSplineWithCompanionParams.moveToFrontRunSpeedDistance";
      tolerance = -3;
    } else {
      actionId = "IdleActions.MoveOnSplineWithCompanionParams.catchUpRunSpeedDistance";
      tolerance = 3;
    };
    result = GetFloat(actionId, 6.5);
    if(this.run && this.shouldBeInFrontOfCompanion) {
      result += tolerance;
    };
    return result;
  }

  public final Float GetTeleportDistance(ScriptExecutionContext context) {
    TweakDBID actionId;
    actionId = "IdleActions.MoveOnSplineWithCompanionParams.catchUpTeleportDistance";
    return GetFloat(actionId, 20);
  }

  public final Bool DoStartFollowTarget(ScriptExecutionContext context) {
    ref<AIFollowTargetCommand> cmd;
    cmd = this.m_followTargetCommand;
    if(!ToBool(cmd)) {
      return false;
    };
    this.companion = cmd.target;
    this.desiredDistance = cmd.desiredDistance;
    this.deadZoneRadius = cmd.tolerance;
    this.stopWhenDestinationReached = cmd.stopWhenDestinationReached;
    this.movementType = cmd.movementType;
    this.lookAtTarget = cmd.lookAtTarget;
    this.matchTargetSpeed = cmd.matchSpeed;
    this.teleportToTarget = cmd.teleport;
    this.shouldTeleportNow = false;
    return true;
  }

  public final Bool SelectFollowTeleportTarget(ScriptExecutionContext context) {
    if(!GetNavigationSystem(GetOwner(context).GetGame()).GetFurthestNavmeshPointBehind(this.companion, 3, 3, this.teleportDestination, true)) {
      return false;
    };
    return true;
  }
}
