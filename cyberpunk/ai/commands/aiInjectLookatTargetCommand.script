
public class InjectLookatTargetCommandTask extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  protected wref<AIInjectLookatTargetCommand> m_currentCommand;

  protected Float m_activationTimeStamp;

  protected Float m_commandDuration;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIInjectLookatTargetCommand> typedCommand;
    GlobalNodeRef globalRef;
    wref<GameObject> target;
    array<EntityID> targetIDs;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    typedCommand = Cast(rawCommand);
    if(ToBool(this.m_currentCommand) && !ToBool(typedCommand)) {
      CancelCommand(context);
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    if(typedCommand == WeakRefToRef(this.m_currentCommand)) {
      if(this.m_commandDuration > 0 && ToFloat(GetAITime(context)) > this.m_activationTimeStamp + this.m_commandDuration) {
        CancelCommand(context);
        if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
          GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, true);
        };
      };
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    this.m_currentCommand = RefToWeakRef(typedCommand);
    this.m_commandDuration = typedCommand.duration;
    this.m_activationTimeStamp = ToFloat(GetAITime(context));
    if(!GetGameObjectFromEntityReference(typedCommand.targetPuppetRef, GetOwner(context).GetGame(), target)) {
      globalRef = ResolveNodeRef(typedCommand.targetNodeRef, Cast(GetRoot()));
      target = RefToWeakRef(Cast(FindEntityByID(GetGame(context), Cast(globalRef))));
    };
    if(!ToBool(target)) {
      CancelCommand(context);
      if(ToBool(typedCommand) && typedCommand.state == AICommandState.Executing) {
        GetPuppet(context).GetAIControllerComponent().StopExecutingCommand(typedCommand, true);
      };
    } else {
      SetArgumentObject(context, "CommandAimTarget", target);
    };
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  private final void Deactivate(ScriptExecutionContext context) {
    CancelCommand(context);
  }

  protected final void CancelCommand(ScriptExecutionContext context) {
    SetArgumentObject(context, "CommandAimTarget", null);
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
    this.m_activationTimeStamp = 0;
    this.m_commandDuration = 0;
    this.m_currentCommand = null;
  }
}
