
public class FindClosestScavengeTarget extends AIbehaviortaskScript {

  protected void Activate(ScriptExecutionContext context) {
    ref<NPCPuppet> owner;
    ref<ScavengeComponent> scavengeComponent;
    array<wref<GameObject>> scavengeTargets;
    wref<GameObject> closestTarget;
    owner = Cast(GetOwner(context));
    scavengeComponent = owner.GetScavengeComponent();
    scavengeTargets = scavengeComponent.GetScavengeTargets();
    closestTarget = GetClosestTarget(context, scavengeTargets);
    SetArgumentObject(context, "ScavengeTarget", closestTarget);
    SetArgumentVector(context, "ScavengeTargetPos", new Vector4(0,0,0,0));
  }

  private final wref<GameObject> GetClosestTarget(ScriptExecutionContext context, array<wref<GameObject>> targets) {
    Int32 i;
    Float currentTargetDistance;
    Float shortestDistance;
    wref<GameObject> closestTarget;
    i = 0;
    while(i < Size(targets)) {
      currentTargetDistance = Distance(GetOwner(context).GetWorldPosition(), WeakRefToRef(targets[i]).GetWorldPosition());
      if(currentTargetDistance < shortestDistance || shortestDistance == 0) {
        shortestDistance = currentTargetDistance;
        closestTarget = targets[i];
      };
      i += 1;
    };
    return closestTarget;
  }
}

public class MoveToScavengeTarget extends AIbehaviortaskScript {

  [Default(MoveToScavengeTarget, -1.f))]
  private Float m_lastTime;

  private Float m_timeout;

  [Default(MoveToScavengeTarget, 0.1f))]
  private Float m_timeoutDuration;

  protected void Activate(ScriptExecutionContext context) {
    this.m_timeout = this.m_timeoutDuration;
  }

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    wref<GameObject> scavengeTarget;
    ref<gameChangeDestination> changeDestinationEvent;
    ref<ActionEvent> actionEvent;
    Float dt;
    if(this.m_lastTime < 0) {
      this.m_lastTime = GetAITime(context);
    };
    dt = GetAITime(context) - this.m_lastTime;
    this.m_lastTime = GetAITime(context);
    this.m_timeout -= dt;
    if(this.m_timeout > 0) {
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    this.m_timeout = this.m_timeoutDuration;
    scavengeTarget = GetArgumentObject(context, "ScavengeTarget");
    SetArgumentVector(context, "ScavengeTargetPos", WeakRefToRef(scavengeTarget).GetWorldPosition());
    changeDestinationEvent = new gameChangeDestination();
    changeDestinationEvent.destination = WeakRefToRef(scavengeTarget).GetWorldPosition();
    actionEvent = new ActionEvent();
    actionEvent.name = "actionEvent";
    actionEvent.internalEvent = changeDestinationEvent;
    GetOwner(context).QueueEvent(actionEvent);
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  protected void Deactivate(ScriptExecutionContext context) {
    this.m_lastTime = -1;
    this.m_timeout = this.m_timeoutDuration;
  }
}

public class ScavengeTarget extends AIbehaviortaskScript {

  protected void Activate(ScriptExecutionContext context) {
    wref<GameObject> scavengeTarget;
    ref<DisassembleEvent> targetDisassembleEvent;
    scavengeTarget = GetArgumentObject(context, "ScavengeTarget");
    targetDisassembleEvent = new DisassembleEvent();
    WeakRefToRef(scavengeTarget).QueueEvent(targetDisassembleEvent);
  }
}

public class HaveScavengeTargets extends AIbehaviorconditionScript {

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    ref<NPCPuppet> owner;
    ref<ScavengeComponent> scavengeComponent;
    array<wref<GameObject>> scavengeTargets;
    owner = Cast(GetOwner(context));
    scavengeComponent = owner.GetScavengeComponent();
    scavengeTargets = scavengeComponent.GetScavengeTargets();
    return Cast(Size(scavengeTargets) > 0);
  }
}

public class AITakedownHandler extends AIbehaviortaskScript {

  public inline edit ref<AIArgumentMapping> m_inCommand;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIFollowerTakedownCommand> takedownCommand;
    ref<AIHumanComponent> aiComponent;
    if(!ToBool(this.m_inCommand)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    takedownCommand = Cast(rawCommand);
    if(!ToBool(takedownCommand)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(!ToBool(takedownCommand.target)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(WeakRefToRef(GetArgumentObject(context, "TakedownTarget")) != WeakRefToRef(takedownCommand.target)) {
      SetArgumentObject(context, "TakedownTarget", takedownCommand.target);
    };
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  private final void Deactivate(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIFollowerTakedownCommand> takedownCommand;
    ref<AIHumanComponent> aiComponent;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    takedownCommand = Cast(rawCommand);
    aiComponent = GetPuppet(context).GetAIControllerComponent();
    if(ToBool(takedownCommand)) {
      aiComponent.StopExecutingCommand(takedownCommand, true);
    };
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
  }
}

public class AICommandDeviceHandler extends AIbehaviortaskScript {

  public inline edit ref<AIArgumentMapping> m_inCommand;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIFollowerDeviceCommand> deviceCommand;
    ref<AIHumanComponent> aiComponent;
    if(!ToBool(this.m_inCommand)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    deviceCommand = Cast(rawCommand);
    if(!ToBool(deviceCommand)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(!ToBool(deviceCommand.target)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    if(WeakRefToRef(GetArgumentObject(context, "Target")) != WeakRefToRef(deviceCommand.target)) {
      SetArgumentObject(context, "Target", deviceCommand.target);
    };
    if(WeakRefToRef(deviceCommand.overrideMovementTarget) != null && WeakRefToRef(GetArgumentObject(context, "MovementTarget")) != WeakRefToRef(deviceCommand.overrideMovementTarget)) {
      SetArgumentObject(context, "MovementTarget", deviceCommand.overrideMovementTarget);
    } else {
      if(WeakRefToRef(deviceCommand.overrideMovementTarget) == null) {
        SetArgumentObject(context, "MovementTarget", deviceCommand.target);
      };
    };
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }
}

public class AISetSoloModeHandler extends AIbehaviortaskScript {

  public inline edit ref<AIArgumentMapping> m_inCommand;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIFlatheadSetSoloModeCommand> soloModeCommand;
    if(!ToBool(this.m_inCommand)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    soloModeCommand = Cast(rawCommand);
    if(!ToBool(soloModeCommand)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    SetArgumentBool(context, "SoloMode", soloModeCommand.soloModeState);
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  private final void Deactivate(ScriptExecutionContext context) {
    ref<IScriptable> rawCommand;
    ref<AIFlatheadSetSoloModeCommand> soloModeCommand;
    rawCommand = GetScriptableMappingValue(context, this.m_inCommand);
    soloModeCommand = Cast(rawCommand);
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
  }
}

public class IsCombatModuleEquipped extends AIAutonomousConditions {

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    ref<NPCPuppet> owner;
    Bool checkResult;
    owner = Cast(GetOwner(context));
    return Cast(GetStatsSystem(owner.GetGame()).GetStatBoolValue(Cast(owner.GetEntityID()), gamedataStatType.CanPickUpWeapon));
  }
}

public class AIPrepareTakedownData extends AIbehaviortaskScript {

  protected void Activate(ScriptExecutionContext context) {
    FillTakedownData(RefToWeakRef(GetPuppet(context)), RefToWeakRef(GetPuppet(context)), GetArgumentObject(context, "TakedownTarget"), Cast("takedowns"), "kill");
  }
}

public class AIDeviceFeedbackData extends AIbehaviortaskScript {

  protected void Activate(ScriptExecutionContext context) {
    WeakRefToRef(GetArgumentObject(context, "Target")).QueueEvent(new SpiderbotOrderCompletedEvent());
  }
}

public class AIFindForwardPositionAround extends AIbehaviortaskScript {

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    Float angleStep;
    Float angle;
    Float angleRad;
    Quaternion orientation;
    Quaternion quat;
    Vector4 findWallEndPos;
    ref<NavigationFindWallResult> fw;
    angleStep = 30;
    orientation = GetOwner(context).GetWorldOrientation();
    angle = 0;
    while(angle < 360) {
      SetIdentity(quat);
      angleRad = Deg2Rad(angle);
      SetZRot(quat, angleRad);
      quat = orientation * quat;
      findWallEndPos = GetOwner(context).GetWorldPosition() + GetForward(quat) * 1;
      fw = GetNavigationSystem(GetGame(context)).FindWallInLine(GetOwner(context).GetWorldPosition(), findWallEndPos, NavGenAgentSize.Human, 0.20000000298023224);
      if(fw.status == worldNavigationRequestStatus.OK && !fw.isHit) {
        SetArgumentVector(context, "ForwardPosition", findWallEndPos);
        return AIbehaviorUpdateOutcome.SUCCESS;
      };
      angle += angleStep;
    };
    if(fw.status != worldNavigationRequestStatus.OK) {
      SetArgumentVector(context, "ForwardPosition", GetOwner(context).GetWorldPosition());
      return AIbehaviorUpdateOutcome.SUCCESS;
    };
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }
}

public class AIFindPositionAroundSelf extends AIbehaviortaskScript {

  public inline edit ref<AIArgumentMapping> m_distanceMin;

  public inline edit ref<AIArgumentMapping> m_distanceMax;

  public inline edit Float m_angle;

  public inline edit Float m_angleOffset;

  public inline edit ref<AIArgumentMapping> m_outPositionArgument;

  protected Vector4 m_finalPosition;

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    Float currentAngle;
    Float initialAngle;
    Float angleRad;
    Quaternion orientation;
    Quaternion quat;
    ref<NavigationFindWallResult> fw;
    Int32 i;
    Float distance;
    orientation = GetOwner(context).GetWorldOrientation();
    initialAngle = this.m_angleOffset - this.m_angle / 2;
    i = 0;
    while(i < 5) {
      currentAngle = RandRangeF(initialAngle, initialAngle + this.m_angle);
      SetIdentity(quat);
      angleRad = Deg2Rad(currentAngle);
      SetZRot(quat, angleRad);
      quat = orientation * quat;
      distance = RandRangeF(FromVariant(GetMappingValue(context, this.m_distanceMin)), FromVariant(GetMappingValue(context, this.m_distanceMax)));
      this.m_finalPosition = GetOwner(context).GetWorldPosition() + GetForward(quat) * distance;
      fw = GetNavigationSystem(GetGame(context)).FindWallInLine(GetOwner(context).GetWorldPosition(), this.m_finalPosition, NavGenAgentSize.Human, 0.20000000298023224);
      if(fw.status == worldNavigationRequestStatus.OK && !fw.isHit && AdditionalOutcomeVerification(context)) {
        SetMappingValue(context, this.m_outPositionArgument, ToVariant(this.m_finalPosition));
        return AIbehaviorUpdateOutcome.SUCCESS;
      };
      i += 1;
    };
    return AIbehaviorUpdateOutcome.FAILURE;
  }

  protected Bool AdditionalOutcomeVerification(ScriptExecutionContext context) {
    return true;
  }
}

public class AISpiderbotFindBoredMovePosition extends AIFindPositionAroundSelf {

  public inline edit ref<AIArgumentMapping> m_maxWanderDistance;

  protected void Activate(ScriptExecutionContext context) {
    this.m_angle = 330;
    this.m_angleOffset = 180;
  }

  protected Bool AdditionalOutcomeVerification(ScriptExecutionContext context) {
    if(Distance(WeakRefToRef(GetArgumentObject(context, "FriendlyTarget")).GetWorldPosition(), this.m_finalPosition) > FromVariant(GetMappingValue(context, this.m_maxWanderDistance))) {
      return false;
    };
    return true;
  }
}

public class AISpiderbotCheckIfFriendlyMoved extends AIAutonomousConditions {

  public inline edit ref<AIArgumentMapping> m_maxAllowedDelta;

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    Float maxAllowed;
    Float dist;
    maxAllowed = FromVariant(GetMappingValue(context, this.m_maxAllowedDelta));
    dist = Distance(WeakRefToRef(GetArgumentObject(context, "FriendlyTarget")).GetWorldPosition(), GetArgumentVector(context, "FriendlyTargetLastPosition"));
    return Cast(Distance(WeakRefToRef(GetArgumentObject(context, "FriendlyTarget")).GetWorldPosition(), GetArgumentVector(context, "FriendlyTargetLastPosition")) > FromVariant(GetMappingValue(context, this.m_maxAllowedDelta)));
  }
}

public class AIFindPositionAroundTarget extends AIbehaviortaskScript {

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    Float angleStep;
    Float angle;
    Quaternion orientation;
    Quaternion quat;
    Vector4 currentPosition;
    Vector4 potentialPosition;
    Vector4 bestPosition;
    ref<NavigationPath> navigationPath;
    ref<NavigationFindWallResult> fw;
    wref<GameObject> target;
    wref<GameObject> friendlyTarget;
    angleStep = 15;
    target = GetArgumentObject(context, "CombatTarget");
    friendlyTarget = GetArgumentObject(context, "FriendlyTarget");
    if(!ToBool(target)) {
      return AIbehaviorUpdateOutcome.FAILURE;
    };
    orientation = WeakRefToRef(target).GetWorldOrientation();
    currentPosition = GetOwner(context).GetWorldPosition();
    angle = 90;
    while(angle < 270) {
      SetIdentity(quat);
      SetZRot(quat, Deg2Rad(angle));
      quat = orientation * quat;
      potentialPosition = WeakRefToRef(target).GetWorldPosition() + GetForward(quat) * RandRangeF(2.5, 3.5);
      navigationPath = GetNavigationSystem(GetGame(context)).CalculatePath(GetOwner(context).GetWorldPosition(), potentialPosition, NavGenAgentSize.Human, 0.20000000298023224);
      fw = GetNavigationSystem(GetGame(context)).FindWallInLine(WeakRefToRef(target).GetWorldPosition(), potentialPosition, NavGenAgentSize.Human, 0.20000000298023224);
      if(navigationPath != null && fw.status == worldNavigationRequestStatus.OK && !fw.isHit && Distance(currentPosition, WeakRefToRef(friendlyTarget).GetWorldPosition()) < 25) {
        if(IsZero(bestPosition) || Distance(currentPosition, potentialPosition) < Distance(currentPosition, bestPosition)) {
          bestPosition = potentialPosition;
        };
      };
      angle += angleStep;
    };
    if(!IsZero(bestPosition)) {
      SetArgumentVector(context, "ForwardPosition", bestPosition);
      return AIbehaviorUpdateOutcome.SUCCESS;
    };
    return AIbehaviorUpdateOutcome.FAILURE;
  }
}

public class AISetHealthRegenerationState extends AIbehaviortaskScript {

  public edit Bool healthRegeneration;

  protected void Activate(ScriptExecutionContext context) {
    StatPoolModifier emptyModifier;
    if(this.healthRegeneration) {
      GetStatPoolsSystem(GetOwner(context).GetGame()).RequestResetingModifier(Cast(GetOwner(context).GetEntityID()), gamedataStatPoolType.Health, gameStatPoolModificationTypes.Regeneration);
    } else {
      GetStatPoolsSystem(GetOwner(context).GetGame()).RequestSettingModifier(Cast(GetOwner(context).GetEntityID()), gamedataStatPoolType.Health, gameStatPoolModificationTypes.Regeneration, emptyModifier);
    };
  }
}

public class AISetAutocraftingState extends AIbehaviortaskScript {

  public edit Bool newState;

  protected void Activate(ScriptExecutionContext context) {
    ref<AutocraftSystem> autocraftSystem;
    ref<AutocraftActivateRequest> autocraftActivateRequest;
    ref<AutocraftDeactivateRequest> autocraftDeactivateRequest;
    autocraftSystem = Cast(GetScriptableSystemsContainer(GetOwner(context).GetGame()).Get("AutocraftSystem"));
    if(this.newState) {
      autocraftActivateRequest = new AutocraftActivateRequest();
      autocraftSystem.QueueRequest(autocraftActivateRequest);
    } else {
      autocraftDeactivateRequest = new AutocraftDeactivateRequest();
      autocraftDeactivateRequest.resetMemory = false;
      autocraftSystem.QueueRequest(autocraftDeactivateRequest);
    };
  }
}

public class SelectClosestPlayerThreat extends AIbehaviortaskScript {

  protected void Activate(ScriptExecutionContext context) {
    ref<TargetTrackerComponent> trackerComponent;
    array<TrackedLocation> threats;
    Vector4 playerPosition;
    Float tempDistance;
    Float closestDistance;
    ref<Entity> closestThreat;
    Int32 i;
    trackerComponent = GetPuppet(context).GetTargetTrackerComponent();
    threats = trackerComponent.GetHostileThreats(false);
    playerPosition = GetPlayerSystem(GetGame(context)).GetLocalPlayerMainGameObject().GetWorldPosition();
    i = 0;
    while(i < Size(threats)) {
      tempDistance = Distance(playerPosition, threats[i].location.position);
      if(tempDistance < closestDistance || closestDistance == 0) {
        closestDistance = tempDistance;
        closestThreat = WeakRefToRef(threats[i].entity);
      };
      i += 1;
    };
    SetArgumentObject(context, "ClosestThreat", RefToWeakRef(Cast(closestThreat)));
  }
}

public class SetManouverPosition extends AIbehaviortaskScript {

  public edit Float m_distance;

  public edit Float m_angle;

  protected void Activate(ScriptExecutionContext context) {
    Vector4 manouverDestination;
    manouverDestination = GetOwner(context).GetWorldPosition() + RotByAngleXY(GetOwner(context).GetWorldForward(), this.m_angle) * this.m_distance;
    SetArgumentVector(context, "ManouverPosition", manouverDestination);
  }
}

public class IsAnyThreatClose extends AIAutonomousConditions {

  public edit Float m_distance;

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    ref<TargetTrackerComponent> trackerComponent;
    array<TrackedLocation> threats;
    Vector4 puppetPosition;
    Float distance;
    Int32 i;
    trackerComponent = GetPuppet(context).GetTargetTrackerComponent();
    threats = trackerComponent.GetThreats(false);
    puppetPosition = GetOwner(context).GetWorldPosition();
    i = 0;
    while(i < Size(threats)) {
      distance = Distance(puppetPosition, threats[i].location.position);
      if(distance < this.m_distance) {
        SetArgumentObject(context, "ClosestThreat", RefToWeakRef(Cast(WeakRefToRef(threats[i].entity))));
        return Cast(true);
      };
      i += 1;
    };
    return Cast(false);
  }
}

public class RemoveCommand extends AIbehaviortaskScript {

  protected inline edit ref<AIArgumentMapping> m_inCommand;

  protected void Activate(ScriptExecutionContext context) {
    SetMappingValue(context, this.m_inCommand, ToVariant(null));
  }
}
