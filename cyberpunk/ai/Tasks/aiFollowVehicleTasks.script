
public class GetFollowTarget extends FollowVehicleTask {

  private ref<IBlackboard> m_blackboard;

  private wref<VehicleObject> m_vehicle;

  [Default(GetFollowTarget, -1.f))]
  private Float m_lastTime;

  private Float m_timeout;

  [Default(GetFollowTarget, 0.1f))]
  private Float m_timeoutDuration;

  protected void Activate(ScriptExecutionContext context) {
    ref<RequestSlotEvent> slotRequest;
    GetVehicle(GetGame(context), RefToWeakRef(GetPlayerSystem(GetGame(context)).GetLocalPlayerMainGameObject()), this.m_vehicle);
    this.m_blackboard = Create(GetAllBlackboardDefs().AIFollowSlot);
    SetArgumentVector(context, "MovementTarget", WeakRefToRef(this.m_vehicle).GetWorldPosition());
    slotRequest = new RequestSlotEvent();
    slotRequest.blackboard = this.m_blackboard;
    slotRequest.requester = RefToWeakRef(GetOwner(context));
    WeakRefToRef(this.m_vehicle).QueueEvent(slotRequest);
    this.m_timeout = this.m_timeoutDuration;
  }

  protected AIbehaviorUpdateOutcome Update(ScriptExecutionContext context) {
    Transform followSlot;
    Vector4 followPointWorldOffset;
    Vector4 target;
    Float dt;
    ref<gameChangeDestination> changeDestinationEvent;
    ref<ActionEvent> actionEvent;
    if(this.m_lastTime < 0) {
      this.m_lastTime = GetAITime(context);
    };
    dt = GetAITime(context) - this.m_lastTime;
    this.m_lastTime = GetAITime(context);
    this.m_timeout -= dt;
    if(this.m_timeout > 0) {
      return AIbehaviorUpdateOutcome.IN_PROGRESS;
    };
    this.m_timeout = this.m_timeoutDuration;
    followSlot = FromVariant(this.m_blackboard.GetVariant(GetAllBlackboardDefs().AIFollowSlot.slotTransform));
    if(!IsZero(GetPosition(followSlot))) {
      followPointWorldOffset = RotByAngleXY(GetPosition(followSlot), -1 * Heading(WeakRefToRef(this.m_vehicle).GetWorldForward()));
      target = WeakRefToRef(this.m_vehicle).GetWorldPosition() + followPointWorldOffset;
      changeDestinationEvent = new gameChangeDestination();
      changeDestinationEvent.destination = target;
      actionEvent = new ActionEvent();
      actionEvent.name = "actionEvent";
      actionEvent.internalEvent = changeDestinationEvent;
      GetOwner(context).QueueEvent(actionEvent);
      SetArgumentVector(context, "MovementTarget", target);
      return AIbehaviorUpdateOutcome.SUCCESS;
    };
    return AIbehaviorUpdateOutcome.IN_PROGRESS;
  }

  protected void Deactivate(ScriptExecutionContext context) {
    ref<ReleaseSlotEvent> slotReleaseRequest;
    slotReleaseRequest = new ReleaseSlotEvent();
    slotReleaseRequest.slotID = this.m_blackboard.GetInt(GetAllBlackboardDefs().AIFollowSlot.slotID);
    WeakRefToRef(this.m_vehicle).QueueEvent(slotReleaseRequest);
  }
}

public class CheckFollowTarget extends AIbehaviorconditionScript {

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    Vector4 nodeRef;
    nodeRef = GetArgumentVector(context, "MovementTarget");
    if(!IsZero(nodeRef)) {
      return Cast(true);
    };
    return Cast(false);
  }
}

public class CheckTargetInVehicle extends AIbehaviorconditionScript {

  protected AIbehaviorConditionOutcomes Check(ScriptExecutionContext context) {
    ref<GameObject> combatTarget;
    ref<Vehicle_Record> targetVehicle;
    combatTarget = WeakRefToRef(GetArgumentObject(context, "CombatTarget"));
    if(!ToBool(combatTarget) || !IsDriver(combatTarget.GetGame(), RefToWeakRef(combatTarget))) {
      return Cast(false);
    };
    return Cast(true);
  }
}
