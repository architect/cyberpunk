
public abstract class UpperBodyTransition extends DefaultTransition {

  protected final const Bool EmptyHandsCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsInSafeSceneTier(scriptInterface) && GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.AimForced) == true || GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.SafeForced) == true) {
      return false;
    };
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UseUnarmed)) {
      return true;
    };
    if(HasRightWeaponEquipped(scriptInterface) && GetParameterBool("requestWeaponUnequip", stateContext, false)) {
      return true;
    };
    return false;
  }

  protected final const ref<TransactionSystem> GetTransactionSystem(ref<StateGameScriptInterface> scriptInterface) {
    ref<TransactionSystem> transactionSystem;
    transactionSystem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    if(!ToBool(transactionSystem)) {
      return null;
    };
    return transactionSystem;
  }

  public final static Bool HasLeftWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    if(ToBool(Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponLeft")))) {
      return true;
    };
    return false;
  }

  public final static Bool HasAnyWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    if(HasLeftWeaponEquipped(scriptInterface) || HasRightWeaponEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }

  public final static Bool HasMeleeWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      if(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).HasTag(GetOwnerGameObject(scriptInterface), GetMeleeWeaponTag(), weapon.GetItemID())) {
        return true;
      };
    };
    return false;
  }

  public final static Bool HasRangedWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      if(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).HasTag(GetOwnerGameObject(scriptInterface), GetRangedWeaponTag(), weapon.GetItemID())) {
        return true;
      };
    };
    return false;
  }

  public final static Bool HasMeleewareEquipped(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      if(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).HasTag(GetOwnerGameObject(scriptInterface), "Meleeware", weapon.GetItemID())) {
        return true;
      };
    };
    return false;
  }

  protected final Bool IsItemMeleeware(ItemID item) {
    array<CName> itemTags;
    itemTags = GetItemRecord(GetTDBID(item)).Tags();
    return Contains(itemTags, "Meleeware");
  }

  public final void PlayEffectOnHeldItems(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    ref<ItemObject> leftItem;
    ref<ItemObject> rightItem;
    ref<entSpawnEffectEvent> spawnEffectEvent;
    leftItem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponLeft");
    rightItem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight");
    spawnEffectEvent = new entSpawnEffectEvent();
    spawnEffectEvent.effectName = effectName;
    if(ToBool(leftItem)) {
      leftItem.QueueEventToChildItems(spawnEffectEvent);
    };
    if(ToBool(rightItem)) {
      rightItem.QueueEventToChildItems(spawnEffectEvent);
    };
  }

  public final void StopEffectOnHeldItems(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    ref<ItemObject> leftItem;
    ref<ItemObject> rightItem;
    ref<entKillEffectEvent> killEffectEvent;
    leftItem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponLeft");
    rightItem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight");
    killEffectEvent = new entKillEffectEvent();
    killEffectEvent.effectName = effectName;
    if(ToBool(leftItem)) {
      leftItem.QueueEventToChildItems(killEffectEvent);
    };
    if(ToBool(rightItem)) {
      rightItem.QueueEventToChildItems(killEffectEvent);
    };
  }

  public final void BreakEffectLoopOnHeldItems(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    ref<ItemObject> leftItem;
    ref<ItemObject> rightItem;
    ref<entBreakEffectLoopEvent> BreakEffectLoopEvent;
    leftItem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponLeft");
    rightItem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight");
    BreakEffectLoopEvent = new entBreakEffectLoopEvent();
    BreakEffectLoopEvent.effectName = effectName;
    if(ToBool(leftItem)) {
      leftItem.QueueEventToChildItems(BreakEffectLoopEvent);
    };
    if(ToBool(rightItem)) {
      rightItem.QueueEventToChildItems(BreakEffectLoopEvent);
    };
  }

  public final const Bool ShouldAim(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Int32 sceneTier;
    if(IsAimForced(scriptInterface)) {
      return true;
    };
    blackboard = GetBlackboard(scriptInterface);
    sceneTier = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.SceneTier);
    if(sceneTier < 3 && stateContext.IsStateMachineActive("Vehicle") && blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle) != ToInt(gamePSMVehicle.Combat)) {
      return false;
    };
    if(sceneTier > 3) {
      return false;
    };
    if(scriptInterface.GetActionValue("CameraAim") > 0) {
      return true;
    };
    if(IsInSafeSceneTier(scriptInterface) && GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.AimForced) == true) {
      return true;
    };
    return false;
  }

  protected final void SendDOFData(ref<StateGameScriptInterface> scriptInterface, String dofSetting) {
    ref<AnimFeature_DOFControl> dofAnimFeature;
    String prefix;
    dofAnimFeature = new AnimFeature_DOFControl();
    prefix = "player." + dofSetting + ".";
    dofAnimFeature.dofIntensity = GetFloat(Create(prefix + "intensity"), 0);
    dofAnimFeature.dofNearBlur = GetFloat(Create(prefix + "nearBlur"), -1);
    dofAnimFeature.dofNearFocus = GetFloat(Create(prefix + "nearFocus"), -1);
    dofAnimFeature.dofFarBlur = GetFloat(Create(prefix + "farBlur"), -1);
    dofAnimFeature.dofFarFocus = GetFloat(Create(prefix + "farFocus"), -1);
    dofAnimFeature.dofBlendInTime = GetFloat(Create(prefix + "dofBlendInTime"), -1);
    dofAnimFeature.dofBlendOutTime = GetFloat(Create(prefix + "dofBlendOutTime"), -1);
    scriptInterface.SetAnimationParameterFeature("DOFControl", dofAnimFeature);
  }

  protected final void SetWeaponHolster(ref<StateGameScriptInterface> scriptInterface, Bool newState) {
    ref<AnimFeature_PlayerCoverActionWeaponHolster> weaponHolsterAnimFeature;
    weaponHolsterAnimFeature = new AnimFeature_PlayerCoverActionWeaponHolster();
    weaponHolsterAnimFeature.isWeaponHolstered = newState;
    scriptInterface.SetAnimationParameterFeature("PlayerCoverWeaponHolstered", weaponHolsterAnimFeature);
  }

  protected final Bool ProcessWeaponSlotInput(ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustReleased("WeaponSlot1")) {
      SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestWeaponSlot1);
    } else {
      if(scriptInterface.IsActionJustReleased("WeaponSlot2")) {
        SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestWeaponSlot2);
      } else {
        if(scriptInterface.IsActionJustReleased("WeaponSlot3")) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestWeaponSlot3);
        } else {
          if(scriptInterface.IsActionJustReleased("WeaponSlot4")) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestWeaponSlot4);
          };
        };
      };
    };
    return false;
  }

  protected final Bool CheckRangedAttackInput(ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustPressed("RangedAttack") && !IsInteractingWithTerminal(scriptInterface);
  }

  protected final Bool CheckMeleeStatesForCombatGadget(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    Bool inQuickmelee;
    inQuickmelee = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.QuickMelee);
    if(HasMeleeWeaponEquipped(scriptInterface)) {
      return !GetParameterBool("isAttacking", stateContext, true);
    };
    return !inQuickmelee;
  }
}

public abstract class UpperBodyEventsTransition extends UpperBodyTransition {

  public Bool m_switchButtonPushed;

  public Bool m_cyclePushed;

  public Float m_delay;

  public Float m_cycleBlock;

  public Bool m_switchPending;

  public Int32 m_counter;

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool switchItemButtonPushed;
    StateResultBool holsterItemButtonPushed;
    StateResultBool switchItemPending;
    StateResultFloat switchItemDelay;
    StateResultInt counter;
    StateResultFloat cycleBlock;
    StateResultBool cyclePushed;
    switchItemButtonPushed = stateContext.GetPermanentBoolParameter("switchItemButtonPushed");
    holsterItemButtonPushed = stateContext.GetPermanentBoolParameter("holsterItemButtonPushed");
    switchItemPending = stateContext.GetPermanentBoolParameter("switchItemPending");
    switchItemDelay = stateContext.GetPermanentFloatParameter("switchItemDelay");
    counter = stateContext.GetPermanentIntParameter("switchCounter");
    cycleBlock = stateContext.GetPermanentFloatParameter("switchCycleBlock");
    cyclePushed = stateContext.GetPermanentBoolParameter("cyclePushed");
    if(switchItemButtonPushed.valid) {
      this.m_switchButtonPushed = switchItemButtonPushed.value;
      this.m_switchPending = switchItemPending.value;
      this.m_delay = switchItemDelay.value;
      this.m_counter = counter.value;
      this.m_cycleBlock = cycleBlock.value;
      this.m_cyclePushed = cyclePushed.value;
    } else {
      ResetEquipVars(stateContext);
    };
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SyncEquipVarsToPermanentStorage(stateContext);
  }

  protected void ResetEquipVars(ref<StateContext> stateContext) {
    this.m_switchButtonPushed = false;
    this.m_switchPending = false;
    this.m_cyclePushed = false;
    this.m_delay = 0;
    this.m_counter = 0;
    this.m_cycleBlock = 0;
    SyncEquipVarsToPermanentStorage(stateContext);
  }

  protected void SyncEquipVarsToPermanentStorage(ref<StateContext> stateContext) {
    stateContext.SetPermanentBoolParameter("switchItemButtonPushed", this.m_switchButtonPushed, true);
    stateContext.SetPermanentBoolParameter("switchItemPending", this.m_switchPending, true);
    stateContext.SetPermanentFloatParameter("switchItemDelay", this.m_delay, true);
    stateContext.SetPermanentIntParameter("switchCounter", this.m_counter, true);
    stateContext.SetPermanentFloatParameter("switchCycleBlock", this.m_cycleBlock, true);
    stateContext.SetPermanentBoolParameter("cyclePushed", this.m_cyclePushed, true);
  }

  protected final Bool UpdateSwitchItem(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<UIInGameNotificationEvent> notificationEvent;
    Float holsterDelay;
    holsterDelay = 0.25;
    notificationEvent = new UIInGameNotificationEvent();
    notificationEvent.m_notificationType = UIInGameNotificationType.ActionRestriction;
    if(HasRestriction(scriptInterface.executionOwner, "FirearmsNoSwitch") || HasRestriction(scriptInterface.executionOwner, "ShootingRangeCompetition") || HasRestriction(scriptInterface.executionOwner, "Fists") || stateContext.IsStateMachineActive("Consumable") || CheckEquipmentStateMachineState(stateContext, EEquipmentSide.Right, EEquipmentState.Equipping)) {
      return false;
    };
    if(this.m_cyclePushed) {
      this.m_cycleBlock += timeDelta;
      if(this.m_cycleBlock > 0.25) {
        this.m_cycleBlock = 0;
        this.m_cyclePushed = false;
      };
    };
    if(scriptInterface.IsActionJustPressed("NextWeapon") && !this.m_cyclePushed) {
      SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.CycleNextWeaponWheelItem);
      this.m_cyclePushed = true;
      return true;
    };
    if(scriptInterface.IsActionJustPressed("PreviousWeapon") && !this.m_cyclePushed) {
      SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.CyclePreviousWeaponWheelItem);
      this.m_cyclePushed = true;
      return true;
    };
    if(scriptInterface.IsActionJustTapped("SwitchItem")) {
      this.m_switchButtonPushed = true;
      this.m_counter += 1;
    };
    if(this.m_switchButtonPushed) {
      this.m_delay += timeDelta;
      if(this.m_delay < holsterDelay && this.m_switchButtonPushed && !this.m_switchPending) {
        if(GetData(WeakRefToRef(scriptInterface.executionOwner)).CycleWeapon(true, true) != undefined()) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipWeapon);
          this.m_switchPending = true;
        };
        return false;
      };
      if(this.m_delay >= holsterDelay) {
        if(this.m_counter == 1) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.CycleNextWeaponWheelItem);
        } else {
          if(this.m_counter > 1 && GetData(WeakRefToRef(scriptInterface.executionOwner)).CycleWeapon(true, true) == undefined() && !HasRestriction(scriptInterface.executionOwner, "FirearmsNoUnequip")) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipWeapon);
          };
        };
        ResetEquipVars(stateContext);
      };
      return true;
    };
    return false;
  }
}

public class ForceEmptyHandsDecisions extends UpperBodyTransition {

  public const Bool stateBodyDone;

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!scriptInterface.CanEquipItem(stateContext)) {
      return false;
    };
    if(IsEmptyHandsForced(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }

  protected final const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsEmptyHandsForced(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsEmptyHandsForced(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }
}

public class ForceEmptyHandsEvents extends UpperBodyEventsTransition {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<PSMStopStateMachine> upperBody;
    upperBody = new PSMStopStateMachine();
    ResetEquipVars(stateContext);
    SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipAll);
    if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Vehicle) == ToInt(gamePSMVehicle.Driving)) {
      upperBody = new PSMStopStateMachine();
      upperBody.stateMachineIdentifier.definitionName = "UpperBody";
      WeakRefToRef(scriptInterface.executionOwner).QueueEvent(upperBody);
    };
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<DPADActionPerformed> dpadAction;
    ref<UIInGameNotificationEvent> notificationEvent;
    if(scriptInterface.IsActionJustReleased("UseConsumable")) {
      dpadAction = new DPADActionPerformed();
      dpadAction.action = EHotkey.DPAD_UP;
      if(!stateContext.IsStateMachineActive("Consumable") && CheckActiveConsumable(scriptInterface) && !AreChoiceHubsActive(scriptInterface) && CheckConsumableLootDataCondition(scriptInterface) && !IsInFocusMode(scriptInterface)) {
        if(!IsUsingConsumableRestricted(scriptInterface)) {
          dpadAction.successful = true;
          GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestConsumable);
        } else {
          notificationEvent = new UIInGameNotificationEvent();
          notificationEvent.m_notificationType = UIInGameNotificationType.ActionRestriction;
          GetUISystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).QueueEvent(notificationEvent);
        };
      } else {
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
      };
    };
  }
}

public class ForceSafeDecisions extends UpperBodyTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsSafeStateForced(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsSafeStateForced(stateContext, scriptInterface) || HasRightWeaponEquipped(scriptInterface) && GetInStateTime(stateContext, scriptInterface) >= 0.44999998807907104 && EmptyHandsCondition(stateContext, scriptInterface)) {
      return true;
    };
    if(!IsMultiplayer() && HasRightWeaponEquipped(scriptInterface) && GetStaticFloatParameter("timeToAutoUnequipWeapon", -1) > 0 && GetConditionParameterFloat("ForceSafeCurrentTimeToAutoUnequip", stateContext) >= GetStaticFloatParameter("timeToAutoUnequipWeapon", -1) + GetConditionParameterFloat("ForceSafeTimeStampToAutoUnequip", stateContext)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsSafeStateForced(stateContext, scriptInterface) && HasRightWeaponEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }
}

public class ForceSafeEvents extends UpperBodyEventsTransition {

  public ref<AnimFeature_SafeAction> m_safeAnimFeature;

  public TweakDBID m_weaponObjectID;

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetPermanentBoolParameter("WeaponInSafe", true, true);
    scriptInterface.SetAnimationParameterFloat("safe", 1);
    this.m_safeAnimFeature = new AnimFeature_SafeAction();
    this.m_weaponObjectID = GetWeaponItemRecord(GetTDBID(GetActiveWeapon(scriptInterface).GetItemID())).GetID();
    stateContext.SetConditionFloatParameter("ForceSafeTimeStampToAutoUnequip", GetInStateTime(stateContext, scriptInterface), true);
    stateContext.SetConditionFloatParameter("ForceSafeCurrentTimeToAutoUnequip", 0, true);
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(HasRightWeaponEquipped(scriptInterface)) {
      if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.HighLevel) == ToInt(gamePSMHighLevel.SceneTier2)) {
        UpdateSwitchItem(timeDelta, stateContext, scriptInterface);
      };
      if(scriptInterface.IsActionJustPressed("RangedAttack") && !IsMagazineEmpty(GetActiveWeapon(scriptInterface))) {
        scriptInterface.PushAnimationEvent("SafeAction");
      } else {
        if(scriptInterface.GetActionValue("RangedAttack") > 0.5) {
          stateContext.SetPermanentBoolParameter("TriggerHeld", true, true);
          this.m_safeAnimFeature.triggerHeld = true;
        } else {
          if(scriptInterface.GetActionValue("RangedAttack") < 0.5) {
            stateContext.SetPermanentBoolParameter("TriggerHeld", false, true);
            this.m_safeAnimFeature.triggerHeld = false;
          } else {
            if(scriptInterface.IsActionJustReleased("RangedAttack")) {
              stateContext.SetConditionFloatParameter("ForceSafeTimeStampToAutoUnequip", GetConditionParameterFloat("ForceSafeTimeStampToAutoUnequip", stateContext) + GetStaticFloatParameter("addedTimeToAutoUnequipAfterSafeAction", 0), true);
            };
          };
        };
      };
      stateContext.SetConditionFloatParameter("ForceSafeCurrentTimeToAutoUnequip", GetConditionParameterFloat("ForceSafeCurrentTimeToAutoUnequip", stateContext) + timeDelta, true);
      this.m_safeAnimFeature.safeActionDuration = GetFloat(this.m_weaponObjectID + ".safeActionDuration");
      scriptInterface.SetAnimationParameterFeature("SafeAction", this.m_safeAnimFeature);
      scriptInterface.SetAnimationParameterFeature("SafeAction", this.m_safeAnimFeature, GetActiveWeapon(scriptInterface));
    } else {
      if(!HasRightWeaponEquipped(scriptInterface)) {
        if(scriptInterface.IsActionJustReleased("SwitchItem") || CheckRangedAttackInput(scriptInterface)) {
          if(HasRestriction(scriptInterface.executionOwner, "OneHandedFirearms")) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableOneHandedRangedWeapon);
          } else {
            if(HasRestriction(scriptInterface.executionOwner, "Melee")) {
              SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableMeleeWeapon);
            } else {
              if(HasRestriction(scriptInterface.executionOwner, "Fists")) {
                SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestFists);
              } else {
                if(HasRestriction(scriptInterface.executionOwner, "Firearms")) {
                  SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableRangedWeapon);
                } else {
                  SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon);
                };
              };
            };
          };
        };
      };
    };
  }
}

public class EmptyHandsDecisions extends UpperBodyTransition {

  public const Bool stateBodyDone;

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!scriptInterface.CanEquipItem(stateContext)) {
      return false;
    };
    if(!HasRightWeaponEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }

  protected final const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasRightWeaponEquipped(scriptInterface);
  }
}

public class EmptyHandsEvents extends UpperBodyEventsTransition {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    scriptInterface.ActivateCameraSetting("weapon");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody, ToInt(gamePSMUpperBodyStates.Default));
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    SetWeaponHolster(scriptInterface, true);
    UpdateAimAssist(stateContext, scriptInterface);
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<DPADActionPerformed> dpadAction;
    if(IsInTakedownState(stateContext)) {
      return ;
    };
    if(!CheckGenericEquipItemConditions(stateContext, scriptInterface) || HasRestriction(scriptInterface.executionOwner, "FirearmsNoUnequipNoSwitch") || HasRestriction(scriptInterface.executionOwner, "ShootingRangeCompetition")) {
      return ;
    };
    ProcessCombatGadgetActionInputCaching(scriptInterface, stateContext);
    if(scriptInterface.IsActionJustReleased("SwitchItem") || CheckRangedAttackInput(scriptInterface) || scriptInterface.IsActionJustPressed("NextWeapon") || scriptInterface.IsActionJustPressed("PreviousWeapon") && !stateContext.IsStateMachineActive("CombatGadget") && !stateContext.IsStateMachineActive("Consumable") && !stateContext.IsStateMachineActive("LeftHandCyberware") && !IsCarryingBody(scriptInterface) || IsCarryingBody(scriptInterface) && !HasRestriction(scriptInterface.executionOwner, "NoCombat")) {
      if(HasRestriction(scriptInterface.executionOwner, "OneHandedFirearms")) {
        SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableOneHandedRangedWeapon);
      } else {
        if(HasRestriction(scriptInterface.executionOwner, "Melee")) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableMeleeWeapon);
        } else {
          if(HasRestriction(scriptInterface.executionOwner, "Fists")) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestFists);
          } else {
            if(HasRestriction(scriptInterface.executionOwner, "Firearms")) {
              SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableRangedWeapon);
            } else {
              SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon);
            };
          };
        };
      };
    } else {
      if(!stateContext.IsStateMachineActive("CombatGadget") && !IsCarryingBody(scriptInterface) && ProcessWeaponSlotInput(scriptInterface)) {
        return ;
      };
      if(scriptInterface.IsActionJustReleased("UseConsumable")) {
        dpadAction = new DPADActionPerformed();
        dpadAction.action = EHotkey.DPAD_UP;
        if(!stateContext.IsStateMachineActive("Consumable") && !stateContext.IsStateMachineActive("CombatGadget") && !stateContext.IsStateMachineActive("LeftHandCyberware") && CheckActiveConsumable(scriptInterface) && !IsInUpperBodyState(stateContext, "temporaryUnequip") && !IsInUpperBodyState(stateContext, "forceEmptyHands") && !AreChoiceHubsActive(scriptInterface) && CheckConsumableLootDataCondition(scriptInterface) && !IsInFocusMode(scriptInterface) && !IsCarryingBody(scriptInterface) && !IsUsingConsumableRestricted(scriptInterface)) {
          dpadAction.successful = true;
          GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestConsumable);
        } else {
          GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
        };
      } else {
        if(scriptInterface.IsActionJustPressed("UseCombatGadget") || GetParameterBool("cgCached", stateContext, true)) {
          dpadAction = new DPADActionPerformed();
          dpadAction.action = EHotkey.RB;
          if(IsUsingLeftHandAllowed(scriptInterface) && !stateContext.IsStateMachineActive("Consumable") && !stateContext.IsStateMachineActive("CombatGadget") && !IsInUpperBodyState(stateContext, "forceEmptyHands") && !AreChoiceHubsActive(scriptInterface) && !IsInSafeZone(scriptInterface) && GetInStateTime(stateContext, scriptInterface) > 0.30000001192092896 && !IsCarryingBody(scriptInterface)) {
            dpadAction.successful = true;
            GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
            if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Gadget)) {
              SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestGadget);
            } else {
              if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Cyberware)) {
                SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLeftHandCyberware);
                stateContext.RemovePermanentBoolParameter("cgCached");
              };
            };
          } else {
            GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
          };
        } else {
          UpdateSwitchItem(timeDelta, stateContext, scriptInterface);
        };
      };
    };
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }
}

public class SingleWieldDecisions extends UpperBodyTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(HasRightWeaponEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StimBroadcasterComponent> broadcaster;
    if(!HasRightWeaponEquipped(scriptInterface) || GetInStateTime(stateContext, scriptInterface) >= 0.44999998807907104 && EmptyHandsCondition(stateContext, scriptInterface)) {
      broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
      if(ToBool(broadcaster)) {
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.WeaponHolstered);
      };
      return true;
    };
    if(HasRightWeaponEquipped(scriptInterface) && GetParameterBool("requestWeaponUnequip", stateContext, false)) {
      broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
      if(ToBool(broadcaster)) {
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.WeaponHolstered);
      };
      return true;
    };
    return false;
  }
}

public class SingleWieldEvents extends UpperBodyEventsTransition {

  public Bool m_hasInstantEquipHackBeenApplied;

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    this.m_hasInstantEquipHackBeenApplied = false;
    if(WeakRefToRef(scriptInterface.executionOwner).IsControlledByAnotherClient()) {
      InstantEquipHACK(stateContext, scriptInterface);
    };
    SetWeaponHolster(scriptInterface, false);
    UpdateAimAssist(stateContext, scriptInterface);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isCPOControlScheme;
    StateMachineIdentifier rhIden;
    ref<DPADActionPerformed> dpadAction;
    Bool isPerformingMeleeAttack;
    if(WeakRefToRef(scriptInterface.executionOwner).IsControlledByAnotherClient() && !this.m_hasInstantEquipHackBeenApplied) {
      InstantEquipHACK(stateContext, scriptInterface);
    };
    if(IsInTakedownState(stateContext) || GetSceneTier(scriptInterface) > 2) {
      return ;
    };
    if(GetConditionParameterBool("AimingInterrupted", stateContext) && scriptInterface.GetActionValue("CameraAim") < 0.5) {
      stateContext.SetConditionBoolParameter("AimingInterrupted", false, true);
    };
    rhIden.definitionName = "Equipment";
    rhIden.referenceName = "RightHand";
    if(!GetParameterBool("isAttacking", stateContext, true) && stateContext.IsStateActiveWithIdentifier(rhIden, "unequipCycle")) {
      UpdateMeleeInputBuffer(stateContext, scriptInterface, true);
    };
    if(!CheckGenericEquipItemConditions(stateContext, scriptInterface)) {
      return ;
    };
    if(scriptInterface.IsActionJustReleased("UseConsumable")) {
      dpadAction = new DPADActionPerformed();
      dpadAction.action = EHotkey.DPAD_UP;
      if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee) == ToInt(gamePSMMelee.Attack)) {
        isPerformingMeleeAttack = true;
      };
      if(!stateContext.IsStateMachineActive("Consumable") && !stateContext.IsStateMachineActive("CombatGadget") && !stateContext.IsStateMachineActive("LeftHandCyberware") && CheckActiveConsumable(scriptInterface) && CheckEquipmentStateMachineState(stateContext, EEquipmentSide.Right, EEquipmentState.Equipped) && !IsInUpperBodyState(stateContext, "temporaryUnequip") && !IsInUpperBodyState(stateContext, "forceEmptyHands") && !AreChoiceHubsActive(scriptInterface) && CheckConsumableLootDataCondition(scriptInterface) && !IsInFocusMode(scriptInterface) && !IsUsingConsumableRestricted(scriptInterface) && !isPerformingMeleeAttack) {
        dpadAction.successful = true;
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
        SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestConsumable);
      } else {
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
      };
    };
    ProcessCombatGadgetActionInputCaching(scriptInterface, stateContext);
    if(scriptInterface.IsActionJustPressed("UseCombatGadget") || GetParameterBool("cgCached", stateContext, true)) {
      dpadAction = new DPADActionPerformed();
      dpadAction.action = EHotkey.RB;
      if(IsUsingLeftHandAllowed(scriptInterface) && !stateContext.IsStateMachineActive("Consumable") && !IsInUpperBodyState(stateContext, "temporaryUnequip") && !IsInUpperBodyState(stateContext, "forceEmptyHands") && CheckEquipmentStateMachineState(stateContext, EEquipmentSide.Right, EEquipmentState.Equipped) && CheckMeleeStatesForCombatGadget(scriptInterface, stateContext) && !AreChoiceHubsActive(scriptInterface) && !stateContext.IsStateMachineActive("CombatGadget") && GetInStateTime(stateContext, scriptInterface) > 0.30000001192092896 && !IsInSafeZone(scriptInterface)) {
        dpadAction.successful = true;
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
        if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Gadget)) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestGadget);
        } else {
          if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Cyberware)) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLeftHandCyberware);
            stateContext.RemovePermanentBoolParameter("cgCached");
          };
        };
      } else {
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
      };
    };
    isCPOControlScheme = GetRuntimeInfo(WeakRefToRef(scriptInterface.executionOwner).GetGame()).IsMultiplayer() || GetPlayerSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).IsCPOControlSchemeForced();
    if(isCPOControlScheme && scriptInterface.IsActionJustReleased("SwitchItem") || scriptInterface.GetActionValue("SwitchItemMW") != 0) {
      SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.CycleWeaponWheelItem);
    } else {
      if(!isCPOControlScheme && UpdateSwitchItem(timeDelta, stateContext, scriptInterface)) {
        return ;
      };
      if(!HasRestriction(scriptInterface.executionOwner, "FirearmsNoUnequipNoSwitch") && !HasRestriction(scriptInterface.executionOwner, "FirearmsNoSwitch") && !HasRestriction(scriptInterface.executionOwner, "ShootingRangeCompetition") && ProcessWeaponSlotInput(scriptInterface)) {
        return ;
      };
    };
  }

  public final void InstantEquipHACK(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_EquipUnequipItem> rightHandItemHandling;
    ItemID weaponID;
    ref<WeaponItem_Record> weaponRecData;
    ref<WeaponObject> weapon;
    weapon = GetActiveWeapon(scriptInterface);
    if(ToBool(weapon)) {
      weaponID = weapon.GetItemID();
      weaponRecData = GetWeaponItemRecord(GetTDBID(weaponID));
      rightHandItemHandling = new AnimFeature_EquipUnequipItem();
      rightHandItemHandling.itemState = 2;
      rightHandItemHandling.stateTransitionDuration = 0;
      rightHandItemHandling.itemType = WeakRefToRef(weaponRecData.ItemType()).AnimFeatureIndex();
      scriptInterface.SetAnimationParameterFeature("rightHandItemHandling", rightHandItemHandling);
      this.m_hasInstantEquipHackBeenApplied = true;
    };
  }
}

public class AimingStateDecisions extends UpperBodyTransition {

  [Default(AimingStateDecisions, 100000.f))]
  private Float m_mouseZoomLevel;

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Int32 combatGadgetState;
    Int32 highLevelState;
    Int32 takedownState;
    Bool inCover;
    Bool pendingAdjust;
    Bool isCombatGadgetActive;
    Bool isAimingInScenePermitted;
    Bool isInTakedown;
    Bool isDead;
    Bool isReloading;
    Bool isInQuickMelee;
    Bool isOverheated;
    Bool isChangingEquipment;
    Bool isDodging;
    Bool isConsumableActive;
    isDodging = IsInLocomotionState(stateContext, "dodge") || IsInLocomotionState(stateContext, "dodgeAir");
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanAimWhileDodging) && isDodging) {
      return false;
    };
    if(HasMeleeWeaponEquipped(scriptInterface) && GetWeaponItemTag(stateContext, scriptInterface, "Throwable", GetActiveWeapon(scriptInterface).GetItemID()) && !HasStatFlag(scriptInterface, gamedataStatType.CanThrowWeapon)) {
      return false;
    };
    isChangingEquipment = IsRightHandInUnequippingState(stateContext) || IsLeftHandInUnequippingState(stateContext);
    if(isChangingEquipment) {
      return false;
    };
    if(IsPlayerInBraindance(scriptInterface)) {
      return false;
    };
    inCover = stateContext.GetStateMachineCurrentState("CoverAction") != "notInCover";
    if(inCover) {
      pendingAdjust = ToBool(stateContext.GetTemporaryScriptableParameter("adjustTransform"));
    };
    if(GetConditionParameterBool("AimingInterrupted", stateContext) || IsAimingSoftBlocked(stateContext, scriptInterface)) {
      return false;
    };
    if(IsAimingBlockedForTime(stateContext, scriptInterface)) {
      return false;
    };
    blackboard = GetBlackboard(scriptInterface);
    highLevelState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel);
    isAimingInScenePermitted = highLevelState == ToInt(gamePSMHighLevel.SceneTier1) || highLevelState == ToInt(gamePSMHighLevel.SceneTier2) || highLevelState == ToInt(gamePSMHighLevel.SceneTier3);
    if(!isAimingInScenePermitted) {
      return false;
    };
    combatGadgetState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.CombatGadget);
    isCombatGadgetActive = combatGadgetState > ToInt(gamePSMCombatGadget.Default) && combatGadgetState < ToInt(gamePSMCombatGadget.WaitForUnequip) && !IsInVisionModeActiveState(stateContext, scriptInterface);
    if(isCombatGadgetActive) {
      return false;
    };
    if(isOverheated) {
      return false;
    };
    isDead = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vitals) == ToInt(gamePSMVitals.Dead);
    if(isDead) {
      return false;
    };
    takedownState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown);
    isInTakedown = takedownState == ToInt(gamePSMTakedown.Grapple) || takedownState == ToInt(gamePSMTakedown.Leap) || takedownState == ToInt(gamePSMTakedown.Takedown);
    if(isInTakedown) {
      return false;
    };
    if(IsWeaponBlockingAiming(scriptInterface)) {
      return false;
    };
    isConsumableActive = stateContext.IsStateMachineActive("Consumable");
    if(isConsumableActive) {
      return false;
    };
    if(IsInItemWheelState(stateContext)) {
      return false;
    };
    return ShouldAim(stateContext, scriptInterface) && !inCover || !pendingAdjust;
  }

  protected final const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsAimingBlockedForTime(stateContext, scriptInterface)) {
      return true;
    };
    if(IsAimingHeldForTime(stateContext, scriptInterface)) {
      return false;
    };
    if(IsWeaponBlockingAiming(scriptInterface)) {
      return true;
    };
    if(!HasRightWeaponEquipped(scriptInterface) || !IsRightHandInEquippedState(stateContext)) {
      return false;
    };
    if(GetParameterBool("InterruptAiming", stateContext, false)) {
      stateContext.SetConditionBoolParameter("AimingInterrupted", true, true);
      return true;
    };
    if(stateContext.IsStateMachineActive("Consumable")) {
      return true;
    };
    if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.QuickMelee)) {
      return true;
    };
    if(!ShouldAim(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !ShouldAim(stateContext, scriptInterface) && !HasRightWeaponEquipped(scriptInterface);
  }

  protected final const Bool ToForceEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(HasRightWeaponEquipped(scriptInterface) && IsEmptyHandsForced(stateContext, scriptInterface)) {
      return true;
    };
    return !ShouldAim(stateContext, scriptInterface) && IsEmptyHandsForced(stateContext, scriptInterface);
  }

  protected final const Bool ToForceSafe(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !ShouldAim(stateContext, scriptInterface) && IsSafeStateForced(stateContext, scriptInterface);
  }
}

public class AimingStateEvents extends UpperBodyEventsTransition {

  private ref<AnimFeature_AimPlayer> m_aim;

  [Default(AimingStateEvents, 100000.f))]
  private Float m_mouseZoomLevel;

  private Int32 m_zoomLevelNum;

  private Int32 m_numZoomLevels;

  private Int32 m_delayAimSnap;

  private Bool m_isAiming;

  [Default(AimingStateEvents, 0.0f))]
  private Float m_aimInTimeRemaining;

  private Bool m_aimBroadcast;

  private Float m_zoomLevel;

  private Float m_finalZoomLevel;

  private Float m_previousZoomLevel;

  private Float m_currentZoomLevel;

  private Float timeToBlendZoom;

  private Float time;

  private Float m_speed;

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter("AimingInterrupted", false, true);
    scriptInterface.SetAnimationParameterBool("has_scope", GetActiveWeapon(scriptInterface).HasScope());
    UpdateAimDownSightsSfx(stateContext, scriptInterface);
    stateContext.SetTemporaryBoolParameter("InterruptSprint", true, true);
    stateContext.SetConditionBoolParameter("SprintToggled", false, true);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody, ToInt(gamePSMUpperBodyStates.Aim));
    PlayEffectOnHeldItems(scriptInterface, "lightswitch");
    OnAimStartBegin(stateContext, scriptInterface);
    this.m_numZoomLevels = GetStaticIntParameter("maxNumberOfZoomLevels", 1);
    UpdateAimAnimFeature(stateContext, scriptInterface);
    UpdateWeaponOffsetPosition(scriptInterface);
    UpdateAimAssist(stateContext, scriptInterface);
  }

  protected final void OnAimStartBegin(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    Bool usingCover;
    GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).OnAimStartBegin(scriptInterface.owner);
    weapon = GetWeaponObject(scriptInterface);
    if(ToBool(weapon)) {
      this.m_aimInTimeRemaining = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.AimInTime);
    } else {
      this.m_aimInTimeRemaining = 0;
      GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).OnAimStartEnd(scriptInterface.owner);
    };
    usingCover = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCoverDirection(WeakRefToRef(scriptInterface.executionOwner)) != gamePlayerCoverDirection.;
    if(usingCover) {
      this.m_delayAimSnap = 2;
    } else {
      this.m_delayAimSnap = 0;
      EvaluateAimSnap(stateContext, scriptInterface);
    };
    NotifyWeaponObject(scriptInterface, true);
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<DPADActionPerformed> dpadAction;
    ref<WeaponObject> weapon;
    ref<StimBroadcasterComponent> broadcaster;
    if(GetParameterBool("ReevaluateAiming", stateContext, false)) {
      GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).OnAimStop(scriptInterface.owner);
      OnAimStartBegin(stateContext, scriptInterface);
      return ;
    };
    if(this.m_aimInTimeRemaining > 0) {
      this.m_aimInTimeRemaining -= timeDelta;
      if(this.m_aimInTimeRemaining < 0) {
        GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).OnAimStartEnd(scriptInterface.owner);
      };
    };
    if(this.m_delayAimSnap > 0) {
      this.m_delayAimSnap -= 1;
      if(this.m_delayAimSnap == 0) {
        EvaluateAimSnap(stateContext, scriptInterface);
      };
    };
    UpdateAimAnimFeature(stateContext, scriptInterface);
    UpdateZoomSfx(stateContext, scriptInterface);
    UpdateZoomVfx(scriptInterface);
    if(!HasRightWeaponEquipped(scriptInterface) && scriptInterface.IsActionJustReleased("SwitchItem") || scriptInterface.IsActionJustPressed("RangedAttack")) {
      if(HasRestriction(scriptInterface.executionOwner, "OneHandedFirearms")) {
        SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableOneHandedRangedWeapon);
      } else {
        if(HasRestriction(scriptInterface.executionOwner, "Melee")) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableMeleeWeapon);
        } else {
          if(HasRestriction(scriptInterface.executionOwner, "Fists")) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestFists);
          } else {
            if(HasRestriction(scriptInterface.executionOwner, "Firearms")) {
              SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableRangedWeapon);
            } else {
              SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon);
            };
          };
        };
      };
    } else {
      if(HasRightWeaponEquipped(scriptInterface) && UpdateSwitchItem(timeDelta, stateContext, scriptInterface)) {
      };
    };
    ProcessCombatGadgetActionInputCaching(scriptInterface, stateContext);
    if(scriptInterface.IsActionJustPressed("UseCombatGadget") || GetParameterBool("cgCached", stateContext, true)) {
      dpadAction = new DPADActionPerformed();
      dpadAction.action = EHotkey.RB;
      if(IsUsingLeftHandAllowed(scriptInterface) && !stateContext.IsStateMachineActive("Consumable") && !IsInUpperBodyState(stateContext, "temporaryUnequip") && !IsInUpperBodyState(stateContext, "forceEmptyHands") && CheckMeleeStatesForCombatGadget(scriptInterface, stateContext) && CheckEquipmentStateMachineState(stateContext, EEquipmentSide.Right, EEquipmentState.Equipped) && !AreChoiceHubsActive(scriptInterface) && !IsInSafeZone(scriptInterface)) {
        dpadAction.successful = true;
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
        if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Gadget)) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestGadget);
        } else {
          if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Cyberware)) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLeftHandCyberware);
            stateContext.RemovePermanentBoolParameter("cgCached");
          };
        };
      } else {
        GetUISystem(WeakRefToRef(scriptInterface.owner).GetGame()).QueueEvent(dpadAction);
      };
    };
    if(this.m_aimInTimeRemaining < 0 && !this.m_aimBroadcast && GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) != ToInt(gamePSMRangedWeaponStates.Safe)) {
      weapon = GetWeaponObject(scriptInterface);
      if(!IsFists(weapon.GetItemID()) && IsRanged(weapon.GetItemID())) {
        this.m_aimBroadcast = true;
        broadcaster = WeakRefToRef(scriptInterface.owner).GetStimBroadcasterComponent();
        if(ToBool(broadcaster)) {
          broadcaster.AddActiveStimuli(scriptInterface.owner, gamedataStimType.CrowdIllegalAction, -1);
        };
      };
    };
  }

  protected final ref<WeaponObject> GetWeaponObject(ref<StateGameScriptInterface> scriptInterface) {
    return Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
  }

  protected final void EvaluateAimSnap(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    AimRequest perfectAimSnapParams;
    ItemID weaponID;
    ref<WeaponItem_Record> weaponRecData;
    ref<WeaponObject> weapon;
    Bool isInvalidMeleeWeapon;
    ref<UserSettings> userSettings;
    ref<ConfigGroup> settingGroup;
    ref<ConfigVarBool> aimSnapSetting;
    Bool aimSnapEnabledInSetting;
    aimSnapEnabledInSetting = true;
    weapon = GetWeaponObject(scriptInterface);
    weaponID = weapon.GetItemID();
    weaponRecData = GetWeaponItemRecord(GetTDBID(weaponID));
    isInvalidMeleeWeapon = IsMelee(weaponID) && WeakRefToRef(weaponRecData.ItemType()).Type() != gamedataItemType.Wea_Knife && WeakRefToRef(weaponRecData.ItemType()).Type() != gamedataItemType.Cyb_NanoWires;
    if(WeakRefToRef(weaponRecData.Evolution()).Type() != gamedataWeaponEvolution.Smart && !isInvalidMeleeWeapon) {
      if(IsRanged(weaponID)) {
        userSettings = GetSettingsSystem(weapon.GetGame());
        settingGroup = userSettings.GetGroup(Cast("/gameplay/difficulty"));
        aimSnapSetting = Cast(settingGroup.GetVar(Cast("AimSnap")));
        if(ToBool(aimSnapSetting)) {
          aimSnapEnabledInSetting = aimSnapSetting.GetValue();
        };
      };
      if(aimSnapEnabledInSetting) {
        if(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).HasTag(GetOwnerGameObject(scriptInterface), "PerfectAim", weapon.GetItemID())) {
          perfectAimSnapParams = GetPerfectAimSnapParams();
          GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).LookAt(scriptInterface.owner, perfectAimSnapParams);
        } else {
          GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).AimSnap(scriptInterface.owner);
        };
      };
    };
  }

  protected final AimRequest GetVehicleAimSnapParams() {
    AimRequest aimSnapParams;
    aimSnapParams.duration = 0.25;
    aimSnapParams.adjustPitch = true;
    aimSnapParams.adjustYaw = true;
    aimSnapParams.endOnTargetReached = false;
    aimSnapParams.endOnCameraInputApplied = true;
    aimSnapParams.endOnTimeExceeded = false;
    aimSnapParams.cameraInputMagToBreak = 0.5;
    aimSnapParams.precision = 0.10000000149011612;
    aimSnapParams.maxDuration = 0;
    aimSnapParams.easeIn = true;
    aimSnapParams.easeOut = true;
    aimSnapParams.checkRange = true;
    aimSnapParams.processAsInput = true;
    return aimSnapParams;
  }

  protected final AimRequest GetPerfectAimSnapParams() {
    AimRequest aimSnapParams;
    aimSnapParams.duration = 0.25;
    aimSnapParams.adjustPitch = true;
    aimSnapParams.adjustYaw = true;
    aimSnapParams.endOnAimingStopped = true;
    aimSnapParams.precision = 0.10000000149011612;
    aimSnapParams.easeIn = true;
    aimSnapParams.easeOut = true;
    aimSnapParams.checkRange = true;
    aimSnapParams.processAsInput = true;
    aimSnapParams.bodyPartsTracking = true;
    aimSnapParams.bptMaxDot = 0.5;
    aimSnapParams.bptMaxSwitches = -1;
    aimSnapParams.bptMinInputMag = 0.5;
    aimSnapParams.bptMinResetInputMag = 0.10000000149011612;
    return aimSnapParams;
  }

  protected final void UpdateAimAnimFeature(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    ref<StatsSystem> stats;
    StatsObjectID ownerID;
    ownerID = Cast(WeakRefToRef(scriptInterface.owner).GetEntityID());
    weapon = Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(!ToBool(this.m_aim)) {
      this.m_aim = new AnimFeature_AimPlayer();
    };
    if(GetParameterBool("WeaponInSafe", stateContext, true) && !GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.AimForced)) {
      this.m_aim.SetAimState(animAimState.Unaimed);
    } else {
      this.m_aim.SetAimState(animAimState.Aimed);
    };
    stats = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    this.m_aim.SetZoomState(animAimState.Aimed);
    this.m_aim.SetAimInTime(stats.GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.AimInTime));
    this.m_aim.SetAimOutTime(stats.GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.AimOutTime));
    scriptInterface.SetAnimationParameterFeature("AnimFeature_AimPlayer", this.m_aim);
    scriptInterface.SetAnimationParameterFeature("AnimFeature_AimPlayer", this.m_aim, weapon);
  }

  protected final void UpdateZoomSfx(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    UpdateAimDownSightsSfx(stateContext, scriptInterface);
  }

  protected final void UpdateAimDownSightsSfx(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("WeaponInSafe", stateContext, true)) {
      if(this.m_isAiming) {
        this.m_isAiming = false;
        ToggleAudioAimDownSights(GetActiveWeapon(scriptInterface), this.m_isAiming);
      };
    } else {
      if(!this.m_isAiming) {
        this.m_isAiming = true;
        ToggleAudioAimDownSights(GetActiveWeapon(scriptInterface), this.m_isAiming);
      };
    };
  }

  protected final void UpdateZoomVfx(ref<StateGameScriptInterface> scriptInterface) {
    if(this.m_finalZoomLevel >= 2) {
      StartZoomEffect(scriptInterface, "zoom");
    } else {
      BreakEffectLoop(scriptInterface, "zoom");
    };
  }

  protected final void StartZoomEffect(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    ref<worldEffectBlackboard> blackboard;
    Float maxZoom;
    Float normalizedZoom;
    maxZoom = GetStaticFloatParameter("noWeaponZoomLevel" + "" + this.m_numZoomLevels, 1);
    normalizedZoom = this.m_finalZoomLevel / maxZoom;
    blackboard = new worldEffectBlackboard();
    blackboard.SetValue("zoomValue", normalizedZoom);
    StartEffectEvent(WeakRefToRef(scriptInterface.owner), "zoom", false, blackboard);
  }

  protected final void UpdateWeaponOffsetPosition(ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_ProceduralIronsightData> posAnimFeature;
    ref<WeaponObject> weapon;
    TweakDBID weaponID;
    Vector3 position;
    ref<StatsSystem> stats;
    Vector3 addedPosition;
    stats = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weapon = GetActiveWeapon(scriptInterface);
    weaponID = GetTDBID(weapon.GetItemID());
    posAnimFeature = new AnimFeature_ProceduralIronsightData();
    posAnimFeature.isEnabled = GetBool(weaponID + ".IsIKEnabled");
    if(weapon.HasScope()) {
      posAnimFeature.position = weapon.GetScopeOffset();
    } else {
      posAnimFeature.position = weapon.GetIronSightOffset();
    };
    addedPosition = GetVector3(weaponID + ".ikOffset");
    posAnimFeature.position += Vector3To4(addedPosition);
    posAnimFeature.hasScope = weapon.HasScope();
    posAnimFeature.offset = stats.GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.AimOffset);
    posAnimFeature.scopeOffset = stats.GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.ScopeOffset);
    scriptInterface.SetAnimationParameterFeature("ProceduralIronsightData", posAnimFeature);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    ref<StimBroadcasterComponent> broadcaster;
    OnExit(stateContext, scriptInterface);
    weapon = GetWeaponObject(scriptInterface);
    this.m_aim.SetAimState(animAimState.Unaimed);
    this.m_aim.SetZoomState(animAimState.Unaimed);
    this.m_isAiming = false;
    scriptInterface.SetAnimationParameterFeature("AnimFeature_AimPlayer", this.m_aim);
    scriptInterface.SetAnimationParameterFeature("AnimFeature_AimPlayer", this.m_aim, weapon);
    if(!GetParameterBool("WeaponInSafe", stateContext, true)) {
      TriggerZoomExitSfx(scriptInterface);
    };
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody, ToInt(gamePSMUpperBodyStates.Default));
    GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).OnAimStop(scriptInterface.owner);
    BreakEffectLoopOnHeldItems(scriptInterface, "lightswitch");
    broadcaster = WeakRefToRef(scriptInterface.owner).GetStimBroadcasterComponent();
    if(ToBool(broadcaster)) {
      this.m_aimBroadcast = false;
      broadcaster.RemoveActiveStimuliByName(scriptInterface.owner, gamedataStimType.CrowdIllegalAction);
    };
    NotifyWeaponObject(scriptInterface, false);
  }

  protected final void TriggerZoomExitSfx(ref<StateGameScriptInterface> scriptInterface) {
    ToggleAudioAimDownSights(GetActiveWeapon(scriptInterface), this.m_isAiming);
  }

  protected final void NotifyWeaponObject(ref<StateGameScriptInterface> scriptInterface, Bool isAiming) {
    ref<WeaponObject> weapon;
    ref<gameweaponeventsOwnerAimEvent> evt;
    weapon = GetWeaponObject(scriptInterface);
    if(ToBool(weapon)) {
      evt = new gameweaponeventsOwnerAimEvent();
      evt.isAiming = isAiming;
      weapon.QueueEvent(evt);
    };
  }
}

public class TemporaryUnequipDecisions extends UpperBodyTransition {

  protected final const Bool IsTemporaryUnequipRequested(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isInWorkspot;
    Bool isInValidState;
    Bool isTemporaryUnequipForced;
    Bool isTemporaryUnequipWeaponForced;
    isTemporaryUnequipForced = GetParameterBool("forcedTemporaryUnequip", stateContext, true);
    isTemporaryUnequipWeaponForced = GetParameterBool("forceTempUnequipWeapon", stateContext, true);
    if(isTemporaryUnequipForced || isTemporaryUnequipWeaponForced) {
      return true;
    };
    isInWorkspot = IsInLocomotionState(stateContext, "workspot");
    if(isInWorkspot) {
      if(GetPlayerPuppet(scriptInterface).HasWorkspotTag("Grab")) {
        return true;
      };
    };
    isInValidState = GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice) || GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice) || IsInLocomotionState(stateContext, "climb") || IsInLocomotionState(stateContext, "ladder") || IsInLocomotionState(stateContext, "ladderSprint") || IsInLocomotionState(stateContext, "ladderSlide") || IsInHighLevelState(stateContext, "swimming") || IsInLocomotionState(stateContext, "veryHardLand");
    return isInValidState;
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsTemporaryUnequipRequested(stateContext, scriptInterface);
  }

  protected const Bool ToWaitForEquip(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsTemporaryUnequipRequested(stateContext, scriptInterface) || GetParameterBool("cgCached", stateContext, true) || scriptInterface.GetActionValue("UseCombatGadget") != 0 && !GetParameterBool("invalidTempUnequipThrow", stateContext, true) || !GetConditionParameterBool("TemporaryUnequipHasUnequippedWeapon", stateContext)) {
      if(!GetParameterBool("ChargeCancelled", stateContext, true)) {
        return false;
      };
    };
    return !ToBool(GetItemInRightHandSlot(scriptInterface));
  }

  protected final const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("cgCached", stateContext, true) || scriptInterface.GetActionValue("UseCombatGadget") != 0 && !GetParameterBool("invalidTempUnequipThrow", stateContext, true) || IsTemporaryUnequipRequested(stateContext, scriptInterface)) {
      if(!GetParameterBool("ChargeCancelled", stateContext, true)) {
        return false;
      };
    };
    return !GetConditionParameterBool("TemporaryUnequipHasUnequippedWeapon", stateContext);
  }

  protected final const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("cgCached", stateContext, true) || scriptInterface.GetActionValue("UseCombatGadget") != 0 && !GetParameterBool("invalidTempUnequipThrow", stateContext, true)) {
      if(!GetParameterBool("ChargeCancelled", stateContext, true)) {
        return false;
      };
    };
    return !IsTemporaryUnequipRequested(stateContext, scriptInterface) && ToBool(GetItemInRightHandSlot(scriptInterface));
  }
}

public class TemporaryUnequipEvents extends UpperBodyEventsTransition {

  private Bool m_forceOpen;

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isInstantUnequipRequired;
    gameEquipAnimationType equipAnimType;
    StateMachineIdentifier lhIden;
    StateMachineIdentifier rhIden;
    this.m_forceOpen = false;
    lhIden.definitionName = "Equipment";
    lhIden.referenceName = "LeftHand";
    rhIden.definitionName = "Equipment";
    rhIden.referenceName = "RightHand";
    ResetEquipVars(stateContext);
    if(!stateContext.IsStateActiveWithIdentifier(lhIden, "unequipped") || !stateContext.IsStateActiveWithIdentifier(rhIden, "unequipped")) {
      isInstantUnequipRequired = GetParameterBool("forcedTemporaryUnequip", stateContext, true) || IsInLocomotionState(stateContext, "climb") || IsInLocomotionState(stateContext, "ladder") || IsInHighLevelState(stateContext, "swimming");
      if(isInstantUnequipRequired) {
        equipAnimType = gameEquipAnimationType.Instant;
      } else {
        equipAnimType = gameEquipAnimationType.Default;
      };
      if(GetParameterBool("forceTempUnequipWeapon", stateContext, true)) {
        SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipWeapon, equipAnimType);
      } else {
        SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipAll, equipAnimType);
      };
      stateContext.SetConditionBoolParameter("TemporaryUnequipHasUnequippedWeapon", true, true);
    } else {
      stateContext.SetConditionBoolParameter("TemporaryUnequipHasUnequippedWeapon", false, true);
      stateContext.RemovePermanentBoolParameter("ChargeCancelled");
    };
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsForceOpeningDoor)) {
      if(!this.m_forceOpen) {
        ForceEquipStrongArms(Cast(WeakRefToRef(scriptInterface.executionOwner)));
      };
      stateContext.SetPermanentBoolParameter("invalidTempUnequipThrow", true, true);
      return ;
    };
    if(IsInTakedownState(stateContext) || GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice) || IsInLocomotionState(stateContext, "climb") || IsInLocomotionState(stateContext, "ladder") || IsInLocomotionState(stateContext, "ladderSprint") || IsInLocomotionState(stateContext, "ladderSlide") || IsInHighLevelState(stateContext, "swimming") || HasStatusEffect(scriptInterface.executionOwner, gamedataStatusEffectType.Knockdown) || IsInLocomotionState(stateContext, "veryHardLand") || IsInLocomotionState(stateContext, "knockdown") || IsInLocomotionState(stateContext, "vehicleKnockdown") || IsInLocomotionState(stateContext, "forcedKnockdown") || stateContext.IsStateMachineActive("LeftHandCyberware")) {
      stateContext.SetPermanentBoolParameter("invalidTempUnequipThrow", true, true);
      return ;
    };
    ProcessCombatGadgetActionInputCaching(scriptInterface, stateContext);
    if(GetCancelChargeButtonInput(scriptInterface) && GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.CombatGadget) == ToInt(gamePSMCombatGadget.Charging) && stateContext.IsStateMachineActive("CombatGadget") || stateContext.IsStateMachineActive("LeftHandCyberware")) {
      stateContext.SetPermanentBoolParameter("ChargeCancelled", true, true);
    } else {
      if(GetParameterBool("ChargeCancelled", stateContext, true) && !stateContext.IsStateMachineActive("CombatGadget") && !stateContext.IsStateMachineActive("LeftHandCyberware")) {
        stateContext.RemovePermanentBoolParameter("ChargeCancelled");
      };
    };
    if(scriptInterface.IsActionJustPressed("UseCombatGadget") || GetParameterBool("cgCached", stateContext, true) && CheckLeftHandForUnequippedState(stateContext)) {
      if(IsUsingLeftHandAllowed(scriptInterface) && !stateContext.IsStateMachineActive("Consumable") && !IsInUpperBodyState(stateContext, "forceEmptyHands") && !AreChoiceHubsActive(scriptInterface) && !IsInSafeZone(scriptInterface)) {
        if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Gadget)) {
          SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestGadget);
          stateContext.SetPermanentBoolParameter("forceTempUnequipWeapon", true, true);
          stateContext.SetPermanentBoolParameter("gadgetRequested", true, true);
        } else {
          if(CheckItemCategoryInQuickWheel(scriptInterface, gamedataItemCategory.Cyberware)) {
            SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.RequestLeftHandCyberware);
            stateContext.RemovePermanentBoolParameter("cgCached");
            stateContext.SetPermanentBoolParameter("gadgetRequested", true, true);
          };
        };
      };
    };
    if(GetParameterBool("gadgetRequested", stateContext, true) && scriptInterface.GetActionValue("UseCombatGadget") == 0 && !GetParameterBool("cgCached", stateContext, true) && !stateContext.IsStateMachineActive("CombatGadget") && !stateContext.IsStateMachineActive("LeftHandCyberware")) {
      stateContext.RemovePermanentBoolParameter("forceTempUnequipWeapon");
    };
  }

  protected final const Bool CheckLeftHandForUnequippedState(ref<StateContext> stateContext) {
    StateMachineIdentifier lhIden;
    lhIden.definitionName = "Equipment";
    lhIden.referenceName = "LeftHand";
    return stateContext.IsStateActiveWithIdentifier(lhIden, "unequipped");
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(this.m_forceOpen) {
      ForceUnequipStrongArms(Cast(WeakRefToRef(scriptInterface.executionOwner)));
    };
    stateContext.RemovePermanentBoolParameter("ChargeCancelled");
    stateContext.RemovePermanentBoolParameter("invalidTempUnequipThrow");
    stateContext.RemovePermanentBoolParameter("gadgetRequested");
  }

  protected final void ForceEquipStrongArms(ref<PlayerPuppet> player) {
    if(ForceEquipStrongArms(player)) {
      this.m_forceOpen = true;
    };
  }

  protected final void ForceUnequipStrongArms(ref<PlayerPuppet> player) {
    ForceUnequipStrongArms(player);
  }
}

public class WaitForEquipDecisions extends UpperBodyTransition {

  protected const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return ToBool(GetActiveWeapon(scriptInterface));
  }

  protected final const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime(stateContext, scriptInterface) > 2;
  }
}

public class WaitForEquipEvents extends UpperBodyEventsTransition {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.ReequipWeapon);
  }
}

public class AdHocAnimationDecisions extends UpperBodyEventsTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    blackboard = blackboardSystem.Get(GetAllBlackboardDefs().AdHocAnimation);
    if(blackboard.GetBool(GetAllBlackboardDefs().AdHocAnimation.IsActive)) {
      return true;
    };
    return false;
  }

  protected const Bool ToSingleWield(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) > GetStaticFloatParameter("animDuration", 2) && HasRightWeaponEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected const Bool ToEmptyHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) > GetStaticFloatParameter("animDuration", 2) && !HasRightWeaponEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }
}

public class AdHocAnimationEvents extends TemporaryUnequipEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_AdHocAnimation> adHocFeature;
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    blackboard = blackboardSystem.Get(GetAllBlackboardDefs().AdHocAnimation);
    adHocFeature = new AnimFeature_AdHocAnimation();
    adHocFeature.useBothHands = true;
    adHocFeature.isActive = true;
    adHocFeature.animationIndex = blackboard.GetInt(GetAllBlackboardDefs().AdHocAnimation.AnimationIndex);
    if(!blackboard.GetBool(GetAllBlackboardDefs().AdHocAnimation.UseBothHands)) {
      adHocFeature.useBothHands = false;
    };
    if(blackboard.GetBool(GetAllBlackboardDefs().AdHocAnimation.UnequipWeapon)) {
      OnEnter(stateContext, scriptInterface);
    };
    scriptInterface.SetAnimationParameterFeature("AdHoc", adHocFeature);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_AdHocAnimation> adHocFeature;
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    blackboard = blackboardSystem.Get(GetAllBlackboardDefs().AdHocAnimation);
    if(blackboard.GetBool(GetAllBlackboardDefs().AdHocAnimation.UnequipWeapon)) {
      OnExit(stateContext, scriptInterface);
    };
    adHocFeature = new AnimFeature_AdHocAnimation();
    adHocFeature.isActive = false;
    scriptInterface.SetAnimationParameterFeature("AdHoc", adHocFeature);
    blackboard.SetBool(GetAllBlackboardDefs().AdHocAnimation.IsActive, false);
  }
}
