
public abstract class WeaponTransition extends DefaultTransition {

  protected final void ShowAttackPreview(Bool showIfAiming, ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    Float ricochetEnableStat;
    Float techPierceStat;
    Float ricochetCount;
    Bool canShowPreview;
    ref<WeaponObject> weaponObject;
    ref<StatsSystem> statSystem;
    ref<WeaponItem_Record> weaponRecord;
    Bool isAiming;
    Bool show;
    canShowPreview = true;
    show = false;
    isAiming = showIfAiming && IsInUpperBodyState(stateContext, "aimingState");
    weaponObject = GetWeaponObject(scriptInterface);
    if(isAiming) {
      statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
      weaponRecord = GetWeaponItemRecord(GetTDBID(weaponObject.GetItemID()));
      if(ToBool(weaponRecord) && weaponRecord.PreviewEffectName() != "") {
        techPierceStat = statSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.TechPierceEnabled);
        if(weaponRecord.PreviewEffectTag() == "ricochet") {
          ricochetEnableStat = statSystem.GetStatValue(Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID()), gamedataStatType.CanSeeRicochetVisuals);
          ricochetCount = statSystem.GetStatValue(Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID()), gamedataStatType.RicochetCount);
          show = ricochetEnableStat > 0 && ricochetCount > 0;
        } else {
          if(weaponRecord.PreviewEffectTag() == "pierce") {
            show = techPierceStat > 0;
          };
        };
      };
    };
    weaponObject.GetCurrentAttack().SetPreviewActive(show);
  }

  protected final ref<Attack_Record> GetDesiredAttackRecord(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<WeaponItem_Record> weaponRecord;
    ref<RangedAttack_Record> rangedAttack;
    ref<Attack_Record> attackRecord;
    Float weaponCharge;
    InnerItemData magazine;
    TweakDBID magazineAttack;
    ref<RangedAttackPackage_Record> rangedAttackPackage;
    weaponObject = GetWeaponObject(scriptInterface);
    weaponRecord = GetWeaponItemRecord(GetTDBID(weaponObject.GetItemID()));
    rangedAttackPackage = WeakRefToRef(weaponRecord.RangedAttacks());
    WeakRefToRef(weaponObject.GetItemData()).GetItemPart(magazine, "AttachmentSlots.DamageMod");
    magazineAttack = Create(GetString(GetTDBID(GetItemID(magazine)) + ".overrideAttack", ""));
    if(IsValid(magazineAttack)) {
      rangedAttackPackage = GetRangedAttackPackageRecord(magazineAttack);
    };
    weaponCharge = GetWeaponChargeNormalized(RefToWeakRef(weaponObject));
    rangedAttack = WeakRefToRef(weaponCharge >= 1 ? rangedAttackPackage.ChargeFire() : rangedAttackPackage.DefaultFire());
    if(GetTimeSystem(WeakRefToRef(scriptInterface.owner).GetGame()).IsTimeDilationActive() && !WeakRefToRef(weaponRecord.Evolution()).Type() == gamedataWeaponEvolution.Tech) {
      attackRecord = WeakRefToRef(rangedAttack.PlayerTimeDilated());
    } else {
      attackRecord = WeakRefToRef(rangedAttack.PlayerAttack());
    };
    return attackRecord;
  }

  protected final const CName GetBurstTimeRemainingName() {
    return "ShootingSequence.BurstTimeRemaining";
  }

  protected final const CName GetBurstCycleTimeName() {
    return "ShootingSequence.BurstCycleTime";
  }

  protected final const CName GetCycleTimeRemainingName() {
    return "ShootingSequence.CycleTimeRemaining";
  }

  protected final const CName GetBurstShotsRemainingName() {
    return "ShootingSequence.BurstShotsRemaining";
  }

  protected final const CName GetShootingStartName() {
    return "ShootingSequence.Start";
  }

  protected final const CName GetShootingNumBurstTotalName() {
    return "ShootingSequence.NumBurstTotal";
  }

  protected final const CName GetIsDelayFireName() {
    return "ShootingSequence.IsDelayFire";
  }

  protected final const CName GetIsChargedFullAutoName() {
    return "ShootingSequence.IsChargedFullAutoSequence";
  }

  protected final const CName GetQuestForceShootName() {
    return "questForceShoot";
  }

  protected final const Bool InShootingSequence(ref<StateContext> stateContext) {
    return GetParameterFloat(GetShootingStartName(), stateContext, true) > 0;
  }

  protected final void StartShootingSequence(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float fireDelay, Float burstCycleTime, Int32 numShotsBurst, Bool isFullChargeFullAuto) {
    stateContext.SetPermanentFloatParameter(GetCycleTimeRemainingName(), fireDelay, true);
    stateContext.SetPermanentBoolParameter(GetIsDelayFireName(), fireDelay > 0, true);
    stateContext.SetPermanentFloatParameter(GetBurstTimeRemainingName(), burstCycleTime, true);
    stateContext.SetPermanentFloatParameter(GetBurstCycleTimeName(), burstCycleTime, true);
    stateContext.SetPermanentIntParameter(GetBurstShotsRemainingName(), numShotsBurst, true);
    stateContext.SetPermanentIntParameter(GetShootingNumBurstTotalName(), 0, true);
    stateContext.SetPermanentFloatParameter(GetShootingStartName(), ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())), true);
    stateContext.SetPermanentBoolParameter(GetIsChargedFullAutoName(), isFullChargeFullAuto, true);
    GetWeaponObject(scriptInterface).SetTriggerDown(true);
    GetWeaponObject(scriptInterface).SetupBurstFireSound(numShotsBurst);
  }

  protected final void ShootingSequencePostShoot(ref<StateContext> stateContext) {
    Int32 currShotCount;
    currShotCount = GetParameterInt(GetBurstShotsRemainingName(), stateContext, true);
    currShotCount = currShotCount - 1;
    stateContext.SetPermanentIntParameter(GetBurstShotsRemainingName(), Max(currShotCount, 0), true);
    stateContext.SetPermanentFloatParameter(GetBurstTimeRemainingName(), GetParameterFloat(GetBurstCycleTimeName(), stateContext, true), true);
  }

  protected final void SetupNextShootingPhase(ref<StateContext> stateContext, Float cycleTime, Float burstCycleTime, Int32 numShotsBurst) {
    Int32 currBurstsInSequenceCount;
    currBurstsInSequenceCount = GetParameterInt(GetShootingNumBurstTotalName(), stateContext, true);
    stateContext.SetPermanentFloatParameter(GetCycleTimeRemainingName(), cycleTime, true);
    stateContext.SetPermanentFloatParameter(GetBurstTimeRemainingName(), burstCycleTime, true);
    stateContext.SetPermanentIntParameter(GetBurstShotsRemainingName(), numShotsBurst, true);
    stateContext.SetPermanentBoolParameter(GetIsDelayFireName(), false, true);
    stateContext.SetPermanentIntParameter(GetShootingNumBurstTotalName(), currBurstsInSequenceCount + 1, true);
  }

  protected final void EndShootingSequence(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetPermanentFloatParameter(GetCycleTimeRemainingName(), 0, true);
    stateContext.SetPermanentFloatParameter(GetBurstTimeRemainingName(), 0, true);
    stateContext.SetPermanentIntParameter(GetBurstShotsRemainingName(), 0, true);
    stateContext.SetPermanentIntParameter(GetShootingNumBurstTotalName(), 0, true);
    stateContext.SetPermanentFloatParameter(GetShootingStartName(), 0, true);
    stateContext.SetPermanentBoolParameter(GetIsDelayFireName(), false, true);
    stateContext.SetPermanentBoolParameter(GetIsChargedFullAutoName(), false, true);
    GetWeaponObject(scriptInterface).SetTriggerDown(false);
  }

  protected final void ShootingSequenceUpdateCycleTime(Float timeDelta, ref<StateContext> stateContext) {
    Float timeRemaining;
    timeRemaining = GetParameterFloat(GetCycleTimeRemainingName(), stateContext, true);
    stateContext.SetPermanentFloatParameter(GetCycleTimeRemainingName(), MaxF(timeRemaining - timeDelta, 0), true);
  }

  protected final void ShootingSequenceUpdateBurstTime(Float timeDelta, ref<StateContext> stateContext) {
    Float timeRemaining;
    timeRemaining = GetParameterFloat(GetBurstTimeRemainingName(), stateContext, true);
    stateContext.SetPermanentFloatParameter(GetBurstTimeRemainingName(), timeRemaining - timeDelta, true);
  }

  protected final const Bool CanPerformNextShotInSequence(ref<WeaponObject> weaponObject, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    GameInstance gameInstance;
    Float cycleTime;
    Float lastShotTime;
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    cycleTime = GetStatsSystem(gameInstance).GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.CycleTime);
    lastShotTime = GetParameterFloat("LastShotTime", stateContext, true);
    return ToFloat(GetSimTime(gameInstance)) > lastShotTime + cycleTime;
  }

  protected final const Bool CanPerformNextSemiAutoShot(ref<WeaponObject> weaponObject, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool validTriggerMode;
    validTriggerMode = false;
    if(CanPerformNextShotInSequence(weaponObject, stateContext, scriptInterface)) {
      validTriggerMode = scriptInterface.IsTriggerModeActive(gamedataTriggerMode.SemiAuto) || scriptInterface.IsTriggerModeActive(gamedataTriggerMode.Burst);
      return validTriggerMode && !IsMagazineEmpty(weaponObject);
    };
    return false;
  }

  protected final const Bool CanPerformNextFullAutoShot(ref<WeaponObject> weaponObject, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool validTriggerMode;
    validTriggerMode = false;
    if(CanPerformNextShotInSequence(weaponObject, stateContext, scriptInterface)) {
      validTriggerMode = scriptInterface.IsTriggerModeActive(gamedataTriggerMode.FullAuto) || GetParameterBool(GetIsChargedFullAutoName(), stateContext, true) && scriptInterface.IsTriggerModeActive(gamedataTriggerMode.Charge);
      return validTriggerMode && !IsMagazineEmpty(weaponObject);
    };
    return false;
  }

  protected final void SetupStandardShootingSequence(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<StatsSystem> statsSystem;
    gamedataStatType burstCycleTimeStat;
    gamedataStatType burstNumShots;
    weaponObject = GetWeaponObject(scriptInterface);
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    burstCycleTimeStat = gamedataStatType.CycleTime_Burst;
    burstNumShots = gamedataStatType.NumShotsInBurst;
    if(HasSecondaryTriggerMode(scriptInterface) && !IsPrimaryTriggerModeActive(scriptInterface)) {
      burstCycleTimeStat = gamedataStatType.CycleTime_BurstSecondary;
      burstNumShots = gamedataStatType.NumShotsInBurstSecondary;
    };
    StartShootingSequence(stateContext, scriptInterface, statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.PreFireTime), statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), burstCycleTimeStat), Cast(statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), burstNumShots)), false);
  }

  protected final const Bool IsSemiAutoAction(ref<WeaponObject> weaponObject, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.GetActionValue("RangedAttack") > 0 && IsInVisionModeActiveState(stateContext, scriptInterface)) {
      return true;
    };
    return scriptInterface.IsActionJustPressed("RangedAttack");
  }

  protected final const Bool IsFullAutoAction(ref<WeaponObject> weaponObject, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.GetActionValue("RangedAttack") > 0;
  }

  protected final void ShowDebugText(String textToShow, ref<StateGameScriptInterface> scriptInterface, out Uint32 layerId) {
    layerId = GetDebugVisualizerSystem(WeakRefToRef(scriptInterface.owner).GetGame()).DrawText(new Vector4(500,550,0,0), textToShow, gameDebugViewETextAlignment.Left, new Color(255,255,0,255));
    GetDebugVisualizerSystem(WeakRefToRef(scriptInterface.owner).GetGame()).SetScale(layerId, new Vector4(1,1,0,0));
  }

  protected final void ClearDebugText(Uint32 layerId, ref<StateGameScriptInterface> scriptInterface) {
    GetDebugVisualizerSystem(WeakRefToRef(scriptInterface.owner).GetGame()).ClearLayer(layerId);
  }

  protected final const ref<WeaponObject> GetWeaponObject(ref<StateGameScriptInterface> scriptInterface) {
    return Cast(WeakRefToRef(scriptInterface.owner));
  }

  protected final void PlayEffect(CName effectName, ref<StateGameScriptInterface> scriptInterface, CName eventTag?) {
    ref<entSpawnEffectEvent> spawnEffectEvent;
    ref<WeaponObject> weapon;
    weapon = GetWeaponObject(scriptInterface);
    if(ToBool(weapon)) {
      spawnEffectEvent = new entSpawnEffectEvent();
      spawnEffectEvent.effectName = effectName;
      spawnEffectEvent.effectInstanceName = eventTag;
      weapon.QueueEventToChildItems(spawnEffectEvent);
    };
  }

  protected final void StopEffect(CName effectName, ref<StateGameScriptInterface> scriptInterface) {
    ref<entKillEffectEvent> killEffectEvent;
    ref<WeaponObject> weapon;
    weapon = GetWeaponObject(scriptInterface);
    if(ToBool(weapon)) {
      killEffectEvent = new entKillEffectEvent();
      killEffectEvent.effectName = effectName;
      weapon.QueueEventToChildItems(killEffectEvent);
    };
  }

  protected final const Bool IsWeaponReadyToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Int32 takedownState;
    Int32 combatGadgetState;
    Float timeStamp;
    Float now;
    Bool isSwitchingItems;
    Bool isSprinting;
    Bool inFocusState;
    Bool isInTakedown;
    Bool isUsingCombatGadget;
    Bool isInWorkspot;
    Bool isInVehCombat;
    Bool isInScene;
    Bool isDodging;
    Bool isVaulting;
    blackboard = GetBlackboard(scriptInterface);
    takedownState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown);
    isSwitchingItems = !IsRightHandInEquippedState(stateContext);
    inFocusState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus);
    isInTakedown = takedownState == ToInt(gamePSMTakedown.Grapple) || takedownState == ToInt(gamePSMTakedown.Leap) || takedownState == ToInt(gamePSMTakedown.Takedown);
    isUsingCombatGadget = combatGadgetState > ToInt(gamePSMCombatGadget.Default) && combatGadgetState < ToInt(gamePSMCombatGadget.WaitForUnequip);
    isInWorkspot = IsInLocomotionState(stateContext, "workspot");
    isInVehCombat = GetParameterBool("isInVehCombat", stateContext, true) || GetParameterBool("isInDriverCombat", stateContext, true);
    isInScene = IsInSafeSceneTier(scriptInterface);
    isDodging = IsInLocomotionState(stateContext, "dodge") || IsInLocomotionState(stateContext, "dodgeAir");
    isVaulting = IsInLocomotionState(stateContext, "vault");
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponShootWhileVaulting) && isVaulting) {
      return false;
    };
    if(stateContext.IsStateMachineActive("Consumable")) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanShootWhileDodging) && isDodging) {
      return false;
    };
    if(IsNoCombatActionsForced(scriptInterface)) {
      return false;
    };
    if(IsRightHandInUnequippingState(stateContext)) {
      return false;
    };
    if(HasStatusEffect(scriptInterface.executionOwner, gamedataStatusEffectType.Stunned)) {
      return false;
    };
    if(isInWorkspot && !isInVehCombat) {
      return false;
    };
    if(isInTakedown) {
      return false;
    };
    if(isSwitchingItems) {
      return false;
    };
    if(isUsingCombatGadget) {
      return false;
    };
    if(isInScene) {
      return false;
    };
    if(HasStatusEffect(scriptInterface.executionOwner, gamedataStatusEffectType.Jam)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponShootWhileSliding) && IsInSlidingState(stateContext)) {
      return false;
    };
    if(IsAimingAtFriendlyTarget(scriptInterface)) {
      return ShouldIgnoreWeaponSafe(scriptInterface);
    };
    return true;
  }

  protected final Int32 GetWeaponTriggerModesNumber(ref<StateGameScriptInterface> scriptInterface) {
    ref<ItemObject> item;
    ItemID itemID;
    array<wref<TriggerMode_Record>> triggerModesArray;
    ref<WeaponItem_Record> weaponRecordData;
    item = GetTransactionSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).GetItemInSlot(WeakRefToRef(scriptInterface.executionOwner), "AttachmentSlots.WeaponRight");
    itemID = item.GetItemID();
    weaponRecordData = GetWeaponItemRecord(GetTDBID(itemID));
    weaponRecordData.TriggerModes(triggerModesArray);
    return Size(triggerModesArray);
  }

  protected final const Bool CompareTimeToPublicSafeTimestamp(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float timeToCompare) {
    Float exitTimeStamp;
    exitTimeStamp = GetParameterFloat("TurnOffPublicSafeTimeStamp", stateContext, true);
    return ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) - exitTimeStamp > timeToCompare;
  }

  protected final const Bool HasSecondaryTriggerMode(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    ref<WeaponItem_Record> weaponRecord;
    weapon = GetWeaponObject(scriptInterface);
    weaponRecord = GetWeaponItemRecord(GetTDBID(weapon.GetItemID()));
    if(ToBool(weaponRecord.SecondaryTriggerMode())) {
      if(WeakRefToRef(weaponRecord.PrimaryTriggerMode()).Type() != WeakRefToRef(weaponRecord.SecondaryTriggerMode()).Type()) {
        return true;
      };
    };
    return false;
  }

  protected final const void SwitchTriggerMode(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponChangeTriggerModeEvent> evt;
    ref<WeaponObject> weapon;
    ref<WeaponItem_Record> weaponRecord;
    evt = new WeaponChangeTriggerModeEvent();
    weapon = GetWeaponObject(scriptInterface);
    weaponRecord = GetWeaponItemRecord(GetTDBID(weapon.GetItemID()));
    if(IsPrimaryTriggerModeActive(scriptInterface)) {
      evt.triggerMode = WeakRefToRef(weaponRecord.SecondaryTriggerMode()).Type();
    } else {
      evt.triggerMode = WeakRefToRef(weaponRecord.PrimaryTriggerMode()).Type();
    };
    weapon.QueueEvent(evt);
  }

  protected final const Bool IsPrimaryTriggerModeActive(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    ref<WeaponItem_Record> weaponRecord;
    weapon = GetWeaponObject(scriptInterface);
    weaponRecord = GetWeaponItemRecord(GetTDBID(weapon.GetItemID()));
    if(weapon.GetCurrentTriggerMode() == WeakRefToRef(weaponRecord.PrimaryTriggerMode())) {
      return true;
    };
    return false;
  }

  protected final void UpdateInputBuffer(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustPressed("Reload")) {
      stateContext.SetConditionFloatParameter("RealodInputPressBuffer", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) + 0.20000000298023224, true);
      stateContext.SetConditionBoolParameter("RealodInputPressed", true, true);
    };
  }

  public final static Bool ShouldReloadWeapon(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsServer()) {
      return ServerHasReloadRequest(stateContext, scriptInterface);
    };
    if(CanReload(Cast(WeakRefToRef(scriptInterface.owner)))) {
      if(scriptInterface.IsActionJustTapped("Reload")) {
        return true;
      };
      if(GetConditionParameterBool("RealodInputPressed", stateContext) && GetConditionParameterFloat("RealodInputPressBuffer", stateContext) > ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame()))) {
        return true;
      };
    };
    return false;
  }

  public final static Float GetPlayerSpeed(ref<StateGameScriptInterface> scriptInterface) {
    Float speed;
    Vector4 velocity;
    ref<PlayerPuppet> player;
    player = Cast(WeakRefToRef(scriptInterface.executionOwner));
    velocity = player.GetVelocity();
    speed = Length2D(velocity);
    return speed;
  }

  public final static Bool ServerHasReloadRequest(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<parameterRequestReload> paramRequest;
    paramRequest = Cast(stateContext.GetTemporaryScriptableParameter("serverRequestReload"));
    if(ToBool(paramRequest)) {
      return WeakRefToRef(paramRequest.item) == WeakRefToRef(scriptInterface.owner);
    };
    return false;
  }

  protected final const Bool IsHeavyWeaponEmpty(ref<StateGameScriptInterface> scriptInterface) {
    wref<WeaponObject> weapon;
    weapon = RefToWeakRef(Cast(WeakRefToRef(scriptInterface.owner)));
    return GetEquipAreaType(WeakRefToRef(weapon).GetItemID()) == gamedataEquipmentArea.WeaponHeavy && IsMagazineEmpty(WeakRefToRef(weapon));
  }

  protected final const Float GetMaxChargeThreshold(ref<StateGameScriptInterface> scriptInterface) {
    if(HasStatFlag(scriptInterface, gamedataStatType.CanOverchargeWeapon)) {
      return GetOverchargeThreshold();
    };
    if(HasStatFlag(scriptInterface, gamedataStatType.CanFullyChargeWeapon)) {
      return GetFullyChargedThreshold();
    };
    return GetBaseMaxChargeThreshold();
  }

  protected final const Float GetReloadAnimSpeed(ref<StateGameScriptInterface> scriptInterface, gamedataStatType statType) {
    ref<WeaponObject> weaponObject;
    ref<WeaponItem_Record> weaponRecord;
    TweakDBID weaponRecID;
    Float baseVal;
    Float statVal;
    Float finalVal;
    weaponObject = GetWeaponObject(scriptInterface);
    weaponRecord = GetWeaponItemRecord(GetTDBID(weaponObject.GetItemID()));
    weaponRecID = weaponRecord.GetID();
    statVal = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), statType);
    if(statType == gamedataStatType.ReloadTime) {
      baseVal = GetFloat(weaponRecID + ".baseReloadTime");
    } else {
      if(statType == gamedataStatType.EmptyReloadTime) {
        baseVal = GetFloat(weaponRecID + ".baseEmptyReloadTime");
      };
    };
    if(baseVal > 0 && statVal > 0) {
      return baseVal / statVal;
    };
    return 1;
  }

  protected final const Bool IsReloadDurationComplete(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<GameObject> owner;
    EntityID ownerID;
    ref<StatsSystem> statsSystem;
    Float statValue;
    StateResultFloat logicalDuration;
    owner = scriptInterface.owner;
    ownerID = WeakRefToRef(owner).GetEntityID();
    statsSystem = GetStatsSystem(WeakRefToRef(owner).GetGame());
    logicalDuration = stateContext.GetPermanentFloatParameter("ReloadLogicalDuration");
    if(logicalDuration.valid) {
      statValue = logicalDuration.value;
      statValue += statsSystem.GetStatValue(Cast(ownerID), gamedataStatType.ReloadEndTime);
      if(GetInStateTime(stateContext, scriptInterface) > statValue) {
        return true;
      };
      return false;
    };
    return true;
  }

  protected final const Bool IsReloadUninterruptible(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat logicalDuration;
    Float stateTime;
    StateResultBool emptyReload;
    Float uninterruptibleTimeStamp;
    ref<WeaponItem_Record> weaponRecord;
    TweakDBID weaponRecID;
    Float reloadSpeed;
    stateTime = GetInStateTime(stateContext, scriptInterface);
    emptyReload = stateContext.GetPermanentBoolParameter("EmptyReload");
    weaponRecord = GetWeaponItemRecord(GetTDBID(GetWeaponObject(scriptInterface).GetItemID()));
    weaponRecID = weaponRecord.GetID();
    if(emptyReload.value) {
      uninterruptibleTimeStamp = GetFloat(weaponRecID + ".uninterruptibleEmptyReloadStart");
      reloadSpeed = GetReloadAnimSpeed(scriptInterface, gamedataStatType.EmptyReloadTime);
    } else {
      uninterruptibleTimeStamp = GetFloat(weaponRecID + ".uninterruptibleReloadStart");
      reloadSpeed = GetReloadAnimSpeed(scriptInterface, gamedataStatType.ReloadTime);
    };
    uninterruptibleTimeStamp = uninterruptibleTimeStamp / reloadSpeed;
    if(GetInStateTime(stateContext, scriptInterface) > uninterruptibleTimeStamp) {
      return true;
    };
    return false;
  }

  protected final const void SetUninteruptibleReloadParams(ref<StateContext> stateContext, Bool clearParam) {
    if(clearParam) {
      stateContext.RemovePermanentBoolParameter("UninteruptibleReload");
      return ;
    };
    if(!GetParameterBool("UninteruptibleReload", stateContext, true)) {
      stateContext.SetPermanentBoolParameter("UninteruptibleReload", true, true);
    };
  }
}

public abstract class WeaponEventsTransition extends WeaponTransition {

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ActivateDamageProjection(false, GetWeaponObject(scriptInterface), scriptInterface, stateContext);
    SetUninteruptibleReloadParams(stateContext, true);
  }

  protected final void StartWeaponCharge(ref<StatPoolsSystem> statPoolsSystem, EntityID weaponEntityID) {
    StatPoolModifier chargeMod;
    statPoolsSystem.GetModifier(Cast(weaponEntityID), gamedataStatPoolType.WeaponCharge, gameStatPoolModificationTypes.Regeneration, chargeMod);
    chargeMod.enabled = true;
    statPoolsSystem.RequestSettingModifier(Cast(weaponEntityID), gamedataStatPoolType.WeaponCharge, gameStatPoolModificationTypes.Regeneration, chargeMod);
    statPoolsSystem.RequestResetingModifier(Cast(weaponEntityID), gamedataStatPoolType.WeaponCharge, gameStatPoolModificationTypes.Decay);
  }

  protected final void OnEnterNonChargeState(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    EntityID weaponID;
    GetWeaponObject(scriptInterface).GetSharedData().SetVariant(GetAllBlackboardDefs().Weapon.ChargeStep, ToVariant(gamedataChargeStep.Idle));
    if(GetParameterBool("WeaponStopChargeRequested", stateContext, true)) {
      weaponID = GetWeaponObject(scriptInterface).GetEntityID();
      StopPool(GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame()), weaponID, gamedataStatPoolType.WeaponCharge, true);
      stateContext.SetPermanentBoolParameter("WeaponStopChargeRequested", false, true);
    };
  }
}

public class ReadyDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsWeaponReadyToShoot(stateContext, scriptInterface);
  }

  protected final const Bool ToSemiAuto(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    StateResultBool questForceShoot;
    Bool validTransition;
    weaponObject = GetWeaponObject(scriptInterface);
    validTransition = false;
    if(CanPerformNextSemiAutoShot(weaponObject, stateContext, scriptInterface)) {
      validTransition = IsSemiAutoAction(weaponObject, stateContext, scriptInterface);
      if(!validTransition) {
        questForceShoot = stateContext.GetTemporaryBoolParameter(GetQuestForceShootName());
        validTransition = questForceShoot.value;
      };
    };
    return validTransition;
  }

  protected final const Bool ToFullAuto(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    StateResultBool questForceShoot;
    Bool validTransition;
    weaponObject = GetWeaponObject(scriptInterface);
    validTransition = false;
    if(CanPerformNextFullAutoShot(weaponObject, stateContext, scriptInterface)) {
      validTransition = IsFullAutoAction(weaponObject, stateContext, scriptInterface);
      if(!validTransition) {
        questForceShoot = stateContext.GetTemporaryBoolParameter(GetQuestForceShootName());
        validTransition = questForceShoot.value;
      };
    };
    return validTransition;
  }
}

public class ReadyEvents extends WeaponEventsTransition {

  public Float m_timeStamp;

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<WeaponObject> weapon;
    OnEnterNonChargeState(stateContext, scriptInterface);
    UpdateAimAssist(stateContext, scriptInterface);
    weapon = RefToWeakRef(Cast(WeakRefToRef(scriptInterface.owner)));
    EndShootingSequence(stateContext, scriptInterface);
    stateContext.SetConditionWeakScriptableParameter("Weapon", weapon, true);
    scriptInterface.TEMP_WeaponStopFiring();
    if(!IsMagazineEmpty(WeakRefToRef(weapon))) {
      SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    };
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Ready));
    stateContext.SetPermanentBoolParameter("WeaponInSafe", false, true);
    scriptInterface.SetAnimationParameterFloat("safe", 0);
    this.m_timeStamp = ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame()));
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<WeaponItem_Record> weaponRecord;
    TweakDBID attackID;
    Bool behindCover;
    ref<AnimFeature_WeaponHandlingStats> animFeature;
    GameInstance gameInstance;
    Float currentTime;
    ref<StatsSystem> statsSystem;
    EntityID ownerID;
    weaponObject = GetWeaponObject(scriptInterface);
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    currentTime = ToFloat(GetSimTime(gameInstance));
    attackID = GetDesiredAttackRecord(stateContext, scriptInterface).GetID();
    if(WeakRefToRef(weaponObject.GetCurrentAttack().GetRecord()).GetID() != attackID) {
      weaponObject.SetAttack(attackID);
    };
    behindCover = GetSpatialQueriesSystem(gameInstance).GetPlayerObstacleSystem().GetCoverDirection(WeakRefToRef(scriptInterface.executionOwner)) != gamePlayerCoverDirection.;
    if(behindCover) {
      this.m_timeStamp = currentTime;
      stateContext.SetPermanentFloatParameter("TurnOffPublicSafeTimeStamp", this.m_timeStamp, true);
    };
    ShowAttackPreview(true, scriptInterface, stateContext);
    HandleDamagePreview(weaponObject, scriptInterface, stateContext);
    if(GetPlayerSpeed(scriptInterface) < 0.10000000149011612 && IsInLocomotionState(stateContext, "stand")) {
      if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Combat) == ToInt(gamePSMCombat.InCombat)) {
        return ;
      };
      if(behindCover) {
        return ;
      };
      if(this.m_timeStamp + GetStaticFloatParameter("timeBetweenIdleBreaks", 20) < currentTime) {
        scriptInterface.PushAnimationEvent("IdleBreak");
        this.m_timeStamp = currentTime;
      };
    };
    if(IsHeavyWeaponEmpty(scriptInterface) && !GetParameterBool("requestHeavyWeaponUnequip", stateContext, true)) {
      stateContext.SetPermanentBoolParameter("requestHeavyWeaponUnequip", true, true);
    };
    statsSystem = GetStatsSystem(gameInstance);
    ownerID = WeakRefToRef(scriptInterface.owner).GetEntityID();
    animFeature = new AnimFeature_WeaponHandlingStats();
    animFeature.weaponRecoil = statsSystem.GetStatValue(Cast(ownerID), gamedataStatType.RecoilAnimation);
    animFeature.weaponSpread = statsSystem.GetStatValue(Cast(ownerID), gamedataStatType.SpreadAnimation);
    scriptInterface.SetAnimationParameterFeature("WeaponHandlingData", animFeature, WeakRefToRef(scriptInterface.executionOwner));
  }

  private final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    ActivateDamageProjection(false, GetWeaponObject(scriptInterface), scriptInterface, stateContext);
  }
}

public class NotReadyDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsWeaponReadyToShoot(stateContext, scriptInterface);
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsWeaponReadyToShoot(stateContext, scriptInterface);
  }
}

public class NotReadyEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnterNonChargeState(stateContext, scriptInterface);
    EndShootingSequence(stateContext, scriptInterface);
    ShowAttackPreview(false, scriptInterface, stateContext);
    scriptInterface.TEMP_WeaponStopFiring();
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    ForceUnhideRegularHands(stateContext, scriptInterface);
    UpdateAimAssist(stateContext, scriptInterface);
    GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).RequestSettingStatPoolMinValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), gamedataStatPoolType.WeaponCharge, scriptInterface.executionOwner);
    if(!GetBoolFromQuestDB(scriptInterface, "block_combat_scripts_tutorials")) {
      TutorialSetFact(scriptInterface, "ranged_combat_tutorial");
    };
  }

  protected final void ForceUnhideRegularHands(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_MeleeData> animFeature;
    animFeature = new AnimFeature_MeleeData();
    animFeature.shouldHandsDisappear = false;
    scriptInterface.SetAnimationParameterFeature("MeleeData", animFeature);
  }
}

public class SafeDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(ShouldEnterSafe(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToPublicSafe(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!EnterCondition(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }
}

public class SafeEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StimBroadcasterComponent> broadcaster;
    OnEnterNonChargeState(stateContext, scriptInterface);
    ShowAttackPreview(false, scriptInterface, stateContext);
    EndShootingSequence(stateContext, scriptInterface);
    scriptInterface.TEMP_WeaponStopFiring();
    stateContext.SetPermanentBoolParameter("WeaponInSafe", true, true);
    stateContext.SetTemporaryBoolParameter(GetReevaluateZoomName(), true, true);
    scriptInterface.SetAnimationParameterFloat("safe", 1);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Safe));
    UpdateAimAssist(stateContext, scriptInterface);
    broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
    if(ToBool(broadcaster)) {
      broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.WeaponSafe);
    };
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetTemporaryBoolParameter(GetReevaluateZoomName(), true, true);
  }
}

public class PublicSafeDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Bool isAiming;
    Bool isKereznikowActive;
    Bool isWeaponActive;
    Bool isSprinting;
    Bool isInVehicleCombat;
    Bool isInVehTurret;
    Bool inFocusState;
    Bool isInCombat;
    Bool isInDangerous;
    StateResultBool equippingFromEmptyHandsWithAttack;
    blackboard = GetBlackboard(scriptInterface);
    isAiming = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
    isKereznikowActive = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Kereznikov);
    isWeaponActive = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Ready);
    isSprinting = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Sprint);
    isInVehicleCombat = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle) == ToInt(gamePSMVehicle.Combat);
    isInVehTurret = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle) == ToInt(gamePSMVehicle.Turret);
    inFocusState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus);
    isInCombat = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == ToInt(gamePSMCombat.InCombat);
    isInDangerous = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Zones) == ToInt(gamePSMZones.Dangerous);
    equippingFromEmptyHandsWithAttack = stateContext.GetPermanentBoolParameter("equippingRangedFromEmptyHandsAttack");
    if(isSprinting) {
      return false;
    };
    if(isInCombat) {
      return false;
    };
    if(isInDangerous) {
      return false;
    };
    if(inFocusState && scriptInterface.GetActionValue("RangedAttack") == 0) {
      return true;
    };
    if(GetParameterBool("ForceReadyState", stateContext, true)) {
      return false;
    };
    if(IsSafeStateForced(stateContext, scriptInterface)) {
      return false;
    };
    if(isKereznikowActive) {
      return false;
    };
    if(isInVehicleCombat || isInVehTurret) {
      return false;
    };
    if(isAiming) {
      return false;
    };
    if(scriptInterface.GetActionValue("RangedAttack") > 0 && !equippingFromEmptyHandsWithAttack.value && equippingFromEmptyHandsWithAttack.valid) {
      return false;
    };
    if(!GetParameterBool("InPublicZone", stateContext, true)) {
      return false;
    };
    if(!CompareTimeToPublicSafeTimestamp(stateContext, scriptInterface, GetStaticFloatParameter("idleTimeToEnter", 1))) {
      return false;
    };
    if(stateContext.IsStateMachineActive("CombatGadget")) {
      return false;
    };
    if(stateContext.IsStateMachineActive("CarriedObject") && HasStatFlag(scriptInterface, gamedataStatType.CanShootWhileCarryingBody)) {
      return false;
    };
    if(GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCoverDirection(WeakRefToRef(scriptInterface.executionOwner)) != gamePlayerCoverDirection.) {
      return false;
    };
    return IsWeaponReadyToShoot(stateContext, scriptInterface);
  }

  protected final const Bool ToNotReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsWeaponReadyToShoot(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToPublicSafeToReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(ShouldLeaveSafe(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToNoAmmo(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsMagazineEmpty(GetWeaponObject(scriptInterface)) && CanReload(GetWeaponObject(scriptInterface))) {
      return true;
    };
    return false;
  }

  private final const Bool ShouldLeaveSafe(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Bool isAiming;
    Bool isKereznikowActive;
    Bool isInCombat;
    blackboard = GetBlackboard(scriptInterface);
    isAiming = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
    isKereznikowActive = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Kereznikov);
    if(IsInFocusMode(scriptInterface) && scriptInterface.IsActionJustPressed("RangedAttack") || scriptInterface.GetActionValue("RangedAttack") > 0) {
      return true;
    };
    if(IsInFocusMode(scriptInterface)) {
      return false;
    };
    if(GetParameterBool("ForceReadyState", stateContext, true)) {
      return true;
    };
    if(isKereznikowActive) {
      return true;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == ToInt(gamePSMCombat.InCombat)) {
      return true;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Zones) == ToInt(gamePSMZones.Dangerous)) {
      return true;
    };
    if(scriptInterface.GetActionValue("RangedAttack") > 0) {
      return true;
    };
    if(isAiming) {
      return true;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.CombatGadget) >= ToInt(gamePSMCombatGadget.Equipped)) {
      return true;
    };
    if(GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCoverDirection(WeakRefToRef(scriptInterface.executionOwner)) != gamePlayerCoverDirection.) {
      return true;
    };
    if(stateContext.IsStateMachineActive("CarriedObject") && HasStatFlag(scriptInterface, gamedataStatType.CanShootWhileCarryingBody)) {
      return true;
    };
    return false;
  }
}

public class PublicSafeEvents extends WeaponEventsTransition {

  [Default(PublicSafeEvents, false))]
  public Bool m_weaponUnequipRequestSent;

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StimBroadcasterComponent> broadcaster;
    OnEnterNonChargeState(stateContext, scriptInterface);
    EndShootingSequence(stateContext, scriptInterface);
    ShowAttackPreview(false, scriptInterface, stateContext);
    scriptInterface.TEMP_WeaponStopFiring();
    stateContext.SetPermanentBoolParameter("WeaponInSafe", true, true);
    scriptInterface.SetAnimationParameterFloat("safe", 1);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Safe));
    UpdateAimAssist(stateContext, scriptInterface);
    broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
    if(ToBool(broadcaster)) {
      broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.WeaponSafe);
    };
    stateContext.SetPermanentBoolParameter("equippingRangedFromEmptyHandsAttack", false, true);
    this.m_weaponUnequipRequestSent = false;
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsMultiplayer()) {
      RequestWeaponUnequipNotifyUpperBody(stateContext, scriptInterface);
    };
  }

  private final void RequestWeaponUnequipNotifyUpperBody(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!this.m_weaponUnequipRequestSent && GetStaticFloatParameter("timeToAutoUnequipWeapon", 0) > 0 && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("timeToAutoUnequipWeapon", 0)) {
      SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipWeapon);
      this.m_weaponUnequipRequestSent = true;
    };
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)

  protected final void OnExitToNotReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)
}

public class PublicSafeToReadyDecisions extends WeaponTransition {

  protected final const Bool ToReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime(stateContext, scriptInterface) > GetStaticFloatParameter("transitionDuration", 0.30000001192092896);
  }
}

public class PublicSafeToReadyEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    scriptInterface.SetAnimationParameterFloat("safe", 0);
    stateContext.SetPermanentBoolParameter("WeaponInSafe", false, true);
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetPermanentFloatParameter("TurnOffPublicSafeTimeStamp", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())), true);
  }
}

public class QuickMeleeDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isCarrying;
    Bool isEmptyInHands;
    wref<StatusEffectPlayerData_Record> statusEffectRecordData;
    statusEffectRecordData = GetStatusEffectRecordData(stateContext);
    if(HasRestriction(scriptInterface.executionOwner, "NoQuickMelee")) {
      return false;
    };
    if(!WeakRefToRef(statusEffectRecordData) == null && WeakRefToRef(statusEffectRecordData).ForceSafeWeapon() || WeakRefToRef(statusEffectRecordData).JamWeapon()) {
      return false;
    };
    if(GetStaticBoolParameter("disable", false)) {
      return false;
    };
    if(IsCooldownActive(WeakRefToRef(scriptInterface.owner), "QuickMelee")) {
      return false;
    };
    if(stateContext.IsStateMachineActive("Consumable") || stateContext.IsStateMachineActive("CombatGadget") || IsInFocusMode(scriptInterface)) {
      return false;
    };
    isCarrying = GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Carrying);
    if(isCarrying) {
      return false;
    };
    if(IsInSafeSceneTier(scriptInterface)) {
      return false;
    };
    if(scriptInterface.IsActionJustTapped("QuickMelee")) {
      return true;
    };
    return false;
  }

  protected final const Bool ToStandardExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("duration", 1)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToSemiAuto(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    weaponObject = GetWeaponObject(scriptInterface);
    return IsSemiAutoAction(weaponObject, stateContext, scriptInterface) && CanPerformNextSemiAutoShot(weaponObject, stateContext, scriptInterface);
  }

  protected final const Bool ToFullAuto(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    weaponObject = GetWeaponObject(scriptInterface);
    return IsFullAutoAction(weaponObject, stateContext, scriptInterface) && CanPerformNextFullAutoShot(weaponObject, stateContext, scriptInterface);
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsPassedCancelWindow(stateContext, scriptInterface);
  }

  protected final const Bool IsPassedCancelWindow(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat cancelWindow;
    cancelWindow = stateContext.GetPermanentFloatParameter("QuickMeleeCancelWindow");
    if(cancelWindow.valid && ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) >= cancelWindow.value) {
      return true;
    };
    return false;
  }
}

public class QuickMeleeEvents extends WeaponEventsTransition {

  public ref<EffectInstance> m_gameEffect;

  public wref<GameObject> m_targetObject;

  public ref<IPlacedComponent> m_targetComponent;

  [Default(QuickMeleeEvents, false))]
  public Bool m_quickMeleeAttackCreated;

  public QuickMeleeAttackData m_quickMeleeAttackData;

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StimBroadcasterComponent> broadcaster;
    this.m_quickMeleeAttackCreated = false;
    scriptInterface.TEMP_WeaponStopFiring();
    broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
    if(ToBool(broadcaster)) {
      broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.MeleeAttack);
    };
    stateContext.SetPermanentBoolParameter("VisionToggled", false, true);
    ForceDisableVisionMode(stateContext);
    stateContext.SetTemporaryBoolParameter("InterruptSprint", true, true);
    stateContext.SetPermanentFloatParameter("TurnOffPublicSafeTimeStamp", scriptInterface.GetNow(), true);
    stateContext.SetPermanentBoolParameter("WeaponInSafe", false, true);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.QuickMelee));
    UpdateAimAssist(stateContext, scriptInterface);
    SendAnimFeature(stateContext, scriptInterface, 1);
    scriptInterface.PushAnimationEvent("QuickMelee");
    PlayRumble(scriptInterface, GetStaticStringParameter("rumbleOnEnter", "light_fast"));
    GetWeaponObject(scriptInterface).SetAttack(GetQuickMeleeAttackTweakID(scriptInterface));
    GetAttackParameters(scriptInterface);
    if(this.m_quickMeleeAttackData.forcePlayerToStand) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    };
    ConsumeStamina(scriptInterface);
    AdjustPlayerPosition(stateContext, scriptInterface, GetQuickMeleeTarget(scriptInterface, this.m_quickMeleeAttackData.adjustmentRange), this.m_quickMeleeAttackData.adjustmentDuration, this.m_quickMeleeAttackData.adjustmentRadius, this.m_quickMeleeAttackData.adjustmentCurve);
    stateContext.SetPermanentFloatParameter("QuickMeleeCancelWindow", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) + this.m_quickMeleeAttackData.duration, true);
  }

  protected final void ConsumeStamina(ref<StateGameScriptInterface> scriptInterface) {
    Float staminaCost;
    wref<Attack_Melee_Record> attackRecord;
    array<wref<StatModifier_Record>> staminaCostMods;
    attackRecord = RefToWeakRef(Cast(GetAttack_GameEffectRecord(GetQuickMeleeAttackTweakID(scriptInterface))));
    WeakRefToRef(attackRecord).StaminaCost(staminaCostMods);
    staminaCost = CalculateStatModifiers(staminaCostMods, WeakRefToRef(scriptInterface.owner).GetGame(), scriptInterface.owner, Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()));
    if(staminaCost > 0) {
      ModifyStamina(Cast(WeakRefToRef(scriptInterface.executionOwner)), -staminaCost);
    };
  }

  protected final void InitiateQuickMeleeAttack(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 startPosition;
    Vector4 endPosition;
    Vector4 colliderBox;
    Float attackTime;
    Vector4 dir;
    startPosition.X = 0;
    startPosition.Y = 0;
    startPosition.Z = 0;
    endPosition.X = 0;
    endPosition.Y = this.m_quickMeleeAttackData.attackRange;
    endPosition.Z = 0;
    dir = endPosition - startPosition;
    colliderBox.X = 0.30000001192092896;
    colliderBox.Y = 0.30000001192092896;
    colliderBox.Z = 0.30000001192092896;
    attackTime = this.m_quickMeleeAttackData.attackGameEffectDuration;
    if(dir.Y != 0) {
      endPosition.Y = this.m_quickMeleeAttackData.attackRange;
    };
    SpawnQuickMeleeGameEffect(stateContext, scriptInterface, startPosition, endPosition, attackTime, colliderBox);
  }

  protected final ref<GameObject> GetQuickMeleeTarget(ref<StateGameScriptInterface> scriptInterface, Float withinDistance?) {
    ref<TargetingSystem> targetingSystem;
    EulerAngles angleOut;
    targetingSystem = GetTargetingSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    this.m_targetComponent = targetingSystem.GetComponentClosestToCrosshair(scriptInterface.executionOwner, angleOut, TSQ_NPC());
    this.m_targetObject = RefToWeakRef(scriptInterface.GetObjectFromComponent(this.m_targetComponent));
    if(WeakRefToRef(this.m_targetObject).IsPuppet() && IsAlive(WeakRefToRef(this.m_targetObject)) && !IsDefeated(WeakRefToRef(this.m_targetObject)) && GetAttitudeTowards(WeakRefToRef(this.m_targetObject), WeakRefToRef(scriptInterface.executionOwner)) == EAIAttitude.AIA_Neutral || GetAttitudeTowards(WeakRefToRef(this.m_targetObject), WeakRefToRef(scriptInterface.executionOwner)) == EAIAttitude.AIA_Hostile) {
      if(withinDistance < 0 || Distance(WeakRefToRef(scriptInterface.executionOwner).GetWorldPosition(), WeakRefToRef(this.m_targetObject).GetWorldPosition()) < withinDistance) {
        return WeakRefToRef(this.m_targetObject);
      };
    };
    return null;
  }

  protected final void SpawnQuickMeleeGameEffect(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Vector4 startPosition, Vector4 endPosition, Float attackTime, Vector4 colliderBox) {
    ref<EffectInstance> effect;
    String effectName;
    CName effectTag;
    Vector4 attackStartPositionWorld;
    Vector4 attackEndPositionWorld;
    Vector4 attackDirectionWorld;
    Transform cameraWorldTransform;
    ref<WeaponObject> weapon;
    ref<Attack_GameEffect> attack;
    AttackInitContext initContext;
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    attackStartPositionWorld = TransformPoint(cameraWorldTransform, startPosition);
    attackStartPositionWorld.W = 0;
    attackEndPositionWorld = TransformPoint(cameraWorldTransform, endPosition);
    attackEndPositionWorld.W = 0;
    attackDirectionWorld = attackEndPositionWorld - attackStartPositionWorld;
    weapon = GetWeaponObject(scriptInterface);
    PlaySound("Quickmelee_guns", scriptInterface);
    initContext.record = GetAttack_GameEffectRecord(GetQuickMeleeAttackTweakID(scriptInterface));
    initContext.source = scriptInterface.executionOwner;
    initContext.instigator = scriptInterface.executionOwner;
    initContext.weapon = RefToWeakRef(weapon);
    attack = Cast(Create(initContext));
    effect = attack.PrepareAttack(scriptInterface.executionOwner);
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.box, colliderBox);
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.duration, attackTime);
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, attackStartPositionWorld);
    SetQuat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.rotation, GetOrientation(cameraWorldTransform));
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, Normalize(attackDirectionWorld));
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, Length(attackDirectionWorld));
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.fxPackage, ToVariant(weapon.GetFxPackageQuickMelee()));
    SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.playerOwnedWeapon, true);
    this.m_gameEffect = effect;
    attack.StartAttack();
    PlayEffect(GetCName(GetQuickMeleeAttackTweakID(scriptInterface) + ".vfxName"), scriptInterface);
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    UpdateInputBuffer(stateContext, scriptInterface);
    if(!this.m_quickMeleeAttackCreated && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("attackGameEffectDelay", 0)) {
      InitiateQuickMeleeAttack(stateContext, scriptInterface);
      this.m_quickMeleeAttackCreated = true;
    };
    if(this.m_quickMeleeAttackCreated) {
      UpdateGameEffectPosition(stateContext, scriptInterface);
    };
  }

  protected final void UpdateGameEffectPosition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 startPosition;
    Vector4 endPosition;
    Transform cameraWorldTransform;
    startPosition.X = 0;
    startPosition.Y = 0;
    startPosition.Z = 0;
    endPosition.X = 0;
    endPosition.Y = this.m_quickMeleeAttackData.attackRange;
    endPosition.Z = 0;
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    startPosition = startPosition;
    endPosition = endPosition;
    startPosition = startPosition;
    SetVector(this.m_gameEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, TransformPoint(cameraWorldTransform, startPosition));
  }

  protected final void GetAttackParameters(ref<StateGameScriptInterface> scriptInterface) {
    TweakDBID recordID;
    recordID = GetQuickMeleeAttackTweakID(scriptInterface);
    this.m_quickMeleeAttackData.attackGameEffectDelay = GetFloat(recordID + ".attackGameEffectDelay");
    this.m_quickMeleeAttackData.attackGameEffectDuration = GetFloat(recordID + ".attackGameEffectDuration");
    this.m_quickMeleeAttackData.attackRange = GetFloat(recordID + ".attackRange");
    this.m_quickMeleeAttackData.forcePlayerToStand = GetBool(recordID + ".forcePlayerToStand");
    this.m_quickMeleeAttackData.shouldAdjust = GetBool(recordID + ".shouldAdjust");
    this.m_quickMeleeAttackData.adjustmentRange = GetFloat(recordID + ".adjustmentRange");
    this.m_quickMeleeAttackData.adjustmentDuration = GetFloat(recordID + ".adjustmentDuration");
    this.m_quickMeleeAttackData.adjustmentRadius = GetFloat(recordID + ".adjustmentRadius");
    this.m_quickMeleeAttackData.adjustmentCurve = GetCName(recordID + ".adjustmentCurve");
    this.m_quickMeleeAttackData.cooldown = GetFloat(recordID + ".cooldown");
    this.m_quickMeleeAttackData.duration = GetFloat(recordID + ".duration");
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    TweakDBID attackID;
    SendAnimFeature(stateContext, scriptInterface, 0);
    attackID = GetDesiredAttackRecord(stateContext, scriptInterface).GetID();
    GetWeaponObject(scriptInterface).SetAttack(attackID);
    StartCooldown(WeakRefToRef(scriptInterface.owner), "QuickMelee", this.m_quickMeleeAttackData.cooldown);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    UpdateAimAssist(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    TweakDBID attackID;
    SendAnimFeature(stateContext, scriptInterface, 0);
    attackID = GetDesiredAttackRecord(stateContext, scriptInterface).GetID();
    GetWeaponObject(scriptInterface).SetAttack(attackID);
    StartCooldown(WeakRefToRef(scriptInterface.owner), "QuickMelee", this.m_quickMeleeAttackData.cooldown);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    UpdateAimAssist(stateContext, scriptInterface);
  }

  protected final void SendAnimFeature(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Int32 state) {
    ref<AnimFeature_QuickMelee> animFeature;
    animFeature = new AnimFeature_QuickMelee();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature("QuickMelee", animFeature);
  }

  protected final TweakDBID GetQuickMeleeAttackTweakID(ref<StateGameScriptInterface> scriptInterface) {
    array<ref<IAttack>> attacks;
    Int32 i;
    ref<Attack_Record> record;
    String attackName;
    TweakDBID iD;
    attacks = GetWeaponObject(scriptInterface).GetAttacks();
    i = 0;
    while(i < Size(attacks)) {
      record = WeakRefToRef(attacks[i].GetRecord());
      attackName = WeakRefToRef(record.AttackType()).Name();
      if(attackName == "QuickMelee") {
        return record.GetID();
      };
      i += 1;
    };
    return undefined();
  }
}

public class NoAmmoDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isSwitchingItems;
    isSwitchingItems = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.SwitchItems);
    return !isSwitchingItems && IsMagazineEmpty(GetWeaponObject(scriptInterface));
  }

  protected final const Bool ToReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsMagazineEmpty(GetWeaponObject(scriptInterface));
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustTapped("QuickMelee")) {
      return true;
    };
    if(!IsMagazineEmpty(GetWeaponObject(scriptInterface)) || !CanReload(GetWeaponObject(scriptInterface))) {
      return false;
    };
    return true;
  }

  protected final const Bool ToReload(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<WeaponItem_Record> weaponRecord;
    weaponObject = GetWeaponObject(scriptInterface);
    weaponRecord = GetWeaponItemRecord(GetTDBID(weaponObject.GetItemID()));
    if(!CanReload(GetWeaponObject(scriptInterface))) {
      return false;
    };
    if(IsServer()) {
      return ServerHasReloadRequest(stateContext, scriptInterface);
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSprinting) && GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Sprint)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileVaulting) && IsInLocomotionState(stateContext, "vault")) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSliding) && IsInLocomotionState(stateContext, "slide")) {
      return false;
    };
    if(scriptInterface.IsActionJustPressed("RangedAttack")) {
      return true;
    };
    if(WeakRefToRef(weaponRecord.ItemType()).Type() == gamedataItemType.Wea_SniperRifle) {
      return !IsInUpperBodyState(stateContext, "aimingState");
    };
    return GetInStateTime(stateContext, scriptInterface) > GetStaticFloatParameter("timeToAutoReload", 0.30000001192092896);
  }

  protected final const Bool ToPublicSafe(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !CanReload(GetWeaponObject(scriptInterface));
  }
}

public class NoAmmoEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponItem_Record> weaponRecord;
    weaponRecord = GetWeaponItemRecord(GetTDBID(GetWeaponObject(scriptInterface).GetItemID()));
    scriptInterface.TEMP_WeaponStopFiring();
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.NoAmmo));
    scriptInterface.SetAnimationParameterFloat("empty_clip", 1);
    OnEnterNonChargeState(stateContext, scriptInterface);
    ShowAttackPreview(false, scriptInterface, stateContext);
    GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).RequestSettingStatPoolMinValue(Cast(GetWeaponObject(scriptInterface).GetEntityID()), gamedataStatPoolType.WeaponCharge, scriptInterface.executionOwner);
    if(WeakRefToRef(GetWeaponObject(scriptInterface).GetItemData()).HasTag("DiscardOnEmpty") || WeakRefToRef(weaponRecord.EquipArea()).Type() == gamedataEquipmentArea.WeaponHeavy && !IsInUpperBodyState(stateContext, "temporaryUnequip")) {
      SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.CycleNextWeaponWheelItem, gameEquipAnimationType.Default);
    };
    if(!HasAvailableAmmo(Cast(WeakRefToRef(scriptInterface.owner)))) {
      PlayCpoServerSyncVoiceOver(WeakRefToRef(scriptInterface.executionOwner), "cpo_out_of_ammo");
    };
    EnterNoAmmo(scriptInterface.executionOwner, RefToWeakRef(Cast(WeakRefToRef(scriptInterface.owner))));
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AudioEvent> dryFireEvent;
    if(scriptInterface.IsActionJustPressed("RangedAttack")) {
      dryFireEvent = new AudioEvent();
      dryFireEvent.eventName = "dry_fire";
      GetWeaponObject(scriptInterface).QueueEvent(dryFireEvent);
    };
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ExitNoAmmo(scriptInterface.executionOwner, RefToWeakRef(Cast(WeakRefToRef(scriptInterface.owner))));
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
  }
}

public class ReloadDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Vision) != ToInt(gamePSMVision.Default)) {
      return false;
    };
    if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.CombatGadget) == ToInt(gamePSMCombatGadget.Charging)) {
      return false;
    };
    if(GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.AimForced)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSliding) && IsInSlidingState(stateContext)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileVaulting) && IsInLocomotionState(stateContext, "vault")) {
      return false;
    };
    if(IsServer()) {
      return ServerHasReloadRequest(stateContext, scriptInterface);
    };
    if(stateContext.IsStateMachineActive("Consumable")) {
      return false;
    };
    return ShouldReloadWeapon(stateContext, scriptInterface);
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsReloadDurationComplete(stateContext, scriptInterface)) {
      if(GetParameterBool("FinishedReload", stateContext)) {
        return true;
      };
    };
    if(GetParameterBool("InterruptReload", stateContext) && !IsReloadUninterruptible(stateContext, scriptInterface)) {
      return true;
    };
    if(scriptInterface.IsActionJustPressed("RangedAttack") && Cast(GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), gamedataStatType.ReloadAmount)) > 0) {
      if(!IsMagazineEmpty(GetWeaponObject(scriptInterface)) && !IsReloadUninterruptible(stateContext, scriptInterface)) {
        return true;
      };
    };
    if(scriptInterface.IsActionJustPressed("QuickMelee") && !IsCooldownActive(WeakRefToRef(scriptInterface.owner), "QuickMelee")) {
      return true;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileVaulting) && IsInLocomotionState(stateContext, "vault")) {
      return true;
    };
    return false;
  }

  protected final const Bool ToReload(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsReloadDurationComplete(stateContext, scriptInterface)) {
      return false;
    };
    if(scriptInterface.GetActionValue("AttackB") > 0) {
      return false;
    };
    return CanReload(GetWeaponObject(scriptInterface));
  }
}

public class ReloadEvents extends WeaponEventsTransition {

  public ref<AnimFeature_SelectRandomAnimSync> m_randomSync;

  public Bool m_sprintingLastUpdate;

  public Bool m_uninteruptibleSet;

  protected final void SendReloadAnimData(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Bool isActive) {
    ref<AnimFeature_WeaponReload> reloadData;
    ref<AnimFeature_WeaponReloadSpeedData> reloadSpeed;
    wref<GameObject> owner;
    EntityID ownerID;
    ref<StatsSystem> statsSystem;
    owner = scriptInterface.owner;
    ownerID = WeakRefToRef(owner).GetEntityID();
    statsSystem = GetStatsSystem(WeakRefToRef(owner).GetGame());
    reloadSpeed = new AnimFeature_WeaponReloadSpeedData();
    reloadData = new AnimFeature_WeaponReload();
    reloadData.amountToReload = Cast(statsSystem.GetStatValue(Cast(ownerID), gamedataStatType.ReloadAmount));
    reloadData.emptyReload = GetParameterBool("EmptyReload", stateContext, true);
    reloadData.continueLoop = GetParameterBool("ContinueReload", stateContext, true);
    reloadData.loopDuration = statsSystem.GetStatValue(Cast(ownerID), gamedataStatType.ReloadTime);
    reloadData.emptyDuration = statsSystem.GetStatValue(Cast(ownerID), gamedataStatType.EmptyReloadTime);
    scriptInterface.SetAnimationParameterBool("is_reload_active", isActive);
    reloadSpeed.reloadSpeed = GetReloadAnimSpeed(scriptInterface, gamedataStatType.ReloadTime);
    reloadSpeed.emptyReloadSpeed = GetReloadAnimSpeed(scriptInterface, gamedataStatType.EmptyReloadTime);
    scriptInterface.SetAnimationParameterFeature("ReloadData", reloadData);
    scriptInterface.SetAnimationParameterFeature("ReloadSpeed", reloadSpeed);
  }

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    Float reloadLogicalDuration;
    weapon = GetWeaponObject(scriptInterface);
    ShowAttackPreview(false, scriptInterface, stateContext);
    this.m_uninteruptibleSet = false;
    SetUninteruptibleReloadParams(stateContext, true);
    if(ShouldUseAutoloader(scriptInterface)) {
      reloadLogicalDuration = weapon.StartReload(GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), gamedataStatType.ReloadTime));
    } else {
      reloadLogicalDuration = weapon.StartReload();
    };
    stateContext.SetPermanentFloatParameter("ReloadLogicalDuration", reloadLogicalDuration, true);
    OnEnterNonChargeState(stateContext, scriptInterface);
    EndShootingSequence(stateContext, scriptInterface);
    weapon = GetWeaponObject(scriptInterface);
    scriptInterface.TEMP_WeaponStopFiring();
    stateContext.SetConditionBoolParameter("RealodInputPressed", false, true);
    stateContext.SetTemporaryBoolParameter("InterruptAiming", true, true);
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSprinting)) {
      stateContext.SetTemporaryBoolParameter("InterruptSprint", true, true);
    };
    this.m_sprintingLastUpdate = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Sprint);
    if(!ToBool(this.m_randomSync)) {
      this.m_randomSync = new AnimFeature_SelectRandomAnimSync();
      this.m_randomSync.value = -1;
    };
    if(WeakRefToRef(scriptInterface.executionOwner).IsControlledByAnotherClient()) {
      this.m_randomSync.value = 0;
    } else {
      this.m_randomSync.value = RandDifferent(this.m_randomSync.value, 3);
    };
    if(IsMagazineEmpty(weapon) && !ShouldUseAutoloader(scriptInterface)) {
      stateContext.SetPermanentBoolParameter("EmptyReload", true, true);
    } else {
      stateContext.SetPermanentBoolParameter("EmptyReload", false, true);
    };
    if(ToBool(this.m_randomSync)) {
      scriptInterface.SetAnimationParameterFeature("RandomSync", this.m_randomSync);
    };
    if(!GetParameterBool("ContinueReload", stateContext, true)) {
      scriptInterface.PushAnimationEvent("Reload");
      TryPlayReloadChatter(WeakRefToRef(scriptInterface.executionOwner));
    } else {
      scriptInterface.PushAnimationEvent("ReloadLoop");
    };
    SendReloadAnimData(stateContext, scriptInterface, true);
    EnterReload(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Reload));
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isSprinting;
    Bool interruptReload;
    StateResultFloat reloadTimeLeft;
    StateResultFloat logicalDuration;
    this.m_sprintingLastUpdate = isSprinting;
    if(!GetParameterBool("FinishedReload", stateContext)) {
      logicalDuration = stateContext.GetPermanentFloatParameter("ReloadLogicalDuration");
      if(logicalDuration.valid) {
        if(GetInStateTime(stateContext, scriptInterface) > logicalDuration.value) {
          GetWeaponObject(scriptInterface).StopReload(gameweaponReloadStatus.Standard);
          stateContext.SetTemporaryBoolParameter("FinishedReload", true, true);
        };
      };
    };
    if(IsReloadUninterruptible(stateContext, scriptInterface) && !this.m_uninteruptibleSet) {
      SetUninteruptibleReloadParams(stateContext, false);
      this.m_uninteruptibleSet = true;
    };
    if(stateContext.IsStateMachineActive("Consumable") || stateContext.IsStateMachineActive("CombatGadget")) {
      interruptReload = true;
    };
    if(interruptReload) {
      scriptInterface.PushAnimationEvent("EndReload");
      stateContext.SetTemporaryBoolParameter("InterruptReload", true, true);
    };
  }

  protected final Bool ShouldUseAutoloader(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = GetWeaponObject(scriptInterface);
    return HasStatFlag(scriptInterface, gamedataStatType.WeaponHasAutoloader, weapon);
  }

  protected void OnExitToReload(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetPermanentBoolParameter("ContinueReload", true, true);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = GetWeaponObject(scriptInterface);
    stateContext.SetPermanentFloatParameter("LastReloadTime", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())), true);
    scriptInterface.SetAnimationParameterFloat("empty_clip", 0);
    scriptInterface.PushAnimationEvent("EndReload");
    stateContext.SetPermanentBoolParameter("ContinueReload", false, true);
    SendReloadAnimData(stateContext, scriptInterface, false);
    ExitReload(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
    SetUninteruptibleReloadParams(stateContext, true);
    this.m_uninteruptibleSet = false;
    if(!GetParameterBool("FinishedReload", stateContext)) {
      scriptInterface.PushAnimationEvent("InterruptReload");
      weapon.StopReload(gameweaponReloadStatus.Interrupted);
    };
  }
}

public class ShootDecisions extends WeaponTransition {

  public const Bool stateBodyDone;

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }
}

public class ShootEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    TweakDBID attackID;
    GameInstance gameInstance;
    ref<StatsSystem> statsSystem;
    Float currentTime;
    ref<StimBroadcasterComponent> broadcaster;
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    statsSystem = GetStatsSystem(gameInstance);
    currentTime = ToFloat(GetSimTime(gameInstance));
    weapon = GetWeaponObject(scriptInterface);
    ForceDisableVisionMode(stateContext);
    ShowAttackPreview(true, scriptInterface, stateContext);
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponShootWhileSprinting) && GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Sprint)) {
      stateContext.SetTemporaryBoolParameter("InterruptSprint", true, true);
    };
    stateContext.SetPermanentFloatParameter("LastShotTime", currentTime, true);
    attackID = GetDesiredAttackRecord(stateContext, scriptInterface).GetID();
    GetWeaponObject(scriptInterface).SetAttack(attackID);
    GetWeaponObject(scriptInterface).QueueEvent(new WeaponPreFireEvent());
    stateContext.SetPermanentIntParameter("LastChargePressCount", Cast(scriptInterface.GetActionPressCount("RangedAttack")), true);
    stateContext.SetPermanentFloatParameter("TurnOffPublicSafeTimeStamp", currentTime, true);
    stateContext.SetPermanentBoolParameter("WeaponInSafe", false, true);
    scriptInterface.PushAnimationEvent("Shoot");
    stateContext.SetPermanentFloatParameter("StoppedFiringTimestamp", currentTime, true);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Shoot));
    broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
    if(statsSystem.GetStatValue(Cast(GetWeaponObject(scriptInterface).GetEntityID()), gamedataStatType.CanSilentKill) > 0) {
      if(ToBool(broadcaster)) {
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.IllegalAction);
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.SilencedGunshot);
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.SilencedGunshot, 8, true);
        if(GetWeaponType(weapon.GetItemID()) == gamedataItemType.Wea_SniperRifle) {
          broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.Gunshot, 8);
        };
      };
      SilencedShoot(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
    } else {
      Shoot(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
      if(ToBool(broadcaster)) {
        if(IsEntityInInteriorArea(RefToWeakRef(GetPlayer(gameInstance)))) {
          broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.Gunshot, 25);
          broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.Gunshot, 45, true);
        } else {
          broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.Gunshot, GetPlayer(gameInstance).GetGunshotRange());
          broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.Gunshot, 50, true);
        };
      };
      BlockCoverVisibilityReduction(scriptInterface.executionOwner);
    };
    SendDataTrackingRequest(scriptInterface, ETelemetryData.RangedAttacksMade, 1);
    ChangeStatPoolValue(scriptInterface, WeakRefToRef(scriptInterface.owner).GetEntityID(), gamedataStatPoolType.WeaponOverheat, GetStaticFloatParameter("overheatValAdd", 10));
    ShootingSequencePostShoot(stateContext);
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<StatPoolsSystem> statPoolsSystem;
    ref<StatsSystem> statsSystem;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponObject = GetWeaponObject(scriptInterface);
    if(statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.FullAutoOnFullCharge) == 0) {
      statPoolsSystem.RequestSettingStatPoolMinValue(Cast(weaponObject.GetEntityID()), gamedataStatPoolType.WeaponCharge, scriptInterface.executionOwner);
    };
  }
}

public class CycleRoundDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    weaponObject = GetWeaponObject(scriptInterface);
    if(IsMagazineEmpty(weaponObject)) {
      return true;
    };
    return CanPerformNextShotInSequence(weaponObject, stateContext, scriptInterface);
  }
}

public class CycleRoundEvents extends WeaponEventsTransition {

  public Bool m_hasBlockedAiming;

  public Float m_blockAimStart;

  public Float m_blockAimDuration;

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = GetWeaponObject(scriptInterface);
    this.m_hasBlockedAiming = false;
    this.m_blockAimStart = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.CycleTimeAimBlockStart);
    this.m_blockAimDuration = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.CycleTimeAimBlockDuration);
    if(this.m_blockAimDuration > 0 && this.m_blockAimStart > 0) {
      if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim)) {
        HoldAimingForTime(stateContext, scriptInterface, this.m_blockAimStart);
      };
    };
    if(WeakRefToRef(GetWeaponObject(scriptInterface).GetItemData()).HasTag("AnimCycleRound")) {
      scriptInterface.PushAnimationEvent("CycleRound");
    };
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Shoot));
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(this.m_hasBlockedAiming) {
      ResetSoftBlockAiming(stateContext, scriptInterface);
    };
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ShowAttackPreview(true, scriptInterface, stateContext);
    if(this.m_blockAimDuration > 0) {
      if(!this.m_hasBlockedAiming && this.m_blockAimStart < GetInStateTime(stateContext, scriptInterface)) {
        BlockAimingForTime(stateContext, scriptInterface, this.m_blockAimDuration);
        this.m_hasBlockedAiming = true;
      };
    };
    ShootingSequenceUpdateCycleTime(timeDelta, stateContext);
    UpdateInputBuffer(stateContext, scriptInterface);
    if(scriptInterface.IsTriggerModeActive(gamedataTriggerMode.FullAuto)) {
      if(scriptInterface.GetActionValue("RangedAttack") < 0 || IsMagazineEmpty(GetWeaponObject(scriptInterface))) {
        scriptInterface.TEMP_WeaponStopFiring();
      };
    };
  }
}

public class CycleTriggerModeDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(HasSecondaryTriggerMode(scriptInterface)) {
      if(IsInUpperBodyState(stateContext, "aimingState") && IsPrimaryTriggerModeActive(scriptInterface)) {
        return true;
      };
      if(!IsInUpperBodyState(stateContext, "aimingState") && !IsPrimaryTriggerModeActive(scriptInterface)) {
        return true;
      };
    };
    return false;
  }

  protected final const Bool ToReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime(stateContext, scriptInterface) > GetConditionParameterFloat("CycleTriggerModeStatCycleTime", stateContext);
  }

  protected final const Bool ToCharge(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    if(!scriptInterface.IsTriggerModeActive(gamedataTriggerMode.Charge)) {
      return false;
    };
    if(GetInStateTime(stateContext, scriptInterface) < GetConditionParameterFloat("CycleTriggerModeStatCycleTime", stateContext)) {
      return false;
    };
    weapon = GetWeaponObject(scriptInterface);
    return !IsMagazineEmpty(weapon) && GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).HasStatPoolValueReachedMin(Cast(weapon.GetEntityID()), gamedataStatPoolType.WeaponCharge) && scriptInterface.GetActionValue("RangedAttack") > 0;
  }
}

public class CycleTriggerModeEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_TriggerModeChange> animFeature;
    StatsObjectID ownerID;
    Float statCycleTime;
    ownerID = Cast(WeakRefToRef(scriptInterface.owner).GetEntityID());
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.ToggleFireMode)) {
      SetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.ToggleFireMode, false);
    };
    SwitchTriggerMode(stateContext, scriptInterface);
    statCycleTime = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(ownerID, gamedataStatType.CycleTriggerModeTime);
    stateContext.SetConditionFloatParameter("CycleTriggerModeStatCycleTime", statCycleTime, true);
    animFeature = new AnimFeature_TriggerModeChange();
    animFeature.cycleTime = statCycleTime;
    scriptInterface.SetAnimationParameterFeature("TriggerModeChange", animFeature);
    scriptInterface.PushAnimationEvent("SwitchFiremode");
  }

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    UpdateInputBuffer(stateContext, scriptInterface);
  }
}

public class SemiAutoDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    weaponObject = GetWeaponObject(scriptInterface);
    return IsSemiAutoAction(weaponObject, stateContext, scriptInterface) && CanPerformNextSemiAutoShot(weaponObject, stateContext, scriptInterface);
  }

  protected final const Bool ToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isFireDelay;
    Float timeRemaining;
    isFireDelay = GetParameterBool(GetIsDelayFireName(), stateContext, true);
    timeRemaining = GetParameterFloat(GetCycleTimeRemainingName(), stateContext, true);
    return !isFireDelay || timeRemaining < 0;
  }
}

public class SemiAutoEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<StatsSystem> statsSystem;
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponObject = GetWeaponObject(scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Shoot));
    SetupStandardShootingSequence(stateContext, scriptInterface);
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ShootingSequenceUpdateCycleTime(timeDelta, stateContext);
  }
}

public class FullAutoDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    weaponObject = GetWeaponObject(scriptInterface);
    return IsFullAutoAction(weaponObject, stateContext, scriptInterface) && CanPerformNextFullAutoShot(weaponObject, stateContext, scriptInterface);
  }

  protected final const Bool ToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    Bool isFireDelay;
    Float timeRemaining;
    Bool validAmmo;
    weapon = GetWeaponObject(scriptInterface);
    isFireDelay = GetParameterBool(GetIsDelayFireName(), stateContext, true);
    timeRemaining = GetParameterFloat(GetCycleTimeRemainingName(), stateContext, true);
    validAmmo = !IsMagazineEmpty(weapon);
    return validAmmo && !isFireDelay || timeRemaining < 0;
  }

  protected final const Bool ToReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isFireDelay;
    Float timeRemaining;
    isFireDelay = GetParameterBool(GetIsDelayFireName(), stateContext, true);
    timeRemaining = GetParameterFloat(GetCycleTimeRemainingName(), stateContext, true);
    return !isFireDelay || timeRemaining < 0 && scriptInterface.GetActionValue("RangedAttack") < 0;
  }
}

public class FullAutoEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    gamedataStatType burstCycleTimeStat;
    gamedataStatType burstNumShots;
    Float cycleTimeForShootingPhase;
    ref<StatsSystem> statsSystem;
    burstCycleTimeStat = gamedataStatType.CycleTime_Burst;
    burstNumShots = gamedataStatType.NumShotsInBurst;
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponObject = GetWeaponObject(scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Shoot));
    if(!InShootingSequence(stateContext)) {
      SetupStandardShootingSequence(stateContext, scriptInterface);
    } else {
      cycleTimeForShootingPhase = CalculateCycleTime(stateContext, scriptInterface);
      SetupNextShootingPhase(stateContext, cycleTimeForShootingPhase, statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.CycleTime_Burst), Cast(statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.NumShotsInBurst)));
    };
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ShootingSequenceUpdateCycleTime(timeDelta, stateContext);
    ShowAttackPreview(true, scriptInterface, stateContext);
  }

  private final Float CalculateCycleTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float lerp;
    Float statMultiplier;
    Float statPeriod;
    Float finalMultiplier;
    Float cycleTimeStart;
    GameInstance gameInstance;
    Float currentTime;
    ref<StatsSystem> statsSystem;
    ref<WeaponObject> weaponObject;
    Float shootingSequenceStartTime;
    finalMultiplier = 1;
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    currentTime = ToFloat(GetSimTime(gameInstance));
    statsSystem = GetStatsSystem(gameInstance);
    weaponObject = GetWeaponObject(scriptInterface);
    shootingSequenceStartTime = GetParameterFloat(GetShootingStartName(), stateContext, true);
    cycleTimeStart = statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.CycleTime);
    statMultiplier = statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.CycleTimeShootingMult);
    statPeriod = statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.CycleTimeShootingMultPeriod);
    if(statMultiplier != 0) {
      if(statPeriod > 0) {
        lerp = LerpF(currentTime - shootingSequenceStartTime / statPeriod, 0, 1, true);
      } else {
        lerp = 1;
      };
      finalMultiplier = 1 + lerp * statMultiplier;
    };
    return cycleTimeStart * finalMultiplier;
  }
}

public class BurstDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    Bool isBursting;
    Int32 burstShotRemaining;
    weaponObject = GetWeaponObject(scriptInterface);
    burstShotRemaining = GetParameterInt(GetBurstShotsRemainingName(), stateContext, true);
    isBursting = burstShotRemaining >= 1;
    return isBursting && !IsMagazineEmpty(GetWeaponObject(scriptInterface));
  }

  protected final const Bool ToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool validTime;
    Bool validAmmo;
    Float timeRemaining;
    timeRemaining = GetParameterFloat(GetBurstTimeRemainingName(), stateContext, true);
    validTime = timeRemaining < 0;
    validAmmo = !IsMagazineEmpty(GetWeaponObject(scriptInterface));
    return validTime && validAmmo;
  }
}

public class BurstEvents extends WeaponEventsTransition {

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ShootingSequenceUpdateBurstTime(timeDelta, stateContext);
    ShowAttackPreview(true, scriptInterface, stateContext);
  }
}

public abstract class ChargeEventsAbstract extends WeaponEventsTransition {

  protected Uint32 m_layerId;

  protected final void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Default));
    stateContext.SetPermanentBoolParameter("WeaponStopChargeRequested", true, true);
    StopEffect("charging", scriptInterface);
    StopEffect("charged", scriptInterface);
    ClearDebugText(this.m_layerId, scriptInterface);
  }

  protected void OnExitToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ShowAttackPreview(true, scriptInterface, stateContext);
  }

  protected final void SetupFullChargedShootingSequence(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weaponObject;
    ref<StatsSystem> statsSystem;
    weaponObject = GetWeaponObject(scriptInterface);
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    StartShootingSequence(stateContext, scriptInterface, 0, statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.CycleTime_BurstMaxCharge), Cast(statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.NumShotsInBurstMaxCharge)), Cast(statsSystem.GetStatValue(Cast(weaponObject.GetEntityID()), gamedataStatType.FullAutoOnFullCharge)));
  }
}

public class ChargeDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    Uint32 actionPressCount;
    StateResultInt lastChargePressCount;
    if(!scriptInterface.IsTriggerModeActive(gamedataTriggerMode.Charge)) {
      return false;
    };
    if(scriptInterface.GetActionValue("RangedAttack") < 0.5) {
      return false;
    };
    actionPressCount = scriptInterface.GetActionPressCount("RangedAttack");
    lastChargePressCount = stateContext.GetPermanentIntParameter("LastChargePressCount");
    if(lastChargePressCount.valid && lastChargePressCount.value == Cast(actionPressCount)) {
      return false;
    };
    weapon = GetWeaponObject(scriptInterface);
    return !IsMagazineEmpty(weapon) && GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).HasStatPoolValueReachedMin(Cast(weapon.GetEntityID()), gamedataStatPoolType.WeaponCharge);
  }

  protected final const Bool ToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float chargeParameter;
    Float readyPercentageParameter;
    Bool fireWhenReadyParameter;
    GameInstance gameInstance;
    ref<StatPoolsSystem> statPoolsSystem;
    ref<StatsSystem> statsSystem;
    ref<WeaponObject> weapon;
    EntityID weaponID;
    weapon = GetWeaponObject(scriptInterface);
    weaponID = weapon.GetEntityID();
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    statPoolsSystem = GetStatPoolsSystem(gameInstance);
    statsSystem = GetStatsSystem(gameInstance);
    weaponID = weapon.GetEntityID();
    chargeParameter = GetWeaponChargeNormalized(RefToWeakRef(weapon));
    readyPercentageParameter = statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.ChargeReadyPercentage);
    fireWhenReadyParameter = statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.ChargeShouldFireWhenReady) > 0;
    if(fireWhenReadyParameter && chargeParameter >= readyPercentageParameter || scriptInterface.GetActionValue("RangedAttack") < 0) {
      return true;
    };
    return false;
  }

  protected final const Bool ToChargeReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float chargeParameter;
    Float readyPercentageParameter;
    Bool fireWhenReadyParameter;
    GameInstance gameInstance;
    ref<StatPoolsSystem> statPoolsSystem;
    ref<StatsSystem> statsSystem;
    ref<WeaponObject> weapon;
    EntityID weaponID;
    weapon = GetWeaponObject(scriptInterface);
    weaponID = weapon.GetEntityID();
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    statPoolsSystem = GetStatPoolsSystem(gameInstance);
    statsSystem = GetStatsSystem(gameInstance);
    chargeParameter = GetWeaponChargeNormalized(RefToWeakRef(weapon));
    readyPercentageParameter = statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.ChargeReadyPercentage);
    fireWhenReadyParameter = statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.ChargeShouldFireWhenReady) > 0;
    if(!fireWhenReadyParameter && chargeParameter >= readyPercentageParameter && scriptInterface.GetActionValue("RangedAttack") > 0) {
      return true;
    };
    return false;
  }
}

public class ChargeEvents extends ChargeEventsAbstract {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Uint32 actionPressCount;
    ref<WeaponObject> weapon;
    EntityID weaponID;
    Float maxCharge;
    weapon = GetWeaponObject(scriptInterface);
    weaponID = weapon.GetEntityID();
    maxCharge = GetMaxChargeThreshold(scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Charging));
    actionPressCount = scriptInterface.GetActionPressCount("RangedAttack");
    stateContext.SetPermanentIntParameter("LastChargePressCount", Cast(actionPressCount), true);
    PlayEffect("charging", scriptInterface);
    weapon.SetMaxChargeThreshold(maxCharge);
    StartPool(GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame()), weaponID, gamedataStatPoolType.WeaponCharge, maxCharge, GetChargeValuePerSec(scriptInterface));
    EnterCharge(scriptInterface.executionOwner, RefToWeakRef(weapon));
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Charging));
    GetWeaponObject(scriptInterface).GetSharedData().SetVariant(GetAllBlackboardDefs().Weapon.ChargeStep, ToVariant(gamedataChargeStep.Charging));
  }

  protected void OnExitToChargeReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ClearDebugText(this.m_layerId, scriptInterface);
  }

  protected final Float GetChargeValuePerSec(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    Float chargeDuration;
    ref<StatsSystem> statsSystem;
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    if(!ToBool(statsSystem)) {
      return -1;
    };
    weapon = GetWeaponObject(scriptInterface);
    if(!ToBool(weapon)) {
      return -1;
    };
    chargeDuration = statsSystem.GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.ChargeTime);
    if(chargeDuration < 0) {
      return -1;
    };
    return 100 / chargeDuration;
  }

  protected void OnExitToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetupStandardShootingSequence(stateContext, scriptInterface);
    OnExitToShoot(stateContext, scriptInterface);
  }
}

public class ChargeReadyDecisions extends WeaponTransition {

  protected final const Bool ToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.GetActionValue("RangedAttack") < 0;
  }

  protected final const Bool ToChargeMax(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StatPoolsSystem> statPoolsSystem;
    EntityID weaponID;
    Float statPoolPerc;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponID = GetWeaponObject(scriptInterface).GetEntityID();
    statPoolPerc = statPoolsSystem.GetStatPoolValue(Cast(weaponID), gamedataStatPoolType.WeaponCharge);
    if(statPoolPerc >= GetMaxChargeThreshold(scriptInterface)) {
      return true;
    };
    return false;
  }
}

public class ChargeReadyEvents extends ChargeEventsAbstract {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Charging));
    GetWeaponObject(scriptInterface).GetSharedData().SetVariant(GetAllBlackboardDefs().Weapon.ChargeStep, ToVariant(gamedataChargeStep.Charged));
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ShowAttackPreview(true, scriptInterface, stateContext);
  }

  protected void OnExitToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetupStandardShootingSequence(stateContext, scriptInterface);
    OnExitToShoot(stateContext, scriptInterface);
  }

  protected void OnExitToChargeMax(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StopEffect("charging", scriptInterface);
    ClearDebugText(this.m_layerId, scriptInterface);
  }
}

public class ChargeMaxDecisions extends WeaponTransition {

  protected final const Bool ToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float timeInStateMaxParameter;
    Float timeInState;
    ref<StatsSystem> statsSystem;
    EntityID weaponID;
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponID = GetWeaponObject(scriptInterface).GetEntityID();
    if(HasStatFlag(scriptInterface, gamedataStatType.CanControlFullyChargedWeapon) && statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.FullAutoOnFullCharge) == 0) {
      if(scriptInterface.GetActionValue("RangedAttack") < 0) {
        return true;
      };
      return false;
    };
    timeInStateMaxParameter = statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.ChargeMaxTimeInChargedState);
    timeInState = GetInStateTime(stateContext, scriptInterface);
    if(timeInState >= timeInStateMaxParameter || scriptInterface.GetActionValue("RangedAttack") < 0) {
      return true;
    };
    return false;
  }
}

public class ChargeMaxEvents extends ChargeEventsAbstract {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StatsSystem> statsSystem;
    ref<WeaponObject> weaponObject;
    statsSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponObject = GetWeaponObject(scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Charging));
    PlayEffect("charged", scriptInterface);
    GetWeaponObject(scriptInterface).GetSharedData().SetVariant(GetAllBlackboardDefs().Weapon.ChargeStep, ToVariant(gamedataChargeStep.Overcharging));
  }

  protected void OnExitToShoot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    PlayEffect("discharge", scriptInterface);
    SetupFullChargedShootingSequence(stateContext, scriptInterface);
    OnExitToShoot(stateContext, scriptInterface);
  }
}

public class DischargeDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float chargeParameter;
    Float readyPercentageParameter;
    GameInstance gameInstance;
    ref<StatPoolsSystem> statPoolsSystem;
    ref<StatsSystem> statsSystem;
    EntityID weaponID;
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    statPoolsSystem = GetStatPoolsSystem(gameInstance);
    statsSystem = GetStatsSystem(gameInstance);
    weaponID = GetWeaponObject(scriptInterface).GetEntityID();
    chargeParameter = statPoolsSystem.GetStatPoolValue(Cast(weaponID), gamedataStatPoolType.WeaponCharge);
    readyPercentageParameter = statsSystem.GetStatValue(Cast(weaponID), gamedataStatType.ChargeReadyPercentage);
    if(chargeParameter < readyPercentageParameter && scriptInterface.GetActionValue("RangedAttack") < 0) {
      return true;
    };
    return false;
  }

  protected final const Bool ToReady(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StatPoolsSystem> statPoolsSystem;
    EntityID weaponID;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weaponID = GetWeaponObject(scriptInterface).GetEntityID();
    return statPoolsSystem.HasStatPoolValueReachedMin(Cast(weaponID), gamedataStatPoolType.WeaponCharge);
  }
}

public class DischargeEvents extends WeaponEventsTransition {

  public Uint32 layerId;

  private ref<StatPoolsSystem> m_statPoolsSystem;

  private ref<StatsSystem> m_statsSystem;

  private EntityID m_weaponID;

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    GameInstance gameInstance;
    StatPoolModifier chargeMod;
    gameInstance = WeakRefToRef(scriptInterface.owner).GetGame();
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Charging));
    ShowDebugText("<<<DISCHARGING>>>", scriptInterface, this.layerId);
    this.m_statPoolsSystem = GetStatPoolsSystem(gameInstance);
    this.m_statsSystem = GetStatsSystem(gameInstance);
    this.m_weaponID = GetWeaponObject(scriptInterface).GetEntityID();
    StopEffect("charging", scriptInterface);
    StopEffect("charged", scriptInterface);
    this.m_statPoolsSystem.GetModifier(Cast(this.m_weaponID), gamedataStatPoolType.WeaponCharge, gameStatPoolModificationTypes.Decay, chargeMod);
    chargeMod.enabled = true;
    this.m_statPoolsSystem.RequestResetingModifier(Cast(this.m_weaponID), gamedataStatPoolType.WeaponCharge, gameStatPoolModificationTypes.Regeneration);
    this.m_statPoolsSystem.RequestSettingModifier(Cast(this.m_weaponID), gamedataStatPoolType.WeaponCharge, gameStatPoolModificationTypes.Decay, chargeMod);
    EnterDischarge(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
    GetWeaponObject(scriptInterface).GetSharedData().SetVariant(GetAllBlackboardDefs().Weapon.ChargeStep, ToVariant(gamedataChargeStep.Discharging));
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ClearDebugText(this.layerId, scriptInterface);
    ExitDischarge(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
  }
}

public class OverheatDecisions extends WeaponTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isOverheated;
    isOverheated = GetWeaponObject(scriptInterface).GetSharedData().GetBool(GetAllBlackboardDefs().Weapon.IsInForcedOverheatCooldown);
    return isOverheated;
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isOverheated;
    ref<BlackboardSystem> blackboardSystem;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    isOverheated = GetWeaponObject(scriptInterface).GetSharedData().GetBool(GetAllBlackboardDefs().Weapon.IsInForcedOverheatCooldown);
    if(!isOverheated) {
      return GetInStateTime(stateContext, scriptInterface) > GetStaticFloatParameter("overheatDuration", 5);
    };
    return false;
  }
}

public class OverheatEvents extends WeaponEventsTransition {

  protected final void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    scriptInterface.TEMP_WeaponStopFiring();
    stateContext.SetTemporaryBoolParameter("InterruptAiming", true, true);
    scriptInterface.PushAnimationEvent("Overheat");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon, ToInt(gamePSMRangedWeaponStates.Overheat));
    EnterOverheat(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ExitOverheat(scriptInterface.executionOwner, RefToWeakRef(GetWeaponObject(scriptInterface)));
  }
}
