
public abstract class ScenesFastForwardTransition extends DefaultTransition {

  protected final const void SetFastForwardAvailableBB(ref<StateGameScriptInterface> scriptInterface, Bool newVal) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    blackboard = blackboardSystem.Get(GetAllBlackboardDefs().UI_FastForward);
  }

  protected final const void SetFastForwardActiveBB(ref<StateGameScriptInterface> scriptInterface, Bool newVal) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    blackboard = blackboardSystem.Get(GetAllBlackboardDefs().UI_FastForward);
  }

  protected final const Bool GetDebugFFConditionParam(ref<StateContext> stateContext) {
    StateResultBool result;
    result = stateContext.GetConditionBoolParameter("debugFF");
    return result.valid && result.value;
  }

  protected final const void ActivateFastForward(ref<StateGameScriptInterface> scriptInterface, scnFastForwardMode mode) {
    GetSceneSystemInterface(scriptInterface).FastForwardingActivate(mode);
  }

  protected final const Bool IsFastForwardModeActive(ref<StateGameScriptInterface> scriptInterface, scnFastForwardMode mode) {
    return GetSceneSystemInterface(scriptInterface).IsFastForwardingActive(mode);
  }

  protected final const void DeActivateFastForward(ref<StateGameScriptInterface> scriptInterface) {
    GetSceneSystemInterface(scriptInterface).FastForwardingDeactivate();
  }

  protected final const Bool IsFastForwardAvailable(ref<StateGameScriptInterface> scriptInterface, scnFastForwardMode mode) {
    return GetSceneSystemInterface(scriptInterface).IsFastForwardingAllowed(mode);
  }

  protected final const Bool FastForwardInputValid(ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustReleased("SceneFastForward");
  }

  protected final const Bool DebugFastForwardInputValid(ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustReleased("FastForward");
  }

  protected final const void ProcessHoldInputFastForwardLock(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("HoldInputFastForwardLock", stateContext, true)) {
      if(scriptInterface.GetActionValue("ToggleCrouch") == 0) {
        stateContext.RemovePermanentBoolParameter("HoldInputFastForwardLock");
      };
    };
  }

  protected final const void StartGlitchFx(ref<StateGameScriptInterface> scriptInterface) {
    ref<worldEffectBlackboard> blackboard;
    blackboard = new worldEffectBlackboard();
    StartEffectEvent(WeakRefToRef(scriptInterface.executionOwner), "transition_glitch_loop", false, blackboard);
  }

  protected final const void StopGlitchFx(ref<StateGameScriptInterface> scriptInterface) {
    StopEffectEvent(WeakRefToRef(scriptInterface.executionOwner), "transition_glitch_loop");
  }

  protected final const Int32 GetFFSceneThrehsoldFromBraindanceSystem(ref<StateGameScriptInterface> scriptInterface) {
    ref<BraindanceSystem> bdSystem;
    bdSystem = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("BraindanceSystem"));
    return bdSystem.GetDebugFFSceneThreshold();
  }

  protected final const void DisplayFFButtonPrompt(ref<StateGameScriptInterface> scriptInterface) {
    ref<UpdateInputHintEvent> evt;
    InputHintData data;
    data.action = "SceneFastForward";
    data.source = "FastForward";
    data.localizedLabel = "LocKey#35482";
    data.holdIndicationType = GetFFButtonType(scriptInterface);
    evt = new UpdateInputHintEvent();
    evt.data = data;
    evt.show = true;
    evt.targetHintContainer = "GameplayInputHelper";
    GetUISystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).QueueEvent(evt);
  }

  protected final const inkInputHintHoldIndicationType GetFFButtonType(ref<StateGameScriptInterface> scriptInterface) {
    if(IsFastForwardByLine(scriptInterface)) {
      return inkInputHintHoldIndicationType.Press;
    };
    return inkInputHintHoldIndicationType.Hold;
  }

  protected final const void HideFFButtonPrompt(ref<StateGameScriptInterface> scriptInterface) {
    ref<UpdateInputHintEvent> evt;
    InputHintData data;
    data.action = "SceneFastForward";
    data.source = "FastForward";
    evt = new UpdateInputHintEvent();
    evt.data = data;
    evt.show = false;
    evt.targetHintContainer = "GameplayInputHelper";
    GetUISystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).QueueEvent(evt);
  }

  protected final const Bool IsLookingAtDialogueEntity(ref<StateGameScriptInterface> scriptInterface) {
    ref<HUDManager> hudManager;
    ref<ScriptedPuppet> npc;
    hudManager = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("HUDManager"));
    if(ToBool(hudManager)) {
      npc = Cast(FindEntityByID(WeakRefToRef(scriptInterface.executionOwner).GetGame(), hudManager.GetCurrentTargetID()));
      if(ToBool(npc) && npc != WeakRefToRef(scriptInterface.executionOwner)) {
        return GetSceneSystemInterface(scriptInterface).IsEntityInDialogue(hudManager.GetCurrentTargetID()) || GetSceneSystemInterface(scriptInterface).IsEntityInDialogue(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
      };
    };
    return false;
  }

  protected final const Bool IsBlockedByPhoneCallRestriction(ref<StateGameScriptInterface> scriptInterface) {
    if(IsFastForwardByLine(scriptInterface)) {
      return HasRestriction(scriptInterface.executionOwner, "PhoneCall");
    };
    return false;
  }

  protected final const Bool PhoneBBStateBlockingFF(ref<StateGameScriptInterface> scriptInterface) {
    ref<UI_ComDeviceDef> blackboardDef;
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    Variant infoVariant;
    PhoneCallInformation lastPhoneCallInformation;
    blackboardDef = GetAllBlackboardDefs().UI_ComDevice;
    blackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(blackboardDef);
    infoVariant = blackboard.GetVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneCallInformation);
    if(IsValid(infoVariant)) {
      lastPhoneCallInformation = FromVariant(infoVariant);
      return lastPhoneCallInformation.callPhase == questPhoneCallPhase.StartCall && GetSceneTier(scriptInterface) < 2;
    };
    return false;
  }
}

public class FastForwardUnavailableDecisions extends ScenesFastForwardTransition {

  protected final const Bool ToFastForwardAvailable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsPlayerInBraindance(scriptInterface) && !IsPlayerInCombat(scriptInterface) && GetSceneTier(scriptInterface) < 5 && !IsMountedToVehicle(scriptInterface) && !scriptInterface.IsEntityInCombat() && !IsBlockedByPhoneCallRestriction(scriptInterface) && !GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideElevator) && IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) || IsFastForwardAvailable(scriptInterface, scnFastForwardMode.GameplayReview);
  }
}

public class FastForwardUnavailableEvents extends ScenesFastForwardTransition {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    DeActivateFastForward(scriptInterface);
    HideFFButtonPrompt(scriptInterface);
    StopGlitchFx(scriptInterface);
    stateContext.RemovePermanentBoolParameter("FFRestriction");
    stateContext.RemovePermanentBoolParameter("FFCrouchLock");
    stateContext.RemovePermanentBoolParameter("FFhintActive");
    stateContext.RemovePermanentBoolParameter("HoldInputFastForwardLock");
    RemoveStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.FastForward");
    RemoveStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.FastForwardCrouchLock");
  }
}

public class FastForwardAvailableDecisions extends ScenesFastForwardTransition {

  protected final const Bool ToFastForwardUnavailable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsPlayerInBraindance(scriptInterface) || IsPlayerInCombat(scriptInterface) || GetSceneTier(scriptInterface) > 4 || scriptInterface.IsEntityInCombat() || IsMountedToVehicle(scriptInterface) || IsBlockedByPhoneCallRestriction(scriptInterface) || GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideElevator);
  }

  protected final const Bool ToFastForwardActive(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetSceneTier(scriptInterface) < 3) {
      if(IsFastForwardByLine(scriptInterface)) {
        if(IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && FastForwardInputValid(scriptInterface) && !GetParameterBool("FFRestriction", stateContext, true) && !GetParameterBool("HoldInputFastForwardLock", stateContext, true)) {
          return true;
        };
      } else {
        if(IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && GetParameterBool("TriggerFF", stateContext, true)) {
          return true;
        };
      };
    } else {
      if(GetSceneTier(scriptInterface) == 4 || GetSceneTier(scriptInterface) == 3) {
        if(IsFastForwardByLine(scriptInterface)) {
          if(IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && FastForwardInputValid(scriptInterface) && !GetParameterBool("FFRestriction", stateContext, true)) {
            return true;
          };
        } else {
          if(IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && GetParameterBool("TriggerFF", stateContext, true)) {
            return true;
          };
        };
      };
    };
    return false;
  }
}

public class FastForwardAvailableEvents extends ScenesFastForwardTransition {

  public Bool forceCloseFX;

  public Float delay;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsFastForwardModeActive(scriptInterface, scnFastForwardMode.GameplayReview) || IsFastForwardModeActive(scriptInterface, scnFastForwardMode.Default)) {
      DeActivateFastForward(scriptInterface);
    };
    HideFFButtonPrompt(scriptInterface);
    stateContext.RemovePermanentBoolParameter("FFhintActive");
    this.forceCloseFX = false;
    this.delay = 0;
  }

  public void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!this.forceCloseFX && GetInStateTime(stateContext, scriptInterface) > 0.30000001192092896 && !IsTimeDilationActive(stateContext, scriptInterface, "")) {
      StopGlitchFx(scriptInterface);
      this.forceCloseFX = true;
    };
    if(GetParameterBool("FFRestriction", stateContext, true) && !IsTimeDilationActive(stateContext, scriptInterface, "")) {
      this.delay += 0.10000000149011612;
      if(this.delay > 0.10000000149011612) {
        stateContext.SetPermanentBoolParameter("FFCrouchLock", true, true);
        stateContext.RemovePermanentBoolParameter("FFRestriction");
        RemoveStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.FastForward");
        if(IsFastForwardByLine(scriptInterface)) {
          ApplyStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.FastForwardCrouchLock");
        };
        this.delay = 0;
      };
    } else {
      if(GetParameterBool("FFRestriction", stateContext, true) && IsTimeDilationActive(stateContext, scriptInterface, "")) {
        timeDelta = 0;
      } else {
        if(GetParameterBool("FFCrouchLock", stateContext, true) && !HasStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.FastForwardCrouchLock")) {
          stateContext.RemovePermanentBoolParameter("FFCrouchLock");
        };
      };
    };
    if(!IsFastForwardByLine(scriptInterface)) {
      if(scriptInterface.IsActionJustHeld("SceneFastForward") && !GetParameterBool("TriggerFF", stateContext, true)) {
        stateContext.SetPermanentBoolParameter("TriggerFF", true, true);
        stateContext.SetPermanentBoolParameter("FFHoldLock", true, true);
      } else {
        if(GetParameterBool("TriggerFF", stateContext, true) && scriptInterface.GetActionValue("SceneFastForward") == 0) {
          stateContext.RemovePermanentBoolParameter("TriggerFF");
          stateContext.RemovePermanentBoolParameter("FFHoldLock");
        };
      };
    };
    if(GetSceneTier(scriptInterface) < 3) {
      if(IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && !IsFastForwardModeActive(scriptInterface, scnFastForwardMode.GameplayReview) && !GetParameterBool("FFhintActive", stateContext, true)) {
        stateContext.SetPermanentBoolParameter("FFhintActive", true, true);
        DisplayFFButtonPrompt(scriptInterface);
      } else {
        if(!IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) || IsFastForwardModeActive(scriptInterface, scnFastForwardMode.GameplayReview) && GetParameterBool("FFhintActive", stateContext, true)) {
          HideFFButtonPrompt(scriptInterface);
          stateContext.RemovePermanentBoolParameter("FFhintActive");
        };
      };
    } else {
      if(GetSceneTier(scriptInterface) == 4 || GetSceneTier(scriptInterface) == 3) {
        if(IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && !IsFastForwardModeActive(scriptInterface, scnFastForwardMode.GameplayReview) && !GetParameterBool("FFhintActive", stateContext, true)) {
          stateContext.SetPermanentBoolParameter("FFhintActive", true, true);
          DisplayFFButtonPrompt(scriptInterface);
        } else {
          if(!IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) || IsFastForwardModeActive(scriptInterface, scnFastForwardMode.GameplayReview) && GetParameterBool("FFhintActive", stateContext, true)) {
            HideFFButtonPrompt(scriptInterface);
            stateContext.RemovePermanentBoolParameter("FFhintActive");
          };
        };
      } else {
        if(GetSceneTier(scriptInterface) == 1 || !IsFastForwardAvailable(scriptInterface, scnFastForwardMode.Default) && GetParameterBool("FFhintActive", stateContext, true)) {
          HideFFButtonPrompt(scriptInterface);
          stateContext.RemovePermanentBoolParameter("FFhintActive");
        };
      };
    };
    if(IsFastForwardByLine(scriptInterface)) {
      ProcessHoldInputFastForwardLock(stateContext, scriptInterface);
    };
  }
}

public class FastForwardActiveDecisions extends ScenesFastForwardTransition {

  protected final const Bool ToFastForwardAvailable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsFastForwardModeActive(scriptInterface, scnFastForwardMode.Default) && GetInStateTime(stateContext, scriptInterface) > 0.10000000149011612;
  }

  protected final const Bool ToFastForwardUnavailable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsPlayerInBraindance(scriptInterface) || IsPlayerInCombat(scriptInterface) || scriptInterface.IsEntityInCombat() || IsMountedToVehicle(scriptInterface) || IsBlockedByPhoneCallRestriction(scriptInterface) || GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideElevator);
  }
}

public class FastForwardActiveEvents extends ScenesFastForwardTransition {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.RemovePermanentBoolParameter("FFhintActive");
    stateContext.RemovePermanentBoolParameter("HoldInputFastForwardLock");
    stateContext.SetPermanentBoolParameter("FFRestriction", true, true);
    ApplyStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.FastForward");
    if(GetDebugFFConditionParam(stateContext)) {
      ActivateFastForward(scriptInterface, scnFastForwardMode.GameplayReview);
    } else {
      ActivateFastForward(scriptInterface, scnFastForwardMode.Default);
    };
    HideFFButtonPrompt(scriptInterface);
    StartGlitchFx(scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetDebugFFConditionParam(stateContext)) {
      stateContext.RemoveConditionBoolParameter("debugFF");
    };
  }
}

public static exec void SetDebugSceneTierThreshold2(GameInstance gameInstance) {
  ref<SetDebugSceneThrehsold> request;
  ref<BraindanceSystem> bdSystem;
  request = new SetDebugSceneThrehsold();
  request.newThreshold = 2;
  bdSystem = Cast(GetScriptableSystemsContainer(gameInstance).Get("BraindanceSystem"));
  bdSystem.QueueRequest(request);
}

public static exec void SetDebugSceneTierThreshold1(GameInstance gameInstance) {
  ref<SetDebugSceneThrehsold> request;
  ref<BraindanceSystem> bdSystem;
  request = new SetDebugSceneThrehsold();
  request.newThreshold = 1;
  bdSystem = Cast(GetScriptableSystemsContainer(gameInstance).Get("BraindanceSystem"));
  bdSystem.QueueRequest(request);
}

public static exec void SetDebugSceneTierThreshold3(GameInstance gameInstance) {
  ref<SetDebugSceneThrehsold> request;
  ref<BraindanceSystem> bdSystem;
  request = new SetDebugSceneThrehsold();
  request.newThreshold = 3;
  bdSystem = Cast(GetScriptableSystemsContainer(gameInstance).Get("BraindanceSystem"));
  bdSystem.QueueRequest(request);
}
