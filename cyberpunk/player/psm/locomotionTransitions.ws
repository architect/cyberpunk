
public abstract class LocomotionTransition extends DefaultTransition {

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
  }

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float capsuleHeight;
    Float capsuleRadius;
    capsuleHeight = GetStaticFloatParameter("capsuleHeight", 1);
    capsuleRadius = GetStaticFloatParameter("capsuleRadius", 1);
    return scriptInterface.CanCapsuleFit(capsuleHeight, capsuleRadius);
  }

  protected final void AddModifierGroupWithName(ref<StateGameScriptInterface> scriptInterface, String modifierTDBName) {
    ref<StatsSystem> statSystem;
    Uint64 stateStatGroupID;
    Bool modGroupDefined;
    TweakDBID cpoModRecordId;
    TweakDBID ownerRecordId;
    ownerRecordId = Cast(WeakRefToRef(scriptInterface.owner)).GetRecordID();
    modGroupDefined = false;
    Append(ownerRecordId, "PlayerLocomotion");
    stateStatGroupID = ToNumber(ownerRecordId);
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    if(IsMultiplayer()) {
      cpoModRecordId = Create(modifierTDBName + "_cpo");
      if(ToBool(GetStatModifierGroupRecord(cpoModRecordId))) {
        modGroupDefined = statSystem.DefineModifierGroupFromRecord(stateStatGroupID, cpoModRecordId);
      };
    };
    if(!modGroupDefined) {
      modGroupDefined = statSystem.DefineModifierGroupFromRecord(stateStatGroupID, Create(modifierTDBName));
    };
    statSystem.ApplyModifierGroup(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), stateStatGroupID);
  }

  protected final void AddModifierGroupForState(ref<StateGameScriptInterface> scriptInterface) {
    ref<StatsSystem> statSystem;
    String stateMachineNameString;
    String stateNameString;
    String packageString;
    Bool modGroupDefined;
    TweakDBID cpoModRecordId;
    modGroupDefined = false;
    stateMachineNameString = NameToString(GetStateMachineName());
    packageString = "Player" + stateMachineNameString + ".";
    stateNameString = NameToString(GetStateName());
    UppercaseFirstChar(stateNameString);
    stateNameString = "player_locomotion_data_" + stateNameString;
    stateNameString = packageString + stateNameString;
    AddModifierGroupWithName(scriptInterface, stateNameString);
  }

  protected final void RemoveModifierGroupForState(ref<StateGameScriptInterface> scriptInterface) {
    ref<StatsSystem> statSystem;
    Uint64 stateStatGroupID;
    TweakDBID ownerRecordId;
    ownerRecordId = Cast(WeakRefToRef(scriptInterface.owner)).GetRecordID();
    Append(ownerRecordId, "PlayerLocomotion");
    stateStatGroupID = ToNumber(ownerRecordId);
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    statSystem.RemoveModifierGroup(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), stateStatGroupID);
    statSystem.UndefineModifierGroup(stateStatGroupID);
  }

  protected final void ShowDebugText(String text, ref<StateGameScriptInterface> scriptInterface, out Uint32 layerId) {
    layerId = GetDebugVisualizerSystem(WeakRefToRef(scriptInterface.owner).GetGame()).DrawText(new Vector4(650,100,0,0), text, gameDebugViewETextAlignment.Center, new Color(0,240,148,100));
    GetDebugVisualizerSystem(WeakRefToRef(scriptInterface.owner).GetGame()).SetScale(layerId, new Vector4(1.5,1.5,0,0));
  }

  protected final void ClearDebugText(Uint32 layerId, ref<StateGameScriptInterface> scriptInterface) {
    GetDebugVisualizerSystem(WeakRefToRef(scriptInterface.owner).GetGame()).ClearLayer(layerId);
  }

  protected final void AddImpulseInMovingDirection(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float impulse) {
    Vector4 direction;
    if(impulse == 0) {
      return ;
    };
    direction = scriptInterface.GetOwnerMovingDirection();
    AddImpulse(stateContext, direction * impulse);
  }

  protected final void AddImpulse(ref<StateContext> stateContext, Vector4 impulse) {
    stateContext.SetTemporaryVectorParameter("impulse", impulse, true);
  }

  protected final void AddVerticalImpulse(ref<StateContext> stateContext, Float force) {
    Vector4 impulse;
    impulse.Z = force;
    AddImpulse(stateContext, impulse);
  }

  public void SetCustomLocomotionParameters(ref<StateGameScriptInterface> scriptInterface, out ref<LocomotionParameters> locomotionParameters)

  public final void SetCollisionFilter(ref<StateGameScriptInterface> scriptInterface) {
    SimulationFilter simulationFilter;
    Bool zero;
    zero = GetStaticBoolParameter("collisionFilterPresetIsZero", false);
    if(zero) {
      simulationFilter = ZERO();
    } else {
      SimulationFilter_BuildFromPreset(simulationFilter, "Player Collision");
    };
    scriptInterface.SetStateVectorParameter(physicsStateValue.SimulationFilter, ToVariant(simulationFilter));
  }

  public ref<LocomotionParameters> SetLocomotionParameters(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<LocomotionParameters> locomotionParameters;
    Float blockAimingFor;
    RemoveModifierGroupForState(scriptInterface);
    AddModifierGroupForState(scriptInterface);
    locomotionParameters = new LocomotionParameters();
    GetStateDefaultLocomotionParameters(scriptInterface, locomotionParameters);
    SetCustomLocomotionParameters(scriptInterface, locomotionParameters);
    stateContext.SetTemporaryScriptableParameter("locomotionParameters", locomotionParameters, true);
    return locomotionParameters;
  }

  protected final void SetLocomotionCameraParameters(ref<StateContext> stateContext) {
    StateResultCName param;
    param = GetStaticCNameParameter("onEnterCameraParamsName");
    if(param.valid) {
      stateContext.SetPermanentCNameParameter("LocomotionCameraParams", param.value, true);
      stateContext.SetTemporaryBoolParameter("SignalChange_LocomotionCameraParams", true, true);
    };
  }

  protected final const void GetStateDefaultLocomotionParameters(ref<StateGameScriptInterface> scriptInterface, out ref<LocomotionParameters> locomotionParameters) {
    locomotionParameters.SetUpwardsGravity(GetStaticFloatParameter("upwardsGravity", -16));
    locomotionParameters.SetDownwardsGravity(GetStaticFloatParameter("downwardsGravity", -16));
    locomotionParameters.SetImperfectTurn(GetStaticBoolParameter("imperfectTurn", false));
    locomotionParameters.SetSpeedBoostInputRequired(GetStaticBoolParameter("speedBoostInputRequired", false));
    locomotionParameters.SetSpeedBoostMultiplyByDot(GetStaticBoolParameter("speedBoostMultiplyByDot", false));
    locomotionParameters.SetUseCameraHeadingForMovement(GetStaticBoolParameter("useCameraHeadingForMovement", false));
    locomotionParameters.SetCapsuleHeight(GetStaticFloatParameter("capsuleHeight", 1.7999999523162842));
    locomotionParameters.SetCapsuleRadius(GetStaticFloatParameter("capsuleRadius", 0.4000000059604645));
    locomotionParameters.SetIgnoreSlope(GetStaticBoolParameter("ignoreSlope", false));
  }

  protected final void BroadcastStimuliFootstepSprint(ref<StateGameScriptInterface> context) {
    Bool broadcastFootstepStim;
    ref<StimBroadcasterComponent> broadcaster;
    broadcastFootstepStim = GetStatsSystem(WeakRefToRef(context.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(context.owner).GetEntityID()), gamedataStatType.CanRunSilently) < 1;
    if(broadcastFootstepStim) {
      broadcaster = WeakRefToRef(context.executionOwner).GetStimBroadcasterComponent();
      if(ToBool(broadcaster)) {
        broadcaster.TriggerSingleBroadcast(context.executionOwner, gamedataStimType.FootStepSprint);
      };
    };
  }

  protected final void BroadcastStimuliFootstepRegular(ref<StateGameScriptInterface> context) {
    Bool broadcastFootstepStim;
    ref<StimBroadcasterComponent> broadcaster;
    broadcastFootstepStim = GetStatsSystem(WeakRefToRef(context.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(context.owner).GetEntityID()), gamedataStatType.CanWalkSilently) < 1;
    if(broadcastFootstepStim) {
      broadcaster = WeakRefToRef(context.executionOwner).GetStimBroadcasterComponent();
      if(ToBool(broadcaster)) {
        broadcaster.TriggerSingleBroadcast(context.executionOwner, gamedataStimType.FootStepRegular);
      };
    };
  }

  protected final void SetDetailedState(ref<StateGameScriptInterface> scriptInterface, gamePSMDetailedLocomotionStates state) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed, ToInt(state));
  }

  public final const Bool IsTouchingGround(ref<StateGameScriptInterface> scriptInterface) {
    Bool onGround;
    onGround = scriptInterface.IsOnGround();
    return onGround;
  }

  public final const Bool HasSecureFooting(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return CheckSecureFootingFailure(HasSecureFootingDetailedResult(stateContext, scriptInterface));
  }

  public final const Bool CheckSecureFootingFailure(SecureFootingResult result) {
    return result.type == moveSecureFootingFailureType.Invalid;
  }

  public final const SecureFootingResult HasSecureFootingDetailedResult(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.HasSecureFooting();
  }

  protected final const Float GetFallingSpeedBasedOnHeight(ref<StateGameScriptInterface> scriptInterface, Float height) {
    Float speed;
    Float acc;
    ref<LocomotionParameters> locomotionParameters;
    if(height < 0) {
      return 0;
    };
    locomotionParameters = new LocomotionParameters();
    GetStateDefaultLocomotionParameters(scriptInterface, locomotionParameters);
    acc = AbsF(locomotionParameters.GetUpwardsGravity(GetStaticFloatParameter("defaultGravity", -16)));
    speed = 0;
    if(acc != 0) {
      speed = acc * SqrtF(2 * height / acc);
    };
    return speed * -1;
  }

  protected final Float GetSpeedBasedOnDistance(ref<StateGameScriptInterface> scriptInterface, Float desiredDistance) {
    Float deceleration;
    ref<StatsSystem> statSystem;
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    deceleration = GetStatFloatValue(scriptInterface, gamedataStatType.Deceleration, statSystem);
    return deceleration * SqrtF(2 * desiredDistance / deceleration);
  }

  protected final const Bool IsCurrentFallSpeed(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float fallSpeedThreshold) {
    Float playerFallingTooFast;
    Float verticalSpeed;
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    playerFallingTooFast = fallSpeedThreshold;
    if(verticalSpeed < playerFallingTooFast) {
      return true;
    };
    return false;
  }

  protected final const Bool IsCurrentFallSpeedTooFastToEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsCurrentFallSpeed(stateContext, scriptInterface, GetParameterFloat("VeryHardLandingFallingSpeed", stateContext, true));
  }

  protected final const Bool IsAiming(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.GetActionValue("CameraAim") > 0;
  }

  protected final const Bool WantsToDodge(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isInCooldown;
    isInCooldown = HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeCooldown") || HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeAirCooldown");
    if(isInCooldown) {
      stateContext.SetConditionIntParameter("DodgeFromMovementInput", ToInt(EDodgeMovementInput.Invalid), true);
      return false;
    };
    if(scriptInterface.IsActionJustPressed("Dodge")) {
      if(scriptInterface.IsMoveInputConsiderable()) {
        stateContext.SetConditionFloatParameter("DodgeDirection", scriptInterface.GetInputHeading(), true);
        return true;
      };
      if(GetStaticBoolParameter("dodgeWithNoMovementInput", false)) {
        stateContext.SetConditionFloatParameter("DodgeDirection", -180, true);
        return true;
      };
    };
    if(WantsToDodgeFromMovementInput(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool WantsToDodgeFromMovementInput(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustPressed("DodgeForward")) {
      stateContext.SetConditionFloatParameter("DodgeDirection", 0, true);
      return true;
    };
    if(scriptInterface.IsActionJustPressed("DodgeRight")) {
      stateContext.SetConditionFloatParameter("DodgeDirection", -90, true);
      return true;
    };
    if(scriptInterface.IsActionJustPressed("DodgeLeft")) {
      stateContext.SetConditionFloatParameter("DodgeDirection", 90, true);
      return true;
    };
    if(scriptInterface.IsActionJustPressed("DodgeBack")) {
      stateContext.SetConditionFloatParameter("DodgeDirection", -180, true);
      return true;
    };
    return false;
  }

  public final const Bool IsIdleForced(ref<StateContext> stateContext) {
    return GetParameterBool("ForceIdle", stateContext, true);
  }

  public final const Bool IsWalkForced(ref<StateContext> stateContext) {
    return GetParameterBool("ForceWalk", stateContext, true);
  }

  public final const Bool IsFreezeForced(ref<StateContext> stateContext) {
    return GetParameterBool("ForceFreeze", stateContext, true);
  }

  protected final void PlayRumbleBasedOnDodgeDirection(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    EPlayerMovementDirection movementDirection;
    String presetName;
    movementDirection = GetMovementDirection(stateContext, scriptInterface);
    if(movementDirection == EPlayerMovementDirection.Right) {
      presetName = "medium_pulse_right";
    } else {
      if(movementDirection == EPlayerMovementDirection.Left) {
        presetName = "medium_pulse_left";
      } else {
        presetName = "medium_pulse";
      };
    };
    PlayRumble(scriptInterface, presetName);
  }

  protected final const Bool IsStatusEffectType(ref<StatusEffect_Record> statusEffectRecord, gamedataStatusEffectType type) {
    gamedataStatusEffectType effectType;
    effectType = WeakRefToRef(statusEffectRecord.StatusEffectType()).Type();
    return effectType == type;
  }

  protected final void SpawnLandingFxGameEffect(TweakDBID attackId, ref<StateGameScriptInterface> scriptInterface) {
    ref<EffectInstance> effect;
    Vector4 position;
    effect = GetGameEffectSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).CreateEffectStatic("landing", "fx", WeakRefToRef(scriptInterface.executionOwner));
    position = WeakRefToRef(scriptInterface.executionOwner).GetWorldPosition();
    position.Z += 0.5;
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, 2);
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, position);
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, new Vector4(0,0,-1,0));
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackId, ToVariant(attackId));
    effect.Run();
  }

  protected final const void ProcessSprintInputLock(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("sprintInputLock", stateContext, true) && scriptInterface.GetActionValue("Sprint") == 0 && scriptInterface.GetActionValue("ToggleSprint") == 0) {
      stateContext.RemovePermanentBoolParameter("sprintInputLock");
    };
  }

  protected final const void SetupSprintInputLock(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.GetActionValue("Sprint") != 0 || scriptInterface.GetActionValue("ToggleSprint") != 0) {
      stateContext.SetPermanentBoolParameter("sprintInputLock", true, true);
    };
  }
}

public abstract class LocomotionEventsTransition extends LocomotionTransition {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float blockAimingFor;
    blockAimingFor = GetStaticFloatParameter("softBlockAimingOnEnterFor", -1);
    if(blockAimingFor > 0) {
      SoftBlockAimingForTime(stateContext, scriptInterface, blockAimingFor);
    };
    SetLocomotionParameters(stateContext, scriptInterface);
    SetCollisionFilter(scriptInterface);
    SetLocomotionCameraParameters(stateContext);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)

  public void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    UpdateIsPlayerMovingHorizontallyBB(stateContext, scriptInterface);
    UpdateIsPlayerMovingVerticallyBB(stateContext, scriptInterface);
    ProcessSprintInputLock(stateContext, scriptInterface);
  }

  protected final void UpdateIsPlayerMovingHorizontallyBB(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isMovingHorizontally;
    isMovingHorizontally = IsPlayerMovingHorizontally(stateContext, scriptInterface);
    SetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsMovingHorizontally, isMovingHorizontally);
  }

  protected final void UpdateIsPlayerMovingVerticallyBB(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isMovingVertically;
    isMovingVertically = IsPlayerMovingVertically(stateContext, scriptInterface);
    SetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsMovingVertically, isMovingVertically);
  }

  protected final void ConsumeStaminaBasedOnLocomotionState(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CName stateName;
    Float staminaReduction;
    staminaReduction = 0;
    stateName = GetStateName();
    switch(stateName) {
      case "sprint":
        if(!HasStatFlag(scriptInterface.executionOwner, gamedataStatType.IsSprintStaminaFree)) {
          staminaReduction = GetSprintStaminaCost();
        };
        break;
      case "swimmingFastDiving":
      case "swimmingSurfaceFast":
        staminaReduction = GetSprintStaminaCost();
        break;
      case "slide":
        staminaReduction = GetSlideStaminaCost();
        break;
      case "hoverJump":
      case "chargeJump":
      case "doubleJump":
      case "jump":
        staminaReduction = GetJumpStaminaCost();
        break;
      case "dodge":
        if(!HasStatFlag(scriptInterface.executionOwner, gamedataStatType.IsDodgeStaminaFree)) {
          staminaReduction = GetDodgeStaminaCost();
        };
        break;
      case "dodgeAir":
        if(!HasStatFlag(scriptInterface.executionOwner, gamedataStatType.IsDodgeStaminaFree)) {
          staminaReduction = GetAirDodgeStaminaCost();
        };
        break;
      default:
        staminaReduction = 0.10000000149011612;
    };
    if(staminaReduction > 0) {
      ModifyStamina(Cast(WeakRefToRef(scriptInterface.executionOwner)), -staminaReduction);
    };
  }

  protected final void UpdateInputToggles(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustPressed("ToggleSprint") || scriptInterface.IsActionJustPressed("Sprint")) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
      return ;
    };
    if(scriptInterface.IsActionJustTapped("ToggleCrouch")) {
      stateContext.SetConditionBoolParameter("CrouchToggled", true, true);
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return ;
    };
  }
}

public abstract class LocomotionGroundDecisions extends LocomotionTransition {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  public const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }

  public final static Bool CheckCrouchEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustTapped("ToggleCrouch") || GetConditionParameterBool("CrouchToggled", stateContext)) {
      stateContext.SetConditionBoolParameter("CrouchToggled", true, true);
      return true;
    };
    return scriptInterface.GetActionValue("Crouch") > 0;
  }

  protected final const Bool CrouchEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CName paramName;
    ref<ScriptedPuppet> puppet;
    ref<ScriptedPuppetPS> puppetPS;
    paramName = GetFFParamsForCrouch(scriptInterface);
    puppet = Cast(WeakRefToRef(scriptInterface.owner));
    if(ToBool(puppet)) {
      puppetPS = puppet.GetPuppetPS();
    };
    if(scriptInterface.IsActionJustTapped("ToggleCrouch") || GetConditionParameterBool("CrouchToggled", stateContext) && !GetParameterBool(paramName, stateContext, true) || ToBool(puppetPS) && puppetPS.IsCrouch()) {
      stateContext.SetConditionBoolParameter("CrouchToggled", true, true);
      return true;
    };
    return scriptInterface.GetActionValue("Crouch") > 0;
  }

  protected final const Bool CrouchExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CName paramName;
    paramName = GetFFParamsForCrouch(scriptInterface);
    if(scriptInterface.IsActionJustReleased("Crouch") || scriptInterface.IsActionJustTapped("ToggleCrouch") && !GetParameterBool(paramName, stateContext, true)) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
      return true;
    };
    if(!GetConditionParameterBool("CrouchToggled", stateContext) && scriptInterface.GetActionValue("Crouch") == 0) {
      return true;
    };
    return false;
  }
}

public abstract class LocomotionGroundEvents extends LocomotionEventsTransition {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_PlayerLocomotionStateMachine> animFeature;
    OnEnter(stateContext, scriptInterface);
    stateContext.RemovePermanentBoolParameter("enteredFallFromAirDodge");
    stateContext.SetPermanentIntParameter("currentNumberOfJumps", 0, true);
    stateContext.SetPermanentIntParameter("currentNumberOfAirDodges", 0, true);
    SetAudioParameter("RTPC_Vertical_Velocity", 0, scriptInterface);
    UpdateAimAssist(stateContext, scriptInterface);
    animFeature = new AnimFeature_PlayerLocomotionStateMachine();
    animFeature.inAirState = false;
    scriptInterface.SetAnimationParameterFeature("LocomotionStateMachine", animFeature);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, ToInt(gamePSMFallStates.Default));
    GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("EnterOnGround");
    StopEffect(scriptInterface, "falling");
    stateContext.SetConditionBoolParameter("JumpPressed", false, true);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("LeaveOnGround");
  }
}

public class ForceIdleDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsIdleForced(stateContext) || scriptInterface.IsSceneAnimationActive() || GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice);
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    MountingInfo mountingInfo;
    mountingInfo = scriptInterface.GetMountingInfo(WeakRefToRef(scriptInterface.executionOwner));
    return !IsIdleForced(stateContext) && !scriptInterface.IsSceneAnimationActive() && !InfoIsComplete(mountingInfo) && !GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice);
  }
}

public class ForceIdleEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().DisableQueriesForOwner(WeakRefToRef(scriptInterface.owner), gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().EnableQueriesForOwner(WeakRefToRef(scriptInterface.owner), gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }
}

public class WorkspotDecisions extends LocomotionGroundDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsInWorkspot(scriptInterface);
  }

  protected final const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsInWorkspot(scriptInterface) || IsInMinigame(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }
}

public class WorkspotEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetPlayerPuppet(scriptInterface).HasWorkspotTag("DisableCameraControl")) {
      SetWorkspotAnimFeature(scriptInterface);
    };
    if(GetPlayerPuppet(scriptInterface).HasWorkspotTag("Grab")) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    };
    stateContext.SetTemporaryBoolParameter("requestSandevistanDeactivation", true, true);
    OnEnter(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInWorkspot, ToInt(gamePSMWorkspotState.Workspot));
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().DisableQueriesForOwner(WeakRefToRef(scriptInterface.owner), gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInWorkspot, ToInt(gamePSMWorkspotState.Default));
    ResetWorkspotAnimFeature(scriptInterface);
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().EnableQueriesForOwner(WeakRefToRef(scriptInterface.owner), gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInWorkspot, ToInt(gamePSMWorkspotState.Default));
    ResetWorkspotAnimFeature(scriptInterface);
  }

  protected final void SetWorkspotAnimFeature(ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_AerialTakedown> animFeature;
    animFeature = new AnimFeature_AerialTakedown();
    animFeature.state = 1;
    scriptInterface.SetAnimationParameterFeature("AerialTakedown", animFeature);
  }

  protected final void ResetWorkspotAnimFeature(ref<StateGameScriptInterface> scriptInterface) {
    ref<AnimFeature_AerialTakedown> animFeature;
    animFeature = new AnimFeature_AerialTakedown();
    animFeature.state = 0;
    scriptInterface.SetAnimationParameterFeature("AerialTakedown", animFeature);
  }
}

public class ForceWalkDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsWalkForced(stateContext);
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsWalkForced(stateContext);
  }
}

public class ForceWalkEvents extends LocomotionGroundEvents {

  public Float m_storedSpeedValue;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }
}

public class ForceFreezeDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsFreezeForced(stateContext);
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsFreezeForced(stateContext);
  }

  protected final const Bool ToWorkspot(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !IsInMinigame(scriptInterface) && IsInWorkspot(scriptInterface);
  }
}

public class ForceFreezeEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().DisableQueriesForOwner(WeakRefToRef(scriptInterface.owner), gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().EnableQueriesForOwner(WeakRefToRef(scriptInterface.owner), gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }
}

public class InitialDecisions extends LocomotionGroundDecisions {

  protected final const Bool ToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }
}

public class StandDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  public const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsTouchingGround(scriptInterface) && GetVerticalSpeed(scriptInterface) < 0.5;
  }
}

public class StandEvents extends LocomotionGroundEvents {

  public Float m_previousStimTimeStamp;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    scriptInterface.PushAnimationEvent("StandEnter");
    stateContext.SetConditionBoolParameter("blockEnteringSlide", false, true);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    PlaySound("lcm_falling_wind_loop_end", scriptInterface);
    this.m_previousStimTimeStamp = -1;
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
    UpdateFootstepRegularStim(stateContext, scriptInterface);
  }

  protected final void UpdateFootstepRegularStim(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float playerSpeed;
    Float footstepStimuliSpeedThreshold;
    playerSpeed = scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed);
    footstepStimuliSpeedThreshold = GetStaticFloatParameter("footstepStimuliSpeedThreshold", 2.5);
    if(playerSpeed > footstepStimuliSpeedThreshold) {
      if(GetInStateTime(stateContext, scriptInterface) >= this.m_previousStimTimeStamp + 0.20000000298023224 && IsTouchingGround(scriptInterface)) {
        this.m_previousStimTimeStamp = GetInStateTime(stateContext, scriptInterface);
        BroadcastStimuliFootstepRegular(scriptInterface);
      };
    };
  }
}

public class AimWalkDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Bool isAiming;
    Bool isBlocking;
    Bool isFocusMode;
    Bool isChargingCyberware;
    blackboard = GetBlackboard(scriptInterface);
    isFocusMode = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus);
    isAiming = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
    isBlocking = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Melee) == ToInt(gamePSMMelee.Block);
    isChargingCyberware = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LeftHandCyberware) == ToInt(gamePSMLeftHandCyberware.Charge);
    if(HasMeleeWeaponEquipped(scriptInterface) && isBlocking && GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), gamedataStatType.IsNotSlowedDuringBlock) > 0) {
      return false;
    };
    return isBlocking || isAiming || isFocusMode || isChargingCyberware;
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool res;
    res = !EnterCondition(stateContext, scriptInterface);
    if(res) {
      Log("");
    };
    return res;
  }
}

public class AimWalkEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.AimWalk);
  }
}

public class CrouchDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool superResult;
    Bool shouldCrouch;
    if(IsFastForwardByLine(scriptInterface) && !HasRestriction(scriptInterface.executionOwner, "ForceStand")) {
      if(!HasStatFlag(scriptInterface, gamedataStatType.CanCrouch) || GetParameterBool("FFhintActive", stateContext, true) || !HasStatFlag(scriptInterface, gamedataStatType.FFInputLock)) {
        if(scriptInterface.IsActionJustHeld("ToggleCrouch")) {
          stateContext.SetConditionBoolParameter("CrouchToggled", true, true);
          stateContext.SetPermanentBoolParameter("HoldInputFastForwardLock", true, true);
          return true;
        };
        return false;
      };
    };
    superResult = EnterCondition(stateContext, scriptInterface) && HasStatFlag(scriptInterface, gamedataStatType.CanCrouch);
    shouldCrouch = CrouchEnterCondition(stateContext, scriptInterface) || IsCrouchForced(scriptInterface);
    return shouldCrouch && superResult && IsTouchingGround(scriptInterface);
  }

  protected const Bool ToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetConditionBoolParameter("SprintToggled", false, true);
    return true;
  }

  protected const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsFastForwardByLine(scriptInterface)) {
      if(HasRestriction(scriptInterface.executionOwner, "FastForwardCrouchLock") || GetParameterBool("FFhintActive", stateContext, true) || !HasStatFlag(scriptInterface, gamedataStatType.FFInputLock)) {
        if(scriptInterface.IsActionJustHeld("ToggleCrouch")) {
          stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
          stateContext.SetPermanentBoolParameter("HoldInputFastForwardLock", true, true);
          return true;
        };
        return false;
      };
      if(!HasStatFlag(scriptInterface, gamedataStatType.CanCrouch) && !GetParameterBool("FFhintActive", stateContext, true)) {
        return true;
      };
    } else {
      if(!HasStatFlag(scriptInterface, gamedataStatType.CanCrouch) && !HasRestriction(scriptInterface.executionOwner, "FastForward") && !GetParameterBool("FFRestriction", stateContext, true) && !GetParameterBool("TriggerFF", stateContext, true)) {
        return true;
      };
    };
    if(CrouchExitCondition(stateContext, scriptInterface) && !IsCrouchForced(scriptInterface)) {
      return true;
    };
    if(HasRestriction(scriptInterface.executionOwner, "ForceStand")) {
      return true;
    };
    return false;
  }

  protected const Bool ToSprint(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool visionToggled;
    ref<IBlackboard> blackboard;
    visionToggled = stateContext.GetPermanentBoolParameter("VisionToggled");
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanSprint)) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().CoverAction.coverActionStateId) == 3) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus) || visionToggled.valid && visionToggled.value) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LeftHandCyberware) == ToInt(gamePSMLeftHandCyberware.Charge)) {
      return false;
    };
    if(scriptInterface.GetActionValue("AttackA") > 0) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim)) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Melee) == ToInt(gamePSMMelee.Block) || IsInMeleeState(stateContext, "meleeChargedHold")) {
      return false;
    };
    if(IsChargingWeapon(scriptInterface)) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon) == ToInt(gamePSMMeleeWeapon.ChargedHold) && !GetParameterBool("canSprintWhileCharging", stateContext, true)) {
      return false;
    };
    if(!GetConditionParameterBool("SprintToggled", stateContext) && scriptInterface.IsActionJustReleased("ToggleSprint") && !HasRestriction(scriptInterface.executionOwner, "NoSlide")) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
    };
    if(scriptInterface.GetActionValue("Crouch") == 0 && scriptInterface.GetActionValue("Sprint") > 0 || scriptInterface.GetActionValue("ToggleSprint") > 0 || GetConditionParameterBool("SprintToggled", stateContext) && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 1 && !HasRestriction(scriptInterface.executionOwner, "NoSlide")) {
      return true;
    };
    return false;
  }
}

public class CrouchEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<ScriptedPuppet> puppet;
    puppet = Cast(WeakRefToRef(scriptInterface.owner));
    if(ToBool(puppet)) {
      puppet.GetPuppetPS().SetCrouch(true);
    };
    OnEnter(stateContext, scriptInterface);
    if(HasRestriction(scriptInterface.executionOwner, "NoSlide")) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
    };
    GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("EnterCrouch");
    GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().OnEnterCrouch(WeakRefToRef(scriptInterface.owner));
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Crouch));
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
    if(HasMeleeWeaponEquipped(scriptInterface)) {
      GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).AimSnap(scriptInterface.owner);
    };
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Crouch);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<ScriptedPuppet> puppet;
    puppet = Cast(WeakRefToRef(scriptInterface.owner));
    if(ToBool(puppet)) {
      puppet.GetPuppetPS().SetCrouch(false);
    };
    OnExit(stateContext, scriptInterface);
    GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("LeaveCrouch");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    scriptInterface.SetAnimationParameterFloat("crouch", 0);
    if(HasMeleeWeaponEquipped(scriptInterface)) {
      GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).AimSnap(scriptInterface.owner);
    };
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    OnExit(stateContext, scriptInterface);
  }
}

public class SprintDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Float minLinearVelocityThreshold;
    Float minStickInputThreshold;
    Float enterAngleThreshold;
    Bool superResult;
    Bool isAiming;
    StateResultFloat lastShotTime;
    Bool isFocusMode;
    Bool isChargingCyberware;
    blackboard = GetBlackboard(scriptInterface);
    superResult = EnterCondition(stateContext, scriptInterface) && IsTouchingGround(scriptInterface);
    minLinearVelocityThreshold = GetStaticFloatParameter("minLinearVelocityThreshold", 0.5);
    minStickInputThreshold = GetStaticFloatParameter("minStickInputThreshold", 0.8999999761581421);
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", -180);
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanSprint)) {
      return false;
    };
    if(!scriptInterface.IsMoveInputConsiderable() || AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold || GetMovementInputActionValue(stateContext, scriptInterface) < minStickInputThreshold || scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < minLinearVelocityThreshold) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return false;
    };
    isAiming = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
    if(isAiming) {
      return false;
    };
    isChargingCyberware = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LeftHandCyberware) == ToInt(gamePSMLeftHandCyberware.Charge);
    if(isChargingCyberware) {
      return false;
    };
    if(IsChargingWeapon(scriptInterface)) {
      return false;
    };
    if(!MeleeSprintStateCondition(stateContext, scriptInterface)) {
      return false;
    };
    lastShotTime = stateContext.GetPermanentFloatParameter("LastShotTime");
    if(lastShotTime.valid) {
      if(ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) - lastShotTime.value < GetStaticFloatParameter("sprintDisableTimeAfterShoot", -2)) {
        return false;
      };
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponShootWhileSprinting) && scriptInterface.GetActionValue("RangedAttack") > 0) {
      return false;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().CoverAction.coverActionStateId) == 3) {
      return false;
    };
    if(scriptInterface.GetActionValue("ToggleSprint") > 0 && !GetParameterBool("sprintInputLock", stateContext, true)) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
    };
    if(GetConditionParameterBool("SprintToggled", stateContext) && !GetParameterBool("sprintInputLock", stateContext, true)) {
      return superResult;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSprinting) && blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload)) {
      if(scriptInterface.IsActionJustPressed("Sprint") && !GetParameterBool("sprintInputLock", stateContext, true)) {
        return superResult;
      };
    } else {
      if(scriptInterface.GetActionValue("Sprint") > 0 && !GetParameterBool("sprintInputLock", stateContext, true)) {
        return superResult;
      };
    };
    return false;
  }

  protected const Bool ToJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator)) {
      return false;
    };
    if(HasStatFlag(scriptInterface, gamedataStatType.CanJump)) {
      if(!HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) && scriptInterface.IsActionJustPressed("Jump")) {
        return true;
      };
      if(HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) && GetActionHoldTime(stateContext, scriptInterface, "Jump") < 0.15000000596046448 && GetConditionParameterFloat("InputHoldTime", stateContext) < 0.15000000596046448 && scriptInterface.IsActionJustReleased("Jump")) {
        return true;
      };
      return false;
    };
    return false;
  }

  protected const Bool ToChargeJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasStatFlag(scriptInterface, gamedataStatType.CanJump) && HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) && GetConditionParameterFloat("InputHoldTime", stateContext) > 0.15000000596046448 && scriptInterface.IsActionJustReleased("Jump");
  }

  protected const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float minLinearVelocityThreshold;
    Float enterAngleThreshold;
    Float minStickInputThreshold;
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanSprint)) {
      return true;
    };
    if(GetParameterBool("InterruptSprint", stateContext)) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    if(GetInStateTime(stateContext, scriptInterface) >= 0.30000001192092896) {
      minLinearVelocityThreshold = GetStaticFloatParameter("minLinearVelocityThreshold", 0.5);
      if(scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < minLinearVelocityThreshold) {
        stateContext.SetConditionBoolParameter("SprintToggled", false, true);
        return true;
      };
    };
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", 45);
    if(!scriptInterface.IsMoveInputConsiderable() || !AbsF(scriptInterface.GetInputHeading()) < enterAngleThreshold) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    minStickInputThreshold = GetStaticFloatParameter("minStickInputThreshold", 0.8999999761581421);
    if(GetConditionParameterBool("SprintToggled", stateContext) && GetMovementInputActionValue(stateContext, scriptInterface) < minStickInputThreshold) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    if(scriptInterface.IsActionJustReleased("Sprint")) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    return false;
  }
}

public class SprintEvents extends LocomotionGroundEvents {

  public Float m_previousStimTimeStamp;

  public ref<gameStatModifierData> m_reloadModifier;

  public Bool m_isInSecondSprint;

  public ref<gameStatModifierData> m_sprintModifier;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    this.m_previousStimTimeStamp = -1;
    this.m_isInSecondSprint = false;
    OnEnter(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Sprint));
    UpdateAimAssist(stateContext, scriptInterface);
    SetupSprintInputLock(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter("blockEnteringSlide", false, true);
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    stateContext.SetTemporaryBoolParameter("CancelGrenadeAction", true, true);
    ForceDisableVisionMode(stateContext);
    StartEffect(scriptInterface, "locomotion_sprint");
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSprinting) && GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload)) {
      stateContext.SetTemporaryBoolParameter("InterruptReload", true, true);
    };
    if(!GetStaticBoolParameter("enableTwoStepSprint_EXPERIMENTAL", false)) {
      AddMaxSpeedModifier(stateContext, scriptInterface);
    };
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Sprint);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isReloading;
    Bool isShooting;
    StatsObjectID ownerID;
    ownerID = Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
    OnUpdate(timeDelta, stateContext, scriptInterface);
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    UpdateFootstepSprintStim(stateContext, scriptInterface);
    isReloading = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload);
    isShooting = scriptInterface.GetActionValue("RangedAttack") > 0;
    EvaluateTwoStepSprint(stateContext, scriptInterface);
    if(isReloading && !HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSprinting) || isShooting && !HasStatFlag(scriptInterface, gamedataStatType.CanWeaponShootWhileSprinting) && !ToBool(this.m_reloadModifier)) {
      SetInputFloat(WeakRefToRef(scriptInterface.executionOwner), "sprint", 0);
      EnableReloadStatModifier(true, stateContext, scriptInterface);
    } else {
      if(!isReloading || isShooting && ToBool(this.m_reloadModifier)) {
        SetInputFloat(WeakRefToRef(scriptInterface.executionOwner), "sprint", 1);
        EnableReloadStatModifier(false, stateContext, scriptInterface);
      };
    };
  }

  private final void EvaluateTwoStepSprint(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetStaticBoolParameter("enableTwoStepSprint_EXPERIMENTAL", false)) {
      if(ShouldEnterSecondSprint(stateContext, scriptInterface)) {
        this.m_isInSecondSprint = true;
        AddMaxSpeedModifier(stateContext, scriptInterface);
      } else {
        if(this.m_isInSecondSprint && GetInStateTime(stateContext, scriptInterface) >= 0.5 && scriptInterface.IsActionJustPressed("ToggleSprint")) {
          this.m_isInSecondSprint = false;
          RemoveMaxSpeedModifier(stateContext, scriptInterface);
        };
      };
    };
  }

  private final void AddMaxSpeedModifier(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StatsSystem> statSystem;
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    this.m_sprintModifier = CreateStatModifierUsingCurve(gamedataStatType.MaxSpeed, gameStatModifierType.Additive, gamedataStatType.Reflexes, "locomotion_stats", "max_speed_in_sprint");
    statSystem.AddModifier(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), this.m_sprintModifier);
  }

  private final void RemoveMaxSpeedModifier(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StatsSystem> statSystem;
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    if(ToBool(this.m_sprintModifier)) {
      statSystem.RemoveModifier(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), this.m_sprintModifier);
      this.m_sprintModifier = null;
    };
  }

  private final Bool ShouldEnterSecondSprint(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !this.m_isInSecondSprint && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("minTimeToEnterTwoStepSprint", 0) && scriptInterface.IsActionJustPressed("ToggleSprint");
  }

  private final void CleanupTwoStepSprint(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    this.m_isInSecondSprint = false;
    RemoveMaxSpeedModifier(stateContext, scriptInterface);
  }

  protected final const Float GetReloadModifier(ref<StateGameScriptInterface> scriptInterface) {
    Float result;
    Float statValue;
    Float lerp;
    Float modifierStart;
    Float modifierEnd;
    Float modifierRange;
    modifierStart = GetStaticFloatParameter("reloadModifierStart", -2);
    modifierEnd = GetStaticFloatParameter("reloadModifierEnd", -2);
    modifierRange = modifierEnd - modifierStart;
    statValue = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), gamedataStatType.Reflexes);
    lerp = statValue - 1 / 19;
    result = modifierStart + lerp * modifierRange;
    return result;
  }

  protected void EnableReloadStatModifier(Bool enable, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StatsObjectID ownerID;
    Float reloadModifierAmount;
    ownerID = Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
    if(enable && !ToBool(this.m_reloadModifier)) {
      reloadModifierAmount = GetReloadModifier(scriptInterface);
      this.m_reloadModifier = CreateStatModifier(gamedataStatType.MaxSpeed, gameStatModifierType.Additive, reloadModifierAmount);
      GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).AddModifier(ownerID, this.m_reloadModifier);
    } else {
      if(!enable && ToBool(this.m_reloadModifier)) {
        GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).RemoveModifier(ownerID, this.m_reloadModifier);
        this.m_reloadModifier = null;
      };
    };
  }

  protected final void UpdateFootstepSprintStim(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= this.m_previousStimTimeStamp + 0.20000000298023224) {
      this.m_previousStimTimeStamp = GetInStateTime(stateContext, scriptInterface);
      BroadcastStimuliFootstepSprint(scriptInterface);
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CleanupTwoStepSprint(stateContext, scriptInterface);
    EnableReloadStatModifier(false, stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    stateContext.SetPermanentFloatParameter("SprintingStoppedTimeStamp", scriptInterface.GetNow(), true);
    StopEffect(scriptInterface, "locomotion_sprint");
    OnExit(stateContext, scriptInterface);
  }

  protected void OnExitToJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CleanupTwoStepSprint(stateContext, scriptInterface);
    EnableReloadStatModifier(false, stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    stateContext.SetPermanentFloatParameter("SprintingStoppedTimeStamp", scriptInterface.GetNow(), true);
    StopEffect(scriptInterface, "locomotion_sprint");
    OnExit(stateContext, scriptInterface);
  }

  protected void OnExitToChargeJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CleanupTwoStepSprint(stateContext, scriptInterface);
    EnableReloadStatModifier(false, stateContext, scriptInterface);
    StopEffect(scriptInterface, "locomotion_sprint");
    OnExit(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    CleanupTwoStepSprint(stateContext, scriptInterface);
    EnableReloadStatModifier(false, stateContext, scriptInterface);
    StopEffect(scriptInterface, "locomotion_sprint");
  }
}

public class SlideFallDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return ShouldFall(stateContext, scriptInterface);
  }

  protected final const Bool ToSlide(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    return true;
  }

  protected final const Bool ToFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float fallingSpeedThreshold;
    Float verticalSpeed;
    Float height;
    if(AbsF(GetCameraYaw(stateContext, scriptInterface)) >= GetStaticFloatParameter("maxCameraYawToExit", 95)) {
      return true;
    };
    if(GetStaticBoolParameter("backInputExitsSlide", false) && scriptInterface.GetActionValue("MoveY") < -0.5) {
      return true;
    };
    if(scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < GetStaticFloatParameter("minSpeedToExit", 2) && stateContext.GetStateMachineCurrentState("TimeDilation") != "kerenzikov") {
      return true;
    };
    height = GetStaticFloatParameter("heightToEnterFall", 0);
    if(height > 0) {
      fallingSpeedThreshold = GetFallingSpeedBasedOnHeight(scriptInterface, height);
      verticalSpeed = GetVerticalSpeed(scriptInterface);
      if(verticalSpeed < fallingSpeedThreshold) {
        return true;
      };
    };
    return false;
  }
}

public class SlideFallEvents extends LocomotionAirEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SlideFall);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }
}

public class SlideDecisions extends CrouchDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool superResult;
    Float currentSpeed;
    Vector4 velocity;
    Float angle;
    SecureFootingResult secureFootingResult;
    superResult = EnterCondition(stateContext, scriptInterface);
    if(!superResult) {
      return false;
    };
    if(GetConditionParameterBool("blockEnteringSlide", stateContext)) {
      return false;
    };
    if(HasRestriction(scriptInterface.executionOwner, "NoSlide")) {
      return false;
    };
    velocity = GetLinearVelocity(scriptInterface);
    angle = GetAngleBetween(WeakRefToRef(scriptInterface.executionOwner).GetWorldForward(), velocity);
    if(AbsF(angle) > 45) {
      return false;
    };
    currentSpeed = Length2D(velocity);
    secureFootingResult = scriptInterface.HasSecureFooting();
    if(secureFootingResult.type == moveSecureFootingFailureType.Slope) {
      return true;
    };
    if(currentSpeed < GetStaticFloatParameter("minSpeedToEnter", 4.5)) {
      return false;
    };
    if(!scriptInterface.IsMoveInputConsiderable()) {
      return false;
    };
    return true;
  }

  protected const Bool ToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    if(ShouldExit(stateContext, scriptInterface)) {
      return scriptInterface.GetActionValue("Crouch") > 0 || GetConditionParameterBool("CrouchToggled", stateContext);
    };
    return false;
  }

  protected const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    if(GetInStateTime(stateContext, scriptInterface) < GetStaticFloatParameter("minTimeToExit", 1)) {
      return false;
    };
    if(!GetConditionParameterBool("CrouchToggled", stateContext) && scriptInterface.GetActionValue("Crouch") < 0) {
      return true;
    };
    if(scriptInterface.IsActionJustReleased("Crouch") || scriptInterface.IsActionJustPressed("Sprint") || scriptInterface.IsActionJustPressed("ToggleSprint") || scriptInterface.IsActionJustPressed("Jump") || scriptInterface.IsActionJustTapped("ToggleCrouch")) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
      return true;
    };
    return ShouldExit(stateContext, scriptInterface);
  }

  protected const Bool ShouldExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isKerenzikovEnd;
    Bool isKerenzikovStateActive;
    if(AbsF(GetCameraYaw(stateContext, scriptInterface)) >= GetStaticFloatParameter("maxCameraYawToExit", 95)) {
      return true;
    };
    isKerenzikovStateActive = stateContext.GetStateMachineCurrentState("TimeDilation") == "kerenzikov";
    if(GetStaticBoolParameter("backInputExitsSlide", false) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("minTimeToExit", 0.699999988079071) && scriptInterface.GetActionValue("MoveY") < -0.5) {
      return true;
    };
    if(scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < GetStaticFloatParameter("minSpeedToExit", 3)) {
      return !isKerenzikovStateActive;
    };
    isKerenzikovEnd = isKerenzikovStateActive && !HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.KerenzikovPlayerBuff");
    if(isKerenzikovEnd) {
      return true;
    };
    return false;
  }
}

public class SlideEvents extends CrouchEvents {

  public Bool m_rumblePlayed;

  public ref<gameStatModifierData> m_addDecelerationModifier;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter("SprintToggled", false, true);
    this.m_rumblePlayed = false;
    if(GetStaticBoolParameter("pushAnimEventOnEnter", false)) {
      scriptInterface.PushAnimationEvent("Slide");
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileSliding) && GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload)) {
      stateContext.SetTemporaryBoolParameter("InterruptReload", true, true);
    };
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Slide);
  }

  public final void OnEnterFromSprint(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    BroadcastStimuliFootstepSprint(scriptInterface);
    OnEnter(stateContext, scriptInterface);
  }

  protected void AddDecelerationStatModifier(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Bool enable) {
    StatsObjectID ownerID;
    ownerID = Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
    if(enable && !ToBool(this.m_addDecelerationModifier)) {
      this.m_addDecelerationModifier = CreateStatModifier(gamedataStatType.Deceleration, gameStatModifierType.Additive, GetStaticFloatParameter("backInputDecelerationModifier", 8));
      GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).AddModifier(ownerID, this.m_addDecelerationModifier);
    } else {
      if(!enable && ToBool(this.m_addDecelerationModifier)) {
        GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).RemoveModifier(ownerID, this.m_addDecelerationModifier);
        this.m_addDecelerationModifier = null;
      };
    };
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    if(!this.m_rumblePlayed && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("rumbleDelay", 0.5)) {
      this.m_rumblePlayed = true;
      PlayRumble(scriptInterface, GetStaticStringParameter("rumbleName", "medium_slow"));
    };
    if(GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("minTimeToExit", 1)) {
      UpdateInputToggles(stateContext, scriptInterface);
    };
    if(GetStaticBoolParameter("backInputDeceleratesSlide", false)) {
      EvaluateSlideDeceleration(stateContext, scriptInterface);
    };
  }

  private final void EvaluateSlideDeceleration(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("minTimeToAllowDeceleration", 0.10000000149011612) && scriptInterface.GetActionValue("MoveY") < -0.5 && !GetParameterBool("isDecelerating", stateContext, true)) {
      stateContext.SetPermanentBoolParameter("isDecelerating", true, true);
      AddDecelerationStatModifier(stateContext, scriptInterface, true);
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpOnExit(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    CleanUpOnExit(stateContext, scriptInterface);
  }

  public final void OnExitToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Crouch));
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
    CleanUpOnExit(stateContext, scriptInterface);
  }

  private final void CleanUpOnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    AddDecelerationStatModifier(stateContext, scriptInterface, false);
    stateContext.RemovePermanentBoolParameter("isDecelerating");
  }
}

public class DodgeDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasDodge)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanAimWhileDodging) && IsInUpperBodyState(stateContext, "aimingState") && IsRangedWeaponEquipped(scriptInterface)) {
      return false;
    };
    if(WantsToDodge(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isKerenzikovStateActive;
    Bool isKerenzikovEnd;
    isKerenzikovStateActive = stateContext.GetStateMachineCurrentState("TimeDilation") == "kerenzikov";
    if(!HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeBuff")) {
      return !isKerenzikovStateActive;
    };
    isKerenzikovEnd = isKerenzikovStateActive && !HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.KerenzikovPlayerBuff");
    if(isKerenzikovEnd) {
      return true;
    };
    return false;
  }
}

public class DodgeEvents extends LocomotionGroundEvents {

  public ref<gameStatModifierData> m_blockStatFlag;

  public ref<gameStatModifierData> m_decelerationModifier;

  [Default(DodgeEvents, false))]
  public Bool m_pressureWaveCreated;

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StatsObjectID ownerID;
    ref<QuestsSystem> questSystem;
    Float dodgeHeading;
    ownerID = Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
    questSystem = GetQuestsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    dodgeHeading = GetConditionParameterFloat("DodgeDirection", stateContext);
    OnEnter(stateContext, scriptInterface);
    Dodge(stateContext, scriptInterface);
    PlayRumbleBasedOnDodgeDirection(stateContext, scriptInterface);
    questSystem.SetFact("gmpl_player_dodged", questSystem.GetFact("gmpl_player_dodged") + 1);
    scriptInterface.PushAnimationEvent("Dodge");
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    ApplyStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeBuff");
    if(dodgeHeading < -45 || dodgeHeading > 45) {
      ApplyStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeInvulnerability");
    };
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    this.m_blockStatFlag = CreateStatModifier(gamedataStatType.IsDodging, gameStatModifierType.Additive, 1);
    GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).AddModifier(ownerID, this.m_blockStatFlag);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Dodge);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
    if(!this.m_pressureWaveCreated && GetInStateTime(stateContext, scriptInterface) >= 0.15000000596046448) {
      this.m_pressureWaveCreated = true;
      PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.Dodge"));
    };
    if(scriptInterface.IsActionJustPressed("Jump")) {
      stateContext.SetConditionBoolParameter("JumpPressed", true, true);
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CleanUpOnExit(stateContext, scriptInterface);
    OnExit(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    CleanUpOnExit(stateContext, scriptInterface);
  }

  private final void CleanUpOnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StatsObjectID ownerID;
    ownerID = Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
    this.m_pressureWaveCreated = false;
    GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).RemoveModifier(ownerID, this.m_blockStatFlag);
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeBuff");
    EnableMovementDecelerationStatModifier(stateContext, scriptInterface, false);
  }

  protected final void Dodge(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float impulseValue;
    Vector4 impulse;
    Float dodgeHeading;
    Float currentStaminaVal;
    if(HasStatusEffect(scriptInterface.executionOwner, GetExhaustedStatusEffectID())) {
      EnableMovementDecelerationStatModifier(stateContext, scriptInterface, true);
      impulseValue = GetStaticFloatParameter("impulseNoStamina", 4.800000190734863);
    } else {
      EnableMovementDecelerationStatModifier(stateContext, scriptInterface, false);
      impulseValue = GetStaticFloatParameter("impulse", 13);
    };
    dodgeHeading = GetConditionParameterFloat("DodgeDirection", stateContext);
    impulse = FromHeading(AngleNormalize180(WeakRefToRef(scriptInterface.executionOwner).GetWorldYaw() + dodgeHeading)) * impulseValue;
    AddImpulse(stateContext, impulse);
  }

  protected void EnableMovementDecelerationStatModifier(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Bool enable) {
    StatsObjectID ownerID;
    ownerID = Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID());
    if(enable && !ToBool(this.m_decelerationModifier)) {
      this.m_decelerationModifier = CreateStatModifier(gamedataStatType.Deceleration, gameStatModifierType.Additive, GetStaticFloatParameter("movementDecelerationNoStamina", -90));
      GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).AddModifier(ownerID, this.m_decelerationModifier);
    } else {
      if(!enable && ToBool(this.m_decelerationModifier)) {
        GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).RemoveModifier(ownerID, this.m_decelerationModifier);
        this.m_decelerationModifier = null;
      };
    };
  }
}

public class ClimbDecisions extends LocomotionGroundDecisions {

  public const Bool stateBodyDone;

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<PlayerClimbInfo> climbInfo;
    Float enterAngleThreshold;
    Bool isObstacleSuitable;
    Bool isPathClear;
    ref<AnimFeature_PreClimbing> preClimbAnimFeature;
    Vector4 playerPosition;
    Bool isInAcceptableAerialState;
    isPathClear = false;
    isInAcceptableAerialState = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Jump) || IsInLocomotionState(stateContext, "dodgeAir") || GetParameterBool("enteredFallFromAirDodge", stateContext, true);
    if(!isInAcceptableAerialState && !GetConditionParameterBool("JumpPressed", stateContext) || scriptInterface.IsActionJustPressed("Jump")) {
      return false;
    };
    playerPosition = GetPlayerPosition(scriptInterface);
    climbInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    isObstacleSuitable = climbInfo.climbValid && OverlapFitTest(scriptInterface, climbInfo, playerPosition);
    if(isObstacleSuitable) {
      isPathClear = TestClimbingPath(scriptInterface, climbInfo, playerPosition);
      isObstacleSuitable = isObstacleSuitable && isPathClear;
    };
    preClimbAnimFeature = new AnimFeature_PreClimbing();
    preClimbAnimFeature.valid = 0;
    if(isObstacleSuitable) {
      preClimbAnimFeature.edgePositionLS = scriptInterface.TransformInvPointFromObject(climbInfo.descResult.topPoint);
      preClimbAnimFeature.valid = 1;
    };
    stateContext.SetConditionScriptableParameter("PreClimbAnimFeature", preClimbAnimFeature, true);
    if(IsVaultingClimbingRestricted(scriptInterface)) {
      return false;
    };
    if(GetStaticBoolParameter("requireForwardEnterAngleToClimb", false) && !ForwardAngleTest(stateContext, scriptInterface, climbInfo)) {
      return false;
    };
    if(IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface)) {
      return false;
    };
    if(GetStaticBoolParameter("requireDirectionalInputToClimb", false) && !scriptInterface.IsMoveInputConsiderable()) {
      return false;
    };
    if(!GetStaticBoolParameter("requireDirectionalInputToClimb", false) && AbsF(scriptInterface.GetInputHeading()) > 45 || IsPlayerMovingBackwards(stateContext, scriptInterface)) {
      return false;
    };
    if(GetStaticBoolParameter("requireMinCameraPitchAngleToClimb", false) && IsCameraPitchAcceptable(stateContext, scriptInterface, GetStaticFloatParameter("cameraPitchThreshold", -30))) {
      return false;
    };
    if(GetStaticBoolParameter("requireFallToClimbFromChargeJump", false) && IsInLocomotionState(stateContext, "chargeJump") && GetVerticalSpeed(scriptInterface) > 0) {
      return false;
    };
    enterAngleThreshold = GetStaticFloatParameter("inputAngleThreshold", -180);
    if(!AbsF(scriptInterface.GetInputHeading()) < enterAngleThreshold) {
      return false;
    };
    if(!MeleeUseExplorationCondition(stateContext, scriptInterface)) {
      return false;
    };
    return isObstacleSuitable;
  }

  public final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }

  public final const Bool ToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }

  private final const Bool ForwardAngleTest(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, ref<PlayerClimbInfo> climbInfo) {
    Vector4 obstaclePosition;
    Vector4 playerForward;
    Float obstacleAngle;
    Float playerMoveDirection;
    Float enterAngleThreshold;
    Float forwardAngleDifference;
    playerForward = scriptInterface.GetOwnerForward();
    obstaclePosition = climbInfo.descResult.collisionNormal;
    forwardAngleDifference = GetAngleBetween(-obstaclePosition, playerForward);
    enterAngleThreshold = GetStaticFloatParameter("obstacleEnterAngleThreshold", -180);
    if(forwardAngleDifference < enterAngleThreshold && forwardAngleDifference - 180 < enterAngleThreshold) {
      return true;
    };
    return false;
  }

  private final const Bool TestClimbingPath(ref<StateGameScriptInterface> scriptInterface, ref<PlayerClimbInfo> climbInfo, Vector4 playerPosition) {
    Vector4 playerCapsuleDimensions;
    Float tolerance;
    Vector4 climbDestination;
    EulerAngles rotation1;
    TraceResult fitTestOvelap1;
    Vector4 overlapPosition1;
    Vector4 rayCastSourcePosition1;
    Vector4 rayCastDestinationPosition1;
    TraceResult rayCastTraceResult1;
    Bool rayCastResult1;
    Bool overlapResult1;
    EulerAngles rotation2;
    TraceResult fitTestOvelap2;
    Vector4 overlapPosition2;
    Vector4 rayCastSourcePosition2;
    Vector4 rayCastDestinationPosition2;
    TraceResult rayCastTraceResult2;
    Bool rayCastResult2;
    Bool overlapResult2;
    Float groundTolerance;
    groundTolerance = 0.05000000074505806;
    tolerance = 0.15000000596046448;
    playerCapsuleDimensions.X = GetStaticFloatParameter("capsuleRadius", 0.4000000059604645);
    playerCapsuleDimensions.Y = -1;
    playerCapsuleDimensions.Z = -1;
    climbDestination = climbInfo.descResult.topPoint + GetUpVector() * playerCapsuleDimensions.X + tolerance;
    overlapPosition1 = playerPosition;
    overlapPosition1.Z = climbDestination.Z;
    rayCastSourcePosition1 = playerPosition;
    rayCastSourcePosition1.Z += groundTolerance;
    rayCastDestinationPosition1 = overlapPosition1;
    rayCastTraceResult1 = scriptInterface.RayCast(rayCastSourcePosition1, rayCastDestinationPosition1, "Simple Environment Collision");
    rayCastResult1 = IsValid(rayCastTraceResult1);
    if(!rayCastResult1) {
      overlapResult1 = scriptInterface.Overlap(playerCapsuleDimensions, overlapPosition1, rotation1, "Simple Environment Collision", fitTestOvelap1);
    };
    if(!rayCastResult1 && !overlapResult1) {
      overlapPosition2 = climbDestination;
      rayCastSourcePosition2 = overlapPosition1;
      rayCastDestinationPosition2 = overlapPosition2;
      rayCastTraceResult2 = scriptInterface.RayCast(rayCastSourcePosition2, rayCastDestinationPosition2, "Simple Environment Collision");
      rayCastResult2 = IsValid(rayCastTraceResult2);
      if(!rayCastResult2) {
        overlapResult2 = scriptInterface.Overlap(playerCapsuleDimensions, overlapPosition2, rotation2, "Simple Environment Collision", fitTestOvelap2);
      };
    };
    return !rayCastResult1 && !overlapResult1 && !rayCastResult2 && !overlapResult2;
  }

  private final const Bool OverlapFitTest(ref<StateGameScriptInterface> scriptInterface, ref<PlayerClimbInfo> climbInfo, Vector4 playerPosition) {
    EulerAngles rotation;
    Bool crouchOverlap;
    TraceResult fitTestOvelap;
    Vector4 playerCapsuleDimensions;
    Vector4 queryPosition;
    Float tolerance;
    tolerance = 0.15000000596046448;
    playerCapsuleDimensions.X = GetStaticFloatParameter("capsuleRadius", 0.4000000059604645);
    playerCapsuleDimensions.Y = -1;
    playerCapsuleDimensions.Z = -1;
    queryPosition = climbInfo.descResult.topPoint + GetUpVector() * playerCapsuleDimensions.X + tolerance;
    crouchOverlap = scriptInterface.Overlap(playerCapsuleDimensions, queryPosition, rotation, "Simple Environment Collision", fitTestOvelap);
    return !crouchOverlap;
  }
}

public class ClimbEvents extends LocomotionGroundEvents {

  public array<ref<IKTargetAddEvent>> m_ikHandEvents;

  public Bool m_shouldIkHands;

  private final ref<ClimbParameters> GetClimbParameter(ref<StateGameScriptInterface> scriptInterface) {
    Vector4 directionOffset;
    ref<PlayerClimbInfo> climbInfo;
    ref<ClimbParameters> climbParameters;
    Vector4 verticalDestination;
    Vector4 horizontalDestination;
    Vector4 playerPosition;
    Vector4 direction;
    Vector4 forwardStep;
    ref<StatsSystem> statSystem;
    Float probeDistanceFromGround;
    Float obstacleHeight;
    String climbTypeKey;
    Float climbSpeed;
    climbParameters = new ClimbParameters();
    probeDistanceFromGround = GetFloat("player.detectionHeight", 0.6499999761581421);
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    direction = scriptInterface.GetOwnerForward();
    directionOffset = direction * GetStaticFloatParameter("capsuleRadius", 0);
    climbInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    playerPosition = GetPlayerPosition(scriptInterface);
    climbParameters.SetObstacleFrontEdgePosition(climbInfo.descResult.topPoint);
    climbParameters.SetObstacleFrontEdgeNormal(climbInfo.descResult.collisionNormal);
    climbParameters.SetObstacleVerticalDestination(climbInfo.descResult.topPoint - directionOffset);
    climbParameters.SetObstacleSurfaceNormal(climbInfo.descResult.topNormal);
    climbParameters.SetObstacleHorizontalDestination(climbInfo.descResult.topPoint + direction * GetStaticFloatParameter("forwardStep", 0.5));
    obstacleHeight = climbInfo.descResult.topPoint.Z - playerPosition.Z;
    if(obstacleHeight > GetStaticFloatParameter("highThreshold", 1)) {
      climbParameters.SetClimbType(0);
      climbTypeKey = "High";
      this.m_shouldIkHands = true;
    } else {
      if(obstacleHeight > GetStaticFloatParameter("midThreshold", 1)) {
        climbParameters.SetClimbType(1);
        climbTypeKey = "Mid";
        this.m_shouldIkHands = true;
      } else {
        climbParameters.SetClimbType(2);
        climbTypeKey = "Low";
        this.m_shouldIkHands = false;
      };
    };
    climbSpeed = GetStatFloatValue(scriptInterface, gamedataStatType.ClimbSpeedModifier, statSystem);
    if(climbSpeed < 0) {
      climbSpeed = 1;
    };
    climbParameters.SetHorizontalDuration(climbSpeed * GetStaticFloatParameter("horizontalDuration" + climbTypeKey, 10));
    climbParameters.SetVerticalDuration(climbSpeed * GetStaticFloatParameter("verticalDuration" + climbTypeKey, 10));
    climbParameters.SetAnimationNameApproach(GetStaticCNameParameter("animationNameApproach", ""));
    return climbParameters;
  }

  private final void CreateIKConstraint(ref<StateGameScriptInterface> scriptInterface, HandIKDescriptionResult handData, Vector4 refUpVector, CName ikChainName) {
    ref<IKTargetAddEvent> ikEvent;
    Vector4 edgeSlop;
    Matrix handOrientation;
    Vector4 handNormal;
    ikEvent = new IKTargetAddEvent();
    edgeSlop = handData.grabPointStart - handData.grabPointEnd;
    handNormal = Cross(edgeSlop, refUpVector);
    handNormal = Normalize(handNormal);
    handNormal.Z = 0.30000001192092896;
    handNormal = Normalize(handNormal);
    handOrientation = BuildFromDirectionVector(handNormal, edgeSlop);
    ikEvent.SetStaticTarget(handData.grabPointEnd + edgeSlop * 0.5);
    ikEvent.SetStaticOrientationTarget(ToQuat(handOrientation));
    ikEvent.request.transitionIn = 0;
    ikEvent.request.priority = -100;
    ikEvent.bodyPart = ikChainName;
    WeakRefToRef(scriptInterface.owner).QueueEvent(ikEvent);
    Push(this.m_ikHandEvents, ikEvent);
  }

  private final void AddHandIK(ref<StateGameScriptInterface> scriptInterface) {
    ref<PlayerClimbInfo> climbInfo;
    climbInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    CreateIKConstraint(scriptInterface, climbInfo.descResult.leftHandData, new Vector4(0,0,1,0), "ikLeftArm");
    CreateIKConstraint(scriptInterface, climbInfo.descResult.rightHandData, new Vector4(0,0,-1,0), "ikRightArm");
  }

  private final void RemoveHandIK(ref<StateGameScriptInterface> scriptInterface) {
    ref<IKTargetAddEvent> ikEvent;
    Int32 i;
    i = 0;
    while(i < Size(this.m_ikHandEvents)) {
      ikEvent = this.m_ikHandEvents[i];
      if(!ToBool(ikEvent)) {
      } else {
        QueueRemoveIkTargetRemoveEvent(WeakRefToRef(scriptInterface.owner), ikEvent);
      };
      i += 1;
    };
    Clear(this.m_ikHandEvents);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<ClimbParameters> climbParameters;
    scriptInterface.SetAnimationParameterFeature("PreClimb", Cast(stateContext.GetConditionScriptableParameter("PreClimbAnimFeature")));
    stateContext.RemoveConditionScriptableParameter("PreClimbAnimFeature");
    OnEnter(stateContext, scriptInterface);
    PlayVoiceOver(WeakRefToRef(scriptInterface.owner), "climbStart", "Scripts:ClimbEvents");
    stateContext.SetTemporaryScriptableParameter("climbInfo", GetClimbParameter(scriptInterface), true);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Climb);
    PlayRumble(scriptInterface, "light_fast");
    if(this.m_shouldIkHands) {
      AddHandIK(scriptInterface);
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    RemoveHandIK(scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    RemoveHandIK(scriptInterface);
  }
}

public class VaultDecisions extends LocomotionGroundDecisions {

  public const Bool stateBodyDone;

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  private final const Bool SpeedTest(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, ref<PlayerClimbInfo> vaultInfo) {
    Float maxSpeedNormalizer;
    Float normalizedSpeed;
    Float detectionRange;
    Float maxExtent;
    Float obstacleExtent;
    Float offsetFromObstacleInVelocityVectorMag;
    Float maxClimbableExtentFromCurve;
    Float maxClimbableDistanceFromCurve;
    Float minDetectionRange;
    Float midObstacleDepth;
    Vector4 linearVelocity;
    Vector4 playerForward;
    Vector4 offsetFromObstacle;
    Vector4 playerPosition;
    Vector4 offsetFromObstacleInVelocityVector;
    Bool resVelocity;
    Bool resDepth;
    minDetectionRange = GetStaticFloatParameter("minDetectionRange", 0.4000000059604645);
    midObstacleDepth = GetStaticFloatParameter("minExtent", 0.009999999776482582);
    maxSpeedNormalizer = GetStaticFloatParameter("maxSpeedNormalizer", 8.5);
    detectionRange = GetStaticFloatParameter("detectionRange", 2);
    linearVelocity = GetLinearVelocity(scriptInterface);
    normalizedSpeed = MinF(1, Length(linearVelocity) / maxSpeedNormalizer);
    playerPosition = GetPlayerPosition(scriptInterface);
    offsetFromObstacle = vaultInfo.descResult.topPoint - playerPosition;
    playerForward = scriptInterface.GetOwnerForward();
    offsetFromObstacleInVelocityVector = playerForward * Dot(playerForward, offsetFromObstacle);
    offsetFromObstacleInVelocityVectorMag = Length(offsetFromObstacleInVelocityVector);
    maxExtent = GetStaticFloatParameter("maxExtent", 2.0999999046325684);
    obstacleExtent = vaultInfo.descResult.topExtent;
    maxClimbableDistanceFromCurve = minDetectionRange + detectionRange - minDetectionRange * normalizedSpeed + 0.05000000074505806;
    maxClimbableExtentFromCurve = midObstacleDepth + maxExtent - midObstacleDepth * normalizedSpeed;
    resVelocity = offsetFromObstacleInVelocityVectorMag < maxClimbableDistanceFromCurve;
    resDepth = obstacleExtent < maxClimbableExtentFromCurve;
    return resVelocity && resDepth;
  }

  protected final const Bool FitTest(ref<StateGameScriptInterface> scriptInterface, Vector4 playerCapsuleDimensions, ref<PlayerClimbInfo> vaultInfo) {
    TraceResult fitTest;
    Vector4 queryPosition;
    Vector4 direction;
    Vector4 playerForward;
    Vector4 playerPosition;
    Float distance;
    EulerAngles rotation;
    Bool freeSpace;
    Bool deltaZ;
    playerForward = scriptInterface.GetOwnerForward();
    playerPosition = GetPlayerPosition(scriptInterface);
    distance = vaultInfo.descResult.topExtent;
    direction = playerForward * distance;
    direction = Normalize(direction);
    queryPosition = vaultInfo.descResult.topPoint + GetUpVector() * playerCapsuleDimensions.X + 0.009999999776482582;
    freeSpace = !scriptInterface.Sweep(playerCapsuleDimensions, queryPosition, rotation, direction, distance, "Simple Environment Collision", false, fitTest);
    deltaZ = vaultInfo.descResult.behindPoint.Z - playerPosition.Z < 0.4000000059604645;
    return freeSpace && deltaZ;
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<PlayerClimbInfo> vaultInfo;
    Float enterAngleThreshold;
    Vector4 playerCapsuleDimensions;
    if(IsVaultingClimbingRestricted(scriptInterface)) {
      return false;
    };
    if(GetStaticBoolParameter("requireDirectionalInputToVault", false) && !scriptInterface.IsMoveInputConsiderable()) {
      return false;
    };
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", -180);
    if(AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold) {
      return false;
    };
    if(!MeleeUseExplorationCondition(stateContext, scriptInterface)) {
      return false;
    };
    vaultInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    playerCapsuleDimensions.X = GetStaticFloatParameter("capsuleRadius", 0.4000000059604645);
    playerCapsuleDimensions.Y = -1;
    playerCapsuleDimensions.Z = -1;
    return vaultInfo.vaultValid && FitTest(scriptInterface, playerCapsuleDimensions, vaultInfo) && SpeedTest(stateContext, scriptInterface, vaultInfo) && scriptInterface.IsActionJustPressed("Jump");
  }

  public final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }

  public final const Bool ToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return this.stateBodyDone;
  }
}

public class VaultEvents extends LocomotionGroundEvents {

  protected final ref<VaultParameters> GetVaultParameter(ref<StateGameScriptInterface> scriptInterface) {
    Vector4 directionOffset;
    Vector4 playerPosition;
    ref<PlayerClimbInfo> climbInfo;
    ref<VaultParameters> vaultParameters;
    Vector4 direction;
    Vector4 landingPoint;
    Vector4 obstacleEnd;
    Float statAcceleration;
    Float statDeceleration;
    Float statMinSpeed;
    Float landingDistance;
    Float behindZ;
    ref<StatsSystem> statSystem;
    playerPosition = GetPlayerPosition(scriptInterface);
    statSystem = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    vaultParameters = new VaultParameters();
    climbInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    direction = scriptInterface.GetOwnerForward();
    directionOffset = direction * GetStaticFloatParameter("capsuleRadius", 0);
    vaultParameters.SetObstacleFrontEdgePosition(climbInfo.descResult.topPoint);
    vaultParameters.SetObstacleFrontEdgeNormal(climbInfo.descResult.collisionNormal);
    vaultParameters.SetObstacleVerticalDestination(climbInfo.descResult.topPoint);
    vaultParameters.SetObstacleSurfaceNormal(climbInfo.descResult.topNormal);
    obstacleEnd = climbInfo.obstacleEnd;
    behindZ = MaxF(climbInfo.descResult.behindPoint.Z, playerPosition.Z);
    landingPoint.X = obstacleEnd.X;
    landingPoint.Y = obstacleEnd.Y;
    landingPoint.Z = behindZ;
    vaultParameters.SetObstacleDestination(landingPoint + direction * GetStaticFloatParameter("forwardStep", 0.5));
    vaultParameters.SetObstacleDepth(climbInfo.descResult.topExtent);
    vaultParameters.SetMinSpeed(GetStaticFloatParameter("minSpeed", 3.5));
    return vaultParameters;
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetTemporaryScriptableParameter("vaultInfo", GetVaultParameter(scriptInterface), true);
    scriptInterface.PushAnimationEvent("Vault");
    PlayVoiceOver(WeakRefToRef(scriptInterface.owner), "Vault", "Scripts:VaultEvents");
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanWeaponReloadWhileVaulting)) {
      stateContext.SetTemporaryBoolParameter("InterruptReload", true, true);
    };
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Vault);
    PlayRumble(scriptInterface, "medium_pulse");
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    stateContext.SetPermanentBoolParameter("ForceSafeState", false, true);
  }
}

public class LadderDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected final const Bool TestParameters(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, out ref<LadderDescription> ladderParameter) {
    StateResultBool ladderFinishedParameter;
    ladderParameter = Cast(stateContext.GetTemporaryScriptableParameter("enterLadder"));
    if(!ToBool(ladderParameter)) {
      ladderParameter = Cast(stateContext.GetConditionScriptableParameter("enterLadder"));
      ladderFinishedParameter = stateContext.GetTemporaryBoolParameter("exitLadder");
      if(ladderFinishedParameter.valid && ladderFinishedParameter.value) {
        stateContext.RemoveConditionScriptableParameter("enterLadder");
        return false;
      };
      if(!ToBool(ladderParameter)) {
        return false;
      };
    } else {
      stateContext.SetConditionScriptableParameter("enterLadder", ladderParameter, true);
    };
    return true;
  }

  protected final const Bool IsActionEnterLadderUsed(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool isActionEnterLadder;
    isActionEnterLadder = stateContext.GetTemporaryBoolParameter("actionEnterLadder");
    if(isActionEnterLadder.valid && isActionEnterLadder.value) {
      return true;
    };
    return false;
  }

  protected final const Bool IsLadderEnterInProgress(ref<StateContext> stateContext) {
    StateResultBool isEntering;
    isEntering = stateContext.GetPermanentBoolParameter("setLadderEnterInputContext");
    if(isEntering.valid && isEntering.value) {
      return true;
    };
    return false;
  }

  protected final const Bool TestMath(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, ref<LadderDescription> ladderParameter) {
    Bool inAir;
    Bool playerMovingForward;
    Vector4 playerPosition;
    Vector4 ladderPosition;
    Vector4 playerVelocity;
    Vector4 directionToLadder;
    Vector4 playerForward;
    Float ladderEntityAngle;
    Float playerMoveDirection;
    Float fromBottomFactor;
    Float enterAngleThreshold;
    playerPosition = GetPlayerPosition(scriptInterface);
    playerForward = scriptInterface.GetOwnerForward();
    playerVelocity = Normalize2D(RotByAngleXY(playerForward, scriptInterface.GetInputHeading()));
    ladderPosition = ladderParameter.position - ladderParameter.normal + ladderParameter.up * ladderParameter.verticalStepBottom;
    directionToLadder = ladderPosition - playerPosition;
    directionToLadder = Normalize2D(directionToLadder);
    ladderEntityAngle = Rad2Deg(AcosF(Dot(playerForward, directionToLadder)));
    playerMoveDirection = Rad2Deg(AcosF(Dot(playerVelocity, -ladderParameter.normal)));
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", 35);
    fromBottomFactor = SgnF(Dot(ladderParameter.up, directionToLadder));
    inAir = !IsTouchingGround(scriptInterface);
    playerMovingForward = !IsPlayerMovingBackwards(stateContext, scriptInterface);
    if(inAir && playerMovingForward) {
      if(fromBottomFactor > 0 && AbsF(ladderEntityAngle) < enterAngleThreshold) {
        return true;
      };
      if(fromBottomFactor < 0 && AbsF(ladderEntityAngle) < enterAngleThreshold) {
        return true;
      };
    } else {
      if(scriptInterface.IsMoveInputConsiderable() && playerMovingForward && fromBottomFactor > 0 && AbsF(ladderEntityAngle) < enterAngleThreshold && AbsF(playerMoveDirection) < enterAngleThreshold) {
        return true;
      };
    };
    return false;
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool finishedLadder;
    finishedLadder = stateContext.GetTemporaryBoolParameter("finishedLadderAction");
    return finishedLadder.valid && finishedLadder.value;
  }

  protected final const Bool ToLadderCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustPressed("Crouch") || scriptInterface.IsActionJustTapped("ToggleCrouch")) {
      return true;
    };
    return false;
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<LadderDescription> ladderParameter;
    Bool testParameters;
    Bool testMath;
    Bool isActionEnterLadder;
    isActionEnterLadder = IsActionEnterLadderUsed(stateContext, scriptInterface);
    testParameters = TestParameters(stateContext, scriptInterface, ladderParameter);
    if(HasRestriction(scriptInterface.executionOwner, "NoLadder")) {
      return false;
    };
    if(ladderParameter == null && !isActionEnterLadder) {
      return false;
    };
    if(!MeleeUseExplorationCondition(stateContext, scriptInterface)) {
      return false;
    };
    testMath = TestMath(stateContext, scriptInterface, ladderParameter);
    return testParameters && testMath || isActionEnterLadder;
  }
}

public class LadderEvents extends LocomotionGroundEvents {

  [Default(LadderEvents, 0.f))]
  public Float m_ladderClimbCameraTimeStamp;

  protected void SendLadderEnterStyleToGraph(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Int32 enterStyle) {
    ref<AnimFeature_LadderEnterStyleData> animFeature;
    animFeature = new AnimFeature_LadderEnterStyleData();
    animFeature.enterStyle = enterStyle;
    scriptInterface.SetAnimationParameterFeature("LadderEnterStyleData", animFeature);
  }

  public final void OnEnterFromJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendLadderEnterStyleToGraph(stateContext, scriptInterface, 1);
    OnEnter(stateContext, scriptInterface);
  }

  public final void OnEnterFromDoubleJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendLadderEnterStyleToGraph(stateContext, scriptInterface, 2);
    OnEnter(stateContext, scriptInterface);
  }

  public final void OnEnterFromChargeJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendLadderEnterStyleToGraph(stateContext, scriptInterface, 3);
    OnEnter(stateContext, scriptInterface);
  }

  public final void OnEnterFromDodgeAir(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendLadderEnterStyleToGraph(stateContext, scriptInterface, 4);
    OnEnter(stateContext, scriptInterface);
  }

  public final void OnEnterFromFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendLadderEnterStyleToGraph(stateContext, scriptInterface, 5);
    OnEnter(stateContext, scriptInterface);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool ignoreUpdatingCameraParemeters;
    OnUpdate(timeDelta, stateContext, scriptInterface);
    if(GetInStateTime(stateContext, scriptInterface) >= GetCameraInputLockDuration(stateContext)) {
      SetLadderEnterInProgress(stateContext, false);
    };
    ignoreUpdatingCameraParemeters = IsInLocomotionState(stateContext, "ladderCrouch") || IsInLocomotionState(stateContext, "ladderJump") || IsInLocomotionState(stateContext, "ladderSprint") || IsInLocomotionState(stateContext, "ladderSlide");
    if(ignoreUpdatingCameraParemeters) {
      UseLadderEnterClimbCameraContext(stateContext, false);
      UseLadderCameraContext(stateContext, false);
      UseLadderClimbCameraContext(stateContext, false);
      return ;
    };
    if(GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("ladderEnterCameraMinActiveTime", 0) && !GetParameterBool("isEnterClimbCameraContextSet", stateContext, true)) {
      stateContext.SetPermanentBoolParameter("isEnterClimbCameraContextSet", true, true);
      UseLadderEnterClimbCameraContext(stateContext, false);
      UseLadderCameraContext(stateContext, true);
    };
    if(WantsToUseLadderClimbCameraContext(stateContext, scriptInterface) && !IsPlayerLookingAtTheLadder(stateContext, scriptInterface) && !GetParameterBool("isLadderClimbCameraContextSet", stateContext, true)) {
      stateContext.SetPermanentBoolParameter("isLadderClimbCameraContextSet", true, true);
      stateContext.RemovePermanentBoolParameter("isLadderCameraContextSet");
      this.m_ladderClimbCameraTimeStamp = GetInStateTime(stateContext, scriptInterface);
      UseLadderCameraContext(stateContext, false);
      UseLadderClimbCameraContext(stateContext, true);
    };
    if(GetInStateTime(stateContext, scriptInterface) >= this.m_ladderClimbCameraTimeStamp + GetStaticFloatParameter("climbCameraMinActiveTime", 0) && !GetParameterBool("isLadderCameraContextSet", stateContext, true)) {
      stateContext.RemovePermanentBoolParameter("isLadderClimbCameraContextSet");
      stateContext.SetPermanentBoolParameter("isLadderCameraContextSet", true, true);
      this.m_ladderClimbCameraTimeStamp = 0;
      UseLadderClimbCameraContext(stateContext, false);
      UseLadderCameraContext(stateContext, true);
    };
  }

  protected final Bool WantsToUseLadderClimbCameraContext(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsMoveInputConsiderable() && GetMovementInputActionValue(stateContext, scriptInterface) >= GetStaticFloatParameter("minStickInputToSwapToClimbCamera", 0) && IsMovingVertically(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool IsPlayerLookingAtTheLadder(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<GeometryDescriptionQuery> geometryDescription;
    QueryFilter staticQueryFilter;
    ref<GeometryDescriptionResult> geometryDescriptionResult;
    Vector4 targetPosition;
    Transform cameraWorldTransform;
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    AddGroup(staticQueryFilter, "Interaction");
    geometryDescription = new GeometryDescriptionQuery();
    geometryDescription.refPosition = GetPosition(cameraWorldTransform);
    geometryDescription.refDirection = GetForward(cameraWorldTransform);
    geometryDescription.filter = staticQueryFilter;
    geometryDescription.primitiveDimension = new Vector4(0.10000000149011612,0.10000000149011612,0.10000000149011612,0);
    geometryDescription.maxDistance = 2;
    geometryDescription.maxExtent = 0.5;
    geometryDescription.probingPrecision = 0.05000000074505806;
    geometryDescription.probingMaxDistanceDiff = 2;
    geometryDescription.AddFlag(worldgeometryDescriptionQueryFlags.DistanceVector);
    geometryDescriptionResult = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).GetGeometryDescriptionSystem().QueryExtents(geometryDescription);
    if(geometryDescriptionResult.queryStatus == worldgeometryDescriptionQueryStatus.NoGeometry) {
      return false;
    };
    return true;
  }

  protected final Bool IsMovingVertically(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float verticalSpeed;
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    return verticalSpeed != 0;
  }

  protected final void UseLadderClimbCameraContext(ref<StateContext> stateContext, Bool value) {
    stateContext.SetPermanentBoolParameter("setLadderClimbCameraContext", value, true);
  }

  protected final void UseLadderEnterClimbCameraContext(ref<StateContext> stateContext, Bool value) {
    stateContext.SetPermanentBoolParameter("setLadderEnterClimbCameraContext", value, true);
  }

  protected final void UseLadderCameraContext(ref<StateContext> stateContext, Bool value) {
    stateContext.SetPermanentBoolParameter("setLadderCameraContext", value, true);
  }

  protected final void SetLadderEnterInProgress(ref<StateContext> stateContext, Bool value) {
    stateContext.SetPermanentBoolParameter("setLadderEnterInputContext", value, true);
  }

  protected final void SetCameraInputLockDuration(ref<StateContext> stateContext) {
    StateResultBool isActionEnterLadder;
    Float inputBlockDuration;
    isActionEnterLadder = stateContext.GetTemporaryBoolParameter("actionEnterLadder");
    if(isActionEnterLadder.valid && isActionEnterLadder.value) {
      inputBlockDuration = GetStaticFloatParameter("enterFromTopBlockCameraInput", 0);
    } else {
      inputBlockDuration = GetStaticFloatParameter("enterBlockCameraInput", 0);
    };
    stateContext.SetPermanentFloatParameter("ladderEnterInputBlockDuration", inputBlockDuration, true);
  }

  protected final Float GetCameraInputLockDuration(ref<StateContext> stateContext) {
    StateResultFloat paramResult;
    paramResult = stateContext.GetPermanentFloatParameter("ladderEnterInputBlockDuration");
    return paramResult.value;
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    this.m_ladderClimbCameraTimeStamp = 0;
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Ladder);
    UseLadderEnterClimbCameraContext(stateContext, true);
    SetLadderEnterInProgress(stateContext, true);
    SetCameraInputLockDuration(stateContext);
  }

  protected final void OnExitToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 direction;
    Vector4 impulse;
    impulse = direction * GetStaticFloatParameter("exitToStandPushMagnitude", 3);
    OnExit(stateContext, scriptInterface);
    AddImpulse(stateContext, impulse);
    CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final void OnExitToLadderJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final void OnExitToLadderCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final void OnExitToKnockdown(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final void OnExitToStunned(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpLadderState(stateContext, scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpLadderState(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CleanUpLadderState(stateContext, scriptInterface);
    OnForcedExit(stateContext, scriptInterface);
  }

  protected final void CleanUpLadderState(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendLadderEnterStyleToGraph(stateContext, scriptInterface, 0);
    UseLadderEnterClimbCameraContext(stateContext, false);
    UseLadderCameraContext(stateContext, false);
    UseLadderClimbCameraContext(stateContext, false);
    stateContext.RemovePermanentBoolParameter("isEnterClimbCameraContextSet");
    stateContext.RemoveConditionScriptableParameter("enterLadder");
    SetLadderEnterInProgress(stateContext, false);
    stateContext.RemovePermanentFloatParameter("ladderEnterInputBlockDuration");
  }
}

public class LadderSprintDecisions extends LadderDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsLadderEnterInProgress(stateContext)) {
      return false;
    };
    if(!scriptInterface.IsMoveInputConsiderable() || GetVerticalSpeed(scriptInterface) < 0) {
      return false;
    };
    if(scriptInterface.IsActionJustPressed("ToggleSprint") || GetConditionParameterBool("SprintToggled", stateContext)) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
      return true;
    };
    if(scriptInterface.GetActionValue("Sprint") > 0 || scriptInterface.GetActionValue("ToggleSprint") > 0) {
      return true;
    };
    return false;
  }

  protected final const Bool ToLadder(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("InterruptSprint", stateContext)) {
      return true;
    };
    if(!scriptInterface.IsMoveInputConsiderable()) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    if(!GetConditionParameterBool("SprintToggled", stateContext) && scriptInterface.GetActionValue("Sprint") == 0) {
      return true;
    };
    if(scriptInterface.IsActionJustReleased("Sprint") || scriptInterface.IsActionJustPressed("AttackA")) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    return false;
  }
}

public class LadderSprintEvents extends LadderEvents {

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.LadderSprint);
  }
}

public class LadderSlideDecisions extends LadderDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsLadderEnterInProgress(stateContext)) {
      return false;
    };
    if(!scriptInterface.IsMoveInputConsiderable() || GetVerticalSpeed(scriptInterface) > 0) {
      return false;
    };
    if(scriptInterface.IsActionJustPressed("ToggleSprint") || GetConditionParameterBool("SprintToggled", stateContext)) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
      return true;
    };
    if(scriptInterface.GetActionValue("Sprint") > 0 || scriptInterface.GetActionValue("ToggleSprint") > 0) {
      return true;
    };
    return false;
  }

  protected final const Bool ToLadder(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetParameterBool("InterruptSprint", stateContext)) {
      return true;
    };
    if(!scriptInterface.IsMoveInputConsiderable() || GetVerticalSpeed(scriptInterface) > 0) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    if(!GetConditionParameterBool("SprintToggled", stateContext) && scriptInterface.GetActionValue("Sprint") == 0) {
      return true;
    };
    if(scriptInterface.IsActionJustReleased("Sprint") || scriptInterface.IsActionJustPressed("AttackA")) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    return false;
  }
}

public class LadderSlideEvents extends LadderEvents {

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface)

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.LadderSlide);
  }
}

public class LadderJumpEvents extends LocomotionAirEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Transform cameraTransform;
    Vector4 jumpDirection;
    Transform ownerTransform;
    Vector4 ownerRight;
    Vector4 horizontalCameraDirection;
    Quaternion cameraAngle;
    Float pitchAngle;
    Float cameraEntityRightDot;
    Float rightMultiplier;
    Float forwardMultiplier;
    Float angleToleranceForLateralJump;
    Float sideDirectionAbs;
    pitchAngle = GetStaticFloatParameter("pitchAngle", 45);
    rightMultiplier = 0;
    forwardMultiplier = 0;
    angleToleranceForLateralJump = 0;
    sideDirectionAbs = 0;
    OnEnter(stateContext, scriptInterface);
    ownerTransform = scriptInterface.GetOwnerTransform();
    ownerRight = GetRight(ownerTransform);
    cameraTransform = scriptInterface.GetCameraWorldTransform();
    horizontalCameraDirection = GetForward(cameraTransform);
    horizontalCameraDirection.Z = 0;
    horizontalCameraDirection = Normalize(horizontalCameraDirection);
    ownerRight.Z = 0;
    ownerRight = Normalize(ownerRight);
    cameraEntityRightDot = Rad2Deg(AcosF(Dot(horizontalCameraDirection, ownerRight)));
    angleToleranceForLateralJump = GetStaticFloatParameter("angleToleranceForLateralJump", 30);
    sideDirectionAbs = AbsF(cameraEntityRightDot);
    if(sideDirectionAbs < 90 - angleToleranceForLateralJump) {
      if(angleToleranceForLateralJump > 0) {
        rightMultiplier = 1;
      };
    } else {
      if(sideDirectionAbs > 90 + angleToleranceForLateralJump) {
        rightMultiplier = -1;
      } else {
        forwardMultiplier = -1;
      };
    };
    jumpDirection.X = rightMultiplier;
    jumpDirection.Y = forwardMultiplier;
    jumpDirection.Z = SinF(Deg2Rad(pitchAngle));
    jumpDirection = Normalize(jumpDirection);
    jumpDirection = TransformVector(ownerTransform, jumpDirection);
    AddImpulse(stateContext, jumpDirection * GetStaticFloatParameter("impulseStrength", 4));
    stateContext.SetTemporaryBoolParameter("finishedLadderAction", true, true);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.LadderJump);
  }
}

public abstract class LocomotionAirDecisions extends LocomotionTransition {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }

  protected final const Bool ShouldFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float regularLandingFallingSpeed;
    Float verticalSpeed;
    if(IsTouchingGround(scriptInterface)) {
      return false;
    };
    IsTouchingGround(scriptInterface);
    if(scriptInterface.IsOnMovingPlatform()) {
      return false;
    };
    if(GetParameterBool("isAttacking", stateContext, true)) {
      return true;
    };
    regularLandingFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("regularLandingHeight", 0.10000000149011612));
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    return verticalSpeed < regularLandingFallingSpeed;
  }

  protected final const Int32 GetLandingType(ref<StateContext> stateContext) {
    return GetParameterInt("LandingType", stateContext, true);
  }

  protected const Bool ToRegularLand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 landingType;
    landingType = GetLandingType(stateContext);
    if(!IsTouchingGround(scriptInterface) || GetVerticalSpeed(scriptInterface) > 0) {
      return false;
    };
    return landingType < ToInt(LandingType.Regular);
  }

  protected final const Bool ToHardLand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 landingType;
    if(!IsTouchingGround(scriptInterface) || GetVerticalSpeed(scriptInterface) > 0) {
      return false;
    };
    landingType = GetLandingType(stateContext);
    return landingType == ToInt(LandingType.Hard);
  }

  protected final const Bool ToVeryHardLand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 landingType;
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    landingType = GetLandingType(stateContext);
    return landingType == ToInt(LandingType.VeryHard);
  }

  protected final const Bool ToSuperheroLand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 landingType;
    landingType = GetLandingType(stateContext);
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    return landingType == ToInt(LandingType.Superhero);
  }

  protected final const Bool ToDeathLand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 landingType;
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    landingType = GetLandingType(stateContext);
    return landingType == ToInt(LandingType.Death);
  }
}

public abstract class LocomotionAirEvents extends LocomotionEventsTransition {

  [Default(LocomotionAirEvents, false))]
  public Bool m_maxSuperheroFallHeight;

  [Default(AirThrustersEvents, false))]
  [Default(DodgeAirEvents, false))]
  [Default(LocomotionAirEvents, true))]
  public Bool m_updateInputToggles;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float regularLandingFallingSpeed;
    Float safeLandingFallingSpeed;
    Float hardLandingFallingSpeed;
    Float veryHardLandingFallingSpeed;
    Float deathLandingFallingSpeed;
    ref<AnimFeature_PlayerLocomotionStateMachine> animFeature;
    OnEnter(stateContext, scriptInterface);
    regularLandingFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("regularLandingHeight", 0.10000000149011612));
    safeLandingFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("safeLandingHeight", 0.10000000149011612));
    hardLandingFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("hardLandingHeight", 1));
    veryHardLandingFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("veryHardLandingHeight", 1));
    deathLandingFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("deathLanding", 1));
    stateContext.SetPermanentFloatParameter("RegularLandingFallingSpeed", regularLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter("SafeLandingFallingSpeed", safeLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter("HardLandingFallingSpeed", hardLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter("VeryHardLandingFallingSpeed", veryHardLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter("DeathLandingFallingSpeed", deathLandingFallingSpeed, true);
    animFeature = new AnimFeature_PlayerLocomotionStateMachine();
    animFeature.inAirState = true;
    scriptInterface.SetAnimationParameterFeature("LocomotionStateMachine", animFeature);
    scriptInterface.PushAnimationEvent("InAir");
    GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).SetIsMovingFast(scriptInterface.owner, true);
    this.m_maxSuperheroFallHeight = false;
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float verticalSpeed;
    Float horizontalSpeed;
    Vector4 playerVelocity;
    Float regularLandingFallingSpeed;
    Float safeLandingFallingSpeed;
    Float hardLandingFallingSpeed;
    Float veryHardLandingFallingSpeed;
    Float deathLandingFallingSpeed;
    Bool isInSuperheroFall;
    Float maxAllowedDistanceToGround;
    LandingType landingType;
    ref<AnimFeature_Landing> landingAnimFeature;
    OnUpdate(timeDelta, stateContext, scriptInterface);
    if(IsTouchingGround(scriptInterface)) {
      return ;
    };
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    if(this.m_updateInputToggles && verticalSpeed < GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("minFallHeightToConsiderInputToggles", 0))) {
      UpdateInputToggles(stateContext, scriptInterface);
    };
    if(scriptInterface.IsActionJustPressed("Jump")) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
      return ;
    };
    if(HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.BerserkPlayerBuff") && verticalSpeed < GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("minFallHeightToEnterSuperheroFall", 0))) {
      stateContext.SetTemporaryBoolParameter("requestSuperheroLandActivation", true, true);
    };
    regularLandingFallingSpeed = GetParameterFloat("RegularLandingFallingSpeed", stateContext, true);
    safeLandingFallingSpeed = GetParameterFloat("SafeLandingFallingSpeed", stateContext, true);
    hardLandingFallingSpeed = GetParameterFloat("HardLandingFallingSpeed", stateContext, true);
    veryHardLandingFallingSpeed = GetParameterFloat("VeryHardLandingFallingSpeed", stateContext, true);
    deathLandingFallingSpeed = GetParameterFloat("DeathLandingFallingSpeed", stateContext, true);
    isInSuperheroFall = IsInLocomotionState(stateContext, "superheroFall");
    maxAllowedDistanceToGround = GetStaticFloatParameter("maxDistToGroundFromSuperheroFall", 20);
    if(isInSuperheroFall && !this.m_maxSuperheroFallHeight) {
      StartEffect(scriptInterface, "falling");
      PlaySound("lcm_falling_wind_loop", scriptInterface);
      if(GetDistanceToGround(scriptInterface) >= maxAllowedDistanceToGround) {
        this.m_maxSuperheroFallHeight = true;
        return ;
      };
      landingType = LandingType.Superhero;
    } else {
      if(verticalSpeed < deathLandingFallingSpeed && !IsMultiplayer() && !GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap)) {
        landingType = LandingType.Death;
        SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, ToInt(gamePSMFallStates.DeathFall));
      } else {
        if(verticalSpeed < veryHardLandingFallingSpeed && !IsMultiplayer() && !GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap)) {
          landingType = LandingType.VeryHard;
          SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, ToInt(gamePSMFallStates.VeryFastFall));
        } else {
          if(verticalSpeed < hardLandingFallingSpeed && !IsMultiplayer()) {
            landingType = LandingType.Hard;
            if(GetLandingType(stateContext) != ToInt(LandingType.Hard)) {
              StartEffect(scriptInterface, "falling");
            };
            SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, ToInt(gamePSMFallStates.FastFall));
          } else {
            if(verticalSpeed < safeLandingFallingSpeed && !IsMultiplayer()) {
              landingType = LandingType.Regular;
              SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, ToInt(gamePSMFallStates.RegularFall));
              playerVelocity = GetLinearVelocity(scriptInterface);
              horizontalSpeed = Length2D(playerVelocity);
              if(horizontalSpeed < GetStaticFloatParameter("maxHorizontalSpeedToAerialTakedown", 0)) {
                SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, ToInt(gamePSMFallStates.SafeFall));
              };
            } else {
              if(verticalSpeed < regularLandingFallingSpeed) {
                if(GetLandingType(stateContext) != ToInt(LandingType.Regular)) {
                  PlaySound("lcm_falling_wind_loop", scriptInterface);
                };
                landingType = LandingType.Regular;
              } else {
                landingType = LandingType.Off;
              };
            };
          };
        };
      };
    };
    stateContext.SetPermanentIntParameter("LandingType", ToInt(landingType), true);
    stateContext.SetPermanentFloatParameter("ImpactSpeed", verticalSpeed, true);
    stateContext.SetPermanentFloatParameter("InAirDuration", GetInStateTime(stateContext, scriptInterface), true);
    landingAnimFeature = new AnimFeature_Landing();
    landingAnimFeature.impactSpeed = verticalSpeed;
    landingAnimFeature.type = ToInt(landingType);
    scriptInterface.SetAnimationParameterFeature("Landing", landingAnimFeature);
    SetAudioParameter("RTPC_Vertical_Velocity", verticalSpeed, scriptInterface);
    SetAudioParameter("RTPC_Landing_Type", Cast(ToInt(landingType)), scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    StopEffect(scriptInterface, "falling");
    PlaySound("lcm_falling_wind_loop_end", scriptInterface);
    GetTargetingSystem(WeakRefToRef(scriptInterface.owner).GetGame()).SetIsMovingFast(scriptInterface.owner, false);
  }

  protected final const Int32 GetLandingType(ref<StateContext> stateContext) {
    return GetParameterInt("LandingType", stateContext, true);
  }
}

public class FallDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool shouldFall;
    shouldFall = ShouldFall(stateContext, scriptInterface);
    if(shouldFall) {
      GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("StartFalling");
    };
    return shouldFall;
  }
}

public class FallEvents extends LocomotionAirEvents {

  public final void OnEnterFromDodgeAir(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetPermanentBoolParameter("enteredFallFromAirDodge", true, true);
    OnEnter(stateContext, scriptInterface);
  }

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    PlaySound("lcm_falling_wind_loop", scriptInterface);
    scriptInterface.PushAnimationEvent("Fall");
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Fall);
  }
}

public class UnsecureFootingFallDecisions extends FallDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SecureFootingResult secureFootingResult;
    Vector4 linearVelocity;
    secureFootingResult = scriptInterface.HasSecureFooting();
    linearVelocity = GetLinearVelocity(scriptInterface);
    if(IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface)) {
      return false;
    };
    return secureFootingResult.type == moveSecureFootingFailureType.Edge && linearVelocity.Z < GetStaticFloatParameter("minVerticalVelocityToEnter", -0.30000001192092896);
  }
}

public class UnsecureFootingFallEvents extends FallEvents {

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 linearVelocity;
    SecureFootingResult secureFootingResult;
    linearVelocity = GetLinearVelocity(scriptInterface);
    secureFootingResult = scriptInterface.HasSecureFooting();
    OnEnter(stateContext, scriptInterface);
    AddImpulse(stateContext, secureFootingResult.slidingDirection * AbsF(linearVelocity.Z) * GetStaticFloatParameter("slideImpulseModifier", 0.20000000298023224));
  }
}

public class AirThrustersDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool shouldFall;
    Float minInputHoldTime;
    Bool autoActivationNearGround;
    shouldFall = ShouldFall(stateContext, scriptInterface);
    if(shouldFall) {
      GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("StartFalling");
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasAirThrusters) && !GetStaticBoolParameter("debug_Enable_Air_Thrusters", false)) {
      return false;
    };
    minInputHoldTime = GetStaticFloatParameter("minInputHoldTime", 0.15000000596046448);
    if(scriptInterface.GetActionValue("Jump") > 0 && scriptInterface.GetActionStateTime("Jump") > minInputHoldTime) {
      return GetDistanceToGround(scriptInterface) >= GetStaticFloatParameter("minDistanceToGround", 0);
    };
    autoActivationNearGround = GetStaticBoolParameter("autoActivationAboutToHitGround", true);
    if(autoActivationNearGround && IsFallHeightAcceptable(stateContext, scriptInterface)) {
      return GetDistanceToGround(scriptInterface) < GetStaticFloatParameter("autoActivationDistanceToGround", 0);
    };
    return false;
  }

  protected final const Bool IsFallHeightAcceptable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float acceptableFallingSpeed;
    Float verticalSpeed;
    acceptableFallingSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, GetStaticFloatParameter("minFallHeight", 3));
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    if(verticalSpeed < acceptableFallingSpeed) {
      return true;
    };
    return false;
  }

  protected final const Bool ToFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasAirThrusters)) {
      return true;
    };
    if(GetStaticBoolParameter("autoTransitionToFall", true) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("stateDuration", 0)) {
      return true;
    };
    if(!GetStaticBoolParameter("autoTransitionToFall", true) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("stateDuration", 0) && scriptInterface.IsActionJustTapped("ToggleCrouch") || scriptInterface.IsActionJustPressed("Crouch")) {
      return true;
    };
    if(GetStaticBoolParameter("allowCancelingWithCrouchAction", true) && scriptInterface.IsActionJustTapped("ToggleCrouch") || scriptInterface.IsActionJustPressed("Crouch")) {
      return true;
    };
    if(GetDistanceToGround(scriptInterface) < GetStaticFloatParameter("minDistanceToGround", 0)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsTouchingGround(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const Bool ToDoubleJump(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasDoubleJump)) {
      return false;
    };
    if(GetParameterInt("currentNumberOfJumps", stateContext, true) >= 2) {
      return false;
    };
    if(GetConditionParameterBool("JumpPressed", stateContext) || scriptInterface.IsActionJustPressed("Jump")) {
      return true;
    };
    return false;
  }
}

public class AirThrustersEvents extends LocomotionAirEvents {

  protected void SendAnimFeatureDataToGraph(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Int32 state) {
    ref<AnimFeature_AirThrusterData> animFeature;
    animFeature = new AnimFeature_AirThrusterData();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature("AirThrusterData", animFeature);
  }

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SendAnimFeatureDataToGraph(stateContext, scriptInterface, 1);
    scriptInterface.SetAnimationParameterFloat("crouch", 0);
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    StopEffect(scriptInterface, "falling");
    PlaySound("q115_thruster_start", scriptInterface);
    PlayEffectOnItem(scriptInterface, "thrusters");
    SetUpwardsThrustStats(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.AirThrusters);
    PlayRumbleLoop(scriptInterface, "light");
  }

  private final ref<ItemObject> GetActiveFeetAreaItem(ref<StateGameScriptInterface> scriptInterface) {
    ref<EquipmentSystem> es;
    ref<ItemObject> feetItem;
    es = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("EquipmentSystem"));
    feetItem = es.GetActiveWeaponObject(WeakRefToRef(scriptInterface.executionOwner), gamedataEquipmentArea.Feet);
    return feetItem;
  }

  private final void PlayEffectOnItem(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    ref<entSpawnEffectEvent> spawnEffectEvent;
    if(ToBool(GetActiveFeetAreaItem(scriptInterface))) {
      spawnEffectEvent = new entSpawnEffectEvent();
      spawnEffectEvent.effectName = effectName;
      WeakRefToRef(GetActiveFeetAreaItem(scriptInterface).GetOwner()).QueueEvent(spawnEffectEvent);
    };
  }

  protected final void StopEffectOnItem(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    ref<entKillEffectEvent> killEffectEvent;
    if(ToBool(GetActiveFeetAreaItem(scriptInterface))) {
      killEffectEvent = new entKillEffectEvent();
      killEffectEvent.effectName = effectName;
      WeakRefToRef(GetActiveFeetAreaItem(scriptInterface).GetOwner()).QueueEvent(killEffectEvent);
    };
  }

  private final void SetUpwardsThrustStats(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StatsSystem> statSystem;
    String stateMachineNameString;
    String stateNameString;
    String packageString;
    ref<LocomotionParameters> locomotionParameters;
    stateMachineNameString = NameToString(GetStateMachineName());
    packageString = "Player" + stateMachineNameString + ".";
    stateNameString = NameToString(GetStateName());
    UppercaseFirstChar(stateNameString);
    stateNameString = "player_locomotion_data_" + stateNameString;
    stateNameString = packageString + stateNameString;
    RemoveModifierGroupForState(scriptInterface);
    AddModifierGroupWithName(scriptInterface, stateNameString);
    locomotionParameters = new LocomotionParameters();
    GetStateDefaultLocomotionParameters(scriptInterface, locomotionParameters);
    locomotionParameters.SetUpwardsGravity(GetStaticFloatParameter("upwardsGravity", -16));
    locomotionParameters.SetDownwardsGravity(GetStaticFloatParameter("downwardsGravity", -4));
    locomotionParameters.SetDoJump(true);
    stateContext.SetTemporaryScriptableParameter("locomotionParameters", locomotionParameters, true);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SendAnimFeatureDataToGraph(stateContext, scriptInterface, 0);
    PlaySound("q115_thruster_stop", scriptInterface);
    StopEffectOnItem(scriptInterface, "thrusters");
    StopRumbleLoop(scriptInterface, "light");
  }
}

public class AirHoverDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isInAcceptableAerialState;
    Bool shouldFall;
    shouldFall = ShouldFall(stateContext, scriptInterface);
    if(shouldFall) {
      GetAudioSystem(WeakRefToRef(scriptInterface.owner).GetGame()).NotifyGameTone("StartFalling");
    };
    if(!HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.BerserkPlayerBuff")) {
      return false;
    };
    if(IsHeavyWeaponEquipped(scriptInterface)) {
      return false;
    };
    isInAcceptableAerialState = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Jump);
    if(GetParameterBool("requestSuperheroLandActivation", stateContext) && isInAcceptableAerialState) {
      if(IsDistanceToGroundAcceptable(stateContext, scriptInterface) && IsFallSpeedAcceptable(stateContext, scriptInterface)) {
        return true;
      };
    };
    return false;
  }

  protected final const Bool IsDistanceToGroundAcceptable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetDistanceToGround(scriptInterface) < GetStaticFloatParameter("minDistanceToGround", 2)) {
      return false;
    };
    return true;
  }

  protected final const Bool IsFallSpeedAcceptable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float playerFallingTooFast;
    Float verticalSpeed;
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    playerFallingTooFast = GetParameterFloat("VeryHardLandingFallingSpeed", stateContext, true);
    if(verticalSpeed < playerFallingTooFast) {
      return false;
    };
    return true;
  }

  protected final const Bool ToSuperheroFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetStaticBoolParameter("autoTransitionToSuperheroFall", true) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("maxAirHoverTime", 0)) {
      return true;
    };
    if(!GetStaticBoolParameter("autoTransitionToSuperheroFall", true) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("maxAirHoverTime", 0) && scriptInterface.IsActionJustTapped("ToggleCrouch") || scriptInterface.IsActionJustPressed("Crouch")) {
      return true;
    };
    return false;
  }
}

public class AirHoverEvents extends LocomotionAirEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float verticalSpeed;
    OnEnter(stateContext, scriptInterface);
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    scriptInterface.PushAnimationEvent("AirHover");
    PlaySound("lcm_wallrun_out", scriptInterface);
    AddUpwardsImpulse(stateContext, scriptInterface, verticalSpeed);
    stateContext.SetPermanentBoolParameter("forcedTemporaryUnequip", true, true);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.AirHover);
  }

  private final void AddUpwardsImpulse(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float verticalSpeed) {
    Float verticalImpulse;
    if(verticalSpeed < -0.5) {
      verticalImpulse = GetStaticFloatParameter("verticalUpwardsImpulse", 4);
      AddVerticalImpulse(stateContext, verticalImpulse);
    };
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }
}

public class SuperheroFallEvents extends LocomotionAirEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    PlaySound("Player_double_jump", scriptInterface);
    scriptInterface.PushAnimationEvent("SuperHeroFall");
    AddVerticalImpulse(stateContext, GetStaticFloatParameter("downwardsImpulseStrength", 0));
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SuperheroFall);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }
}

public class JumpDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanJump)) {
      return false;
    };
    if(HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) || HasStatFlag(scriptInterface, gamedataStatType.HasAirHover)) {
      if(GetActionHoldTime(stateContext, scriptInterface, "Jump") < 0.15000000596046448 && GetConditionParameterFloat("InputHoldTime", stateContext) < 0.20000000298023224 && scriptInterface.IsActionJustReleased("Jump")) {
        return true;
      };
    } else {
      if(GetConditionParameterBool("JumpPressed", stateContext) || scriptInterface.IsActionJustPressed("Jump")) {
        return true;
      };
    };
    return false;
  }
}

public class JumpEvents extends LocomotionAirEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    if(!IsInRpgContext(scriptInterface)) {
      stateContext.SetPermanentBoolParameter("VisionToggled", false, true);
    };
    scriptInterface.PushAnimationEvent("Jump");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Jump));
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Jump);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
  }
}

public class DoubleJumpDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 currentNumberOfJumps;
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasDoubleJump)) {
      return false;
    };
    if(HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) || HasStatFlag(scriptInterface, gamedataStatType.HasAirHover)) {
      return false;
    };
    if(IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface)) {
      return false;
    };
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator)) {
      return false;
    };
    currentNumberOfJumps = GetParameterInt("currentNumberOfJumps", stateContext, true);
    if(currentNumberOfJumps >= GetStaticIntParameter("numberOfMultiJumps", 1)) {
      return false;
    };
    if(GetConditionParameterBool("JumpPressed", stateContext) || scriptInterface.IsActionJustPressed("Jump")) {
      return true;
    };
    return false;
  }
}

public class DoubleJumpEvents extends LocomotionAirEvents {

  public final void OnEnterFromAirThrusters(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetPermanentIntParameter("currentNumberOfJumps", 1, true);
    OnEnter(stateContext, scriptInterface);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 currentNumberOfJumps;
    OnEnter(stateContext, scriptInterface);
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    currentNumberOfJumps = GetParameterInt("currentNumberOfJumps", stateContext, true);
    currentNumberOfJumps += 1;
    stateContext.SetPermanentIntParameter("currentNumberOfJumps", currentNumberOfJumps, true);
    PlaySound("lcm_player_double_jump", scriptInterface);
    PlayRumble(scriptInterface, GetStaticStringParameter("rumbleOnEnter", "medium_fast"));
    scriptInterface.PushAnimationEvent("DoubleJump");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Jump));
    stateContext.SetConditionBoolParameter("JumpPressed", false, true);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.DoubleJump);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
  }
}

public class ChargeJumpDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) || HasStatFlag(scriptInterface, gamedataStatType.HasAirHover)) {
      return false;
    };
    if(IsPlayerInAnyMenu(scriptInterface) || IsRadialWheelOpen(scriptInterface)) {
      return false;
    };
    if(IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface)) {
      return false;
    };
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator)) {
      return false;
    };
    if(HasStatFlag(scriptInterface, gamedataStatType.HasChargeJump) && GetConditionParameterFloat("InputHoldTime", stateContext) > 0.15000000596046448 && scriptInterface.IsActionJustReleased("Jump")) {
      return true;
    };
    return false;
  }
}

public class ChargeJumpEvents extends LocomotionAirEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float inputHoldTime;
    OnEnter(stateContext, scriptInterface);
    inputHoldTime = GetConditionParameterFloat("InputHoldTime", stateContext);
    scriptInterface.PushAnimationEvent("ChargeJump");
    PlaySound("lcm_player_double_jump", scriptInterface);
    PlayRumble(scriptInterface, GetStaticStringParameter("rumbleOnEnter", "medium_fast"));
    StartEffect(scriptInterface, "charged_jump");
    PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.PressureWave"));
    SpawnLandingFxGameEffect("Attacks.PressureWave", scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Jump));
    SetChargeJumpParameters(stateContext, scriptInterface, inputHoldTime);
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.ChargeJump);
  }

  private final void SetChargeJumpParameters(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float inputHoldTime) {
    Float upwardsGravity;
    Float downwardsGravity;
    String nameSuffix;
    if(inputHoldTime >= GetStaticFloatParameter("minHoldTime", 0.10000000149011612) && inputHoldTime < GetStaticFloatParameter("medChargeHoldTime", 0.20000000298023224)) {
      upwardsGravity = GetStaticFloatParameter("upwardsGravityMinCharge", -16);
      downwardsGravity = GetStaticFloatParameter("downwardsGravityMinCharge", -16);
      nameSuffix = "Low";
    } else {
      if(inputHoldTime > GetStaticFloatParameter("medChargeHoldTime", 0.20000000298023224) && inputHoldTime < GetStaticFloatParameter("maxChargeHoldTime", 0.30000001192092896)) {
        upwardsGravity = GetStaticFloatParameter("upwardsGravityMedCharge", -16);
        downwardsGravity = GetStaticFloatParameter("downwardsGravityMedCharge", -16);
        nameSuffix = "Medium";
      } else {
        if(inputHoldTime >= GetStaticFloatParameter("maxChargeHoldTime", 0.30000001192092896)) {
          upwardsGravity = GetStaticFloatParameter("upwardsGravityMaxCharge", -16);
          downwardsGravity = GetStaticFloatParameter("downwardsGravityMaxCharge", -20);
          nameSuffix = "High";
          AddVerticalImpulse(stateContext, GetStaticFloatParameter("verticalImpulse", 2));
        };
      };
    };
    UpdateChargeJumpStats(stateContext, scriptInterface, upwardsGravity, downwardsGravity, nameSuffix);
  }

  private final void UpdateChargeJumpStats(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float upwardsGravity, Float downwardsGravity, String nameSuffix) {
    ref<StatsSystem> statSystem;
    String stateMachineNameString;
    String stateNameString;
    String packageString;
    CName stateStatGroupName;
    Bool modGroupDefined;
    TweakDBID cpoModRecordId;
    ref<LocomotionParameters> locomotionParameters;
    modGroupDefined = false;
    stateMachineNameString = NameToString(GetStateMachineName());
    packageString = "Player" + stateMachineNameString + ".";
    stateNameString = NameToString(GetStateName());
    UppercaseFirstChar(stateNameString);
    stateNameString = "player_locomotion_data_" + stateNameString;
    stateNameString = packageString + stateNameString + nameSuffix;
    RemoveModifierGroupForState(scriptInterface);
    AddModifierGroupWithName(scriptInterface, stateNameString);
    locomotionParameters = new LocomotionParameters();
    GetStateDefaultLocomotionParameters(scriptInterface, locomotionParameters);
    locomotionParameters.SetUpwardsGravity(upwardsGravity);
    locomotionParameters.SetDownwardsGravity(downwardsGravity);
    locomotionParameters.SetDoJump(true);
    stateContext.SetTemporaryScriptableParameter("locomotionParameters", locomotionParameters, true);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
  }
}

public class HoverJumpDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasAirHover)) {
      return false;
    };
    if(IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface)) {
      return false;
    };
    if(GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator)) {
      return false;
    };
    if(scriptInterface.IsActionJustHeld("Jump")) {
      return true;
    };
    return false;
  }
}

public class HoverJumpEvents extends LocomotionAirEvents {

  protected void SendHoverJumpStateToGraph(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Int32 state) {
    ref<AnimFeature_HoverJumpData> animFeature;
    animFeature = new AnimFeature_HoverJumpData();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature("HoverJumpData", animFeature);
  }

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SendHoverJumpStateToGraph(stateContext, scriptInterface, 1);
    PlaySound("lcm_player_double_jump", scriptInterface);
    PlayRumble(scriptInterface, GetStaticStringParameter("rumbleOnEnter", "medium_fast"));
    StartEffect(scriptInterface, "charged_jump");
    PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.PressureWave"));
    SpawnLandingFxGameEffect("Attacks.PressureWave", scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Jump));
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.HoverJump);
    ApplyStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.HoverJumpPlayerBuff");
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float verticalSpeed;
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    if(!GetParameterBool("isAboutToLand", stateContext, true) && verticalSpeed < -1 && GetDistanceToGround(scriptInterface) < 1) {
      SendHoverJumpStateToGraph(stateContext, scriptInterface, 2);
      stateContext.SetPermanentBoolParameter("isAboutToLand", true, true);
    };
    if(CanHover(stateContext, scriptInterface) && scriptInterface.GetActionValue("CameraAim") == 0 && scriptInterface.GetActionValue("Jump") > 0) {
      AddUpwardsThrust(stateContext, scriptInterface, GetStaticFloatParameter("verticalImpulse", 4));
      if(!GetParameterBool("isHovering", stateContext, true)) {
        stateContext.RemovePermanentBoolParameter("isStabilising");
        stateContext.SetPermanentBoolParameter("isHovering", true, true);
        UpdateHoverJumpStats(stateContext, scriptInterface, GetStaticFloatParameter("upwardsGravityOnThrust", -10), GetStaticFloatParameter("downwardsGravityOnThrust", -5), "");
      };
    };
    if(CanHover(stateContext, scriptInterface) && GetStaticBoolParameter("stabilizeOnAim", false) && scriptInterface.GetActionValue("CameraAim") > 0) {
      if(!GetParameterBool("isStabilising", stateContext, true)) {
        if(verticalSpeed < -0.5) {
          AddUpwardsThrust(stateContext, scriptInterface, GetStaticFloatParameter("verticalImpulseStabilize", 4));
        };
        stateContext.RemovePermanentBoolParameter("isHovering");
        stateContext.SetPermanentBoolParameter("isStabilising", true, true);
        UpdateHoverJumpStats(stateContext, scriptInterface, GetStaticFloatParameter("upwardsGravityOnStabilize", -10), GetStaticFloatParameter("downwardsGravityOnStabilize", -3), "Thrust");
        PlaySound("lcm_wallrun_in", scriptInterface);
      };
    } else {
      UpdateHoverJumpStats(stateContext, scriptInterface, GetStaticFloatParameter("upwardsGravity", -16), GetStaticFloatParameter("downwardsGravity", -16), "");
    };
    OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  private final Bool CanHover(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.HoverJumpPlayerBuff");
  }

  private final void UpdateHoverJumpStats(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float upwardsGravity, Float downwardsGravity, String nameSuffix) {
    ref<StatsSystem> statSystem;
    String stateMachineNameString;
    String stateNameString;
    String packageString;
    ref<LocomotionParameters> locomotionParameters;
    stateMachineNameString = NameToString(GetStateMachineName());
    packageString = "Player" + stateMachineNameString + ".";
    stateNameString = NameToString(GetStateName());
    UppercaseFirstChar(stateNameString);
    stateNameString = "player_locomotion_data_" + stateNameString;
    stateNameString = packageString + stateNameString + nameSuffix;
    RemoveModifierGroupForState(scriptInterface);
    AddModifierGroupWithName(scriptInterface, stateNameString);
    locomotionParameters = new LocomotionParameters();
    GetStateDefaultLocomotionParameters(scriptInterface, locomotionParameters);
    locomotionParameters.SetUpwardsGravity(upwardsGravity);
    locomotionParameters.SetDownwardsGravity(downwardsGravity);
    locomotionParameters.SetDoJump(true);
    stateContext.SetTemporaryScriptableParameter("locomotionParameters", locomotionParameters, true);
  }

  private final void AddUpwardsThrust(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float verticalImpulse) {
    if(verticalImpulse > 0) {
      AddVerticalImpulse(stateContext, verticalImpulse);
    };
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    CleanUpOnExit(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    CleanUpOnExit(stateContext, scriptInterface);
  }

  private final void CleanUpOnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendHoverJumpStateToGraph(stateContext, scriptInterface, 0);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.HoverJumpPlayerBuff");
    UpdateHoverJumpStats(stateContext, scriptInterface, -16, -16, "");
    stateContext.RemovePermanentBoolParameter("isStabilising");
    stateContext.RemovePermanentBoolParameter("isHovering");
    stateContext.RemovePermanentBoolParameter("isAboutToLand");
  }
}

public class DodgeAirDecisions extends LocomotionAirDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 currentNumberOfAirDodges;
    if(!HasStatFlag(scriptInterface, gamedataStatType.HasDodgeAir)) {
      return false;
    };
    if(!HasStatFlag(scriptInterface, gamedataStatType.CanAimWhileDodging) && IsInUpperBodyState(stateContext, "aimingState")) {
      return false;
    };
    if(IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface)) {
      return false;
    };
    currentNumberOfAirDodges = GetParameterInt("currentNumberOfAirDodges", stateContext, true);
    if(currentNumberOfAirDodges >= GetStaticIntParameter("numberOfAirDodges", 1)) {
      return false;
    };
    if(WantsToDodge(stateContext, scriptInterface)) {
      return true;
    };
    return false;
  }

  protected const Bool ToFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool isKerenzikovStateActive;
    Bool isKerenzikovEnd;
    isKerenzikovStateActive = stateContext.GetStateMachineCurrentState("TimeDilation") == "kerenzikov";
    if(!HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeAirBuff")) {
      return !isKerenzikovStateActive;
    };
    isKerenzikovEnd = isKerenzikovStateActive && !HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.KerenzikovPlayerBuff");
    if(isKerenzikovEnd) {
      return true;
    };
    return false;
  }
}

public class DodgeAirEvents extends LocomotionAirEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<SoundPlayEvent> soundEvent;
    Int32 currentNumberOfAirDodges;
    OnEnter(stateContext, scriptInterface);
    currentNumberOfAirDodges = GetParameterInt("currentNumberOfAirDodges", stateContext, true);
    currentNumberOfAirDodges += 1;
    stateContext.SetPermanentIntParameter("currentNumberOfAirDodges", currentNumberOfAirDodges, true);
    Dodge(stateContext, scriptInterface);
    PlayRumbleBasedOnDodgeDirection(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent("Dodge");
    stateContext.SetConditionBoolParameter("SprintToggled", false, true);
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    ApplyStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeAirBuff");
    ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustPressed("Jump")) {
      stateContext.SetConditionBoolParameter("JumpPressed", true, true);
    };
    OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.DodgeAirBuff");
  }

  protected final void Dodge(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float impulseValue;
    Vector4 impulse;
    Float dodgeHeading;
    impulseValue = GetStaticFloatParameter("impulse", 10);
    dodgeHeading = GetConditionParameterFloat("DodgeDirection", stateContext);
    impulse = FromHeading(AngleNormalize180(WeakRefToRef(scriptInterface.executionOwner).GetWorldYaw() + dodgeHeading)) * impulseValue;
    AddImpulse(stateContext, impulse);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.DodgeAir);
  }
}

public abstract class AbstractLandDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }
}

public abstract class AbstractLandEvents extends LocomotionGroundEvents {

  [Default(AbstractLandEvents, false))]
  public Bool m_blockLandingStimBroadcasting;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    array<ControllerHit> collisionReport;
    ControllerHit hit;
    ref<PSMFall> fallEvent;
    Variant parameter;
    Float impactSpeed;
    Vector4 playerPositionCentreOfSphere;
    Vector4 up;
    Bool bottomCollisionFound;
    Vector4 bottomCollisionNormal;
    Vector4 centreSlope;
    Vector4 crossNormalUp;
    Vector4 playerPosition;
    Vector4 touchNormal;
    Int32 collisionIndex;
    Float capsuleRadius;
    up = GetUpVector();
    OnEnter(stateContext, scriptInterface);
    impactSpeed = AbsF(GetParameterFloat("ImpactSpeed", stateContext, true));
    SetAudioParameter("RTPC_Landing_Type", 0, scriptInterface);
    fallEvent = new PSMFall();
    fallEvent.SetSpeed(impactSpeed);
    WeakRefToRef(scriptInterface.owner).QueueEvent(fallEvent);
    scriptInterface.PushAnimationEvent("Land");
    capsuleRadius = FromVariant(scriptInterface.GetStateVectorParameter(physicsStateValue.Radius));
    playerPosition = GetPlayerPosition(scriptInterface);
    collisionReport = scriptInterface.GetCollisionReport();
    playerPositionCentreOfSphere = playerPosition + up * capsuleRadius;
    bottomCollisionFound = false;
    collisionIndex = 0;
    while(collisionIndex < Size(collisionReport) && !bottomCollisionFound) {
      hit = collisionReport[collisionIndex];
      touchNormal = Normalize(playerPositionCentreOfSphere - hit.worldPos);
      if(touchNormal.Z > 0 && bottomCollisionNormal.Z < touchNormal.Z) {
        bottomCollisionNormal = touchNormal;
        if(bottomCollisionNormal.Z < 1) {
          bottomCollisionFound = true;
        };
      };
      collisionIndex += 1;
    };
  }

  protected final void BroadcastLandingStim(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, gamedataStimType stimType) {
    Bool broadcastLandingStim;
    StateResultFloat impactSpeed;
    Float speedThresholdToSendStim;
    ref<StimBroadcasterComponent> broadcaster;
    broadcastLandingStim = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatValue(Cast(WeakRefToRef(scriptInterface.owner).GetEntityID()), gamedataStatType.CanLandSilently) < 1;
    if(!broadcastLandingStim || this.m_blockLandingStimBroadcasting) {
      this.m_blockLandingStimBroadcasting = false;
      return ;
    };
    if(CheckCrouchEnterCondition(stateContext, scriptInterface) && stimType == gamedataStimType.LandingRegular) {
      return ;
    };
    impactSpeed = stateContext.GetPermanentFloatParameter("ImpactSpeed");
    speedThresholdToSendStim = GetFallingSpeedBasedOnHeight(scriptInterface, 1.2000000476837158);
    if(impactSpeed.value < speedThresholdToSendStim) {
      broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
      if(ToBool(broadcaster)) {
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, stimType);
      };
    };
  }

  protected final void EvaluatePlayingLandingVFX(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat impactSpeed;
    Float minFallSpeed;
    impactSpeed = stateContext.GetPermanentFloatParameter("ImpactSpeed");
    minFallSpeed = GetFallingSpeedBasedOnHeight(scriptInterface, 2);
    if(impactSpeed.value < minFallSpeed) {
      StartEffect(scriptInterface, "landing_regular");
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.Default));
  }
}

public abstract class FailedLandingAbstractDecisions extends AbstractLandDecisions {

  public final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("duration", 2.5);
  }
}

public abstract class FailedLandingAbstractEvents extends AbstractLandEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
  }
}

public class RegularLandEvents extends AbstractLandEvents {

  public final void OnEnterFromLadderCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    this.m_blockLandingStimBroadcasting = true;
    OnEnter(stateContext, scriptInterface);
  }

  public final void OnEnterFromUnsecureFootingFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetConditionBoolParameter("blockEnteringSlide", true, true);
    OnEnter(stateContext, scriptInterface);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 linearVelocity;
    SecureFootingResult secureFootingResult;
    linearVelocity = GetLinearVelocity(scriptInterface);
    secureFootingResult = scriptInterface.HasSecureFooting();
    PlayVoiceOver(WeakRefToRef(scriptInterface.owner), "regularLanding", "Scripts:RegularLandEvents");
    EvaluateTransitioningToSlideAfterLanding(stateContext, scriptInterface);
    ShouldTriggerDestruction(stateContext, scriptInterface);
    EvaluatePlayingLandingVFX(stateContext, scriptInterface);
    BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingRegular);
    OnEnter(stateContext, scriptInterface);
    TryPlayRumble(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.RegularLand);
    AddImpulse(stateContext, secureFootingResult.slidingDirection * AbsF(linearVelocity.Z * secureFootingResult.staticGroundFactor));
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.RegularLand));
  }

  protected final void EvaluateTransitioningToSlideAfterLanding(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat inAirTime;
    Vector4 velocity;
    Float currentSpeed;
    if(!GetConditionParameterBool("CrouchToggled", stateContext)) {
      return ;
    };
    inAirTime = stateContext.GetPermanentFloatParameter("InAirDuration");
    velocity = GetLinearVelocity(scriptInterface);
    currentSpeed = Length2D(velocity);
    if(inAirTime.valid && inAirTime.value > 0.699999988079071 && currentSpeed < 5 || inAirTime.valid && inAirTime.value < 0.5) {
      stateContext.SetConditionBoolParameter("blockEnteringSlide", true, true);
    };
  }

  protected final void TryPlayRumble(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat impactSpeed;
    StateResultFloat inAirTime;
    impactSpeed = stateContext.GetPermanentFloatParameter("ImpactSpeed");
    inAirTime = stateContext.GetPermanentFloatParameter("InAirDuration");
    if(GetConditionParameterBool("CrouchToggled", stateContext) && impactSpeed.valid && impactSpeed.value > GetFallingSpeedBasedOnHeight(scriptInterface, 1.2000000476837158)) {
      return ;
    };
    if(impactSpeed.valid && impactSpeed.value < GetFallingSpeedBasedOnHeight(scriptInterface, 0.6600000262260437)) {
      PlayRumble(scriptInterface, "light_pulse");
    } else {
      if(inAirTime.valid && inAirTime.value > 0.3330000042915344) {
        PlayRumble(scriptInterface, "light_pulse");
      };
    };
  }

  protected final void ShouldTriggerDestruction(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat impactSpeed;
    impactSpeed = stateContext.GetPermanentFloatParameter("ImpactSpeed");
    if(impactSpeed.value < GetFallingSpeedBasedOnHeight(scriptInterface, 2.5)) {
      PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.PressureWave"));
      SpawnLandingFxGameEffect("Attacks.PressureWave", scriptInterface);
    };
  }
}

public class HardLandEvents extends FailedLandingAbstractEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StartEffect(scriptInterface, "landing_hard");
    PlayVoiceOver(WeakRefToRef(scriptInterface.owner), "hardLanding", "Scripts:HardLandEvents");
    PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.HardLanding"));
    SpawnLandingFxGameEffect("Attacks.HardLanding", scriptInterface);
    BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingHard);
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.HardLand);
    PlayRumble(scriptInterface, "medium_pulse");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.HardLand));
  }
}

public class VeryHardLandEvents extends FailedLandingAbstractEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StartEffect(scriptInterface, "landing_very_hard");
    PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.VeryHardLanding"));
    SpawnLandingFxGameEffect("Attacks.VeryHardLanding", scriptInterface);
    PlayVoiceOver(WeakRefToRef(scriptInterface.owner), "veryhardLanding", "Scripts:VeryHardLandEvents");
    BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingVeryHard);
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.VeryHardLand);
    PlayRumble(scriptInterface, "heavy_fast");
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.VeryHardLand));
  }
}

public class DeathLandEvents extends FailedLandingAbstractEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StartEffect(scriptInterface, "landing_death");
    PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.DeathLanding"));
    SpawnLandingFxGameEffect("Attacks.DeathLanding", scriptInterface);
    PlayVoiceOver(WeakRefToRef(scriptInterface.owner), "veryhardLanding", "Scripts:DeathLandEvents");
    BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingVeryHard);
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.DeathLand);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.DeathLand));
  }
}

public class SuperheroLandDecisions extends AbstractLandDecisions {

  public final const Bool ToSuperheroLandRecovery(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("stateDuration", 0.7699999809265137);
  }
}

public class SuperheroLandEvents extends AbstractLandEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<StimBroadcasterComponent> broadcaster;
    OnEnter(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent("SuperheroLand");
    PlaySound("lcm_wallrun_in", scriptInterface);
    StartEffect(scriptInterface, "stagger_effect");
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    PlayRumble(scriptInterface, "heavy_fast");
    PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.SuperheroLanding"));
    SpawnLandingFxGameEffect("Attacks.SuperheroLanding", scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SuperheroLand);
    broadcaster = WeakRefToRef(scriptInterface.executionOwner).GetStimBroadcasterComponent();
    if(ToBool(broadcaster)) {
      broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.LandingVeryHard);
    };
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.SuperheroLand));
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.Default));
  }
}

public class SuperheroLandRecoveryDecisions extends AbstractLandDecisions {

  public final const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("stateDuration", 0.4000000059604645);
  }
}

public class SuperheroLandRecoveryEvents extends AbstractLandEvents {

  protected void SendAnimFeature(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Int32 state) {
    ref<AnimFeature_SuperheroLand> animFeature;
    animFeature = new AnimFeature_SuperheroLand();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature("SuperheroLand", animFeature);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SuperheroLandRecovery);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.SuperheroLandRecovery));
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SendAnimFeature(stateContext, scriptInterface, 0);
    stateContext.SetPermanentBoolParameter("forcedTemporaryUnequip", false, true);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, ToInt(gamePSMLandingState.Default));
  }
}

public abstract class WallCollisionHelpers extends IScriptable {

  public final static Bool GetWallCollision(ref<StateGameScriptInterface> scriptInterface, Vector4 playerPosition, Vector4 up, Float capsuleRadius, out ControllerHit wallCollision) {
    array<ControllerHit> collisionReport;
    ControllerHit hit;
    Vector4 playerPositionCentreOfSphere;
    Bool sideCollisionFound;
    Int32 collisionIndex;
    Vector4 touchDirection;
    collisionReport = scriptInterface.GetCollisionReport();
    playerPositionCentreOfSphere = playerPosition + up * capsuleRadius;
    sideCollisionFound = false;
    collisionIndex = 0;
    while(collisionIndex < Size(collisionReport) && !sideCollisionFound) {
      hit = collisionReport[collisionIndex];
      touchDirection = Normalize(hit.worldPos - playerPositionCentreOfSphere);
      if(touchDirection.Z >= 0) {
        wallCollision = hit;
        return true;
      };
      collisionIndex += 1;
    };
    return false;
  }
}

public class StatusEffectDecisions extends LocomotionGroundDecisions {

  protected const Bool InternalEnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return InternalEnterCondition(stateContext, scriptInterface);
  }

  public const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetStaticStringParameter("statusEffectEnumName", "") == GetStatusEffectName(stateContext, scriptInterface);
  }

  protected const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !HasMovementAffiliatedStatusEffect(stateContext, scriptInterface);
  }

  protected const Bool ToRegularFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !HasMovementAffiliatedStatusEffect(stateContext, scriptInterface) && !IsTouchingGround(scriptInterface);
  }

  private final const Bool HasMovementAffiliatedStatusEffect(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<StatusEffect_Record> statusEffectRecord;
    statusEffectRecord = RefToWeakRef(Cast(stateContext.GetConditionScriptableParameter("AffectMovementStatusEffectRecord")));
    return HasStatusEffect(scriptInterface.owner, WeakRefToRef(statusEffectRecord).GetID());
  }

  private final const String GetStatusEffectName(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<StatusEffect_Record> statusEffectRecord;
    String statusEffectName;
    statusEffectRecord = RefToWeakRef(Cast(stateContext.GetTemporaryScriptableParameter(GetAppliedStatusEffectKey())));
    if(ToBool(statusEffectRecord)) {
      statusEffectName = WeakRefToRef(WeakRefToRef(statusEffectRecord).StatusEffectType()).EnumName();
    };
    return statusEffectName;
  }
}

public class StatusEffectEvents extends LocomotionGroundEvents {

  public wref<StatusEffect_Record> m_statusEffectRecord;

  public wref<StatusEffectPlayerData_Record> m_playerStatusEffectRecordData;

  public ref<AnimFeature_StatusEffect> m_animFeatureStatusEffect;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    this.m_statusEffectRecord = GetStatusEffectRecord(stateContext, scriptInterface);
    this.m_playerStatusEffectRecordData = GetStatusEffectPlayerData(scriptInterface, stateContext);
    stateContext.SetConditionScriptableParameter("AffectMovementStatusEffectRecord", WeakRefToRef(this.m_statusEffectRecord), true);
    stateContext.SetConditionScriptableParameter("PlayerStatusEffectRecordData", WeakRefToRef(this.m_playerStatusEffectRecordData), true);
    if(ShouldForceUnequipWeapon()) {
      stateContext.SetPermanentBoolParameter("forcedTemporaryUnequip", true, true);
    };
    ProcessStatusEffectBasedOnType(scriptInterface, stateContext, GetStatusEffectType(scriptInterface, stateContext));
    if(ShouldRotateToSource()) {
      RotateToKnockdownSource(stateContext, scriptInterface);
    };
  }

  protected final void RotateToKnockdownSource(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<AdjustTransformWithDurations> adjustRequest;
    Vector4 direction;
    Vector4 postion;
    direction = GetStatusEffectHitDirection(scriptInterface);
    if(IsZero(direction)) {
      return ;
    };
    adjustRequest = new AdjustTransformWithDurations();
    adjustRequest.SetPosition(new Vector4(0,0,0,0));
    adjustRequest.SetSlideDuration(-1);
    adjustRequest.SetRotation(BuildFromDirectionVector(-direction, new Vector4(0,0,1,0)));
    adjustRequest.SetRotationDuration(0.5);
    stateContext.SetTemporaryScriptableParameter("adjustTransform", adjustRequest, true);
  }

  private final void ProcessStatusEffectBasedOnType(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext, gamedataStatusEffectType type) {
    if(!ToBool(this.m_statusEffectRecord)) {
      return ;
    };
    if(!ShouldUseCustomAdditives(scriptInterface)) {
      if(type == gamedataStatusEffectType.Stunned) {
        scriptInterface.PushAnimationEvent("StaggerHit");
        if(!HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.Parry")) {
          stateContext.SetPermanentBoolParameter("InterruptMelee", WeakRefToRef(this.m_playerStatusEffectRecordData).ForceSafeWeapon(), true);
        };
      };
      SendCameraShakeDataToGraph(scriptInterface, stateContext, GetCameraShakeStrength());
      SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Start);
    };
    ApplyCounterForce(scriptInterface, stateContext, GetImpulseDistance(), GetScaleImpulseDistance());
  }

  private final void SendCameraShakeDataToGraph(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext, Float camShakeStrength) {
    ref<AnimFeature_PlayerHitReactionData> animFeatureHitReaction;
    animFeatureHitReaction = new AnimFeature_PlayerHitReactionData();
    animFeatureHitReaction.hitStrength = camShakeStrength;
    scriptInterface.SetAnimationParameterFeature("HitReactionData", animFeatureHitReaction);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  protected final const Float GetTimeInStatusEffect(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultFloat startTime;
    Float timeInState;
    startTime = stateContext.GetPermanentFloatParameter(GetStateStartTimeKey());
    if(!startTime.valid) {
      return 0;
    };
    timeInState = ToFloat(GetTimeSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetSimTime()) - startTime.value;
    return timeInState;
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnForcedExit(stateContext, scriptInterface);
    DefaultOnExit(stateContext, scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    DefaultOnExit(stateContext, scriptInterface);
  }

  protected final void OnExitToFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    DefaultOnExit(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent("StraightToFall");
  }

  protected void CommonOnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    RemoveStatusEffect(scriptInterface, stateContext);
    if(ShouldForceUnequipWeapon()) {
      stateContext.SetPermanentBoolParameter("forcedTemporaryUnequip", false, true);
    };
    stateContext.RemovePermanentFloatParameter(GetStateStartTimeKey());
  }

  protected final void DefaultOnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(ToBool(this.m_animFeatureStatusEffect)) {
      this.m_animFeatureStatusEffect.Clear();
    };
    scriptInterface.SetAnimationParameterFeature("StatusEffect", this.m_animFeatureStatusEffect);
    if(GetStaticBoolParameter("forceExitToStand", false)) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    };
    CommonOnExit(stateContext, scriptInterface);
  }

  protected void SendStatusEffectAnimDataToGraph(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, EKnockdownStates state) {
    if(!ToBool(this.m_animFeatureStatusEffect)) {
      this.m_animFeatureStatusEffect = new AnimFeature_StatusEffect();
    };
    stateContext.SetPermanentFloatParameter(GetStateStartTimeKey(), ToFloat(GetTimeSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetSimTime()), true);
    PopulateStatusEffectAnimData(WeakRefToRef(scriptInterface.owner), this.m_statusEffectRecord, state, GetStatusEffectHitDirection(scriptInterface), this.m_animFeatureStatusEffect);
    scriptInterface.SetAnimationParameterFeature("StatusEffect", this.m_animFeatureStatusEffect);
  }

  protected final void ApplyCounterForce(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext, Float desiredDistance, Bool scaleDistance) {
    Vector4 direction;
    ref<PSMImpulse> ev;
    Vector4 impulseDir;
    Float deceleration;
    Float speed;
    if(desiredDistance < 0) {
      return ;
    };
    direction = GetStatusEffectHitDirection(scriptInterface);
    direction.Z = 0;
    if(scaleDistance) {
      desiredDistance *= Length2D(direction);
    };
    if(IsZero(direction)) {
      direction = WeakRefToRef(scriptInterface.owner).GetWorldForward() * -1;
    } else {
      Normalize2D(direction);
    };
    speed = GetSpeedBasedOnDistance(scriptInterface, desiredDistance);
    impulseDir = direction * speed;
    ev = new PSMImpulse();
    ev.id = "impulse";
    ev.impulse = impulseDir;
    WeakRefToRef(scriptInterface.owner).QueueEvent(ev);
  }

  private final const void RemoveStatusEffect(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    RemoveStatusEffect(scriptInterface.owner, WeakRefToRef(this.m_statusEffectRecord).GetID());
    stateContext.RemoveConditionScriptableParameter("PlayerStatusEffectRecordData");
  }

  private final const gamedataStatusEffectType GetStatusEffectType(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    return WeakRefToRef(WeakRefToRef(this.m_statusEffectRecord).StatusEffectType()).Type();
  }

  protected final const Float GetStatusEffectRemainingDuration(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    return GetStatusEffectByID(scriptInterface.owner, WeakRefToRef(this.m_statusEffectRecord).GetID()).GetRemainingDuration();
  }

  protected final const Vector4 GetStatusEffectHitDirection(ref<StateGameScriptInterface> scriptInterface) {
    return GetStatusEffectByID(scriptInterface.owner, WeakRefToRef(this.m_statusEffectRecord).GetID()).GetDirection();
  }

  protected final const Float GetStartupAnimDuration() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).StartupAnimDuration();
  }

  protected final const Bool ShouldRotateToSource() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).RotateToSource();
  }

  protected final const Float GetAirRecoveryAnimDuration() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).AirRecoveryAnimDuration();
  }

  protected final const Float GetRecoveryAnimDuration() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).RecoveryAnimDuration();
  }

  protected final const Float GetLandAnimDuration() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).LandAnimDuration();
  }

  private final const Float GetImpulseDistance() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).ImpulseDistance();
  }

  private final const Bool GetScaleImpulseDistance() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).ScaleImpulseDistance();
  }

  private final const Float GetCameraShakeStrength() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).CameraShakeStrength();
  }

  private final const Bool ShouldForceUnequipWeapon() {
    return WeakRefToRef(this.m_playerStatusEffectRecordData).ForceUnequipWeapon();
  }

  protected final const Bool ShouldUseCustomAdditives(ref<StateGameScriptInterface> scriptInterface) {
    return HasStatusEffectWithTag(scriptInterface.executionOwner, "UseCustomAdditives");
  }
}

public class KnockdownDecisions extends StatusEffectDecisions {

  protected const Bool ToStand(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool canExit;
    canExit = stateContext.GetTemporaryBoolParameter(GetCanExitKnockdownKey());
    if(canExit.valid) {
      return ToStand(stateContext, scriptInterface);
    };
    return false;
  }

  protected const Bool ToRegularFall(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool canExit;
    canExit = stateContext.GetTemporaryBoolParameter(GetCanExitKnockdownKey());
    if(canExit.valid) {
      return ToRegularFall(stateContext, scriptInterface);
    };
    return false;
  }

  protected const Bool ToSecondaryKnockdown(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    StateResultBool canTriggerSecondaryKnockdown;
    canTriggerSecondaryKnockdown = stateContext.GetPermanentBoolParameter(TriggerSecondaryKnockdownKey());
    if(canTriggerSecondaryKnockdown.valid) {
      return EnterCondition(stateContext, scriptInterface);
    };
    return false;
  }
}

public class KnockdownEvents extends StatusEffectEvents {

  public Vector4 m_cachedPlayerVelocity;

  public Vector4 m_secondaryKnockdownDir;

  public Float m_secondaryKnockdownTimer;

  public Bool m_playedImpactAnim;

  public Bool m_frictionForceApplied;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Knockdown);
    OnEnter(stateContext, scriptInterface);
    this.m_playedImpactAnim = false;
    this.m_frictionForceApplied = false;
    this.m_secondaryKnockdownTimer = -1;
    this.m_cachedPlayerVelocity = GetLinearVelocity(scriptInterface);
  }

  protected void CommonOnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    CommonOnExit(stateContext, scriptInterface);
    stateContext.RemovePermanentBoolParameter(TriggerSecondaryKnockdownKey());
  }

  protected void SendStatusEffectAnimDataToGraph(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, EKnockdownStates state) {
    if(state == EKnockdownStates.Land && this.m_animFeatureStatusEffect.state != ToInt(state)) {
      RemoveModifierGroupForState(scriptInterface);
      AddModifierGroupWithName(scriptInterface, "PlayerLocomotion.player_locomotion_data_KnockdownLand");
    };
    SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, state);
  }

  private final void UpdateStatusEffectAnimStates(Float timeDelta, ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    switch(ToEnum(this.m_animFeatureStatusEffect.state)) {
      case EKnockdownStates.Start:
        if(GetTimeInStatusEffect(stateContext, scriptInterface) >= GetStartupAnimDuration()) {
          if(IsTouchingGround(scriptInterface)) {
            SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Land);
          } else {
            SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.FallLoop);
          };
        };
        break;
      case EKnockdownStates.FallLoop:
        if(IsTouchingGround(scriptInterface)) {
          SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Land);
        } else {
          if(GetStatusEffectRemainingDuration(scriptInterface, stateContext) < GetAirRecoveryAnimDuration()) {
            SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.AirRecovery);
          };
        };
        break;
      case EKnockdownStates.Land:
        if(GetTimeInStatusEffect(stateContext, scriptInterface) >= GetLandAnimDuration() && GetStatusEffectRemainingDuration(scriptInterface, stateContext) < GetRecoveryAnimDuration()) {
          SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Recovery);
        };
        break;
      case EKnockdownStates.Recovery:
        if(GetTimeInStatusEffect(stateContext, scriptInterface) >= GetRecoveryAnimDuration()) {
          stateContext.SetTemporaryBoolParameter(GetCanExitKnockdownKey(), true, true);
        };
        break;
      case EKnockdownStates.AirRecovery:
        if(IsTouchingGround(scriptInterface)) {
          SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Land);
        } else {
          if(GetTimeInStatusEffect(stateContext, scriptInterface) >= GetAirRecoveryAnimDuration()) {
            stateContext.SetTemporaryBoolParameter(GetCanExitKnockdownKey(), true, true);
          };
        };
        break;
      default:
    };
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<PSMImpulse> impulseEvent;
    Vector4 playerFacing;
    Vector4 collisionFrictionForce;
    Float frictionForceScale;
    Float impactForce;
    Float startupInterruptPoint;
    ControllerHit wallCollision;
    Int32 impactDirection;
    Bool playImpact;
    Bool triggerSecondaryKnockdown;
    impactDirection = -1;
    playImpact = false;
    triggerSecondaryKnockdown = false;
    UpdateStatusEffectAnimStates(timeDelta, scriptInterface, stateContext);
    UpdateQueuedSecondaryKnockdown(stateContext, scriptInterface, timeDelta);
    OnUpdate(timeDelta, stateContext, scriptInterface);
    if(DidPlayerCollideWithWall(scriptInterface, wallCollision)) {
      impactForce = -Dot(this.m_cachedPlayerVelocity, wallCollision.worldNormal);
      frictionForceScale = 0;
      collisionFrictionForce = -this.m_cachedPlayerVelocity + wallCollision.worldNormal * impactForce;
      if(impactForce > 25) {
        frictionForceScale = 0.8999999761581421;
        PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.HardWallImpact"));
        triggerSecondaryKnockdown = true;
      } else {
        if(impactForce > 15) {
          frictionForceScale = 0.6000000238418579;
          PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.MediumWallImpact"));
          triggerSecondaryKnockdown = true;
        } else {
          if(impactForce > 7) {
            frictionForceScale = 0.30000001192092896;
            PrepareGameEffectAoEAttack(stateContext, scriptInterface, GetAttackRecord("Attacks.LightWallImpact"));
            triggerSecondaryKnockdown = Length2D(collisionFrictionForce) < 1;
          };
        };
      };
      if(frictionForceScale > 0) {
        playImpact = true;
        impactDirection = GetLocalAngleForDirectionInInt(wallCollision.worldNormal, WeakRefToRef(scriptInterface.owner));
        if(!this.m_frictionForceApplied) {
          this.m_frictionForceApplied = true;
          impulseEvent = new PSMImpulse();
          impulseEvent.id = "impulse";
          impulseEvent.impulse = collisionFrictionForce * frictionForceScale;
          WeakRefToRef(scriptInterface.owner).QueueEvent(impulseEvent);
        };
      };
    };
    if(playImpact != this.m_animFeatureStatusEffect.playImpact) {
      if(!this.m_playedImpactAnim) {
        this.m_playedImpactAnim = playImpact;
      };
      if(playImpact) {
        this.m_animFeatureStatusEffect.playImpact = true;
        this.m_animFeatureStatusEffect.impactDirection = impactDirection;
      } else {
        this.m_animFeatureStatusEffect.playImpact = false;
      };
      scriptInterface.SetAnimationParameterFeature("StatusEffect", this.m_animFeatureStatusEffect);
    };
    if(this.m_playedImpactAnim && this.m_animFeatureStatusEffect.state == ToInt(EKnockdownStates.Start)) {
      startupInterruptPoint = WeakRefToRef(this.m_playerStatusEffectRecordData).StartupAnimInterruptPoint();
      if(startupInterruptPoint >= 0) {
        if(GetTimeInStatusEffect(stateContext, scriptInterface) >= startupInterruptPoint) {
          SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.FallLoop);
        };
      };
    };
    if(triggerSecondaryKnockdown) {
      QueueSecondaryKnockdown(stateContext, scriptInterface, wallCollision.worldNormal);
    };
    this.m_cachedPlayerVelocity = GetLinearVelocity(scriptInterface);
  }

  protected final void QueueSecondaryKnockdown(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Vector4 knockdownDir) {
    Float startupInterruptPoint;
    if(this.m_secondaryKnockdownTimer < 0) {
      this.m_secondaryKnockdownTimer = 0.10000000149011612;
      this.m_secondaryKnockdownDir = knockdownDir;
      if(this.m_animFeatureStatusEffect.state == ToInt(EKnockdownStates.Start)) {
        startupInterruptPoint = WeakRefToRef(this.m_playerStatusEffectRecordData).StartupAnimInterruptPoint();
        if(startupInterruptPoint < 0) {
          startupInterruptPoint = WeakRefToRef(this.m_playerStatusEffectRecordData).StartupAnimDuration();
        };
        this.m_secondaryKnockdownTimer += startupInterruptPoint - GetTimeInStatusEffect(stateContext, scriptInterface);
      };
    };
  }

  protected final void UpdateQueuedSecondaryKnockdown(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float deltaTime) {
    wref<StatusEffect_Record> statusEffectRecord;
    Uint32 stackcount;
    stackcount = 1;
    if(this.m_secondaryKnockdownTimer > 0 && this.m_animFeatureStatusEffect.state < ToInt(EKnockdownStates.Land)) {
      this.m_secondaryKnockdownTimer -= deltaTime;
      if(this.m_secondaryKnockdownTimer < 0) {
        stateContext.SetPermanentBoolParameter(TriggerSecondaryKnockdownKey(), true, true);
        statusEffectRecord = RefToWeakRef(GetStatusEffectRecord("BaseStatusEffect.SecondaryKnockdown"));
        GetStatusEffectSystem(WeakRefToRef(scriptInterface.owner).GetGame()).ApplyStatusEffect(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), WeakRefToRef(statusEffectRecord).GetID(), GetTDBID(scriptInterface.owner), WeakRefToRef(scriptInterface.owner).GetEntityID(), stackcount, this.m_secondaryKnockdownDir);
      };
    };
  }

  protected final Bool DidPlayerCollideWithWall(ref<StateGameScriptInterface> scriptInterface, out ControllerHit wallCollision) {
    Vector4 playerPosition;
    Float capsuleRadius;
    playerPosition = GetPlayerPosition(scriptInterface);
    capsuleRadius = FromVariant(scriptInterface.GetStateVectorParameter(physicsStateValue.Radius));
    return GetWallCollision(scriptInterface, playerPosition, GetUpVector(), capsuleRadius, wallCollision);
  }
}

public class ForcedKnockdownDecisions extends KnockdownDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasForcedStatusEffect(stateContext, scriptInterface);
  }

  private final const Bool HasForcedStatusEffect(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetStaticStringParameter("statusEffectEnumName", "") == GetForcedStatusEffectName(stateContext, scriptInterface);
  }

  private final const String GetForcedStatusEffectName(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<StatusEffect_Record> statusEffectRecord;
    String statusEffectName;
    statusEffectRecord = RefToWeakRef(Cast(stateContext.GetPermanentScriptableParameter(GetForceKnockdownKey())));
    if(ToBool(statusEffectRecord)) {
      statusEffectName = WeakRefToRef(WeakRefToRef(statusEffectRecord).StatusEffectType()).EnumName();
    };
    return statusEffectName;
  }
}

public class ForcedKnockdownEvents extends KnockdownEvents {

  public Bool m_firstUpdate;

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<StatusEffect_Record> statusEffectRecord;
    String statusEffectName;
    StateResultFloat originalStartTime;
    statusEffectRecord = RefToWeakRef(Cast(stateContext.GetPermanentScriptableParameter(GetForceKnockdownKey())));
    stateContext.RemovePermanentScriptableParameter(GetForceKnockdownKey());
    stateContext.SetTemporaryScriptableParameter(GetAppliedStatusEffectKey(), WeakRefToRef(statusEffectRecord), true);
    originalStartTime = stateContext.GetPermanentFloatParameter(GetStateStartTimeKey());
    OnEnter(stateContext, scriptInterface);
    if(originalStartTime.valid) {
      stateContext.SetPermanentFloatParameter(GetStateStartTimeKey(), originalStartTime.value, true);
    };
    this.m_firstUpdate = true;
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(this.m_firstUpdate) {
      this.m_firstUpdate = false;
    } else {
      OnUpdate(timeDelta, stateContext, scriptInterface);
    };
  }
}
