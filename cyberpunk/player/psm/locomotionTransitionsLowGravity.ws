
public class StandLowGravityEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    OnEnter(stateContext, scriptInterface);
    stateContext.SetPermanentBoolParameter("transitionFromCrouch", false, true);
  }
}

public class PreCrouchLowGravityDecisions extends LocomotionGroundDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool superResult;
    superResult = EnterCondition(stateContext, scriptInterface);
    return CrouchEnterCondition(stateContext, scriptInterface) && superResult;
  }

  protected const Bool ToStandLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetParameterBool("transitionFromCrouch", stateContext, true) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("timeToEnterCrouch", 0.20000000298023224);
  }

  protected const Bool ToCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !GetParameterBool("transitionFromCrouch", stateContext, true) && GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("timeToEnterCrouch", 0.20000000298023224);
  }

  protected const Bool ToDodgeLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) < 0.06499999761581421) {
      return false;
    };
    return scriptInterface.IsActionJustPressed("Dodge") && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 0.10000000149011612 && !GetParameterBool("transitionFromCrouch", stateContext, true);
  }

  protected const Bool ToDodgeCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) < 0.06499999761581421) {
      return false;
    };
    return scriptInterface.IsActionJustPressed("Dodge") && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 0.10000000149011612 && GetParameterBool("transitionFromCrouch", stateContext, true);
  }
}

public class PreCrouchLowGravityEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    scriptInterface.SetAnimationParameterFloat("crouch", 0);
  }

  public final void OnExitToDodgeCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) > 0.06499999761581421) {
      scriptInterface.SetAnimationParameterFloat("crouch", 0.6000000238418579);
    };
  }
}

public class CrouchLowGravityDecisions extends LocomotionGroundDecisions {

  protected const Bool ToCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return true;
  }

  protected const Bool ToPreCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return CrouchExitCondition(stateContext, scriptInterface);
  }
}

public class CrouchLowGravityEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter("CrouchToggled", true, true);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Crouch));
    stateContext.SetPermanentBoolParameter("transitionFromCrouch", true, true);
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
    scriptInterface.SetAnimationParameterFloat("crouch", 0);
  }

  public final void OnExitToSnapToCover(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Crouch));
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
  }

  public final void OnExitToPreCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Crouch));
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
  }
}

public class DodgeLowGravityDecisions extends LocomotionGroundDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustPressed("Dodge") && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 0.5;
  }

  protected final const Bool ToStandLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float maxDuration;
    maxDuration = GetStaticFloatParameter("maxDuration", 0);
    return GetInStateTime(stateContext, scriptInterface) >= maxDuration;
  }
}

public class DodgeLowGravityEvents extends LocomotionGroundEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent("Dodge");
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float maxDuration;
    maxDuration = GetStaticFloatParameter("maxDuration", 0);
    OnExit(stateContext, scriptInterface);
  }
}

public class DodgeCrouchLowGravityDecisions extends LocomotionGroundDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustPressed("Dodge") && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 0.5;
  }

  protected final const Bool ToCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float maxDuration;
    maxDuration = GetStaticFloatParameter("maxDuration", 0);
    return GetInStateTime(stateContext, scriptInterface) >= maxDuration;
  }
}

public class DodgeCrouchLowGravityEvents extends LocomotionGroundEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent("Dodge");
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float maxDuration;
    maxDuration = GetStaticFloatParameter("maxDuration", 0);
    OnExit(stateContext, scriptInterface);
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= GetFloat("cyberware.kereznikovDodge.timeStampToEnter", 0)) {
      stateContext.SetTemporaryBoolParameter("extendKerenzikovDuration", true, true);
    };
  }
}

public class SprintWindupLowGravityDecisions extends SprintLowGravityDecisions {

  protected const Bool ToSprintLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= GetStaticFloatParameter("speedToEnterSprint", 4)) {
      return true;
    };
    return false;
  }
}

public class SprintWindupLowGravityEvents extends SprintLowGravityEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
  }
}

public class SprintLowGravityDecisions extends LocomotionGroundDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float enterAngleThreshold;
    Bool superResult;
    Bool isAiming;
    Bool isReloading;
    isAiming = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
    isReloading = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload);
    superResult = EnterCondition(stateContext, scriptInterface);
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    if(isAiming) {
      return false;
    };
    if(isReloading && !scriptInterface.IsActionJustPressed("ToggleSprint") && !scriptInterface.IsActionJustPressed("Sprint")) {
      return false;
    };
    if(scriptInterface.GetActionValue("AttackA") > 0) {
      return false;
    };
    if(scriptInterface.IsActionJustPressed("ToggleSprint") || GetConditionParameterBool("SprintToggled", stateContext)) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
      return superResult;
    };
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", -180);
    if(!scriptInterface.IsMoveInputConsiderable() || AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold) {
      return false;
    };
    if(scriptInterface.GetActionValue("Sprint") > 0) {
      return superResult;
    };
    return false;
  }

  protected const Bool ToStandLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float enterAngleThreshold;
    if(scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < 0.5) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    if(GetParameterBool("InterruptSprint", stateContext)) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", -180);
    if(!scriptInterface.IsMoveInputConsiderable() || AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    if(!GetConditionParameterBool("SprintToggled", stateContext) && scriptInterface.GetActionValue("Sprint") == 0) {
      return true;
    };
    if(scriptInterface.IsActionJustReleased("Sprint") || scriptInterface.IsActionJustPressed("AttackA")) {
      stateContext.SetConditionBoolParameter("SprintToggled", false, true);
      return true;
    };
    return false;
  }

  protected const Bool ToSprintJumpLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= GetStaticFloatParameter("timeBetweenJumps", 0.20000000298023224)) {
      return true;
    };
    return false;
  }
}

public class SprintLowGravityEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    if(scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) > 2.5) {
      SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Sprint));
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
      scriptInterface.PushAnimationEvent("Jump");
      scriptInterface.SetAnimationParameterFloat("sprint", 0.10000000149011612);
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetPermanentFloatParameter("SprintingStoppedTimeStamp", scriptInterface.GetNow(), true);
  }
}

public class SprintJumpLowGravityDecisions extends LocomotionAirLowGravityDecisions {

  protected const Bool ToSprintLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= 0.30000001192092896) {
      return scriptInterface.IsOnGround();
    };
    return false;
  }

  protected const Bool ToJumpLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) < GetStaticFloatParameter("maxTimeToEnterJump", 0.8999999761581421)) {
      return scriptInterface.IsActionJustPressed("Jump");
    };
    return false;
  }
}

public class SprintJumpLowGravityEvents extends LocomotionAirLowGravityEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    stateContext.SetTemporaryBoolParameter("InterruptReload", true, true);
    stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
  }
}

public class SlideLowGravityDecisions extends CrouchLowGravityDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool superResult;
    return false;
  }

  protected const Bool ToCrouchLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetConditionParameterBool("CrouchToggled", stateContext) || scriptInterface.GetActionValue("Crouch") > 0) {
      return ShouldExit(stateContext, scriptInterface);
    };
    return false;
  }

  protected const Bool ShouldExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < GetStaticFloatParameter("minSpeedToExit", 2);
  }
}

public class SlideLowGravityEvents extends CrouchLowGravityEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float temp;
    OnEnter(stateContext, scriptInterface);
    temp = scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed);
    stateContext.SetConditionBoolParameter("SprintToggled", false, true);
    if(GetStaticBoolParameter("pushAnimEventOnEnter", false)) {
      scriptInterface.PushAnimationEvent("Slide");
    };
  }

  protected void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) >= GetFloat("cyberware.kereznikovSlide.minThresholdToEnter", 0)) {
      stateContext.SetTemporaryBoolParameter("canEnterKerenzikovSlide", true, true);
    };
    if(GetInStateTime(stateContext, scriptInterface) >= 0.10000000149011612) {
      UpdateCrouch(stateContext, scriptInterface);
      UpdateSprint(stateContext, scriptInterface);
    };
  }

  private final void UpdateSprint(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.IsActionJustPressed("ToggleSprint")) {
      stateContext.SetConditionBoolParameter("SprintToggled", true, true);
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    };
  }

  private final void UpdateCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool crouchToggled;
    crouchToggled = GetConditionParameterBool("CrouchToggled", stateContext);
    if(crouchToggled && scriptInterface.IsActionJustReleased("Crouch")) {
      stateContext.SetConditionBoolParameter("CrouchToggled", false, true);
    } else {
      if(scriptInterface.IsActionJustPressed("ToggleCrouch")) {
        stateContext.SetConditionBoolParameter("CrouchToggled", !crouchToggled, true);
        if(crouchToggled) {
          stateContext.SetConditionBoolParameter("SprintToggled", false, true);
        };
      };
    };
  }

  public final void OnExitToCrouch(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Crouch));
    scriptInterface.SetAnimationParameterFloat("crouch", 1);
    stateContext.SetConditionBoolParameter("SprintToggled", false, true);
  }
}

public class LocomotionAirLowGravityDecisions extends LocomotionAirDecisions {

  protected final const Bool ToRegularLandLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 landingType;
    landingType = GetLandingType(stateContext);
    if(!IsTouchingGround(scriptInterface)) {
      return false;
    };
    return landingType < ToInt(LandingType.Regular);
  }
}

public class JumpLowGravityDecisions extends LocomotionAirLowGravityDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustPressed("Jump");
  }

  protected final const Bool ToFallLowGravity(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(scriptInterface.GetActionValue("Jump") < 0.10000000149011612) {
      return true;
    };
    return false;
  }
}

public class JumpLowGravityEvents extends LocomotionAirLowGravityEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Jump));
  }

  protected void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnExit(stateContext, scriptInterface);
    SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, ToInt(gamePSMLocomotionStates.Default));
  }
}

public class FallLowGravityDecisions extends LocomotionAirLowGravityDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(GetInStateTime(stateContext, scriptInterface) < 0.10000000149011612) {
      return false;
    };
    return ShouldFall(stateContext, scriptInterface);
  }
}

public class FallLowGravityEvents extends LocomotionAirLowGravityEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    PlaySound("Player_falling_wind_loop", scriptInterface);
    scriptInterface.PushAnimationEvent("Fall");
  }
}

public class RegularLandLowGravityEvents extends AbstractLandEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
  }
}

public class DodgeAirLowGravityDecisions extends LocomotionAirLowGravityDecisions {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Int32 currentNumberOfAirDodges;
    if(GetStaticBoolParameter("disable", false)) {
      return false;
    };
    currentNumberOfAirDodges = GetParameterInt("currentNumberOfAirDodges", stateContext, true);
    if(currentNumberOfAirDodges >= GetStaticIntParameter("numberOfAirDodges", 1)) {
      return false;
    };
    return scriptInterface.IsActionJustPressed("Dodge") && scriptInterface.IsMoveInputConsiderable();
  }
}

public class DodgeAirLowGravityEvents extends LocomotionAirLowGravityEvents {

  protected void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<SoundPlayEvent> soundEvent;
    Int32 currentNumberOfAirDodges;
    OnEnter(stateContext, scriptInterface);
    currentNumberOfAirDodges = GetParameterInt("currentNumberOfAirDodges", stateContext, true);
    currentNumberOfAirDodges += 1;
    stateContext.SetPermanentIntParameter("currentNumberOfAirDodges", currentNumberOfAirDodges, true);
    scriptInterface.PushAnimationEvent("Dodge");
  }
}

public class ClimbLowGravityDecisions extends LocomotionGroundDecisions {

  private final const Bool OverlapFitTest(ref<StateGameScriptInterface> scriptInterface, ref<PlayerClimbInfo> climbInfo) {
    EulerAngles rotation;
    Bool crouchOverlap;
    TraceResult fitTestOvelap;
    Vector4 playerCapsuleDimensions;
    Vector4 playerPosition;
    Vector4 Z;
    Vector4 queryPosition;
    Z.Z = 1;
    playerPosition = GetPlayerPosition(scriptInterface);
    playerCapsuleDimensions.X = GetStaticFloatParameter("capsuleRadius", 0.4000000059604645);
    playerCapsuleDimensions.Y = -1;
    playerCapsuleDimensions.Z = -1;
    queryPosition = climbInfo.descResult.topPoint + Z * playerCapsuleDimensions.X;
    crouchOverlap = scriptInterface.Overlap(playerCapsuleDimensions, queryPosition, rotation, "Static", fitTestOvelap);
    return !crouchOverlap;
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<PlayerClimbInfo> climbInfo;
    Float enterAngleThreshold;
    if(GetStaticBoolParameter("allowClimbOnlyWhenMovingDown", false) && GetVerticalSpeed(scriptInterface) > 0) {
      return false;
    };
    enterAngleThreshold = GetStaticFloatParameter("enterAngleThreshold", -180);
    if(!scriptInterface.IsMoveInputConsiderable() || !AbsF(scriptInterface.GetInputHeading()) < enterAngleThreshold) {
      return false;
    };
    climbInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    return climbInfo.climbValid && OverlapFitTest(scriptInterface, climbInfo);
  }
}

public class ClimbLowGravityEvents extends LocomotionGroundEvents {

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<PlayerClimbInfo> climbInfo;
    Vector4 direction;
    OnEnter(stateContext, scriptInterface);
    climbInfo = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetPlayerObstacleSystem().GetCurrentClimbInfo(WeakRefToRef(scriptInterface.owner));
    direction = scriptInterface.GetOwnerForward();
    direction = RotByAngleXY(direction, -scriptInterface.GetInputHeading());
    stateContext.SetTemporaryVectorParameter("obstacleVerticalDestination", climbInfo.descResult.topPoint - direction * GetStaticFloatParameter("capsuleRadius", 0), true);
    stateContext.SetTemporaryVectorParameter("obstacleHorizontalDestination", climbInfo.descResult.topPoint, true);
    stateContext.SetTemporaryVectorParameter("obstacleSurfaceNormal", climbInfo.descResult.topNormal, true);
  }
}
