
public class DefaultTransition extends StateFunctor {

  public final void ForceFreeze(ref<StateContext> stateContext) {
    stateContext.SetPermanentBoolParameter("ForceIdle", false, true);
    stateContext.SetPermanentBoolParameter("ForceWalk", false, true);
    stateContext.SetPermanentBoolParameter("ForceFreeze", true, true);
    stateContext.SetPermanentBoolParameter("ForceIdleVehicle", false, true);
  }

  public final void ForceIdle(ref<StateContext> stateContext) {
    stateContext.SetPermanentBoolParameter("ForceIdle", true, true);
    stateContext.SetPermanentBoolParameter("ForceWalk", false, true);
    stateContext.SetPermanentBoolParameter("ForceFreeze", false, true);
    stateContext.SetPermanentBoolParameter("ForceIdleVehicle", false, true);
  }

  public final void ForceIdleVehicle(ref<StateContext> stateContext) {
    stateContext.SetPermanentBoolParameter("ForceIdleVehicle", true, true);
    stateContext.SetPermanentBoolParameter("ForceIdle", false, true);
    stateContext.SetPermanentBoolParameter("ForceWalk", false, true);
    stateContext.SetPermanentBoolParameter("ForceFreeze", false, true);
  }

  public final void ResetForceFlags(ref<StateContext> stateContext) {
    stateContext.SetPermanentBoolParameter("ForceIdle", false, true);
    stateContext.SetPermanentBoolParameter("ForceWalk", false, true);
    stateContext.SetPermanentBoolParameter("ForceFreeze", false, true);
    stateContext.SetPermanentBoolParameter("ForceEmptyHands", false, true);
    stateContext.SetPermanentBoolParameter("ForceSafeState", false, true);
    stateContext.SetPermanentBoolParameter("ForceReadyState", false, true);
    stateContext.SetPermanentBoolParameter("ForceIdleVehicle", false, true);
  }

  public final const ref<IBlackboard> GetBlackboard(ref<StateGameScriptInterface> scriptInterface) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard;
  }

  public final ref<IBlackboard> GetMutableBlackboard(ref<StateGameScriptInterface> scriptInterface) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard;
  }

  public final const Float GetBlackboardFloatVariable(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Float id) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard.GetFloat(id);
  }

  public final void SetBlackboardFloatVariable(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Float id, Float value) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    blackboard.SetFloat(id, value);
    blackboard.SignalFloat(id);
  }

  public final static Int32 GetBlackboardIntVariable(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Int id) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard.GetInt(id);
  }

  public final static Int32 GetBlackboardIntVariable(ref<GameObject> executionOwner, BlackboardID_Int id) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(executionOwner.GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(executionOwner.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard.GetInt(id);
  }

  public final void SetBlackboardIntVariable(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Int id, Int32 value) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    if(ToBool(blackboard)) {
      blackboard.SetInt(id, value);
    };
  }

  public final const Bool GetBlackboardBoolVariable(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Bool id) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard.GetBool(id);
  }

  public final void SetBlackboardBoolVariable(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Bool id, Bool value) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    blackboard.SetBool(id, value);
  }

  public final const Bool GetBoolFromQuestDB(ref<StateGameScriptInterface> scriptInterface, CName varName) {
    return GetQuestsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).GetFact(varName) != 0;
  }

  public final const Float GetInStateTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetInStateTime();
  }

  public final const void HoldAimingForTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float blockAimingFor) {
    stateContext.SetPermanentFloatParameter("HoldAimingTillTimeStamp", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) + blockAimingFor, true);
  }

  public final const void BlockAimingForTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float blockAimingFor) {
    stateContext.SetPermanentFloatParameter("BlockAimingTillTimeStamp", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) + blockAimingFor, true);
  }

  public final const void SoftBlockAimingForTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float blockAimingFor) {
    stateContext.SetPermanentFloatParameter("SoftBlockAimingTillTimeStamp", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) + blockAimingFor, true);
    stateContext.SetTemporaryBoolParameter("InterruptAiming", true, true);
  }

  public final const void ResetSoftBlockAiming(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    stateContext.SetPermanentFloatParameter("SoftBlockAimingTillTimeStamp", ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())), true);
  }

  protected final const Bool HasTimeStampElapsed(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, CName timeStampName) {
    Float timeStampValue;
    timeStampValue = GetParameterFloat(timeStampName, stateContext, true);
    if(timeStampValue < 0) {
      return false;
    };
    return ToFloat(GetSimTime(WeakRefToRef(scriptInterface.owner).GetGame())) < timeStampValue;
  }

  protected final const Bool IsAimingSoftBlocked(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasTimeStampElapsed(stateContext, scriptInterface, "SoftBlockAimingTillTimeStamp");
  }

  protected final const Bool IsAimingHeldForTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasTimeStampElapsed(stateContext, scriptInterface, "HoldAimingTillTimeStamp");
  }

  protected final const Bool IsAimingBlockedForTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasTimeStampElapsed(stateContext, scriptInterface, "BlockAimingTillTimeStamp");
  }

  public final const Bool GetStaticBoolParameter(String parameterName, Bool defaultValue) {
    Bool result;
    StateResultBool stateResult;
    stateResult = GetStaticBoolParameter(parameterName);
    if(stateResult.valid) {
      result = stateResult.value;
    } else {
      result = defaultValue;
    };
    return result;
  }

  public final const Int32 GetStaticIntParameter(String parameterName, Int32 defaultValue) {
    Int32 result;
    StateResultInt stateResult;
    stateResult = GetStaticIntParameter(parameterName);
    if(stateResult.valid) {
      result = stateResult.value;
    } else {
      result = defaultValue;
    };
    return result;
  }

  public final const Float GetStaticFloatParameter(String parameterName, Float defaultValue) {
    Float result;
    StateResultFloat stateResult;
    stateResult = GetStaticFloatParameter(parameterName);
    if(stateResult.valid) {
      result = stateResult.value;
    } else {
      result = defaultValue;
    };
    return result;
  }

  public final const CName GetStaticCNameParameter(String parameterName, CName defaultValue) {
    CName result;
    StateResultCName stateResult;
    stateResult = GetStaticCNameParameter(parameterName);
    if(stateResult.valid) {
      result = stateResult.value;
    } else {
      result = defaultValue;
    };
    return result;
  }

  public final const String GetStaticStringParameter(String parameterName, String defaultValue) {
    String result;
    StateResultString stateResult;
    stateResult = GetStaticStringParameter(parameterName);
    if(stateResult.valid) {
      result = stateResult.value;
    } else {
      result = defaultValue;
    };
    return result;
  }

  public final const Float GetStatFloatValue(ref<StateGameScriptInterface> scriptInterface, gamedataStatType statType, ref<StatsSystem> statSystem, ref<GameObject> object?) {
    Float result;
    ref<GameObject> objectToLookAt;
    StatsObjectID objectToLookAtID;
    if(ToBool(object)) {
      objectToLookAt = object;
    } else {
      objectToLookAt = WeakRefToRef(scriptInterface.owner);
    };
    objectToLookAtID = Cast(objectToLookAt.GetEntityID());
    result = statSystem.GetStatValue(objectToLookAtID, statType);
    return result;
  }

  public final static Bool GetParameterBool(CName parameterName, ref<StateContext> stateContext, Bool permanent?) {
    StateResultBool parameter;
    Bool result;
    if(permanent) {
      parameter = stateContext.GetPermanentBoolParameter(parameterName);
    } else {
      parameter = stateContext.GetTemporaryBoolParameter(parameterName);
    };
    if(parameter.valid) {
      result = parameter.value;
    } else {
      result = false;
    };
    return result;
  }

  public final static Int32 GetParameterInt(CName parameterName, ref<StateContext> stateContext, Bool permanent?) {
    StateResultInt parameter;
    Int32 result;
    if(permanent) {
      parameter = stateContext.GetPermanentIntParameter(parameterName);
    } else {
      parameter = stateContext.GetTemporaryIntParameter(parameterName);
    };
    if(parameter.valid) {
      result = parameter.value;
    };
    return result;
  }

  public final static Float GetParameterFloat(CName parameterName, ref<StateContext> stateContext, Bool permanent?) {
    StateResultFloat parameter;
    Float result;
    if(permanent) {
      parameter = stateContext.GetPermanentFloatParameter(parameterName);
    } else {
      parameter = stateContext.GetTemporaryFloatParameter(parameterName);
    };
    if(parameter.valid) {
      result = parameter.value;
    };
    return result;
  }

  public final static Vector4 GetParameterVector(CName parameterName, ref<StateContext> stateContext, Bool global?) {
    StateResultVector parameter;
    Vector4 result;
    if(global) {
      parameter = stateContext.GetPermanentVectorParameter(parameterName);
    } else {
      parameter = stateContext.GetTemporaryVectorParameter(parameterName);
    };
    if(parameter.valid) {
      result = parameter.value;
    };
    return result;
  }

  public final static Bool GetConditionParameterBool(CName parameterName, ref<StateContext> stateContext) {
    StateResultBool parameter;
    parameter = stateContext.GetConditionBoolParameter(parameterName);
    return parameter.valid && parameter.value;
  }

  public final static Float GetConditionParameterFloat(CName parameterName, ref<StateContext> stateContext) {
    StateResultFloat parameter;
    Float result;
    parameter = stateContext.GetConditionFloatParameter(parameterName);
    return parameter.valid ? parameter.value : 0;
  }

  public final static Int32 GetConditionParameterInt(CName parameterName, ref<StateContext> stateContext) {
    StateResultInt parameter;
    Int32 result;
    parameter = stateContext.GetConditionIntParameter(parameterName);
    return parameter.valid ? parameter.value : 0;
  }

  protected final const Bool ShouldEnterSafe(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    Int32 takedownState;
    blackboard = GetBlackboard(scriptInterface);
    if(!IsPlayerInCombat(scriptInterface) && IsAimingAtFriendlyTarget(scriptInterface) && !ShouldIgnoreWeaponSafe(scriptInterface)) {
      return true;
    };
    if(IsInteractingWithTerminal(scriptInterface)) {
      return true;
    };
    if(IsSafeStateForced(stateContext, scriptInterface)) {
      return true;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus)) {
      return true;
    };
    takedownState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown);
    if(takedownState == ToInt(gamePSMTakedown.Takedown) || takedownState == ToInt(gamePSMTakedown.Leap) || takedownState == ToInt(gamePSMTakedown.Grapple)) {
      return true;
    };
    if(stateContext.IsStateActive("Zoom", "zoomLevelScan") || stateContext.IsStateActive("Zoom", "zoomLevel3") || stateContext.IsStateActive("Zoom", "zoomLevel4")) {
      return true;
    };
    return false;
  }

  protected final const Bool ShouldIgnoreWeaponSafe(ref<StateGameScriptInterface> scriptInterface) {
    if(IsAimingAtChildTarget(scriptInterface) && IsEnemyVisible(scriptInterface, 50)) {
      return true;
    };
    return false;
  }

  protected final const Bool IsAimingAtFriendlyTarget(ref<StateGameScriptInterface> scriptInterface) {
    ref<TargetingSystem> targetingSystem;
    ref<GameObject> targetObject;
    Bool isFriendlyNPC;
    isFriendlyNPC = false;
    targetingSystem = GetTargetingSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    targetObject = targetingSystem.GetLookAtObject(scriptInterface.executionOwner, true, true);
    isFriendlyNPC = IsTargetFriendlyNPC(Cast(WeakRefToRef(scriptInterface.executionOwner)), targetObject);
    return isFriendlyNPC;
  }

  protected final const Bool IsAimingAtChildTarget(ref<StateGameScriptInterface> scriptInterface) {
    ref<TargetingSystem> targetingSystem;
    ref<GameObject> targetObject;
    Bool isFriendlyNPC;
    isFriendlyNPC = false;
    targetingSystem = GetTargetingSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    targetObject = targetingSystem.GetLookAtObject(scriptInterface.executionOwner, true, true);
    isFriendlyNPC = IsTargetChildNPC(Cast(WeakRefToRef(scriptInterface.executionOwner)), targetObject);
    return isFriendlyNPC;
  }

  protected final const Bool IsEnemyVisible(ref<StateGameScriptInterface> scriptInterface, Float distance?) {
    ref<TargetingSystem> targetingSystem;
    Bool isEnemyVisible;
    isEnemyVisible = false;
    targetingSystem = GetTargetingSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    isEnemyVisible = targetingSystem.IsAnyEnemyVisible(scriptInterface.executionOwner, distance);
    return isEnemyVisible;
  }

  protected final const Bool IsEnemyOrSensoryDeviceVisible(ref<StateGameScriptInterface> scriptInterface, Float distance?) {
    ref<TargetingSystem> targetingSystem;
    Bool isEnemyVisible;
    isEnemyVisible = false;
    targetingSystem = GetTargetingSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    isEnemyVisible = targetingSystem.IsAnyEnemyOrSensorVisible(scriptInterface.executionOwner, distance);
    return isEnemyVisible;
  }

  public final static Float GetDistanceToTarget(ref<StateGameScriptInterface> scriptInterface) {
    ref<GameObject> playerObject;
    ref<GameObject> targetObject;
    playerObject = WeakRefToRef(scriptInterface.executionOwner);
    targetObject = GetTargetObject(scriptInterface);
    return Distance2D(playerObject.GetWorldPosition(), targetObject.GetWorldPosition());
  }

  public final static ref<GameObject> GetTargetObject(ref<StateGameScriptInterface> scriptInterface, Float withinDistance?) {
    ref<TargetingSystem> targetingSystem;
    ref<GameObject> targetObject;
    EulerAngles angleOut;
    targetingSystem = GetTargetingSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    targetObject = targetingSystem.GetObjectClosestToCrosshair(scriptInterface.executionOwner, angleOut, TSQ_NPC());
    if(!ToBool(targetingSystem) || !ToBool(targetObject)) {
      return null;
    };
    if(!targetObject.IsPuppet() || !IsActive(targetObject)) {
      return null;
    };
    if(GetAttitudeTowards(targetObject, WeakRefToRef(scriptInterface.executionOwner)) == EAIAttitude.AIA_Friendly) {
      return null;
    };
    if(withinDistance < 0 || Distance(WeakRefToRef(scriptInterface.executionOwner).GetWorldPosition(), targetObject.GetWorldPosition()) < withinDistance) {
      return targetObject;
    };
    return null;
  }

  protected final Bool RequestPlayerPositionAdjustment(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, ref<GameObject> target, Float slideTime, Float distanceRadius, Float rotationDuration, Vector4 adjustPosition, Bool useParabolicMotion?) {
    ref<AdjustTransformWithDurations> adjustRequest;
    adjustRequest = new AdjustTransformWithDurations();
    if(ToBool(target)) {
      adjustRequest.SetTarget(RefToWeakRef(target));
      adjustRequest.SetDistanceRadius(distanceRadius);
    };
    adjustRequest.SetPosition(adjustPosition);
    adjustRequest.SetSlideDuration(slideTime);
    adjustRequest.SetRotation(WeakRefToRef(scriptInterface.executionOwner).GetWorldOrientation());
    adjustRequest.SetRotationDuration(rotationDuration);
    adjustRequest.SetGravity(GetStaticFloatParameter("downwardsGravity", -16));
    adjustRequest.SetUseParabolicMotion(useParabolicMotion);
    stateContext.SetTemporaryScriptableParameter("adjustTransform", adjustRequest, true);
    return true;
  }

  protected final Bool RequestPlayerPositionAdjustmentWithCurve(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float slideTime, Float distanceRadius, Vector4 adjustPosition, CName adjustCurveName) {
    ref<AdjustTransformWithDurations> adjustRequest;
    adjustRequest = new AdjustTransformWithDurations();
    adjustRequest.SetPosition(adjustPosition);
    adjustRequest.SetSlideDuration(slideTime);
    adjustRequest.SetRotationDuration(-1);
    adjustRequest.SetGravity(GetStaticFloatParameter("downwardsGravity", -16));
    adjustRequest.SetDistanceRadius(distanceRadius);
    adjustRequest.SetUseParabolicMotion(true);
    adjustRequest.SetCurve(adjustCurveName);
    stateContext.SetTemporaryScriptableParameter("adjustTransform", adjustRequest, true);
    return true;
  }

  public final static Bool IsInteractingWithTerminal(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> uiBlackboard;
    Variant interactionVariant;
    bbUIInteractionData interactionData;
    Bool isAiming;
    isAiming = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
    if(isAiming) {
      return false;
    };
    uiBlackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(GetAllBlackboardDefs().UIGameData);
    interactionVariant = uiBlackboard.GetVariant(GetAllBlackboardDefs().UIGameData.InteractionData);
    if(IsValid(interactionVariant)) {
      interactionData = FromVariant(interactionVariant);
      if(interactionData.terminalInteractionActive) {
        return true;
      };
    };
    return false;
  }

  public final static Bool HasActiveInteraction(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> uiBlackboard;
    Variant interactionVariant;
    bbUIInteractionData interactionData;
    uiBlackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(GetAllBlackboardDefs().UIGameData);
    interactionVariant = uiBlackboard.GetVariant(GetAllBlackboardDefs().UIGameData.InteractionData);
    if(IsValid(interactionVariant)) {
      interactionData = FromVariant(interactionVariant);
      if(HasAnyInteraction(interactionData)) {
        return true;
      };
    };
    return false;
  }

  protected final const Bool IsDoorInteractionActive(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsDoorInteractionActive);
  }

  public const wref<StatusEffect_Record> GetStatusEffectRecord(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    wref<StatusEffect_Record> statusEffectRecord;
    statusEffectRecord = RefToWeakRef(Cast(stateContext.GetTemporaryScriptableParameter(GetAppliedStatusEffectKey())));
    return statusEffectRecord;
  }

  public const wref<StatusEffectPlayerData_Record> GetStatusEffectPlayerData(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    return GetStatusEffectRecord(WeakRefToRef(GetStatusEffectRecord(stateContext, scriptInterface)).GetID()).PlayerData();
  }

  public const wref<StatusEffectPlayerData_Record> GetStatusEffectRecordData(ref<StateContext> stateContext) {
    wref<StatusEffectPlayerData_Record> recordData;
    recordData = RefToWeakRef(Cast(stateContext.GetConditionScriptableParameter("PlayerStatusEffectRecordData")));
    return recordData;
  }

  public const Bool IsPlayerTired(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Stamina) != ToInt(gamePSMStamina.Rested);
  }

  public const Bool IsPlayerExhausted(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Stamina) == ToInt(gamePSMStamina.Exhausted);
  }

  protected final void ChangeStatPoolValue(ref<StateGameScriptInterface> scriptInterface, EntityID entityID, gamedataStatPoolType statPoolType, Float val) {
    GetStatPoolsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).RequestChangingStatPoolValue(Cast(entityID), statPoolType, val, null, false, false);
  }

  protected final const Float GetStatPoolValue(ref<StateGameScriptInterface> scriptInterface, EntityID entityID, gamedataStatPoolType statPool, Bool asPrecentage?) {
    ref<StatPoolsSystem> statPoolsSystem;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    return statPoolsSystem.GetStatPoolValue(Cast(entityID), statPool, asPrecentage);
  }

  protected final const Bool HasStatPoolValueReachedMax(ref<StateGameScriptInterface> scriptInterface, EntityID entityID, gamedataStatPoolType statPool) {
    ref<StatPoolsSystem> statPoolsSystem;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    return statPoolsSystem.HasStatPoolValueReachedMax(Cast(entityID), statPool);
  }

  protected final void StartStatPoolDecay(ref<StateGameScriptInterface> scriptInterface, gamedataStatPoolType statPoolType) {
    ref<StatPoolsSystem> statPoolsSystem;
    StatsObjectID entityID;
    StatPoolModifier mod;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    entityID = Cast(GetPlayerPuppet(scriptInterface).GetEntityID());
    statPoolsSystem.GetModifier(entityID, statPoolType, gameStatPoolModificationTypes.Decay, mod);
    mod.enabled = true;
    statPoolsSystem.RequestSettingModifier(entityID, statPoolType, gameStatPoolModificationTypes.Decay, mod);
    statPoolsSystem.RequestResetingModifier(entityID, statPoolType, gameStatPoolModificationTypes.Regeneration);
  }

  protected final void StopStatPoolDecayAndRegenerate(ref<StateGameScriptInterface> scriptInterface, gamedataStatPoolType statPoolType) {
    ref<StatPoolsSystem> statPoolsSystem;
    StatsObjectID entityID;
    StatPoolModifier mod;
    statPoolsSystem = GetStatPoolsSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    entityID = Cast(GetPlayerPuppet(scriptInterface).GetEntityID());
    statPoolsSystem.GetModifier(entityID, statPoolType, gameStatPoolModificationTypes.Regeneration, mod);
    mod.enabled = true;
    statPoolsSystem.RequestSettingModifier(entityID, statPoolType, gameStatPoolModificationTypes.Regeneration, mod);
    statPoolsSystem.RequestResetingModifier(entityID, statPoolType, gameStatPoolModificationTypes.Decay);
  }

  public final static void UppercaseFirstChar(out String stringToChange) {
    String firstChar;
    String restOfTheString;
    Int32 length;
    length = StrLen(stringToChange);
    firstChar = StrLeft(stringToChange, 1);
    restOfTheString = StrRight(stringToChange, length - 1);
    firstChar = StrUpper(firstChar);
    stringToChange = firstChar + restOfTheString;
  }

  public final static ref<GameObject> GetOwnerGameObject(ref<StateGameScriptInterface> scriptInterface) {
    ref<GameObject> gameObject;
    gameObject = WeakRefToRef(scriptInterface.executionOwner);
    return gameObject;
  }

  public final static ref<PlayerPuppet> GetPlayerPuppet(ref<StateGameScriptInterface> scriptInterface) {
    return Cast(WeakRefToRef(scriptInterface.executionOwner));
  }

  public final static void PlayRumble(ref<StateGameScriptInterface> scriptInterface, String presetName) {
    CName rumbleName;
    rumbleName = GetCName(Create("rumble.local." + presetName));
    PlaySound(GetPlayerPuppet(scriptInterface), rumbleName);
  }

  public final static void PlayRumbleLoop(ref<StateGameScriptInterface> scriptInterface, String intensity) {
    CName rumbleName;
    rumbleName = GetCName(Create("rumble.local." + "loop_" + intensity));
    PlaySound(GetPlayerPuppet(scriptInterface), rumbleName);
  }

  public final static void StopRumbleLoop(ref<StateGameScriptInterface> scriptInterface, String intensity) {
    CName rumbleName;
    rumbleName = GetCName(Create("rumble.loopstop." + intensity));
    PlaySound(GetPlayerPuppet(scriptInterface), rumbleName);
  }

  public final static void RemoveAllBreathingEffects(ref<StateGameScriptInterface> scriptInterface) {
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.BreathingLow");
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.BreathingMedium");
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.BreathingHeavy");
    RemoveStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.BreathingSick");
  }

  public final static Vector4 GetPlayerPosition(ref<StateGameScriptInterface> scriptInterface) {
    Vector4 playerPosition;
    Variant positionParameter;
    positionParameter = scriptInterface.GetStateVectorParameter(physicsStateValue.Position);
    playerPosition = FromVariant(positionParameter);
    return playerPosition;
  }

  public final static Float GetPlayerDistanceToGround(ref<StateGameScriptInterface> scriptInterface, Float downwardRaycastLength) {
    return Distance2D(GetPlayerPosition(scriptInterface), GetGroundPosition(scriptInterface, downwardRaycastLength));
  }

  public final static Float GetDistanceToGround(ref<StateGameScriptInterface> scriptInterface) {
    ref<GeometryDescriptionQuery> geometryDescription;
    QueryFilter queryFilter;
    ref<GeometryDescriptionResult> geometryDescriptionResult;
    Vector4 currentPosition;
    Float distanceToGround;
    currentPosition = GetPlayerPosition(scriptInterface);
    AddGroup(queryFilter, "Static");
    AddGroup(queryFilter, "Terrain");
    AddGroup(queryFilter, "PlayerBlocker");
    geometryDescription = new GeometryDescriptionQuery();
    geometryDescription.AddFlag(worldgeometryDescriptionQueryFlags.DistanceVector);
    geometryDescription.filter = queryFilter;
    geometryDescription.refPosition = currentPosition;
    geometryDescription.refDirection = new Vector4(0,0,-1,0);
    geometryDescription.primitiveDimension = new Vector4(0.5,0.10000000149011612,0.10000000149011612,0);
    geometryDescription.maxDistance = 100;
    geometryDescription.maxExtent = 100;
    geometryDescription.probingPrecision = 10;
    geometryDescription.probingMaxDistanceDiff = 100;
    geometryDescriptionResult = GetSpatialQueriesSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetGeometryDescriptionSystem().QueryExtents(geometryDescription);
    if(geometryDescriptionResult.queryStatus == worldgeometryDescriptionQueryStatus.NoGeometry || geometryDescriptionResult.queryStatus != worldgeometryDescriptionQueryStatus.OK) {
      return -1;
    };
    distanceToGround = AbsF(geometryDescriptionResult.distanceVector.Z);
    return distanceToGround;
  }

  public final static Vector4 GetGroundPosition(ref<StateGameScriptInterface> scriptInterface, Float inLenght) {
    Vector4 playerFeetPosition;
    Vector4 startPosition;
    Vector4 raycastLenght;
    TraceResult findGround;
    TraceResult findWater;
    Vector4 position;
    playerFeetPosition = GetPlayerPosition(scriptInterface);
    startPosition = playerFeetPosition;
    startPosition.Z = startPosition.Z;
    raycastLenght = playerFeetPosition;
    raycastLenght.Z -= inLenght;
    findGround = scriptInterface.RayCast(startPosition, raycastLenght, "Static");
    findWater = scriptInterface.RayCast(startPosition, raycastLenght, "Water");
    if(IsValid(findGround)) {
      position = Cast(findGround.position);
    } else {
      if(IsValid(findWater)) {
        position = Cast(findWater.position);
      };
    };
    return position;
  }

  protected final const Bool IsDeepEnoughToSwim(ref<StateGameScriptInterface> scriptInterface) {
    Bool deepEnough;
    Float waterLevel;
    Vector4 playerFeetPosition;
    Vector4 depthRaycastDestination;
    playerFeetPosition = GetPlayerPosition(scriptInterface);
    depthRaycastDestination = playerFeetPosition;
    depthRaycastDestination.Z = depthRaycastDestination.Z - 2;
    deepEnough = false;
    if(scriptInterface.GetWaterLevel(playerFeetPosition, depthRaycastDestination, waterLevel)) {
      deepEnough = playerFeetPosition.Z - waterLevel < -0.8999999761581421 + -0.10000000149011612;
    };
    return deepEnough;
  }

  public final static Vector4 GetLinearVelocity(ref<StateGameScriptInterface> scriptInterface) {
    Variant parameter;
    Vector4 velocity;
    parameter = scriptInterface.GetStateVectorParameter(physicsStateValue.LinearVelocity);
    velocity = FromVariant(parameter);
    return velocity;
  }

  public final static Vector4 GetUpVector() {
    Vector4 up;
    up.Z = 1;
    return up;
  }

  public final static Float Get2DLinearSpeed(ref<StateGameScriptInterface> scriptInterface) {
    Variant parameter;
    Vector4 velocity;
    parameter = scriptInterface.GetStateVectorParameter(physicsStateValue.LinearVelocity);
    velocity = FromVariant(parameter);
    return Length2D(velocity);
  }

  protected final const Float GetVerticalSpeed(ref<StateGameScriptInterface> scriptInterface) {
    Variant parameter;
    Vector4 velocity;
    parameter = scriptInterface.GetStateVectorParameter(physicsStateValue.LinearVelocity);
    velocity = FromVariant(parameter);
    return velocity.Z;
  }

  public final static EPlayerMovementDirection GetMovementDirection(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    EPlayerMovementDirection direction;
    Float currentYaw;
    currentYaw = GetYawMovementDirection(stateContext, scriptInterface);
    if(currentYaw >= -45 && currentYaw < 45) {
      direction = EPlayerMovementDirection.Forward;
    } else {
      if(currentYaw > 45 && currentYaw < 135) {
        direction = EPlayerMovementDirection.Right;
      } else {
        if(currentYaw >= 135 && currentYaw < 180 || currentYaw < -135 && currentYaw >= -180) {
          direction = EPlayerMovementDirection.Back;
        } else {
          if(currentYaw > -135 && currentYaw < -45) {
            direction = EPlayerMovementDirection.Left;
          };
        };
      };
    };
    return direction;
  }

  public final static Float GetYawMovementDirection(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 linearVel;
    Vector4 playerForward;
    linearVel = GetLinearVelocity(scriptInterface);
    playerForward = WeakRefToRef(scriptInterface.executionOwner).GetWorldForward();
    return GetAngleDegAroundAxis(linearVel, playerForward, GetUpVector());
  }

  public final static Float GetMovementInputActionValue(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float x;
    Float y;
    Float res;
    x = scriptInterface.GetActionValue("MoveX");
    y = scriptInterface.GetActionValue("MoveY");
    res = SqrtF(SqrF(x) + SqrF(y));
    return res;
  }

  protected final const Bool IsMovementInput(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsMoveInputConsiderable();
  }

  protected final const Bool IsPlayerMoving(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return IsPlayerMovingHorizontally(stateContext, scriptInterface) || IsPlayerMovingVertically(stateContext, scriptInterface);
  }

  protected final const Bool IsPlayerMovingHorizontally(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 playerVelocity;
    Float horizontalSpeed;
    playerVelocity = GetLinearVelocity(scriptInterface);
    horizontalSpeed = Length2D(playerVelocity);
    return horizontalSpeed > 0;
  }

  protected final const Bool IsPlayerMovingVertically(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float verticalSpeed;
    verticalSpeed = GetVerticalSpeed(scriptInterface);
    return verticalSpeed > 0 || verticalSpeed < 0;
  }

  protected final const Bool IsPlayerMovingBackwards(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Float movementDirection;
    movementDirection = GetHorizontalMovementDirection(stateContext, scriptInterface);
    if(movementDirection >= 135 && movementDirection < 180 || movementDirection < -135 && movementDirection >= -180) {
      return true;
    };
    return false;
  }

  protected final const Float GetHorizontalMovementDirection(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Vector4 linearVel;
    Vector4 playerForward;
    linearVel = GetLinearVelocity(scriptInterface);
    playerForward = WeakRefToRef(scriptInterface.executionOwner).GetWorldForward();
    return GetAngleDegAroundAxis(linearVel, playerForward, GetUpVector());
  }

  public final static ref<ItemObject> GetActiveLeftHandItem(ref<StateGameScriptInterface> scriptInterface) {
    return GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponLeft");
  }

  public final static Bool IsHeavyWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = GetActiveWeapon(scriptInterface);
    if(ToBool(weapon)) {
      return GetEquipAreaType(weapon.GetItemID()) == gamedataEquipmentArea.WeaponHeavy;
    };
    return false;
  }

  public final static ref<WeaponObject> GetActiveWeapon(ref<StateGameScriptInterface> scriptInterface) {
    return Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
  }

  public final static Bool IsXYActionInputGreaterEqual(ref<StateGameScriptInterface> scriptInterface, Float threshold) {
    return AbsF(scriptInterface.GetActionValue("MoveX")) >= threshold || AbsF(scriptInterface.GetActionValue("MoveY")) >= threshold;
  }

  public final static Bool IsAxisButtonHeldGreaterEqual(ref<StateGameScriptInterface> scriptInterface, Float threshold) {
    return scriptInterface.GetActionValue("Forward") > 0 && scriptInterface.GetActionStateTime("Forward") > threshold || scriptInterface.GetActionValue("Right") > 0 && scriptInterface.GetActionStateTime("Right") > threshold || scriptInterface.GetActionValue("Back") > 0 && scriptInterface.GetActionStateTime("Back") > threshold || scriptInterface.GetActionValue("Left") > 0 && scriptInterface.GetActionStateTime("Left") > threshold;
  }

  public final const Bool IsSafeStateForced(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetParameterBool("ForceSafeState", stateContext, true) || GetParameterBool("ForceSafeStateByZone", stateContext, true) || GetBoolFromQuestDB(scriptInterface, "ForceSafeState") && !GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.AimForced) && !GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.SafeForced);
  }

  protected final const Float GetActionHoldTime(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, CName actionName) {
    Float holdTime;
    if(scriptInterface.GetActionValue(actionName) > 0) {
      holdTime = scriptInterface.GetActionStateTime(actionName);
      stateContext.SetConditionFloatParameter("InputHoldTime", holdTime, true);
    };
    return holdTime;
  }

  protected final void ToggleAudioAimDownSights(ref<WeaponObject> weapon, Bool toggleOn) {
    ref<ToggleAimDownSightsEvent> ADSToggleEvent;
    ADSToggleEvent = new ToggleAimDownSightsEvent();
    ADSToggleEvent.toggleOn = toggleOn;
    weapon.QueueEvent(ADSToggleEvent);
  }

  protected final void DisableCameraBobbing(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Bool b) {
    SetInputBool(GetPlayerPuppet(scriptInterface), "disable_camera_bobbing", b);
  }

  protected final void StartEffect(ref<StateGameScriptInterface> scriptInterface, CName effectName, ref<worldEffectBlackboard> blackboard?) {
    StartEffectEvent(WeakRefToRef(scriptInterface.executionOwner), effectName, false, blackboard);
  }

  protected final void StopEffect(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    StopEffectEvent(WeakRefToRef(scriptInterface.executionOwner), effectName);
  }

  protected final void BreakEffectLoop(ref<StateGameScriptInterface> scriptInterface, CName effectName) {
    BreakEffectLoopEvent(WeakRefToRef(scriptInterface.executionOwner), effectName);
  }

  protected final void PlaySound(CName soundName, ref<StateGameScriptInterface> scriptInterface) {
    ref<SoundPlayEvent> audioEvent;
    audioEvent = new SoundPlayEvent();
    audioEvent.soundName = soundName;
    WeakRefToRef(scriptInterface.owner).QueueEvent(audioEvent);
  }

  protected final void SetAudioParameter(CName paramName, Float paramValue, ref<StateGameScriptInterface> scriptInterface) {
    ref<SoundParameterEvent> audioParam;
    audioParam = new SoundParameterEvent();
    audioParam.parameterName = paramName;
    audioParam.parameterValue = paramValue;
    WeakRefToRef(scriptInterface.owner).QueueEvent(audioParam);
  }

  protected final void PlaySoundMetadataEvent(CName evtName, ref<StateGameScriptInterface> scriptInterface, Float evtParam) {
    ref<AudioEvent> metadataEvent;
    metadataEvent = new AudioEvent();
    metadataEvent.eventName = evtName;
    metadataEvent.floatData = evtParam;
    metadataEvent.eventFlags = audioAudioEventFlags.Metadata;
    WeakRefToRef(scriptInterface.owner).QueueEvent(metadataEvent);
  }

  protected final void SetSurfaceMaterialProbingDirection(gameaudioeventsSurfaceDirection direction, ref<StateGameScriptInterface> scriptInterface) {
    ref<NotifySurfaceDirectionChangedEvent> directionChangedEvent;
    directionChangedEvent = new NotifySurfaceDirectionChangedEvent();
    directionChangedEvent.surfaceDirection = direction;
    WeakRefToRef(scriptInterface.owner).QueueEvent(directionChangedEvent);
  }

  protected final Bool AdjustPlayerPosition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, ref<GameObject> target, Float duration, Float distanceRadius, CName curveName) {
    ref<AdjustTransformWithDurations> adjustRequest;
    Vector4 pos;
    Vector4 vecToTarget;
    Vector4 position;
    if(!ToBool(target) || duration < 0) {
      return false;
    };
    adjustRequest = new AdjustTransformWithDurations();
    vecToTarget = target.GetWorldPosition() - WeakRefToRef(scriptInterface.executionOwner).GetWorldPosition();
    position = Normalize(vecToTarget) * -0.10000000149011612;
    adjustRequest.SetDistanceRadius(distanceRadius);
    adjustRequest.SetTarget(RefToWeakRef(target));
    adjustRequest.SetSlideDuration(duration);
    adjustRequest.SetPosition(position);
    adjustRequest.SetRotationDuration(-1);
    if(IsNameValid(curveName)) {
      adjustRequest.SetCurve(curveName);
    };
    stateContext.SetTemporaryScriptableParameter("adjustTransform", adjustRequest, true);
    return true;
  }

  public final static Bool HasMeleeWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    ref<TransactionSystem> transactionSystem;
    transactionSystem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weapon = Cast(transactionSystem.GetItemInSlot(WeakRefToRef(scriptInterface.executionOwner), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      if(transactionSystem.HasTag(WeakRefToRef(scriptInterface.executionOwner), GetMeleeWeaponTag(), weapon.GetItemID())) {
        return true;
      };
    };
    return false;
  }

  public final static Bool IsRangedWeaponEquipped(ref<StateGameScriptInterface> scriptInterface) {
    GameInstance game;
    ref<WeaponObject> weapon;
    ref<StatPoolsSystem> statPoolSystem;
    game = WeakRefToRef(scriptInterface.owner).GetGame();
    weapon = Cast(GetTransactionSystem(game).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      statPoolSystem = GetStatPoolsSystem(game);
      if(IsRanged(weapon.GetItemID())) {
        return true;
      };
    };
    return false;
  }

  public final static Bool IsChargeRangedWeapon(ref<StateGameScriptInterface> scriptInterface) {
    GameInstance game;
    ref<WeaponObject> weapon;
    ref<StatPoolsSystem> statPoolSystem;
    game = WeakRefToRef(scriptInterface.owner).GetGame();
    weapon = Cast(GetTransactionSystem(game).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      statPoolSystem = GetStatPoolsSystem(game);
      if(IsRanged(weapon.GetItemID())) {
        return weapon.GetCurrentTriggerMode().Type() == gamedataTriggerMode.Charge;
      };
    };
    return false;
  }

  public final static Bool IsChargingWeapon(ref<StateGameScriptInterface> scriptInterface) {
    ref<WeaponObject> weapon;
    weapon = Cast(GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon) && IsRanged(weapon.GetItemID())) {
      return GetWeaponChargeNormalized(RefToWeakRef(weapon)) > 0;
    };
    return false;
  }

  public final static Bool HasRightWeaponEquipped(ref<StateGameScriptInterface> scriptInterface, Bool checkForTag?) {
    ref<TransactionSystem> transactionSystem;
    ref<WeaponObject> weapon;
    transactionSystem = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    weapon = Cast(transactionSystem.GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight"));
    if(ToBool(weapon)) {
      if(checkForTag && !transactionSystem.HasTag(GetOwnerGameObject(scriptInterface), "Weapon", weapon.GetItemID())) {
        return false;
      };
      return true;
    };
    return false;
  }

  protected final static Bool HasStatFlag(ref<StateGameScriptInterface> scriptInterface, gamedataStatType flag, ref<GameObject> owner?) {
    Bool flagOn;
    if(ToBool(owner)) {
      flagOn = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatBoolValue(Cast(owner.GetEntityID()), flag);
    } else {
      flagOn = GetStatsSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetStatBoolValue(Cast(WeakRefToRef(scriptInterface.executionOwner).GetEntityID()), flag);
    };
    return flagOn;
  }

  protected final void StartPool(ref<StatPoolsSystem> statPoolsSystem, EntityID weaponEntityID, gamedataStatPoolType poolType, Float rangeEnd?, Float valuePerSec?) {
    StatPoolModifier mod;
    statPoolsSystem.GetModifier(Cast(weaponEntityID), poolType, gameStatPoolModificationTypes.Regeneration, mod);
    mod.enabled = true;
    if(rangeEnd > 0) {
      mod.rangeEnd = rangeEnd;
    };
    if(valuePerSec > 0) {
      mod.valuePerSec = valuePerSec;
    };
    statPoolsSystem.RequestSettingModifier(Cast(weaponEntityID), poolType, gameStatPoolModificationTypes.Regeneration, mod);
    statPoolsSystem.RequestResetingModifier(Cast(weaponEntityID), poolType, gameStatPoolModificationTypes.Decay);
  }

  protected final void StopPool(ref<StatPoolsSystem> statPoolsSystem, EntityID weaponEntityID, gamedataStatPoolType poolType, Bool startDecay) {
    StatPoolModifier mod;
    statPoolsSystem.RequestResetingModifier(Cast(weaponEntityID), poolType, gameStatPoolModificationTypes.Regeneration);
    if(startDecay) {
      statPoolsSystem.GetModifier(Cast(weaponEntityID), poolType, gameStatPoolModificationTypes.Decay, mod);
      mod.enabled = true;
      statPoolsSystem.RequestSettingModifier(Cast(weaponEntityID), poolType, gameStatPoolModificationTypes.Decay, mod);
    };
  }

  protected final const Bool GetWeaponItemTag(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, CName tag, ItemID itemID?) {
    ref<WeaponObject> weapon;
    ref<Item_Record> record;
    array<CName> tags;
    Int32 i;
    if(!IsValid(itemID)) {
      weapon = GetActiveWeapon(scriptInterface);
      itemID = weapon.GetItemID();
    };
    record = GetItemRecord(GetTDBID(itemID));
    tags = record.Tags();
    i = 0;
    while(i < Size(tags)) {
      if(tags[i] == tag) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static Bool GetWeaponItemType(ref<StateGameScriptInterface> scriptInterface, ref<WeaponObject> weapon, out gamedataItemType itemType) {
    if(!ToBool(weapon)) {
      return false;
    };
    itemType = WeakRefToRef(GetItemRecord(GetTDBID(weapon.GetItemID())).ItemType()).Type();
    return true;
  }

  public final static Bool IsInWorkspot(ref<StateGameScriptInterface> scriptInterface) {
    ref<WorkspotGameSystem> workspotSystem;
    Bool res;
    workspotSystem = GetWorkspotSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    res = workspotSystem.IsActorInWorkspot(WeakRefToRef(scriptInterface.executionOwner));
    return res;
  }

  protected final const GameplayTier GetCurrentTier(ref<StateContext> stateContext) {
    ref<SceneTier> sceneTier;
    sceneTier = Cast(stateContext.GetPermanentScriptableParameter("SceneTier"));
    if(ToBool(sceneTier)) {
      return sceneTier.GetTier();
    };
    return GameplayTier.Tier1_FullGameplay;
  }

  protected final const ref<SceneTierData> GetCurrentSceneTierData(ref<StateContext> stateContext) {
    ref<SceneTier> sceneTier;
    sceneTier = Cast(stateContext.GetPermanentScriptableParameter("SceneTier"));
    if(ToBool(sceneTier)) {
      return sceneTier.GetTierData();
    };
    return null;
  }

  protected final const Bool IsInMinigame(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInMinigame);
  }

  protected final const Bool IsUploadingQuickHack(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsUploadingQuickHack) > 0;
  }

  public final static Bool IsInRpgContext(ref<StateGameScriptInterface> scriptInterface) {
    ref<InputContextSystem> ics;
    ics = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.owner).GetGame()).Get("InputContextSystem"));
    return ics.IsActiveContextRPG();
  }

  protected final const Bool IsCameraPitchAcceptable(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, Float cameraPitchThreshold) {
    Transform cameraWorldTransform;
    EulerAngles angles;
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    angles = ToEulerAngles(cameraWorldTransform);
    return angles.Pitch < cameraPitchThreshold;
  }

  protected final const Float GetCameraYaw(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Transform cameraWorldTransform;
    EulerAngles angles;
    Float result;
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    result = GetAngleDegAroundAxis(GetForward(cameraWorldTransform), WeakRefToRef(scriptInterface.owner).GetWorldForward(), GetUpVector());
    return result;
  }

  protected final const Bool IsPlayerInCombat(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Combat) == ToInt(gamePSMCombat.InCombat);
  }

  protected final const Bool IsInSafeSceneTier(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.HighLevel) > ToInt(gamePSMHighLevel.SceneTier1);
  }

  protected final const Int32 GetSceneTier(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.HighLevel);
  }

  protected final const ref<IBlackboard> GetSceneGameplayOverridesBlackboard(ref<StateGameScriptInterface> scriptInterface) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> sceneOverridesBlackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    sceneOverridesBlackboard = blackboardSystem.Get(GetAllBlackboardDefs().SceneGameplayOverrides);
    return sceneOverridesBlackboard;
  }

  public final static Bool IsMountedToVehicle(ref<StateGameScriptInterface> scriptInterface) {
    return IsMountedToVehicle(WeakRefToRef(scriptInterface.executionOwner).GetGame(), scriptInterface.executionOwner);
  }

  protected final void SetSceneGameplayOverrideBool(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Bool id, Bool value) {
    ref<IBlackboard> sceneOverridesBlackboard;
    sceneOverridesBlackboard = GetSceneGameplayOverridesBlackboard(scriptInterface);
    sceneOverridesBlackboard.SetBool(id, value);
  }

  protected final const Bool GetSceneGameplayOverrideBool(ref<StateGameScriptInterface> scriptInterface, BlackboardID_Bool id) {
    ref<IBlackboard> sceneOverridesBlackboard;
    sceneOverridesBlackboard = GetSceneGameplayOverridesBlackboard(scriptInterface);
    return sceneOverridesBlackboard.GetBool(id);
  }

  protected final const void ForceDisableVisionMode(ref<StateContext> stateContext) {
    stateContext.SetPermanentBoolParameter("forceDisableVision", true, true);
  }

  protected final const void ForceDisableRadialWheel(ref<StateGameScriptInterface> scriptInterface) {
    ref<ForceRadialWheelShutdown> radialMenuCloseEvt;
    radialMenuCloseEvt = new ForceRadialWheelShutdown();
    WeakRefToRef(scriptInterface.executionOwner).QueueEvent(radialMenuCloseEvt);
  }

  protected final const Bool CheckItemCategoryInQuickWheel(ref<StateGameScriptInterface> scriptInterface, gamedataItemCategory compareToType) {
    GameInstance game;
    ItemID quickSlotID;
    ref<Item_Record> quickSlotRecord;
    ref<TransactionSystem> ts;
    Bool inInventory;
    Bool itemValid;
    ref<EquipmentSystem> eqs;
    game = WeakRefToRef(scriptInterface.executionOwner).GetGame();
    ts = GetTransactionSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    eqs = Cast(GetScriptableSystemsContainer(game).Get("EquipmentSystem"));
    if(!ToBool(eqs)) {
      return false;
    };
    quickSlotID = eqs.GetItemIDFromHotkey(scriptInterface.executionOwner, EHotkey.RB);
    itemValid = IsValid(quickSlotID);
    inInventory = ts.GetItemQuantity(WeakRefToRef(scriptInterface.executionOwner), quickSlotID) > 0;
    quickSlotRecord = GetItemRecord(GetTDBID(quickSlotID));
    return WeakRefToRef(quickSlotRecord.ItemCategory()).Type() == compareToType && itemValid && inInventory;
  }

  protected final const Bool IsQuickWheelItemACyberdeck(ref<StateGameScriptInterface> scriptInterface) {
    ItemID quickSlotID;
    wref<Item_Record> itemRecord;
    array<CName> itemTags;
    ref<EquipmentSystem> eqs;
    Bool checkSuccessful;
    eqs = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("EquipmentSystem"));
    if(!ToBool(eqs)) {
      return false;
    };
    quickSlotID = eqs.GetItemIDFromHotkey(scriptInterface.executionOwner, EHotkey.RB);
    itemRecord = RefToWeakRef(GetItemRecord(quickSlotID));
    itemTags = WeakRefToRef(itemRecord).Tags();
    checkSuccessful = IsValid(quickSlotID) && Contains(itemTags, "Cyberdeck");
    return checkSuccessful;
  }

  protected final const Bool IsInFocusMode(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus);
  }

  protected final const void SetZoomStateAnimFeature(ref<StateGameScriptInterface> scriptInterface, Bool shouldAim) {
    ref<AnimFeature_AimPlayer> af;
    af = new AnimFeature_AimPlayer();
    if(shouldAim) {
      af.SetZoomState(animAimState.Aimed);
    } else {
      af.SetZoomState(animAimState.Unaimed);
    };
    af.SetAimInTime(0.20000000298023224);
    af.SetAimOutTime(0.20000000298023224);
    scriptInterface.SetAnimationParameterFeature("AnimFeature_AimPlayer", af);
  }

  protected final const ref<SceneSystemInterface> GetSceneSystemInterface(ref<StateGameScriptInterface> scriptInterface) {
    return GetSceneSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).GetScriptInterface();
  }

  protected final Bool PrepareGameEffectAoEAttack(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, ref<Attack_Record> attackRecord) {
    Float attackRadius;
    ref<Attack_GameEffect> attack;
    ref<EffectInstance> effect;
    AttackInitContext attackContext;
    array<ref<gameStatModifierData>> statMods;
    attackRadius = attackRecord.Range();
    attackContext.record = attackRecord;
    attackContext.instigator = scriptInterface.executionOwner;
    attackContext.source = scriptInterface.executionOwner;
    attack = Cast(Create(attackContext));
    attack.GetStatModList(statMods);
    effect = attack.PrepareAttack(scriptInterface.executionOwner);
    if(!ToBool(attack)) {
      return false;
    };
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, attackRadius);
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, attackRadius);
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, WeakRefToRef(scriptInterface.executionOwner).GetWorldPosition());
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(attack));
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackStatModList, ToVariant(statMods));
    attack.StartAttack();
    return true;
  }

  protected final const Bool IsPlayerInBraindance(ref<StateGameScriptInterface> scriptInterface) {
    return GetSceneSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).GetScriptInterface().IsRewindableSectionActive();
  }

  protected final const ref<BraindanceSystem> GetBraindanceSystem(ref<StateGameScriptInterface> scriptInterface) {
    ref<BraindanceSystem> bdSys;
    bdSys = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("BraindanceSystem"));
    return bdSys;
  }

  protected final const Bool IsInPhotoMode(ref<StateGameScriptInterface> scriptInterface) {
    ref<PhotoModeSystem> photoModeSys;
    photoModeSys = GetPhotoModeSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    return photoModeSys.IsPhotoModeActive();
  }

  protected final const void SendEquipmentSystemWeaponManipulationRequest(ref<StateGameScriptInterface> scriptInterface, EquipmentManipulationAction requestType, gameEquipAnimationType equipAnimType?) {
    ref<EquipmentSystemWeaponManipulationRequest> request;
    ref<EquipmentSystem> eqs;
    eqs = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("EquipmentSystem"));
    request = new EquipmentSystemWeaponManipulationRequest();
    request.owner = scriptInterface.executionOwner;
    request.requestType = requestType;
    if(equipAnimType != gameEquipAnimationType.Default) {
      request.equipAnimType = equipAnimType;
    };
    eqs.QueueRequest(request);
  }

  protected final const void SendDrawItemRequest(ref<StateGameScriptInterface> scriptInterface, ItemID item, gameEquipAnimationType equipAnimType?) {
    ref<DrawItemRequest> drawItem;
    wref<EquipmentSystem> equipmentSystem;
    drawItem = new DrawItemRequest();
    equipmentSystem = RefToWeakRef(Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("EquipmentSystem")));
    drawItem.owner = scriptInterface.executionOwner;
    drawItem.itemID = item;
    if(equipAnimType != gameEquipAnimationType.Default) {
      drawItem.equipAnimationType = equipAnimType;
    };
    WeakRefToRef(equipmentSystem).QueueRequest(drawItem);
  }

  protected final const Bool IsItemMeleeWeapon(ItemID item) {
    array<CName> tags;
    tags = GetWeaponItemRecord(GetTDBID(item)).Tags();
    return Contains(tags, GetMeleeWeaponTag());
  }

  protected final const ItemID GetLeftHandItemFromParam(ref<StateContext> stateContext) {
    ref<ItemIdWrapper> wrapper;
    wrapper = Cast(stateContext.GetPermanentScriptableParameter("leftHandItem"));
    if(ToBool(wrapper)) {
      return wrapper.itemID;
    };
    return undefined();
  }

  protected final const ItemID GetRightHandItemFromParam(ref<StateContext> stateContext) {
    ref<ItemIdWrapper> wrapper;
    wrapper = Cast(stateContext.GetPermanentScriptableParameter("rightHandItem"));
    if(ToBool(wrapper)) {
      return wrapper.itemID;
    };
    return undefined();
  }

  protected final const Bool IsLookingAtEnemyNPC(ref<StateGameScriptInterface> scriptInterface) {
    GameInstance game;
    ref<HUDManager> hudManager;
    ref<ScriptedPuppet> npc;
    game = WeakRefToRef(scriptInterface.executionOwner).GetGame();
    hudManager = Cast(GetScriptableSystemsContainer(game).Get("HUDManager"));
    if(ToBool(hudManager)) {
      npc = Cast(FindEntityByID(game, hudManager.GetCurrentTargetID()));
      return ToBool(npc) && npc.IsHostile();
    };
    return false;
  }

  protected final const ref<HUDManager> GetHudManager(ref<StateGameScriptInterface> scriptInterface) {
    ref<HUDManager> hudManager;
    hudManager = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("HUDManager"));
    return hudManager;
  }

  public final void SetGameplayCameraParameters(ref<StateGameScriptInterface> scriptInterface, String tweakDBPath) {
    ref<GameplayCameraData> cameraParameters;
    ref<AnimFeature_CameraGameplay> animFeature;
    GetGameplayCameraParameters(cameraParameters, tweakDBPath);
    animFeature = new AnimFeature_CameraGameplay();
    animFeature.is_forward_offset = cameraParameters.is_forward_offset;
    animFeature.forward_offset_value = cameraParameters.forward_offset_value;
    animFeature.upperbody_pitch_weight = cameraParameters.upperbody_pitch_weight;
    animFeature.upperbody_yaw_weight = cameraParameters.upperbody_yaw_weight;
    animFeature.is_pitch_off = cameraParameters.is_pitch_off;
    animFeature.is_yaw_off = cameraParameters.is_yaw_off;
    scriptInterface.SetAnimationParameterFeature("CameraGameplay", animFeature);
  }

  public final void GetGameplayCameraParameters(out ref<GameplayCameraData> cameraParameters, String tweakDBPath) {
    cameraParameters = new GameplayCameraData();
    cameraParameters.is_forward_offset = GetFloat(Create("player." + tweakDBPath + "." + "is_forward_offset"), 0);
    cameraParameters.forward_offset_value = GetFloat(Create("player." + tweakDBPath + "." + "forward_offset_value"), 0);
    cameraParameters.upperbody_pitch_weight = GetFloat(Create("player." + tweakDBPath + "." + "upperbody_pitch_weight"), 0);
    cameraParameters.upperbody_yaw_weight = GetFloat(Create("player." + tweakDBPath + "." + "upperbody_yaw_weight"), 0);
    cameraParameters.is_pitch_off = GetFloat(Create("player." + tweakDBPath + "." + "is_pitch_off"), 0);
    cameraParameters.is_yaw_off = GetFloat(Create("player." + tweakDBPath + "." + "is_yaw_off"), 0);
  }

  public final static Bool DEBUG_IsSwimmingForced(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return DEBUG_IsSurfaceSwimmingForced(stateContext, scriptInterface) || DEBUG_IsDivingForced(stateContext, scriptInterface);
  }

  public final static Bool DEBUG_IsSurfaceSwimmingForced(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.ForceSwim");
  }

  public final static Bool DEBUG_IsDivingForced(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return HasStatusEffect(scriptInterface.executionOwner, "BaseStatusEffect.ForceDive");
  }

  protected final const Bool IsInTier2Locomotion(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "Tier2Locomotion");
  }

  protected final const Bool IsAimForced(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "ForceAim");
  }

  protected final const Bool IsCrouchForced(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "ForceCrouch");
  }

  protected final const Bool IsVaultingClimbingRestricted(ref<StateGameScriptInterface> scriptInterface) {
    return IsInTier2Locomotion(scriptInterface) || HasRestriction(scriptInterface.executionOwner, "NoJump") || HasRestriction(scriptInterface.executionOwner, "BodyCarryingGeneric");
  }

  protected final const Bool IsUsingMeleeForced(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "Fists") || HasRestriction(scriptInterface.executionOwner, "Melee");
  }

  protected final const Bool IsUsingFistsForced(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "Fists");
  }

  protected final const Bool IsUsingFirearmsForced(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "Firearms") || HasRestriction(scriptInterface.executionOwner, "FirearmsNoUnequip") || HasRestriction(scriptInterface.executionOwner, "FirearmsNoSwitch") || HasRestriction(scriptInterface.executionOwner, "ShootingRangeCompetition");
  }

  protected final const Bool IsNoCombatActionsForced(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "NoCombat") || HasRestriction(scriptInterface.executionOwner, "VehicleScene");
  }

  protected final const Bool IsVehicleCameraChangeBlocked(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "VehicleFPP") || HasRestriction(scriptInterface.executionOwner, "VehicleCombatNoInterruptions");
  }

  protected final const Bool IsVehicleExitCombatModeBlocked(ref<StateGameScriptInterface> scriptInterface) {
    return HasRestriction(scriptInterface.executionOwner, "VehicleCombatBlockExit");
  }

  protected final const Bool HasAnyValidWeaponAvailable(ref<StateGameScriptInterface> scriptInterface) {
    return GetFirstAvailableWeapon(WeakRefToRef(scriptInterface.executionOwner)) != undefined();
  }

  protected final const Bool IsUsingLeftHandAllowed(ref<StateGameScriptInterface> scriptInterface) {
    if(HasStatusEffect(scriptInterface.executionOwner, gamedataStatusEffectType.Stunned)) {
      return false;
    };
    if(IsNoCombatActionsForced(scriptInterface)) {
      return false;
    };
    if(IsUsingFirearmsForced(scriptInterface)) {
      return false;
    };
    if(IsUsingFistsForced(scriptInterface)) {
      return false;
    };
    if(IsUsingMeleeForced(scriptInterface)) {
      return false;
    };
    if(IsCarryingBody(scriptInterface)) {
      return false;
    };
    return true;
  }

  protected final const Bool IsUsingConsumableRestricted(ref<StateGameScriptInterface> scriptInterface) {
    if(IsHotkeyRestricted(WeakRefToRef(scriptInterface.executionOwner).GetGame(), EHotkey.DPAD_UP)) {
      return true;
    };
    if(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.HighLevel) >= ToInt(gamePSMHighLevel.SceneTier3)) {
      return true;
    };
    if(IsCarryingBody(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected final const ETakedownActionType GetTakedownAction(ref<StateContext> stateContext) {
    CName enumName;
    StateResultCName param;
    param = stateContext.GetPermanentCNameParameter("ETakedownActionType");
    enumName = param.value;
    return ToEnum(Cast(EnumValueFromName("ETakedownActionType", enumName)));
  }

  public final const Bool IsEmptyHandsForced(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return !GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.AimForced) && !GetSceneGameplayOverrideBool(scriptInterface, GetAllBlackboardDefs().SceneGameplayOverrides.SafeForced) && GetParameterBool("ForceEmptyHands", stateContext, true) || GetParameterBool("ForceEmptyHandsByZone", stateContext, true) || GetBoolFromQuestDB(scriptInterface, "ForceEmptyHands") || GetParameterBool("InVehicle", stateContext, true) || IsNoCombatActionsForced(scriptInterface);
  }

  protected final const Bool CheckGenericEquipItemConditions(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.CanEquipItem(stateContext) && GetSceneTier(scriptInterface) < 5 && !IsInLocomotionState(stateContext, "vault") && !GetParameterBool("UninteruptibleReload", stateContext, true) && !IsEmptyHandsForced(stateContext, scriptInterface) || HasActiveConsumable(scriptInterface) && !IsInItemWheelState(stateContext) && stateContext.GetStateMachineCurrentState("Vehicle") != "entering" && GetVehicleAllowsCombat(WeakRefToRef(scriptInterface.executionOwner).GetGame(), scriptInterface.executionOwner);
  }

  protected final const Bool IsCarryingBody(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Carrying);
  }

  protected final const Bool IsExaminingDevice(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsUIZoomDevice);
  }

  protected final const Bool HasActiveConsumable(ref<StateGameScriptInterface> scriptInterface) {
    ItemID consumable;
    consumable = GetData(WeakRefToRef(scriptInterface.executionOwner)).GetActiveConsumable();
    return IsValid(consumable);
  }

  public final const Bool IsInItemWheelState(ref<StateContext> stateContext) {
    CName quickSlotsStateName;
    quickSlotsStateName = stateContext.GetStateMachineCurrentState("QuickSlots");
    return quickSlotsStateName == "WeaponWheel" || quickSlotsStateName == "GadgetWheel" || quickSlotsStateName == "VehicleWheel" || quickSlotsStateName == "InteractionWheel" || quickSlotsStateName == "ConsumableWheel";
  }

  public final const Bool IsInEmptyHandsState(ref<StateContext> stateContext) {
    CName upperBodyStateName;
    upperBodyStateName = stateContext.GetStateMachineCurrentState("UpperBody");
    return upperBodyStateName == "emptyHands" || upperBodyStateName == "forceEmptyHands" || upperBodyStateName == "forceSafe";
  }

  public final const Bool IsInUpperBodyState(ref<StateContext> stateContext, CName upperBodyStateName) {
    CName upperBodyState;
    upperBodyState = stateContext.GetStateMachineCurrentState("UpperBody");
    return upperBodyState == upperBodyStateName;
  }

  public final const Bool IsInHighLevelState(ref<StateContext> stateContext, CName highLevelStateName) {
    CName highLevelState;
    highLevelState = stateContext.GetStateMachineCurrentState("HighLevel");
    return highLevelState == highLevelStateName;
  }

  public final const Bool IsInWeaponReloadState(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload);
  }

  public final const Bool IsWeaponBlockingAiming(ref<StateGameScriptInterface> scriptInterface) {
    Int32 weaponState;
    weaponState = GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon);
    if(weaponState == ToInt(gamePSMRangedWeaponStates.QuickMelee) || weaponState == ToInt(gamePSMRangedWeaponStates.Overheat) || weaponState == ToInt(gamePSMRangedWeaponStates.Reload)) {
      return true;
    };
    return false;
  }

  public final const Bool IsWeaponStateBlockingAiming(ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.Reload);
  }

  public final const Bool IsInVisionModeActiveState(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus);
  }

  public final const Bool IsInTakedownState(ref<StateContext> stateContext) {
    return stateContext.IsStateMachineActive("LocomotionTakedown");
  }

  public final const Bool IsInLocomotionState(ref<StateContext> stateContext, CName locomotionStateName) {
    CName locomotionState;
    locomotionState = stateContext.GetStateMachineCurrentState("Locomotion");
    return locomotionState == locomotionStateName;
  }

  public final const CName GetLocomotionState(ref<StateContext> stateContext) {
    CName locomotionState;
    locomotionState = stateContext.GetStateMachineCurrentState("Locomotion");
    return locomotionState;
  }

  public final const Bool IsInVehicleState(ref<StateContext> stateContext, CName vehicleStateName) {
    CName vehicleState;
    vehicleState = stateContext.GetStateMachineCurrentState("Vehicle");
    return vehicleState == vehicleStateName;
  }

  public final const Bool IsInInputContextState(ref<StateContext> stateContext, CName inputContextStateName) {
    CName inputContextState;
    inputContextState = stateContext.GetStateMachineCurrentState("InputContext");
    return inputContextState == inputContextStateName;
  }

  public final const Bool IsInLadderState(ref<StateContext> stateContext) {
    CName ladderState;
    ladderState = stateContext.GetStateMachineCurrentState("Locomotion");
    return ladderState == "ladder" || ladderState == "ladderSprint" || ladderState == "ladderSlide" || ladderState == "ladderCrouch" || ladderState == "ladderJump";
  }

  public final const Bool IsInMeleeState(ref<StateContext> stateContext, CName meleeStateName) {
    CName meleeState;
    StateMachineIdentifier identifier;
    identifier.definitionName = "Melee";
    identifier.referenceName = "WeaponRight";
    meleeState = stateContext.GetStateMachineCurrentStateWithIdentifier(identifier);
    return meleeState == meleeStateName;
  }

  protected final const Bool IsInSlidingState(ref<StateContext> stateContext) {
    CName slidingState;
    slidingState = stateContext.GetStateMachineCurrentState("Locomotion");
    return slidingState == "slide" || slidingState == "slideAfterFall" || slidingState == "slideFall";
  }

  public final const Bool CompareSMState(CName smName, CName smState, ref<StateContext> stateContext) {
    CName smCurrentState;
    smCurrentState = stateContext.GetStateMachineCurrentState(smName);
    return smCurrentState == smState;
  }

  public final const Bool CompareSMStateWithIden(CName definitionName, CName referenceName, CName smState, ref<StateContext> stateContext) {
    CName smCurrentState;
    StateMachineIdentifier identifier;
    identifier.definitionName = definitionName;
    identifier.referenceName = referenceName;
    smCurrentState = stateContext.GetStateMachineCurrentStateWithIdentifier(identifier);
    return smCurrentState == smState;
  }

  public final const Bool CompareSMState(CName smName, array<CName> smState, ref<StateContext> stateContext) {
    CName smCurrentState;
    Int32 i;
    smCurrentState = stateContext.GetStateMachineCurrentState(smName);
    i = 0;
    while(i < Size(smState)) {
      if(smCurrentState == smState[i]) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  protected final const Bool CheckActiveConsumable(ref<StateGameScriptInterface> scriptInterface) {
    ref<TransactionSystem> ts;
    Bool itemValid;
    Bool inInventory;
    ts = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame());
    itemValid = IsValid(GetData(WeakRefToRef(scriptInterface.owner)).GetActiveConsumable());
    inInventory = ts.GetItemQuantity(WeakRefToRef(scriptInterface.owner), GetData(WeakRefToRef(scriptInterface.owner)).GetActiveConsumable()) > 0;
    return itemValid && inInventory;
  }

  protected final const ref<ItemObject> GetItemInRightHandSlot(ref<StateGameScriptInterface> scriptInterface) {
    ref<TransactionSystem> transactionSystem;
    transactionSystem = GetTransactionSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    return transactionSystem.GetItemInSlot(WeakRefToRef(scriptInterface.owner), "AttachmentSlots.WeaponRight");
  }

  protected final const ref<ItemObject> GetItemInLeftHandSlot(ref<StateGameScriptInterface> scriptInterface) {
    ref<TransactionSystem> transactionSystem;
    transactionSystem = GetTransactionSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    return transactionSystem.GetItemInSlot(WeakRefToRef(scriptInterface.owner), "AttachmentSlots.WeaponLeft");
  }

  protected final const Bool IsRightHandInEquippedState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "RightHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "equipped");
  }

  protected final const Bool IsRightHandInUnequippedState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "RightHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "unequipped");
  }

  protected final const Bool IsRightHandChangingEquipState(ref<StateContext> stateContext) {
    return IsRightHandInUnequippingState(stateContext) || IsRightHandInEquippingState(stateContext);
  }

  protected final const Bool IsRightHandInUnequippingState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "RightHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "unequipCycle");
  }

  protected final const Bool IsRightHandInEquippingState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "RightHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "equipCycle");
  }

  protected final const Bool IsLeftHandInEquippedState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "LeftHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "equipped");
  }

  protected final const Bool IsLeftHandInUnequippedState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "LeftHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "unequipped");
  }

  protected final const Bool IsLeftHandInUnequippingState(ref<StateContext> stateContext) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = "LeftHand";
    return stateContext.IsStateActiveWithIdentifier(smIden, "unequipCycle");
  }

  protected final const CName GetReferenceNameFromEquipmentSide(EEquipmentSide side) {
    switch(side) {
      case EEquipmentSide.Left:
        return "LeftHand";
      case EEquipmentSide.Right:
        return "RightHand";
      default:
        return "error DT01";
    };
  }

  protected final const CName GetStateNameFromEquipmentState(EEquipmentState equipmentState) {
    switch(equipmentState) {
      case EEquipmentState.Unequipped:
        return "unequipped";
      case EEquipmentState.Equipped:
        return "equipped";
      case EEquipmentState.Equipping:
        return "equipCycle";
      case EEquipmentState.Unequipping:
        return "unequipCycle";
      case EEquipmentState.FirstEquip:
        return "firstEquip";
      default:
        return "error DT02";
    };
  }

  protected final const Bool CheckEquipmentStateMachineState(ref<StateContext> stateContext, EEquipmentSide SMSide, EEquipmentState compareToState) {
    StateMachineIdentifier smIden;
    smIden.definitionName = "Equipment";
    smIden.referenceName = GetReferenceNameFromEquipmentSide(SMSide);
    return stateContext.IsStateActiveWithIdentifier(smIden, GetStateNameFromEquipmentState(compareToState));
  }

  protected final const Bool IsInFirstEquip(ref<StateContext> stateContext) {
    StateResultBool result;
    result = stateContext.GetConditionBoolParameter("firstEquip");
    return result.valid && result.value;
  }

  protected final const Bool AreChoiceHubsActive(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> interactonsBlackboard;
    ref<UIInteractionsDef> interactionData;
    DialogChoiceHubs data;
    interactonsBlackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(GetAllBlackboardDefs().UIInteractions);
    interactionData = GetAllBlackboardDefs().UIInteractions;
    data = FromVariant(interactonsBlackboard.GetVariant(interactionData.DialogChoiceHubs));
    return Size(data.choiceHubs) > 0;
  }

  protected final const LootData GetLootData(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> interactonsBlackboard;
    ref<UIInteractionsDef> interactionData;
    LootData data;
    interactonsBlackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(GetAllBlackboardDefs().UIInteractions);
    interactionData = GetAllBlackboardDefs().UIInteractions;
    data = FromVariant(interactonsBlackboard.GetVariant(interactionData.LootData));
    return data;
  }

  protected final const Bool IsLootDataActive(ref<StateGameScriptInterface> scriptInterface) {
    LootData data;
    data = GetLootData(scriptInterface);
    return data.isActive;
  }

  protected final const Int32 ItemsInLootData(ref<StateGameScriptInterface> scriptInterface) {
    LootData data;
    array<ItemID> items;
    data = GetLootData(scriptInterface);
    items = data.itemIDs;
    return Size(items);
  }

  protected final const Bool CheckConsumableLootDataCondition(ref<StateGameScriptInterface> scriptInterface) {
    if(IsLootDataActive(scriptInterface)) {
      return ItemsInLootData(scriptInterface) < 1;
    };
    return true;
  }

  protected final const void SetItemIDWrapperPermanentParameter(ref<StateContext> stateContext, CName parameterName, ItemID item) {
    ref<ItemIdWrapper> wrapper;
    wrapper = new ItemIdWrapper();
    wrapper.itemID = item;
    stateContext.SetPermanentScriptableParameter(parameterName, wrapper, true);
  }

  protected final const ItemID GetItemIDFromWrapperPermanentParameter(ref<StateContext> stateContext, CName parameterName) {
    ref<ItemIdWrapper> wrapper;
    wrapper = Cast(stateContext.GetPermanentScriptableParameter(parameterName));
    if(ToBool(wrapper)) {
      return wrapper.itemID;
    };
    return undefined();
  }

  protected final const void ClearItemIDWrapperPermanentParameter(ref<StateContext> stateContext, CName parameterName) {
    stateContext.RemovePermanentScriptableParameter(parameterName);
  }

  protected final const Bool IsPlayerInAnyMenu(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    ref<UI_SystemDef> uiSystemBB;
    blackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(GetAllBlackboardDefs().UI_System);
    uiSystemBB = GetAllBlackboardDefs().UI_System;
    return blackboard.GetBool(uiSystemBB.IsInMenu);
  }

  protected final const void SendDataTrackingRequest(ref<StateGameScriptInterface> scriptInterface, ETelemetryData telemetryData, Int32 modifyValue) {
    ref<ModifyTelemetryVariable> request;
    request = new ModifyTelemetryVariable();
    request.dataTrackingFact = telemetryData;
    request.value = modifyValue;
    GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get("DataTrackingSystem").QueueRequest(request);
  }

  protected final void RequestVehicleCameraPerspective(ref<StateGameScriptInterface> scriptInterface, vehicleCameraPerspective requestedCameraPerspective) {
    ref<vehicleRequestCameraPerspectiveEvent> camEvent;
    camEvent = new vehicleRequestCameraPerspectiveEvent();
    camEvent.cameraPerspective = requestedCameraPerspective;
    WeakRefToRef(scriptInterface.executionOwner).QueueEvent(camEvent);
  }

  protected final void SetVehicleCameraSceneMode(ref<StateGameScriptInterface> scriptInterface, Bool sceneMode) {
    ref<vehicleCameraSceneEnableEvent> camEvent;
    camEvent = new vehicleCameraSceneEnableEvent();
    camEvent.scene = sceneMode;
    WeakRefToRef(scriptInterface.executionOwner).QueueEvent(camEvent);
  }

  protected final const Bool IsInSafeZone(ref<StateGameScriptInterface> scriptInterface) {
    return ToEnum(GetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Zones)) == gamePSMZones.Safe;
  }

  protected final void TutorialSetFact(ref<StateGameScriptInterface> scriptInterface, CName factName) {
    ref<QuestsSystem> questSystem;
    questSystem = GetQuestsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    if(questSystem.GetFact(factName) == 0 && questSystem.GetFact("disable_tutorials") == 0) {
      questSystem.SetFact(factName, 1);
    };
  }

  protected final const void TutorialAddFact(ref<StateGameScriptInterface> scriptInterface, CName factName, Int32 add) {
    ref<QuestsSystem> questSystem;
    Int32 val;
    questSystem = GetQuestsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    if(questSystem.GetFact("disable_tutorials") == 0) {
      val = questSystem.GetFact(factName) + add;
      questSystem.SetFact(factName, val);
    };
  }

  protected final const Bool IsQuickHackPanelOpened(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> bb;
    bb = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame()).Get(GetAllBlackboardDefs().UI_QuickSlotsData);
    return bb.GetBool(GetAllBlackboardDefs().UI_QuickSlotsData.quickhackPanelOpen);
  }

  protected final const Bool IsRadialWheelOpen(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> bb;
    bb = GetBlackboardSystem(WeakRefToRef(scriptInterface.owner).GetGame()).Get(GetAllBlackboardDefs().UI_QuickSlotsData);
    return bb.GetBool(GetAllBlackboardDefs().UI_QuickSlotsData.UIRadialContextRequest);
  }

  protected final const Bool IsTimeDilationActive(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface, CName timeDilationReason) {
    ref<TimeSystem> timeSystem;
    timeSystem = GetTimeSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    return timeSystem.IsTimeDilationActive(timeDilationReason);
  }

  protected final const Bool ThreatsOnPlayerThreatList(ref<StateGameScriptInterface> scriptInterface) {
    return GetPlayerPuppet(scriptInterface).GetTargetTrackerComponent().HasHostileThreat(false);
  }

  protected final const Bool IsPlayerInSecuritySystem(ref<StateGameScriptInterface> scriptInterface) {
    array<PersistentID> overlappedZones;
    overlappedZones = GetPlayerPuppet(scriptInterface).GetOverlappedSecurityZones();
    return Size(overlappedZones) > 0;
  }

  protected final const Bool IsInStealthLocomotion(ref<StateContext> stateContext) {
    return CompareSMState("Locomotion", "crouch", stateContext);
  }

  protected final const void ShowInputHint(ref<StateGameScriptInterface> scriptInterface, CName actionName, CName source, String label, inkInputHintHoldIndicationType holdIndicationType?, Bool enableHoldAnimation?) {
    ref<UpdateInputHintEvent> evt;
    InputHintData data;
    if(IsDisplayingInputHintBlocked(scriptInterface, actionName)) {
      return ;
    };
    data.action = actionName;
    data.source = source;
    data.localizedLabel = label;
    data.holdIndicationType = holdIndicationType;
    data.enableHoldAnimation = enableHoldAnimation;
    evt = new UpdateInputHintEvent();
    evt.data = data;
    evt.show = true;
    evt.targetHintContainer = "GameplayInputHelper";
    GetUISystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).QueueEvent(evt);
  }

  protected final const void RemoveInputHint(ref<StateGameScriptInterface> scriptInterface, CName actionName, CName source) {
    ref<UpdateInputHintEvent> evt;
    InputHintData data;
    data.action = actionName;
    data.source = source;
    evt = new UpdateInputHintEvent();
    evt.data = data;
    evt.show = false;
    evt.targetHintContainer = "GameplayInputHelper";
    GetUISystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).QueueEvent(evt);
  }

  protected final const void RemoveInputHintsBySource(ref<StateGameScriptInterface> scriptInterface, CName source) {
    ref<DeleteInputHintBySourceEvent> evt;
    evt = new DeleteInputHintBySourceEvent();
    evt.source = source;
    evt.targetHintContainer = "GameplayInputHelper";
    GetUISystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).QueueEvent(evt);
  }

  protected final const Bool IsDisplayingInputHintBlocked(ref<StateGameScriptInterface> scriptInterface, CName actionName) {
    switch(actionName) {
      case "RangedAttack":
        return HasStatusEffectWithTag(scriptInterface.executionOwner, "NoCombat") || !HasAnyValidWeaponAvailable(scriptInterface);
      case "Jump":
        return HasStatusEffect(scriptInterface.executionOwner, "GameplayRestriction.NoJump");
      case "Exit":
        return HasRestriction(scriptInterface.executionOwner, "VehicleScene");
      case "ToggleVehCamera":
        return IsVehicleCameraChangeBlocked(scriptInterface);
      case "WeaponWheel":
        return IsNoCombatActionsForced(scriptInterface) || IsVehicleExitCombatModeBlocked(scriptInterface);
      case "Dodge":
        return IsInTier2Locomotion(scriptInterface) || HasRestriction(scriptInterface.executionOwner, "PhoneCall");
      case "SwitchItem":
        return HasRestriction(scriptInterface.executionOwner, "NoCombat") || !HasItemInArea(WeakRefToRef(scriptInterface.executionOwner), gamedataEquipmentArea.Weapon);
      case "DropCarriedObject":
        return HasRestriction(scriptInterface.executionOwner, "BodyCarryingNoDrop");
      case "QuickMelee":
        return HasRestriction(scriptInterface.executionOwner, "NoQuickMelee");
      default:
        return false;
    };
  }

  protected final const Bool GetCancelChargeButtonInput(ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.IsActionJustPressed("CancelChargingCG");
  }

  protected final const void ProcessCombatGadgetActionInputCaching(ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    if(scriptInterface.IsActionJustHeld("UseCombatGadget") && !GetParameterBool("cgCached", stateContext, true)) {
      stateContext.SetPermanentBoolParameter("cgCached", true, true);
    } else {
      if(GetParameterBool("cgCached", stateContext, true) && scriptInterface.GetActionValue("UseCombatGadget") == 0) {
        stateContext.RemovePermanentBoolParameter("cgCached");
      };
    };
  }

  protected final const void TriggerNoiseStim(wref<GameObject> owner, ETakedownActionType takedownActionType) {
    ref<StimBroadcasterComponent> broadcaster;
    broadcaster = WeakRefToRef(owner).GetStimBroadcasterComponent();
    if(ToBool(broadcaster)) {
      broadcaster.TriggerNoiseStim(owner, takedownActionType);
    };
  }

  protected final void ActivateDamageProjection(Bool newState, ref<WeaponObject> weapon, ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    GameInstance game;
    ref<DamageSystem> damageSystem;
    ref<WeaponItem_Record> weaponRecord;
    ref<IBlackboard> blackboard;
    weaponRecord = GetWeaponItemRecord(GetTDBID(weapon.GetItemID()));
    game = WeakRefToRef(scriptInterface.executionOwner).GetGame();
    damageSystem = GetDamageSystem(game);
    damageSystem.ClearPreviewTargetStruct();
    if(ToBool(weapon)) {
      damageSystem.SetPreviewLock(!newState);
      weapon.GetCurrentAttack().SetDamageProjectionActive(newState);
      stateContext.SetPermanentBoolParameter("DamagePreviewActive", true, true);
    };
    if(!newState) {
      GetBlackboardSystem(game).Get(GetAllBlackboardDefs().UI_NameplateData);
      if(ToBool(blackboard)) {
        blackboard.SetInt(GetAllBlackboardDefs().UI_NameplateData.DamageProjection, 0, true);
      };
      damageSystem.ClearPreviewTargetStruct();
      stateContext.RemovePermanentBoolParameter("DamagePreviewActive");
    };
  }

  protected final Bool IsNameplateVisible(ref<StateGameScriptInterface> scriptInterface) {
    ref<IBlackboard> blackboard;
    blackboard = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).Get(GetAllBlackboardDefs().UI_NameplateData);
    return blackboard.GetBool(GetAllBlackboardDefs().UI_NameplateData.IsVisible);
  }

  protected final void HandleDamagePreview(ref<WeaponObject> weapon, ref<StateGameScriptInterface> scriptInterface, ref<StateContext> stateContext) {
    if(CompareSMState("Combat", "stealth", stateContext) && CanWeaponSilentKill(weapon, scriptInterface) && IsNameplateVisible(scriptInterface) && !GetParameterBool("DamagePreviewActive", stateContext, true)) {
      ActivateDamageProjection(true, weapon, scriptInterface, stateContext);
    } else {
      if(GetParameterBool("DamagePreviewActive", stateContext, true) && !CompareSMState("Combat", "stealth", stateContext) || !IsNameplateVisible(scriptInterface) || !CanWeaponSilentKill(weapon, scriptInterface)) {
        ActivateDamageProjection(false, weapon, scriptInterface, stateContext);
      };
    };
    ClearPredictionForCurrentVisibleTarget(scriptInterface);
  }

  protected final void ClearPredictionForCurrentVisibleTarget(ref<StateGameScriptInterface> scriptInterface) {
    GameInstance game;
    ref<BlackboardSystem> bbSystem;
    EntityID eID;
    game = WeakRefToRef(scriptInterface.executionOwner).GetGame();
    bbSystem = GetBlackboardSystem(game);
    eID = bbSystem.Get(GetAllBlackboardDefs().UI_TargetingInfo).GetEntityID(GetAllBlackboardDefs().UI_TargetingInfo.CurrentVisibleTarget);
    if(!IsDefined(eID)) {
      bbSystem.Get(GetAllBlackboardDefs().UI_NameplateData).SetInt(GetAllBlackboardDefs().UI_NameplateData.DamageProjection, 0, true);
      GetDamageSystem(game).ClearPreviewTargetStruct();
    };
  }

  protected final const Bool CanWeaponSilentKill(ref<WeaponObject> weapon, ref<StateGameScriptInterface> scriptInterface) {
    return GetStatsSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame()).GetStatValue(Cast(weapon.GetEntityID()), gamedataStatType.CanSilentKill) > 0;
  }

  protected final const Bool UsingJohnnyReplacer(ref<StateGameScriptInterface> scriptInterface) {
    return Cast(WeakRefToRef(scriptInterface.executionOwner)).GetRecordID() == "Character.johnny_replacer";
  }

  protected final const Bool IsPlayingAsReplacer(ref<StateGameScriptInterface> scriptInterface) {
    return Cast(WeakRefToRef(scriptInterface.executionOwner)).GetRecordID() != "Character.Player_Puppet_Base";
  }

  protected final const Bool IsFastForwardByLine(ref<StateGameScriptInterface> scriptInterface) {
    return GetIsFastForwardByLine(WeakRefToRef(scriptInterface.executionOwner));
  }

  protected final const Bool CheckIsFastForwardByLine(ref<StateGameScriptInterface> scriptInterface) {
    ref<GameplaySettingsSystem> gss;
    gss = Cast(GetScriptableSystemsContainer(WeakRefToRef(scriptInterface.owner).GetGame()).Get("GameplaySettingsSystem"));
    return gss.GetIsFastForwardByLine();
  }

  protected final const CName GetFFParamsForCrouch(ref<StateGameScriptInterface> scriptInterface) {
    CName param;
    param = CheckIsFastForwardByLine(scriptInterface) ? "FFhintActive" : "FFHoldLock";
    return param;
  }

  protected final static void UpdateAimAssist(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool inSprint;
    Int32 meleeState;
    Bool inMeleeAssistState;
    Bool hasMeleeEquipped;
    Bool inLefthandCW;
    Int32 leftHandCWState;
    ref<PlayerPuppet> playerPuppet;
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(scriptInterface.executionOwner).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(WeakRefToRef(scriptInterface.executionOwner).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerPuppet = Cast(WeakRefToRef(scriptInterface.executionOwner));
    if(!ToBool(playerPuppet)) {
      return ;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == ToInt(gamePSMVision.Focus)) {
      playerPuppet.ApplyAimAssistSettings("Settings_Scanning");
      return ;
    };
    if(WeakRefToRef(scriptInterface.executionOwner).GetTakeOverControlSystem().IsDeviceControlled()) {
      playerPuppet.ApplyAimAssistSettings("Settings_Default");
      return ;
    };
    leftHandCWState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LeftHandCyberware);
    if(leftHandCWState == ToInt(gamePSMLeftHandCyberware.Charge)) {
      playerPuppet.ApplyAimAssistSettings("Settings_LeftHandCyberwareCharge");
      return ;
    };
    inLefthandCW = stateContext.IsStateMachineActive("LeftHandCyberware") && leftHandCWState != ToInt(gamePSMLeftHandCyberware.Unequip) && leftHandCWState != ToInt(gamePSMLeftHandCyberware.StartUnequip);
    if(inLefthandCW) {
      playerPuppet.ApplyAimAssistSettings("Settings_LeftHandCyberware");
      return ;
    };
    hasMeleeEquipped = HasMeleeWeaponEquipped(scriptInterface);
    inSprint = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == ToInt(gamePSMLocomotionStates.Sprint);
    if(!inSprint && hasMeleeEquipped) {
      meleeState = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon);
      inMeleeAssistState = meleeState == ToInt(gamePSMMeleeWeapon.Block) || meleeState == ToInt(gamePSMMeleeWeapon.Deflect) || meleeState == ToInt(gamePSMMeleeWeapon.DeflectAttack);
      playerPuppet.ApplyAimAssistSettings(inMeleeAssistState ? "Settings_MeleeCombat" : "Settings_MeleeCombatIdle");
      return ;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim)) {
      playerPuppet.ApplyAimAssistSettings(HasStatFlag(scriptInterface, gamedataStatType.CanWeaponSnapToLimbs) ? "Settings_AimingLimbCyber" : "Settings_Aiming");
      return ;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == ToInt(gamePSMRangedWeaponStates.QuickMelee)) {
      playerPuppet.ApplyAimAssistSettings("Settings_QuickMelee");
      return ;
    };
    if(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle) == ToInt(gamePSMVehicle.Combat)) {
      playerPuppet.ApplyAimAssistSettings("Settings_VehicleCombat");
      return ;
    };
    if(inSprint) {
      playerPuppet.ApplyAimAssistSettings("Settings_Sprinting");
      return ;
    };
    playerPuppet.ApplyAimAssistSettings(HasStatFlag(scriptInterface, gamedataStatType.CanWeaponSnapToLimbs) ? "Settings_LimbCyber" : "Settings_Default");
  }
}
