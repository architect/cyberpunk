
public abstract class LookAtPresetBaseDecisions extends DefaultTransition {

  public final const Bool HasItemEquipped(ref<StateGameScriptInterface> scriptInterface) {
    String desiredTypeStr;
    gamedataItemType desiredItemType;
    ref<ItemObject> equippedObject;
    gamedataItemType equippedItemType;
    desiredTypeStr = GetStaticStringParameter("itemType", "");
    desiredItemType = ToEnum(Cast(EnumValueFromString("gamedataItemType", desiredTypeStr)));
    if(GetStaticBoolParameter("leftHandItem", false)) {
      equippedObject = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponLeft");
    } else {
      equippedObject = GetTransactionSystem(WeakRefToRef(scriptInterface.owner).GetGame()).GetItemInSlot(GetOwnerGameObject(scriptInterface), "AttachmentSlots.WeaponRight");
    };
    if(equippedObject == null) {
      return false;
    };
    equippedItemType = WeakRefToRef(GetItemRecord(GetTDBID(equippedObject.GetItemID())).ItemType()).Type();
    return desiredItemType == equippedItemType;
  }

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(HasItemEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }

  protected const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(!HasItemEquipped(scriptInterface)) {
      return true;
    };
    return false;
  }
}

public abstract class LookAtPresetBaseEvents extends DefaultTransition {

  public array<ref<LookAtAddEvent>> m_lookAtEvents;

  public Bool m_attachLeft;

  public Bool m_attachRight;

  public final static void GetLookatPartsRequests(wref<LookAtPreset_Record> lookatPresetRecord, out array<LookAtPartRequest> lookAtParts) {
    Int32 i;
    array<wref<LookAtPart_Record>> partRecords;
    LookAtPartRequest lookAtPartRequest;
    WeakRefToRef(lookatPresetRecord).LookAtParts(partRecords);
    i = 0;
    while(i < Size(partRecords)) {
      lookAtPartRequest.partName = WeakRefToRef(partRecords[i]).PartName();
      lookAtPartRequest.weight = WeakRefToRef(partRecords[i]).Weight();
      lookAtPartRequest.suppress = WeakRefToRef(partRecords[i]).Suppress();
      lookAtPartRequest.mode = WeakRefToRef(partRecords[i]).Mode();
      Push(lookAtParts, lookAtPartRequest);
      i += 1;
    };
  }

  public final static void AddLookat(ref<StateGameScriptInterface> scriptInterface, TweakDBID recordID, Int32 priority, out array<ref<LookAtAddEvent>> lookAtEventsArray, out Bool attachLeft, out Bool attachRight) {
    array<LookAtPartRequest> lookAtPartRequests;
    wref<LookAtPreset_Record> lookatPreset;
    ref<LookAtAddEvent> lookAtEvent;
    ref<LookAtFacingPositionProvider> facingPosProvider;
    lookatPreset = RefToWeakRef(GetLookAtPresetRecord(recordID));
    if(!ToBool(lookatPreset)) {
      return ;
    };
    lookAtEvent = new LookAtAddEvent();
    facingPosProvider = new LookAtFacingPositionProvider();
    facingPosProvider.SetCameraComponent(WeakRefToRef(scriptInterface.executionOwner));
    lookAtEvent.targetPositionProvider = facingPosProvider;
    lookAtEvent.bodyPart = WeakRefToRef(lookatPreset).BodyPart();
    lookAtEvent.request.transitionSpeed = WeakRefToRef(lookatPreset).TransitionSpeed();
    lookAtEvent.request.hasOutTransition = WeakRefToRef(lookatPreset).HasOutTransition();
    lookAtEvent.request.outTransitionSpeed = WeakRefToRef(lookatPreset).OutTransitionSpeed();
    lookAtEvent.request.limits.softLimitDegrees = WeakRefToRef(lookatPreset).SoftLimitDegrees();
    lookAtEvent.request.limits.hardLimitDegrees = WeakRefToRef(lookatPreset).HardLimitDegrees();
    lookAtEvent.request.limits.hardLimitDistance = WeakRefToRef(lookatPreset).HardLimitDistance();
    lookAtEvent.request.limits.backLimitDegrees = WeakRefToRef(lookatPreset).BackLimitDegrees();
    lookAtEvent.request.calculatePositionInParentSpace = WeakRefToRef(lookatPreset).CalculatePositionInParentSpace();
    if(!IsFinal()) {
      lookAtEvent.SetDebugInfo("Gameplay " + ToStringDEBUG(WeakRefToRef(lookatPreset).GetID()));
    };
    lookAtEvent.request.suppress = WeakRefToRef(lookatPreset).Suppress();
    lookAtEvent.request.mode = WeakRefToRef(lookatPreset).Mode();
    lookAtEvent.request.priority = priority;
    GetLookatPartsRequests(lookatPreset, lookAtPartRequests);
    lookAtEvent.SetAdditionalPartsArray(lookAtPartRequests);
    WeakRefToRef(scriptInterface.executionOwner).QueueEvent(lookAtEvent);
    Push(lookAtEventsArray, lookAtEvent);
    attachLeft = WeakRefToRef(lookatPreset).AttachLeftHandtoRightHand();
    attachRight = WeakRefToRef(lookatPreset).AttachRightHandtoLeftHand();
  }

  public final void SetHandAttachAnimVars(ref<StateGameScriptInterface> scriptInterface) {
    SetInputFloatToReplicate(WeakRefToRef(scriptInterface.executionOwner), "pla_left_hand_attach", this.m_attachLeft ? 1 : 0);
    SetInputFloatToReplicate(WeakRefToRef(scriptInterface.executionOwner), "pla_right_hand_attach", this.m_attachRight ? 1 : 0);
  }

  public final void AddAllLookAtsInList(ref<StateGameScriptInterface> scriptInterface, array<String> presetNames, Int32 priority, out array<ref<LookAtAddEvent>> lookAtEventsArray) {
    Bool attachLeftReturn;
    Bool attachRightReturn;
    Int32 i;
    this.m_attachLeft = false;
    this.m_attachRight = false;
    i = 0;
    while(i < Size(presetNames)) {
      AddLookat(scriptInterface, Create("LookatPreset." + presetNames[i]), priority, lookAtEventsArray, attachLeftReturn, attachRightReturn);
      this.m_attachLeft = this.m_attachLeft || attachLeftReturn;
      this.m_attachRight = this.m_attachRight || attachRightReturn;
      i += 1;
    };
    SetHandAttachAnimVars(scriptInterface);
  }

  public final static void RemoveAddedLookAts(ref<StateGameScriptInterface> scriptInterface, out array<ref<LookAtAddEvent>> lookAtEventsArray) {
    ref<LookAtAddEvent> lookAtEvent;
    Int32 i;
    i = 0;
    while(i < Size(lookAtEventsArray)) {
      lookAtEvent = lookAtEventsArray[i];
      if(!ToBool(lookAtEvent)) {
      } else {
        QueueRemoveLookatEvent(WeakRefToRef(scriptInterface.executionOwner), lookAtEvent);
      };
      i += 1;
    };
    Clear(lookAtEventsArray);
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    AddAllLookAtsInList(scriptInterface, GetStaticStringArrayParameter("lookAtPresetNames"), 1, this.m_lookAtEvents);
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    RemoveAddedLookAts(scriptInterface, this.m_lookAtEvents);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    RemoveAddedLookAts(scriptInterface, this.m_lookAtEvents);
  }
}

public class lookAtPresetGunBaseEvents extends LookAtPresetBaseEvents {

  public array<ref<LookAtAddEvent>> m_overrideLookAtEvents;

  public Int32 m_gunState;

  public Bool m_originalAttachLeft;

  public Bool m_originalAttachRight;

  public final static Bool IsReloading(ref<StateContext> stateContext) {
    return stateContext.GetStateMachineCurrentState("Weapon") == "reload";
  }

  public final static Bool IsInSafeMode(ref<StateContext> stateContext) {
    return stateContext.GetStateMachineCurrentState("Weapon") == "publicSafe";
  }

  public final void SetGunState(ref<StateGameScriptInterface> scriptInterface, Int32 newGunState) {
    if(this.m_gunState != newGunState) {
      RemoveAddedLookAts(scriptInterface, this.m_overrideLookAtEvents);
      this.m_gunState = newGunState;
      if(this.m_gunState == 1) {
        AddAllLookAtsInList(scriptInterface, GetStaticStringArrayParameter("safeLookAtPresetNames"), 0, this.m_overrideLookAtEvents);
      } else {
        if(this.m_gunState == 2) {
          AddAllLookAtsInList(scriptInterface, GetStaticStringArrayParameter("reloadLookAtPresetNames"), 0, this.m_overrideLookAtEvents);
        } else {
          this.m_attachLeft = this.m_originalAttachLeft;
          this.m_attachRight = this.m_originalAttachRight;
          SetHandAttachAnimVars(scriptInterface);
        };
      };
    };
  }

  public void OnEnter(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    OnEnter(stateContext, scriptInterface);
    this.m_originalAttachLeft = this.m_attachLeft;
    this.m_originalAttachRight = this.m_attachRight;
    this.m_gunState = 0;
  }

  protected final void OnUpdate(Float timeDelta, ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(IsReloading(stateContext)) {
      SetGunState(scriptInterface, 2);
    } else {
      if(IsInSafeMode(stateContext)) {
        SetGunState(scriptInterface, 1);
      } else {
        SetGunState(scriptInterface, 0);
      };
    };
  }

  public void OnExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetGunState(scriptInterface, 0);
    OnExit(stateContext, scriptInterface);
  }

  public void OnForcedExit(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    SetGunState(scriptInterface, 0);
    OnExit(stateContext, scriptInterface);
  }
}

public class UnarmedLookAtDecisions extends LookAtPresetBaseDecisions {

  protected const Bool ExitCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    if(ToBool(GetActiveWeapon(scriptInterface)) || ToBool(GetActiveLeftHandItem(scriptInterface))) {
      return true;
    };
    return false;
  }
}
