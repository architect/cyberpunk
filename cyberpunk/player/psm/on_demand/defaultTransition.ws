
public class Ground extends DefaultTransition {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool onGround;
    onGround = scriptInterface.IsOnGround();
    return onGround;
  }
}

public class Air extends DefaultTransition {

  protected const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    Bool onGround;
    onGround = scriptInterface.IsOnGround();
    return !onGround;
  }
}
