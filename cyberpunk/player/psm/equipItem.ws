
public class EquipItemLeftDecisions extends DefaultTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.GetActionValue("ItemLeft") > 0;
  }
}

public class EquipItemRightDecisions extends DefaultTransition {

  protected final const Bool EnterCondition(ref<StateContext> stateContext, ref<StateGameScriptInterface> scriptInterface) {
    return scriptInterface.GetActionValue("ItemRight") > 0;
  }
}
