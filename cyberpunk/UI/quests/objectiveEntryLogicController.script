
public class ObjectiveEntryLogicController extends inkLogicController {

  [Default(ObjectiveEntryLogicController, 0.8f))]
  public edit Float m_blinkInterval;

  [Default(ObjectiveEntryLogicController, 5.0f))]
  public edit Float m_blinkTotalTime;

  [Default(ObjectiveEntryLogicController, tracked_left))]
  public edit CName m_texturePart_Tracked;

  [Default(ObjectiveEntryLogicController, untracked_left))]
  public edit CName m_texturePart_Untracked;

  [Default(ObjectiveEntryLogicController, succeeded))]
  public edit CName m_texturePart_Succeeded;

  [Default(ObjectiveEntryLogicController, failed))]
  public edit CName m_texturePart_Failed;

  [Default(ObjectiveEntryLogicController, false))]
  public edit Bool m_isLargeUpdateWidget;

  private wref<inkText> m_entryName;

  private wref<inkText> m_entryOptional;

  private wref<inkImage> m_stateIcon;

  private wref<inkImage> m_trackedIcon;

  private wref<inkWidget> m_blinkWidget;

  private wref<inkWidget> m_root;

  private ref<inkAnimDef> m_animBlinkDef;

  private ref<inkAnimProxy> m_animBlink;

  private ref<inkAnimDef> m_animFadeDef;

  private ref<inkAnimProxy> m_animFade;

  private Int32 m_entryId;

  private UIObjectiveEntryType m_type;

  private gameJournalEntryState m_state;

  private wref<ObjectiveEntryLogicController> m_parentEntry;

  [Default(ObjectiveEntryLogicController, 0))]
  private Int32 m_childCount;

  private Bool m_updated;

  private Bool m_isTracked;

  public Bool m_isOptional;

  protected cb Bool OnInitialize() {
    this.m_root = GetRootWidget();
    this.m_blinkWidget = RefToWeakRef(WeakRefToRef(GetWidget("temp_blinker")));
    this.m_stateIcon = RefToWeakRef(Cast(WeakRefToRef(GetWidget("temp_blinker/stateIcon"))));
    this.m_entryName = RefToWeakRef(Cast(WeakRefToRef(GetWidget("entryFlex/entryName"))));
    this.m_entryOptional = RefToWeakRef(Cast(WeakRefToRef(GetWidget("entryOptional"))));
    this.m_trackedIcon = RefToWeakRef(Cast(WeakRefToRef(GetWidget("tracked"))));
    CreateAnimations();
    WeakRefToRef(this.m_blinkWidget).SetVisible(false);
    WeakRefToRef(this.m_root).SetVisible(false);
  }

  public final void SetUpdated(Bool updated) {
    this.m_updated = updated;
  }

  public final Bool IsUpdated() {
    return this.m_updated;
  }

  public final Bool IsTracked() {
    return this.m_isTracked;
  }

  public final UIObjectiveEntryType GetEntryType() {
    return this.m_type;
  }

  public final gameJournalEntryState GetEntryState() {
    return this.m_state;
  }

  public final Int32 GetEntryId() {
    return this.m_entryId;
  }

  public final void SetEntryId(Int32 id) {
    this.m_entryId = id;
  }

  public final void SetEntryData(UIObjectiveEntryData data) {
    CName stateIconTexturePart;
    CName stateName;
    WeakRefToRef(this.m_root).SetVisible(false);
    SetUpdated(true);
    StopFadeAnimation();
    this.m_state = data.m_state;
    this.m_type = data.m_type;
    this.m_isTracked = data.m_isTracked;
    this.m_isOptional = data.m_isOptional;
    if(this.m_state == gameJournalEntryState.Inactive || data.m_name == "") {
      NotifyForRemoval();
      return ;
    };
    LogUI("ObjectiveEntryLogicController:SetEntryData - Name: " + data.m_name);
    WeakRefToRef(this.m_entryName).SetLetterCase(textLetterCase.UpperCase);
    if(this.m_isOptional) {
      WeakRefToRef(this.m_entryName).SetText(this.m_type == UIObjectiveEntryType.Quest ? data.m_name : GetLocalizedText(data.m_name) + data.m_counter + " [" + GetLocalizedText("UI-ScriptExports-Optional0") + "]");
    } else {
      WeakRefToRef(this.m_entryName).SetText(this.m_type == UIObjectiveEntryType.Quest ? data.m_name : GetLocalizedText(data.m_name) + data.m_counter);
    };
    stateIconTexturePart = GetStateIconTexturePart(this.m_state, data.m_isTracked);
    if(stateIconTexturePart == "") {
      WeakRefToRef(this.m_stateIcon).SetVisible(false);
    } else {
      WeakRefToRef(this.m_stateIcon).SetTexturePart(stateIconTexturePart);
    };
    stateName = GetJournalStateName(this.m_state, this.m_isTracked);
    WeakRefToRef(this.m_trackedIcon).SetVisible(this.m_isTracked);
    WeakRefToRef(this.m_entryName).SetState(stateName);
    WeakRefToRef(this.m_entryOptional).SetState(stateName);
    WeakRefToRef(GetRootWidget()).SetState(stateName);
    if(this.m_state == gameJournalEntryState.Succeeded || this.m_state == gameJournalEntryState.Failed) {
      if(this.m_type != UIObjectiveEntryType.Quest) {
        if(this.m_state == gameJournalEntryState.Succeeded) {
          this.m_animBlink = PlayLibraryAnimation("ObjectiveEntryComplete");
        };
        if(this.m_state == gameJournalEntryState.Failed) {
          this.m_animBlink = PlayLibraryAnimation("ObjectiveEntryFailed");
        };
        this.m_animBlink.RegisterToCallback(inkanimEventType.OnFinish, this, "OnAnimationComplete");
      };
    } else {
      if(this.m_state == gameJournalEntryState.Active) {
        Show();
      };
    };
  }

  private final CName GetStateIconTexturePart(gameJournalEntryState state, Bool isTracked) {
    switch(state) {
      case gameJournalEntryState.Active:
        return isTracked ? this.m_texturePart_Tracked : this.m_texturePart_Untracked;
      case gameJournalEntryState.Succeeded:
        return this.m_texturePart_Succeeded;
      case gameJournalEntryState.Failed:
        return this.m_texturePart_Failed;
    };
    return "";
  }

  private final void CreateAnimations() {
    ref<inkAnimTransparency> fadeOutInterp;
    ref<inkAnimTransparency> fadeInInterp;
    ref<inkAnimTransparency> fadeInterp;
    this.m_animBlinkDef = new inkAnimDef();
    fadeOutInterp = new inkAnimTransparency();
    fadeOutInterp.SetStartTransparency(0.20000000298023224);
    fadeOutInterp.SetEndTransparency(3);
    fadeOutInterp.SetDuration(this.m_blinkInterval / 2);
    fadeOutInterp.SetType(inkanimInterpolationType.Linear);
    this.m_animBlinkDef.AddInterpolator(fadeOutInterp);
    fadeInInterp = new inkAnimTransparency();
    fadeInInterp.SetStartTransparency(0.20000000298023224);
    fadeInInterp.SetEndTransparency(3);
    fadeInInterp.SetDuration(this.m_blinkInterval / 2);
    fadeInInterp.SetType(inkanimInterpolationType.Linear);
    this.m_animBlinkDef.AddInterpolator(fadeInInterp);
    this.m_animFadeDef = new inkAnimDef();
    fadeInterp = new inkAnimTransparency();
    fadeInterp.SetStartTransparency(1);
    fadeInterp.SetEndTransparency(1);
    fadeInterp.SetDuration(0);
    fadeInterp.SetDirection(inkanimInterpolationDirection.To);
    fadeInterp.SetType(inkanimInterpolationType.Linear);
    this.m_animFadeDef.AddInterpolator(fadeInterp);
  }

  public final Bool IsReadyToRemove() {
    return !this.m_state == gameJournalEntryState.Active;
  }

  protected cb Bool OnAnimationComplete(ref<inkAnimProxy> anim) {
    WeakRefToRef(this.m_root).SetVisible(false);
    NotifyForRemoval();
  }

  private final void NotifyForRemoval() {
    CallCustomCallback("OnReadyToRemove");
  }

  public final void Hide() {
    if(this.m_type == UIObjectiveEntryType.Quest) {
      WeakRefToRef(this.m_root).SetVisible(false);
      NotifyForRemoval();
      return ;
    };
    if(this.m_state == gameJournalEntryState.Succeeded || this.m_state == gameJournalEntryState.Failed) {
      return ;
    };
    if(this.m_animBlink.IsPlaying()) {
      this.m_animBlink.Stop();
    };
    this.m_animBlink = PlayLibraryAnimation("ObjectiveEntryFadeOut");
    this.m_animBlink.RegisterToCallback(inkanimEventType.OnFinish, this, "OnAnimationComplete");
  }

  private final void StopFadeAnimation() {
    if(ToBool(this.m_animBlink) && this.m_animBlink.IsPlaying()) {
      this.m_animBlink.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnAnimationComplete");
      this.m_animBlink.Stop();
    };
    if(ToBool(this.m_animFade) && this.m_animFade.IsPlaying()) {
      this.m_animFade.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnAnimationComplete");
      this.m_animFade.Stop();
    };
    WeakRefToRef(this.m_root).SetOpacity(1);
  }

  public final void Show() {
    WeakRefToRef(this.m_root).SetVisible(true);
  }

  public final void AttachToParent(wref<ObjectiveEntryLogicController> parentEntry) {
    this.m_parentEntry = parentEntry;
    WeakRefToRef(this.m_parentEntry).IncrementChildCount();
  }

  public final void DetachFromParent() {
    WeakRefToRef(this.m_parentEntry).DecrementChildCount();
    this.m_parentEntry = null;
  }

  public final void IncrementChildCount() {
    this.m_childCount += 1;
  }

  public final void DecrementChildCount() {
    this.m_childCount -= 1;
  }
}
