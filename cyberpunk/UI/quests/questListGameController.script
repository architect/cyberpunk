
public struct QuestUIUtils {

  public final static CName GetJournalStateName(gameJournalEntryState state, Bool isTracked) {
    switch(state) {
      case gameJournalEntryState.Active:
        if(isTracked) {
          return "tracked";
        };
        return "untracked";
      case gameJournalEntryState.Succeeded:
        return "succeeded";
      case gameJournalEntryState.Failed:
        return "failed";
    };
    return "Default";
  }

  public final static UIObjectiveEntryType GetEntryTypeFromName(CName entryTypeName) {
    switch(entryTypeName) {
      case "gameJournalQuest":
        return UIObjectiveEntryType.Quest;
      case "gameJournalQuestObjective":
        return UIObjectiveEntryType.Objective;
      case "gameJournalQuestSubObjective":
        return UIObjectiveEntryType.SubObjective;
      default:
        return UIObjectiveEntryType.Invalid;
    };
  }

  public final static CName GetLibraryIDForEntryType(UIObjectiveEntryType entryType) {
    switch(entryType) {
      case UIObjectiveEntryType.Quest:
        return "QuestEntry";
      case UIObjectiveEntryType.Objective:
        return "ObjectiveEntry";
      case UIObjectiveEntryType.SubObjective:
        return "SubObjectiveEntry";
      default:
        return "";
    };
  }
}

public class QuestListGameController extends inkHUDGameController {

  private edit inkVerticalPanelRef m_entryList;

  private edit inkCompoundRef m_scanPulse;

  private edit inkWidgetRef m_optionalHeader;

  private edit inkWidgetRef m_toDoHeader;

  private edit inkVerticalPanelRef m_optionalList;

  private edit inkVerticalPanelRef m_nonOptionalList;

  private ref<inkArray> m_entryControllers;

  private ref<inkAnimProxy> m_scanPulseAnimProxy;

  private Uint32 m_stateChangesBlackboardId;

  private Uint32 m_trackedChangesBlackboardId;

  private ref<JournalWrapper> m_JournalWrapper;

  private wref<GameObject> m_player;

  private ref<QuestListHeaderLogicController> m_optionalHeaderController;

  private ref<QuestListHeaderLogicController> m_toDoHeaderController;

  private ref<QuestObjectiveWrapper> m_lastNonOptionalObjective;

  protected cb Bool OnInitialize() {
    wref<GameObject> ownerEntity;
    GameInstance gameInstance;
    ownerEntity = RefToWeakRef(Cast(GetOwnerEntity()));
    gameInstance = WeakRefToRef(ownerEntity).GetGame();
    this.m_JournalWrapper = new JournalWrapper();
    this.m_JournalWrapper.Init(gameInstance);
    WeakRefToRef(this.m_JournalWrapper.GetJournalManager()).RegisterScriptCallback(this, "OnStateChanges", gameJournalListenerType.State);
    WeakRefToRef(this.m_JournalWrapper.GetJournalManager()).RegisterScriptCallback(this, "OnTrackedEntryChanges", gameJournalListenerType.Tracked);
    WeakRefToRef(this.m_JournalWrapper.GetJournalManager()).RegisterScriptCallback(this, "OnCounterChanged", gameJournalListenerType.Counter);
    this.m_entryControllers = new inkArray();
    this.m_player = RefToWeakRef(Cast(GetOwnerEntity()));
    WeakRefToRef(this.m_player).RegisterInputListener(this, "VisionPush");
    WeakRefToRef(this.m_player).RegisterInputListener(this, "UI_DPadDown");
    this.m_optionalHeaderController = Cast(WeakRefToRef(GetController(this.m_optionalHeader)));
    this.m_optionalHeaderController.SetLabel("UI-Cyberpunk-HUD-QuestList-optional");
    SetVisible(this.m_optionalHeader, false);
    this.m_toDoHeaderController = Cast(WeakRefToRef(GetController(this.m_toDoHeader)));
    this.m_toDoHeaderController.SetLabel("UI-Cyberpunk-HUD-QuestList-toDo");
    SetVisible(this.m_toDoHeader, false);
    UpdateEntries();
  }

  protected cb Bool OnUninitialize() {
    if(ToBool(this.m_JournalWrapper.GetJournalManager())) {
      WeakRefToRef(this.m_JournalWrapper.GetJournalManager()).UnregisterScriptCallback(this, "OnStateChanges");
      WeakRefToRef(this.m_JournalWrapper.GetJournalManager()).UnregisterScriptCallback(this, "OnTrackedEntryChanges");
      WeakRefToRef(this.m_JournalWrapper.GetJournalManager()).UnregisterScriptCallback(this, "OnCounterChanged");
    };
  }

  protected cb Bool OnStateChanges(Uint32 hash, CName className, JournalNotifyOption notifyOption, JournalChangeType changeType) {
    UpdateEntries();
  }

  protected cb Bool OnTrackedEntryChanges(Uint32 hash, CName className, JournalNotifyOption notifyOption, JournalChangeType changeType) {
    UpdateEntries();
  }

  protected cb Bool OnCounterChanged(Uint32 hash, CName className, JournalNotifyOption notifyOption, JournalChangeType changeType) {
    UpdateEntries();
  }

  private final void UpdateEntries() {
    Int32 i;
    Int32 limit;
    Int32 oldEntriesNum;
    array<wref<JournalEntry>> listQuests;
    array<wref<IScriptable>> entryControllers;
    ref<ObjectiveEntryLogicController> entryController;
    Bool hasOptional;
    Bool showOptional;
    ref<QuestsSystem> questSystem;
    entryControllers = this.m_entryControllers.Get();
    i = 0;
    oldEntriesNum = Size(entryControllers);
    while(i < oldEntriesNum) {
      entryController = Cast(WeakRefToRef(entryControllers[i]));
      entryController.SetUpdated(false);
      i += 1;
    };
    this.m_JournalWrapper.GetQuests(listQuests);
    i = 0;
    limit = Size(listQuests);
    while(i < limit) {
      hasOptional = UpdateQuest(this.m_JournalWrapper.BuildQuestData(RefToWeakRef(Cast(WeakRefToRef(listQuests[i])))));
      i += 1;
    };
    entryControllers = this.m_entryControllers.Get();
    i = 0;
    limit = Size(entryControllers);
    while(i < limit) {
      entryController = Cast(WeakRefToRef(entryControllers[i]));
      if(!entryController.IsUpdated()) {
        entryController.Hide();
      };
      i += 1;
    };
  }

  private final Bool UpdateQuest(ref<QuestDataWrapper> questData) {
    wref<ObjectiveEntryLogicController> entryController;
    Bool trackedQuest;
    Bool isAToDoQuest;
    Int32 entryIndex;
    Bool isOptional;
    Bool hasOptionalObjectives;
    trackedQuest = questData.IsTracked() || questData.IsTrackedInHierarchy();
    if(trackedQuest) {
      entryController = GetOrCreateEntry(questData.GetUniqueId(), UIObjectiveEntryType.Quest, null);
      if(ToBool(entryController)) {
        WeakRefToRef(entryController).SetEntryData(BuildEntryData(questData));
        isOptional = UpdateObjectives(questData, entryController, trackedQuest);
        if(!hasOptionalObjectives && isOptional) {
          hasOptionalObjectives = true;
        };
      };
    } else {
      entryController = FindEntry(questData.GetUniqueId());
      if(ToBool(entryController)) {
        WeakRefToRef(entryController).SetEntryData(BuildEntryData(questData));
        isOptional = UpdateObjectives(questData, entryController, trackedQuest);
        if(!hasOptionalObjectives && isOptional) {
          hasOptionalObjectives = true;
        };
      };
      if(WeakRefToRef(entryController).GetEntryState() != gameJournalEntryState.Succeeded && WeakRefToRef(entryController).GetEntryState() != gameJournalEntryState.Failed) {
        WeakRefToRef(entryController).Hide();
      };
    };
    return hasOptionalObjectives;
  }

  private final Bool UpdateObjectives(ref<QuestDataWrapper> questData, wref<ObjectiveEntryLogicController> parent, Bool isParentTracked) {
    array<ref<QuestObjectiveWrapper>> questObjectives;
    Int32 i;
    Int32 limit;
    Bool hasOptionalObjectives;
    Int32 entryIndex;
    wref<inkWidget> entryWidget;
    Bool isOptional;
    UIObjectiveEntryData data;
    hasOptionalObjectives = false;
    isOptional = false;
    questObjectives = questData.GetObjectives();
    i = 0;
    limit = Size(questObjectives);
    while(i < limit) {
      UpdateObjective(questObjectives[i], parent, isParentTracked);
      data = BuildEntryData(questObjectives[i]);
      isOptional = questObjectives[i].IsOptional();
      if(!hasOptionalObjectives && isOptional) {
        hasOptionalObjectives = true;
      };
      i += 1;
    };
    return hasOptionalObjectives;
  }

  private final void UpdateObjective(ref<QuestObjectiveWrapper> objectiveData, wref<ObjectiveEntryLogicController> parent, Bool isParentTracked) {
    wref<ObjectiveEntryLogicController> entryController;
    gameJournalEntryState entryState;
    if(isParentTracked && objectiveData.GetStatus() == gameJournalEntryState.Active) {
      entryController = GetOrCreateEntry(objectiveData.GetUniqueId(), UIObjectiveEntryType.Objective, parent, objectiveData.IsOptional());
    } else {
      entryController = FindEntry(objectiveData.GetUniqueId());
    };
    if(ToBool(entryController)) {
      WeakRefToRef(entryController).SetEntryData(BuildEntryData(objectiveData));
      UpdateSubObjectives(objectiveData, entryController, objectiveData.IsTracked() || objectiveData.IsTrackedInHierarchy());
      entryState = WeakRefToRef(entryController).GetEntryState();
      if(!isParentTracked && entryState != gameJournalEntryState.Succeeded && entryState != gameJournalEntryState.Failed) {
        WeakRefToRef(entryController).Hide();
      };
    };
  }

  private final void UpdateSubObjectives(ref<QuestObjectiveWrapper> questData, wref<ObjectiveEntryLogicController> parent, Bool isParentTracked) {
    array<ref<QuestSubObjectiveWrapper>> questSubObjectives;
    Int32 i;
    Int32 limit;
    questSubObjectives = questData.GetSubObjectives();
    i = 0;
    limit = Size(questSubObjectives);
    while(i < limit) {
      UpdateSubObjective(questSubObjectives[i], parent, isParentTracked);
      i += 1;
    };
  }

  private final void UpdateSubObjective(ref<QuestSubObjectiveWrapper> subObjectiveData, wref<ObjectiveEntryLogicController> parent, Bool isParentTracked) {
    wref<ObjectiveEntryLogicController> entryController;
    if(isParentTracked && subObjectiveData.GetStatus() == gameJournalEntryState.Active) {
      entryController = GetOrCreateEntry(subObjectiveData.GetUniqueId(), UIObjectiveEntryType.SubObjective, parent);
    } else {
      entryController = FindEntry(subObjectiveData.GetUniqueId());
    };
    if(ToBool(entryController)) {
      WeakRefToRef(entryController).SetEntryData(BuildEntryData(subObjectiveData));
      if(!isParentTracked && WeakRefToRef(entryController).GetEntryState() != gameJournalEntryState.Succeeded && WeakRefToRef(entryController).GetEntryState() != gameJournalEntryState.Failed) {
        WeakRefToRef(entryController).Hide();
      };
    };
  }

  private final wref<ObjectiveEntryLogicController> FindEntry(Int32 entryId) {
    array<wref<IScriptable>> entryControllers;
    Int32 totalControllers;
    Int32 i;
    wref<ObjectiveEntryLogicController> entryController;
    entryControllers = this.m_entryControllers.Get();
    totalControllers = Size(entryControllers);
    i = 0;
    while(i < totalControllers) {
      entryController = RefToWeakRef(Cast(WeakRefToRef(entryControllers[i])));
      if(WeakRefToRef(entryController).GetEntryId() == entryId) {
        return entryController;
      };
      i += 1;
    };
    return null;
  }

  private final wref<ObjectiveEntryLogicController> GetOrCreateEntry(Int32 id, UIObjectiveEntryType entryType, wref<ObjectiveEntryLogicController> parent, Bool isOptional?) {
    wref<inkWidget> entryWidget;
    wref<ObjectiveEntryLogicController> entryController;
    CName libraryID;
    Int32 entryIndex;
    inkVerticalPanelRef list;
    libraryID = GetLibraryIDForEntryType(entryType);
    entryController = FindEntry(id);
    if(WeakRefToRef(entryController) == null) {
      if(isOptional) {
        list = this.m_optionalList;
      } else {
        list = this.m_nonOptionalList;
      };
      entryWidget = SpawnFromLocal(Get(list), libraryID);
      WeakRefToRef(entryWidget).SetHAlign(inkEHorizontalAlign.Right);
      entryController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(entryWidget).GetController())));
      WeakRefToRef(entryController).SetEntryId(id);
      WeakRefToRef(entryController).RegisterToCallback("OnReadyToRemove", this, "OnRemoveEntry");
      if(entryType != UIObjectiveEntryType.SubObjective || WeakRefToRef(parent) == null) {
        this.m_entryControllers.PushBack(entryController);
      } else {
        entryIndex = FindNewEntryIndex(entryType, WeakRefToRef(parent));
        if(entryIndex > -1) {
          ReorderChild(list, entryWidget, entryIndex);
          this.m_entryControllers.InsertAt(Cast(entryIndex), entryController);
        } else {
          this.m_entryControllers.PushBack(entryController);
        };
        WeakRefToRef(entryController).AttachToParent(parent);
      };
    };
    return entryController;
  }

  private final Int32 FindNewEntryIndex(UIObjectiveEntryType entryType, ref<ObjectiveEntryLogicController> parent) {
    array<wref<IScriptable>> entryControllers;
    Int32 totalControllers;
    Int32 i;
    wref<ObjectiveEntryLogicController> currEntryController;
    Bool foundParent;
    foundParent = false;
    entryControllers = this.m_entryControllers.Get();
    totalControllers = Size(entryControllers);
    i = 0;
    while(i < totalControllers) {
      currEntryController = RefToWeakRef(Cast(WeakRefToRef(entryControllers[i])));
      if(!foundParent) {
        foundParent = WeakRefToRef(currEntryController).GetEntryId() == parent.GetEntryId();
      } else {
        if(WeakRefToRef(currEntryController).GetEntryType() != entryType) {
          return i;
        };
      };
      i += 1;
    };
    return -1;
  }

  protected cb Bool OnRemoveEntry(wref<inkWidget> entryWidget) {
    wref<ObjectiveEntryLogicController> entryController;
    entryController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(entryWidget).GetController())));
    if(WeakRefToRef(entryController).IsReadyToRemove()) {
      WeakRefToRef(entryController).DetachFromParent();
      RemoveEntry(entryWidget);
      this.m_entryControllers.Remove(entryController);
    };
  }

  private final void RemoveEntry(wref<inkWidget> entryWidget) {
    Int32 i;
    wref<ObjectiveEntryLogicController> entryController;
    wref<inkWidget> tempInkWidget;
    wref<ObjectiveEntryLogicController> tempController;
    entryController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(entryWidget).GetController())));
    i = 0;
    while(i < GetNumChildren(this.m_optionalList)) {
      tempInkWidget = GetWidgetByIndex(this.m_optionalList, i);
      tempController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(tempInkWidget).GetController())));
      if(WeakRefToRef(tempController) == WeakRefToRef(entryController)) {
        RemoveChild(this.m_optionalList, entryWidget);
      } else {
        i += 1;
      };
    };
    i = 0;
    while(i < GetNumChildren(this.m_nonOptionalList)) {
      tempInkWidget = GetWidgetByIndex(this.m_nonOptionalList, i);
      tempController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(tempInkWidget).GetController())));
      if(WeakRefToRef(tempController) == WeakRefToRef(entryController)) {
        RemoveChild(this.m_nonOptionalList, entryWidget);
      } else {
        i += 1;
      };
    };
  }

  private final UIObjectiveEntryData BuildEntryData(ref<ABaseWrapper> inData) {
    UIObjectiveEntryData outData;
    ref<QuestDataWrapper> questData;
    ref<QuestObjectiveWrapper> objectiveData;
    ref<QuestSubObjectiveWrapper> subObjectiveData;
    questData = Cast(inData);
    if(ToBool(questData)) {
      outData.m_name = questData.GetTitle();
      outData.m_counter = "";
      outData.m_isTracked = questData.IsTracked();
      outData.m_type = UIObjectiveEntryType.Quest;
      outData.m_state = questData.GetStatus();
      outData.m_isOptional = questData.IsOptional();
    } else {
      objectiveData = Cast(inData);
      if(ToBool(objectiveData)) {
        outData.m_name = objectiveData.GetDescription();
        outData.m_counter = objectiveData.GetCounterText();
        outData.m_isTracked = objectiveData.IsTracked();
        outData.m_type = UIObjectiveEntryType.Objective;
        outData.m_state = objectiveData.GetStatus();
        outData.m_isOptional = questData.IsOptional() || objectiveData.IsOptional();
      } else {
        subObjectiveData = Cast(inData);
        if(ToBool(subObjectiveData)) {
          outData.m_name = subObjectiveData.GetDescription();
          outData.m_isTracked = subObjectiveData.IsTracked();
          outData.m_isTracked = subObjectiveData.IsTracked();
          outData.m_type = UIObjectiveEntryType.SubObjective;
          outData.m_state = subObjectiveData.GetStatus();
          outData.m_isOptional = questData.IsOptional();
        };
      };
    };
    return outData;
  }

  private final Bool ShouldDisplayEntry(UIObjectiveEntryType entryType) {
    return entryType != UIObjectiveEntryType.Invalid;
  }

  protected cb Bool OnAction(ListenerAction action, ListenerActionConsumer consumer) {
    CName actionName;
    gameinputActionType actionType;
    actionName = GetName(action);
    actionType = GetType(action);
    if(actionName == "VisionPush" && actionType == gameinputActionType.BUTTON_PRESSED) {
      if(this.m_scanPulseAnimProxy.IsPlaying()) {
        this.m_scanPulseAnimProxy.Stop();
      };
      this.m_scanPulseAnimProxy = PlayLibraryAnimation("ScanPulseAnimation");
    };
  }
}

public class QuestListHeaderLogicController extends inkLogicController {

  private edit inkTextRef m_label;

  public final void SetLabel(String text) {
    SetText(this.m_label, text);
  }
}
