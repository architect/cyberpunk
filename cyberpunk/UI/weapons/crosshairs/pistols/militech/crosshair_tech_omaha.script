
public class Crosshair_Tech_Omaha extends gameuiCrosshairBaseGameController {

  private wref<inkWidget> m_leftPart;

  private wref<inkWidget> m_rightPart;

  private wref<inkWidget> m_topPart;

  private wref<inkRectangle> m_chargeBar;

  private Vector2 m_sizeOfChargeBar;

  private Uint32 m_chargeBBID;

  protected cb Bool OnInitialize() {
    this.m_rootWidget = GetRootWidget();
    this.m_leftPart = RefToWeakRef(WeakRefToRef(GetWidget("left")));
    this.m_rightPart = RefToWeakRef(WeakRefToRef(GetWidget("right")));
    this.m_topPart = RefToWeakRef(WeakRefToRef(GetWidget("top")));
    this.m_chargeBar = RefToWeakRef(Cast(WeakRefToRef(GetWidget("chargeBar/chargeBarFG"))));
    this.m_sizeOfChargeBar = WeakRefToRef(this.m_chargeBar).GetSize();
    WeakRefToRef(this.m_rootWidget).SetOpacity(0);
    OnInitialize();
  }

  protected cb Bool OnUninitialize() {
    OnUninitialize();
  }

  protected cb Bool OnPreIntro() {
    OnPreIntro();
    if(ToBool(this.m_targetBB)) {
      this.m_chargeBBID = WeakRefToRef(this.m_targetBB).RegisterListenerFloat(GetAllBlackboardDefs().Weapon.Charge, this, "OnChargeChanged");
    };
  }

  protected cb Bool OnPreOutro() {
    if(ToBool(this.m_targetBB)) {
      WeakRefToRef(this.m_targetBB).UnregisterListenerFloat(GetAllBlackboardDefs().Weapon.Charge, this.m_chargeBBID);
    };
    OnPreOutro();
  }

  public ref<inkAnimDef> GetIntroAnimation(Bool firstEquip) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(0);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    return anim;
  }

  public ref<inkAnimDef> GetOutroAnimation() {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    return anim;
  }

  protected cb Bool OnBulletSpreadChanged(Vector2 spread) {
    WeakRefToRef(this.m_leftPart).SetMargin(new inkMargin(-spread.X,0,0,0));
    WeakRefToRef(this.m_rightPart).SetMargin(new inkMargin(spread.X,0,0,0));
    WeakRefToRef(this.m_topPart).SetMargin(new inkMargin(0,-spread.Y,0,0));
  }

  protected final void OnChargeChanged(Float charge) {
    WeakRefToRef(this.m_chargeBar).SetSize(new Vector2(MinF(this.m_sizeOfChargeBar.X, charge * 100),this.m_sizeOfChargeBar.Y));
  }

  protected void ApplyCrosshairGUIState(CName state, ref<Entity> aimedAtEntity) {
    WeakRefToRef(this.m_leftPart).SetState(state);
    WeakRefToRef(this.m_rightPart).SetState(state);
    WeakRefToRef(this.m_topPart).SetState(state);
  }
}
