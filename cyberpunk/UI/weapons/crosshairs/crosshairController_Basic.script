
public class CrosshairGameController_Basic extends gameuiCrosshairBaseGameController {

  private edit inkImageRef m_leftPart;

  private edit inkImageRef m_rightPart;

  private edit inkImageRef m_upPart;

  private edit inkImageRef m_downPart;

  private edit inkImageRef m_centerPart;

  private Vector2 m_bufferedSpread;

  private gamedataTriggerMode m_currentFireMode;

  private ref<IBlackboard> m_weaponlocalBB;

  private Uint32 m_bbcurrentFireMode;

  private Uint32 m_ricochetModeActive;

  private Uint32 m_RicochetChance;

  private ref<IBlackboard> m_uiBlackboard;

  [Default(CrosshairGameController_Basic, 20))]
  public edit Float m_horizontalMinSpread;

  [Default(CrosshairGameController_Basic, 20))]
  public edit Float m_verticalMinSpread;

  [Default(CrosshairGameController_Basic, 1))]
  public edit Float m_gameplaySpreadMultiplier;

  protected cb Bool OnInitialize() {
    OnInitialize();
  }

  protected cb Bool OnUninitialize() {
    OnUninitialize();
  }

  protected cb Bool OnPreIntro() {
    WeakRefToRef(GetRootWidget()).SetOpacity(0);
    this.m_weaponlocalBB = GetWeaponLocalBlackboard();
    this.m_bbcurrentFireMode = this.m_weaponlocalBB.RegisterListenerVariant(GetAllBlackboardDefs().Weapon.TriggerMode, this, "OnTriggerModeChanged");
    this.m_currentFireMode = gamedataTriggerMode.Invalid;
    OnPreIntro();
  }

  protected cb Bool OnPreOutro() {
    if(ToBool(this.m_weaponlocalBB)) {
      this.m_weaponlocalBB.UnregisterListenerVariant(GetAllBlackboardDefs().Weapon.TriggerMode, this.m_bbcurrentFireMode);
    };
    OnPreOutro();
  }

  protected final void OnTriggerModeChanged(Variant value) {
    ref<TriggerMode_Record> record;
    ref<inkAnimDef> anim;
    ref<inkAnimRotation> rotationInterpolator;
    gamedataTriggerMode previousFireMode;
    record = FromVariant(value);
    previousFireMode = this.m_currentFireMode;
    this.m_currentFireMode = record.Type();
    if(this.m_currentFireMode == gamedataTriggerMode.Burst) {
      anim = new inkAnimDef();
      rotationInterpolator = new inkAnimRotation();
      rotationInterpolator.SetStartRotation(0);
      rotationInterpolator.SetEndRotation(-90);
      rotationInterpolator.SetDuration(0.10000000149011612);
      rotationInterpolator.SetType(inkanimInterpolationType.Linear);
      rotationInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
      anim.AddInterpolator(rotationInterpolator);
      WeakRefToRef(GetRootWidget()).PlayAnimation(anim);
    } else {
      if(previousFireMode == gamedataTriggerMode.Burst) {
        anim = new inkAnimDef();
        rotationInterpolator = new inkAnimRotation();
        rotationInterpolator.SetStartRotation(-90);
        rotationInterpolator.SetEndRotation(0);
        rotationInterpolator.SetDuration(0.10000000149011612);
        rotationInterpolator.SetType(inkanimInterpolationType.Linear);
        rotationInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
        anim.AddInterpolator(rotationInterpolator);
        WeakRefToRef(GetRootWidget()).PlayAnimation(anim);
      };
    };
  }

  public ref<inkAnimDef> GetIntroAnimation(Bool firstEquip) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(0);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    OnShow();
    return anim;
  }

  public ref<inkAnimDef> GetOutroAnimation() {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    OnHide();
    return anim;
  }

  protected cb Bool OnBulletSpreadChanged(Vector2 spread) {
    SetMargin(this.m_leftPart, new inkMargin(-this.m_horizontalMinSpread + this.m_gameplaySpreadMultiplier * spread.X,0,0,0));
    SetMargin(this.m_rightPart, new inkMargin(this.m_horizontalMinSpread + this.m_gameplaySpreadMultiplier * spread.X,0,0,0));
    SetMargin(this.m_upPart, new inkMargin(0,-this.m_verticalMinSpread + this.m_gameplaySpreadMultiplier * spread.Y,0,0));
    SetMargin(this.m_downPart, new inkMargin(0,this.m_verticalMinSpread + this.m_gameplaySpreadMultiplier * spread.Y,0,0));
    this.m_bufferedSpread = spread;
  }

  protected final void ColapseCrosshair(Bool full, Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimMargin> marginInterpolator;
    ref<inkAnimTransparency> alphaInterpolator;
    StopAllAnimations(this.m_leftPart);
    StopAllAnimations(this.m_rightPart);
    StopAllAnimations(this.m_upPart);
    StopAllAnimations(this.m_downPart);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_leftPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_leftPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_leftPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_rightPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_rightPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_rightPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_upPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_upPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_upPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_downPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_downPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_downPart, anim);
    if(full) {
      HideCenterPart(duration);
    };
  }

  protected final void ExpandCrosshair(Bool full, Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimMargin> marginInterpolator;
    ref<inkAnimTransparency> alphaInterpolator;
    StopAllAnimations(this.m_leftPart);
    StopAllAnimations(this.m_rightPart);
    StopAllAnimations(this.m_upPart);
    StopAllAnimations(this.m_downPart);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(-this.m_horizontalMinSpread - this.m_bufferedSpread.X * this.m_gameplaySpreadMultiplier,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_leftPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_leftPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(this.m_horizontalMinSpread + this.m_bufferedSpread.X * this.m_gameplaySpreadMultiplier,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_rightPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_rightPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(0,-this.m_verticalMinSpread - this.m_bufferedSpread.Y * this.m_gameplaySpreadMultiplier,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_upPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_upPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(0,this.m_verticalMinSpread + this.m_bufferedSpread.Y * this.m_gameplaySpreadMultiplier,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_downPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_downPart, anim);
    if(full) {
      ShowCenterPart(duration);
    };
  }

  private final void ShowCenterPart(Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_centerPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_centerPart, anim);
  }

  private final void HideCenterPart(Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_centerPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_centerPart, anim);
  }

  private final void OnShow() {
    switch(GetCrosshairState()) {
      case gamePSMCrosshairStates.Safe:
        ShowCenterPart(0.25);
        break;
      default:
        ExpandCrosshair(true, 0.25);
    };
  }

  private final void OnHide() {
    switch(GetCrosshairState()) {
      case gamePSMCrosshairStates.Safe:
        HideCenterPart(0.25);
        break;
      default:
        ColapseCrosshair(true, 0.25);
    };
  }

  protected void OnState_Safe() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_HipFire() {
    ExpandCrosshair(true, 0.10000000149011612);
  }

  protected void OnState_Aim() {
    ColapseCrosshair(true, 0.10000000149011612);
  }

  protected void OnState_Reload() {
    ColapseCrosshair(false, 0.25);
  }

  protected void OnState_Sprint() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_GrenadeCharging() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_Scanning() {
    ColapseCrosshair(true, 0.10000000149011612);
  }

  protected void ApplyCrosshairGUIState(CName state, ref<Entity> aimedAtEntity) {
    SetState(this.m_leftPart, state);
    SetState(this.m_rightPart, state);
    SetState(this.m_upPart, state);
    SetState(this.m_downPart, state);
  }
}
