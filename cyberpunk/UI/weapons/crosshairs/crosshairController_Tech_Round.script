
public class CrosshairGameController_Tech_Round extends BaseTechCrosshairController {

  private wref<inkWidget> m_root;

  private edit inkImageRef m_leftPart;

  private edit inkImageRef m_rightPart;

  [Default(CrosshairGameController_Tech_Round, .8))]
  private Float offsetLeftRight;

  [Default(CrosshairGameController_Tech_Round, 1.2))]
  private Float offsetLeftRightExtra;

  [Default(CrosshairGameController_Tech_Round, 40.0))]
  private Float latchVertical;

  private edit inkImageRef m_topPart;

  private edit inkImageRef m_bottomPart;

  private edit inkWidgetRef m_horiPart;

  private edit inkWidgetRef m_vertPart;

  private wref<inkCanvas> m_chargeBar;

  private wref<inkRectangle> m_chargeBarFG;

  private wref<inkRectangle> m_chargeBarBG;

  private wref<inkRectangle> m_chargeBarMG;

  private wref<inkWidget> m_centerPart;

  private wref<inkWidget> m_bottom_hip_bar;

  private wref<inkText> m_realFluffText_1;

  private wref<inkText> m_realFluffText_2;

  private Vector2 m_bufferedSpread;

  private ref<IBlackboard> m_weaponlocalBB;

  private Uint32 m_bbcharge;

  private Uint32 m_bbmagazineAmmoCapacity;

  private Uint32 m_bbmagazineAmmoCount;

  private Uint32 m_bbcurrentFireMode;

  [Default(CrosshairGameController_Tech_Round, 2))]
  private Int32 m_currentAmmo;

  [Default(CrosshairGameController_Tech_Round, 2))]
  private Int32 m_currentMaxAmmo;

  [Default(CrosshairGameController_Tech_Round, 8))]
  private Int32 m_maxSupportedAmmo;

  private gamedataTriggerMode m_currentFireMode;

  private Vector2 m_orgSideSize;

  private Float m_sidesScale;

  private Uint32 m_bbNPCStatsInfo;

  private Uint32 m_currentObstructedTargetBBID;

  private wref<GameObject> m_potentialVisibleTarget;

  private wref<GameObject> m_potentialObstructedTarget;

  [Default(CrosshairGameController_Tech_Round, true))]
  private Bool m_useVisibleTarget;

  [Default(CrosshairGameController_Tech_Round, 0))]
  public edit Float m_horizontalMinSpread;

  [Default(CrosshairGameController_Tech_Round, 0))]
  public edit Float m_verticalMinSpread;

  [Default(CrosshairGameController_Tech_Round, 1))]
  public edit Float m_gameplaySpreadMultiplier;

  public ref<inkAnimProxy> m_chargeAnimationProxy;

  private Float spreadRA;

  protected cb Bool OnInitialize() {
    this.m_root = RefToWeakRef(WeakRefToRef(GetRootWidget()));
    if(ToBool(this.m_rootWidget)) {
      WeakRefToRef(this.m_rootWidget).SetOpacity(0);
    };
    this.m_chargeBar = RefToWeakRef(Cast(WeakRefToRef(GetWidget("chargeBar"))));
    this.m_chargeBarBG = RefToWeakRef(Cast(WeakRefToRef(GetWidget("chargeBar/chargeBarBG"))));
    this.m_chargeBarMG = RefToWeakRef(Cast(WeakRefToRef(GetWidget("chargeBar/chargeBarMG"))));
    this.m_chargeBarFG = RefToWeakRef(Cast(WeakRefToRef(GetWidget("chargeBar/chargeBarFG"))));
    this.m_bottom_hip_bar = RefToWeakRef(WeakRefToRef(GetWidget("bottom_hip_bar")));
    this.m_realFluffText_1 = RefToWeakRef(Cast(WeakRefToRef(GetWidget("realFluffText_1"))));
    this.m_realFluffText_2 = RefToWeakRef(Cast(WeakRefToRef(GetWidget("realFluffText_2"))));
    this.m_orgSideSize = GetSize(this.m_leftPart);
    this.m_sidesScale = 1;
    WeakRefToRef(this.m_chargeBar).SetVisible(false);
    OnInitialize();
  }

  protected cb Bool OnUninitialize() {
    OnUninitialize();
  }

  protected cb Bool OnPreIntro() {
    ref<IBlackboard> m_uiBlackboard;
    OnPreIntro();
    WeakRefToRef(GetRootWidget()).SetOpacity(0);
    this.m_weaponlocalBB = GetWeaponLocalBlackboard();
    if(ToBool(this.m_targetBB)) {
      this.m_currentObstructedTargetBBID = WeakRefToRef(this.m_targetBB).RegisterDelayedListenerEntityID(GetAllBlackboardDefs().UI_TargetingInfo.CurrentObstructedTarget, this, "OnCurrentObstructedTarget");
      OnCurrentObstructedTarget(WeakRefToRef(this.m_targetBB).GetEntityID(GetAllBlackboardDefs().UI_TargetingInfo.CurrentObstructedTarget));
    };
    if(ToBool(this.m_weaponlocalBB)) {
      this.m_bbcharge = this.m_weaponlocalBB.RegisterListenerFloat(GetAllBlackboardDefs().Weapon.Charge, this, "OnChargeChanged");
      this.m_bbmagazineAmmoCount = this.m_weaponlocalBB.RegisterListenerUint(GetAllBlackboardDefs().Weapon.MagazineAmmoCount, this, "OnAmmoCountChanged");
      this.m_bbmagazineAmmoCapacity = this.m_weaponlocalBB.RegisterListenerUint(GetAllBlackboardDefs().Weapon.MagazineAmmoCapacity, this, "OnAmmoCapacityChanged");
      this.m_bbcurrentFireMode = this.m_weaponlocalBB.RegisterListenerVariant(GetAllBlackboardDefs().Weapon.TriggerMode, this, "OnTriggerModeChanged");
    };
    m_uiBlackboard = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_NPCNextToTheCrosshair);
    if(ToBool(m_uiBlackboard)) {
      this.m_bbNPCStatsInfo = m_uiBlackboard.RegisterListenerVariant(GetAllBlackboardDefs().UI_NPCNextToTheCrosshair.NameplateData, this, "OnNPCStatsChanged");
    };
    OnChargeChanged(0);
  }

  protected cb Bool OnPreOutro() {
    if(ToBool(this.m_targetBB)) {
      WeakRefToRef(this.m_targetBB).UnregisterDelayedListener(GetAllBlackboardDefs().UI_TargetingInfo.CurrentObstructedTarget, this.m_currentObstructedTargetBBID);
    };
    if(ToBool(this.m_weaponlocalBB)) {
      this.m_weaponlocalBB.UnregisterListenerFloat(GetAllBlackboardDefs().Weapon.Charge, this.m_bbcharge);
      this.m_weaponlocalBB.UnregisterListenerUint(GetAllBlackboardDefs().Weapon.MagazineAmmoCount, this.m_bbmagazineAmmoCount);
      this.m_weaponlocalBB.UnregisterListenerUint(GetAllBlackboardDefs().Weapon.MagazineAmmoCapacity, this.m_bbmagazineAmmoCapacity);
      this.m_weaponlocalBB.UnregisterListenerVariant(GetAllBlackboardDefs().Weapon.TriggerMode, this.m_bbcurrentFireMode);
    };
    OnPreOutro();
  }

  protected final void OnTriggerModeChanged(Variant value) {
    ref<TriggerMode_Record> record;
    record = FromVariant(value);
    this.m_currentFireMode = record.Type();
    if(this.m_currentFireMode == gamedataTriggerMode.Charge) {
      WeakRefToRef(this.m_chargeBar).SetVisible(true);
      WeakRefToRef(this.m_bottom_hip_bar).SetVisible(false);
    } else {
      WeakRefToRef(this.m_chargeBar).SetVisible(false);
      WeakRefToRef(this.m_bottom_hip_bar).SetVisible(true);
    };
  }

  protected final void OnAmmoCountChanged(Uint32 value) {
    this.m_currentAmmo = Cast(value);
  }

  protected final void OnAmmoCapacityChanged(Uint32 value) {
    this.m_currentMaxAmmo = Cast(value);
  }

  protected final void OnChargeChanged(Float charge) {
    Float actualMaxValue;
    inkAnimOptions playbackOptions;
    actualMaxValue = GetCurrentChargeLimit();
    WeakRefToRef(this.m_chargeBar).SetVisible(charge > 0);
    WeakRefToRef(this.m_bottom_hip_bar).SetVisible(charge > 0);
    WeakRefToRef(this.m_realFluffText_1).SetVisible(charge > 0);
    WeakRefToRef(this.m_realFluffText_2).SetVisible(charge > 0);
    WeakRefToRef(this.m_chargeBarFG).SetSize(new Vector2(charge * 3,6));
    WeakRefToRef(this.m_chargeBarBG).SetSize(new Vector2(GetBaseMaxChargeThreshold() * 3,6));
    WeakRefToRef(this.m_chargeBarMG).SetSize(new Vector2(GetFullyChargedThreshold() * 3,6));
    if(charge >= actualMaxValue) {
      if(!ToBool(this.m_chargeAnimationProxy)) {
        playbackOptions.loopInfinite = true;
        playbackOptions.loopType = inkanimLoopType.Cycle;
        this.m_chargeAnimationProxy = PlayLibraryAnimation("chargeMax", playbackOptions);
      };
    } else {
      if(ToBool(this.m_chargeAnimationProxy)) {
        this.m_chargeAnimationProxy.Stop();
        this.m_chargeAnimationProxy = null;
      };
    };
  }

  public ref<inkAnimDef> GetIntroAnimation(Bool firstEquip) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(0);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    OnShow();
    return anim;
  }

  public ref<inkAnimDef> GetOutroAnimation() {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    OnHide();
    return anim;
  }

  protected cb Bool OnBulletSpreadChanged(Vector2 spread) {
    SetMargin(this.m_bottomPart, new inkMargin(0,this.spreadRA,0,0));
    SetMargin(this.m_leftPart, new inkMargin(-spread.X * this.offsetLeftRight,0,0,0));
    SetMargin(this.m_rightPart, new inkMargin(spread.X * this.offsetLeftRight,0,0,0));
    SetSize(this.m_vertPart, 3, spread.Y * 2 + this.latchVertical);
    SetSize(this.m_horiPart, spread.X * 2, 3);
    SetMargin(this.m_topPart, new inkMargin(0,-spread.Y,0,0));
    SetMargin(this.m_bottomPart, new inkMargin(0,spread.Y,0,0));
  }

  protected final void ColapseCrosshair(Bool full, Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimMargin> marginInterpolator;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_leftPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_leftPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_leftPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_rightPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_rightPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_rightPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(GetMargin(this.m_bottomPart));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_bottomPart));
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_bottomPart, anim);
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(WeakRefToRef(this.m_root).GetOpacity());
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    if(ToBool(this.m_centerPart)) {
      WeakRefToRef(this.m_centerPart).SetVisible(false);
    };
    if(full) {
      HideCenterPart(duration);
    };
    if(ToBool(this.m_chargeAnimationProxy)) {
      this.m_chargeAnimationProxy.Stop();
    };
  }

  protected final void ExpandCrosshair(Bool full, Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimMargin> marginInterpolator;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(0,0,SinF(20) * this.spreadRA,CosF(20) * this.spreadRA));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_leftPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_leftPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(SinF(20) * this.spreadRA,0,0,CosF(20) * this.spreadRA));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_rightPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_rightPart, anim);
    anim = new inkAnimDef();
    marginInterpolator = new inkAnimMargin();
    marginInterpolator.SetStartMargin(new inkMargin(0,0,0,0));
    marginInterpolator.SetEndMargin(new inkMargin(0,this.spreadRA,0,0));
    marginInterpolator.SetDuration(duration);
    marginInterpolator.SetType(inkanimInterpolationType.Linear);
    marginInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(marginInterpolator);
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(GetOpacity(this.m_bottomPart));
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    PlayAnimation(this.m_bottomPart, anim);
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(WeakRefToRef(this.m_root).GetOpacity());
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    if(ToBool(this.m_centerPart)) {
      WeakRefToRef(this.m_centerPart).SetVisible(true);
    };
    if(full) {
      ShowCenterPart(duration);
    };
    if(ToBool(this.m_chargeAnimationProxy)) {
      this.m_chargeAnimationProxy.Stop();
    };
  }

  private final void ShowCenterPart(Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    if(ToBool(this.m_centerPart)) {
      alphaInterpolator.SetStartTransparency(WeakRefToRef(this.m_centerPart).GetOpacity());
    };
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    if(ToBool(this.m_centerPart)) {
      WeakRefToRef(this.m_centerPart).PlayAnimation(anim);
    };
  }

  private final void HideCenterPart(Float duration) {
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(WeakRefToRef(this.m_centerPart).GetOpacity());
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetDuration(duration);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(alphaInterpolator);
    WeakRefToRef(this.m_centerPart).PlayAnimation(anim);
  }

  private final void OnShow() {
    switch(GetCrosshairState()) {
      case gamePSMCrosshairStates.Safe:
        ShowCenterPart(0.25);
        break;
      default:
        ExpandCrosshair(true, 0.25);
    };
  }

  private final void OnHide() {
    switch(GetCrosshairState()) {
      case gamePSMCrosshairStates.Safe:
        HideCenterPart(0.25);
        break;
      default:
        ColapseCrosshair(true, 0.25);
    };
  }

  protected void OnState_Safe() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_HipFire() {
    ref<inkAnimDef> anim;
    ref<inkAnimSize> sizeInterpolator;
    anim = new inkAnimDef();
    sizeInterpolator = new inkAnimSize();
    sizeInterpolator.SetStartSize(new Vector2(this.m_orgSideSize.X / 2,this.m_orgSideSize.Y / 2));
    sizeInterpolator.SetEndSize(this.m_orgSideSize);
    sizeInterpolator.SetDuration(0.10000000149011612);
    sizeInterpolator.SetType(inkanimInterpolationType.Linear);
    sizeInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    anim.AddInterpolator(sizeInterpolator);
    PlayAnimation(this.m_leftPart, anim);
    PlayAnimation(this.m_rightPart, anim);
    PlayAnimation(this.m_bottomPart, anim);
    ExpandCrosshair(true, 0.10000000149011612);
  }

  protected void OnState_Aim() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_Reload() {
    ColapseCrosshair(false, 0.25);
  }

  protected void OnState_Sprint() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_GrenadeCharging() {
    ColapseCrosshair(false, 0.10000000149011612);
  }

  protected void OnState_Scanning() {
    ColapseCrosshair(true, 0.10000000149011612);
  }

  protected cb Bool OnCurrentAimTarget(EntityID entId) {
    this.m_potentialVisibleTarget = RefToWeakRef(Cast(FindEntityByID(GetGame(), entId)));
    OnTargetsChanged();
  }

  protected cb Bool OnCurrentObstructedTarget(EntityID entId) {
    this.m_potentialObstructedTarget = RefToWeakRef(Cast(FindEntityByID(GetGame(), entId)));
    OnTargetsChanged();
  }

  private final void OnTargetsChanged() {
    wref<GameObject> newTarget;
    gameVisionModeSystemRevealIdentifier revealRequest;
    revealRequest.sourceEntityId = GetOwnerEntity().GetEntityID();
    revealRequest.reason = "TechWeapon";
    if(ToBool(this.m_potentialVisibleTarget)) {
      newTarget = this.m_potentialVisibleTarget;
      this.m_useVisibleTarget = true;
    } else {
      if(ToBool(this.m_potentialObstructedTarget) && WeakRefToRef(this.m_potentialObstructedTarget).HasRevealRequest(revealRequest)) {
        newTarget = this.m_potentialObstructedTarget;
        this.m_useVisibleTarget = false;
      };
    };
    if(WeakRefToRef(newTarget) != WeakRefToRef(this.m_targetEntity)) {
      RegisterTargetCallbacks(false);
      this.m_targetEntity = newTarget;
      RegisterTargetCallbacks(true);
      UpdateCrosshairGUIState(true);
    };
  }

  protected void ApplyCrosshairGUIState(CName state, ref<Entity> aimedAtEntity) {
    WeakRefToRef(this.m_root).SetState(state);
    SetState(this.m_leftPart, state);
    SetState(this.m_rightPart, state);
    SetState(this.m_bottomPart, state);
    WeakRefToRef(this.m_chargeBarBG).SetState(state);
    WeakRefToRef(this.m_chargeBarFG).SetState(state);
  }

  protected Float GetDistanceToTarget() {
    BlackboardID_EntityID targetBBID;
    BlackboardID_Float distanceBBID;
    EntityID targetID;
    Float distance;
    distance = 0;
    if(this.m_useVisibleTarget) {
      targetBBID = GetAllBlackboardDefs().UI_TargetingInfo.CurrentVisibleTarget;
      distanceBBID = GetAllBlackboardDefs().UI_TargetingInfo.VisibleTargetDistance;
    } else {
      targetBBID = GetAllBlackboardDefs().UI_TargetingInfo.CurrentObstructedTarget;
      distanceBBID = GetAllBlackboardDefs().UI_TargetingInfo.ObstructedTargetDistance;
    };
    targetID = WeakRefToRef(this.m_targetBB).GetEntityID(targetBBID);
    if(IsDefined(targetID)) {
      distance = WeakRefToRef(this.m_targetBB).GetFloat(distanceBBID);
    };
    return distance;
  }
}
