
public class NameplateVisualsLogicController extends inkLogicController {

  private wref<inkCompoundWidget> m_rootWidget;

  private edit inkWidgetRef m_bigIconMain;

  private edit inkTextRef m_bigLevelText;

  private edit inkTextRef m_nameTextMain;

  private edit inkImageRef m_bigIconArt;

  private edit inkWidgetRef m_preventionIcon;

  private edit inkImageRef m_levelContainer;

  private edit inkWidgetRef m_nameFrame;

  private edit inkWidgetRef m_healthbarWidget;

  private edit inkWidgetRef m_healthBarFull;

  private edit inkWidgetRef m_healthBarFrame;

  private edit inkWidgetRef m_taggedIcon;

  private edit inkWidgetRef m_iconBG;

  private edit inkWidgetRef m_civilianIcon;

  private edit inkCompoundRef m_stealthMappinSlot;

  private edit inkCompoundRef m_iconTextWrapper;

  private edit inkWidgetRef m_container;

  private edit inkCompoundRef m_LevelcontainerAndText;

  private edit inkCompoundRef m_rareStars;

  private edit inkCompoundRef m_eliteStars;

  private edit inkImageRef m_hardEnemy;

  private edit inkWidgetRef m_hardEnemyWrapper;

  private edit inkWidgetRef m_damagePreviewWrapper;

  private edit inkWidgetRef m_damagePreviewWidget;

  private edit inkWidgetRef m_damagePreviewArrow;

  private edit inkHorizontalPanelRef m_buffsList;

  private array<wref<inkWidget>> m_buffWidgets;

  private wref<GameObject> m_cachedPuppet;

  private NPCNextToTheCrosshair m_cachedIncomingData;

  private Bool m_isOfficer;

  private Bool m_isBoss;

  private Bool m_isElite;

  private Bool m_isRare;

  private Bool m_isPrevention;

  private Bool m_canCallReinforcements;

  private Bool m_isCivilian;

  private Bool m_isBurning;

  private Bool m_isPoisoned;

  private Color m_bossColor;

  private Bool m_npcDefeated;

  private Bool m_isStealthMappinVisible;

  private gamePSMZones m_playerZone;

  private wref<NameplateBarLogicController> m_healthController;

  private Bool m_hasCenterIcon;

  private inkWidgetRef m_animatingObject;

  private Bool m_isAnimating;

  private ref<inkAnimProxy> m_animProxy;

  private ref<inkAnimDef> m_alpha_fadein;

  private ref<inkAnimProxy> m_preventionAnimProxy;

  private ref<inkAnimProxy> m_damagePreviewAnimProxy;

  [Default(NameplateVisualsLogicController, false))]
  private Bool m_isQuestTarget;

  [Default(NameplateVisualsLogicController, false))]
  private Bool m_forceHide;

  private Bool m_isHardEnemy;

  private Bool m_npcIsAggressive;

  private Bool m_playerAimingDownSights;

  private Bool m_playerInCombat;

  private Bool m_playerInStealth;

  private Bool m_healthNotFull;

  private Bool m_healthbarVisible;

  private Bool m_levelContainerShouldBeVisible;

  private Int32 m_currentHealth;

  private Int32 m_maximumHealth;

  private Int32 m_currentDamagePreviewValue;

  protected cb Bool OnInitialize() {
    this.m_rootWidget = RefToWeakRef(Cast(WeakRefToRef(GetRootWidget())));
    this.m_healthController = RefToWeakRef(Cast(WeakRefToRef(GetController(this.m_healthbarWidget))));
    this.m_npcDefeated = false;
    this.m_playerAimingDownSights = false;
    this.m_playerInCombat = false;
    this.m_playerInStealth = false;
    this.m_healthNotFull = false;
    this.m_healthbarVisible = false;
    SetVisible(this.m_buffsList, false);
    SetVisible(this.m_healthbarWidget, false);
  }

  public final void SetVisualData(ref<GameObject> puppet, NPCNextToTheCrosshair incomingData, Bool isNewNpc?) {
    Int32 quickHackUploadPercent;
    ref<NPCPuppet> npc;
    this.m_cachedPuppet = RefToWeakRef(puppet);
    this.m_cachedIncomingData = incomingData;
    npc = Cast(WeakRefToRef(incomingData.npc));
    if(ToBool(npc)) {
      if(incomingData.attitude == EAIAttitude.AIA_Hostile) {
        this.m_npcIsAggressive = true;
      } else {
        if(npc.IsAggressive() && incomingData.attitude != EAIAttitude.AIA_Friendly) {
          this.m_npcIsAggressive = true;
        } else {
          this.m_npcIsAggressive = false;
        };
      };
    } else {
      this.m_npcIsAggressive = false;
    };
    this.m_currentHealth = incomingData.currentHealth;
    this.m_maximumHealth = incomingData.maximumHealth;
    this.m_healthNotFull = incomingData.currentHealth < incomingData.maximumHealth;
    this.m_npcDefeated = !IsActive(WeakRefToRef(incomingData.npc));
    if(ToBool(npc) && !this.m_npcDefeated) {
      this.m_npcDefeated = npc.IsAboutToBeDefeated() || npc.IsAboutToDie();
    };
    SetNPCType(RefToWeakRef(Cast(puppet)));
    SetAttitudeColors(RefToWeakRef(Cast(puppet)), incomingData);
    SetElementVisibility(incomingData);
    if(incomingData.level == 0) {
      this.m_levelContainerShouldBeVisible = false;
    };
    UpdateHealthbarVisibility();
    if(incomingData.maximumHealth == 0) {
      WeakRefToRef(this.m_healthController).SetNameplateBarProgress(0, isNewNpc);
    } else {
      WeakRefToRef(this.m_healthController).SetNameplateBarProgress(Cast(incomingData.currentHealth) / Cast(incomingData.maximumHealth), isNewNpc);
    };
    if(this.m_currentDamagePreviewValue > 0) {
      PreviewDamage(this.m_currentDamagePreviewValue);
    };
    quickHackUploadPercent = incomingData.quickHackUpload;
  }

  public final void PreviewDamage(Int32 value) {
    inkAnimOptions animOptions;
    Float damagePercentage;
    Float currentHealthPercentage;
    Float offset;
    Float renderTransformXPivot;
    this.m_currentDamagePreviewValue = value;
    if(value < 0) {
      if(ToBool(this.m_damagePreviewAnimProxy) && this.m_damagePreviewAnimProxy.IsPlaying()) {
        this.m_damagePreviewAnimProxy.Stop();
      };
      SetVisible(this.m_damagePreviewWrapper, false);
    } else {
      if(this.m_maximumHealth > 0) {
        currentHealthPercentage = Cast(this.m_currentHealth) / Cast(this.m_maximumHealth);
        damagePercentage = Cast(value) / Cast(this.m_maximumHealth);
        damagePercentage = MinF(damagePercentage, currentHealthPercentage);
        renderTransformXPivot = damagePercentage < 1 ? currentHealthPercentage - damagePercentage / 1 - damagePercentage : 1;
        offset = 100 + 150 * damagePercentage - 150 * currentHealthPercentage;
        SetRenderTransformPivot(this.m_damagePreviewWidget, new Vector2(renderTransformXPivot,1));
        SetScale(this.m_damagePreviewWidget, new Vector2(damagePercentage,1));
        SetMargin(this.m_damagePreviewArrow, 0, -22, offset, 0);
        if(!ToBool(this.m_damagePreviewAnimProxy) || !this.m_damagePreviewAnimProxy.IsPlaying()) {
          animOptions.loopType = inkanimLoopType.Cycle;
          animOptions.loopInfinite = true;
          this.m_damagePreviewAnimProxy = PlayLibraryAnimation("damage_preview_looping", animOptions);
        };
        SetVisible(this.m_damagePreviewWrapper, true);
      };
    };
  }

  public final Float GetHeightOffser() {
    Vector2 size;
    size = GetDesiredSize(this.m_container);
    return size.Y;
  }

  public final void UpdateBecauseOfMapPin() {
    SetVisualData(WeakRefToRef(this.m_cachedPuppet), this.m_cachedIncomingData);
  }

  public final void UpdatePlayerZone(gamePSMZones zone, Bool onlySetValue?) {
    this.m_playerZone = zone;
    if(ToBool(this.m_cachedPuppet) && !onlySetValue) {
      SetVisualData(WeakRefToRef(this.m_cachedPuppet), this.m_cachedIncomingData);
    };
  }

  public final void UpdatePlayerAimStatus(gamePSMUpperBodyStates state, Bool onlySetValue?) {
    this.m_playerAimingDownSights = state == gamePSMUpperBodyStates.Aim;
    if(ToBool(this.m_cachedPuppet) && !onlySetValue) {
      UpdateHealthbarVisibility();
    };
  }

  public final void UpdatePlayerCombat(gamePSMCombat state, Bool onlySetValue?) {
    this.m_playerInCombat = state == gamePSMCombat.InCombat;
    this.m_playerInStealth = state == gamePSMCombat.Stealth;
    if(ToBool(this.m_cachedPuppet) && !onlySetValue) {
      UpdateHealthbarVisibility();
    };
  }

  public final void UpdateHealthbarColor(Bool isHostile) {
    if(isHostile) {
      SetState(this.m_healthbarWidget, "Hostile");
      SetState(this.m_healthBarFull, "Hostile");
    } else {
      SetState(this.m_healthbarWidget, "Neutral_Enemy");
      SetState(this.m_healthBarFull, "Neutral_Enemy");
    };
  }

  private final void UpdateHealthbarVisibility() {
    Bool hpVisible;
    hpVisible = this.m_npcIsAggressive && !this.m_isBoss && this.m_healthNotFull || this.m_playerAimingDownSights || this.m_playerInCombat || this.m_playerInStealth;
    if(this.m_healthbarVisible != hpVisible) {
      this.m_healthbarVisible = hpVisible;
      SetVisible(this.m_healthbarWidget, this.m_healthbarVisible);
    };
  }

  private final void SetNPCType(wref<ScriptedPuppet> puppet) {
    gamedataNPCRarity puppetRarity;
    this.m_isOfficer = false;
    this.m_isBoss = false;
    this.m_isCivilian = false;
    this.m_canCallReinforcements = false;
    this.m_isElite = false;
    this.m_isRare = false;
    this.m_isPrevention = false;
    if(ToBool(puppet)) {
      this.m_isPrevention = WeakRefToRef(puppet).IsPrevention();
      puppetRarity = WeakRefToRef(puppet).GetPuppetRarity().Type();
      switch(puppetRarity) {
        case gamedataNPCRarity.Officer:
          this.m_isOfficer = true;
          break;
        case gamedataNPCRarity.Boss:
          this.m_isBoss = true;
          break;
        case gamedataNPCRarity.Elite:
          this.m_isElite = true;
          break;
        case gamedataNPCRarity.Rare:
          this.m_isRare = true;
      };
      this.m_canCallReinforcements = GetStatsSystem(WeakRefToRef(puppet).GetGame()).GetStatBoolValue(Cast(WeakRefToRef(puppet).GetEntityID()), gamedataStatType.CanCallReinforcements);
    };
  }

  private final void UpdateCenterIcon(CName texture) {
    if(texture == "") {
      SetVisible(this.m_bigIconArt, false);
    } else {
      SetVisible(this.m_bigIconArt, true);
      SetTexturePart(this.m_bigIconArt, texture);
    };
  }

  private final void SetAttitudeColors(wref<gamePuppetBase> puppet, NPCNextToTheCrosshair incomingData) {
    CName attitudeColor;
    CName className;
    TweakDBID characterRecordID;
    ref<NPCPuppet> npc;
    npc = Cast(WeakRefToRef(incomingData.npc));
    SetLetterCase(this.m_nameTextMain, textLetterCase.UpperCase);
    SetText(this.m_nameTextMain, incomingData.name);
    SetText(this.m_bigLevelText, "");
    switch(incomingData.attitude) {
      case EAIAttitude.AIA_Hostile:
        attitudeColor = "Hostile";
        break;
      case EAIAttitude.AIA_Friendly:
        attitudeColor = "Friendly";
        break;
      case EAIAttitude.AIA_Neutral:
        attitudeColor = "Neutral";
        break;
      default:
        attitudeColor = "Civilian";
    };
    if(this.m_npcIsAggressive) {
      SetState(this.m_bigLevelText, attitudeColor);
      SetState(this.m_bigIconArt, this.m_isQuestTarget ? "Quest" : "Hostile");
      SetState(this.m_civilianIcon, this.m_isQuestTarget ? "Quest" : "Hostile");
      SetState(this.m_rareStars, "Hostile");
      SetState(this.m_eliteStars, "Hostile");
      SetState(this.m_nameTextMain, this.m_isQuestTarget ? "Quest" : "Hostile");
    } else {
      SetState(this.m_bigLevelText, attitudeColor);
      SetState(this.m_bigIconArt, this.m_isQuestTarget ? "Quest" : attitudeColor);
      SetState(this.m_civilianIcon, this.m_isQuestTarget ? "Quest" : attitudeColor);
      SetState(this.m_rareStars, attitudeColor);
      SetState(this.m_eliteStars, attitudeColor);
      SetState(this.m_nameTextMain, this.m_isQuestTarget ? "Quest" : attitudeColor);
      SetState(this.m_hardEnemy, attitudeColor);
    };
    if(this.m_isBoss) {
      attitudeColor = "Boss";
    };
    if(WeakRefToRef(puppet) != null && WeakRefToRef(puppet).IsPlayer()) {
      characterRecordID = WeakRefToRef(puppet).GetRecordID();
      className = GetCharacterRecord(characterRecordID).CpoClassName();
      SetState(this.m_nameTextMain, "CPO_Player");
    };
    if(this.m_isPrevention) {
      PlayPreventionAnim();
    } else {
      StopPreventionAnim();
    };
  }

  private final void SetElementVisibility(NPCNextToTheCrosshair incomingData) {
    ref<NPCPuppet> npc;
    EPowerDifferential enemyDifficulty;
    SetVisible(this.m_bigIconArt, false);
    SetVisible(this.m_nameTextMain, false);
    SetVisible(this.m_eliteStars, false);
    SetVisible(this.m_rareStars, false);
    SetVisible(this.m_civilianIcon, false);
    SetVisible(this.m_hardEnemyWrapper, false);
    SetVisible(this.m_preventionIcon, false);
    this.m_levelContainerShouldBeVisible = false;
    this.m_isHardEnemy = false;
    npc = Cast(WeakRefToRef(incomingData.npc));
    if(ToBool(npc)) {
      WeakRefToRef(this.m_rootWidget).SetVisible(!this.m_forceHide && WeakRefToRef(incomingData.npc).IsPlayer() || !this.m_npcDefeated);
    };
    if(this.m_npcIsAggressive) {
      enemyDifficulty = CalculatePowerDifferential(npc);
      if(enemyDifficulty == EPowerDifferential.IMPOSSIBLE || HasVisualTag(RefToWeakRef(npc), "Sumo")) {
        this.m_isHardEnemy = true;
        SetVisible(this.m_hardEnemyWrapper, true);
      } else {
        this.m_isHardEnemy = false;
        this.m_isAnimating = false;
        if(ToBool(this.m_animProxy)) {
          this.m_animProxy.Stop();
          this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnFadeInComplete");
          this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnFadeOutComplete");
          this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnScreenDelayComplete");
        };
      };
      this.m_levelContainerShouldBeVisible = true;
      SetVisible(this.m_bigLevelText, true);
      if(this.m_isPrevention) {
        UpdateCenterIcon("");
        SetVisible(this.m_preventionIcon, true);
        SetVisible(this.m_hardEnemyWrapper, false);
      } else {
        if(this.m_isOfficer) {
        } else {
          if(this.m_isElite) {
            SetVisible(this.m_hardEnemyWrapper, true);
          } else {
            if(this.m_isBoss) {
              SetVisible(this.m_hardEnemyWrapper, true);
            } else {
              if(this.m_isRare) {
              };
            };
          };
        };
      };
    };
    if(ToBool(npc) && npc.IsVendor()) {
      SetVisible(this.m_nameTextMain, true);
      this.m_levelContainerShouldBeVisible = false;
    };
    if(incomingData.attitude == EAIAttitude.AIA_Friendly) {
      SetVisible(this.m_nameTextMain, true);
      this.m_levelContainerShouldBeVisible = false;
    };
    if(IsVisible(this.m_nameTextMain) && IsVisible(this.m_nameFrame)) {
      SetVisible(this.m_civilianIcon, false);
    };
  }

  public final Bool IsAnyElementVisible() {
    return IsVisible(this.m_nameTextMain) || this.m_levelContainerShouldBeVisible;
  }

  private final void SetCycleAnimation(Bool isNewNPC, NPCNextToTheCrosshair incomingData) {
    ref<NPCPuppet> npc;
    npc = Cast(WeakRefToRef(incomingData.npc));
    if(isNewNPC) {
      this.m_animProxy.Stop();
      this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnFadeInComplete");
      this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnFadeOutComplete");
      this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnScreenDelayComplete");
      this.m_isAnimating = false;
      SetOpacity(this.m_LevelcontainerAndText, 1);
    };
    if(!this.m_isAnimating) {
      this.m_animatingObject = this.m_hardEnemyWrapper;
      SetOpacity(this.m_LevelcontainerAndText, 0);
      OnScreenDelay();
      this.m_isAnimating = true;
    };
  }

  private final void PlayPreventionAnim() {
    CName initialState;
    StopPreventionAnim();
    this.m_preventionAnimProxy = PlayPreventionBlinkAnimation(GetRootWidget(), initialState);
    if(ToBool(this.m_preventionAnimProxy)) {
      SetState(this.m_preventionIcon, initialState);
      this.m_preventionAnimProxy.RegisterToCallback(inkanimEventType.OnEndLoop, this, "OnPreventionAnimLoop");
    };
  }

  private final void StopPreventionAnim() {
    if(ToBool(this.m_preventionAnimProxy)) {
      this.m_preventionAnimProxy.Stop();
      this.m_preventionAnimProxy = null;
    };
  }

  public final void OnPreventionAnimLoop(ref<inkAnimProxy> anim) {
    CName preventionState;
    preventionState = GetState(this.m_preventionIcon);
    CyclePreventionState(preventionState);
    SetState(this.m_preventionIcon, preventionState);
  }

  public final void UpdateBuffDebuffList(Variant argData, Bool argIsBuffList) {
    ref<StatusEffect_Record> data;
    array<BuffInfo> buffList;
    Int32 onScreenBuffsCount;
    Int32 incomingBuffsCount;
    Int32 i;
    wref<inkWidget> currBuffWidget;
    wref<buffListItemLogicController> currBuffLoc;
    Int32 stackCount;
    Float buffTimeRemaining;
    String iconPath;
    String dispayName;
    if(IsValid(argData)) {
      buffList = FromVariant(argData);
    };
    incomingBuffsCount = Size(buffList);
    onScreenBuffsCount = GetNumChildren(this.m_buffsList);
    SetVisible(this.m_buffsList, incomingBuffsCount > 0);
    if(incomingBuffsCount != 0) {
      if(onScreenBuffsCount > incomingBuffsCount) {
        i = incomingBuffsCount - 1;
        while(i < onScreenBuffsCount) {
          currBuffWidget = this.m_buffWidgets[i];
          WeakRefToRef(currBuffWidget).SetVisible(false);
          i = i + 1;
        };
      } else {
        if(onScreenBuffsCount < incomingBuffsCount) {
          while(onScreenBuffsCount < incomingBuffsCount) {
            currBuffWidget = SpawnFromLocal(Get(this.m_buffsList), "Buff");
            WeakRefToRef(currBuffWidget).SetVisible(false);
            Push(this.m_buffWidgets, currBuffWidget);
            onScreenBuffsCount = onScreenBuffsCount + 1;
          };
        };
      };
    } else {
      i = 0;
      while(i < onScreenBuffsCount) {
        currBuffWidget = this.m_buffWidgets[i];
        WeakRefToRef(currBuffWidget).SetVisible(false);
        i += 1;
      };
    };
    i = 0;
    while(i < incomingBuffsCount) {
      data = GetStatusEffectRecord(buffList[i].buffID);
      buffTimeRemaining = buffList[i].timeRemaining;
      iconPath = WeakRefToRef(data.UiData()).IconPath();
      dispayName = WeakRefToRef(data.UiData()).DisplayName();
      if(iconPath == "") {
      } else {
        currBuffWidget = this.m_buffWidgets[i];
        WeakRefToRef(currBuffWidget).SetVisible(true);
        currBuffLoc = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(currBuffWidget).GetController())));
        WeakRefToRef(currBuffLoc).SetData(StringToName(iconPath), buffTimeRemaining);
      };
      i = i + 1;
    };
    SetVisualData(WeakRefToRef(this.m_cachedPuppet), this.m_cachedIncomingData);
  }

  public final void CheckStealthMappinVisibility() {
    ref<inkWidget> stealthMappinRef;
    if(GetNumChildren(this.m_stealthMappinSlot) > 0) {
      stealthMappinRef = WeakRefToRef(GetWidgetByIndex(this.m_stealthMappinSlot, 0));
      this.m_isStealthMappinVisible = stealthMappinRef.IsVisible();
      if(this.m_hasCenterIcon || IsVisible(this.m_bigLevelText)) {
        SetVisible(this.m_iconTextWrapper, true);
      };
    };
  }

  public final Bool IsQuestTarget() {
    return this.m_isQuestTarget;
  }

  public final void SetQuestTarget(Bool value) {
    this.m_isQuestTarget = value;
  }

  public final void SetForceHide(Bool value) {
    this.m_forceHide = value;
  }

  private final void OnFadeIn() {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetStartTransparency(0);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_animatingObject, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnFadeInComplete");
  }

  protected cb Bool OnFadeInComplete(ref<inkAnimProxy> anim) {
    this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnFadeInComplete");
    OnScreenDelay();
  }

  private final void OnScreenDelay() {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(2);
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_animatingObject, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnScreenDelayComplete");
  }

  protected cb Bool OnScreenDelayComplete(ref<inkAnimProxy> anim) {
    this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnScreenDelayComplete");
    OnFadeOut();
  }

  private final void OnFadeOut() {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(0.25);
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(0);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_animatingObject, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnFadeOutComplete");
  }

  protected cb Bool OnFadeOutComplete(ref<inkAnimProxy> anim) {
    this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnFadeOutComplete");
    if(this.m_animatingObject == this.m_LevelcontainerAndText) {
      this.m_animatingObject = this.m_hardEnemyWrapper;
    } else {
      if(this.m_animatingObject == this.m_hardEnemyWrapper) {
        this.m_animatingObject = this.m_LevelcontainerAndText;
      };
    };
    OnFadeIn();
  }
}
