
public class DamageIndicatorGameController extends inkHUDGameController {

  private final Bool ShouldShowDamage(ref<gameDamageReceivedEvent> evt) {
    if(evt.hitEvent.attackData.GetAttackType() == gamedataAttackType.Effect) {
      return false;
    };
    if(evt.hitEvent.attackData.HasFlag(hitFlag.DisableNPCHitReaction)) {
      return false;
    };
    return true;
  }
}

public class DamageIndicatorPartLogicController extends BaseDirectionalIndicatorPartLogicController {

  private edit inkImageRef m_arrowFrontWidget;

  [Default(DamageIndicatorPartLogicController, 100))]
  private edit Float m_damageThreshold;

  private wref<inkWidget> m_root;

  private ref<inkAnimProxy> m_animProxy;

  private Float m_damageTaken;

  private Bool m_continuous;

  protected final native void SetReadyToRemove()

  protected final native void SetShowingDamage(Bool showing)

  protected final native void SetContinuous(Bool continuous)

  protected final native void SetMinimumOpacity(Float opacity)

  protected final native void ResetMinimumOpacity()

  protected cb Bool OnInitialize() {
    this.m_root = GetRootWidget();
    Reset();
  }

  protected final void InitPart() {
    Reset();
  }

  protected final void AddIncomingDamage(ref<gameDamageReceivedEvent> evt) {
    Float damagePercent;
    this.m_damageTaken += evt.totalDamageReceived;
    damagePercent = this.m_damageTaken / this.m_damageThreshold * 100;
    SetTexturePart(this.m_arrowFrontWidget, StringToName("damage_ind_anim_00" + RoundF(ClampF(damagePercent, 0, 100))));
    if(!this.m_continuous) {
      if(ToBool(this.m_animProxy) && this.m_animProxy.IsPlaying()) {
        this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnOutroComplete");
        this.m_animProxy.Stop();
      };
      WeakRefToRef(this.m_root).SetState("Damage");
      PlayAnim("Outro", "OnOutroComplete");
      SetShowingDamage(true);
      ResetMinimumOpacity();
    };
  }

  protected final void AddAttackAttempt(ref<AIAttackAttemptEvent> evt) {
    Float progress;
    if(ToBool(this.m_animProxy)) {
      progress = this.m_animProxy.GetProgression();
    };
    if(evt.continuousMode != gameEContinuousMode.) {
      if(evt.continuousMode == gameEContinuousMode.Start) {
        if(!this.m_continuous) {
          if(this.m_animProxy.IsPlaying()) {
            this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnOutroComplete");
            this.m_animProxy.Stop();
          };
          SetShowingDamage(false);
          SetMinimumOpacity(evt.minimumOpacity);
          WeakRefToRef(this.m_root).SetState("Hacking");
          this.m_continuous = true;
          this.m_animProxy = PlayLibraryAnimation("Intro_Continuous");
        };
      } else {
        if(evt.continuousMode == gameEContinuousMode.Stop) {
          if(this.m_continuous) {
            SetMinimumOpacity(evt.minimumOpacity);
          };
          StopContinuousEffect();
        };
      };
      SetContinuous(this.m_continuous);
    } else {
      if(!this.m_continuous) {
        if(this.m_damageTaken == 0 || progress > 0.75) {
          WeakRefToRef(this.m_root).SetState("Danger");
          SetShowingDamage(false);
          SetMinimumOpacity(evt.minimumOpacity);
          if(ToBool(this.m_animProxy) && this.m_animProxy.IsPlaying()) {
            this.m_animProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnOutroComplete");
            this.m_animProxy.Stop();
            if(evt.isWindUp && progress > 0.75) {
              PlayAnim("Outro_WindUp", "OnOutroComplete");
            } else {
              PlayAnim("Outro_Miss_NoDelay", "OnOutroComplete");
            };
          } else {
            if(evt.isWindUp) {
              PlayAnim("Outro_WindUp", "OnOutroComplete");
            } else {
              PlayAnim("Outro_Miss", "OnOutroComplete");
            };
          };
        };
      };
    };
  }

  private final void StopContinuousEffect() {
    if(this.m_continuous) {
      if(this.m_animProxy.IsPlaying()) {
        this.m_animProxy.Stop();
      };
      this.m_continuous = false;
      PlayAnim("Outro_Continuous", "OnOutroComplete");
    } else {
      if(!ToBool(this.m_animProxy) || !this.m_animProxy.IsPlaying()) {
        SetReadyToRemove();
      };
    };
  }

  private final void Reset() {
    this.m_damageTaken = 0;
    this.m_continuous = false;
    SetShowingDamage(false);
    SetContinuous(false);
  }

  private final void PlayAnim(CName animName, CName callback) {
    this.m_animProxy = PlayLibraryAnimation(animName);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, callback);
  }

  protected cb Bool OnOutroComplete(ref<inkAnimProxy> e) {
    SetReadyToRemove();
  }
}
