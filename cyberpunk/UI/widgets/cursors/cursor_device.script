
public class cursorDeviceGameController extends inkGameController {

  private ref<IBlackboard> m_bbUIData;

  private wref<IBlackboard> m_bbWeaponInfo;

  private Uint32 m_bbWeaponEventId;

  private Uint32 m_bbPlayerTierEventId;

  private Uint32 m_interactionBlackboardId;

  private Uint32 m_upperBodyStateBlackboardId;

  private GameplayTier m_sceneTier;

  private gamePSMUpperBodyStates m_upperBodyState;

  private Bool m_isUnarmed;

  private wref<inkImage> m_cursorDevice;

  private ref<inkAnimDef> m_fadeOutAnimation;

  private ref<inkAnimDef> m_fadeInAnimation;

  private Bool m_wasLastInteractionWithDevice;

  private Bool m_interactionDeviceState;

  protected cb Bool OnInitialize() {
    ref<IBlackboard> bbPlayerSM;
    ref<PlayerStateMachineDef> playerSMDef;
    wref<GameObject> playerPuppet;
    this.m_wasLastInteractionWithDevice = false;
    this.m_bbUIData = GetBlackboardSystem().Get(GetAllBlackboardDefs().UIGameData);
    this.m_interactionBlackboardId = this.m_bbUIData.RegisterListenerVariant(GetAllBlackboardDefs().UIGameData.InteractionData, this, "OnInteractionStateChange");
    this.m_bbWeaponInfo = RefToWeakRef(GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_ActiveWeaponData));
    this.m_bbWeaponEventId = WeakRefToRef(this.m_bbWeaponInfo).RegisterListenerVariant(GetAllBlackboardDefs().UI_ActiveWeaponData.WeaponRecordID, this, "OnWeaponSwap");
    this.m_cursorDevice = RefToWeakRef(Cast(WeakRefToRef(GetWidget("cursor_device"))));
    WeakRefToRef(this.m_cursorDevice).SetOpacity(0);
    this.m_isUnarmed = true;
    this.m_interactionDeviceState = false;
    this.m_upperBodyState = gamePSMUpperBodyStates.Default;
    this.m_interactionDeviceState = false;
    playerPuppet = RefToWeakRef(Cast(GetOwnerEntity()));
    playerSMDef = GetAllBlackboardDefs().PlayerStateMachine;
    if(ToBool(playerSMDef)) {
      bbPlayerSM = GetPSMBlackboard(WeakRefToRef(playerPuppet));
      if(ToBool(bbPlayerSM)) {
        this.m_upperBodyStateBlackboardId = bbPlayerSM.RegisterListenerInt(playerSMDef.UpperBody, this, "OnUpperBodyChange");
      };
    };
    CreateAnimations();
  }

  protected cb Bool OnUninitialize() {
    ref<IBlackboard> bbPlayerSM;
    ref<PlayerStateMachineDef> playerSMDef;
    wref<GameObject> playerPuppet;
    if(ToBool(this.m_bbUIData)) {
      this.m_bbUIData.UnregisterListenerVariant(GetAllBlackboardDefs().UIGameData.InteractionData, this.m_interactionBlackboardId);
    };
    if(ToBool(this.m_bbWeaponInfo)) {
      WeakRefToRef(this.m_bbWeaponInfo).UnregisterListenerVariant(GetAllBlackboardDefs().UI_ActiveWeaponData.WeaponRecordID, this.m_bbWeaponEventId);
    };
    playerPuppet = RefToWeakRef(Cast(GetOwnerEntity()));
    playerSMDef = GetAllBlackboardDefs().PlayerStateMachine;
    if(ToBool(playerSMDef)) {
      bbPlayerSM = GetPSMBlackboard(WeakRefToRef(playerPuppet));
      if(ToBool(bbPlayerSM)) {
        bbPlayerSM.UnregisterListenerInt(playerSMDef.UpperBody, this.m_upperBodyStateBlackboardId);
        this.m_upperBodyStateBlackboardId = 0;
      };
    };
  }

  protected cb Bool OnPlayerAttach(ref<GameObject> playerGameObject) {
    RegisterPSMListeners(playerGameObject);
  }

  protected cb Bool OnPlayerDetach(ref<GameObject> playerGameObject) {
    UnregisterPSMListeners(playerGameObject);
  }

  protected final void RegisterPSMListeners(ref<GameObject> playerPuppet) {
    ref<IBlackboard> bbSceneTier;
    bbSceneTier = GetPSMBlackboard(playerPuppet);
    if(ToBool(bbSceneTier)) {
      this.m_bbPlayerTierEventId = bbSceneTier.RegisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.SceneTier, this, "OnSceneTierChange");
    };
  }

  protected final void UnregisterPSMListeners(ref<GameObject> playerPuppet) {
    ref<IBlackboard> bbSceneTier;
    bbSceneTier = GetPSMBlackboard(playerPuppet);
    if(ToBool(bbSceneTier)) {
      bbSceneTier.UnregisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.SceneTier, this.m_bbPlayerTierEventId);
    };
  }

  protected cb Bool OnWeaponSwap(Variant value) {
    this.m_isUnarmed = FromVariant(value) == undefined();
  }

  protected cb Bool OnSceneTierChange(Int32 argTier) {
    this.m_sceneTier = ToEnum(argTier);
  }

  protected cb Bool OnUpperBodyChange(Int32 state) {
    this.m_upperBodyState = ToEnum(state);
    UpdateIsInteractingWithDevice();
  }

  protected cb Bool OnInteractionStateChange(Variant value) {
    bbUIInteractionData interactionData;
    interactionData = FromVariant(value);
    this.m_interactionDeviceState = interactionData.terminalInteractionActive;
    UpdateIsInteractingWithDevice();
  }

  private final void UpdateIsInteractingWithDevice() {
    Bool isInteractingWithDevice;
    isInteractingWithDevice = this.m_interactionDeviceState && this.m_upperBodyState != gamePSMUpperBodyStates.Aim;
    if(isInteractingWithDevice != this.m_wasLastInteractionWithDevice) {
      if(isInteractingWithDevice && this.m_isUnarmed || this.m_sceneTier != GameplayTier.Tier1_FullGameplay) {
        WeakRefToRef(this.m_cursorDevice).StopAllAnimations();
        WeakRefToRef(this.m_cursorDevice).SetOpacity(1);
      } else {
        if(isInteractingWithDevice) {
          WeakRefToRef(this.m_cursorDevice).StopAllAnimations();
          WeakRefToRef(this.m_cursorDevice).PlayAnimation(this.m_fadeInAnimation);
        } else {
          if(this.m_wasLastInteractionWithDevice) {
            WeakRefToRef(this.m_cursorDevice).StopAllAnimations();
            WeakRefToRef(this.m_cursorDevice).PlayAnimation(this.m_fadeOutAnimation);
          };
        };
      };
    };
    this.m_wasLastInteractionWithDevice = isInteractingWithDevice;
  }

  private final void CreateAnimations() {
    ref<inkAnimTransparency> fadeOutInterp;
    ref<inkAnimTransparency> fadeInInterp;
    this.m_fadeInAnimation = new inkAnimDef();
    fadeInInterp = new inkAnimTransparency();
    fadeInInterp.SetStartDelay(0.75);
    fadeInInterp.SetStartTransparency(0);
    fadeInInterp.SetEndTransparency(0.8500000238418579);
    fadeInInterp.SetDuration(0.20000000298023224);
    fadeInInterp.SetType(inkanimInterpolationType.Quadratic);
    fadeInInterp.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_fadeInAnimation.AddInterpolator(fadeInInterp);
    this.m_fadeOutAnimation = new inkAnimDef();
    fadeOutInterp = new inkAnimTransparency();
    fadeOutInterp.SetStartTransparency(1);
    fadeOutInterp.SetEndTransparency(0);
    fadeOutInterp.SetDuration(0.10000000149011612);
    fadeOutInterp.SetType(inkanimInterpolationType.Quadratic);
    fadeOutInterp.SetMode(inkanimInterpolationMode.EasyOut);
    this.m_fadeOutAnimation.AddInterpolator(fadeOutInterp);
  }
}
