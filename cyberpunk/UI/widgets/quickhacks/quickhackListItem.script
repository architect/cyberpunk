
public class QuickhacksListItemController extends ListItemController {

  [Default(QuickhacksListItemController, 0.2f))]
  private Float m_expandAnimationDuration;

  private edit inkImageRef m_icon;

  private edit inkTextRef m_description;

  private edit inkTextRef m_memoryValue;

  private edit inkCompoundRef m_memoryCells;

  private edit inkWidgetRef m_actionStateRoot;

  private edit inkTextRef m_actionStateText;

  private edit inkWidgetRef m_cooldownIcon;

  private edit inkTextRef m_cooldownValue;

  private edit inkWidgetRef m_descriptionSize;

  private edit inkWidgetRef m_costReductionArrow;

  private edit Float m_curveRadius;

  private ref<inkAnimProxy> m_selectedLoop;

  private CName m_currentAnimationName;

  private ref<inkAnimProxy> m_choiceAccepted;

  private ref<inkAnimController> m_resizeAnim;

  private wref<inkWidget> m_root;

  private ref<QuickhackData> m_data;

  private Bool m_isSelected;

  private Bool m_expanded;

  private Vector2 m_cachedDescriptionSize;

  private inkMargin m_defaultMargin;

  protected cb Bool OnInitialize() {
    this.m_root = GetRootWidget();
    RegisterToCallback("OnSelected", this, "OnSelected");
    RegisterToCallback("OnDeselected", this, "OnDeselected");
    SetVisible(this.m_description, false);
    AdjustToTextDescriptionSize(true);
  }

  protected cb Bool OnUninitialize() {
    UnregisterFromCallback("OnSelected", this, "OnSelected");
    UnregisterFromCallback("OnDeselected", this, "OnDeselected");
  }

  protected cb Bool OnDataChanged(ref<IScriptable> value) {
    ref<QuickhackDescriptionUpdate> evt;
    String description;
    this.m_data = Cast(value);
    this.m_currentAnimationName = "";
    if(IsValid(this.m_data.m_icon)) {
      RequestSetImage(this, this.m_icon, this.m_data.m_icon);
    };
    this.m_selectedLoop.UnregisterFromAllCallbacks(inkanimEventType.OnFinish);
    SetText(this.m_labelPathRef, this.m_data.m_title);
    if(IsStringValid(this.m_data.m_titleAlternative)) {
      SetupTitleFromChunks(this.m_data.m_title, this.m_data.m_titleAlternative);
    } else {
      SetText(this.m_labelPathRef, this.m_data.m_title);
    };
    SetCooldownVisibility(false);
    SetActionState();
    SetText(this.m_memoryValue, IntToString(this.m_data.m_cost));
    UpdateState();
    ChangeMargin();
    SetReductionArrowVisibility();
  }

  protected cb Bool OnQuickhackDescriptionUpdate(ref<QuickhackDescriptionUpdate> evt) {
    this.m_cachedDescriptionSize = GetDesiredSize(this.m_description);
    Expand(this.m_isSelected);
  }

  protected cb Bool OnSelected(wref<ListItemController> itemController) {
    this.m_isSelected = true;
    UpdateState();
  }

  protected cb Bool OnDeselected(wref<ListItemController> itemController) {
    this.m_isSelected = false;
    UpdateState();
  }

  private final void UpdateState() {
    if(this.m_selectedLoop.IsPlaying()) {
      this.m_selectedLoop.Stop();
    };
    if(this.m_isSelected) {
      if(this.m_data.m_isLocked) {
        WeakRefToRef(GetRootWidget()).SetState("LockedSelected");
        if(this.m_currentAnimationName != "lockedSelected") {
          PlayLibraryAnimation("loopSelected_out");
        };
        if(!IsChoiceAcceptedPlaying()) {
          this.m_selectedLoop = PlayLibraryAnimation("lockedSelected", GetAnimOptionsInfiniteLoop(inkanimLoopType.Cycle));
          this.m_currentAnimationName = "lockedSelected";
        };
      } else {
        WeakRefToRef(GetRootWidget()).SetState("Selected");
        if(this.m_currentAnimationName != "loopSelected") {
          PlayLibraryAnimation("lockedSelected_out");
        };
        if(!IsChoiceAcceptedPlaying()) {
          this.m_selectedLoop = PlayLibraryAnimation("loopSelected", GetAnimOptionsInfiniteLoop(inkanimLoopType.Cycle));
          this.m_currentAnimationName = "loopSelected";
        };
      };
    } else {
      if(this.m_data.m_isLocked) {
        WeakRefToRef(GetRootWidget()).SetState("Locked");
        this.m_selectedLoop = PlayLibraryAnimation("lockedSelected_out");
      } else {
        WeakRefToRef(GetRootWidget()).SetState("Default");
        this.m_selectedLoop = PlayLibraryAnimation("loopSelected_out");
      };
    };
  }

  private final void SetupTitleFromChunks(String title, String alternativeTitle) {
    ref<inkTextParams> textParams;
    textParams = new inkTextParams();
    textParams.AddLocalizedString("ALTNAME", alternativeTitle);
    textParams.AddLocalizedString("NAME", title);
    Cast(WeakRefToRef(Get(this.m_labelPathRef))).SetLocalizedTextString("LocKey#53370", textParams);
  }

  protected cb Bool OnUpdateAnimationState(ref<inkAnimProxy> e?) {
    UpdateState();
  }

  private final void SetActionState() {
    SetLocalizationKeyString(this.m_actionStateText, EActionInactivityResonToLocalizationString(this.m_data.m_actionState));
    if(this.m_data.m_actionState == EActionInactivityReson.Ready) {
      SetState(this.m_actionStateRoot, "Default");
    } else {
      SetState(this.m_actionStateRoot, "Locked");
    };
  }

  private final void SetReductionArrowVisibility() {
    if(this.m_data.m_cost < this.m_data.m_costRaw) {
      SetVisible(this.m_costReductionArrow, true);
    } else {
      SetVisible(this.m_costReductionArrow, false);
    };
  }

  public final void UpdateCooldown(Float cooldown) {
    SetText(this.m_cooldownValue, FloatToStringPrec(cooldown, 4));
    SetCooldownVisibility(true);
  }

  public final void SetCooldownVisibility(Bool isVisible) {
    if(IsVisible(this.m_cooldownValue) != isVisible) {
      if(isVisible) {
        this.m_data.m_actionState = EActionInactivityReson.Recompilation;
      };
      SetActionState();
      SetVisible(this.m_cooldownValue, isVisible);
      SetVisible(this.m_cooldownIcon, isVisible);
    };
  }

  private final void Expand(Bool value, Bool force?) {
    Vector2 animSizeFrom;
    Vector2 animSizeTo;
    if(this.m_expanded == value && !force) {
      return ;
    };
    this.m_expanded = value;
    SetVisible(this.m_description, this.m_expanded);
    if(this.m_expanded) {
      SetVisible(this.m_description, true);
    };
    if(ToBool(this.m_resizeAnim)) {
      this.m_resizeAnim.Stop();
    };
    this.m_resizeAnim = new inkAnimController();
    if(this.m_expanded) {
      animSizeFrom = new Vector2(0,0);
      animSizeTo = this.m_cachedDescriptionSize;
      this.m_resizeAnim.Select(WeakRefToRef(Get(this.m_descriptionSize))).Interpolate("size", ToVariant(animSizeFrom), ToVariant(animSizeTo)).Duration(this.m_expandAnimationDuration).Type(inkanimInterpolationType.Exponential).Mode(inkanimInterpolationMode.EasyOut);
      this.m_resizeAnim.Play();
      this.m_resizeAnim.RegisterToCallback(inkanimEventType.OnFinish, this, "OnResizingFinished");
    } else {
      animSizeFrom = this.m_cachedDescriptionSize;
      animSizeTo = new Vector2(0,0);
      this.m_resizeAnim.Select(WeakRefToRef(Get(this.m_descriptionSize))).Interpolate("size", ToVariant(animSizeFrom), ToVariant(animSizeTo)).Duration(this.m_expandAnimationDuration).Type(inkanimInterpolationType.Exponential).Mode(inkanimInterpolationMode.EasyOut);
      this.m_resizeAnim.Play();
      this.m_resizeAnim.RegisterToCallback(inkanimEventType.OnFinish, this, "OnResizingFinished");
    };
  }

  protected cb Bool OnResizingFinished(ref<inkAnimProxy> anim) {
    if(!this.m_expanded) {
      SetVisible(this.m_description, false);
    };
    AdjustToTextDescriptionSize(true);
  }

  private final void ShowMemoryCell(Int32 value) {
    Int32 i;
    RemoveAllChildren(this.m_memoryCells);
    i = 0;
    while(i < 1) {
      SpawnFromLocal(RefToWeakRef(Cast(WeakRefToRef(Get(this.m_memoryCells)))), "memory_cell_small");
      i += 1;
    };
  }

  private final void AdjustToTextDescriptionSize(Bool value) {
    if(value) {
      SetVAlign(this.m_description, inkEVerticalAlign.Top);
      SetHAlign(this.m_description, inkEHorizontalAlign.Left);
      SetVAlign(this.m_descriptionSize, inkEVerticalAlign.Fill);
      SetHAlign(this.m_descriptionSize, inkEHorizontalAlign.Fill);
    } else {
      SetVAlign(this.m_description, inkEVerticalAlign.Fill);
      SetHAlign(this.m_description, inkEHorizontalAlign.Fill);
      SetVAlign(this.m_descriptionSize, inkEVerticalAlign.Top);
      SetHAlign(this.m_descriptionSize, inkEHorizontalAlign.Left);
    };
  }

  private final void ChangeMargin() {
    Float offset;
    Float index;
    Float angleRange;
    Float anglePerItem;
    Float angleOffset;
    index = Cast(GetIndex());
    angleRange = Cast(Min(this.m_data.m_maxListSize, 8)) * 180 / 8;
    anglePerItem = angleRange / Cast(this.m_data.m_maxListSize);
    angleOffset = AbsF(angleRange * 0.5 - 90) + anglePerItem * 0.5;
    offset = this.m_curveRadius * SinF(Deg2Rad(angleOffset + anglePerItem * index));
    WeakRefToRef(this.m_root).SetMargin(new inkMargin(-offset,0,offset,0));
  }

  public final void PlayChoiceAcceptedAnimation() {
    if(!IsChoiceAcceptedPlaying()) {
      if(this.m_data.m_isLocked) {
        this.m_choiceAccepted = PlayLibraryAnimation("click_locked");
      } else {
        this.m_choiceAccepted = PlayLibraryAnimation("choiceAccept");
      };
      this.m_choiceAccepted.RegisterToCallback(inkanimEventType.OnFinish, this, "OnUpdateAnimationState");
      if(this.m_currentAnimationName != "lockedSelected") {
        PlayLibraryAnimation("loopSelected_out");
      };
      if(this.m_currentAnimationName != "loopSelected") {
        PlayLibraryAnimation("lockedSelected_out");
      };
    };
  }

  private final Bool IsChoiceAcceptedPlaying() {
    return this.m_choiceAccepted.IsPlaying();
  }
}
