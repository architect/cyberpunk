
public class TargetHitIndicatorGameController extends inkGameController {

  private ref<inkAnimProxy> m_currentAnim;

  private ref<inkAnimProxy> m_bonusAnim;

  private wref<inkWidget> m_currentAnimWidget;

  private Int32 m_currentPriority;

  private wref<TargetHitIndicatorLogicController> m_currentController;

  private wref<TargetHitIndicatorLogicController> m_damageController;

  private wref<TargetHitIndicatorLogicController> m_defeatController;

  private wref<TargetHitIndicatorLogicController> m_killController;

  private wref<TargetHitIndicatorLogicController> m_bonusController;

  private Uint32 m_damageListBlackboardId;

  private Uint32 m_killListBlackboardId;

  private Uint32 m_indicatorEnabledBlackboardId;

  private Uint32 m_weaponSwayBlackboardId;

  private Uint32 m_weaponChangeBlackboardId;

  private Uint32 m_aimingStatusBlackboardId;

  private Uint32 m_zoomLevelBlackboardId;

  private wref<GameObject> m_realOwner;

  private Bool m_hitIndicatorEnabled;

  private wref<GameObject> m_entityHit;

  private wref<inkWidget> m_rootWidget;

  private wref<PlayerPuppet> m_player;

  private Vector2 m_currentSway;

  public Float m_currentWeaponZoom;

  public Bool m_weaponZoomNeedsUpdate;

  private Float m_currentZoomLevel;

  private ref<HitIndicatorWeaponZoomListener> m_weaponZoomListener;

  private StatsObjectID m_weaponID;

  private Bool m_isAimingDownSights;

  protected cb Bool OnInitialize() {
    ref<IBlackboard> damageInfoBB;
    ref<IBlackboard> weaponInfoBB;
    this.m_rootWidget = GetRootWidget();
    this.m_realOwner = RefToWeakRef(Cast(GetOwnerEntity()));
    this.m_damageController = RefToWeakRef(SpawnIndicator("Damage"));
    this.m_defeatController = RefToWeakRef(SpawnIndicator("Defeat"));
    this.m_killController = RefToWeakRef(SpawnIndicator("Kill"));
    this.m_bonusController = RefToWeakRef(SpawnIndicator("Bonus"));
    damageInfoBB = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_DamageInfo);
    this.m_damageListBlackboardId = damageInfoBB.RegisterListenerVariant(GetAllBlackboardDefs().UI_DamageInfo.DamageList, this, "OnDamageAdded");
    this.m_killListBlackboardId = damageInfoBB.RegisterListenerVariant(GetAllBlackboardDefs().UI_DamageInfo.KillList, this, "OnKillAdded");
    this.m_indicatorEnabledBlackboardId = damageInfoBB.RegisterListenerBool(GetAllBlackboardDefs().UI_DamageInfo.HitIndicatorEnabled, this, "OnHitIndicatorEnabledChanged");
    this.m_hitIndicatorEnabled = damageInfoBB.GetBool(GetAllBlackboardDefs().UI_DamageInfo.HitIndicatorEnabled);
    weaponInfoBB = GetBlackboardSystem().Get(GetAllBlackboardDefs().UIGameData);
    this.m_weaponSwayBlackboardId = weaponInfoBB.RegisterListenerVector2(GetAllBlackboardDefs().UIGameData.WeaponSway, this, "OnSway");
    this.m_weaponChangeBlackboardId = weaponInfoBB.RegisterListenerVariant(GetAllBlackboardDefs().UIGameData.RightWeaponRecordID, this, "OnWeaponChange");
  }

  protected cb Bool OnPlayerAttach(ref<GameObject> player) {
    ref<IBlackboard> playerStateMachineBB;
    ref<StatsSystem> stats;
    ref<WeaponObject> weapon;
    this.m_realOwner = RefToWeakRef(player);
    this.m_player = RefToWeakRef(Cast(player));
    if(ToBool(this.m_player)) {
      playerStateMachineBB = WeakRefToRef(this.m_player).GetPlayerStateMachineBlackboard();
      this.m_aimingStatusBlackboardId = playerStateMachineBB.RegisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody, this, "OnAimStatusChange");
      this.m_zoomLevelBlackboardId = playerStateMachineBB.RegisterListenerFloat(GetAllBlackboardDefs().PlayerStateMachine.ZoomLevel, this, "OnZoomLevelChange");
      this.m_isAimingDownSights = playerStateMachineBB.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == ToInt(gamePSMUpperBodyStates.Aim);
      if(!ToBool(this.m_weaponZoomListener)) {
        this.m_weaponZoomListener = new HitIndicatorWeaponZoomListener();
        this.m_weaponZoomListener.m_gameController = RefToWeakRef(this);
      };
      stats = GetStatsSystem(WeakRefToRef(this.m_player).GetGame());
      weapon = Cast(GetTransactionSystem(WeakRefToRef(this.m_player).GetGame()).GetItemInSlot(WeakRefToRef(this.m_player), "AttachmentSlots.WeaponRight"));
      if(ToBool(weapon)) {
        this.m_weaponID = WeakRefToRef(weapon.GetItemData()).GetStatsObjectID();
        this.m_weaponZoomListener.SetStatType(gamedataStatType.ZoomLevel);
        stats.RegisterListener(this.m_weaponID, this.m_weaponZoomListener);
        this.m_weaponZoomNeedsUpdate = true;
      };
    };
  }

  protected cb Bool OnUninitialize() {
    ref<IBlackboard> damageInfoBB;
    ref<IBlackboard> weaponInfoBB;
    damageInfoBB = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_DamageInfo);
    if(this.m_damageListBlackboardId > 0) {
      damageInfoBB.UnregisterListenerVariant(GetAllBlackboardDefs().UI_DamageInfo.DamageList, this.m_damageListBlackboardId);
      this.m_damageListBlackboardId = 0;
    };
    if(this.m_killListBlackboardId > 0) {
      damageInfoBB.UnregisterListenerVariant(GetAllBlackboardDefs().UI_DamageInfo.KillList, this.m_killListBlackboardId);
      this.m_killListBlackboardId = 0;
    };
    if(this.m_indicatorEnabledBlackboardId > 0) {
      damageInfoBB.UnregisterListenerBool(GetAllBlackboardDefs().UI_DamageInfo.HitIndicatorEnabled, this.m_indicatorEnabledBlackboardId);
      this.m_indicatorEnabledBlackboardId = 0;
    };
    weaponInfoBB = GetBlackboardSystem().Get(GetAllBlackboardDefs().UIGameData);
    if(this.m_weaponSwayBlackboardId > 0) {
      weaponInfoBB.UnregisterListenerVector2(GetAllBlackboardDefs().UIGameData.WeaponSway, this.m_weaponSwayBlackboardId);
      this.m_weaponSwayBlackboardId = 0;
    };
    if(this.m_weaponChangeBlackboardId > 0) {
      weaponInfoBB.UnregisterListenerVariant(GetAllBlackboardDefs().UIGameData.RightWeaponRecordID, this.m_weaponChangeBlackboardId);
      this.m_weaponChangeBlackboardId = 0;
    };
  }

  protected cb Bool OnPlayerDetach(ref<GameObject> player) {
    ref<IBlackboard> playerStateMachineBB;
    if(ToBool(this.m_player)) {
      playerStateMachineBB = WeakRefToRef(this.m_player).GetPlayerStateMachineBlackboard();
      if(this.m_aimingStatusBlackboardId > 0) {
        playerStateMachineBB.UnregisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody, this.m_aimingStatusBlackboardId);
        this.m_aimingStatusBlackboardId = 0;
      };
      if(this.m_zoomLevelBlackboardId > 0) {
        playerStateMachineBB.UnregisterListenerFloat(GetAllBlackboardDefs().PlayerStateMachine.ZoomLevel, this.m_zoomLevelBlackboardId);
        this.m_zoomLevelBlackboardId = 0;
      };
      GetStatsSystem(WeakRefToRef(this.m_player).GetGame()).UnregisterListener(this.m_weaponID, this.m_weaponZoomListener);
      this.m_player = null;
    };
  }

  private final ref<TargetHitIndicatorLogicController> SpawnIndicator(CName type) {
    ref<inkWidget> newInkWidget;
    newInkWidget = WeakRefToRef(SpawnFromLocal(GetRootWidget(), type));
    newInkWidget.SetAnchor(inkEAnchor.Centered);
    newInkWidget.SetAnchorPoint(0.5, 0.5);
    return Cast(WeakRefToRef(newInkWidget.GetController()));
  }

  protected cb Bool OnDamageAdded(Variant value) {
    array<DamageInfo> damageList;
    Int32 i;
    wref<GameObject> hitEntity;
    damageList = FromVariant(value);
    i = 0;
    while(i < Size(damageList)) {
      if(WeakRefToRef(damageList[i].entityHit) != null && WeakRefToRef(this.m_realOwner) == WeakRefToRef(damageList[i].instigator) && ShouldShowDamage(damageList[i])) {
        if(ShouldShowBonus(damageList[i])) {
          ShowBonus();
        };
        hitEntity = damageList[i].entityHit;
      } else {
        i += 1;
      };
    };
    if(WeakRefToRef(hitEntity) != null) {
      Show(hitEntity, false);
    };
  }

  protected cb Bool OnKillAdded(Variant value) {
    array<KillInfo> killList;
    Int32 i;
    wref<GameObject> killedEntity;
    gameKillType killType;
    killList = FromVariant(value);
    i = 0;
    while(i < Size(killList)) {
      if(WeakRefToRef(killList[i].victimEntity) != null && WeakRefToRef(this.m_realOwner) == WeakRefToRef(killList[i].killerEntity)) {
        killedEntity = killList[i].victimEntity;
        killType = killList[i].killType;
        Show(killedEntity, true, killType);
      };
      i += 1;
    };
  }

  private final Bool ShouldShowDamage(DamageInfo damageInfo) {
    Int32 i;
    if(damageInfo.damageValue == 0) {
      return false;
    };
    i = 0;
    while(i < Size(damageInfo.userData.flags)) {
      if(damageInfo.userData.flags[i].flag == hitFlag.FriendlyFire || damageInfo.userData.flags[i].flag == hitFlag.ImmortalTarget || damageInfo.userData.flags[i].flag == hitFlag.DealNoDamage || damageInfo.userData.flags[i].flag == hitFlag.DontShowDamageFloater) {
        return false;
      };
      i += 1;
    };
    return true;
  }

  private final Bool ShouldShowBonus(DamageInfo damageInfo) {
    Int32 i;
    i = 0;
    while(i < Size(damageInfo.userData.flags)) {
      if(damageInfo.userData.flags[i].flag == hitFlag.Headshot || damageInfo.userData.flags[i].flag == hitFlag.WeakspotHit) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final void Show(wref<GameObject> entity, Bool isDead, gameKillType killType?) {
    if(this.m_hitIndicatorEnabled) {
      if(isDead) {
        if(killType == gameKillType.Normal) {
          this.m_currentController = this.m_killController;
        } else {
          if(killType == gameKillType.Defeat) {
            this.m_currentController = this.m_defeatController;
          };
        };
      } else {
        this.m_currentController = this.m_damageController;
      };
      this.m_entityHit = entity;
      if(this.m_currentPriority < WeakRefToRef(this.m_currentController).m_animationPriority) {
        if(ToBool(this.m_currentAnim) && this.m_currentAnim.IsPlaying()) {
          WeakRefToRef(this.m_currentAnimWidget).SetVisible(false);
          this.m_currentAnim.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnAnimFinished");
          this.m_currentAnim.Stop();
        };
        PlayAnimation();
      };
    };
  }

  public final void PlayAnimation() {
    this.m_currentAnimWidget = WeakRefToRef(this.m_currentController).GetRootWidget();
    WeakRefToRef(this.m_currentAnimWidget).SetOpacity(1);
    WeakRefToRef(this.m_currentAnimWidget).SetVisible(true);
    this.m_currentAnim = WeakRefToRef(this.m_currentController).PlayLibraryAnimation(WeakRefToRef(this.m_currentController).m_animName);
    this.m_currentAnim.RegisterToCallback(inkanimEventType.OnFinish, this, "OnAnimFinished");
    this.m_currentPriority = WeakRefToRef(this.m_currentController).m_animationPriority;
  }

  protected cb Bool OnAnimFinished(ref<inkAnimProxy> anim) {
    WeakRefToRef(this.m_currentAnimWidget).SetVisible(false);
    this.m_currentAnim.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnAnimFinished");
    this.m_currentPriority = 0;
  }

  private final void ShowBonus() {
    wref<inkWidget> bonusAnimWidget;
    if(this.m_hitIndicatorEnabled) {
      if(ToBool(this.m_bonusAnim) && this.m_bonusAnim.IsPlaying()) {
        this.m_bonusAnim.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnBonusAnimFinished");
        this.m_bonusAnim.Stop();
      };
      bonusAnimWidget = WeakRefToRef(this.m_bonusController).GetRootWidget();
      WeakRefToRef(bonusAnimWidget).SetOpacity(1);
      WeakRefToRef(bonusAnimWidget).SetVisible(true);
      this.m_bonusAnim = WeakRefToRef(this.m_bonusController).PlayLibraryAnimation(WeakRefToRef(this.m_bonusController).m_animName);
      this.m_bonusAnim.RegisterToCallback(inkanimEventType.OnFinish, this, "OnBonusAnimFinished");
    };
  }

  protected cb Bool OnBonusAnimFinished(ref<inkAnimProxy> anim) {
    WeakRefToRef(WeakRefToRef(this.m_bonusController).GetRootWidget()).SetVisible(false);
    this.m_bonusAnim.UnregisterFromCallback(inkanimEventType.OnFinish, this, "OnBonusAnimFinished");
  }

  protected cb Bool OnSway(Vector2 pos) {
    this.m_currentSway = pos;
    UpdateWidgetPosition();
  }

  protected cb Bool OnAimStatusChange(Int32 value) {
    this.m_isAimingDownSights = value == ToInt(gamePSMUpperBodyStates.Aim);
    if(ToBool(this.m_player) && this.m_weaponZoomNeedsUpdate) {
      this.m_currentWeaponZoom = GetStatsSystem(WeakRefToRef(this.m_player).GetGame()).GetStatValue(this.m_weaponID, gamedataStatType.ZoomLevel);
      this.m_weaponZoomNeedsUpdate = false;
    };
    UpdateWidgetPosition();
  }

  protected cb Bool OnZoomLevelChange(Float value) {
    this.m_currentZoomLevel = value;
    UpdateWidgetPosition();
  }

  private final void UpdateWidgetPosition() {
    Float multiplier;
    Float rFov;
    if(this.m_isAimingDownSights) {
      if(ToBool(this.m_player)) {
        rFov = Deg2Rad(GetCameraSystem(WeakRefToRef(this.m_player).GetGame()).GetActiveCameraFOV());
        if(rFov == 0) {
          rFov = 0.8899999856948853;
        };
      } else {
        rFov = 0.8899999856948853;
      };
      if(this.m_currentZoomLevel >= 2) {
        multiplier = this.m_currentZoomLevel / TanF(rFov * 0.5);
      } else {
        multiplier = this.m_currentWeaponZoom / TanF(rFov * 0.5);
      };
      WeakRefToRef(this.m_rootWidget).SetMargin(new inkMargin(-19.200000762939453 * this.m_currentSway.X * multiplier,-18.899999618530273 * this.m_currentSway.Y * multiplier,0,0));
    } else {
      WeakRefToRef(this.m_rootWidget).SetMargin(new inkMargin(0,0,0,0));
    };
  }

  protected cb Bool OnWeaponChange(Variant value) {
    ref<StatsSystem> stats;
    ref<WeaponObject> weapon;
    if(ToBool(this.m_player)) {
      stats = GetStatsSystem(WeakRefToRef(this.m_player).GetGame());
      stats.UnregisterListener(this.m_weaponID, this.m_weaponZoomListener);
      weapon = Cast(GetTransactionSystem(WeakRefToRef(this.m_player).GetGame()).GetItemInSlot(WeakRefToRef(this.m_player), "AttachmentSlots.WeaponRight"));
      if(ToBool(weapon)) {
        this.m_weaponID = WeakRefToRef(weapon.GetItemData()).GetStatsObjectID();
        this.m_weaponZoomListener.SetStatType(gamedataStatType.ZoomLevel);
        stats.RegisterListener(this.m_weaponID, this.m_weaponZoomListener);
        this.m_weaponZoomNeedsUpdate = true;
      };
    };
  }

  protected cb Bool OnHitIndicatorEnabledChanged(Bool value) {
    this.m_hitIndicatorEnabled = value;
  }
}

public class TargetHitIndicatorLogicController extends inkLogicController {

  public edit CName m_animName;

  public edit Int32 m_animationPriority;

  protected cb Bool OnInitialize() {
    WeakRefToRef(GetRootWidget()).SetVisible(false);
  }
}

public class HitIndicatorWeaponZoomListener extends ScriptStatsListener {

  public wref<TargetHitIndicatorGameController> m_gameController;

  public void OnStatChanged(StatsObjectID ownerID, gamedataStatType statType, Float diff, Float total) {
    WeakRefToRef(this.m_gameController).m_currentWeaponZoom = total;
    WeakRefToRef(this.m_gameController).m_weaponZoomNeedsUpdate = false;
  }
}
