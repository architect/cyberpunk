
public class InventoryWeaponItemChooser extends InventoryGenericItemChooser {

  protected edit inkCompoundRef m_scopeRootContainer;

  protected edit inkCompoundRef m_magazineRootContainer;

  protected edit inkCompoundRef m_silencerRootContainer;

  protected edit inkCompoundRef m_scopeContainer;

  protected edit inkCompoundRef m_magazineContainer;

  protected edit inkCompoundRef m_silencerContainer;

  protected edit inkTextRef m_attachmentsLabel;

  protected edit inkWidgetRef m_attachmentsContainer;

  protected edit inkTextRef m_softwareModsLabel;

  protected edit inkWidgetRef m_softwareModsPush;

  protected edit inkWidgetRef m_softwareModsContainer;

  protected array<InventoryItemAttachments> GetSlots() {
    return GetMods(this.itemDisplay.GetItemData(), true);
  }

  protected void RebuildSlots() {
    RebuildSlots();
    RebuildParts();
  }

  protected Bool ForceDisplayLabel() {
    array<InventoryItemAttachments> itemParts;
    itemParts = GetParts(this.itemDisplay.GetItemData());
    return Size(itemParts) > 0;
  }

  private final void UpdateModsLabel(array<InventoryItemAttachments> parts) {
    Bool hasMods;
    Bool hasParts;
    hasMods = GetAttachmentsSize(this.itemDisplay.GetItemData()) - Size(parts) > 0;
    hasParts = Size(parts) > 0;
    SetVisible(this.m_attachmentsLabel, hasParts);
    SetVisible(this.m_attachmentsContainer, hasParts);
    SetVisible(this.m_softwareModsLabel, hasMods);
    SetVisible(this.m_softwareModsContainer, hasMods);
    SetVisible(this.m_softwareModsPush, hasParts);
  }

  private final inkCompoundRef GetRootSlotContainerFromType(WeaponPartType partType) {
    inkCompoundRef emptyResult;
    switch(partType) {
      case WeaponPartType.Silencer:
        return this.m_silencerRootContainer;
      case WeaponPartType.Magazine:
        return this.m_magazineRootContainer;
      case WeaponPartType.Scope:
        return this.m_scopeRootContainer;
    };
    return emptyResult;
  }

  private final inkCompoundRef GetSlotContainerFromType(WeaponPartType partType) {
    inkCompoundRef emptyResult;
    switch(partType) {
      case WeaponPartType.Silencer:
        return this.m_silencerContainer;
      case WeaponPartType.Magazine:
        return this.m_magazineContainer;
      case WeaponPartType.Scope:
        return this.m_scopeContainer;
    };
    return emptyResult;
  }

  private final CName GetAtlasPartFromType(WeaponPartType partType) {
    switch(partType) {
      case WeaponPartType.Silencer:
        return "mod_silencer-1";
      case WeaponPartType.Magazine:
        return "mod_magazine-1";
      case WeaponPartType.Scope:
        return "mod_scope-1";
    };
    return "item_grid_accent";
  }

  private final array<WeaponPartType> GetAllPartsTypes() {
    array<WeaponPartType> result;
    Push(result, WeaponPartType.Silencer);
    Push(result, WeaponPartType.Magazine);
    Push(result, WeaponPartType.Scope);
    return result;
  }

  private final InventoryItemAttachments GetPartDataByType(array<InventoryItemAttachments> parts, WeaponPartType type) {
    Int32 i;
    InventoryItemAttachments emptyResult;
    i = 0;
    while(i < Size(parts)) {
      if(GetPartType(parts[i]) == type) {
        return parts[i];
      };
      i += 1;
    };
    return emptyResult;
  }

  protected void RebuildParts() {
    Int32 i;
    Int32 j;
    InventoryItemAttachments part;
    array<InventoryItemAttachments> itemParts;
    array<WeaponPartType> allParts;
    ref<InventoryItemDisplayController> slot;
    inkCompoundRef slotContainer;
    inkCompoundRef slotRootContainer;
    InventoryItemData inventoryItemData;
    wref<gameItemData> itemData;
    inventoryItemData = this.itemDisplay.GetItemData();
    itemData = RefToWeakRef(GetGameItemData(inventoryItemData));
    allParts = GetAllPartsTypes();
    itemParts = GetParts(inventoryItemData);
    UpdateModsLabel(itemParts);
    i = 0;
    while(i < Size(allParts)) {
      slot = null;
      part = GetPartDataByType(itemParts, allParts[i]);
      slotRootContainer = GetRootSlotContainerFromType(allParts[i]);
      slotContainer = GetSlotContainerFromType(allParts[i]);
      if(IsValid(part.SlotID)) {
        SetVisible(slotRootContainer, true);
        j = 0;
        while(j < GetNumChildren(slotContainer)) {
          if(ToBool(Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(slotContainer, j)).GetController())))) {
            slot = Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(slotContainer, j)).GetController()));
          };
          j += 1;
        };
        if(!ToBool(slot)) {
          slot = Cast(WeakRefToRef(SpawnCommonSlotController(this, slotContainer, GetSlotDisplayToSpawn())));
          WeakRefToRef(slot.GetRootWidget()).RegisterToCallback("OnRelease", this, "OnItemInventoryClick");
          WeakRefToRef(slot.GetRootWidget()).RegisterToCallback("OnHoverOver", this, "OnInventoryItemHoverOver");
          WeakRefToRef(slot.GetRootWidget()).RegisterToCallback("OnHoverOut", this, "OnInventoryItemHoverOut");
        };
        slot.SetDefaultShadowIcon(GetAtlasPartFromType(allParts[i]));
        slot.SetParentItem(itemData);
        slot.Setup(this.inventoryDataManager, part.ItemData, part.SlotID, ItemDisplayContext.Attachment, true);
      } else {
        SetVisible(slotRootContainer, false);
      };
      i += 1;
    };
  }

  protected CName GetDisplayToSpawn() {
    return "weaponDisplay";
  }

  protected CName GetIntroAnimation() {
    return "weaponItemChoser_intro";
  }
}
