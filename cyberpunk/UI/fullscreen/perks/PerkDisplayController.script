
public class PerkDisplayController extends inkButtonController {

  protected edit inkTextRef m_levelText;

  protected edit inkImageRef m_icon;

  protected edit inkTextRef m_fluffText;

  protected edit inkWidgetRef m_requiredTrainerIcon;

  protected edit inkTextRef m_requiredPointsText;

  protected ref<BasePerkDisplayData> m_displayData;

  protected ref<PlayerDevelopmentDataManager> m_dataManager;

  protected wref<PlayerDevelopmentData> m_playerDevelopmentData;

  protected Bool m_recentlyPurchased;

  protected Bool m_holdStarted;

  protected Bool m_isTrait;

  protected Bool m_wasLocked;

  protected Int32 m_index;

  protected ref<inkAnimProxy> m_cool_in_proxy;

  protected ref<inkAnimProxy> m_cool_out_proxy;

  protected cb Bool OnInitialize() {
    RegisterToCallback("OnRelease", this, "OnPerkDisplayClick");
    RegisterToCallback("OnHold", this, "OnPerkDisplayHold");
    RegisterToCallback("OnHoverOver", this, "OnPerkItemHoverOver");
    RegisterToCallback("OnHoverOut", this, "OnPerkItemHoverOut");
  }

  protected cb Bool OnUninitialize() {
    UnregisterFromCallback("OnRelease", this, "OnPerkDisplayClick");
    UnregisterFromCallback("OnHold", this, "OnPerkDisplayHold");
    UnregisterFromCallback("OnHoverOver", this, "OnPerkItemHoverOver");
    UnregisterFromCallback("OnHoverOut", this, "OnPerkItemHoverOut");
  }

  public final void Setup(ref<BasePerkDisplayData> displayData, ref<PlayerDevelopmentDataManager> dataManager, Int32 index?) {
    this.m_playerDevelopmentData = dataManager.GetPlayerDevelopmentData();
    this.m_index = index;
    this.m_isTrait = Cast(displayData) != null;
    this.m_dataManager = dataManager;
    UpdateLayout(displayData);
    CheckRevealAnimation(displayData, this.m_displayData);
    this.m_displayData = displayData;
  }

  public final void CheckRevealAnimation(ref<BasePerkDisplayData> newDisplayData, ref<BasePerkDisplayData> oldDisplayData) {
    Bool isFirstTime;
    ref<UnlimitedUnlocked> unlimitedUnlocked;
    if(ToBool(oldDisplayData)) {
      isFirstTime = oldDisplayData.m_proficiency != newDisplayData.m_proficiency;
    } else {
      isFirstTime = true;
    };
    if(isFirstTime) {
      this.m_wasLocked = newDisplayData.m_locked;
    } else {
      this.m_wasLocked = oldDisplayData.m_locked;
      if(this.m_wasLocked && !newDisplayData.m_locked) {
        if(this.m_isTrait) {
          unlimitedUnlocked = new UnlimitedUnlocked();
          QueueEvent(unlimitedUnlocked);
        } else {
          PlayLibraryAnimation("reveal_perk");
        };
      };
    };
  }

  private final void UpdateLayout(ref<BasePerkDisplayData> data) {
    CName state;
    ref<PerkDisplayData> perkDisplayData;
    wref<inkWidget> root;
    Bool isDataNull;
    root = GetRootWidget();
    isDataNull = data == null;
    if(isDataNull == WeakRefToRef(root).IsVisible()) {
      WeakRefToRef(root).SetVisible(!isDataNull);
    };
    if(isDataNull) {
      return ;
    };
    state = "Default";
    if(data.m_locked) {
      state = "Locked";
    } else {
      if(data.m_level == data.m_maxLevel) {
        state = "Maxed";
      } else {
        if(data.m_level > 0) {
          state = "Bought";
        };
      };
    };
    WeakRefToRef(GetRootWidget()).SetState(state);
    RequestSetImage(this, this.m_icon, "UIIcon." + ToString(data.m_iconID));
    SetVisible(this.m_icon, data.m_iconID != "");
    if(!this.m_isTrait) {
      UpdateLayout(Cast(data));
    } else {
      UpdateLayout(Cast(data));
    };
  }

  private final void UpdateLayout(ref<PerkDisplayData> data) {
    ref<PerkArea_Record> perkAreaRecord;
    Int32 numPointsRequired;
    Bool requiresPoints;
    Bool requiresMastery;
    ref<inkTextParams> levelParams;
    ref<StatPrereq_Record> statPrereqRecord;
    gamedataStatType statType;
    Float statValue;
    numPointsRequired = 0;
    requiresPoints = false;
    requiresMastery = false;
    levelParams = new inkTextParams();
    perkAreaRecord = WeakRefToRef(this.m_playerDevelopmentData).GetPerkAreaRecord(data.m_area);
    statPrereqRecord = Cast(WeakRefToRef(perkAreaRecord.Requirement()));
    statType = ToEnum(Cast(EnumValueFromName("gamedataStatType", statPrereqRecord.StatType())));
    statValue = GetStatsSystem(WeakRefToRef(this.m_dataManager.GetPlayer()).GetGame()).GetStatValue(Cast(WeakRefToRef(this.m_dataManager.GetPlayer()).GetEntityID()), statType);
    numPointsRequired = Cast(statPrereqRecord.ValueToCheck()) - Cast(statValue);
    requiresPoints = numPointsRequired > 0;
    SetVisible(this.m_requiredPointsText, requiresPoints);
    SetText(this.m_requiredPointsText, IntToString(numPointsRequired));
    requiresMastery = !WeakRefToRef(this.m_playerDevelopmentData).IsPerkAreaMasteryReqMet(perkAreaRecord);
    SetVisible(this.m_requiredTrainerIcon, requiresMastery);
    levelParams.AddString("level", IntToString(data.m_level));
    levelParams.AddString("maxLevel", IntToString(data.m_maxLevel));
    SetTextParameters(this.m_levelText, levelParams);
    SetText(this.m_fluffText, "FNC_" + IntToString(GetFluffRand(data)) + " " + IntToString(GetFluffRand(data, 3425)));
  }

  private final void UpdateLayout(ref<TraitDisplayData> data) {
    ref<Trait_Record> traitRecord;
    Int32 numPointsRequired;
    Bool requiresPoints;
    ref<inkTextParams> levelParams;
    ref<StatPrereq_Record> statPrereqRecord;
    ref<StatPrereq_Record> prereq;
    gamedataProficiencyType proficiencyType;
    numPointsRequired = 0;
    requiresPoints = false;
    levelParams = new inkTextParams();
    prereq = Cast(WeakRefToRef(traitRecord.Requirement()));
    proficiencyType = ToEnum(Cast(EnumValueFromName("gamedataProficiencyType", statPrereqRecord.StatType())));
    numPointsRequired = Cast(prereq.ValueToCheck()) - WeakRefToRef(this.m_playerDevelopmentData).GetProficiencyLevel(proficiencyType);
    traitRecord = GetTraitRecord(data.m_type);
    requiresPoints = numPointsRequired > 0;
    SetVisible(this.m_requiredPointsText, requiresPoints);
    SetText(this.m_requiredPointsText, IntToString(numPointsRequired));
    levelParams.AddString("level", IntToString(data.m_level));
    SetTextParameters(this.m_levelText, levelParams);
    SetText(this.m_fluffText, "FNC_" + IntToString(GetFluffRand(data)) + " " + IntToString(GetFluffRand(data, 6327)));
  }

  protected final Int32 GetFluffRand(ref<PerkDisplayData> perkData, Int32 offset?) {
    return Cast(RandNoiseF(ToInt(perkData.m_proficiency) * 1000 + ToInt(perkData.m_area) * 100 + ToInt(perkData.m_type) + offset, 1000, 9999));
  }

  protected final Int32 GetFluffRand(ref<TraitDisplayData> traitData, Int32 offset?) {
    return Cast(RandNoiseF(traitData.m_level * 100 + ToInt(traitData.m_type) + offset, 1000, 9999));
  }

  private final void Upgrade() {
    if(!this.m_isTrait) {
      this.m_dataManager.UpgradePerk(Cast(this.m_displayData));
    } else {
      this.m_dataManager.UpgradeTrait(Cast(this.m_displayData));
    };
  }

  protected cb Bool OnUnlimitedUnlocked(ref<UnlimitedUnlocked> evt) {
    if(this.m_isTrait) {
      PlayLibraryAnimation("reveal_unlimited_perk");
    };
  }

  protected cb Bool OnPerkItemHoverOver(ref<inkPointerEvent> evt) {
    ref<PerkHoverOverEvent> hoverOverEvent;
    hoverOverEvent = new PerkHoverOverEvent();
    hoverOverEvent.widget = GetRootWidget();
    hoverOverEvent.perkIndex = this.m_index;
    hoverOverEvent.perkData = this.m_displayData;
    QueueEvent(hoverOverEvent);
    StopHoverAnimations();
    this.m_cool_in_proxy = PlayLibraryAnimation(this.m_isTrait ? "cool_unlimited_hover" : "cool_hover");
  }

  protected cb Bool OnPerkItemHoverOut(ref<inkPointerEvent> evt) {
    ref<PerkHoverOutEvent> hoverOutEvent;
    hoverOutEvent = new PerkHoverOutEvent();
    hoverOutEvent.widget = GetRootWidget();
    hoverOutEvent.perkData = this.m_displayData;
    QueueEvent(hoverOutEvent);
    StopHoverAnimations();
    this.m_cool_out_proxy = PlayLibraryAnimation(this.m_isTrait ? "cool_unlimited_hover_out" : "cool_hover_out");
  }

  private final void StopHoverAnimations() {
    if(ToBool(this.m_cool_in_proxy)) {
      this.m_cool_in_proxy.Stop();
    };
    if(ToBool(this.m_cool_out_proxy)) {
      this.m_cool_out_proxy.Stop();
    };
  }

  protected cb Bool OnPerkDisplayClick(ref<inkPointerEvent> evt) {
    this.m_holdStarted = false;
  }

  protected cb Bool OnPerkDisplayHold(ref<inkPointerEvent> evt) {
    Float progress;
    ref<PerksItemHoldStart> holdStartEvent;
    if(evt.IsAction("upgrade_perk")) {
      progress = evt.GetHoldProgress();
      if(progress > 0 && !this.m_holdStarted) {
        holdStartEvent = new PerksItemHoldStart();
        holdStartEvent.widget = GetRootWidget();
        holdStartEvent.perkData = this.m_displayData;
        holdStartEvent.actionName = evt.GetActionName();
        QueueEvent(holdStartEvent);
        this.m_holdStarted = true;
        if(!this.m_dataManager.IsPerkUpgradeable(this.m_displayData, true) && IsActionNameCompatible(evt)) {
          PlayLibraryAnimation(this.m_isTrait ? "locked_unlimited_perk" : "locked_perk");
          PlaySound("Perk", "OnBuyFail");
        };
      };
      if(progress >= 1) {
        if(this.m_dataManager.IsPerkUpgradeable(this.m_displayData) && IsActionNameCompatible(evt)) {
          this.m_recentlyPurchased = true;
          Upgrade();
          if(this.m_displayData.m_level == this.m_displayData.m_maxLevel - 1) {
            PlayLibraryAnimation("maxed_perk");
            PlaySound("Perk", "OnBuyFail");
          } else {
            if(this.m_displayData.m_level >= 0) {
              PlayLibraryAnimation(this.m_isTrait ? "buy_unlimited_perk" : "buy_perk");
              PlaySound("Perk", "OnLevelUp");
            };
          };
        };
      } else {
        if(!this.m_recentlyPurchased) {
          this.m_recentlyPurchased = false;
        };
      };
    };
  }

  private final Bool IsActionNameCompatible(ref<inkPointerEvent> evt) {
    return evt.IsAction("use_item") || evt.IsAction("click") || evt.IsAction("upgrade_perk");
  }
}

public class PerkDisplayContainerController extends inkLogicController {

  protected edit Int32 m_index;

  protected edit Bool m_isTrait;

  protected edit inkWidgetRef m_widget;

  protected ref<BasePerkDisplayData> m_data;

  protected ref<PlayerDevelopmentDataManager> m_dataManager;

  protected ref<PerkDisplayController> m_controller;

  protected cb Bool OnInitialize() {
    ref<PerkDisplayContainerCreatedEvent> evt;
    evt = new PerkDisplayContainerCreatedEvent();
    evt.index = this.m_index;
    evt.isTrait = this.m_isTrait;
    evt.container = RefToWeakRef(this);
    QueueEvent(evt);
  }

  private final void SpawnController() {
    wref<inkWidget> widget;
    WeakRefToRef(Cast(GetRootWidget())).RemoveAllChildren();
    widget = SpawnFromLocal(Cast(GetRootWidget()), this.m_isTrait ? "SkillUnlimitedPerkDisplay" : "SkillPerkDisplay");
    WeakRefToRef(widget).SetVAlign(inkEVerticalAlign.Top);
    WeakRefToRef(widget).SetHAlign(inkEHorizontalAlign.Left);
    this.m_controller = Cast(WeakRefToRef(WeakRefToRef(widget).GetController()));
  }

  public final void SetData(ref<BasePerkDisplayData> perkData, ref<PlayerDevelopmentDataManager> dataManager) {
    this.m_data = perkData;
    this.m_dataManager = dataManager;
    if(this.m_controller == null) {
      SpawnController();
    };
    this.m_controller.Setup(this.m_data, this.m_dataManager, this.m_index);
  }

  public final ref<BasePerkDisplayData> GetPerkDisplayData() {
    return this.m_data;
  }

  public final Int32 GetPerkIndex() {
    return this.m_index;
  }
}
