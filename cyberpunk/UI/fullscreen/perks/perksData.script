
public class IDisplayData extends IScriptable {

  public ref<BasePerksMenuTooltipData> CreateTooltipData(ref<PlayerDevelopmentDataManager> manager) {
    return null;
  }
}

public class PerkDisplayData extends BasePerkDisplayData {

  public gamedataPerkArea m_area;

  public gamedataPerkType m_type;

  public ref<BasePerksMenuTooltipData> CreateTooltipData(ref<PlayerDevelopmentDataManager> manager) {
    ref<PerkTooltipData> data;
    data = new PerkTooltipData();
    data.manager = manager;
    data.perkType = this.m_type;
    data.perkArea = this.m_area;
    data.proficiency = this.m_proficiency;
    data.attributeId = this.m_attributeId;
    data.perkData = this;
    return data;
  }
}

public class TraitDisplayData extends BasePerkDisplayData {

  public gamedataTraitType m_type;

  public ref<BasePerksMenuTooltipData> CreateTooltipData(ref<PlayerDevelopmentDataManager> manager) {
    ref<TraitTooltipData> data;
    data = new TraitTooltipData();
    data.manager = manager;
    data.traitType = this.m_type;
    data.proficiency = this.m_proficiency;
    data.attributeId = this.m_attributeId;
    return data;
  }
}

public class ProficiencyDisplayData extends IDisplayData {

  public TweakDBID m_attributeId;

  public gamedataProficiencyType m_proficiency;

  public Int32 m_index;

  public array<ref<AreaDisplayData>> m_areas;

  public array<ref<LevelRewardDisplayData>> m_passiveBonusesData;

  public ref<TraitDisplayData> m_traitData;

  public String m_localizedName;

  public String m_localizedDescription;

  public Int32 m_level;

  public Int32 m_maxLevel;

  public Int32 m_expPoints;

  public Int32 m_maxExpPoints;

  public Int32 m_unlockedLevel;

  public ref<BasePerksMenuTooltipData> CreateTooltipData(ref<PlayerDevelopmentDataManager> manager) {
    ref<SkillTooltipData> data;
    data = new SkillTooltipData();
    data.manager = manager;
    data.proficiencyType = this.m_proficiency;
    data.attributeRecord = GetAttributeRecord(this.m_attributeId);
    return data;
  }
}

public class AttributeDisplayData extends IDisplayData {

  public TweakDBID m_attributeId;

  public array<ref<ProficiencyDisplayData>> m_proficiencies;

  public ref<BasePerksMenuTooltipData> CreateTooltipData(ref<PlayerDevelopmentDataManager> manager) {
    ref<AttributeTooltipData> data;
    data = new AttributeTooltipData();
    data.manager = manager;
    data.attributeId = this.m_attributeId;
    data.attributeType = manager.GetAttributeEnumFromRecordID(this.m_attributeId);
    return data;
  }
}

public class AttributeData extends IDisplayData {

  public String label;

  public String icon;

  public TweakDBID id;

  public Int32 value;

  public Int32 maxValue;

  public String description;

  public Bool availableToUpgrade;

  public gamedataStatType type;

  public ref<BasePerksMenuTooltipData> CreateTooltipData(ref<PlayerDevelopmentDataManager> manager) {
    ref<AttributeTooltipData> data;
    data = new AttributeTooltipData();
    data.manager = manager;
    data.attributeId = this.id;
    data.attributeType = manager.GetAttributeEnumFromRecordID(this.id);
    return data;
  }
}

public class PlayerDevelopmentDataManager extends IScriptable {

  private wref<PlayerPuppet> m_player;

  private ref<PlayerDevelopmentSystem> m_playerDevSystem;

  private wref<inkGameController> m_parentGameCtrl;

  public final void Initialize(ref<PlayerPuppet> player, ref<inkGameController> parentGameCtrl) {
    this.m_parentGameCtrl = RefToWeakRef(parentGameCtrl);
    this.m_player = RefToWeakRef(player);
    this.m_playerDevSystem = GetInstance(player);
  }

  public final wref<PlayerDevelopmentSystem> GetPlayerDevelopmentSystem() {
    return RefToWeakRef(this.m_playerDevSystem);
  }

  public final wref<PlayerDevelopmentData> GetPlayerDevelopmentData() {
    return RefToWeakRef(GetData(WeakRefToRef(this.m_player)));
  }

  public final wref<PlayerPuppet> GetPlayer() {
    return this.m_player;
  }

  public final ref<PerkDisplayData> GetPerkDisplayData(gamedataPerkType perkType, gamedataPerkArea perkArea, gamedataProficiencyType proficiency, TweakDBID attributeId, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    return GetPerkDisplayData(perkType, perkArea, proficiency, GetAttributeRecord(attributeId), playerDevelopmentData);
  }

  public final ref<PerkDisplayData> GetPerkDisplayData(gamedataPerkType perkType, gamedataPerkArea perkArea, gamedataProficiencyType proficiency, ref<Attribute_Record> attributeRecord, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    ref<PerkDisplayData> curPerkDisplayData;
    wref<Perk_Record> perkRecord;
    Int32 perkCurrLevel;
    Int32 perkIndex;
    if(!ToBool(playerDevelopmentData)) {
      playerDevelopmentData = RefToWeakRef(GetData(WeakRefToRef(this.m_player)));
    };
    perkRecord = RefToWeakRef(WeakRefToRef(playerDevelopmentData).GetPerkRecord(perkType));
    perkIndex = WeakRefToRef(playerDevelopmentData).GetPerkIndex(perkArea, perkType);
    curPerkDisplayData = new PerkDisplayData();
    curPerkDisplayData.m_attributeId = attributeRecord.GetID();
    curPerkDisplayData.m_localizedName = GetLocalizedText(WeakRefToRef(perkRecord).Loc_name_key());
    curPerkDisplayData.m_localizedDescription = GetLocalizedText(WeakRefToRef(perkRecord).Loc_desc_key());
    curPerkDisplayData.m_binkRef = WeakRefToRef(perkRecord).BinkPath();
    curPerkDisplayData.m_type = perkType;
    curPerkDisplayData.m_iconID = WeakRefToRef(perkRecord).EnumName();
    curPerkDisplayData.m_locked = !WeakRefToRef(playerDevelopmentData).IsPerkAreaUnlocked(perkArea);
    perkCurrLevel = this.m_playerDevSystem.GetPerkLevel(WeakRefToRef(this.m_player), curPerkDisplayData.m_type);
    curPerkDisplayData.m_level = perkCurrLevel < 0 ? 0 : perkCurrLevel;
    curPerkDisplayData.m_maxLevel = this.m_playerDevSystem.GetPerkMaxLevel(WeakRefToRef(this.m_player), curPerkDisplayData.m_type);
    curPerkDisplayData.m_proficiency = proficiency;
    curPerkDisplayData.m_area = perkArea;
    return curPerkDisplayData;
  }

  public final ref<TraitDisplayData> GetTraitDisplayData(gamedataTraitType traitType, TweakDBID attributeId, gamedataProficiencyType proficiency, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    return GetTraitDisplayData(RefToWeakRef(GetTraitRecord(traitType)), GetAttributeRecord(attributeId), proficiency, playerDevelopmentData);
  }

  public final ref<TraitDisplayData> GetTraitDisplayData(TweakDBID traitRecordId, TweakDBID attributeId, gamedataProficiencyType proficiency, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    return GetTraitDisplayData(RefToWeakRef(GetTraitRecord(traitRecordId)), GetAttributeRecord(attributeId), proficiency, playerDevelopmentData);
  }

  public final ref<TraitDisplayData> GetTraitDisplayData(wref<Trait_Record> traitRecord, ref<Attribute_Record> attributeRecord, gamedataProficiencyType proficiency, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    ref<TraitDisplayData> traitData;
    wref<TraitData_Record> traitDataRecord;
    gamedataTraitType traitType;
    if(!ToBool(playerDevelopmentData)) {
      playerDevelopmentData = RefToWeakRef(GetData(WeakRefToRef(this.m_player)));
    };
    traitDataRecord = WeakRefToRef(traitRecord).InfiniteTraitData();
    traitType = WeakRefToRef(traitRecord).Type();
    traitData = new TraitDisplayData();
    traitData.m_attributeId = attributeRecord.GetID();
    traitData.m_localizedName = GetLocalizedText(WeakRefToRef(traitRecord).Loc_name_key());
    traitData.m_localizedDescription = GetLocalizedText(WeakRefToRef(traitRecord).Loc_desc_key());
    traitData.m_type = traitType;
    traitData.m_proficiency = proficiency;
    traitData.m_iconID = WeakRefToRef(traitRecord).EnumName();
    traitData.m_locked = !WeakRefToRef(playerDevelopmentData).IsTraitUnlocked(traitType);
    traitData.m_level = WeakRefToRef(playerDevelopmentData).GetTraitLevel(traitType);
    traitData.m_maxLevel = -1;
    return traitData;
  }

  public final ref<AreaDisplayData> GetAreaDisplayData(gamedataPerkArea perkArea, gamedataProficiencyType proficiency, TweakDBID attributeId, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    return GetAreaDisplayData(perkArea, proficiency, GetAttributeRecord(attributeId), playerDevelopmentData);
  }

  public final ref<AreaDisplayData> GetAreaDisplayData(gamedataPerkArea perkArea, gamedataProficiencyType proficiency, ref<Attribute_Record> attributeRecord, wref<PlayerDevelopmentData> playerDevelopmentData?) {
    ref<AreaDisplayData> curPerkAreaDisplayData;
    if(!ToBool(playerDevelopmentData)) {
      playerDevelopmentData = RefToWeakRef(GetData(WeakRefToRef(this.m_player)));
    };
    curPerkAreaDisplayData = new AreaDisplayData();
    curPerkAreaDisplayData.m_attributeId = attributeRecord.GetID();
    curPerkAreaDisplayData.m_locked = !WeakRefToRef(playerDevelopmentData).IsPerkAreaUnlocked(perkArea);
    curPerkAreaDisplayData.m_area = perkArea;
    curPerkAreaDisplayData.m_proficency = proficiency;
    return curPerkAreaDisplayData;
  }

  public final ref<ProficiencyDisplayData> GetProficiencyDisplayData(gamedataProficiencyType proficiency, TweakDBID attributeId) {
    return GetProficiencyDisplayData(proficiency, GetAttributeRecord(attributeId));
  }

  public final ref<ProficiencyDisplayData> GetProficiencyDisplayData(gamedataProficiencyType proficiency, ref<Attribute_Record> attributeRecord) {
    ref<ProficiencyDisplayData> curProfDisplayData;
    ref<Proficiency_Record> proficiencyRecord;
    ref<AttributeData> attributeData;
    proficiencyRecord = WeakRefToRef(GetProficiencyRecord(proficiency));
    attributeData = GetAttribute(attributeRecord.GetID());
    curProfDisplayData = new ProficiencyDisplayData();
    curProfDisplayData.m_attributeId = attributeRecord.GetID();
    curProfDisplayData.m_proficiency = proficiency;
    curProfDisplayData.m_traitData = GetTraitDisplayData(proficiencyRecord.Trait(), attributeRecord, proficiency);
    curProfDisplayData.m_localizedName = GetLocalizedText(proficiencyRecord.Loc_name_key());
    curProfDisplayData.m_localizedDescription = GetLocalizedText(proficiencyRecord.Loc_desc_key());
    curProfDisplayData.m_level = this.m_playerDevSystem.GetProficiencyLevel(WeakRefToRef(this.m_player), proficiency);
    curProfDisplayData.m_expPoints = this.m_playerDevSystem.GetCurrentLevelProficiencyExp(WeakRefToRef(this.m_player), proficiency);
    curProfDisplayData.m_maxExpPoints = curProfDisplayData.m_expPoints + this.m_playerDevSystem.GetRemainingExpForLevelUp(WeakRefToRef(this.m_player), proficiency);
    curProfDisplayData.m_maxLevel = this.m_playerDevSystem.GetProficiencyAbsoluteMaxLevel(WeakRefToRef(this.m_player), proficiency);
    curProfDisplayData.m_unlockedLevel = attributeData.value;
    curProfDisplayData.m_passiveBonusesData = GetPassiveBonusDisplayData(proficiencyRecord);
    return curProfDisplayData;
  }

  public final array<ref<LevelRewardDisplayData>> GetPassiveBonusDisplayData(ref<Proficiency_Record> proficiencyRecord) {
    ref<UILocalizationDataPackage> dataPackage;
    array<ref<LevelRewardDisplayData>> bonusesDisplay;
    ref<LevelRewardDisplayData> bonusDisplay;
    wref<PassiveProficiencyBonus_Record> bonusData;
    Int32 bonusIndex;
    bonusIndex = 1;
    while(bonusIndex < proficiencyRecord.GetPassiveBonusesCount()) {
      bonusDisplay = new LevelRewardDisplayData();
      bonusData = proficiencyRecord.GetPassiveBonusesItem(bonusIndex);
      bonusDisplay.level = bonusIndex + 1;
      bonusDisplay.locPackage = FromPassiveUIDataPackage(WeakRefToRef(bonusData).UiData());
      bonusDisplay.description = LocKeyToString(WeakRefToRef(WeakRefToRef(bonusData).UiData()).Loc_name_key());
      Push(bonusesDisplay, bonusDisplay);
      bonusIndex += 1;
    };
    return bonusesDisplay;
  }

  private final wref<Attribute_Record> GetAttributeRecordFromProficiency(gamedataProficiencyType proficiency) {
    Int32 i;
    Int32 j;
    ref<PlayerDevelopmentData> playerDevelopmentData;
    array<SAttribute> attributes;
    ref<Attribute_Record> attributeRecord;
    array<wref<Proficiency_Record>> proficiencies;
    playerDevelopmentData = GetData(WeakRefToRef(this.m_player));
    attributes = playerDevelopmentData.GetAttributes();
    i = 0;
    while(i < Size(attributes)) {
      attributeRecord = Cast(playerDevelopmentData.GetAttributeRecord(attributes[i].attributeName));
      attributeRecord.Proficiencies(proficiencies);
      j = 0;
      while(j < Size(proficiencies)) {
        if(WeakRefToRef(proficiencies[j]).Type() == proficiency) {
          return RefToWeakRef(attributeRecord);
        };
        j += 1;
      };
      i += 1;
    };
    return null;
  }

  private final wref<Proficiency_Record> GetProficiencyRecord(wref<Attribute_Record> attributeRecord, gamedataProficiencyType proficiency) {
    Int32 i;
    ref<PlayerDevelopmentData> playerDevelopmentData;
    array<SAttribute> attributes;
    array<wref<Proficiency_Record>> proficiencies;
    WeakRefToRef(attributeRecord).Proficiencies(proficiencies);
    i = 0;
    while(i < Size(proficiencies)) {
      if(WeakRefToRef(proficiencies[i]).Type() == proficiency) {
        return proficiencies[i];
      };
      i += 1;
    };
    return null;
  }

  private final wref<Proficiency_Record> GetProficiencyRecord(gamedataProficiencyType proficiency) {
    Int32 i;
    Int32 j;
    ref<PlayerDevelopmentData> playerDevelopmentData;
    array<SAttribute> attributes;
    ref<Attribute_Record> attributeRecord;
    array<wref<Proficiency_Record>> proficiencies;
    playerDevelopmentData = GetData(WeakRefToRef(this.m_player));
    attributes = playerDevelopmentData.GetAttributes();
    i = 0;
    while(i < Size(attributes)) {
      attributeRecord = Cast(playerDevelopmentData.GetAttributeRecord(attributes[i].attributeName));
      attributeRecord.Proficiencies(proficiencies);
      j = 0;
      while(j < Size(proficiencies)) {
        if(WeakRefToRef(proficiencies[j]).Type() == proficiency) {
          return proficiencies[j];
        };
        j += 1;
      };
      i += 1;
    };
    return null;
  }

  public final ref<ProficiencyDisplayData> GetProficiencyWithData(gamedataProficiencyType proficiency) {
    ref<Attribute_Record> attributeRecord;
    Int32 areaIdx;
    Int32 perkIdx;
    wref<Proficiency_Record> curProficiency;
    ref<ProficiencyDisplayData> curProfDisplayData;
    wref<PerkArea_Record> curPerkArea;
    ref<AreaDisplayData> curPerkAreaDisplayData;
    wref<Perk_Record> curPerk;
    ref<PerkDisplayData> curPerkDisplayData;
    ref<PlayerDevelopmentData> playerDevelopmentData;
    playerDevelopmentData = GetData(WeakRefToRef(this.m_player));
    attributeRecord = WeakRefToRef(GetAttributeRecordFromProficiency(proficiency));
    curProficiency = GetProficiencyRecord(RefToWeakRef(attributeRecord), proficiency);
    curProfDisplayData = GetProficiencyDisplayData(WeakRefToRef(curProficiency).Type(), attributeRecord);
    areaIdx = 0;
    while(areaIdx < WeakRefToRef(curProficiency).GetPerkAreasCount()) {
      curPerkArea = WeakRefToRef(curProficiency).GetPerkAreasItem(areaIdx);
      curPerkAreaDisplayData = GetAreaDisplayData(WeakRefToRef(curPerkArea).Type(), WeakRefToRef(curProficiency).Type(), attributeRecord, RefToWeakRef(playerDevelopmentData));
      Push(curProfDisplayData.m_areas, curPerkAreaDisplayData);
      perkIdx = 0;
      while(perkIdx < WeakRefToRef(curPerkArea).GetPerksCount()) {
        curPerkDisplayData = GetPerkDisplayData(WeakRefToRef(WeakRefToRef(curPerkArea).GetPerksItem(perkIdx)).Type(), WeakRefToRef(curPerkArea).Type(), WeakRefToRef(curProficiency).Type(), attributeRecord, RefToWeakRef(playerDevelopmentData));
        Push(curPerkAreaDisplayData.m_perks, curPerkDisplayData);
        perkIdx += 1;
      };
      areaIdx += 1;
    };
    return curProfDisplayData;
  }

  public final ref<AttributeDisplayData> GetAttributeData(TweakDBID attributeId) {
    ref<Attribute_Record> attributeRecord;
    ref<AttributeDisplayData> attributeDisplayData;
    Int32 profIdx;
    Int32 areaIdx;
    Int32 perkIdx;
    wref<Proficiency_Record> curProficiency;
    ref<ProficiencyDisplayData> curProfDisplayData;
    wref<PerkArea_Record> curPerkArea;
    ref<AreaDisplayData> curPerkAreaDisplayData;
    wref<Perk_Record> curPerk;
    ref<PerkDisplayData> curPerkDisplayData;
    ref<PlayerDevelopmentData> playerDevelopmentData;
    playerDevelopmentData = GetData(WeakRefToRef(this.m_player));
    attributeRecord = GetAttributeRecord(attributeId);
    attributeDisplayData = new AttributeDisplayData();
    attributeDisplayData.m_attributeId = attributeId;
    profIdx = 0;
    while(profIdx < attributeRecord.GetProficienciesCount()) {
      curProficiency = attributeRecord.GetProficienciesItem(profIdx);
      curProfDisplayData = GetProficiencyDisplayData(WeakRefToRef(curProficiency).Type(), attributeRecord);
      areaIdx = 0;
      while(areaIdx < WeakRefToRef(curProficiency).GetPerkAreasCount()) {
        curPerkArea = WeakRefToRef(curProficiency).GetPerkAreasItem(areaIdx);
        curPerkAreaDisplayData = GetAreaDisplayData(WeakRefToRef(curPerkArea).Type(), WeakRefToRef(curProficiency).Type(), attributeRecord, RefToWeakRef(playerDevelopmentData));
        perkIdx = 0;
        while(perkIdx < WeakRefToRef(curPerkArea).GetPerksCount()) {
          curPerkDisplayData = GetPerkDisplayData(WeakRefToRef(WeakRefToRef(curPerkArea).GetPerksItem(perkIdx)).Type(), WeakRefToRef(curPerkArea).Type(), WeakRefToRef(curProficiency).Type(), attributeRecord, RefToWeakRef(playerDevelopmentData));
          Push(curPerkAreaDisplayData.m_perks, curPerkDisplayData);
          perkIdx += 1;
        };
        Push(curProfDisplayData.m_areas, curPerkAreaDisplayData);
        areaIdx += 1;
      };
      Push(attributeDisplayData.m_proficiencies, curProfDisplayData);
      profIdx += 1;
    };
    return attributeDisplayData;
  }

  public final array<ref<AttributeData>> GetAttributes() {
    Int32 i;
    array<SAttribute> sAttributeData;
    array<ref<AttributeData>> attributeDataArray;
    ref<AttributeData> outData;
    SAttribute sAttribute;
    sAttributeData = this.m_playerDevSystem.GetAttributes(WeakRefToRef(this.m_player));
    i = 0;
    while(i < Size(sAttributeData)) {
      sAttribute = sAttributeData[i];
      outData = new AttributeData();
      FillAttributeData(sAttribute, outData);
      Push(attributeDataArray, outData);
      i += 1;
    };
    return attributeDataArray;
  }

  public final ref<AttributeData> GetAttributeFromType(gamedataStatType attributeType) {
    Int32 i;
    ref<AttributeData> outData;
    array<SAttribute> sAttributeData;
    sAttributeData = this.m_playerDevSystem.GetAttributes(WeakRefToRef(this.m_player));
    i = 0;
    while(i < Size(sAttributeData)) {
      if(sAttributeData[i].attributeName == attributeType) {
        outData = new AttributeData();
        FillAttributeData(sAttributeData[i], outData);
      } else {
        i += 1;
      };
    };
    return outData;
  }

  public final ref<AttributeData> GetAttribute(TweakDBID attributeID) {
    Int32 i;
    ref<AttributeData> outData;
    array<SAttribute> sAttributeData;
    sAttributeData = this.m_playerDevSystem.GetAttributes(WeakRefToRef(this.m_player));
    i = 0;
    while(i < Size(sAttributeData)) {
      if(sAttributeData[i].id == attributeID) {
        outData = new AttributeData();
        FillAttributeData(sAttributeData[i], outData);
      } else {
        i += 1;
      };
    };
    return outData;
  }

  public final TweakDBID GetAttributeRecordIDFromEnum(PerkMenuAttribute attribute) {
    switch(attribute) {
      case PerkMenuAttribute.Body:
        return GetStatRecord("BaseStats.Strength").GetID();
      case PerkMenuAttribute.Reflex:
        return GetStatRecord("BaseStats.Reflexes").GetID();
      case PerkMenuAttribute.Technical_Ability:
        return GetStatRecord("BaseStats.TechnicalAbility").GetID();
      case PerkMenuAttribute.Cool:
        return GetStatRecord("BaseStats.Cool").GetID();
      case PerkMenuAttribute.Intelligence:
        return GetStatRecord("BaseStats.Intelligence").GetID();
    };
    return undefined();
  }

  public final PerkMenuAttribute GetAttributeEnumFromRecordID(TweakDBID recordID) {
    if(recordID == "BaseStats.Strength") {
      return PerkMenuAttribute.Body;
    };
    if(recordID == "BaseStats.Reflexes") {
      return PerkMenuAttribute.Reflex;
    };
    if(recordID == "BaseStats.TechnicalAbility") {
      return PerkMenuAttribute.Technical_Ability;
    };
    if(recordID == "BaseStats.Cool") {
      return PerkMenuAttribute.Cool;
    };
    if(recordID == "BaseStats.Intelligence") {
      return PerkMenuAttribute.Intelligence;
    };
    return PerkMenuAttribute.Johnny;
  }

  private final void FillAttributeData(SAttribute attribute, out ref<AttributeData> outData) {
    outData.label = GetStatRecord(Create("BaseStats." + EnumValueToString("gamedataStatType", Cast(ToInt(attribute.attributeName))))).LocalizedName();
    outData.value = attribute.value;
    outData.maxValue = 20;
    outData.id = attribute.id;
    outData.availableToUpgrade = outData.value < outData.maxValue;
    outData.type = attribute.attributeName;
    outData.description = GetStatRecord(Create("BaseStats." + EnumValueToString("gamedataStatType", Cast(ToInt(attribute.attributeName))))).LocalizedDescription();
  }

  public final Int32 GetPerkPoints() {
    return this.m_playerDevSystem.GetDevPoints(WeakRefToRef(this.m_player), gamedataDevelopmentPointType.Primary);
  }

  public final Int32 GetAttributePoints() {
    return this.m_playerDevSystem.GetDevPoints(WeakRefToRef(this.m_player), gamedataDevelopmentPointType.Attribute);
  }

  public final Int32 GetPerkLevel(gamedataPerkType type) {
    return this.m_playerDevSystem.GetPerkLevel(WeakRefToRef(this.m_player), type);
  }

  public final static String PerkUtilityToString(gamedataPerkUtility utility) {
    switch(utility) {
      case gamedataPerkUtility.ActiveUtility:
        return GetLocalizedText("UI-ScriptExports-Active");
      case gamedataPerkUtility.PassiveUtility:
        return GetLocalizedText("UI-Tooltips-Passive");
      case gamedataPerkUtility.TriggeredUtility:
        return GetLocalizedText("UI-Tooltips-Trigger");
    };
  }

  private final void UpdateData() {
    ref<PlayerDevUpdateDataEvent> evt;
    evt = new PlayerDevUpdateDataEvent();
    WeakRefToRef(this.m_parentGameCtrl).QueueEvent(evt);
  }

  private final void NotifyAttributeUpdate(TweakDBID attributeId) {
    ref<AttributeUpdatedEvent> evt;
    evt = new AttributeUpdatedEvent();
    evt.attributeId = attributeId;
    WeakRefToRef(this.m_parentGameCtrl).QueueEvent(evt);
  }

  public final void UpgradePerk(ref<PerkDisplayData> data) {
    ref<BuyPerk> request;
    request = new BuyPerk();
    request.Set(this.m_player, data.m_type);
    this.m_playerDevSystem.QueueRequest(request);
    UpdateData();
  }

  public final void UpgradeTrait(ref<TraitDisplayData> data) {
    ref<IncreaseTraitLevel> request;
    request = new IncreaseTraitLevel();
    request.Set(this.m_player, data.m_type);
    this.m_playerDevSystem.QueueRequest(request);
    UpdateData();
  }

  public final void UpgradeAttribute(ref<AttributeData> data) {
    ref<RefreshPerkAreas> refresh;
    UpgradeAttribute(data.type);
    NotifyAttributeUpdate(data.id);
    refresh = new RefreshPerkAreas();
    refresh.owner = this.m_player;
    this.m_playerDevSystem.QueueRequest(refresh);
  }

  public final void UpgradeAttribute(gamedataStatType type) {
    ref<BuyAttribute> request;
    request = new BuyAttribute();
    request.Set(this.m_player, type);
    this.m_playerDevSystem.QueueRequest(request);
    UpdateData();
  }

  public final Bool IsPerkUpgradeable(ref<BasePerkDisplayData> data, Bool showNotification?) {
    ref<UIMenuNotificationEvent> notificationEvent;
    UIMenuNotificationType notificationType;
    Bool isPerkUpgradeable;
    isPerkUpgradeable = true;
    if(GetPerkPoints() < 0) {
      notificationType = UIMenuNotificationType.NoPerksPoints;
      isPerkUpgradeable = false;
    };
    if(data.m_locked) {
      notificationType = UIMenuNotificationType.PerksLocked;
      isPerkUpgradeable = false;
    };
    if(data.m_maxLevel > -1 && data.m_level >= data.m_maxLevel) {
      notificationType = UIMenuNotificationType.MaxLevelPerks;
      isPerkUpgradeable = false;
    };
    if(!isPerkUpgradeable && showNotification) {
      notificationEvent = new UIMenuNotificationEvent();
      notificationEvent.m_notificationType = notificationType;
      GetUISystem(WeakRefToRef(this.m_player).GetGame()).QueueEvent(notificationEvent);
    };
    return isPerkUpgradeable;
  }

  public final Bool HasAvailableAttributePoints(Bool showNotification?) {
    ref<UIMenuNotificationEvent> notificationEvent;
    Bool points;
    points = GetAttributePoints() > 0;
    if(!points && showNotification) {
      notificationEvent = new UIMenuNotificationEvent();
      notificationEvent.m_notificationType = UIMenuNotificationType.NoAttributePoints;
      GetUISystem(WeakRefToRef(this.m_player).GetGame()).QueueEvent(notificationEvent);
    };
    return points;
  }
}
