
public class ItemsNotificationQueue extends gameuiGenericNotificationGameController {

  [Default(ItemsNotificationQueue, 6.0f))]
  private Float m_showDuration;

  private wref<TransactionSystem> m_transactionSystem;

  [Default(ItemsNotificationQueue, notification_currency))]
  private CName m_currencyNotification;

  [Default(ItemsNotificationQueue, Item_Received_SMALL))]
  private CName m_itemNotification;

  [Default(ItemsNotificationQueue, progression))]
  private CName m_xpNotification;

  private wref<GameObject> m_playerPuppet;

  private wref<InventoryScriptListener> m_inventoryListener;

  private wref<InventoryScriptListener> m_currencyInventoryListener;

  private ref<IBlackboard> m_playerStatsBlackboard;

  private ref<PlayerDevelopmentSystem> m_playerDevelopmentSystem;

  private Uint32 m_combatModeListener;

  private ref<InventoryDataManagerV2> m_InventoryManager;

  private ref<ItemPreferredComparisonResolver> m_comparisonResolver;

  private gamePSMCombat m_combatModePSM;

  public Bool GetShouldSaveState() {
    return true;
  }

  public Int32 GetID() {
    return ToInt(GenericNotificationType.ProgressionNotification);
  }

  protected cb Bool OnCombatStateChanged(Int32 value) {
    this.m_combatModePSM = ToEnum(value);
    if(this.m_combatModePSM == gamePSMCombat.InCombat) {
      SetNotificationPause(true);
      WeakRefToRef(GetRootWidget()).SetVisible(false);
    } else {
      SetNotificationPause(false);
      WeakRefToRef(GetRootWidget()).SetVisible(true);
    };
  }

  protected cb Bool OnPlayerAttach(ref<GameObject> playerPuppet) {
    ref<ItemAddedInventoryCallback> itemCallback;
    ref<CurrencyChangeInventoryCallback> currencyCallback;
    itemCallback = new ItemAddedInventoryCallback();
    itemCallback.m_notificationQueue = this;
    this.m_playerPuppet = RefToWeakRef(playerPuppet);
    this.m_transactionSystem = RefToWeakRef(GetTransactionSystem(playerPuppet.GetGame()));
    this.m_inventoryListener = RefToWeakRef(WeakRefToRef(this.m_transactionSystem).RegisterInventoryListener(playerPuppet, itemCallback));
    currencyCallback = new CurrencyChangeInventoryCallback();
    currencyCallback.m_notificationQueue = this;
    this.m_currencyInventoryListener = RefToWeakRef(WeakRefToRef(this.m_transactionSystem).RegisterInventoryListener(playerPuppet, currencyCallback));
    this.m_playerDevelopmentSystem = Cast(GetScriptableSystemsContainer(WeakRefToRef(this.m_playerPuppet).GetGame()).Get("PlayerDevelopmentSystem"));
    RegisterPSMListeners(WeakRefToRef(this.m_playerPuppet));
    this.m_InventoryManager = new InventoryDataManagerV2();
    this.m_InventoryManager.Initialize(Cast(WeakRefToRef(this.m_playerPuppet)));
    this.m_comparisonResolver = Make(this.m_InventoryManager);
    SetNotificationPauseWhenHidden(true);
  }

  protected cb Bool OnPlayerDetach(ref<GameObject> playerPuppet) {
    WeakRefToRef(this.m_transactionSystem).UnregisterInventoryListener(playerPuppet, WeakRefToRef(this.m_inventoryListener));
    this.m_inventoryListener = null;
    WeakRefToRef(this.m_transactionSystem).UnregisterInventoryListener(playerPuppet, WeakRefToRef(this.m_currencyInventoryListener));
    this.m_currencyInventoryListener = null;
    UnregisterPSMListeners(WeakRefToRef(this.m_playerPuppet));
  }

  protected cb Bool OnUILootedItemEvent(ref<UILootedItemEvent> evt) {
    InventoryItemData inventoryItem;
    inventoryItem = this.m_InventoryManager.GetItemFromRecord(GetTDBID(evt.itemID));
    if(NeedsNotification(GetEquipmentArea(inventoryItem))) {
      PushItemNotification(evt.itemID, QualityEnumToName(GetItemDataQuality(RefToWeakRef(GetGameItemData(inventoryItem)))));
    };
  }

  private final Bool NeedsNotification(gamedataEquipmentArea type) {
    return type == gamedataEquipmentArea.Weapon || type == gamedataEquipmentArea.WeaponHeavy || type == gamedataEquipmentArea.WeaponWheel || type == gamedataEquipmentArea.WeaponLeft || type == gamedataEquipmentArea.Face || type == gamedataEquipmentArea.Feet || type == gamedataEquipmentArea.Head || type == gamedataEquipmentArea.InnerChest || type == gamedataEquipmentArea.OuterChest || type == gamedataEquipmentArea.Legs || type == gamedataEquipmentArea.Outfit || type == gamedataEquipmentArea.Quest;
  }

  protected cb Bool OnVendorBoughtItemEvent(ref<VendorBoughtItemEvent> evt) {
    Int32 i;
    Int32 limit;
    InventoryItemData inventoryItem;
    i = 0;
    limit = Size(evt.items);
    while(i < limit) {
      inventoryItem = this.m_InventoryManager.GetItemFromRecord(GetTDBID(evt.items[i]));
      PushItemNotification(evt.items[i], QualityEnumToName(GetItemDataQuality(RefToWeakRef(GetGameItemData(inventoryItem)))));
      i += 1;
    };
  }

  protected cb Bool OnCharacterProficiencyUpdated(ref<ProficiencyProgressEvent> evt) {
    switch(evt.type) {
      case gamedataProficiencyType.Level:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "XP", "LocKey#40364", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.StreetCred:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "StreetCred", "LocKey#1210", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Assault:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22315", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Athletics:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22299", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Brawling:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22306", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.ColdBlood:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22302", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.CombatHacking:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22332", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Engineering:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22326", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Gunslinger:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22311", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Kenjutsu:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22318", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Stealth:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22324", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Demolition:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22320", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Crafting:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22328", evt.type, evt.currentLevel, evt.isLevelMaxed);
        break;
      case gamedataProficiencyType.Hacking:
        PushXPNotification(evt.expValue, evt.remainingXP, evt.delta, "Skills", "LocKey#22330", evt.type, evt.currentLevel, evt.isLevelMaxed);
    };
  }

  protected cb Bool OnNewTarotCardAdded(ref<TarotCardAdded> evt) {
    gameuiGenericNotificationData notificationData;
    ref<TarotCardAddedNotificationViewData> userData;
    ref<OpenTarotCollectionNotificationAction> action;
    userData = new TarotCardAddedNotificationViewData();
    userData.cardName = evt.cardName;
    userData.imagePart = evt.imagePart;
    userData.animation = "tarot_card";
    action = new OpenTarotCollectionNotificationAction();
    action.m_eventDispatcher = RefToWeakRef(this);
    userData.action = action;
    userData.soundEvent = "TarotCollectedPopup";
    userData.soundAction = "OnCollect";
    notificationData.widgetLibraryItemName = "tarot_card";
    notificationData.notificationData = userData;
    notificationData.time = this.m_showDuration;
    AddNewNotificationData(notificationData);
  }

  public final void PushXPNotification(Int32 value, Int32 remainingPointsToLevelUp, Int32 delta, CName notificationColorTheme, String notificationName, gamedataProficiencyType type, Int32 currentLevel, Bool isLevelMaxed) {
    Float progress;
    ref<ProgressionViewData> userData;
    gameuiGenericNotificationData notificationData;
    Int32 sum;
    sum = remainingPointsToLevelUp + value;
    progress = Cast(value) / Cast(sum);
    if(progress == 0) {
      progress = Cast(sum);
    };
    notificationData.widgetLibraryItemName = this.m_xpNotification;
    userData = new ProgressionViewData();
    userData.expProgress = progress;
    userData.expValue = value;
    userData.notificationColorTheme = notificationColorTheme;
    userData.title = notificationName;
    userData.delta = delta;
    userData.type = type;
    userData.currentLevel = currentLevel;
    userData.isLevelMaxed = isLevelMaxed;
    notificationData.time = 3;
    notificationData.notificationData = userData;
    AddNewNotificationData(notificationData);
  }

  public final void PushCurrencyNotification(Int32 diff, Uint32 total) {
    gameuiGenericNotificationData notificationData;
    ref<CurrencyUpdateNotificationViewData> userData;
    if(diff == 0) {
      return ;
    };
    userData = new CurrencyUpdateNotificationViewData();
    userData.m_diff = diff;
    userData.m_total = total;
    userData.soundEvent = "QuestUpdatePopup";
    userData.soundAction = "OnOpen";
    notificationData.time = 6.099999904632568;
    notificationData.widgetLibraryItemName = this.m_currencyNotification;
    notificationData.notificationData = userData;
    AddNewNotificationData(notificationData);
  }

  public final void PushItemNotification(ItemID itemID, CName itemRarity) {
    ref<ItemAddedNotificationViewData> data;
    gameuiGenericNotificationData notificationData;
    ref<Item_Record> currentItemRecord;
    array<CName> currentItemRecordTags;
    Bool isShard;
    currentItemRecord = GetItemRecord(GetTDBID(itemID));
    if(ToBool(currentItemRecord)) {
      currentItemRecordTags = currentItemRecord.Tags();
    };
    isShard = Contains(currentItemRecordTags, "Shard");
    if(itemID != Money() && !isShard) {
      data = new ItemAddedNotificationViewData();
      data.animation = this.m_itemNotification;
      data.itemRarity = itemRarity;
      data.itemID = itemID;
      data.title = GetLocalizedText("Story-base-gameplay-gui-widgets-notifications-quest_update-_localizationString19");
      notificationData.time = 7.5;
      notificationData.widgetLibraryItemName = this.m_itemNotification;
      notificationData.notificationData = data;
      AddNewNotificationData(notificationData);
    };
  }

  protected final void RegisterPSMListeners(ref<GameObject> playerObject) {
    ref<IBlackboard> playerStateMachineBlackboard;
    ref<PlayerStateMachineDef> playerSMDef;
    playerSMDef = GetAllBlackboardDefs().PlayerStateMachine;
    if(ToBool(playerSMDef)) {
      playerStateMachineBlackboard = GetPSMBlackboard(playerObject);
      if(ToBool(playerStateMachineBlackboard)) {
        this.m_combatModeListener = playerStateMachineBlackboard.RegisterListenerInt(playerSMDef.Combat, this, "OnCombatStateChanged");
      };
    };
  }

  protected final void UnregisterPSMListeners(ref<GameObject> playerObject) {
    ref<IBlackboard> playerStateMachineBlackboard;
    ref<PlayerStateMachineDef> playerSMDef;
    playerSMDef = GetAllBlackboardDefs().PlayerStateMachine;
    if(ToBool(playerSMDef)) {
      playerStateMachineBlackboard = GetPSMBlackboard(playerObject);
      if(ToBool(playerStateMachineBlackboard)) {
        playerStateMachineBlackboard.UnregisterDelayedListener(playerSMDef.Combat, this.m_combatModeListener);
        this.m_combatModeListener = 0;
      };
    };
  }

  private final ItemComparisonState GetComparisonState(InventoryItemData item) {
    if(this.m_comparisonResolver.IsComparable(item)) {
      return this.m_comparisonResolver.GetItemComparisonState(item);
    };
    return ItemComparisonState.Default;
  }
}

public class ItemAddedInventoryCallback extends InventoryScriptCallback {

  public ref<ItemsNotificationQueue> m_notificationQueue;

  public void OnItemNotification(ItemID itemID, wref<gameItemData> itemData) {
    ref<ItemAddedNotificationViewData> data;
    gameuiGenericNotificationData notificationData;
    this.m_notificationQueue.PushItemNotification(itemID, GetItemRarity(itemData));
  }

  private final CName GetItemRarity(wref<gameItemData> data) {
    CName quality;
    gamedataQuality qual;
    qual = GetItemDataQuality(data);
    quality = QualityEnumToName(qual);
    return quality;
  }
}

public class ItemAddedNotification extends GenericNotificationController {

  protected edit inkImageRef m_itemImage;

  protected edit inkWidgetRef m_rarityBar;

  protected ItemIconGender m_itemIconGender;

  protected CName m_animationName;

  public void SetNotificationData(ref<GenericNotificationViewData> notificationData) {
    ref<ItemAddedNotificationViewData> data;
    data = Cast(notificationData);
    this.m_animationName = data.animation;
    SetIcon(GetTDBID(data.itemID), data.itemRarity);
    SetNotificationData(notificationData);
  }

  protected cb Bool OnPlayerAttach(ref<GameObject> playerPuppet) {
    this.m_itemIconGender = GetIconGender(RefToWeakRef(Cast(playerPuppet)));
  }

  private final void SetIcon(TweakDBID itemID, CName rarity) {
    String iconName;
    String iconPath;
    ref<IconsNameResolver> iconsNameResolver;
    wref<Item_Record> itemRecord;
    itemRecord = RefToWeakRef(GetItemRecord(itemID));
    iconsNameResolver = GetIconsNameResolver();
    iconPath = WeakRefToRef(itemRecord).IconPath();
    SetVisible(this.m_itemImage, false);
    if(IsStringValid(iconPath)) {
      iconName = iconPath;
    } else {
      iconName = NameToString(iconsNameResolver.TranslateItemToIconName(itemID, this.m_itemIconGender == ItemIconGender.Male));
    };
    if(iconName != "None" && iconName != "") {
      SetScale(this.m_itemImage, WeakRefToRef(WeakRefToRef(itemRecord).EquipArea()).Type() == gamedataEquipmentArea.Outfit ? new Vector2(0.5,0.5) : new Vector2(1,1));
      RequestSetImage(this, this.m_itemImage, "UIIcon." + iconName, "OnIconCallback");
    };
    UpdateRarity(rarity);
  }

  protected cb Bool OnIconCallback(ref<iconAtlasCallbackData> e) {
    SetVisible(this.m_itemImage, e.loadResult == inkIconResult.Success);
    PlayLibraryAnimation(this.m_animationName);
  }

  protected void UpdateRarity(CName rarity) {
    Bool visible;
    visible = rarity != "";
    visible = false;
    SetVisible(this.m_rarityBar, visible);
    SetState(this.m_rarityBar, rarity);
  }
}

public class ItemAddedNotificationViewData extends GenericNotificationViewData {

  public native ItemID itemID;

  public native CName animation;

  public native CName itemRarity;

  public Bool CanMerge(ref<GenericNotificationViewData> data) {
    ref<ItemAddedNotificationViewData> compareTo;
    compareTo = Cast(data);
    return ToBool(compareTo) && compareTo.itemID == this.itemID;
  }
}

public class TarotCardAddedNotification extends GenericNotificationController {

  protected edit inkImageRef m_cardImage;

  protected edit inkTextRef m_cardNameLabel;

  public void SetNotificationData(ref<GenericNotificationViewData> notificationData) {
    ref<TarotCardAddedNotificationViewData> data;
    data = Cast(notificationData);
    SetText(this.m_cardNameLabel, data.cardName);
    RequestSetImage(this, this.m_cardImage, "UIIcon." + NameToString(data.imagePart));
    PlayLibraryAnimation(data.animation);
    SetNotificationData(notificationData);
  }
}
