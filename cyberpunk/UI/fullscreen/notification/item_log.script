
public class ItemLog extends gameuiMenuGameController {

  private edit inkCompoundRef m_listRef;

  [Default(ItemLog, 1.0))]
  private edit Float m_initialPopupDelay;

  private array<ref<DisassemblePopupLogicController>> m_popupList;

  private array<InventoryItemData> m_listOfAddedInventoryItems;

  private wref<PlayerPuppet> m_player;

  private ref<InventoryDataManagerV2> m_InventoryManager;

  private ref<ItemLogUserData> m_data;

  private Int32 m_onScreenCount;

  private ref<inkAnimProxy> m_animProxy;

  private ref<inkAnimDef> m_alpha_fadein;

  private inkAnimOptions m_AnimOptions;

  protected cb Bool OnInitialize() {
    this.m_player = RefToWeakRef(Cast(GetOwnerEntity()));
    this.m_InventoryManager = new InventoryDataManagerV2();
    this.m_InventoryManager.Initialize(WeakRefToRef(this.m_player));
    RemoveAllChildren(this.m_listRef);
    this.m_data = Cast(WeakRefToRef(GetRootWidget()).GetUserData("ItemLogUserData"));
    WeakRefToRef(this.m_data.token).RegisterListener(this, "OnItemAdded");
  }

  protected cb Bool OnUninitialize() {
    this.m_InventoryManager.UnInitialize();
  }

  public final void ManagePopups() {
    ref<ItemLogUserData> userData;
    if(Size(this.m_listOfAddedInventoryItems) == 0 && this.m_onScreenCount < 0) {
      userData = new ItemLogUserData();
      userData.itemLogQueueEmpty = true;
      WeakRefToRef(this.m_data.token).TriggerCallback(userData);
    };
    if(Size(this.m_listOfAddedInventoryItems) > 0 && this.m_onScreenCount < 3 && !this.m_animProxy.IsPlaying()) {
      CreatePopup();
      CreatePopupDelay();
    };
  }

  private final void CreatePopup() {
    wref<ItemLogPopupLogicController> popup;
    popup = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(this.m_listRef), "itemLog_popup")).GetController())));
    WeakRefToRef(popup).SetupData(Pop(this.m_listOfAddedInventoryItems));
    WeakRefToRef(popup).RegisterToCallback("OnPopupComplete", this, "OnRemovePopup");
    this.m_onScreenCount += 1;
  }

  private final void CreatePopupDelay() {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(this.m_initialPopupDelay);
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_listRef, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnDelayComplete");
  }

  protected cb Bool OnItemAdded(ref<inkGameNotificationData> data) {
    ref<ItemLogUserData> userData;
    InventoryItemData tempData;
    ItemID itemID;
    wref<gameItemData> itemdata;
    userData = Cast(data);
    itemID = userData.itemID;
    if(userData.itemLogQueueEmpty) {
      return false;
    };
    if(IsValid(itemID)) {
      itemdata = GetTransactionSystem(WeakRefToRef(this.m_player).GetGame()).GetItemData(WeakRefToRef(this.m_player), itemID);
      tempData = this.m_InventoryManager.GetInventoryItemData(itemdata);
      if(!IsItemBlacklisted(itemdata)) {
        Push(this.m_listOfAddedInventoryItems, tempData);
        ManagePopups();
      };
    };
  }

  protected cb Bool OnRemovePopup(wref<inkWidget> widget) {
    RemoveChild(this.m_listRef, widget);
    this.m_onScreenCount -= 1;
    ManagePopups();
  }

  protected cb Bool OnDelayComplete(ref<inkAnimProxy> anim) {
    ManagePopups();
  }
}

public class ItemLogPopupLogicController extends inkLogicController {

  private edit inkTextRef m_quantity;

  private edit inkImageRef m_icon;

  private edit inkTextRef m_label;

  [Default(ItemLogPopupLogicController, 3.0f))]
  private edit Float m_duration;

  private ref<inkAnimProxy> m_animProxy;

  private ref<inkAnimDef> m_alpha_fadein;

  private inkAnimOptions m_AnimOptions;

  protected cb Bool OnInitialize()

  public final void SetupData(InventoryItemData itemData) {
    SetText(this.m_label, GetName(itemData));
    SetText(this.m_quantity, "x" + ToString(GetQuantity(itemData)));
    SetTexturePart(this.m_icon, StringToName(GetIconPath(itemData)));
    this.m_animProxy = PlayLibraryAnimation("AddPopup");
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnAddPopupComplete");
  }

  protected cb Bool OnAddPopupComplete(ref<inkAnimProxy> anim) {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(this.m_duration);
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_quantity, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnPopupDurationComplete");
  }

  protected cb Bool OnPopupDurationComplete(ref<inkAnimProxy> anim) {
    CallCustomCallback("OnPopupComplete");
  }
}
