
public class CyberwareMainGameController extends inkGameController {

  private edit inkWidgetRef m_MainViewRoot;

  private edit inkCompoundRef m_CyberwareColumnLeft;

  private edit inkCompoundRef m_CyberwareColumnRight;

  private edit inkCompoundRef m_personalStatsList;

  private edit inkCompoundRef m_attributesList;

  private edit inkCompoundRef m_resistancesList;

  private edit inkWidgetRef m_TooltipsManagerRef;

  private wref<gameuiTooltipsManager> m_TooltipsManager;

  private ref<InventoryDataManagerV2> m_InventoryManager;

  private wref<PlayerPuppet> m_player;

  private CName m_resistanceView;

  private CName m_statView;

  private inkMargin m_toolTipOffset;

  private array<StatViewData> m_rawStatsData;

  protected cb Bool OnInitialize() {
    this.m_TooltipsManager = RefToWeakRef(Cast(WeakRefToRef(GetControllerByType(this.m_TooltipsManagerRef, "gameuiTooltipsManager"))));
    this.m_player = RefToWeakRef(Cast(GetPlayerControlledObject()));
    this.m_InventoryManager = new InventoryDataManagerV2();
    this.m_InventoryManager.Initialize(WeakRefToRef(this.m_player));
    this.m_InventoryManager.GetPlayerStats(this.m_rawStatsData);
    RemoveAllChildren(this.m_CyberwareColumnLeft);
    RemoveAllChildren(this.m_CyberwareColumnRight);
    this.m_toolTipOffset.left = 60;
    this.m_toolTipOffset.top = 5;
    RemoveAllChildren(this.m_personalStatsList);
    RemoveAllChildren(this.m_attributesList);
    RemoveAllChildren(this.m_resistancesList);
    this.m_resistanceView = "resistanceView";
    this.m_statView = "statView";
    SetupBB();
    PrepareTooltips();
    PrepareCyberwareSlots();
    PopulateStats();
    OnIntro();
  }

  protected cb Bool OnUninitialize() {
    this.m_InventoryManager.UnInitialize();
    RemoveBB();
  }

  private final void SetupBB()

  private final void RemoveBB()

  private final void PrepareCyberwareSlots() {
    AddCyberwareSlot(gamedataEquipmentArea.SystemReplacementCW, this.m_CyberwareColumnRight);
    AddCyberwareSlot(gamedataEquipmentArea.EyesCW, this.m_CyberwareColumnRight);
    AddCyberwareSlot(gamedataEquipmentArea.HandsCW, this.m_CyberwareColumnRight);
    AddCyberwareSlot(gamedataEquipmentArea.ArmsCW, this.m_CyberwareColumnRight);
    AddCyberwareSlot(gamedataEquipmentArea.LegsCW, this.m_CyberwareColumnRight);
    AddCyberwareSlot(gamedataEquipmentArea.MusculoskeletalSystemCW, this.m_CyberwareColumnLeft);
    AddCyberwareSlot(gamedataEquipmentArea.NervousSystemCW, this.m_CyberwareColumnLeft);
    AddCyberwareSlot(gamedataEquipmentArea.CardiovascularSystemCW, this.m_CyberwareColumnLeft);
    AddCyberwareSlot(gamedataEquipmentArea.ImmuneSystemCW, this.m_CyberwareColumnLeft);
    AddCyberwareSlot(gamedataEquipmentArea.IntegumentarySystemCW, this.m_CyberwareColumnLeft);
  }

  public final void PopulateStats() {
    AddStat(gamedataStatType.Health, this.m_personalStatsList);
    AddStat(gamedataStatType.Evasion, this.m_personalStatsList);
    AddStat(gamedataStatType.Accuracy, this.m_personalStatsList);
    AddStat(gamedataStatType.CarryCapacity, this.m_personalStatsList);
    AddStat(gamedataStatType.Reflexes, this.m_attributesList);
    AddStat(gamedataStatType.Intelligence, this.m_attributesList);
    AddStat(gamedataStatType.TechnicalAbility, this.m_attributesList);
    AddStat(gamedataStatType.Strength, this.m_attributesList);
    AddStat(gamedataStatType.Cool, this.m_attributesList);
    AddStat(gamedataStatType.PhysicalResistance, this.m_resistancesList, this.m_resistanceView);
    AddStat(gamedataStatType.ThermalResistance, this.m_resistancesList, this.m_resistanceView);
    AddStat(gamedataStatType.ElectricResistance, this.m_resistancesList, this.m_resistanceView);
    AddStat(gamedataStatType.ChemicalResistance, this.m_resistancesList, this.m_resistanceView);
  }

  private final void AddStat(gamedataStatType statType, inkCompoundRef list, CName viewElement?) {
    StatViewData statData;
    wref<StatsViewController> statView;
    if(viewElement == "") {
      viewElement = this.m_statView;
    };
    statData = RequestStat(statType);
    statView = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(list), viewElement)).GetControllerByType("StatsViewController"))));
    WeakRefToRef(statView).Setup(statData);
  }

  private final void AddCyberwareSlot(gamedataEquipmentArea equipArea, inkCompoundRef parentRef) {
    wref<CyberwareSlot> cybSlot;
    Int32 numSlots;
    cybSlot = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(parentRef), "cyberware_slot")).GetControllerByType("CyberwareSlot"))));
    if(ToBool(cybSlot)) {
      numSlots = this.m_InventoryManager.GetNumberOfSlots(equipArea);
      WeakRefToRef(cybSlot).Setup(equipArea, numSlots);
      WeakRefToRef(cybSlot).RegisterToCallback("OnHoverOver", this, "OnCyberwareSlotHoverOver");
      WeakRefToRef(cybSlot).RegisterToCallback("OnHoverOut", this, "OnCyberwareSlotHoverOut");
    };
  }

  protected cb Bool OnCyberwareSlotHoverOver(ref<inkPointerEvent> evt) {
    wref<CyberwareSlot> cyberwareSlot;
    cyberwareSlot = RefToWeakRef(GetCyberwareSlotControllerFromTarget(evt));
    OnCyberwareRequestTooltip(cyberwareSlot);
  }

  protected cb Bool OnCyberwareSlotHoverOut(ref<inkPointerEvent> evt) {
    HideTooltips();
  }

  private final void PrepareTooltips() {
    this.m_TooltipsManager = RefToWeakRef(Cast(WeakRefToRef(GetControllerByType(this.m_TooltipsManagerRef, "gameuiTooltipsManager"))));
    WeakRefToRef(this.m_TooltipsManager).Setup(ETooltipsStyle.Menus);
  }

  private final void OnCyberwareRequestTooltip(wref<CyberwareSlot> slot) {
    ref<CyberwareTooltipData> tooltipsData;
    Int32 i;
    tooltipsData = new CyberwareTooltipData();
    if(ToBool(slot)) {
      tooltipsData.label = ToString(WeakRefToRef(slot).GetEquipmentArea());
      i = 0;
      while(i < WeakRefToRef(slot).GetNumSlots()) {
        tooltipsData.AddCyberwareSlotItemData(this.m_InventoryManager.GetItemDataEquippedInArea(WeakRefToRef(slot).GetEquipmentArea(), i));
        i += 1;
      };
      WeakRefToRef(this.m_TooltipsManager).ShowTooltip(0, tooltipsData, this.m_toolTipOffset);
    };
  }

  private final void HideTooltips() {
    WeakRefToRef(this.m_TooltipsManager).HideTooltips();
  }

  private final void OnIntro()

  private final ref<CyberwareSlot> GetCyberwareSlotControllerFromTarget(ref<inkPointerEvent> evt) {
    ref<inkWidget> widget;
    wref<CyberwareSlot> controller;
    widget = WeakRefToRef(evt.GetCurrentTarget());
    controller = RefToWeakRef(Cast(WeakRefToRef(widget.GetController())));
    return WeakRefToRef(controller);
  }

  private final StatViewData RequestStat(gamedataStatType stat) {
    StatViewData data;
    Int32 i;
    i = 0;
    while(i < Size(this.m_rawStatsData)) {
      if(this.m_rawStatsData[i].type == stat) {
        return this.m_rawStatsData[i];
      };
      i += 1;
    };
    return data;
  }
}
