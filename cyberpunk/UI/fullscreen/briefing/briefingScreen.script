
public class BriefingScreen extends inkHUDGameController {

  public edit inkWidgetRef m_logicControllerRef;

  protected wref<JournalManager> m_journalManager;

  private Uint32 m_bbOpenerEventID;

  private Uint32 m_bbSizeEventID;

  private Uint32 m_bbAlignmentEventID;

  protected cb Bool OnInitialize() {
    this.m_journalManager = RefToWeakRef(GetJournalManager(GetPlayerControlledObject().GetGame()));
    this.m_bbOpenerEventID = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Briefing).RegisterDelayedListenerString(GetAllBlackboardDefs().UI_Briefing.BriefingToOpen, this, "OnBriefingOpenerCalled");
    this.m_bbSizeEventID = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Briefing).RegisterDelayedListenerVariant(GetAllBlackboardDefs().UI_Briefing.BriefingSize, this, "OnBriefingSizeCalled");
    this.m_bbAlignmentEventID = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Briefing).RegisterDelayedListenerVariant(GetAllBlackboardDefs().UI_Briefing.BriefingAlignment, this, "OnBriefingAlignmentCalled");
  }

  protected cb Bool OnBriefingOpenerCalled(String value) {
    JournalRequestContext context;
    array<wref<JournalEntry>> entries;
    array<wref<JournalEntry>> childEntries;
    ref<BriefingScreenLogic> logicController;
    String parent;
    String target;
    wref<JournalEntry> parentEntry;
    wref<JournalEntry> targetEntry;
    StrSplitLast(StrLower(value), "/", parent, target);
    context.stateFilter.active = true;
    WeakRefToRef(this.m_journalManager).GetBriefings(context, entries);
    parentEntry = RefToWeakRef(FindEntry(parent, entries));
    WeakRefToRef(this.m_journalManager).GetChildren(parentEntry, context.stateFilter, childEntries);
    targetEntry = RefToWeakRef(FindEntry(target, childEntries));
    logicController = Cast(WeakRefToRef(GetController(this.m_logicControllerRef)));
    logicController.ShowBriefing(targetEntry);
  }

  protected cb Bool OnBriefingSizeCalled(Variant value) {
    ref<BriefingScreenLogic> logicController;
    logicController = Cast(WeakRefToRef(GetController(this.m_logicControllerRef)));
    logicController.SetSize(FromVariant(value));
  }

  protected cb Bool OnBriefingAlignmentCalled(Variant value) {
    ref<BriefingScreenLogic> logicController;
    logicController = Cast(WeakRefToRef(GetController(this.m_logicControllerRef)));
    logicController.SetAlignment(FromVariant(value));
  }

  private final ref<JournalEntry> FindEntry(String toFind, array<wref<JournalEntry>> entries) {
    Int32 i;
    i = 0;
    while(i < Size(entries)) {
      if(WeakRefToRef(entries[i]).GetId() == toFind) {
        return WeakRefToRef(entries[i]);
      };
      i += 1;
    };
    return null;
  }

  protected cb Bool OnUninitialize() {
    GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Briefing).UnregisterDelayedListener(GetAllBlackboardDefs().UI_Briefing.BriefingToOpen, this.m_bbOpenerEventID);
    GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Briefing).UnregisterDelayedListener(GetAllBlackboardDefs().UI_Briefing.BriefingSize, this.m_bbSizeEventID);
    GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Briefing).UnregisterDelayedListener(GetAllBlackboardDefs().UI_Briefing.BriefingAlignment, this.m_bbAlignmentEventID);
  }
}

public class BriefingScreenLogic extends inkLogicController {

  protected Vector2 m_lastSizeSet;

  protected Bool m_isBriefingVisible;

  protected wref<JournalEntry> m_briefingToOpen;

  private edit inkVideoRef m_videoWidget;

  private edit inkWidgetRef m_mapWidget;

  private edit inkWidgetRef m_paperdollWidget;

  private edit inkWidgetRef m_animatedWidget;

  private edit Float m_fadeDuration;

  private edit inkanimInterpolationType m_InterpolationType;

  private edit inkanimInterpolationMode m_InterpolationMode;

  private edit Vector2 m_minimizedSize;

  private edit Vector2 m_maximizedSize;

  protected cb Bool OnInitialize() {
    HideAll();
    this.m_lastSizeSet = this.m_minimizedSize;
    this.m_isBriefingVisible = false;
  }

  public final void ShowBriefing(wref<JournalEntry> briefingToOpen) {
    this.m_briefingToOpen = briefingToOpen;
    if(this.m_isBriefingVisible) {
      Fade(1, 0, "OnFadeOutEnd");
    } else {
      SetBriefing();
    };
  }

  private final void SetBriefing() {
    ref<JournalBriefingBaseSection> toOpen;
    HideAll();
    if(ToBool(this.m_briefingToOpen)) {
      toOpen = Cast(WeakRefToRef(this.m_briefingToOpen));
      switch(toOpen.GetType()) {
        case gameJournalBriefingContentType.MapLocation:
          ProcessMap(Cast(WeakRefToRef(this.m_briefingToOpen)));
          break;
        case gameJournalBriefingContentType.VideoContent:
          ProcessVideo(Cast(WeakRefToRef(this.m_briefingToOpen)));
          break;
        case gameJournalBriefingContentType.Paperdoll:
          ProcessPaperdoll(Cast(WeakRefToRef(this.m_briefingToOpen)));
      };
      Fade(0, 1, "OnFadeInEnd");
      this.m_briefingToOpen = null;
    };
  }

  protected cb Bool OnFadeInEnd(ref<inkAnimProxy> proxy) {
    this.m_isBriefingVisible = true;
  }

  protected cb Bool OnFadeOutEnd(ref<inkAnimProxy> proxy) {
    this.m_isBriefingVisible = false;
    SetBriefing();
  }

  private final void ProcessMap(ref<JournalBriefingMapSection> toProcess) {
    WeakRefToRef(GetRootWidget()).SetVisible(true);
    SetVisible(this.m_mapWidget, true);
  }

  private final void ProcessVideo(ref<JournalBriefingVideoSection> toProcess) {
    WeakRefToRef(GetRootWidget()).SetVisible(true);
    SetVisible(this.m_videoWidget, true);
    SetVideoPath(this.m_videoWidget, toProcess.GetVideoPath());
    Play(this.m_videoWidget);
  }

  private final void ProcessPaperdoll(ref<JournalBriefingPaperDollSection> toProcess) {
    WeakRefToRef(GetRootWidget()).SetVisible(true);
    SetVisible(this.m_paperdollWidget, true);
  }

  private final void Fade(Float startValue, Float endValue, CName callbackName) {
    ref<inkAnimProxy> animProxy;
    ref<inkAnimDef> anim;
    ref<inkAnimTransparency> alphaInterpolator;
    anim = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(this.m_fadeDuration);
    alphaInterpolator.SetStartTransparency(startValue);
    alphaInterpolator.SetEndTransparency(endValue);
    alphaInterpolator.SetMode(this.m_InterpolationMode);
    alphaInterpolator.SetType(this.m_InterpolationType);
    anim.AddInterpolator(alphaInterpolator);
    animProxy = PlayAnimation(this.m_animatedWidget, anim);
    animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, callbackName);
  }

  public final void SetSize(questJournalSizeEventType sizeToSet) {
    ref<inkAnimProxy> animProxy;
    ref<inkAnimDef> anim;
    ref<inkAnimSize> sizeInterpolator;
    Vector2 targetSize;
    anim = new inkAnimDef();
    sizeInterpolator = new inkAnimSize();
    switch(sizeToSet) {
      case questJournalSizeEventType.Maximize:
        targetSize = this.m_maximizedSize;
        break;
      case questJournalSizeEventType.Minimize:
        targetSize = this.m_minimizedSize;
    };
    sizeInterpolator.SetStartSize(this.m_lastSizeSet);
    sizeInterpolator.SetEndSize(targetSize);
    sizeInterpolator.SetDuration(this.m_fadeDuration);
    sizeInterpolator.SetMode(this.m_InterpolationMode);
    sizeInterpolator.SetType(this.m_InterpolationType);
    anim.AddInterpolator(sizeInterpolator);
    animProxy = PlayAnimation(this.m_animatedWidget, anim);
    this.m_lastSizeSet = targetSize;
  }

  public final void SetAlignment(questJournalAlignmentEventType alignmentToSet) {
    inkEAnchor targetAnchor;
    Float xAnchorPoint;
    switch(alignmentToSet) {
      case questJournalAlignmentEventType.Left:
        targetAnchor = inkEAnchor.TopLeft;
        xAnchorPoint = 0;
        break;
      case questJournalAlignmentEventType.Center:
        targetAnchor = inkEAnchor.TopCenter;
        xAnchorPoint = 0.5;
        break;
      case questJournalAlignmentEventType.Right:
        targetAnchor = inkEAnchor.TopRight;
        xAnchorPoint = 1;
    };
    SetAnchor(this.m_animatedWidget, targetAnchor);
    SetAnchorPoint(this.m_animatedWidget, xAnchorPoint, 0);
  }

  private final void HideAll() {
    WeakRefToRef(GetRootWidget()).SetVisible(false);
    SetVisible(this.m_videoWidget, false);
    SetVisible(this.m_mapWidget, false);
    SetVisible(this.m_paperdollWidget, false);
  }
}
