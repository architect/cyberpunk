
public class MessengerContactDataView extends VirtualNestedListDataView {

  protected void SortItems(ref<CompareBuilder> compareBuilder, ref<VirutalNestedListData> left, ref<VirutalNestedListData> right) {
    ref<ContactData> leftData;
    ref<ContactData> rightData;
    leftData = Cast(left.m_data);
    rightData = Cast(right.m_data);
    if(ToBool(leftData) && ToBool(rightData)) {
      compareBuilder.BoolTrue(Size(leftData.unreadMessages) > 0, Size(rightData.unreadMessages) > 0).GameTimeDesc(leftData.timeStamp, rightData.timeStamp);
    };
  }
}

public class MessengerContactItemVirtualController extends inkVirtualCompoundItemController {

  private edit inkTextRef m_label;

  private edit inkTextRef m_msgPreview;

  private edit inkTextRef m_msgCounter;

  private edit inkWidgetRef m_msgIndicator;

  private edit inkWidgetRef m_replyAlertIcon;

  private edit inkWidgetRef m_collapseIcon;

  private edit inkImageRef m_image;

  private ref<ContactData> m_contactData;

  private ref<VirutalNestedListData> m_nestedListData;

  private MessengerContactType m_type;

  private wref<MessengerContactSyncData> m_activeItemSync;

  private Bool m_isContactActive;

  private Bool m_isItemHovered;

  private Bool m_isItemToggled;

  protected cb Bool OnInitialize() {
    RegisterToCallback("OnToggledOn", this, "OnToggledOn");
    RegisterToCallback("OnToggledOff", this, "OnToggledOff");
    RegisterToCallback("OnSelected", this, "OnSelected");
    RegisterToCallback("OnDeselected", this, "OnDeselected");
  }

  public final void OnDataChanged(Variant value) {
    this.m_nestedListData = Cast(FromVariant(value));
    this.m_contactData = Cast(this.m_nestedListData.m_data);
    this.m_activeItemSync = this.m_contactData.activeDataSync;
    if(this.m_nestedListData.m_collapsable) {
      this.m_type = MessengerContactType.Group;
    } else {
      if(this.m_nestedListData.m_widgetType == 1) {
        this.m_type = MessengerContactType.Thread;
      } else {
        this.m_type = MessengerContactType.Contact;
      };
    };
    if(Size(this.m_contactData.unreadMessages) > 0) {
      SetVisible(this.m_msgCounter, true);
      SetText(this.m_msgCounter, ToString(Size(this.m_contactData.unreadMessages)));
    } else {
      SetVisible(this.m_msgCounter, false);
    };
    if(this.m_contactData.playerIsLastSender) {
      SetText(this.m_msgPreview, GetLocalizedTextByKey("UI-Phone-LabelYou") + this.m_contactData.lastMesssagePreview);
    } else {
      SetText(this.m_msgPreview, this.m_contactData.lastMesssagePreview);
    };
    SetVisible(this.m_replyAlertIcon, this.m_contactData.playerCanReply && this.m_type != MessengerContactType.Group);
    SetText(this.m_label, this.m_contactData.localizedName);
    if(IsValid(this.m_contactData.avatarID)) {
      SetVisible(this.m_image, true);
      RequestSetImage(this, this.m_image, this.m_contactData.avatarID);
    };
    if(IsValid(this.m_collapseIcon)) {
      SetVisible(this.m_collapseIcon, this.m_nestedListData.m_collapsable);
    };
    UpdateState();
  }

  protected cb Bool OnContactSyncData(ref<MessengerContactSyncBackEvent> evt) {
    UpdateState();
  }

  protected cb Bool OnMessengerThreadSelectedEvent(ref<MessengerThreadSelectedEvent> evt) {
    Remove(this.m_contactData.unreadMessages, Cast(evt.m_hash));
    if(Size(this.m_contactData.unreadMessages) > 0) {
      SetVisible(this.m_msgCounter, true);
      SetText(this.m_msgCounter, ToString(Size(this.m_contactData.unreadMessages)));
    } else {
      SetVisible(this.m_msgCounter, false);
    };
  }

  protected cb Bool OnToggledOn(wref<inkVirtualCompoundItemController> itemController) {
    ref<MessengerContactSelectedEvent> evt;
    evt = new MessengerContactSelectedEvent();
    evt.m_entryHash = this.m_contactData.hash;
    evt.m_level = this.m_nestedListData.m_level;
    evt.m_type = this.m_type;
    QueueEvent(evt);
    this.m_isItemToggled = true;
  }

  protected cb Bool OnToggledOff(wref<inkVirtualCompoundItemController> itemController) {
    this.m_isItemToggled = false;
    UpdateState();
  }

  protected cb Bool OnSelected(wref<inkVirtualCompoundItemController> itemController, Bool discreteNav) {
    this.m_isItemHovered = true;
    UpdateState();
    if(discreteNav) {
      SetCursorOverWidget(GetRootWidget());
    };
  }

  protected cb Bool OnDeselected(wref<inkVirtualCompoundItemController> itemController) {
    this.m_isItemHovered = false;
    UpdateState();
  }

  private final void UpdateState() {
    if(WeakRefToRef(this.m_activeItemSync).m_entryHash == this.m_contactData.hash) {
      WeakRefToRef(GetRootWidget()).SetState("Active");
    } else {
      if(WeakRefToRef(this.m_activeItemSync).m_level == this.m_nestedListData.m_level && this.m_type == MessengerContactType.Group) {
        WeakRefToRef(GetRootWidget()).SetState("SubActive");
      } else {
        if(this.m_isItemHovered) {
          WeakRefToRef(GetRootWidget()).SetState("Hover");
        } else {
          WeakRefToRef(GetRootWidget()).SetState("Default");
        };
      };
    };
  }
}
