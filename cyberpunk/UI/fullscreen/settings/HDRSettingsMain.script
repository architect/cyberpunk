
public class HDRSettingsVarListener extends ConfigVarListener {

  private wref<HDRSettingsGameController> m_ctrl;

  public final void RegisterController(ref<HDRSettingsGameController> ctrl) {
    this.m_ctrl = RefToWeakRef(ctrl);
  }

  public void OnVarModified(CName groupPath, CName varName, ConfigVarType varType, ConfigChangeReason reason) {
    Log("HDRSettingsVarListener::OnVarModified");
    WeakRefToRef(this.m_ctrl).OnVarModified(groupPath, varName, varType, reason);
  }
}

public class HDRSettingsGameController extends gameuiMenuGameController {

  private CName s_maxBrightnessGroup;

  private CName s_paperWhiteGroup;

  private CName s_toneMappingeGroup;

  private CName s_calibrationImageDay;

  private CName s_calibrationImageNight;

  private CName s_currentCalibrationImage;

  private edit inkCompoundRef m_paperWhiteOptionSelector;

  private edit inkCompoundRef m_maxBrightnessOptionSelector;

  private edit inkCompoundRef m_toneMappingOptionSelector;

  private edit inkWidgetRef m_targetImageWidget;

  private wref<inkMenuEventDispatcher> m_menuEventDispatcher;

  private ref<UserSettings> m_settings;

  private ref<HDRSettingsVarListener> m_settingsListener;

  private array<wref<SettingsSelectorController>> m_SettingsElements;

  private Bool m_isPreGame;

  private ref<inkAnimDef> m_calibrationImagesCycleAnimDef;

  private ref<inkAnimProxy> m_calibrationImagesCycleProxy;

  protected cb Bool OnInitialize() {
    this.s_maxBrightnessGroup = "/video/display";
    this.s_paperWhiteGroup = "/video/display";
    this.s_toneMappingeGroup = "/video/display";
    this.s_calibrationImageDay = "hdr_day";
    this.s_calibrationImageNight = "hdr_night";
    this.m_settings = WeakRefToRef(GetSystemRequestsHandler()).GetUserSettings();
    this.m_isPreGame = WeakRefToRef(GetSystemRequestsHandler()).IsPreGame();
    this.m_settingsListener = new HDRSettingsVarListener();
    this.m_settingsListener.RegisterController(this);
    this.m_settingsListener.Register(this.s_paperWhiteGroup);
    this.m_settingsListener.Register(this.s_maxBrightnessGroup);
    this.m_settingsListener.Register(this.s_toneMappingeGroup);
    RegisterToGlobalInputCallback("OnPostOnRelease", this, "OnButtonRelease");
    Clear(this.m_SettingsElements);
    SetOptionSelector("MaxMonitorBrightness");
    SetOptionSelector("PaperWhiteLevel");
    SetOptionSelector("TonemappingMidpoint");
    PrepareHDRCycleAnimations();
    this.s_currentCalibrationImage = this.s_calibrationImageNight;
    SetTexturePart(this.s_currentCalibrationImage);
  }

  protected cb Bool OnUninitialize() {
    UnregisterFromGlobalInputCallback("OnPostOnRelease", this, "OnButtonRelease");
    SetHDRCalibrationScreen(false);
  }

  private final void SetOptionSelector(CName optionName) {
    ref<ConfigVar> option;
    wref<SettingsSelectorController> selector;
    inkCompoundRef selectorWidget;
    if(optionName == "MaxMonitorBrightness") {
      selectorWidget = this.m_maxBrightnessOptionSelector;
    } else {
      if(optionName == "PaperWhiteLevel") {
        selectorWidget = this.m_paperWhiteOptionSelector;
      } else {
        selectorWidget = this.m_toneMappingOptionSelector;
      };
    };
    option = this.m_settings.GetVar(this.s_maxBrightnessGroup, optionName);
    if(this.m_isPreGame ? option.IsInPreGame() : option.IsInGame()) {
      selector = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(Get(selectorWidget)).GetController())));
      if(ToBool(selector)) {
        WeakRefToRef(selector).Setup(option, this.m_isPreGame);
        Push(this.m_SettingsElements, selector);
      };
    };
  }

  public final void OnVarModified(CName groupPath, CName varName, ConfigVarType varType, ConfigChangeReason reason) {
    Int32 i;
    Int32 size;
    ref<SettingsSelectorController> item;
    Log("[VAR] modified groupPath: " + NameToString(groupPath) + " varName: " + NameToString(varName));
    size = Size(this.m_SettingsElements);
    i = 0;
    while(i < size) {
      item = WeakRefToRef(this.m_SettingsElements[i]);
      if(item.GetGroupPath() == groupPath && item.GetVarName() == varName) {
        WeakRefToRef(this.m_SettingsElements[i]).Refresh();
      };
      i += 1;
    };
  }

  private final void PrepareHDRCycleAnimations() {
    ref<inkAnimTransparency> alphaInterpolator;
    inkAnimOptions options;
    options.loopType = inkanimLoopType.Cycle;
    options.loopInfinite = true;
    this.m_calibrationImagesCycleAnimDef = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetStartTransparency(0.0020000000949949026);
    alphaInterpolator.SetEndTransparency(0.0024999999441206455);
    alphaInterpolator.SetDuration(5);
    alphaInterpolator.SetStartDelay(0.25);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_calibrationImagesCycleAnimDef.AddInterpolator(alphaInterpolator);
    this.m_calibrationImagesCycleProxy = PlayAnimationWithOptions(this.m_targetImageWidget, this.m_calibrationImagesCycleAnimDef, options);
    this.m_calibrationImagesCycleProxy.RegisterToCallback(inkanimEventType.OnEndLoop, this, "OnCalibrationImageEndLoop");
    this.m_calibrationImagesCycleProxy.RegisterToCallback(inkanimEventType.OnStart, this, "OnCalibrationImageAnimStart");
  }

  protected cb Bool OnCalibrationImageAnimStart(ref<inkAnimProxy> anim) {
    SetHDRCalibrationScreen(true);
  }

  protected cb Bool OnCalibrationImageEndLoop(ref<inkAnimProxy> anim) {
    if(this.s_currentCalibrationImage == this.s_calibrationImageNight) {
      this.s_currentCalibrationImage = this.s_calibrationImageDay;
    } else {
      this.s_currentCalibrationImage = this.s_calibrationImageNight;
    };
    SetTexturePart(this.s_currentCalibrationImage);
  }

  protected cb Bool OnSetMenuEventDispatcher(wref<inkMenuEventDispatcher> menuEventDispatcher) {
    this.m_menuEventDispatcher = menuEventDispatcher;
    WeakRefToRef(this.m_menuEventDispatcher).RegisterToEvent("OnBack", this, "OnBack");
  }

  private final native void SetTexturePart(CName partName)

  private final native void SetHDRCalibrationScreen(Bool enabled)

  private final native void SetRenderGameInBackground(Bool enabled)
}
