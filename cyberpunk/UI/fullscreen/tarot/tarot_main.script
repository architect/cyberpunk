
public class TarotMainGameController extends gameuiMenuGameController {

  protected edit inkWidgetRef m_buttonHintsManagerRef;

  private edit inkWidgetRef m_TooltipsManagerRef;

  private edit inkCompoundRef m_list;

  protected wref<JournalManager> m_journalManager;

  private wref<ButtonHints> buttonHintsController;

  private wref<gameuiTooltipsManager> m_TooltipsManager;

  private wref<tarotCardLogicController> m_selectedTarotCard;

  private ref<TarotPreviewGameController> m_fullscreenPreviewController;

  private wref<inkMenuEventDispatcher> m_menuEventDispatcher;

  private ref<inkGameNotificationToken> m_tarotPreviewPopupToken;

  private Bool m_afterCloseRequest;

  [Default(TarotMainGameController, 22))]
  private Int32 numberOfCardsInTarotDeck;

  protected cb Bool OnInitialize() {
    this.buttonHintsController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromExternal(Get(this.m_buttonHintsManagerRef), "base\gameplay\gui\common\buttonhints.inkwidget", "Root")).GetController())));
    WeakRefToRef(this.buttonHintsController).AddButtonHint("back", GetLocalizedText("Common-Access-Close"));
    this.m_journalManager = RefToWeakRef(GetJournalManager(GetPlayerControlledObject().GetGame()));
    WeakRefToRef(this.m_journalManager).RegisterScriptCallback(this, "OnJournalReady", gameJournalListenerType.State);
    RemoveAllChildren(this.m_list);
    PrepareTooltips();
    PushCodexData();
    PlayLibraryAnimation("panel_intro");
  }

  protected cb Bool OnUninitalize() {
    WeakRefToRef(this.m_menuEventDispatcher).UnregisterFromEvent("OnBack", this, "OnBack");
  }

  protected cb Bool OnSetMenuEventDispatcher(wref<inkMenuEventDispatcher> menuEventDispatcher) {
    this.m_menuEventDispatcher = menuEventDispatcher;
    WeakRefToRef(this.m_menuEventDispatcher).RegisterToEvent("OnBack", this, "OnBack");
  }

  protected cb Bool OnTarotCardPreviewShowRequest(ref<TarotCardPreviewPopupEvent> evt) {
    this.m_tarotPreviewPopupToken = ShowGameNotification(evt.m_data);
    this.m_tarotPreviewPopupToken.RegisterListener(this, "OnTarotPreviewPopup");
  }

  protected cb Bool OnTarotPreviewPopup(ref<inkGameNotificationData> data) {
    this.m_tarotPreviewPopupToken = null;
  }

  protected cb Bool OnBack(ref<IScriptable> userData) {
    if(!this.m_afterCloseRequest && !HasRestriction(RefToWeakRef(GetPlayerControlledObject()), "LockInHubMenu")) {
      WeakRefToRef(this.m_menuEventDispatcher).SpawnEvent("OnCloseHubMenu");
    } else {
      this.m_afterCloseRequest = false;
    };
  }

  protected cb Bool OnJournalReady(Uint32 entryHash, CName className, JournalNotifyOption notifyOption, JournalChangeType changeType) {
    ref<JournalTarotGroup> entryCodex;
    entryCodex = Cast(WeakRefToRef(WeakRefToRef(this.m_journalManager).GetEntry(entryHash)));
    if(entryCodex != null) {
      PushCodexData();
    };
  }

  private final void PushCodexData() {
    JournalRequestContext journalContext;
    array<wref<JournalEntry>> groupEntries;
    array<wref<JournalEntry>> tarotEntries;
    wref<JournalTarot> entry;
    array<TarotCardData> dataArray;
    array<TarotCardData> sortedArray;
    TarotCardData data;
    Int32 i;
    Int32 j;
    Bool cardFound;
    journalContext.stateFilter.active = true;
    WeakRefToRef(this.m_journalManager).GetTarots(journalContext, groupEntries);
    i = 0;
    while(i < Size(groupEntries)) {
      Clear(tarotEntries);
      WeakRefToRef(this.m_journalManager).GetChildren(groupEntries[i], journalContext.stateFilter, tarotEntries);
      i = 0;
      while(i < Size(tarotEntries)) {
        entry = RefToWeakRef(Cast(WeakRefToRef(tarotEntries[i])));
        data.empty = false;
        data.index = WeakRefToRef(entry).GetIndex();
        data.imagePath = WeakRefToRef(entry).GetImagePart();
        data.label = WeakRefToRef(entry).GetName();
        data.desc = WeakRefToRef(entry).GetDescription();
        Push(dataArray, data);
        i += 1;
      };
      i += 1;
    };
    i = 0;
    while(i < this.numberOfCardsInTarotDeck) {
      cardFound = false;
      j = 0;
      while(j < Size(dataArray)) {
        if(dataArray[j].index == i) {
          Push(sortedArray, dataArray[j]);
          cardFound = true;
        } else {
          j += 1;
        };
      };
      if(!cardFound) {
        data.empty = true;
        data.index = i;
        data.imagePath = "";
        data.label = "";
        data.desc = "";
        Push(sortedArray, data);
      };
      i += 1;
    };
    CreateTarotCards(sortedArray);
  }

  protected cb Bool OnUninitialize() {
    WeakRefToRef(this.m_journalManager).UnregisterScriptCallback(this, "OnJournalReady");
  }

  private final void CreateTarotCards(array<TarotCardData> data) {
    Int32 i;
    wref<tarotCardLogicController> tarotCard;
    i = 0;
    while(i < Size(data)) {
      tarotCard = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(this.m_list), "tarotCard")).GetController())));
      WeakRefToRef(tarotCard).SetupData(data[i]);
      WeakRefToRef(tarotCard).RegisterToCallback("OnRelease", this, "OnElementClick");
      WeakRefToRef(tarotCard).RegisterToCallback("OnHoverOver", this, "OnElementHoverOver");
      WeakRefToRef(tarotCard).RegisterToCallback("OnHoverOut", this, "OnElementHoverOut");
      i += 1;
    };
  }

  protected cb Bool OnElementClick(ref<inkPointerEvent> evt) {
    wref<tarotCardLogicController> controller;
    TarotCardData data;
    ref<TarotCardPreviewPopupEvent> previewEvent;
    ref<TarotCardPreviewData> previewEventData;
    if(evt.IsAction("click")) {
      controller = RefToWeakRef(GetTarotCardControllerFromTarget(evt));
      data = WeakRefToRef(controller).GetData();
      if(!data.empty) {
        previewEvent = new TarotCardPreviewPopupEvent();
        previewEvent.m_data = new TarotCardPreviewData();
        previewEvent.m_data.cardData = data;
        previewEvent.m_data.queueName = "modal_popup";
        previewEvent.m_data.notificationName = "base\gameplay\gui\widgets\notifications\tarot_card_preview.inkwidget";
        previewEvent.m_data.isBlocking = true;
        previewEvent.m_data.useCursor = true;
        QueueBroadcastEvent(previewEvent);
        HideTooltips();
        WeakRefToRef(this.m_selectedTarotCard).HoverOut();
      };
    };
  }

  protected cb Bool OnElementHoverOver(ref<inkPointerEvent> evt) {
    wref<tarotCardLogicController> controller;
    TarotCardData data;
    controller = RefToWeakRef(GetTarotCardControllerFromTarget(evt));
    data = WeakRefToRef(controller).GetData();
    if(!data.empty) {
      RequestTooltip(WeakRefToRef(controller).GetData());
    };
    this.m_selectedTarotCard = controller;
    WeakRefToRef(this.m_selectedTarotCard).HoverOver();
  }

  protected cb Bool OnElementHoverOut(ref<inkPointerEvent> evt) {
    HideTooltips();
    WeakRefToRef(this.m_selectedTarotCard).HoverOut();
  }

  private final void PrepareTooltips() {
    this.m_TooltipsManager = RefToWeakRef(Cast(WeakRefToRef(GetControllerByType(this.m_TooltipsManagerRef, "gameuiTooltipsManager"))));
    WeakRefToRef(this.m_TooltipsManager).Setup(ETooltipsStyle.Menus);
  }

  private final void RequestTooltip(TarotCardData data) {
    ref<MessageTooltipData> toolTipData;
    toolTipData = new MessageTooltipData();
    toolTipData.Title = data.label;
    toolTipData.Description = data.desc;
    WeakRefToRef(this.m_TooltipsManager).ShowTooltip(toolTipData);
  }

  private final void HideTooltips() {
    WeakRefToRef(this.m_TooltipsManager).HideTooltips();
  }

  private final ref<tarotCardLogicController> GetTarotCardControllerFromTarget(ref<inkPointerEvent> evt) {
    ref<inkWidget> widget;
    wref<tarotCardLogicController> controller;
    widget = WeakRefToRef(evt.GetCurrentTarget());
    controller = RefToWeakRef(Cast(WeakRefToRef(widget.GetController())));
    return WeakRefToRef(controller);
  }
}

public class tarotCardLogicController extends inkLogicController {

  private edit inkImageRef m_image;

  private edit inkWidgetRef m_highlight;

  private TarotCardData m_data;

  protected cb Bool OnInitialize() {
    SetVisible(this.m_highlight, false);
  }

  public final void SetupData(TarotCardData data) {
    this.m_data = data;
    if(!this.m_data.empty) {
      RequestSetImage(this, this.m_image, "UIIcon." + NameToString(this.m_data.imagePath));
    } else {
      RequestSetImage(this, this.m_image, "UIIcon.TarotCard_Reverse");
    };
  }

  public final void HoverOver() {
    SetVisible(this.m_highlight, true);
  }

  public final void HoverOut() {
    SetVisible(this.m_highlight, false);
  }

  public final TarotCardData GetData() {
    return this.m_data;
  }
}
