
public struct GameTimeUtils {

  public final static Bool CanPlayerTimeSkip(ref<PlayerPuppet> playerPuppet) {
    ref<IBlackboard> psmBlackboard;
    ref<TimeSystem> timeSystem;
    GameInstance gameInstance;
    Bool blockTimeSkip;
    blockTimeSkip = false;
    gameInstance = playerPuppet.GetGame();
    psmBlackboard = playerPuppet.GetPlayerStateMachineBlackboard();
    blockTimeSkip = psmBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == ToInt(gamePSMCombat.InCombat) || HasRestriction(RefToWeakRef(playerPuppet), "NoTimeSkip") || timeSystem.IsPausedState() || playerPuppet.IsMovingVertically() || psmBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Swimming) == ToInt(gamePSMSwimming.Diving) || playerPuppet.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel) >= ToInt(gamePSMHighLevel.SceneTier3);
    return !blockTimeSkip;
  }

  public final static Bool IsTimeDisplayGlitched(ref<PlayerPuppet> playerPuppet) {
    ref<TimeSystem> timeSystem;
    Bool blockTimeSkip;
    blockTimeSkip = false;
    blockTimeSkip = HasRestriction(RefToWeakRef(playerPuppet), "NoTimeDisplay");
    return blockTimeSkip;
  }

  public final static void UpdateGameTimeText(ref<TimeSystem> timeSystem, inkTextRef textWidgetRef, ref<inkTextParams> textParamsRef) {
    GameTime gameTime;
    if(timeSystem == null) {
      return ;
    };
    gameTime = timeSystem.GetGameTime();
    if(textParamsRef == null) {
      textParamsRef = new inkTextParams();
      textParamsRef.AddNCGameTime("VALUE", gameTime);
      SetText(textWidgetRef, "{VALUE,time,short}", textParamsRef);
    } else {
      textParamsRef.UpdateTime("VALUE", gameTime);
    };
  }
}

public class TimeMenuGameController extends inkGameController {

  private edit inkWidgetRef m_selectTimeText;

  private edit inkWidgetRef m_selectorRef;

  private edit inkTextRef m_currentTime;

  private edit inkWidgetRef m_applyBtn;

  private edit inkWidgetRef m_backBtn;

  private edit inkTextRef m_combatWarning;

  private ref<TimeSkipPopupData> m_data;

  private GameInstance m_gameInstance;

  [Default(TimeMenuGameController, true))]
  private Bool m_inputEnabled;

  [Default(TimeMenuGameController, false))]
  private Bool m_timeChanged;

  private wref<SelectorController> m_selectorCtrl;

  private ref<TimeSystem> m_timeSystem;

  private Int32 m_hoursToSkip;

  private wref<inkMenuEventDispatcher> m_menuEventDispatcher;

  private ref<inkTextParams> m_currentTimeTextParams;

  private ref<inkAnimProxy> m_animProxy;

  private Uint32 m_playerSpawnedCallbackID;

  protected cb Bool OnInitialize() {
    ref<PlayerPuppet> playerPuppet;
    Bool canSkipTime;
    this.m_data = Cast(WeakRefToRef(GetRootWidget()).GetUserData("TimeSkipPopupData"));
    this.m_gameInstance = Cast(GetOwnerEntity()).GetGame();
    this.m_timeSystem = GetTimeSystem(this.m_gameInstance);
    UpdateTimeText();
    SetupSelector();
    WeakRefToRef(this.m_selectorCtrl).RegisterToCallback("OnSelectionChanged", this, "OnHoursChanged");
    RegisterToCallback(this.m_applyBtn, "OnPress", this, "OnPressApply");
    RegisterToCallback(this.m_backBtn, "OnPress", this, "OnPressBack");
    RegisterToGlobalInputCallback("OnPostOnRelease", this, "OnGlobalInput");
    PlayIntroAnim();
    playerPuppet = Cast(GetOwnerEntity());
    canSkipTime = CanPlayerTimeSkip(playerPuppet);
    if(!canSkipTime) {
      ToggleTimeSkip(canSkipTime);
    };
  }

  private final void ToggleTimeSkip(Bool enableTimeSkip) {
    if(enableTimeSkip) {
      SetVisible(this.m_selectorRef, true);
      SetVisible(this.m_currentTime, true);
      SetVisible(this.m_applyBtn, true);
      SetVisible(this.m_selectTimeText, true);
      SetVisible(this.m_combatWarning, false);
    } else {
      SetVisible(this.m_selectorRef, false);
      SetVisible(this.m_currentTime, false);
      SetVisible(this.m_applyBtn, false);
      SetVisible(this.m_selectTimeText, false);
      SetVisible(this.m_combatWarning, true);
    };
  }

  protected cb Bool OnUninitialize() {
    WeakRefToRef(this.m_selectorCtrl).UnregisterFromCallback("OnSelectionChanged", this, "OnHoursChanged");
    UnregisterFromCallback(this.m_applyBtn, "OnPress", this, "OnPressApply");
    UnregisterFromCallback(this.m_backBtn, "OnPress", this, "OnPressBack");
  }

  private final void SetupSelector() {
    this.m_selectorCtrl = RefToWeakRef(Cast(WeakRefToRef(GetController(this.m_selectorRef))));
    WeakRefToRef(this.m_selectorCtrl).AddValue("1:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("2:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("3:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("4:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("5:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("6:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("7:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("8:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("9:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("10:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("11:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("12:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("13:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("14:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("15:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("16:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("17:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("18:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("19:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("20:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("21:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("22:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("23:00");
    WeakRefToRef(this.m_selectorCtrl).AddValue("24:00");
    WeakRefToRef(this.m_selectorCtrl).SetCurrIndex(0);
    this.m_hoursToSkip = 1;
  }

  protected cb Bool OnHoursChanged(Int32 index, String value) {
    GetAudioSystem(this.m_gameInstance).Play("ui_menu_onpress");
    this.m_hoursToSkip = index + 1;
  }

  protected cb Bool OnPressApply(ref<inkPointerEvent> e) {
    if(e.IsAction("click")) {
      e.Handle();
      Apply();
    };
  }

  protected cb Bool OnPressBack(ref<inkPointerEvent> e) {
    if(e.IsAction("click")) {
      e.Handle();
      Cancel();
    };
  }

  protected cb Bool OnGlobalInput(ref<inkPointerEvent> e) {
    if(e.IsHandled()) {
      return false;
    };
    if(e.IsAction("one_click_confirm")) {
      e.Handle();
      Apply();
    } else {
      if(e.IsAction("back") || e.IsAction("cancel")) {
        e.Handle();
        Cancel();
      };
    };
  }

  private final void Apply() {
    GameTime currentTime;
    Int32 hours;
    Int32 minutes;
    Int32 seconds;
    if(!this.m_inputEnabled) {
      return ;
    };
    this.m_inputEnabled = false;
    if(this.m_hoursToSkip > 0) {
      currentTime = this.m_timeSystem.GetGameTime();
      hours = Hours(currentTime) + this.m_hoursToSkip;
      minutes = Minutes(currentTime);
      seconds = Seconds(currentTime);
      this.m_timeSystem.SetGameTimeByHMS(hours, minutes, seconds);
      FastForwardPlayerState();
      UpdateTimeText();
      this.m_timeChanged = true;
    };
    PlayLibraryAnimation("change");
    GetAudioSystem(this.m_gameInstance).Play("ui_menu_map_timeskip");
    PlayOutroAnim();
  }

  private final void Cancel() {
    if(!this.m_inputEnabled) {
      return ;
    };
    this.m_inputEnabled = false;
    GetAudioSystem(this.m_gameInstance).Play("ui_menu_onpress");
    PlayOutroAnim();
  }

  public final void FastForwardPlayerState() {
    ref<GameObject> player;
    Float maxPassiveRegenValue;
    ref<StatPoolsSystem> statPoolsSys;
    ref<StatusEffectSystem> statusEffectSys;
    array<ref<StatusEffect>> effects;
    Int32 i;
    Float remainingTime;
    player = GetPlayerControlledObject();
    if(ToBool(player)) {
      statPoolsSys = GetStatPoolsSystem(player.GetGame());
      if(ToBool(statPoolsSys)) {
        maxPassiveRegenValue = GetStatsSystem(player.GetGame()).GetStatValue(Cast(player.GetEntityID()), gamedataStatType.HealthOutOfCombatRegenEndThreshold);
        if(statPoolsSys.GetStatPoolValue(Cast(player.GetEntityID()), gamedataStatPoolType.Health) < maxPassiveRegenValue) {
          statPoolsSys.RequestSettingStatPoolValue(Cast(player.GetEntityID()), gamedataStatPoolType.Health, maxPassiveRegenValue, RefToWeakRef(player));
        };
        statPoolsSys.RequestSettingStatPoolValue(Cast(player.GetEntityID()), gamedataStatPoolType.Stamina, 100, RefToWeakRef(player));
      };
      statusEffectSys = GetStatusEffectSystem(player.GetGame());
      statusEffectSys.GetAppliedEffects(player.GetEntityID(), effects);
      i = 0;
      while(i < Size(effects)) {
        remainingTime = effects[i].GetRemainingDuration();
        if(remainingTime > 0) {
          statusEffectSys.RemoveStatusEffect(player.GetEntityID(), WeakRefToRef(effects[i].GetRecord()).GetID(), effects[i].GetStackCount());
        };
        i += 1;
      };
    };
  }

  private final void StopAnim() {
    if(ToBool(this.m_animProxy)) {
      this.m_animProxy.UnregisterFromAllCallbacks(inkanimEventType.OnFinish);
      this.m_animProxy.Stop();
      this.m_animProxy = null;
    };
  }

  private final void PlayIntroAnim() {
    StopAnim();
    this.m_animProxy = PlayLibraryAnimation("intro");
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnIntroAnimEnd");
  }

  private final void PlayLoopAnim() {
    inkAnimOptions options;
    StopAnim();
    options.loopType = inkanimLoopType.Cycle;
    options.loopInfinite = true;
    this.m_animProxy = PlayLibraryAnimation("loop", options);
  }

  private final void PlayOutroAnim() {
    StopAnim();
    this.m_animProxy = PlayLibraryAnimation("outro");
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnOutroAnimEnd");
  }

  protected cb Bool OnIntroAnimEnd(ref<inkAnimProxy> proxy) {
    PlayLoopAnim();
  }

  protected cb Bool OnOutroAnimEnd(ref<inkAnimProxy> proxy) {
    StopAnim();
    Close();
  }

  protected cb Bool OnSetMenuEventDispatcher(wref<inkMenuEventDispatcher> menuEventDispatcher) {
    this.m_menuEventDispatcher = menuEventDispatcher;
  }

  protected final void Close() {
    ref<TimeSkipPopupCloseData> data;
    data = new TimeSkipPopupCloseData();
    data.timeChanged = this.m_timeChanged;
    WeakRefToRef(this.m_data.token).TriggerCallback(data);
  }

  private final void UpdateTimeText() {
    if(IsValid(this.m_currentTime)) {
      UpdateGameTimeText(this.m_timeSystem, this.m_currentTime, this.m_currentTimeTextParams);
    };
  }
}
