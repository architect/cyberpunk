
public class DisassembleManager extends gameuiMenuGameController {

  private edit inkCompoundRef m_listRef;

  [Default(DisassembleManager, 1.0))]
  private edit Float m_initialPopupDelay;

  private array<ref<DisassemblePopupLogicController>> m_popupList;

  private array<InventoryItemData> m_listOfAddedInventoryItems;

  private wref<PlayerPuppet> m_player;

  private ref<InventoryDataManagerV2> m_InventoryManager;

  private ref<TransactionSystem> m_transactionSystem;

  private ref<inkWidget> m_root;

  private ref<inkAnimProxy> m_animProxy;

  private ref<inkAnimDef> m_alpha_fadein;

  private inkAnimOptions m_AnimOptions;

  private ref<UI_CraftingDef> m_DisassembleCallback;

  private ref<IBlackboard> m_DisassembleBlackboard;

  private Uint32 m_DisassembleBBID;

  private Uint32 m_CraftingBBID;

  protected cb Bool OnInitialize() {
    this.m_player = RefToWeakRef(Cast(GetOwnerEntity()));
    this.m_InventoryManager = new InventoryDataManagerV2();
    this.m_InventoryManager.Initialize(WeakRefToRef(this.m_player));
    this.m_transactionSystem = GetTransactionSystem(WeakRefToRef(this.m_player).GetGame());
    RemoveAllChildren(this.m_listRef);
    SetupBB();
  }

  protected cb Bool OnUninitialize() {
    this.m_InventoryManager.UnInitialize();
    UnregisterFromBB();
  }

  private final void SetupBB() {
    this.m_DisassembleCallback = GetAllBlackboardDefs().UI_Crafting;
    this.m_DisassembleBlackboard = GetBlackboardSystem().Get(this.m_DisassembleCallback);
    if(ToBool(this.m_DisassembleBlackboard)) {
      this.m_DisassembleBBID = this.m_DisassembleBlackboard.RegisterDelayedListenerVariant(this.m_DisassembleCallback.lastIngredients, this, "OnDisassembleComplete", true);
    };
  }

  private final void UnregisterFromBB() {
    if(ToBool(this.m_DisassembleBlackboard)) {
      this.m_DisassembleBlackboard.UnregisterDelayedListener(this.m_DisassembleCallback.lastIngredients, this.m_DisassembleBBID);
    };
  }

  public final void ManagePopups() {
    if(Size(this.m_listOfAddedInventoryItems) > 0) {
      CreatePopup();
      CreatePopupDelay();
    };
  }

  private final void CreatePopup() {
    wref<DisassemblePopupLogicController> popup;
    popup = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(this.m_listRef), "disassemble_popup")).GetController())));
    WeakRefToRef(popup).SetupData(Pop(this.m_listOfAddedInventoryItems));
    WeakRefToRef(popup).RegisterToCallback("OnPopupComplete", this, "OnRemovePopup");
  }

  private final void CreatePopupDelay() {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(this.m_initialPopupDelay);
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_listRef, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnDelayComplete");
  }

  protected cb Bool OnDisassembleComplete(Variant value) {
    array<IngredientData> disassembledIngredientData;
    InventoryItemData tempData;
    Int32 i;
    ItemID itemID;
    ref<Item_Record> itemRecord;
    wref<gameItemData> itemdata;
    disassembledIngredientData = FromVariant(value);
    if(Size(disassembledIngredientData) > 0) {
      i = 0;
      while(i < Size(disassembledIngredientData)) {
        itemRecord = disassembledIngredientData[i].id;
        itemID = FromTDBID(itemRecord.GetID());
        itemdata = GetTransactionSystem(WeakRefToRef(this.m_player).GetGame()).GetItemData(WeakRefToRef(this.m_player), itemID);
        tempData = this.m_InventoryManager.GetInventoryItemData(itemdata);
        Push(this.m_listOfAddedInventoryItems, tempData);
        GetActivityLogSystem(Cast(GetPlayerControlledObject()).GetGame()).AddLog(GetLocalizedText("UI-ScriptExports-Looted") + ": " + GetItemName(itemRecord, itemdata));
        i += 1;
      };
    };
  }

  protected cb Bool OnRemovePopup(wref<inkWidget> widget) {
    RemoveChild(this.m_listRef, widget);
    ManagePopups();
  }

  protected cb Bool OnDelayComplete(ref<inkAnimProxy> anim) {
    ManagePopups();
  }
}

public class DisassemblePopupLogicController extends inkLogicController {

  private edit inkTextRef m_quantity;

  private edit inkImageRef m_icon;

  private edit inkTextRef m_label;

  [Default(DisassemblePopupLogicController, 3.0f))]
  private edit Float m_duration;

  private ref<inkAnimProxy> m_animProxy;

  private ref<inkAnimDef> m_alpha_fadein;

  private inkAnimOptions m_AnimOptions;

  protected cb Bool OnInitialize()

  public final void SetupData(InventoryItemData itemData) {
    SetText(this.m_label, GetName(itemData));
    SetText(this.m_quantity, "x" + ToString(GetQuantity(itemData)));
    SetTexturePart(this.m_icon, StringToName(GetIconPath(itemData)));
    this.m_animProxy = PlayLibraryAnimation("AddPopup");
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnAddPopupComplete");
  }

  protected cb Bool OnAddPopupComplete(ref<inkAnimProxy> anim) {
    ref<inkAnimTransparency> alphaInterpolator;
    this.m_alpha_fadein = new inkAnimDef();
    alphaInterpolator = new inkAnimTransparency();
    alphaInterpolator.SetDuration(this.m_duration);
    alphaInterpolator.SetStartTransparency(1);
    alphaInterpolator.SetEndTransparency(1);
    alphaInterpolator.SetType(inkanimInterpolationType.Linear);
    alphaInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_alpha_fadein.AddInterpolator(alphaInterpolator);
    this.m_animProxy = PlayAnimation(this.m_quantity, this.m_alpha_fadein);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnPopupDurationComplete");
  }

  protected cb Bool OnPopupDurationComplete(ref<inkAnimProxy> anim) {
    CallCustomCallback("OnPopupComplete");
  }
}
