
public class hubSelectorSingleCarouselController extends SelectorController {

  [Default(hubSelectorSingleCarouselController, 7))]
  [Default(hubSelectorSingleSmallCarouselController, 5))]
  protected Int32 NUMBER_OF_WIDGETS;

  [Default(hubSelectorSingleCarouselController, 10.0f))]
  protected Float WIDGETS_PADDING;

  [Default(hubSelectorSingleCarouselController, 0.8f))]
  protected Float SMALL_WIDGET_SCALE;

  [Default(hubSelectorSingleCarouselController, 1.0f))]
  protected Float SMALL_WIDGET_OPACITY;

  [Default(hubSelectorSingleCarouselController, 0.2f))]
  protected Float ANIMATION_TIME;

  protected HDRColor DEFAULT_WIDGET_COLOR;

  protected HDRColor SELECTED_WIDGET_COLOR;

  protected edit inkWidgetRef m_leftArrowWidget;

  protected edit inkWidgetRef m_rightArrowWidget;

  protected edit inkWidgetRef m_container;

  protected edit inkWidgetRef m_defaultColorDummy;

  protected edit inkWidgetRef m_activeColorDummy;

  public ref<inkInputDisplayController> m_leftArrowController;

  public ref<inkInputDisplayController> m_rightArrowController;

  protected array<MenuData> m_elements;

  protected Int32 m_centerElementIndex;

  protected array<ref<HubMenuLabelContentContainer>> m_widgetsControllers;

  protected Bool m_waitForSizes;

  protected Bool m_translationOnce;

  protected Int32 m_currentIndex;

  protected array<ref<inkAnimProxy>> m_activeAnimations;

  protected cb Bool OnInitialize() {
    Int32 i;
    wref<inkWidget> widget;
    ref<HubMenuLabelContentContainer> controller;
    this.m_leftArrowController = Cast(WeakRefToRef(GetController(this.m_leftArrowWidget)));
    this.m_rightArrowController = Cast(WeakRefToRef(GetController(this.m_rightArrowWidget)));
    this.DEFAULT_WIDGET_COLOR = GetTintColor(this.m_defaultColorDummy);
    this.SELECTED_WIDGET_COLOR = GetTintColor(this.m_activeColorDummy);
    this.m_centerElementIndex = this.NUMBER_OF_WIDGETS / 2;
    i = 0;
    while(i < this.NUMBER_OF_WIDGETS) {
      widget = SpawnFromLocal(Get(this.m_container), "menu_element");
      WeakRefToRef(widget).SetRenderTransformPivot(0.5, 1);
      WeakRefToRef(widget).SetAnchor(inkEAnchor.Centered);
      WeakRefToRef(widget).SetAnchorPoint(0.5, 0.5);
      controller = Cast(WeakRefToRef(WeakRefToRef(widget).GetController()));
      controller.RegisterToCallback("OnRelease", this, "OnMenuLabelClick");
      controller.RegisterToCallback("OnHoverOver", this, "OnMenuLabelHover");
      controller.RegisterToCallback("OnHoverOut", this, "OnMenuLabelHoverOut");
      controller.SetCarouselPosition(i);
      if(i == this.m_centerElementIndex) {
        controller.SetInteractive(false);
      };
      Push(this.m_widgetsControllers, controller);
      i += 1;
    };
    UpdateArrowsVisibility();
  }

  protected cb Bool OnMenuLabelClick(ref<inkPointerEvent> e) {
    ref<HubMenuLabelContentContainer> controller;
    inkSelectorChangeDirection direction;
    Int32 targetPosition;
    if(e.IsAction("click")) {
      controller = Cast(WeakRefToRef(WeakRefToRef(e.GetCurrentTarget()).GetController()));
      if(controller.GetCarouselPosition() != this.m_centerElementIndex) {
        PlaySound("TabButton", "OnPress");
        targetPosition = this.m_centerElementIndex - controller.GetCarouselPosition();
        if(controller.GetCarouselPosition() < this.m_centerElementIndex) {
          direction = inkSelectorChangeDirection.Prior;
        } else {
          if(controller.GetCarouselPosition() > this.m_centerElementIndex) {
            direction = inkSelectorChangeDirection.Next;
          };
        };
        SetCurrIndexWithDirection(GetLoopedValue(this.m_currentIndex - targetPosition, GetValuesCount()), direction);
      };
    };
  }

  protected cb Bool OnMenuLabelHover(ref<inkPointerEvent> e) {
    ref<HubMenuLabelContentContainer> controller;
    inkSelectorChangeDirection direction;
    Int32 targetPosition;
    controller = Cast(WeakRefToRef(WeakRefToRef(e.GetCurrentTarget()).GetController()));
    controller.SetTintColor(this.SELECTED_WIDGET_COLOR);
  }

  protected cb Bool OnMenuLabelHoverOut(ref<inkPointerEvent> e) {
    ref<HubMenuLabelContentContainer> controller;
    inkSelectorChangeDirection direction;
    Int32 targetPosition;
    controller = Cast(WeakRefToRef(WeakRefToRef(e.GetCurrentTarget()).GetController()));
    controller.SetTintColor(this.DEFAULT_WIDGET_COLOR);
  }

  public final void SetupMenu(array<MenuData> data, Int32 startIdentifier) {
    Int32 i;
    Int32 startIndex;
    Clear();
    Clear(this.m_elements);
    startIndex = 0;
    i = 0;
    while(i < Size(data)) {
      Push(this.m_elements, data[i]);
      AddValue(data[i].label);
      if(data[i].identifier == startIdentifier) {
        startIndex = i;
      };
      i += 1;
    };
    SetCurrIndex(startIndex);
    SetFinishedValues(startIndex);
    this.m_currentIndex = startIndex;
    UpdateArrowsVisibility();
  }

  public final void ScrollTo(MenuData data) {
    Int32 i;
    Int32 startIndex;
    startIndex = 0;
    i = 0;
    while(i < Size(this.m_elements)) {
      if(this.m_elements[i].identifier == data.identifier) {
        startIndex = i;
      };
      i += 1;
    };
    SetCurrIndex(startIndex);
    SetFinishedValues(startIndex);
    this.m_currentIndex = startIndex;
    UpdateArrowsVisibility();
  }

  private final void UpdateArrowsVisibility() {
    this.m_leftArrowController.SetVisible(Size(this.m_elements) > 1);
    this.m_rightArrowController.SetVisible(Size(this.m_elements) > 1);
  }

  protected cb Bool OnUpdateValue(String value, Int32 index, inkSelectorChangeDirection changeDirection) {
    Int32 direction;
    this.m_currentIndex = index;
    Animate(index, changeDirection);
  }

  protected final Int32 GetLoopedValue(Int32 value, Int32 limit) {
    return value >= 0 ? value % limit : limit + value + 1 % limit - 1;
  }

  protected final void SetFinishedValues(Int32 selectedIndex) {
    Int32 i;
    Int32 limit;
    Int32 realIndex;
    array<Int32> values;
    MenuData emptyData;
    limit = GetValuesCount();
    if(limit == 1) {
      this.m_widgetsControllers[this.m_centerElementIndex].SetData(this.m_elements[0]);
      i = 0;
      while(i < this.NUMBER_OF_WIDGETS) {
        if(i != this.m_centerElementIndex) {
          this.m_widgetsControllers[i].SetData(emptyData);
        };
        i += 1;
      };
    } else {
      i = 0;
      while(i < this.NUMBER_OF_WIDGETS) {
        realIndex = GetLoopedValue(selectedIndex - this.NUMBER_OF_WIDGETS / 2 + i, limit);
        this.m_widgetsControllers[i].SetData(this.m_elements[realIndex]);
        i += 1;
      };
    };
    this.m_waitForSizes = true;
  }

  protected cb Bool OnArrangeChildrenComplete() {
    Int32 i;
    if(this.m_waitForSizes) {
      i = 0;
      while(i < Size(this.m_activeAnimations)) {
        this.m_activeAnimations[i].Stop();
        i += 1;
      };
      Clear(this.m_activeAnimations);
      ResetAnimatedStates();
      this.m_waitForSizes = false;
    };
  }

  protected final array<Float> GetTranslations(Int32 targetIndex) {
    array<Float> resultTranslations;
    Int32 i;
    Float prevSidesMaxWidth;
    Float sidesMaxWidth;
    Float sidesTranslation;
    i = 0;
    while(i < this.NUMBER_OF_WIDGETS) {
      Push(resultTranslations, 0);
      i += 1;
    };
    targetIndex = this.m_centerElementIndex + targetIndex != 0 ? targetIndex > 0 ? 1 : -1 : 0;
    sidesTranslation = this.m_widgetsControllers[targetIndex].GetRealDesiredWidth() / 2 + this.WIDGETS_PADDING;
    i = 1;
    while(i < this.NUMBER_OF_WIDGETS / 2 + 1) {
      sidesMaxWidth = MaxF(this.m_widgetsControllers[targetIndex - i].GetRealDesiredWidth(), this.m_widgetsControllers[targetIndex + i].GetRealDesiredWidth());
      sidesTranslation += prevSidesMaxWidth / 2 + sidesMaxWidth / 2 + this.WIDGETS_PADDING;
      resultTranslations[targetIndex - i] = -sidesTranslation;
      resultTranslations[targetIndex + i] = sidesTranslation;
      prevSidesMaxWidth = sidesMaxWidth;
      i += 1;
    };
    return resultTranslations;
  }

  protected final Float GetMaskTargetWidth(Int32 targetIndex) {
    Int32 i;
    Float nonZeroPrevSidesMaxWidth;
    Float prevSidesMaxWidth;
    Float sidesMaxWidth;
    Float sidesTranslation;
    targetIndex = this.m_centerElementIndex + targetIndex != 0 ? targetIndex > 0 ? 1 : -1 : 0;
    sidesTranslation = this.m_widgetsControllers[targetIndex].GetRealDesiredWidth() / 2 + this.WIDGETS_PADDING;
    i = 1;
    while(i < this.NUMBER_OF_WIDGETS / 2 + 1) {
      sidesMaxWidth = MaxF(this.m_widgetsControllers[targetIndex - i].GetRealDesiredWidth(), this.m_widgetsControllers[targetIndex + i].GetRealDesiredWidth());
      sidesTranslation += prevSidesMaxWidth / 2 + sidesMaxWidth / 2 + this.WIDGETS_PADDING;
      nonZeroPrevSidesMaxWidth = prevSidesMaxWidth;
      prevSidesMaxWidth = sidesMaxWidth;
      i += 1;
    };
    return sidesTranslation - nonZeroPrevSidesMaxWidth + this.WIDGETS_PADDING * 2 * 2;
  }

  protected final void ResetAnimatedStates() {
    Int32 i;
    array<Float> translations;
    ref<HubMenuLabelContentContainer> widgetCenter;
    ref<HubMenuLabelContentContainer> widgetLeft;
    ref<HubMenuLabelContentContainer> widgetRight;
    Float prevSidesMaxWidth;
    Float sidesMaxWidth;
    Float sidesTranslation;
    widgetCenter = this.m_widgetsControllers[this.m_centerElementIndex];
    sidesTranslation = widgetCenter.GetRealDesiredWidth() / 2 + this.WIDGETS_PADDING;
    i = 1;
    while(i < this.NUMBER_OF_WIDGETS / 2) {
      widgetLeft = this.m_widgetsControllers[this.m_centerElementIndex - i];
      widgetRight = this.m_widgetsControllers[this.m_centerElementIndex + i];
      sidesMaxWidth = MaxF(widgetLeft.GetRealDesiredWidth(), widgetRight.GetRealDesiredWidth());
      WeakRefToRef(widgetLeft.GetRootWidget()).SetWidth(sidesMaxWidth);
      WeakRefToRef(widgetRight.GetRootWidget()).SetWidth(sidesMaxWidth);
      i += 1;
    };
    WeakRefToRef(Get(this.m_container)).SetSize(new Vector2(GetMaskTargetWidth(0),GetHeight(this.m_container)));
    i = 0;
    while(i < this.NUMBER_OF_WIDGETS) {
      if(i == this.m_centerElementIndex) {
        WeakRefToRef(this.m_widgetsControllers[i].GetRootWidget()).SetScale(new Vector2(1,1));
        WeakRefToRef(this.m_widgetsControllers[i].GetRootWidget()).SetOpacity(1);
        this.m_widgetsControllers[i].SetTintColor(this.SELECTED_WIDGET_COLOR);
      } else {
        WeakRefToRef(this.m_widgetsControllers[i].GetRootWidget()).SetScale(new Vector2(this.SMALL_WIDGET_SCALE,this.SMALL_WIDGET_SCALE));
        WeakRefToRef(this.m_widgetsControllers[i].GetRootWidget()).SetOpacity(this.SMALL_WIDGET_OPACITY);
        this.m_widgetsControllers[i].SetTintColor(this.DEFAULT_WIDGET_COLOR);
      };
      i += 1;
    };
    translations = GetTranslations(0);
    i = 0;
    while(i < this.NUMBER_OF_WIDGETS) {
      WeakRefToRef(this.m_widgetsControllers[i].GetRootWidget()).SetTranslation(translations[i], 0);
      i += 1;
    };
  }

  protected final void Animate(Int32 targetIndex, inkSelectorChangeDirection direction) {
    Int32 i;
    Int32 offset;
    Vector2 prevVector;
    array<Float> translations;
    offset = 0;
    if(direction == inkSelectorChangeDirection.Prior) {
      offset = -1;
    } else {
      if(direction == inkSelectorChangeDirection.Next) {
        offset = 1;
      };
    };
    translations = GetTranslations(offset);
    i = 0;
    while(i < this.NUMBER_OF_WIDGETS) {
      prevVector = WeakRefToRef(this.m_widgetsControllers[i].GetRootWidget()).GetTranslation();
      AddActiveProxy(TranslationAnimation(this.m_widgetsControllers[i].GetRootWidget(), prevVector.X, translations[i]));
      i += 1;
    };
    AddActiveProxy(ScaleAnimation(this.m_widgetsControllers[this.m_centerElementIndex].GetRootWidget(), 1, this.SMALL_WIDGET_SCALE));
    AddActiveProxy(OpacityAnimation(this.m_widgetsControllers[this.m_centerElementIndex].GetRootWidget(), 1, this.SMALL_WIDGET_OPACITY));
    AddActiveProxy(ColorAnimation(this.m_widgetsControllers[this.m_centerElementIndex].GetTintedWidgets(), this.SELECTED_WIDGET_COLOR, this.DEFAULT_WIDGET_COLOR));
    AddActiveProxy(ScaleAnimation(this.m_widgetsControllers[this.m_centerElementIndex + offset].GetRootWidget(), this.SMALL_WIDGET_SCALE, 1));
    AddActiveProxy(OpacityAnimation(this.m_widgetsControllers[this.m_centerElementIndex + offset].GetRootWidget(), this.SMALL_WIDGET_OPACITY, 1));
    AddActiveProxy(ColorAnimation(this.m_widgetsControllers[this.m_centerElementIndex + offset].GetTintedWidgets(), this.DEFAULT_WIDGET_COLOR, this.SELECTED_WIDGET_COLOR));
    this.m_translationOnce = true;
  }

  protected final void AddActiveProxy(ref<inkAnimProxy> proxy) {
    Push(this.m_activeAnimations, proxy);
  }

  protected final void AddActiveProxy(array<ref<inkAnimProxy>> proxies) {
    Int32 i;
    i = 0;
    while(i < Size(proxies)) {
      Push(this.m_activeAnimations, proxies[i]);
      i += 1;
    };
  }

  protected ref<inkAnimProxy> ScaleAnimation(wref<inkWidget> targetWidget, Float startScale, Float endScale) {
    ref<inkAnimProxy> proxy;
    ref<inkAnimDef> moveElementsAnimDef;
    ref<inkAnimScale> scaleInterpolator;
    moveElementsAnimDef = new inkAnimDef();
    scaleInterpolator = new inkAnimScale();
    scaleInterpolator.SetType(inkanimInterpolationType.Linear);
    scaleInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    scaleInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    scaleInterpolator.SetStartScale(new Vector2(startScale,startScale));
    scaleInterpolator.SetEndScale(new Vector2(endScale,endScale));
    scaleInterpolator.SetDuration(this.ANIMATION_TIME);
    moveElementsAnimDef.AddInterpolator(scaleInterpolator);
    proxy = WeakRefToRef(targetWidget).PlayAnimation(moveElementsAnimDef);
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnScaleCompleted");
    return proxy;
  }

  protected ref<inkAnimProxy> TranslationAnimation(wref<inkWidget> targetWidget, Float startTranslation, Float endTranslation) {
    ref<inkAnimProxy> proxy;
    ref<inkAnimDef> moveElementsAnimDef;
    ref<inkAnimTranslation> translationInterpolator;
    moveElementsAnimDef = new inkAnimDef();
    translationInterpolator = new inkAnimTranslation();
    translationInterpolator.SetType(inkanimInterpolationType.Linear);
    translationInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    translationInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    translationInterpolator.SetStartTranslation(new Vector2(startTranslation,0));
    translationInterpolator.SetEndTranslation(new Vector2(endTranslation,0));
    translationInterpolator.SetDuration(this.ANIMATION_TIME);
    moveElementsAnimDef.AddInterpolator(translationInterpolator);
    proxy = WeakRefToRef(targetWidget).PlayAnimation(moveElementsAnimDef);
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnTranslationCompleted");
    return proxy;
  }

  protected ref<inkAnimProxy> SizeAnimation(wref<inkWidget> targetWidget, Vector2 startSize, Vector2 endSize) {
    ref<inkAnimProxy> proxy;
    ref<inkAnimDef> moveElementsAnimDef;
    ref<inkAnimSize> sizeInterpolator;
    moveElementsAnimDef = new inkAnimDef();
    sizeInterpolator = new inkAnimSize();
    sizeInterpolator.SetType(inkanimInterpolationType.Linear);
    sizeInterpolator.SetMode(inkanimInterpolationMode.EasyOut);
    sizeInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    sizeInterpolator.SetStartSize(startSize);
    sizeInterpolator.SetEndSize(endSize);
    sizeInterpolator.SetDuration(this.ANIMATION_TIME * 1.5);
    moveElementsAnimDef.AddInterpolator(sizeInterpolator);
    proxy = WeakRefToRef(targetWidget).PlayAnimation(moveElementsAnimDef);
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnSizeCompleted");
    return proxy;
  }

  protected ref<inkAnimProxy> OpacityAnimation(wref<inkWidget> targetWidget, Float startOpacity, Float endOpacity) {
    ref<inkAnimProxy> proxy;
    ref<inkAnimDef> moveElementsAnimDef;
    ref<inkAnimTransparency> transparencyInterpolator;
    moveElementsAnimDef = new inkAnimDef();
    transparencyInterpolator = new inkAnimTransparency();
    transparencyInterpolator.SetType(inkanimInterpolationType.Linear);
    transparencyInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    transparencyInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    transparencyInterpolator.SetStartTransparency(startOpacity);
    transparencyInterpolator.SetEndTransparency(endOpacity);
    transparencyInterpolator.SetDuration(this.ANIMATION_TIME);
    moveElementsAnimDef.AddInterpolator(transparencyInterpolator);
    proxy = WeakRefToRef(targetWidget).PlayAnimation(moveElementsAnimDef);
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnOpacityCompleted");
    return proxy;
  }

  protected array<ref<inkAnimProxy>> ColorAnimation(array<wref<inkWidget>> targetWidgets, HDRColor startColor, HDRColor endColor) {
    Int32 i;
    array<ref<inkAnimProxy>> proxies;
    ref<inkAnimProxy> proxy;
    ref<inkAnimDef> colorElementsAnimDef;
    ref<inkAnimColor> colorInterpolator;
    colorElementsAnimDef = new inkAnimDef();
    colorInterpolator = new inkAnimColor();
    colorInterpolator.SetType(inkanimInterpolationType.Linear);
    colorInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    colorInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    colorInterpolator.SetStartColor(startColor);
    colorInterpolator.SetEndColor(endColor);
    colorInterpolator.SetDuration(this.ANIMATION_TIME);
    colorElementsAnimDef.AddInterpolator(colorInterpolator);
    i = 0;
    while(i < Size(targetWidgets)) {
      proxy = WeakRefToRef(targetWidgets[i]).PlayAnimation(colorElementsAnimDef);
      Push(proxies, proxy);
      i += 1;
    };
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnColorCompleted");
    return proxies;
  }

  protected cb Bool OnTranslationCompleted(ref<inkAnimProxy> anim) {
    if(this.m_translationOnce) {
      SetFinishedValues(this.m_currentIndex);
      ResetAnimatedStates();
      this.m_translationOnce = false;
    };
  }
}

public class hubSelectorController extends SelectorController {

  public edit inkWidgetRef m_leftArrowWidget;

  public edit inkWidgetRef m_rightArrowWidget;

  public edit inkHorizontalPanelRef m_menuLabelHolder;

  public wref<HubMenuLabelController> m_selectedMenuLabel;

  private wref<HubMenuLabelController> m_previouslySelectedMenuLabel;

  private array<MenuData> hubElementsData;

  private Int32 m_previousIndex;

  protected cb Bool OnInitialize() {
    RegisterToCallback(this.m_leftArrowWidget, "OnRelease", this, "OnLeft");
    RegisterToCallback(this.m_rightArrowWidget, "OnRelease", this, "OnRight");
  }

  public final void AddMenuTab(MenuData data) {
    Int32 childrenNum;
    Int32 selectedIndex;
    ref<HubMenuLabelController> menuLabelController;
    wref<inkWidget> widget;
    childrenNum = GetNumChildren(this.m_menuLabelHolder);
    if(GetNumChildren(this.m_menuLabelHolder) < 3) {
      widget = SpawnFromLocal(Get(this.m_menuLabelHolder), "menu_label");
      if(!ToBool(widget)) {
        widget = SpawnFromExternal(Get(this.m_menuLabelHolder), "base\gameplay\gui\fullscreen\hub_menu\new_hub.inkwidget", "menu_label");
      };
      menuLabelController = Cast(WeakRefToRef(WeakRefToRef(widget).GetController()));
      menuLabelController.SetData(data);
      menuLabelController.RegisterToCallback("OnRelease", this, "OnMenuLabelClick");
    };
    WeakRefToRef(this.m_selectedMenuLabel).SetActive(false);
    selectedIndex = CeilF(Cast(childrenNum - 1) / 2);
    this.m_selectedMenuLabel = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(this.m_menuLabelHolder, selectedIndex)).GetController())));
    WeakRefToRef(this.m_selectedMenuLabel).SetActive(true);
    Push(this.hubElementsData, data);
  }

  public final void RemoveOldTabs() {
    Clear(this.hubElementsData);
    RemoveAllChildren(this.m_menuLabelHolder);
  }

  public final void RegisterToMenuTabCallback(CName eventName, ref<IScriptable> object, CName functionName) {
    wref<inkWidget> widget;
    Int32 i;
    i = 0;
    while(i < GetNumChildren(this.m_menuLabelHolder)) {
      widget = GetWidgetByIndex(this.m_menuLabelHolder, i);
      WeakRefToRef(widget).RegisterToCallback(eventName, object, functionName);
      i += 1;
    };
  }

  protected final Int32 CycleInRange(Int32 index, Int32 range) {
    if(index > 0) {
      return index % range;
    };
    if(index < 0) {
      return range + index % range;
    };
    return 0;
  }

  protected final array<MenuData> GetNearestWidgetsData(Int32 index) {
    array<MenuData> result;
    Int32 i;
    Int32 targetIndex;
    i = -1;
    while(i < 1) {
      Push(result, this.hubElementsData[CycleInRange(index + i, GetValuesCount())]);
      i += 1;
    };
    return result;
  }

  protected cb Bool OnUpdateValue(String value, Int32 index, inkSelectorChangeDirection changeDirection) {
    ref<HubMenuLabelController> menuLabelController;
    Int32 i;
    Int32 idx;
    Int32 valuesSize;
    Int32 relativeChange;
    idx = GetCurrIndex();
    valuesSize = GetValuesCount();
    relativeChange = idx - this.m_previousIndex;
    this.m_previousIndex = idx;
    if(relativeChange == valuesSize - 1 || relativeChange * -1 == valuesSize - 1) {
      relativeChange *= -1;
    };
    if(valuesSize < 1) {
      Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(this.m_menuLabelHolder, i + 1)).GetController())).SetData(this.hubElementsData[idx]);
    } else {
      i = valuesSize > 2 ? -1 : 0;
      while(i < 1) {
        menuLabelController = Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(this.m_menuLabelHolder, i + valuesSize > 2 ? 1 : 0)).GetController()));
        menuLabelController.SetTargetData(this.hubElementsData[CycleInRange(idx + i, valuesSize)], relativeChange);
        if(i == 0) {
          menuLabelController.SetActive(true);
        };
        i += 1;
      };
    };
  }

  protected cb Bool OnLeft(ref<inkPointerEvent> e) {
    if(e.IsAction("click")) {
      Prior();
    };
  }

  protected cb Bool OnRight(ref<inkPointerEvent> e) {
    if(e.IsAction("click")) {
      Next();
    };
  }

  protected cb Bool OnMenuLabelClick(ref<inkPointerEvent> e) {
    wref<inkWidget> target;
    ref<HubMenuLabelController> logicController;
    Int32 clickedIndex;
    Int32 visibleElements;
    if(e.IsAction("click")) {
      target = e.GetTarget();
      logicController = Cast(WeakRefToRef(WeakRefToRef(target).GetController()));
      clickedIndex = DetermineIndex(logicController);
      visibleElements = GetNumChildren(this.m_menuLabelHolder);
      if(visibleElements > 1) {
        if(clickedIndex == visibleElements - 1) {
          Next();
        } else {
          if(clickedIndex == 0) {
            Prior();
          };
        };
      };
    };
  }

  private final ref<HubMenuLabelController> FindLabel(String label) {
    wref<HubMenuLabelController> selectedLabel;
    Int32 i;
    i = 0;
    while(i < GetNumChildren(this.m_menuLabelHolder)) {
      selectedLabel = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(this.m_menuLabelHolder, i)).GetController())));
      if(WeakRefToRef(selectedLabel).m_data.label == label) {
        return WeakRefToRef(selectedLabel);
      };
      i += 1;
    };
    return null;
  }

  private final Int32 DetermineIndex(ref<HubMenuLabelController> controller) {
    wref<HubMenuLabelController> selectedLabel;
    Int32 i;
    i = 0;
    while(i < GetNumChildren(this.m_menuLabelHolder)) {
      selectedLabel = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(GetWidgetByIndex(this.m_menuLabelHolder, i)).GetController())));
      if(WeakRefToRef(selectedLabel) == controller) {
        return i;
      };
      i += 1;
    };
    return i;
  }
}

public class HubMenuLabelContentContainer extends inkLogicController {

  protected edit inkTextRef m_label;

  protected edit inkImageRef m_icon;

  protected edit inkWidgetRef m_desiredSizeWrapper;

  protected edit inkBorderRef m_border;

  protected Int32 m_carouselPosition;

  public String m_labelName;

  public MenuData m_data;

  protected cb Bool OnInitialize() {
    SetVisible(this.m_border, false);
    SetVisible(this.m_icon, false);
  }

  public void SetData(MenuData data) {
    this.m_data = data;
    this.m_labelName = this.m_data.label;
    SetText(this.m_label, this.m_labelName);
    SetLocalizedTextScript(this.m_label, StringToName(this.m_labelName));
    if(this.m_data.icon != "") {
      SetVisible(this.m_icon, true);
      SetTexturePart(this.m_icon, this.m_data.icon);
    } else {
      SetVisible(this.m_icon, false);
    };
  }

  public void SetCarouselPosition(Int32 carouselPosition) {
    this.m_carouselPosition = carouselPosition;
  }

  public void SetInteractive(Bool value) {
    SetInteractive(this.m_desiredSizeWrapper, value);
  }

  public Int32 GetCarouselPosition() {
    return this.m_carouselPosition;
  }

  public Int32 GetIdentifier() {
    return this.m_data.identifier;
  }

  public Float GetSize() {
    return WeakRefToRef(GetRootWidget()).GetWidth();
  }

  public Vector2 GetRealDesiredSize() {
    return GetDesiredSize(this.m_desiredSizeWrapper);
  }

  public Float GetRealDesiredWidth() {
    return GetDesiredWidth(this.m_desiredSizeWrapper);
  }

  public array<wref<inkWidget>> GetTintedWidgets() {
    array<wref<inkWidget>> result;
    Push(result, Get(this.m_label));
    Push(result, Get(this.m_icon));
    return result;
  }

  public void SetTintColor(HDRColor color) {
    SetTintColor(this.m_label, color);
    SetTintColor(this.m_icon, color);
  }
}

public class HubMenuLabelController extends inkLogicController {

  protected edit inkCompoundRef m_container;

  protected wref<inkWidget> m_wrapper;

  protected wref<inkWidget> m_wrapperNext;

  protected wref<HubMenuLabelContentContainer> m_wrapperController;

  protected wref<HubMenuLabelContentContainer> m_wrapperNextController;

  public MenuData m_data;

  protected Bool m_watchForSize;

  protected Bool m_watchForInstatnSize;

  protected Bool m_once;

  protected Int32 m_direction;

  protected cb Bool OnInitialize() {
    this.m_wrapper = SpawnFromLocal(Get(this.m_container), "menu_label_content");
    if(!ToBool(this.m_wrapper)) {
      this.m_wrapper = SpawnFromExternal(Get(this.m_container), "base\gameplay\gui\fullscreen\hub_menu\new_hub.inkwidget", "menu_label_content");
    };
    WeakRefToRef(this.m_wrapper).SetAnchor(inkEAnchor.Fill);
    WeakRefToRef(this.m_wrapper).SetTranslation(0, 0);
    this.m_wrapperNext = SpawnFromLocal(Get(this.m_container), "menu_label_content");
    if(!ToBool(this.m_wrapperNext)) {
      this.m_wrapperNext = SpawnFromExternal(Get(this.m_container), "base\gameplay\gui\fullscreen\hub_menu\new_hub.inkwidget", "menu_label_content");
    };
    WeakRefToRef(this.m_wrapperNext).SetAnchor(inkEAnchor.Fill);
    WeakRefToRef(this.m_wrapperNext).SetTranslation(400, 0);
    this.m_wrapperController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(this.m_wrapper).GetController())));
    this.m_wrapperNextController = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(this.m_wrapperNext).GetController())));
  }

  public void SetData(MenuData data) {
    this.m_data = data;
    this.m_watchForInstatnSize = true;
    WeakRefToRef(this.m_wrapperController).SetData(data);
  }

  public void SetTargetData(MenuData data, Int32 direction) {
    this.m_data = data;
    this.m_direction = direction;
    if(direction != 0) {
      this.m_watchForSize = true;
      WeakRefToRef(this.m_wrapperNextController).SetData(data);
    } else {
      this.m_watchForInstatnSize = true;
      WeakRefToRef(this.m_wrapperController).SetData(data);
    };
  }

  public void SetActive(Bool active) {
    SetState(this.m_container, active ? "Selected" : "Unselected");
  }

  protected cb Bool OnArrangeChildrenComplete() {
    Float desiredWidth;
    if(this.m_watchForSize) {
      desiredWidth = WeakRefToRef(this.m_wrapperNext).GetDesiredWidth();
      ResizeAnimation(Get(this.m_container), desiredWidth);
      SwipeAnimation(this.m_wrapper, 0, this.m_direction > 0 ? -desiredWidth : desiredWidth);
      SwipeAnimation(this.m_wrapperNext, this.m_direction > 0 ? desiredWidth : -desiredWidth, 0);
      this.m_once = true;
      this.m_watchForSize = false;
    };
    if(this.m_watchForInstatnSize) {
      SetSize(this.m_container, WeakRefToRef(this.m_wrapper).GetDesiredSize());
      this.m_watchForInstatnSize = false;
    };
  }

  protected cb Bool OnSwipeCompleted(ref<inkAnimProxy> anim) {
    if(this.m_once) {
      WeakRefToRef(this.m_wrapperController).SetData(this.m_data);
      this.m_once = false;
    };
  }

  protected ref<inkAnimDef> SwipeAnimation(wref<inkWidget> targetWidget, Float startTranslation, Float endTranslation) {
    ref<inkAnimProxy> proxy;
    ref<inkAnimDef> moveElementsAnimDef;
    ref<inkAnimTranslation> translationInterpolator;
    moveElementsAnimDef = new inkAnimDef();
    translationInterpolator = new inkAnimTranslation();
    translationInterpolator.SetType(inkanimInterpolationType.Linear);
    translationInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    translationInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    translationInterpolator.SetStartTranslation(new Vector2(startTranslation,0));
    translationInterpolator.SetEndTranslation(new Vector2(endTranslation,0));
    translationInterpolator.SetDuration(0.30000001192092896);
    moveElementsAnimDef.AddInterpolator(translationInterpolator);
    proxy = WeakRefToRef(targetWidget).PlayAnimation(moveElementsAnimDef);
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnSwipeCompleted");
    return moveElementsAnimDef;
  }

  protected ref<inkAnimDef> ResizeAnimation(wref<inkWidget> targetWidget, Float width) {
    ref<inkAnimDef> resizeElementsAnimDef;
    ref<inkAnimSize> sizeInterpolator;
    resizeElementsAnimDef = new inkAnimDef();
    sizeInterpolator = new inkAnimSize();
    sizeInterpolator.SetType(inkanimInterpolationType.Linear);
    sizeInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    sizeInterpolator.SetDirection(inkanimInterpolationDirection.FromTo);
    sizeInterpolator.SetStartSize(GetSize(this.m_container));
    sizeInterpolator.SetEndSize(new Vector2(width,WeakRefToRef(this.m_wrapper).GetDesiredHeight()));
    sizeInterpolator.SetDuration(0.07500000298023224);
    resizeElementsAnimDef.AddInterpolator(sizeInterpolator);
    PlayAnimation(this.m_container, resizeElementsAnimDef);
    return resizeElementsAnimDef;
  }
}
