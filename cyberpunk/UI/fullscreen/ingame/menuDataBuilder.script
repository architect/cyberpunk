
public class MenuDataBuilder extends IScriptable {

  public array<MenuData> m_data;

  public final static ref<MenuDataBuilder> Make() {
    ref<MenuDataBuilder> instance;
    instance = new MenuDataBuilder();
    return instance;
  }

  public final ref<MenuDataBuilder> AddIf(Bool condition, Int32 identifier, CName fullscreenName, CName icon, CName labelKey, ref<IScriptable> userData?) {
    if(condition) {
      return Add(identifier, fullscreenName, icon, labelKey, userData);
    };
    return this;
  }

  public final ref<MenuDataBuilder> Add(Int32 identifier, CName fullscreenName, CName icon, CName labelKey, ref<IScriptable> userData?) {
    MenuData emptyData;
    MenuData data;
    data.label = GetLocalizedTextByKey(labelKey);
    data.icon = icon;
    data.fullscreenName = fullscreenName;
    data.identifier = identifier;
    data.userData = userData;
    Push(this.m_data, data);
    return this;
  }

  public final ref<MenuDataBuilder> Add(HubMenuItems identifier, HubMenuItems parentIdentifier, CName fullscreenName, CName icon, CName labelKey, ref<IScriptable> userData?, Bool disabled?) {
    MenuData emptyData;
    MenuData data;
    data.label = GetLocalizedTextByKey(labelKey);
    data.icon = icon;
    data.fullscreenName = fullscreenName;
    data.identifier = ToInt(identifier);
    data.parentIdentifier = ToInt(parentIdentifier);
    data.userData = userData;
    data.disabled = disabled;
    Push(this.m_data, data);
    return this;
  }

  public final ref<SubmenuDataBuilder> AddWithSubmenu(Int32 identifier, CName fullscreenName, CName icon, CName labelKey, ref<IScriptable> userData?, Bool disabled?) {
    ref<SubmenuDataBuilder> submenuBuilder;
    MenuData emptyData;
    MenuData data;
    data.label = GetLocalizedTextByKey(labelKey);
    data.icon = icon;
    data.fullscreenName = fullscreenName;
    data.identifier = identifier;
    data.userData = userData;
    data.disabled = disabled;
    Push(this.m_data, data);
    return Make(this, Size(this.m_data) - 1);
  }

  public final array<MenuData> Get() {
    return this.m_data;
  }

  public final array<MenuData> GetMainMenus() {
    Int32 i;
    Int32 count;
    array<MenuData> res;
    MenuData currentData;
    count = Size(this.m_data);
    i = 0;
    while(i < count) {
      currentData = this.m_data[i];
      if(currentData.parentIdentifier == ToInt(HubMenuItems.)) {
        Push(res, currentData);
      };
      i = i + 1;
    };
    return res;
  }

  public final MenuData GetData(Int32 identifier) {
    Int32 i;
    Int32 count;
    MenuData res;
    count = Size(this.m_data);
    i = 0;
    while(i < count) {
      res = this.m_data[i];
      if(res.identifier == identifier) {
        return res;
      };
      i = i + 1;
    };
    res.identifier = ToInt(HubMenuItems.);
    return res;
  }

  public final MenuData GetData(CName fullscreenName) {
    Int32 i;
    Int32 count;
    MenuData res;
    count = Size(this.m_data);
    i = 0;
    while(i < count) {
      res = this.m_data[i];
      if(res.fullscreenName == fullscreenName) {
        return res;
      };
      i = i + 1;
    };
    res.identifier = ToInt(HubMenuItems.);
    return res;
  }
}

public class SubmenuDataBuilder extends IScriptable {

  private ref<MenuDataBuilder> m_menuBuilder;

  private Int32 m_menuDataIndex;

  public final static ref<SubmenuDataBuilder> Make(ref<MenuDataBuilder> menuBuilder, Int32 menuDataIndex) {
    ref<SubmenuDataBuilder> instance;
    instance = new SubmenuDataBuilder();
    instance.m_menuDataIndex = menuDataIndex;
    instance.m_menuBuilder = menuBuilder;
    return instance;
  }

  public final ref<SubmenuDataBuilder> AddSubmenu(Int32 identifier, CName fullscreenName, CName labelKey, ref<IScriptable> userData?) {
    MenuData emptyData;
    MenuData data;
    data.label = GetLocalizedTextByKey(labelKey);
    data.fullscreenName = fullscreenName;
    data.identifier = identifier;
    data.userData = userData;
    Push(this.m_menuBuilder.m_data[this.m_menuDataIndex].subMenus, data);
    return this;
  }

  public final ref<SubmenuDataBuilder> AddSubmenuIf(Bool condition, Int32 identifier, CName fullscreenName, CName labelKey, ref<IScriptable> userData?) {
    if(condition) {
      return AddSubmenu(identifier, fullscreenName, labelKey, userData);
    };
    return this;
  }

  public final ref<MenuDataBuilder> GetMenuBuilder() {
    return this.m_menuBuilder;
  }
}
