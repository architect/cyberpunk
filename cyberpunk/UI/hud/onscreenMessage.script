
public class OnscreenMessageGameController extends inkHUDGameController {

  private wref<inkWidget> m_root;

  private ref<IBlackboard> m_blackboard;

  private ref<UI_NotificationsDef> m_blackboardDef;

  private Uint32 m_screenMessageUpdateCallbackId;

  private SimpleScreenMessage m_screenMessage;

  private edit inkTextRef m_mainTextWidget;

  private ref<inkAnimDef> m_blinkingAnim;

  private ref<inkAnimDef> m_showAnim;

  private ref<inkAnimDef> m_hideAnim;

  private ref<inkAnimProxy> m_animProxyShow;

  private ref<inkAnimProxy> m_animProxyHide;

  private ref<inkAnimProxy> m_animProxyTimeout;

  protected cb Bool OnInitialize() {
    Variant variant;
    this.m_root = GetRootWidget();
    WeakRefToRef(this.m_root).SetVisible(false);
    this.m_blackboardDef = GetAllBlackboardDefs().UI_Notifications;
    this.m_blackboard = GetBlackboardSystem().Get(this.m_blackboardDef);
    this.m_screenMessageUpdateCallbackId = this.m_blackboard.RegisterDelayedListenerVariant(this.m_blackboardDef.OnscreenMessage, this, "OnScreenMessageUpdate");
    variant = this.m_blackboard.GetVariant(this.m_blackboardDef.OnscreenMessage);
    if(IsValid(variant)) {
      this.m_screenMessage = FromVariant(variant);
    };
    CreateAnimations();
  }

  protected cb Bool OnUnitialize() {
    this.m_blackboard.UnregisterDelayedListener(this.m_blackboardDef.OnscreenMessage, this.m_screenMessageUpdateCallbackId);
    this.m_screenMessageUpdateCallbackId = 0;
  }

  protected cb Bool OnScreenMessageUpdate(Variant value) {
    this.m_screenMessage = FromVariant(value);
    UpdateWidgets();
  }

  private final void UpdateWidgets() {
    WeakRefToRef(this.m_root).StopAllAnimations();
    if(this.m_screenMessage.isShown) {
      SetLetterCase(this.m_mainTextWidget, textLetterCase.UpperCase);
      SetText(this.m_mainTextWidget, GetLocalizedText(this.m_screenMessage.message));
      WeakRefToRef(this.m_root).SetVisible(true);
      this.m_animProxyShow = PlayLibraryAnimation("CInematic_Subtitle");
    } else {
      WeakRefToRef(this.m_root).SetVisible(false);
    };
  }

  private final void SetTimeout(Float value) {
    ref<inkAnimTransparency> interpol;
    ref<inkAnimDef> timeoutAnim;
    if(value > 0) {
      timeoutAnim = new inkAnimDef();
      interpol = new inkAnimTransparency();
      interpol.SetDuration(value);
      interpol.SetStartTransparency(1);
      interpol.SetEndTransparency(1);
      interpol.SetIsAdditive(true);
      timeoutAnim.AddInterpolator(interpol);
      this.m_animProxyTimeout = WeakRefToRef(this.m_root).PlayAnimation(timeoutAnim);
      this.m_animProxyTimeout.RegisterToCallback(inkanimEventType.OnFinish, this, "OnTimeout");
    };
  }

  protected cb Bool OnTimeout(ref<inkAnimProxy> anim) {
    if(anim.IsFinished()) {
      this.m_blackboard.SetVariant(this.m_blackboardDef.OnscreenMessage, ToVariant(NoScreenMessage()));
    };
  }

  protected cb Bool OnShown(ref<inkAnimProxy> anim) {
    if(anim.IsFinished()) {
      TriggerBlinkAnimation();
    };
  }

  protected cb Bool OnBlinkAnimation(ref<inkAnimProxy> anim) {
    if(anim.IsFinished()) {
      TriggerBlinkAnimation();
    };
  }

  protected cb Bool OnHidden(ref<inkAnimProxy> anim) {
    WeakRefToRef(this.m_root).SetVisible(false);
  }

  private final void TriggerBlinkAnimation() {
    ref<inkAnimProxy> proxy;
    proxy = WeakRefToRef(this.m_root).PlayAnimation(this.m_blinkingAnim);
    proxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnBlinkAnimation");
  }

  private final void CreateAnimations() {
    ref<inkAnimTransparency> alphaBlinkInInterpol;
    ref<inkAnimTransparency> alphaBlinkOutInterpol;
    ref<inkAnimTransparency> alphaShowInterpol;
    ref<inkAnimTransparency> alphaHideInterpol;
    this.m_blinkingAnim = new inkAnimDef();
    alphaBlinkOutInterpol = new inkAnimTransparency();
    alphaBlinkOutInterpol.SetStartTransparency(1);
    alphaBlinkOutInterpol.SetEndTransparency(0.4000000059604645);
    alphaBlinkOutInterpol.SetDuration(0.5);
    alphaBlinkOutInterpol.SetType(inkanimInterpolationType.Linear);
    alphaBlinkOutInterpol.SetMode(inkanimInterpolationMode.EasyOut);
    alphaBlinkInInterpol = new inkAnimTransparency();
    alphaBlinkInInterpol.SetStartTransparency(0.4000000059604645);
    alphaBlinkInInterpol.SetEndTransparency(1);
    alphaBlinkInInterpol.SetDuration(0.5);
    alphaBlinkInInterpol.SetStartDelay(0.5);
    alphaBlinkInInterpol.SetType(inkanimInterpolationType.Linear);
    alphaBlinkInInterpol.SetMode(inkanimInterpolationMode.EasyOut);
    this.m_blinkingAnim.AddInterpolator(alphaBlinkOutInterpol);
    this.m_blinkingAnim.AddInterpolator(alphaBlinkInInterpol);
    this.m_showAnim = new inkAnimDef();
    alphaShowInterpol = new inkAnimTransparency();
    alphaShowInterpol.SetStartTransparency(0);
    alphaShowInterpol.SetEndTransparency(1);
    alphaShowInterpol.SetDuration(0.5);
    alphaShowInterpol.SetType(inkanimInterpolationType.Exponential);
    alphaShowInterpol.SetMode(inkanimInterpolationMode.EasyOut);
    this.m_showAnim.AddInterpolator(alphaShowInterpol);
    this.m_hideAnim = new inkAnimDef();
    alphaHideInterpol = new inkAnimTransparency();
    alphaHideInterpol.SetStartTransparency(1);
    alphaHideInterpol.SetEndTransparency(0);
    alphaHideInterpol.SetDuration(1);
    alphaBlinkInInterpol.SetStartDelay(0.10000000149011612);
    alphaHideInterpol.SetType(inkanimInterpolationType.Exponential);
    alphaHideInterpol.SetMode(inkanimInterpolationMode.EasyOut);
    this.m_hideAnim.AddInterpolator(alphaHideInterpol);
  }
}
