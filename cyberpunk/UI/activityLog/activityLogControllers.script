
public class activityLogGameController extends inkHUDGameController {

  private Int32 m_readIndex;

  private Int32 m_writeIndex;

  private Int32 m_maxSize;

  private array<String> m_entries;

  private edit inkVerticalPanelRef m_panel;

  private Uint32 m_onNewEntries;

  private Uint32 m_onHide;

  protected cb Bool OnInitialize() {
    Int32 i;
    Int32 count;
    wref<activityLogEntryLogicController> entry;
    ref<IBlackboard> uiBlackboard;
    ref<IBlackboard> bb;
    this.m_readIndex = 0;
    this.m_writeIndex = 0;
    this.m_maxSize = 20;
    Resize(this.m_entries, this.m_maxSize);
    if(ToBool(Get(this.m_panel))) {
      count = GetNumChildren(this.m_panel);
      i = 0;
      while(i < count) {
        entry = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(GetWidget(this.m_panel, i)).GetController())));
        if(ToBool(entry)) {
          WeakRefToRef(entry).Reset();
          WeakRefToRef(entry).RegisterToCallback("OnTypingFinished", this, "OnTypingFinished");
          WeakRefToRef(entry).RegisterToCallback("OnDisappeared", this, "OnDisappeared");
        };
        i += 1;
      };
    };
    uiBlackboard = GetUIBlackboard();
    if(ToBool(uiBlackboard)) {
      this.m_onNewEntries = uiBlackboard.RegisterListenerVariant(GetAllBlackboardDefs().UIGameData.ActivityLog, this, "OnNewEntries");
    };
    bb = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_ActivityLog);
    if(ToBool(bb)) {
      this.m_onHide = bb.RegisterListenerBool(GetAllBlackboardDefs().UI_ActivityLog.activityLogHide, this, "OnHide");
    };
  }

  protected cb Bool OnUninitialize() {
    ref<IBlackboard> uiBlackboard;
    ref<IBlackboard> bb;
    uiBlackboard = GetUIBlackboard();
    if(ToBool(uiBlackboard)) {
      uiBlackboard.UnregisterListenerVariant(GetAllBlackboardDefs().UIGameData.ActivityLog, this.m_onNewEntries);
    };
    bb = GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_ActivityLog);
    if(ToBool(bb)) {
      bb.UnregisterListenerBool(GetAllBlackboardDefs().UI_ActivityLog.activityLogHide, this.m_onHide);
    };
  }

  public final void AddNewEntry(String value) {
    Bool bReset;
    Int32 i;
    Int32 count;
    wref<inkWidget> widget;
    wref<activityLogEntryLogicController> entry;
    count = GetNumChildren(this.m_panel);
    bReset = true;
    i = 0;
    while(i < count) {
      widget = GetWidget(this.m_panel, i);
      entry = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(widget).GetController())));
      if(WeakRefToRef(entry).IsAvailable()) {
        WeakRefToRef(entry).SetText(value);
        bReset = false;
      } else {
        i += 1;
      };
    };
    if(bReset) {
      widget = GetWidget(this.m_panel, 0);
      entry = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(widget).GetController())));
      WeakRefToRef(entry).Reset();
      WeakRefToRef(entry).SetText(value);
      ReorderChild(this.m_panel, widget, -1);
    };
  }

  protected cb Bool OnNewEntries(Variant value) {
    Bool bAdd;
    Int32 i;
    Int32 count;
    array<String> newEntries;
    newEntries = FromVariant(value);
    bAdd = this.m_readIndex == this.m_writeIndex;
    count = Size(newEntries);
    i = 0;
    while(i < count) {
      if(this.m_writeIndex - this.m_readIndex < this.m_maxSize) {
        this.m_entries[this.m_writeIndex % this.m_maxSize] = newEntries[i];
        this.m_writeIndex += 1;
      } else {
        LogUIWarning("Too many pending entries in activity log!!! Ask UI team to increase stack size.");
        goto 435;
      };
      i += 1;
    };
    if(bAdd && this.m_readIndex < this.m_writeIndex) {
      AddNewEntry(this.m_entries[this.m_readIndex]);
    };
  }

  protected cb Bool OnTypingFinished(wref<inkWidget> widget) {
    this.m_entries[this.m_readIndex % this.m_maxSize] = "";
    this.m_readIndex += 1;
    if(this.m_readIndex < this.m_writeIndex) {
      AddNewEntry(this.m_entries[this.m_readIndex % this.m_maxSize]);
    } else {
      this.m_readIndex = 0;
      this.m_writeIndex = 0;
    };
  }

  protected cb Bool OnDisappeared(wref<inkWidget> widget) {
    wref<activityLogEntryLogicController> entry;
    entry = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(widget).GetController())));
    WeakRefToRef(entry).Reset();
    ReorderChild(this.m_panel, widget, -1);
  }

  protected cb Bool OnHide(Bool val) {
    SetVisible(this.m_panel, !val);
  }
}

public class activityLogEntryLogicController extends inkLogicController {

  private Bool m_available;

  private Uint16 m_originalSize;

  private Uint16 m_size;

  private String m_displayText;

  private wref<inkText> m_root;

  private ref<inkAnimController> m_appearingAnim;

  private ref<inkAnimController> m_typingAnim;

  private ref<inkAnimController> m_disappearingAnim;

  private ref<inkAnimDef> m_typingAnimDef;

  private ref<inkAnimProxy> m_typingAnimProxy;

  private ref<inkAnimDef> m_disappearingAnimDef;

  private ref<inkAnimProxy> m_disappearingAnimProxy;

  protected cb Bool OnInitialize() {
    ref<inkAnimSize> sizeInterpolator;
    ref<inkAnimTransparency> alphaInterpolator;
    Vector2 size;
    this.m_available = true;
    this.m_root = RefToWeakRef(Cast(WeakRefToRef(GetRootWidget())));
    WeakRefToRef(this.m_root).SetLetterCase(textLetterCase.UpperCase);
    size = WeakRefToRef(this.m_root).GetSize();
    this.m_appearingAnim = new inkAnimController();
    this.m_appearingAnim.Select(WeakRefToRef(this.m_root)).Interpolate("size", ToVariant(new Vector2(0,0)), ToVariant(size)).Duration(0.20000000298023224).Type(inkanimInterpolationType.Linear).Mode(inkanimInterpolationMode.EasyIn);
    this.m_typingAnim = new inkAnimController();
    this.m_typingAnim.Select(WeakRefToRef(this.m_root)).Interpolate("transparency", ToVariant(1), ToVariant(1)).Duration(0.0005000000237487257).Type(inkanimInterpolationType.Linear).Mode(inkanimInterpolationMode.EasyIn);
    this.m_disappearingAnim = new inkAnimController();
    this.m_disappearingAnim.Select(WeakRefToRef(this.m_root)).Interpolate("transparency", ToVariant(1), ToVariant(0)).Delay(1).Duration(0.5).Type(inkanimInterpolationType.Linear).Mode(inkanimInterpolationMode.EasyOut);
  }

  public final void SetText(String displayText) {
    inkAnimOptions typingAnimPlaybackOptions;
    this.m_available = false;
    this.m_displayText = displayText;
    this.m_originalSize = Cast(StrLen(this.m_displayText));
    this.m_size = this.m_originalSize;
    WeakRefToRef(this.m_root).SetText("");
    this.m_appearingAnim.Stop();
    this.m_appearingAnim.Play();
    typingAnimPlaybackOptions.loopType = inkanimLoopType.Cycle;
    typingAnimPlaybackOptions.loopCounter = Cast(this.m_originalSize + 1);
    this.m_typingAnim.Stop();
    this.m_typingAnim.PlayWithOptions(typingAnimPlaybackOptions);
    this.m_typingAnim.RegisterToCallback(inkanimEventType.OnStartLoop, this, "OnTyping");
    this.m_typingAnim.RegisterToCallback(inkanimEventType.OnFinish, this, "OnStopTyping");
    this.m_disappearingAnim.Stop();
  }

  public final void Reset() {
    this.m_appearingAnim.Stop();
    this.m_typingAnim.Stop();
    this.m_disappearingAnim.Stop();
    WeakRefToRef(this.m_root).SetText("");
    WeakRefToRef(this.m_root).SetSize(new Vector2(0,0));
    this.m_available = true;
  }

  public final Bool IsAvailable() {
    return this.m_available;
  }

  protected cb Bool OnTyping(ref<inkAnimProxy> proxy) {
    String nextSymbol;
    nextSymbol = StrMid(this.m_displayText, Cast(this.m_originalSize - this.m_size), 1);
    WeakRefToRef(this.m_root).SetLetterCase(textLetterCase.UpperCase);
    WeakRefToRef(this.m_root).SetText(WeakRefToRef(this.m_root).GetText() + nextSymbol);
    this.m_size -= 1;
  }

  protected cb Bool OnStopTyping(ref<inkAnimProxy> proxy) {
    CallCustomCallback("OnTypingFinished");
    this.m_disappearingAnim.Play();
    this.m_disappearingAnim.RegisterToCallback(inkanimEventType.OnFinish, this, "OnDisappeared");
  }

  protected cb Bool OnDisappeared(ref<inkAnimProxy> proxy) {
    CallCustomCallback("OnDisappeared");
  }
}
