
public struct MappinUIUtils {

  public final static native EngineTime GetEngineTime()

  public final static native ref<MappinUIGlobalProfile_Record> GetGlobalProfile()

  public final static native wref<District_Record> GetDistrictRecord(gamedataDistrict districtType)

  private final static native wref<MappinUIFilterGroup_Record> GetFilterGroupFromFilter(gamedataWorldMapFilter filter)

  private final static native wref<MappinUIFilterGroup_Record> GetFilterGroupFromVariant(gamedataMappinVariant mappinVariant)

  public final static wref<MappinUIFilterGroup_Record> GetFilterGroup(gamedataWorldMapFilter filter) {
    return GetFilterGroupFromFilter(filter);
  }

  public final static wref<MappinUIFilterGroup_Record> GetFilterGroup(gamedataMappinVariant mappinVariant) {
    return GetFilterGroupFromVariant(mappinVariant);
  }

  public final static native Bool IsPlayerInArea(wref<IMappin> mappin)

  public final static Bool IsMappinServicePoint(gamedataMappinVariant mappinVariant) {
    switch(mappinVariant) {
      case gamedataMappinVariant.ServicePointClothesVariant:
        return true;
    };
    return false;
  }

  public final static CName MappinToTexturePart(wref<IMappin> mappin) {
    return MappinToTexturePart(WeakRefToRef(mappin).GetVariant(), WeakRefToRef(mappin).GetPhase());
  }

  public final static CName MappinToTexturePart(gamedataMappinVariant mappinVariant, gamedataMappinPhase mappinPhase) {
    if(mappinPhase == gamedataMappinPhase.UndiscoveredPhase) {
      return "undiscovered";
    };
    return MappinToTexturePart(mappinVariant);
  }

  public final static CName MappinToTexturePart(gamedataMappinVariant mappinVariant) {
    switch(mappinVariant) {
      case gamedataMappinVariant.DefaultVariant:
        return "quest";
      case gamedataMappinVariant.DefaultQuestVariant:
        return "minor_quest";
      case gamedataMappinVariant.DefaultInteractionVariant:
        return "quest";
      case gamedataMappinVariant.ConversationVariant:
        return "talk";
      case gamedataMappinVariant.QuestGiverVariant:
        return "quest_giver";
      case gamedataMappinVariant.MinorActivityVariant:
        return "minor_activity";
      case gamedataMappinVariant.ExclamationMarkVariant:
        return "quest";
      case gamedataMappinVariant.RetrievingVariant:
        return "retrieving";
      case gamedataMappinVariant.ThieveryVariant:
        return "thievery";
      case gamedataMappinVariant.SabotageVariant:
        return "sabotage";
      case gamedataMappinVariant.ClientInDistressVariant:
        return "client_in_distress";
      case gamedataMappinVariant.BountyHuntVariant:
        return "bounty_hunt";
      case gamedataMappinVariant.CourierVariant:
        return "courier";
      case gamedataMappinVariant.GangWatchVariant:
        return "gang_watch";
      case gamedataMappinVariant.OutpostVariant:
        return "outpost";
      case gamedataMappinVariant.ResourceVariant:
        return "resource";
      case gamedataMappinVariant.HiddenStashVariant:
        return "hidden_stash";
      case gamedataMappinVariant.HuntForPsychoVariant:
        return "hunt_for_psycho";
      case gamedataMappinVariant.SmugglersDenVariant:
        return "smugglers";
      case gamedataMappinVariant.WanderingMerchantVariant:
        return "wandering_merchant";
      case gamedataMappinVariant.ConvoyVariant:
        return "convoy";
      case gamedataMappinVariant.FixerVariant:
        return "fixer";
      case gamedataMappinVariant.DropboxVariant:
        return "dropbox";
      case gamedataMappinVariant.ApartmentVariant:
        return "apartment";
      case gamedataMappinVariant.ServicePointClothesVariant:
        return "clothes";
      case gamedataMappinVariant.ServicePointFoodVariant:
        return "food_vendor";
      case gamedataMappinVariant.ServicePointBarVariant:
        return "bar";
      case gamedataMappinVariant.ServicePointGunsVariant:
        return "gun";
      case gamedataMappinVariant.ServicePointMedsVariant:
        return "medicine";
      case gamedataMappinVariant.ServicePointMeleeTrainerVariant:
        return "melee";
      case gamedataMappinVariant.ServicePointNetTrainerVariant:
        return "netservice";
      case gamedataMappinVariant.ServicePointProstituteVariant:
        return "prostitute";
      case gamedataMappinVariant.ServicePointRipperdocVariant:
        return "ripperdoc";
      case gamedataMappinVariant.ServicePointTechVariant:
        return "tech";
      case gamedataMappinVariant.ServicePointJunkVariant:
        return "junk_shop";
      case gamedataMappinVariant.FastTravelVariant:
        return "fast_travel";
      case gamedataMappinVariant.EffectDropPointVariant:
        return "dropbox";
      case gamedataMappinVariant.ServicePointDropPointVariant:
        return "dropbox";
      case gamedataMappinVariant.VehicleVariant:
        return "car";
      case gamedataMappinVariant.GrenadeVariant:
        return "grenade";
      case gamedataMappinVariant.CustomPositionVariant:
        return "dynamic_event";
      case gamedataMappinVariant.InvalidVariant:
        return "invalid";
      case gamedataMappinVariant.SitVariant:
        return "Sit";
      case gamedataMappinVariant.GetInVariant:
        return "GetIn";
      case gamedataMappinVariant.GetUpVariant:
        return "GetUp";
      case gamedataMappinVariant.AllowVariant:
        return "Allow";
      case gamedataMappinVariant.BackOutVariant:
        return "BackOut";
      case gamedataMappinVariant.JackInVariant:
        return "JackIn";
      case gamedataMappinVariant.HitVariant:
        return "Hit";
      case gamedataMappinVariant.TakeDownVariant:
        return "TakeDown";
      case gamedataMappinVariant.NonLethalTakedownVariant:
        return "NonLethalTakedown";
      case gamedataMappinVariant.TakeControlVariant:
        return "TakeControl";
      case gamedataMappinVariant.OpenVendorVariant:
        return "OpenVendor";
      case gamedataMappinVariant.DistractVariant:
        return "Distract";
      case gamedataMappinVariant.ChangeToFriendlyVariant:
        return "ChangeToFriendly";
      case gamedataMappinVariant.GunSuicideVariant:
        return "GunSuicide";
      case gamedataMappinVariant.LifepathCorpoVariant:
        return "LifepathCorpo";
      case gamedataMappinVariant.LifepathNomadVariant:
        return "LifepathNomad";
      case gamedataMappinVariant.LifepathStreetKidVariant:
        return "LifepathStreetKid";
      case gamedataMappinVariant.AimVariant:
        return "Aim";
      case gamedataMappinVariant.JamWeaponVariant:
        return "JamWeapon";
      case gamedataMappinVariant.OffVariant:
        return "Off";
      case gamedataMappinVariant.UseVariant:
        return "Use";
      case gamedataMappinVariant.PhoneCallVariant:
        return "PhoneCall";
      case gamedataMappinVariant.SpeechVariant:
        return "talk";
      case gamedataMappinVariant.GPSPortalVariant:
        return "quest";
      case gamedataMappinVariant.FailedCrossingVariant:
        return "failed_crossing";
      case gamedataMappinVariant.TarotVariant:
        return "tarot_card";
      default:
        return "invalid";
    };
  }

  public final static CName MappinToString(gamedataMappinVariant mappinVariant, gamedataMappinPhase mappinPhase) {
    if(mappinPhase == gamedataMappinPhase.UndiscoveredPhase) {
      return "UI-MappinTypes-Undiscovered";
    };
    return MappinToString(mappinVariant);
  }

  public final static CName MappinToString(gamedataMappinVariant mappinVariant) {
    switch(mappinVariant) {
      case gamedataMappinVariant.DefaultQuestVariant:
        return "UI-MappinTypes-MinorQuest";
      case gamedataMappinVariant.ConversationVariant:
        return "UI-MappinTypes-Conversation";
      case gamedataMappinVariant.QuestGiverVariant:
        return "UI-MappinTypes-QuestGiver";
      case gamedataMappinVariant.MinorActivityVariant:
        return "UI-MappinTypes-MinorActivity";
      case gamedataMappinVariant.ExclamationMarkVariant:
        return "UI-MappinTypes-Quest";
      case gamedataMappinVariant.RetrievingVariant:
        return "UI-MappinTypes-Retrieving";
      case gamedataMappinVariant.ThieveryVariant:
        return "UI-MappinTypes-Thievery";
      case gamedataMappinVariant.SabotageVariant:
        return "UI-MappinTypes-Sabotage";
      case gamedataMappinVariant.ClientInDistressVariant:
        return "UI-MappinTypes-ClientinDistress";
      case gamedataMappinVariant.BountyHuntVariant:
        return "UI-MappinTypes-BountyHunt";
      case gamedataMappinVariant.CourierVariant:
        return "UI-MappinTypes-Courier";
      case gamedataMappinVariant.GangWatchVariant:
        return "UI-MappinTypes-GangWatch";
      case gamedataMappinVariant.OutpostVariant:
        return "UI-MappinTypes-Outpost";
      case gamedataMappinVariant.ResourceVariant:
        return "UI-MappinTypes-Resource";
      case gamedataMappinVariant.HiddenStashVariant:
        return "UI-MappinTypes-HiddenStash";
      case gamedataMappinVariant.HuntForPsychoVariant:
        return "UI-MappinTypes-HuntforPsycho";
      case gamedataMappinVariant.SmugglersDenVariant:
        return "UI-MappinTypes-SmugglersDen";
      case gamedataMappinVariant.WanderingMerchantVariant:
        return "UI-MappinTypes-WanderingMerchant";
      case gamedataMappinVariant.ConvoyVariant:
        return "UI-MappinTypes-Convoy";
      case gamedataMappinVariant.FixerVariant:
        return "UI-MappinTypes-Fixer";
      case gamedataMappinVariant.DropboxVariant:
        return "UI-MappinTypes-Dropbox";
      case gamedataMappinVariant.ApartmentVariant:
        return "UI-MappinTypes-Apartment";
      case gamedataMappinVariant.ServicePointClothesVariant:
        return "UI-MappinTypes-ClothingServicePoint";
      case gamedataMappinVariant.ServicePointFoodVariant:
        return "UI-MappinTypes-FoodServicePoint";
      case gamedataMappinVariant.ServicePointBarVariant:
        return "UI-MappinTypes-BarServicePoint";
      case gamedataMappinVariant.ServicePointGunsVariant:
        return "UI-MappinTypes-GunServicePoint";
      case gamedataMappinVariant.ServicePointMedsVariant:
        return "UI-MappinTypes-MedicineServicePoint";
      case gamedataMappinVariant.ServicePointMeleeTrainerVariant:
        return "UI-MappinTypes-MeleeServicePoint";
      case gamedataMappinVariant.ServicePointNetTrainerVariant:
        return "UI-MappinTypes-NetTrainerServicePoint";
      case gamedataMappinVariant.ServicePointProstituteVariant:
        return "UI-MappinTypes-Prostitute";
      case gamedataMappinVariant.ServicePointRipperdocVariant:
        return "UI-MappinTypes-RipperdocServicePoint";
      case gamedataMappinVariant.ServicePointTechVariant:
        return "UI-MappinTypes-TechServicePoint";
      case gamedataMappinVariant.ServicePointJunkVariant:
        return "UI-MappinTypes-JunkServicePoint";
      case gamedataMappinVariant.FastTravelVariant:
        return "UI-MappinTypes-FastTravel";
      case gamedataMappinVariant.EffectDropPointVariant:
        return "UI-MappinTypes-Dropbox";
      case gamedataMappinVariant.ServicePointDropPointVariant:
        return "UI-MappinTypes-Dropbox";
      case gamedataMappinVariant.VehicleVariant:
        return "UI-MappinTypes-Vehicle";
      case gamedataMappinVariant.CustomPositionVariant:
        return "UI-MappinTypes-CustomLocation";
      case gamedataMappinVariant.InvalidVariant:
        return "UI-MappinTypes-Invalid";
      case gamedataMappinVariant.SitVariant:
        return "UI-MappinTypes-Sit";
      case gamedataMappinVariant.GetInVariant:
        return "UI-MappinTypes-GetIn";
      case gamedataMappinVariant.GetUpVariant:
        return "UI-MappinTypes-GetUp";
      case gamedataMappinVariant.AllowVariant:
        return "UI-MappinTypes-Allow";
      case gamedataMappinVariant.BackOutVariant:
        return "UI-MappinTypes-BackOut";
      case gamedataMappinVariant.JackInVariant:
        return "UI-MappinTypes-JackIn";
      case gamedataMappinVariant.HitVariant:
        return "UI-MappinTypes-Hit";
      case gamedataMappinVariant.TakeDownVariant:
        return "UI-MappinTypes-TakeDown";
      case gamedataMappinVariant.NonLethalTakedownVariant:
        return "NonLethalTakedown";
      case gamedataMappinVariant.TakeControlVariant:
        return "TakeControl";
      case gamedataMappinVariant.OpenVendorVariant:
        return "UI-MappinTypes-OpenVendor";
      case gamedataMappinVariant.DistractVariant:
        return "UI-MappinTypes-Distract";
      case gamedataMappinVariant.ChangeToFriendlyVariant:
        return "UI-MappinTypes-ChangeToFriendly";
      case gamedataMappinVariant.GunSuicideVariant:
        return "UI-MappinTypes-GunSuicide";
      case gamedataMappinVariant.LifepathCorpoVariant:
        return "UI-MappinTypes-LifepathCorpo";
      case gamedataMappinVariant.LifepathNomadVariant:
        return "UI-MappinTypes-LifepathNomad";
      case gamedataMappinVariant.LifepathStreetKidVariant:
        return "UI-MappinTypes-LifepathStreetKid";
      case gamedataMappinVariant.AimVariant:
        return "UI-MappinTypes-Aim";
      case gamedataMappinVariant.JamWeaponVariant:
        return "UI-MappinTypes-JamWeapon";
      case gamedataMappinVariant.OffVariant:
        return "UI-MappinTypes-Off";
      case gamedataMappinVariant.UseVariant:
        return "UI-MappinTypes-Use";
      case gamedataMappinVariant.PhoneCallVariant:
        return "UI-MappinTypes-PhoneCall";
      case gamedataMappinVariant.FailedCrossingVariant:
        return "UI-MappinTypes-FailedCrossing";
      case gamedataMappinVariant.TarotVariant:
        return "UI-MappinTypes-Tarot";
    };
    return "UI-MappinTypes-UNKNOWN";
  }

  public final static CName MappinToDescriptionString(gamedataMappinVariant mappinVariant) {
    switch(mappinVariant) {
      case gamedataMappinVariant.DefaultQuestVariant:
        return "UI-MappinTypes-MinorQuestDescription";
      case gamedataMappinVariant.ConversationVariant:
        return "UI-MappinTypes-ConversationDescription";
      case gamedataMappinVariant.QuestGiverVariant:
        return "UI-MappinTypes-QuestGiverDescription";
      case gamedataMappinVariant.MinorActivityVariant:
        return "UI-MappinTypes-MinorActivityDescription";
      case gamedataMappinVariant.ExclamationMarkVariant:
        return "UI-MappinTypes-QuestDescription";
      case gamedataMappinVariant.RetrievingVariant:
        return "UI-MappinTypes-RetrievingDescription";
      case gamedataMappinVariant.ThieveryVariant:
        return "UI-MappinTypes-ThieveryDescription";
      case gamedataMappinVariant.SabotageVariant:
        return "UI-MappinTypes-SabotageDescription";
      case gamedataMappinVariant.ClientInDistressVariant:
        return "UI-MappinTypes-ClientinDistressDescription";
      case gamedataMappinVariant.BountyHuntVariant:
        return "UI-MappinTypes-BountyHuntDescription";
      case gamedataMappinVariant.CourierVariant:
        return "UI-MappinTypes-CourierDescription";
      case gamedataMappinVariant.GangWatchVariant:
        return "UI-MappinTypes-GangWatchDescription";
      case gamedataMappinVariant.OutpostVariant:
        return "UI-MappinTypes-OutpostDescription";
      case gamedataMappinVariant.ResourceVariant:
        return "UI-MappinTypes-ResourceDescription";
      case gamedataMappinVariant.HiddenStashVariant:
        return "UI-MappinTypes-HiddenStashDescription";
      case gamedataMappinVariant.HuntForPsychoVariant:
        return "UI-MappinTypes-HuntforPsychoDescription";
      case gamedataMappinVariant.SmugglersDenVariant:
        return "UI-MappinTypes-SmugglersDenDescription";
      case gamedataMappinVariant.WanderingMerchantVariant:
        return "UI-MappinTypes-WanderingMerchantDescription";
      case gamedataMappinVariant.ConvoyVariant:
        return "UI-MappinTypes-ConvoyDescription";
      case gamedataMappinVariant.FixerVariant:
        return "UI-MappinTypes-FixerDescription";
      case gamedataMappinVariant.DropboxVariant:
        return "UI-MappinTypes-DropboxDescription";
      case gamedataMappinVariant.ApartmentVariant:
        return "UI-MappinTypes-ApartmentDescription";
      case gamedataMappinVariant.ServicePointClothesVariant:
        return "UI-MappinTypes-ClothingServicePointDescription";
      case gamedataMappinVariant.ServicePointFoodVariant:
        return "UI-MappinTypes-FoodServicePointDescription";
      case gamedataMappinVariant.ServicePointBarVariant:
        return "UI-MappinTypes-BarServicePointDescription";
      case gamedataMappinVariant.ServicePointGunsVariant:
        return "UI-MappinTypes-GunServicePointDescription";
      case gamedataMappinVariant.ServicePointMedsVariant:
        return "UI-MappinTypes-MedicineServicePointDescription";
      case gamedataMappinVariant.ServicePointMeleeTrainerVariant:
        return "UI-MappinTypes-MeleeServicePointDescription";
      case gamedataMappinVariant.ServicePointNetTrainerVariant:
        return "UI-MappinTypes-NetTrainerServicePointDescription";
      case gamedataMappinVariant.ServicePointProstituteVariant:
        return "UI-MappinTypes-ProstituteDescription";
      case gamedataMappinVariant.ServicePointRipperdocVariant:
        return "UI-MappinTypes-RipperdocServicePointDescription";
      case gamedataMappinVariant.ServicePointTechVariant:
        return "UI-MappinTypes-TechServicePointDescription";
      case gamedataMappinVariant.ServicePointJunkVariant:
        return "UI-MappinTypes-JunkServicePointDescription";
      case gamedataMappinVariant.FastTravelVariant:
        return "UI-MappinTypes-FastTravelDescription";
      case gamedataMappinVariant.EffectDropPointVariant:
        return "UI-MappinTypes-DropboxDescription";
      case gamedataMappinVariant.ServicePointDropPointVariant:
        return "UI-MappinTypes-DropboxDescription";
      case gamedataMappinVariant.VehicleVariant:
        return "UI-MappinTypes-VehicleDescription";
      case gamedataMappinVariant.CustomPositionVariant:
        return "UI-MappinTypes-CustomLocationDescription";
      case gamedataMappinVariant.InvalidVariant:
        return "UI-MappinTypes-Invalid-Description";
      case gamedataMappinVariant.SitVariant:
        return "UI-MappinTypes-Sit-Description";
      case gamedataMappinVariant.GetInVariant:
        return "UI-MappinTypes-GetIn-Description";
      case gamedataMappinVariant.GetUpVariant:
        return "UI-MappinTypes-GetUp-Description";
      case gamedataMappinVariant.AllowVariant:
        return "UI-MappinTypes-Allow-Description";
      case gamedataMappinVariant.BackOutVariant:
        return "UI-MappinTypes-BackOut-Description";
      case gamedataMappinVariant.JackInVariant:
        return "UI-MappinTypes-JackIn-Description";
      case gamedataMappinVariant.HitVariant:
        return "UI-MappinTypes-Hit-Description";
      case gamedataMappinVariant.TakeDownVariant:
        return "UI-MappinTypes-TakeDown-Description";
      case gamedataMappinVariant.NonLethalTakedownVariant:
        return "NonLethalTakedown-Description";
      case gamedataMappinVariant.TakeControlVariant:
        return "TakeControl-Description";
      case gamedataMappinVariant.OpenVendorVariant:
        return "UI-MappinTypes-OpenVendor-Description";
      case gamedataMappinVariant.DistractVariant:
        return "UI-MappinTypes-Distract-Description";
      case gamedataMappinVariant.ChangeToFriendlyVariant:
        return "UI-MappinTypes-ChangeToFriendly-Description";
      case gamedataMappinVariant.GunSuicideVariant:
        return "UI-MappinTypes-GunSuicide-Description";
      case gamedataMappinVariant.LifepathCorpoVariant:
        return "UI-MappinTypes-LifepathCorpo-Description";
      case gamedataMappinVariant.LifepathNomadVariant:
        return "UI-MappinTypes-LifepathNomad-Description";
      case gamedataMappinVariant.LifepathStreetKidVariant:
        return "UI-MappinTypes-LifepathStreetKid-Description";
      case gamedataMappinVariant.AimVariant:
        return "UI-MappinTypes-Aim-Description";
      case gamedataMappinVariant.JamWeaponVariant:
        return "UI-MappinTypes-JamWeapon-Description";
      case gamedataMappinVariant.OffVariant:
        return "UI-MappinTypes-Off-Description";
      case gamedataMappinVariant.UseVariant:
        return "UI-MappinTypes-Use-Description";
      case gamedataMappinVariant.PhoneCallVariant:
        return "UI-MappinTypes-PhoneCall-Description";
      case gamedataMappinVariant.FailedCrossingVariant:
        return "UI-MappinTypes-FailedCrossingDescription";
      case gamedataMappinVariant.TarotVariant:
        return "UI-MappinTypes-TarotDescription";
    };
    return "UI-MappinTypes-UNKNOWN";
  }

  public final static CName MappinToObjectiveString(gamedataMappinVariant mappinVariant) {
    switch(mappinVariant) {
      case gamedataMappinVariant.GangWatchVariant:
        return "UI-MappinTypes-GangWatchObjective";
      case gamedataMappinVariant.OutpostVariant:
        return "UI-MappinTypes-OutpostObjective";
      case gamedataMappinVariant.ResourceVariant:
        return "UI-MappinTypes-ResourceObjective";
      case gamedataMappinVariant.HiddenStashVariant:
        return "UI-MappinTypes-HiddenStashObjective";
      case gamedataMappinVariant.HuntForPsychoVariant:
        return "UI-MappinTypes-HuntforPsychoObjective";
      case gamedataMappinVariant.SmugglersDenVariant:
        return "UI-MappinTypes-SmugglersDenObjective";
      case gamedataMappinVariant.FailedCrossingVariant:
        return "UI-MappinTypes-FailedCrossingObjective";
      case gamedataMappinVariant.CustomPositionVariant:
        return "UI-MappinTypes-DynamicEventObjective";
      case gamedataMappinVariant.TarotVariant:
        return "UI-MappinTypes-TarotObjective";
    };
    return "UI-MappinTypes-UNKNOWN";
  }

  public final static ref<inkAnimProxy> PlayPreventionBlinkAnimation(wref<inkWidget> widget, out CName initialState) {
    Float engineTime;
    Int32 engineTimeInt;
    Float timeDelay;
    ref<inkAnimDef> animDef;
    ref<inkAnimPadding> animInterp;
    inkAnimOptions animOptions;
    engineTime = ToFloat(GetEngineTime());
    engineTimeInt = Cast(engineTime);
    timeDelay = 1 - engineTime - Cast(engineTimeInt);
    if(timeDelay > 0.5) {
      timeDelay -= 0.5;
      initialState = "Prevention_Blue";
    } else {
      initialState = "Prevention_Red";
    };
    animDef = new inkAnimDef();
    animInterp = new inkAnimPadding();
    animInterp.SetDuration(0.5);
    animDef.AddInterpolator(animInterp);
    animOptions.loopType = inkanimLoopType.Cycle;
    animOptions.loopInfinite = true;
    animOptions.executionDelay = timeDelay;
    return WeakRefToRef(widget).PlayAnimationWithOptions(animDef, animOptions);
  }

  public final static void CyclePreventionState(out CName state) {
    state = state == "Prevention_Red" ? "Prevention_Blue" : "Prevention_Red";
  }
}
