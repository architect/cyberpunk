
public struct PostponedCursorContext {

  public CName context;

  public ref<inkUserData> data;

  public final static void Set(ref<PostponedCursorContext> self, CName c, ref<inkUserData> d) {
    FromScriptRef(self).context = c;
    if(ToBool(d)) {
      FromScriptRef(self).data = d;
    };
  }

  public final static void Reset(ref<PostponedCursorContext> self) {
    FromScriptRef(self).context = "";
    FromScriptRef(self).data = null;
  }
}

public class CursorGameController extends inkGameController {

  protected edit inkWidgetRef m_mainCursor;

  private edit inkWidgetRef m_progressBar;

  private CName m_currentContext;

  private inkMargin m_margin;

  private ref<inkAnimProxy> m_animProxy;

  private ref<MenuCursorUserData> m_data;

  [Default(CursorGameController, false))]
  private Bool m_isCursorVisible;

  private array<PostponedCursorContext> m_postponedContexts;

  [Default(CursorGameController, 0))]
  private Int32 m_readIndex;

  [Default(CursorGameController, 0))]
  private Int32 m_writeIndex;

  [Default(CursorGameController, 20))]
  private Int32 m_bufferSize;

  protected cb Bool OnInitialize() {
    ref<inkCursorInfo> cursorVisibilityInfo;
    ref<inkWidget> root;
    Resize(this.m_postponedContexts, this.m_bufferSize);
    root = WeakRefToRef(GetRootWidget());
    root.RegisterToCallback("OnSetCursorVisibility", this, "OnSetCursorVisibility");
    root.RegisterToCallback("OnSetCursorPosition", this, "OnSetCursorPosition");
    root.RegisterToCallback("OnSetCursorContext", this, "OnSetCursorContext");
    RegisterToGlobalInputCallback("OnPostOnHold", this, "OnHold");
    RegisterToGlobalInputCallback("OnPostOnRelease", this, "OnRelease");
    this.m_isCursorVisible = false;
    cursorVisibilityInfo = Cast(root.GetUserData("inkCursorInfo"));
    cursorVisibilityInfo.SetSize(GetSize(this.m_mainCursor));
    OnSetCursorVisibility(cursorVisibilityInfo.isVisible);
    OnSetCursorPosition(cursorVisibilityInfo.pos);
  }

  protected cb Bool OnUnitialize() {
    ref<inkWidget> root;
    root = WeakRefToRef(GetRootWidget());
    root.UnregisterFromCallback("OnSetCursorVisibility", this, "OnSetCursorVisibility");
    root.UnregisterFromCallback("OnSetCursorPosition", this, "OnSetCursorPosition");
    root.UnregisterFromCallback("OnSetCursorContext", this, "OnSetCursorContext");
    UnregisterFromGlobalInputCallback("OnPostOnHold", this, "OnHold");
    UnregisterFromGlobalInputCallback("OnPostOnRelease", this, "OnRelease");
  }

  protected cb Bool OnSetCursorVisibility(Bool isVisible) {
    if(this.m_isCursorVisible != isVisible) {
      this.m_isCursorVisible = isVisible;
      if(this.m_isCursorVisible) {
        ProcessCursorContext("Show", null);
      } else {
        ProcessCursorContext("Hide", null);
      };
    };
  }

  protected cb Bool OnSetCursorPosition(Vector2 pos) {
    this.m_margin.left = pos.X;
    this.m_margin.top = pos.Y;
    SetMargin(this.m_mainCursor, this.m_margin);
  }

  protected cb Bool OnSetCursorContext(CName context, ref<inkUserData> data) {
    ProcessCursorContext(context, data);
  }

  protected cb Bool OnHold(ref<inkPointerEvent> evt) {
    Float progress;
    array<CName> actionsList;
    progress = evt.GetHoldProgress();
    if(progress >= 1) {
      return false;
    };
    if(this.m_data == null) {
      return false;
    };
    if(DoesActionMatch(evt, this.m_data.GetActions())) {
      UpdateFillPercent(progress);
    };
  }

  protected cb Bool OnRelease(ref<inkPointerEvent> evt) {
    array<CName> actionsList;
    if(ToBool(this.m_data)) {
      actionsList = this.m_data.GetActions();
    };
    if(DoesActionMatch(evt, actionsList)) {
      UpdateFillPercent(0);
    };
  }

  public final void UpdateFillPercent(Float percent) {
    Vector2 newScale;
    newScale.X = percent;
    newScale.Y = 1;
    SetScale(this.m_progressBar, newScale);
  }

  private final void ProcessCursorContext(CName context, ref<inkUserData> data, Bool skipAnim?) {
    Bool startAnim;
    Int32 index;
    Int32 lastPostponedContext;
    Int32 postponedContextsCount;
    CName animationOverride;
    startAnim = false;
    postponedContextsCount = this.m_writeIndex - this.m_readIndex;
    if(ToBool(this.m_animProxy) && this.m_animProxy.IsLoading() || this.m_animProxy.IsPlaying()) {
      if(this.m_currentContext == context && postponedContextsCount == 0) {
        if(data == null) {
          data = this.m_data;
        };
        this.m_animProxy.GotoStartAndStop(true);
        startAnim = true;
      } else {
        lastPostponedContext = Cast(postponedContextsCount) ? this.m_writeIndex - 1 % this.m_bufferSize : -1;
        if(lastPostponedContext >= 0 && this.m_postponedContexts[lastPostponedContext].context == context) {
          Set(ToScriptRef(this.m_postponedContexts[lastPostponedContext]), context, data);
        } else {
          if(postponedContextsCount < this.m_bufferSize) {
            index = this.m_writeIndex % this.m_bufferSize;
            Set(ToScriptRef(this.m_postponedContexts[index]), context, data);
            this.m_writeIndex = this.m_writeIndex + 1;
          };
        };
      };
    } else {
      if(this.m_currentContext != context) {
        startAnim = true;
      };
    };
    if(startAnim) {
      this.m_currentContext = context;
      this.m_data = Cast(data);
      if(ToBool(this.m_data)) {
        animationOverride = this.m_data.GetAnimationOverride();
      };
      UpdateFillPercent(0);
      switch(context) {
        case "Show":
          PlayAnim(skipAnim, "show", animationOverride);
          break;
        case "Hide":
          PlayAnim(skipAnim, "hide", animationOverride);
          break;
        case "Default":
          PlayAnim(skipAnim, "default", animationOverride);
          break;
        case "Hover":
          PlayAnim(skipAnim, "hover", animationOverride);
          GetAudioSystem(GetPlayerControlledObject().GetGame()).Play("ui_menu_hover");
          break;
        case "InvalidAction":
          PlayAnim(skipAnim, "invalid", animationOverride);
      };
    };
  }

  private final void ProcessNextContext(Bool skipAnim) {
    Int32 index;
    PostponedCursorContext postponedContext;
    index = this.m_readIndex % this.m_bufferSize;
    postponedContext = this.m_postponedContexts[index];
    ProcessCursorContext(postponedContext.context, postponedContext.data, skipAnim);
    Reset(ToScriptRef(this.m_postponedContexts[index]));
    this.m_readIndex = this.m_readIndex + 1;
  }

  private final void PlayAnim(Bool skipAnim, CName animation, CName animationOverride, inkAnimOptions animOptions?) {
    if(animationOverride == "") {
      this.m_animProxy = PlayLibraryAnimation(animation, animOptions);
    } else {
      this.m_animProxy = PlayLibraryAnimation(animationOverride, animOptions);
    };
    if(skipAnim) {
      this.m_animProxy.GotoEndAndStop(true);
    } else {
      this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnAnimationFinished");
    };
  }

  private final void OnAnimationFinished(ref<inkAnimProxy> proxy) {
    Int32 postponedContextsCount;
    postponedContextsCount = this.m_writeIndex - this.m_readIndex;
    if(postponedContextsCount > 0) {
      while(postponedContextsCount > 1) {
        ProcessNextContext(true);
        postponedContextsCount = this.m_writeIndex - this.m_readIndex;
      };
      ProcessNextContext(false);
    } else {
      this.m_readIndex = this.m_writeIndex = 0;
    };
  }

  private final Bool DoesActionMatch(ref<inkPointerEvent> evt, array<CName> actionsList) {
    Int32 i;
    Int32 count;
    count = Size(actionsList);
    if(Cast(count)) {
      i = 0;
      while(i < count) {
        if(evt.IsAction(actionsList[i])) {
          return true;
        };
        i += 1;
      };
    };
    return false;
  }
}

public class MenuCursorUserData extends inkUserData {

  private CName animationOverride;

  private array<CName> actions;

  public final void SetAnimationOverride(CName anim) {
    this.animationOverride = anim;
  }

  public final CName GetAnimationOverride() {
    return this.animationOverride;
  }

  public final void AddAction(CName action) {
    Push(this.actions, action);
  }

  public final array<CName> GetActions() {
    return this.actions;
  }

  public final Int32 GetActionsListSize() {
    return Size(this.actions);
  }
}
