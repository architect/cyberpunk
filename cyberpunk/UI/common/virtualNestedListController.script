
public class VirutalNestedListClassifier extends inkVirtualItemTemplateClassifier {

  public Uint32 ClassifyItem(Variant data) {
    ref<VirutalNestedListData> listData;
    listData = Cast(FromVariant(data));
    if(!ToBool(listData)) {
      return 0;
    };
    return listData.m_widgetType;
  }
}

public class VirtualNestedListDataView extends ScriptableDataView {

  public ref<CompareBuilder> m_compareBuilder;

  public Bool m_defaultCollapsed;

  public array<Int32> m_toggledLevels;

  public final void Setup() {
    this.m_compareBuilder = Make();
  }

  public final void SetToggledLevels(array<Int32> toggledLevels, Bool defaultCollapsed) {
    this.m_toggledLevels = toggledLevels;
    this.m_defaultCollapsed = defaultCollapsed;
    Filter();
    EnableSorting();
    Sort();
    DisableSorting();
  }

  private Bool FilterItem(ref<IScriptable> data) {
    ref<VirutalNestedListData> itemData;
    itemData = Cast(data);
    return itemData.m_isHeader || Contains(this.m_toggledLevels, itemData.m_level) != this.m_defaultCollapsed && FilterItems(itemData);
  }

  protected Bool FilterItems(ref<VirutalNestedListData> data) {
    return true;
  }

  private Bool SortItem(ref<IScriptable> left, ref<IScriptable> right) {
    ref<VirutalNestedListData> leftData;
    ref<VirutalNestedListData> rightData;
    leftData = Cast(left);
    rightData = Cast(right);
    this.m_compareBuilder.Reset();
    PreSortItems(this.m_compareBuilder, leftData, rightData);
    this.m_compareBuilder.IntAsc(leftData.m_level, rightData.m_level);
    this.m_compareBuilder.BoolTrue(leftData.m_isHeader, rightData.m_isHeader);
    SortItems(this.m_compareBuilder, leftData, rightData);
    return this.m_compareBuilder.GetBool();
  }

  protected void PreSortItems(ref<CompareBuilder> compareBuilder, ref<VirutalNestedListData> left, ref<VirutalNestedListData> right)

  protected void SortItems(ref<CompareBuilder> compareBuilder, ref<VirutalNestedListData> left, ref<VirutalNestedListData> right)
}

public class VirtualNestedListController extends inkVirtualListController {

  protected ref<VirtualNestedListDataView> m_dataView;

  protected ref<ScriptableDataSource> m_dataSource;

  protected ref<VirutalNestedListClassifier> m_classifier;

  protected Bool m_defaultCollapsed;

  protected array<Int32> m_toggledLevels;

  protected cb Bool OnInitialize() {
    this.m_dataView = GetDataView();
    this.m_dataSource = new ScriptableDataSource();
    this.m_classifier = new VirutalNestedListClassifier();
    this.m_dataView.Setup();
    this.m_dataView.EnableSorting();
    this.m_dataView.SetSource(RefToWeakRef(this.m_dataSource));
    SetClassifier(RefToWeakRef(this.m_classifier));
    SetSource(RefToWeakRef(this.m_dataView));
    this.m_defaultCollapsed = true;
  }

  protected cb Bool OnUninitialize() {
    this.m_dataView.SetSource(null);
    SetSource(null);
    SetClassifier(null);
    this.m_classifier = null;
    this.m_dataSource = null;
  }

  protected ref<VirtualNestedListDataView> GetDataView() {
    ref<VirtualNestedListDataView> result;
    result = new VirtualNestedListDataView();
    return result;
  }

  public void SetData(array<ref<VirutalNestedListData>> data, Bool keepToggledLevels?, Bool sortOnce?) {
    Int32 i;
    array<ref<IScriptable>> castedData;
    i = 0;
    while(i < Size(data)) {
      Push(castedData, data[i]);
      i += 1;
    };
    if(!keepToggledLevels) {
      Clear(this.m_toggledLevels);
    };
    this.m_dataSource.Reset(castedData);
    this.m_dataView.SetToggledLevels(this.m_toggledLevels, this.m_defaultCollapsed);
    EnableSorting();
    if(sortOnce) {
      DisableSorting();
    };
  }

  public void ToggleLevel(Int32 targetLevel) {
    if(Contains(this.m_toggledLevels, targetLevel)) {
      Remove(this.m_toggledLevels, targetLevel);
    } else {
      Push(this.m_toggledLevels, targetLevel);
    };
    this.m_dataView.SetToggledLevels(this.m_toggledLevels, this.m_defaultCollapsed);
  }

  public Bool IsLevelToggled(Int32 targetLevel) {
    return Contains(this.m_toggledLevels, targetLevel);
  }

  public Variant GetItem(Uint32 index) {
    ref<VirutalNestedListData> item;
    item = Cast(this.m_dataView.GetItem(index));
    return ToVariant(item.m_data);
  }

  public Int32 GetDataSize() {
    return Cast(this.m_dataView.Size());
  }

  public void EnableSorting() {
    this.m_dataView.EnableSorting();
  }

  public void DisableSorting() {
    this.m_dataView.DisableSorting();
  }

  public Bool IsSortingEnabled() {
    return this.m_dataView.IsSortingEnabled();
  }
}
