
public class ProjectileLaunchHelper extends IScriptable {

  public final static Bool SpawnProjectileFromScreenCenter(ref<GameObject> ownerObject, CName projectileTemplateName, ref<ItemObject> itemObj) {
    ref<gameprojectileSpawnerLaunchEvent> launchEvent;
    ref<TargetingSystem> targetingSystem;
    ref<IPlacedComponent> targetComponent;
    Vector4 componentPosition;
    targetingSystem = GetTargetingSystem(ownerObject.GetGame());
    launchEvent = new gameprojectileSpawnerLaunchEvent();
    launchEvent.launchParams.logicalPositionProvider = targetingSystem.GetDefaultCrosshairPositionProvider(RefToWeakRef(ownerObject));
    launchEvent.launchParams.logicalOrientationProvider = targetingSystem.GetDefaultCrosshairOrientationProvider(RefToWeakRef(ownerObject));
    launchEvent.templateName = projectileTemplateName;
    launchEvent.owner = RefToWeakRef(ownerObject);
    if(projectileTemplateName != "knife") {
      targetComponent = GetTargetingComponent(RefToWeakRef(ownerObject), TSQ_NPC());
      if(ToBool(targetComponent)) {
        componentPosition = GetTargetingComponentsWorldPosition(targetComponent);
        launchEvent.projectileParams.trackedTargetComponent = RefToWeakRef(targetComponent);
        launchEvent.projectileParams.targetPosition = componentPosition;
      };
    };
    itemObj.QueueEvent(launchEvent);
    return true;
  }

  public final static Bool SetLinearLaunchTrajectory(ref<ProjectileComponent> projectileComponent, Float velocity) {
    ref<LinearTrajectoryParams> linearParams;
    if(velocity < 0) {
      return false;
    };
    linearParams = new LinearTrajectoryParams();
    linearParams.startVel = velocity;
    projectileComponent.AddLinear(linearParams);
    return true;
  }

  public final static Bool SetParabolicLaunchTrajectory(ref<ProjectileComponent> projectileComponent, Float gravitySimulation, Float velocity, Float energyLossFactorAfterCollision) {
    ref<ParabolicTrajectoryParams> parabolicParams;
    if(velocity < 0) {
      return false;
    };
    parabolicParams = GetAccelVelParabolicParams(new Vector4(0,0,gravitySimulation,0), velocity);
    projectileComponent.SetEnergyLossFactor(energyLossFactorAfterCollision, energyLossFactorAfterCollision);
    projectileComponent.AddParabolic(parabolicParams);
    return true;
  }

  public final static Bool SetCurvedLaunchTrajectory(ref<ProjectileComponent> projectileComponent, wref<GameObject> targetObject?, ref<IPlacedComponent> targetComponent, Float startVelocity, Float linearTimeRatio, Float interpolationTimeRatio, Float returnTimeMargin, Float bendTimeRatio, Float bendFactor, Float halfLeanAngle, Float endLeanAngle, Float angleInterpolationDuration) {
    ref<FollowCurveTrajectoryParams> followCurveParams;
    Vector4 customTargetPosition;
    followCurveParams = new FollowCurveTrajectoryParams();
    if(!ToBool(targetComponent) && !ToBool(targetObject) || startVelocity < 0 || linearTimeRatio < 0 || interpolationTimeRatio < 0) {
      return false;
    };
    followCurveParams.startVelocity = startVelocity;
    followCurveParams.linearTimeRatio = linearTimeRatio;
    followCurveParams.interpolationTimeRatio = interpolationTimeRatio;
    followCurveParams.returnTimeMargin = returnTimeMargin;
    followCurveParams.bendTimeRatio = bendTimeRatio;
    followCurveParams.bendFactor = bendFactor;
    followCurveParams.halfLeanAngle = halfLeanAngle;
    followCurveParams.endLeanAngle = endLeanAngle;
    followCurveParams.angleInterpolationDuration = angleInterpolationDuration;
    followCurveParams.targetComponent = RefToWeakRef(targetComponent);
    followCurveParams.target = targetObject;
    projectileComponent.AddFollowCurve(followCurveParams);
    return true;
  }

  public final static Bool SetCustomTargetPositionToFollow(ref<ProjectileComponent> projectileComponent, Matrix localToWorld, Float startVelocity, Float distance, Float sideOffset, Float height, Float linearTimeRatio, Float interpolationTimeRatio, Float returnTimeMargin, Float bendTimeRatio, Float bendFactor, Float accuracy, Float halfLeanAngle, Float endLeanAngle, Float angleInterpolationDuration) {
    ref<FollowCurveTrajectoryParams> followCurveParams;
    Vector4 customTargetPosition;
    followCurveParams = new FollowCurveTrajectoryParams();
    if(startVelocity < 0) {
      return false;
    };
    followCurveParams.startVelocity = startVelocity;
    followCurveParams.linearTimeRatio = linearTimeRatio;
    followCurveParams.interpolationTimeRatio = interpolationTimeRatio;
    followCurveParams.returnTimeMargin = returnTimeMargin;
    followCurveParams.bendTimeRatio = bendTimeRatio;
    followCurveParams.bendFactor = bendFactor;
    followCurveParams.accuracy = accuracy;
    followCurveParams.halfLeanAngle = halfLeanAngle;
    followCurveParams.endLeanAngle = endLeanAngle;
    followCurveParams.angleInterpolationDuration = angleInterpolationDuration;
    customTargetPosition = GetTranslation(localToWorld) + GetAxisY(localToWorld) * distance - GetAxisX(localToWorld) * sideOffset + GetAxisZ(localToWorld) * height;
    followCurveParams.targetPosition = customTargetPosition;
    projectileComponent.AddFollowCurve(followCurveParams);
    return true;
  }
}

public class ProjectileGameEffectHelper extends IScriptable {

  public final static Bool FillProjectileHitAoEData(wref<GameObject> source, wref<GameObject> instigator, Vector4 position, Float radius, ref<Attack_Record> attackRecord?, wref<WeaponObject> weapon?) {
    ref<Attack_GameEffect> attack;
    ref<EffectInstance> effect;
    AttackInitContext attackContext;
    array<ref<gameStatModifierData>> statMods;
    SHitFlag flag;
    array<SHitFlag> hitFlags;
    Int32 i;
    attackContext.record = attackRecord;
    attackContext.instigator = instigator;
    attackContext.source = source;
    attackContext.weapon = weapon;
    attack = Cast(Create(attackContext));
    attack.GetStatModList(statMods);
    effect = attack.PrepareAttack(instigator);
    if(!ToBool(attack)) {
      return false;
    };
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, radius);
    SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, radius);
    SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, position);
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(attack));
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackStatModList, ToVariant(statMods));
    i = 0;
    while(i < attackRecord.GetHitFlagsCount()) {
      flag.flag = ToEnum(Cast(EnumValueFromString("hitFlag", attackRecord.GetHitFlagsItem(i))));
      flag.source = "Attack";
      Push(hitFlags, flag);
      i += 1;
    };
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.flags, ToVariant(hitFlags));
    attack.StartAttack();
    return true;
  }

  public final static Bool FillProjectileHitData(wref<GameObject> source, wref<GameObject> user, ref<ProjectileComponent> projectileComponent, ref<gameprojectileHitEvent> eventData) {
    ref<EffectInstance> effect;
    EffectData effectData;
    effect = projectileComponent.GetGameEffectInstance();
    if(!ToBool(effect)) {
      return false;
    };
    effectData = effect.GetSharedData();
    SetVariant(effectData, GetAllBlackboardDefs().EffectSharedData.projectileHitEvent, ToVariant(eventData));
    effect.Run();
    return true;
  }

  public final static Bool RunEffectFromAttack(wref<GameObject> instigator, wref<GameObject> source, wref<WeaponObject> weapon, ref<Attack_Record> attackRecord, ref<gameprojectileHitEvent> eventData) {
    ref<Attack_GameEffect> attack;
    ref<EffectInstance> effect;
    AttackInitContext attackContext;
    array<ref<gameStatModifierData>> statMods;
    attackContext.record = attackRecord;
    attackContext.instigator = instigator;
    attackContext.source = source;
    attackContext.weapon = weapon;
    attack = Cast(Create(attackContext));
    if(!ToBool(attack)) {
      return false;
    };
    attack.GetStatModList(statMods);
    effect = attack.PrepareAttack(instigator);
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(attack));
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackStatModList, ToVariant(statMods));
    SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.projectileHitEvent, ToVariant(eventData));
    effect.Run();
    return true;
  }
}

public class ProjectileTargetingHelper extends IScriptable {

  public final static ref<IPlacedComponent> GetTargetingComponent(wref<GameObject> ownerObject, TargetSearchQuery filterBy) {
    ref<IPlacedComponent> component;
    EulerAngles angleDist;
    component = GetTargetingSystem(WeakRefToRef(ownerObject).GetGame()).GetComponentClosestToCrosshair(ownerObject, angleDist, filterBy);
    return component;
  }

  public final static Vector4 GetTargetingComponentsWorldPosition(ref<IPlacedComponent> targetComponent) {
    Matrix componentPositionMatrix;
    Vector4 componentPosition;
    componentPositionMatrix = targetComponent.GetLocalToWorld();
    componentPosition = GetTranslation(componentPositionMatrix);
    return componentPosition;
  }

  public final static Vector4 GetObjectCurrentPosition(wref<GameObject> obj) {
    Vector4 objectPosition;
    Variant positionParameter;
    positionParameter = ToVariant(WeakRefToRef(obj).GetWorldPosition());
    objectPosition = FromVariant(positionParameter);
    return objectPosition;
  }
}

public class ProjectileHitHelper extends IScriptable {

  public final static wref<GameObject> GetHitObject(gameprojectileHitInstance hitInstance) {
    ref<GameObject> object;
    object = Cast(WeakRefToRef(hitInstance.hitObject));
    return RefToWeakRef(object);
  }
}

public class ProjectileHelper extends IScriptable {

  public final static void SpawnTrailVFX(ref<ProjectileComponent> projectileComponent) {
    projectileComponent.SpawnTrailVFX();
  }

  public final static Int32 GetPSMBlackboardIntVariable(wref<GameObject> user, BlackboardID_Int id) {
    ref<BlackboardSystem> blackboardSystem;
    ref<IBlackboard> blackboard;
    ref<GameObject> playerPuppet;
    playerPuppet = GetPlayerSystem(WeakRefToRef(user).GetGame()).GetLocalPlayerMainGameObject();
    blackboardSystem = GetBlackboardSystem(WeakRefToRef(user).GetGame());
    blackboard = blackboardSystem.GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return blackboard.GetInt(id);
  }

  public final static ref<EffectInstance> SpawnExplosionAttack(ref<Attack_Record> attackRecord, wref<WeaponObject> weapon, wref<GameObject> instigator, wref<GameObject> source, Vector4 pos?, Float duration?) {
    ref<Attack_GameEffect> attack;
    ref<EffectInstance> effect;
    AttackInitContext attackContext;
    array<ref<gameStatModifierData>> statMods;
    Float range;
    attackContext.record = attackRecord;
    attackContext.instigator = instigator;
    attackContext.source = source;
    attackContext.weapon = weapon;
    attack = Cast(Create(attackContext));
    if(ToBool(attack)) {
      attack.GetStatModList(statMods);
      effect = attack.PrepareAttack(instigator);
      range = attackRecord.Range();
      if(range > 0) {
        SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, range);
        SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, range);
      };
      SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, pos);
      if(duration > 0) {
        SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.duration, duration);
      };
      SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(attack));
      GetDebugDrawHistorySystem(WeakRefToRef(instigator).GetGame()).DrawWireSphere(pos, range, new Color(255,0,0,255), "ProjectileExplosionAttack");
      attack.StartAttack();
    };
    return effect;
  }
}
