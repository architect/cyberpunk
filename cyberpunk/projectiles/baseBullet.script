
public class BaseBullet extends BaseProjectile {

  private ref<IComponent> m_meshComponent;

  protected Float m_countTime;

  protected edit Float m_startVelocity;

  protected edit Float m_lifetime;

  [Default(BaseBullet, true))]
  private Bool m_alive;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "MeshComponent", "IComponent", true);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    OnTakeControl(ri);
    this.m_meshComponent = GetComponent(ri, "MeshComponent");
  }

  private final void Reset() {
    this.m_countTime = 0;
    this.m_alive = true;
    this.m_meshComponent.Toggle(true);
  }

  protected cb Bool OnInitialize(ref<gameprojectileSetUpEvent> eventData) {
    ref<LinearTrajectoryParams> linearParams;
    linearParams = new LinearTrajectoryParams();
    linearParams.startVel = this.m_startVelocity;
    this.m_projectileComponent.AddLinear(linearParams);
    this.m_projectileComponent.ToggleAxisRotation(true);
    this.m_projectileComponent.AddAxisRotation(new Vector4(0,1,0,0), 100);
  }

  private final void StartTrailEffect() {
    this.m_projectileComponent.SpawnTrailVFX();
    PlaySoundEvent(this, "Time_Dilation_Bullet_Trails_bullets_normal");
  }

  protected cb Bool OnShoot(ref<gameprojectileShootEvent> eventData) {
    Reset();
    StartTrailEffect();
  }

  protected cb Bool OnShootTarget(ref<gameprojectileShootTargetEvent> eventData) {
    Reset();
    StartTrailEffect();
  }

  protected cb Bool OnTick(ref<gameprojectileTickEvent> eventData) {
    this.m_countTime += eventData.deltaTime;
    if(this.m_countTime > this.m_lifetime || !this.m_alive) {
      Release();
    };
  }

  protected cb Bool OnCollision(ref<gameprojectileHitEvent> eventData) {
    if(this.m_alive) {
      PlaySoundEvent(this, "Stop_Time_Dilation_Bullet_Trails_bullets_normal");
      DealDamage(eventData);
    };
  }

  protected void DealDamage(ref<gameprojectileHitEvent> eventData) {
    ref<GameObject> object;
    Int32 i;
    ref<EffectInstance> damageEffect;
    damageEffect = this.m_projectileComponent.GetGameEffectInstance();
    SetVariant(damageEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.projectileHitEvent, ToVariant(eventData));
    damageEffect.Run();
    i = 0;
    while(i < Size(eventData.hitInstances)) {
      object = Cast(WeakRefToRef(eventData.hitInstances[i].hitObject));
      if(!object.HasTag("bullet_no_destroy")) {
        this.m_countTime = 0;
        this.m_alive = false;
        this.m_meshComponent.Toggle(false);
        this.m_projectileComponent.ClearTrajectories();
      } else {
        i += 1;
      };
    };
  }

  protected final void PerformAttack(ref<gameprojectileHitEvent> eventData) {
    ref<Attack_GameEffect> explosionAttack;
    ref<EffectInstance> explosionEffect;
    AttackInitContext attackContext;
    array<ref<gameStatModifierData>> statMods;
    attackContext.record = GetAttackRecord("Attacks.REMOVE_BulletWithDamage");
    attackContext.instigator = RefToWeakRef(this);
    attackContext.source = RefToWeakRef(this);
    explosionAttack = Cast(Create(attackContext));
    explosionEffect = explosionAttack.PrepareAttack(RefToWeakRef(this));
    explosionAttack.GetStatModList(statMods);
    SetVariant(explosionEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(explosionAttack));
    SetVariant(explosionEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackStatModList, ToVariant(statMods));
    SetVariant(explosionEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.projectileHitEvent, ToVariant(eventData));
    explosionAttack.StartAttack();
  }
}
