
public class Intercom extends InteractiveDevice {

  private Bool m_isShortGlitchActive;

  private DelayID m_shortGlitchDelayID;

  [Default(Intercom, dev_radio_ditraction_glitching))]
  protected CName m_distractionSound;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "terminalui", "worlduiWidgetComponent", false);
    OnRequestComponents(ri);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.m_uiComponent = RefToWeakRef(Cast(GetComponent(ri, "terminalui")));
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    if(IsUIdirty() && this.m_isInsideLogicArea) {
      RefreshUI();
    };
  }

  protected void OnVisibilityChanged() {
    if(IsReadyForUI() && IsUIdirty()) {
      RefreshUI();
    };
  }

  protected cb Bool OnQuestPickUpCall(ref<QuestPickUpCall> evt) {
    UpdateDisplayUI(IntercomStatus.TALKING);
    UpdateDeviceState();
  }

  protected cb Bool OnQuestHangUpCall(ref<QuestHangUpCall> evt) {
    UpdateDisplayUI(IntercomStatus.CALL_ENDED);
    UpdateDeviceState();
  }

  protected cb Bool OnStartCall(ref<StartCall> evt) {
    if(evt.IsStarted()) {
      UpdateDisplayUI(IntercomStatus.CALLING);
    } else {
      UpdateDisplayUI(IntercomStatus.CALL_MISSED);
    };
    UpdateDeviceState();
  }

  protected cb Bool OnResetIntercom(ref<DelayEvent> evt) {
    UpdateDisplayUI(IntercomStatus.DEFAULT);
    UpdateDeviceState();
  }

  protected void StartGlitching(EGlitchState glitchState, Float intensity?) {
    GlitchData glitchData;
    ref<AdvertGlitchEvent> evt;
    glitchData.state = glitchState;
    glitchData.intensity = intensity;
    if(intensity == 0) {
      intensity = 1;
    };
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(intensity);
    QueueEvent(evt);
    GetBlackboard().SetVariant(GetAllBlackboardDefs().TVDeviceBlackboard.GlitchData, ToVariant(glitchData));
    GetBlackboard().FireCallbacks();
    PlaySoundEvent(this, this.m_distractionSound);
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Start, "hack_fx");
  }

  protected void StopGlitching() {
    GlitchData glitchData;
    ref<AdvertGlitchEvent> evt;
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(0);
    QueueEvent(evt);
    glitchData.state = EGlitchState.NONE;
    GetBlackboard().SetVariant(GetAllBlackboardDefs().TVDeviceBlackboard.GlitchData, ToVariant(glitchData));
    GetBlackboard().FireCallbacks();
    StopSoundEvent(this, this.m_distractionSound);
  }

  protected cb Bool OnStopShortGlitch(ref<StopShortGlitchEvent> evt) {
    this.m_isShortGlitchActive = false;
    if(!GetDevicePS().IsGlitching()) {
      StopGlitching();
    };
  }

  private final void StartShortGlitch() {
    ref<StopShortGlitchEvent> evt;
    if(GetDevicePS().IsGlitching()) {
      return ;
    };
    if(!this.m_isShortGlitchActive) {
      evt = new StopShortGlitchEvent();
      StartGlitching(EGlitchState.DEFAULT, 1);
      this.m_shortGlitchDelayID = GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), evt, 0.25);
      this.m_isShortGlitchActive = true;
    };
  }

  protected cb Bool OnHitEvent(ref<gameHitEvent> hit) {
    OnHitEvent(hit);
    StartShortGlitch();
  }

  protected final void UpdateDisplayUI(IntercomStatus status) {
    GetBlackboard().SetVariant(GetAllBlackboardDefs().IntercomBlackboard.Status, ToVariant(status), true);
    GetBlackboard().FireCallbacks();
    this.m_isUIdirty = true;
  }

  protected void CreateBlackboard() {
    this.m_blackboard = Create(GetAllBlackboardDefs().IntercomBlackboard);
  }

  public const ref<IntercomBlackboardDef> GetBlackboardDef() {
    return GetDevicePS().GetBlackboardDef();
  }

  public const ref<IntercomControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected const ref<IntercomController> GetController() {
    return Cast(this.m_controller);
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.Distract;
  }
}
