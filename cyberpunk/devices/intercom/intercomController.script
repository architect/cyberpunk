
public class IntercomController extends ScriptableDC {

  protected const ref<IntercomControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class IntercomControllerPS extends ScriptableDeviceComponentPS {

  protected Bool m_isCalling;

  protected Bool m_sceneStarted;

  protected Bool m_endingCall;

  private EntityID m_forceLookAt;

  private Bool m_forceFollow;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
  }

  protected void Initialize() {
    Initialize();
  }

  protected void GameAttached()

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetActions(actions, context);
    if(IsON() && !this.m_isCalling && !this.m_sceneStarted) {
      Push(actions, ActionStartCall());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  public void GetQuestActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetQuestActions(actions, context);
    Push(actions, ActionQuestPickUpCall());
    Push(actions, ActionQuestHangUpCall());
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    return true;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> action;
    action = ActionQuickHackDistraction();
    action.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    action.SetDurationValue(GetDistractionDuration(action));
    action.SetInactiveWithReason(!IsDistracting(), "LocKey#7004");
    Push(actions, action);
    FinalizeGetQuickHackActions(actions, context);
  }

  public final const Bool CallStarted() {
    return this.m_isCalling;
  }

  protected final ref<StartCall> ActionStartCall() {
    ref<StartCall> action;
    action = new StartCall();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(GetDeviceName());
    action.SetDurationValue(6);
    action.CreateActionWidgetPackage();
    return action;
  }

  public final EntityNotificationType OnStartCall(ref<StartCall> evt) {
    UseNotifier(evt);
    if(evt.IsStarted()) {
      this.m_isCalling = true;
      this.m_forceFollow = true;
      this.m_forceLookAt = WeakRefToRef(evt.GetExecutor()).GetEntityID();
      RefreshSlaves_Event();
      ExecutePSActionWithDelay(evt, this, evt.GetDurationValue());
    } else {
      if(!this.m_sceneStarted) {
        this.m_forceFollow = false;
        RefreshSlaves_Event();
        ExecutePSActionWithDelay(ActionResetIntercom(), this, 2);
      } else {
        return EntityNotificationType.DoNotNotifyEntity;
      };
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final ref<QuestPickUpCall> ActionQuestPickUpCall() {
    ref<QuestPickUpCall> action;
    action = new QuestPickUpCall();
    action.clearanceLevel = GetQuestClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public final EntityNotificationType OnQuestPickUpCall(ref<QuestPickUpCall> evt) {
    this.m_sceneStarted = true;
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final ref<QuestHangUpCall> ActionQuestHangUpCall() {
    ref<QuestHangUpCall> action;
    action = new QuestHangUpCall();
    action.clearanceLevel = GetQuestClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public final EntityNotificationType OnQuestHangUpCall(ref<QuestHangUpCall> evt) {
    this.m_forceFollow = false;
    RefreshSlaves_Event();
    UseNotifier(evt);
    ExecutePSActionWithDelay(ActionResetIntercom(), this, 2);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final ref<DelayEvent> ActionResetIntercom() {
    ref<DelayEvent> delayEvent;
    delayEvent = new DelayEvent();
    delayEvent.SetUp(this);
    delayEvent.SetProperties();
    delayEvent.AddDeviceName(this.m_deviceName);
    return delayEvent;
  }

  public final EntityNotificationType OnResetIntercom(ref<DelayEvent> evt) {
    this.m_isCalling = false;
    this.m_sceneStarted = false;
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final ref<QuestLookAtTarget> ActionForceFollowTarget() {
    ref<QuestLookAtTarget> action;
    action = new QuestLookAtTarget();
    action.SetUp(this);
    action.SetPropertiesFromScripts(this.m_forceLookAt);
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  protected final ref<QuestStopLookAtTarget> ActionStopFollowingTarget() {
    ref<QuestStopLookAtTarget> action;
    action = new QuestStopLookAtTarget();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public final const array<ref<DeviceComponentPS>> GetImmediateSlaves() {
    EntityID entityID;
    array<ref<DeviceComponentPS>> immediateSlaves;
    entityID = ExtractEntityID(GetID());
    GetDeviceSystem(GetGameInstance()).GetChildren(entityID, immediateSlaves);
    return immediateSlaves;
  }

  public final void RefreshSlaves_Event() {
    ref<RefreshSlavesEvent> evt;
    evt = new RefreshSlavesEvent();
    GetPersistencySystem().QueuePSEvent(GetID(), GetClassName(), evt);
  }

  protected final EntityNotificationType OnRefreshSlavesEvent(ref<RefreshSlavesEvent> evt) {
    RefreshSlaves();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  private final void RefreshSlaves() {
    Int32 i;
    array<ref<DeviceComponentPS>> devices;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(ToBool(Cast(devices[i]))) {
        if(this.m_forceFollow) {
          ExecutePSAction(ActionForceFollowTarget(), devices[i]);
        } else {
          ExecutePSAction(ActionStopFollowingTarget(), devices[i]);
        };
      };
      i = i + 1;
    };
  }

  public const ref<IntercomBlackboardDef> GetBlackboardDef() {
    return GetAllBlackboardDefs().IntercomBlackboard;
  }
}
