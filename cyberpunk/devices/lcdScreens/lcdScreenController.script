
public class SetMessageRecordEvent extends Event {

  [Attrib(customEditor, "TweakDBGroupInheritance;ScreenMessageData")]
  public edit TweakDBID m_messageRecordID;

  public edit Bool m_replaceTextWithCustomNumber;

  public edit Int32 m_customNumber;

  public final String GetFriendlyDescription() {
    return "Set Message Record";
  }
}

public class LcdScreenController extends ScriptableDC {

  protected const ref<LcdScreenControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class LcdScreenControllerPS extends ScriptableDeviceComponentPS {

  [Attrib(category, "UI")]
  [Attrib(customEditor, "TweakDBGroupInheritance;ScreenMessageData")]
  private persistent TweakDBID m_messageRecordID;

  [Attrib(category, "UI")]
  private persistent Bool m_replaceTextWithCustomNumber;

  [Attrib(category, "UI")]
  private persistent Int32 m_customNumber;

  [Attrib(category, "UI")]
  private inline persistent ref<ScreenMessageSelector> m_messageRecordSelector;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
    if(!IsStringValid(this.m_deviceName)) {
      this.m_deviceName = "Gameplay-Devices-DisplayNames-Screen";
    };
  }

  private final EntityNotificationType OnSetMessageRecord(ref<SetMessageRecordEvent> evt) {
    this.m_messageRecordID = evt.m_messageRecordID;
    this.m_replaceTextWithCustomNumber = evt.m_replaceTextWithCustomNumber;
    this.m_customNumber = evt.m_customNumber;
    if(ToBool(this.m_messageRecordSelector)) {
      this.m_messageRecordSelector.SetRecordID(evt.m_messageRecordID);
      this.m_messageRecordSelector.SetReplaceTextWithCustomNumber(evt.m_replaceTextWithCustomNumber);
      this.m_messageRecordSelector.SetCustomNumber(evt.m_customNumber);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    return true;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    ref<ScriptableDeviceAction> currentAction;
    currentAction = ActionGlitchScreen("DeviceAction.GlitchScreenSuicide", "QuickHack.DeviceSuicideHack");
    currentAction.SetDurationValue(GetDistractionDuration(currentAction));
    Push(outActions, currentAction);
    currentAction = ActionGlitchScreen("DeviceAction.GlitchScreenBlind", "QuickHack.BlindHack");
    currentAction.SetDurationValue(GetDistractionDuration(currentAction));
    Push(outActions, currentAction);
    currentAction = ActionGlitchScreen("DeviceAction.GlitchScreenHeartAttack", "QuickHack.HeartAttackHack");
    currentAction.SetDurationValue(GetDistractionDuration(currentAction));
    Push(outActions, currentAction);
    currentAction = ActionGlitchScreen("DeviceAction.GlitchScreenGrenade", "QuickHack.GrenadeHack");
    currentAction.SetDurationValue(GetDistractionDuration(currentAction));
    Push(outActions, currentAction);
    if(!IsDefaultConditionMet(this, context)) {
      SetActionsInactiveAll(ToScriptRef(outActions), "LocKey#7003");
    };
    currentAction = ActionQuickHackDistraction();
    currentAction.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    currentAction.SetDurationValue(GetDistractionDuration(currentAction));
    currentAction.SetInactiveWithReason(IsDefaultConditionMet(this, context), "LocKey#7003");
    Push(outActions, currentAction);
    if(IsGlitching() || IsDistracting()) {
      SetActionsInactiveAll(ToScriptRef(outActions), "LocKey#7004");
    };
    FinalizeGetQuickHackActions(outActions, context);
  }

  public void GetQuestActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    GetQuestActions(outActions, context);
  }

  public const ref<LcdScreenBlackBoardDef> GetBlackboardDef() {
    return GetAllBlackboardDefs().LcdScreenBlackBoard;
  }

  public final const Bool HasCustomNumber() {
    return this.m_replaceTextWithCustomNumber;
  }

  public final const Int32 GetCustomNumber() {
    return this.m_customNumber;
  }

  public final const TweakDBID GetMessageRecordID() {
    TweakDBID id;
    if(ToBool(this.m_messageRecordSelector)) {
      id = this.m_messageRecordSelector.GetRecordID();
    };
    if(!IsValid(id)) {
      id = this.m_messageRecordID;
    };
    return id;
  }

  protected final void SetMessageRecordID(TweakDBID id) {
    this.m_messageRecordID = id;
    if(ToBool(this.m_messageRecordSelector)) {
      this.m_messageRecordSelector.SetRecordID(id);
    };
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.ScreenDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.ScreenDeviceBackground";
  }
}
