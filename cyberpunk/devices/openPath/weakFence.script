
public class WeakFence extends InteractiveDevice {

  protected Float m_impulseForce;

  protected Vector4 m_impulseVector;

  protected edit const array<CName> m_sideTriggerNames;

  protected array<ref<TriggerComponent>> m_triggerComponents;

  protected CName m_currentWorkspotSuffix;

  protected ref<OffMeshConnectionComponent> m_offMeshConnectionComponent;

  protected ref<IPlacedComponent> m_physicalMesh;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    Int32 i;
    OnRequestComponents(ri);
    RequestComponent(ri, "offMeshConnection", "OffMeshConnectionComponent", true);
    RequestComponent(ri, "fence_door", "IPlacedComponent", true);
    i = 0;
    while(i < Size(this.m_sideTriggerNames)) {
      RequestComponent(ri, this.m_sideTriggerNames[i], "TriggerComponent", true);
      i += 1;
    };
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    Int32 i;
    OnTakeControl(ri);
    this.m_offMeshConnectionComponent = Cast(GetComponent(ri, "offMeshConnection"));
    this.m_physicalMesh = Cast(GetComponent(ri, "fence_door"));
    this.m_controller = Cast(GetComponent(ri, "controller"));
    i = 0;
    while(i < Size(this.m_sideTriggerNames)) {
      Push(this.m_triggerComponents, Cast(GetComponent(ri, this.m_sideTriggerNames[i])));
      i += 1;
    };
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    if(GetDevicePS().IsDisabled()) {
      this.m_physicalMesh.Toggle(false);
      EnableOffMeshConnections();
    } else {
      DisableOffMeshConnections();
    };
  }

  protected const ref<WeakFenceController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<WeakFenceControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected cb Bool OnActivateDevice(ref<ActivateDevice> evt) {
    PlayWorkspotAnimations();
    EnableOffMeshConnections();
  }

  protected cb Bool OnActionEngineering(ref<ActionEngineering> evt) {
    PlayWorkspotAnimations();
    EnableOffMeshConnections();
  }

  protected final void PlayWorkspotAnimations() {
    ref<WorkspotGameSystem> workspotSystem;
    ref<IBlackboard> playerStateMachineBlackboard;
    playerStateMachineBlackboard = GetBlackboardSystem(GetGame()).GetLocalInstanced(GetPlayerSystem(GetGame()).GetLocalPlayerControlledGameObject().GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, true);
    CheckCurrentSide();
    workspotSystem = GetWorkspotSystem(GetGame());
    workspotSystem.PlayInDevice(this, GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject(), "lockedCamera", "playerWorkspot" + this.m_currentWorkspotSuffix, "deviceWorkspot" + this.m_currentWorkspotSuffix, "fence_sync");
  }

  protected cb Bool OnWorkspotFinished(CName componentName) {
    OnWorkspotFinished(componentName);
    UpdateAnimState();
  }

  protected final void CheckCurrentSide() {
    String finalName;
    ref<GameObject> activator;
    Int32 i;
    Int32 j;
    array<ref<Entity>> overlappingEntities;
    i = 0;
    while(i < Size(this.m_triggerComponents)) {
      overlappingEntities = this.m_triggerComponents[i].GetOverlappingEntities();
      j = 0;
      while(j < Size(overlappingEntities)) {
        if(Cast(overlappingEntities[j]).IsPlayer()) {
          finalName = "Side" + ToString(i + 1);
          this.m_currentWorkspotSuffix = StringToName(finalName);
        };
        j += 1;
      };
      i += 1;
    };
  }

  private final void UpdateAnimState() {
    ref<AnimFeature_SimpleDevice> animFeature;
    animFeature = new AnimFeature_SimpleDevice();
    animFeature.isOpen = true;
    if(this.m_currentWorkspotSuffix == "Side1") {
      animFeature.isOpenLeft = true;
    } else {
      animFeature.isOpenRight = true;
    };
    ApplyFeature(this, "weakFence", animFeature);
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.OpenPath;
  }

  protected final void EnableOffMeshConnections() {
    if(this.m_offMeshConnectionComponent != null) {
      this.m_offMeshConnectionComponent.EnableOffMeshConnection();
      this.m_offMeshConnectionComponent.EnableForPlayer();
    };
  }

  protected final void DisableOffMeshConnections() {
    if(this.m_offMeshConnectionComponent != null) {
      this.m_offMeshConnectionComponent.DisableOffMeshConnection();
      this.m_offMeshConnectionComponent.DisableForPlayer();
    };
  }
}
