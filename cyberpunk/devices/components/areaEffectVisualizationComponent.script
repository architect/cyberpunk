
public class AreaEffectVisualizationComponent extends ScriptableComponent {

  protected ref<FxResourceMapperComponent> m_fxResourceMapper;

  private array<ref<GameEffectTargetVisualizationData>> m_forceHighlightTargetBuckets;

  private array<CName> m_availableQuickHacks;

  private array<CName> m_availablespiderbotActions;

  private ref<BaseScriptableAction> m_activeAction;

  [Default(AreaEffectVisualizationComponent, -1))]
  private Int32 m_activeEffectIndex;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "FxResourceMapper", "FxResourceMapperComponent", false);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.m_fxResourceMapper = Cast(GetComponent(ri, "FxResourceMapper"));
  }

  protected cb Bool OnHUDInstruction(ref<HUDInstruction> evt) {
    if(GetOwner().GetHudManager().IsQuickHackPanelOpened()) {
      return false;
    };
    if(evt.highlightInstructions.GetState() == InstanceState.ON || evt.highlightInstructions.GetState() == InstanceState.HIDDEN && evt.highlightInstructions.isLookedAt) {
      ResolveAreaEffectVisualisations(true);
    } else {
      if(evt.highlightInstructions.WasProcessed()) {
        ResolveAreaEffectVisualisations(false);
      };
    };
  }

  protected final ref<FxResourceMapperComponent> GetFxMapper() {
    return Cast(GetOwner()).GetFxResourceMapper();
  }

  public final void ResolveAreaEffectVisualisations(Bool activated) {
    if(GetFxMapper().GetAreaEffectDataSize() < 0 && GetFxMapper().GetAreaEffectInFocusSize() < 0) {
      return ;
    };
    ResolveAreaEffectsVisibility(activated);
  }

  protected cb Bool OnAreaEffectVisualisationRequest(ref<AreaEffectVisualisationRequest> evt) {
    Int32 areaEffectIndex;
    areaEffectIndex = GetFxMapper().GetAreaEffectDataIndexByName(evt.areaEffectID);
    if(areaEffectIndex != -1) {
      ToggleAreaEffectVisibility(areaEffectIndex, evt.show);
    };
  }

  protected void ResolveAreaEffectsVisibility(Bool show) {
    ResolveAreaSpiderbotVisibility(show);
    ResolveAreaEffectsInFocusModeVisibility(show);
  }

  protected void ResolveAreaEffectsInFocusModeVisibility(Bool show) {
    Int32 i;
    Int32 effectIndex;
    i = 0;
    while(i < GetFxMapper().GetAreaEffectInFocusSize()) {
      if(!GetFxMapper().GetAreaEffectInFocusModeByIndex(i).onSelf) {
      } else {
        effectIndex = GetFxMapper().GetAreaEffectDataIndexByName(GetFxMapper().GetAreaEffectInFocusModeByIndex(i).areaEffectID);
        if(effectIndex >= 0) {
          ToggleAreaEffectVisibility(effectIndex, show);
        };
      };
      i += 1;
    };
  }

  protected void ResolveAreaQuickHacksVisibility(Bool show) {
    Int32 i;
    Int32 quickHackIndex;
    array<CName> availableQuickHacks;
    if(show) {
      availableQuickHacks = Cast(GetOwner()).GetDevicePS().GetAvailableQuickHacks();
      if(Size(availableQuickHacks) > 0) {
        this.m_availableQuickHacks = availableQuickHacks;
      };
    };
    i = 0;
    while(i < Size(this.m_availableQuickHacks)) {
      quickHackIndex = GetFxMapper().GetAreaEffectDataIndexByName(this.m_availableQuickHacks[i]);
      if(quickHackIndex >= 0) {
        ToggleAreaEffectVisibility(quickHackIndex, show);
      };
      i += 1;
    };
  }

  protected void ResolveAreaQuickHacksVisibility(Bool show, ref<BaseScriptableAction> action) {
    Int32 i;
    Int32 quickHackIndex;
    ref<AreaEffectVisualisationRequest> evt;
    if(action == null) {
      return ;
    };
    quickHackIndex = GetFxMapper().GetAreaEffectDataIndexByAction(action);
    if(show && quickHackIndex == this.m_activeEffectIndex || !show && this.m_activeEffectIndex == -1) {
      return ;
    };
    if(quickHackIndex >= 0) {
      if(show && this.m_activeEffectIndex >= 0) {
        ToggleAreaEffectVisibility(this.m_activeEffectIndex, false, action);
        this.m_activeAction = null;
        this.m_activeEffectIndex = -1;
        return ;
      };
      ToggleAreaEffectVisibility(quickHackIndex, show);
    } else {
      if(this.m_activeEffectIndex >= 0) {
        ToggleAreaEffectVisibility(this.m_activeEffectIndex, false);
        show = false;
      };
    };
    if(show) {
      this.m_activeAction = action;
      this.m_activeEffectIndex = quickHackIndex;
    } else {
      this.m_activeAction = null;
      this.m_activeEffectIndex = -1;
    };
  }

  protected void ResolveAreaSpiderbotVisibility(Bool show) {
    Int32 i;
    Int32 actionIndex;
    array<CName> availablespiderbotActions;
    if(show) {
      availablespiderbotActions = Cast(GetOwner()).GetDevicePS().GetAvailableSpiderbotActions();
      if(Size(availablespiderbotActions) > 0) {
        this.m_availablespiderbotActions = availablespiderbotActions;
      };
    };
    i = 0;
    while(i < Size(this.m_availablespiderbotActions)) {
      actionIndex = GetFxMapper().GetAreaEffectDataIndexByName(this.m_availablespiderbotActions[i]);
      if(actionIndex >= 0) {
        ToggleAreaEffectVisibility(actionIndex, show);
      };
      i += 1;
    };
  }

  protected final void ToggleAreaEffectVisibility(Int32 effectDataIDX, Bool show, ref<IScriptable> responseData?) {
    if(show) {
      StartDrawingAreaEffectRange(GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX));
      if(GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).highlightTargets) {
        StartHighlightingTargets(effectDataIDX, responseData);
      };
    } else {
      StopDrawingAreaEffectRange(GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX));
      StopHighlightingTargets(effectDataIDX, responseData);
    };
  }

  protected final void StartDrawingAreaEffectRange(ref<AreaEffectData> effectData) {
    ref<worldEffectBlackboard> effectBlackboard;
    if(!effectData.useIndicatorEffect || !IsNameValid(effectData.indicatorEffectName)) {
      return ;
    };
    effectBlackboard = new worldEffectBlackboard();
    effectBlackboard.SetValue("change_size", effectData.indicatorEffectSize);
    StartEffectEvent(GetOwner(), effectData.indicatorEffectName, false, effectBlackboard);
  }

  protected final void StopDrawingAreaEffectRange(ref<AreaEffectData> effectData) {
    if(!effectData.useIndicatorEffect || !IsNameValid(effectData.indicatorEffectName)) {
      return ;
    };
    StopEffectEvent(GetOwner(), effectData.indicatorEffectName);
  }

  protected final void StartHighlightingTargets(Int32 effectDataIDX, ref<IScriptable> responseData?) {
    ref<EffectInstance> effect;
    Vector4 position;
    ref<FocusForcedHighlightData> highlight;
    gamedataStimType stimType;
    ref<Device> device;
    ref<PuppetForceVisionAppearanceData> bbData;
    if(GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).effectInstance == null) {
      device = Cast(GetOwner());
      position = GetOwner().GetAcousticQuerryStartPoint();
      if(IsNameValid(GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).gameEffectOverrideName)) {
        effect = GetGameEffectSystem(GetOwner().GetGame()).CreateEffectStatic("forceVisionAppearanceOnNPC", GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).gameEffectOverrideName, GetOwner());
      } else {
        effect = GetGameEffectSystem(GetOwner().GetGame()).CreateEffectStatic("forceVisionAppearanceOnNPC", "inRange", GetOwner());
      };
      bbData = new PuppetForceVisionAppearanceData();
      bbData.m_highlightType = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).highlightType;
      bbData.m_outlineType = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).outlineType;
      bbData.m_effectName = NameToString(GetFxMapper().GetAreaEffectDataNameByIndex(effectDataIDX));
      bbData.m_priority = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).highlightPriority;
      SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.maxPathLength, GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).stimRange * 1.5);
      SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, position);
      SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).stimRange);
      SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forceVisionAppearanceData, ToVariant(bbData));
      if(ToBool(device)) {
        stimType = MapStimType(GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).stimType);
        SetInt(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.stimType, ToInt(stimType));
      };
      effect.Run();
      GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).SetEffectInstance(effect);
    };
  }

  protected final void StopHighlightingTargets(Int32 effectDataIDX, ref<IScriptable> responseData?) {
    Int32 i;
    ref<ForceVisionApperanceEvent> evt;
    ref<FocusForcedHighlightData> highlight;
    ref<EffectInstance> effectInstance;
    highlight = new FocusForcedHighlightData();
    highlight.sourceID = GetOwner().GetEntityID();
    highlight.sourceName = GetFxMapper().GetAreaEffectDataNameByIndex(effectDataIDX);
    highlight.highlightType = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).highlightType;
    highlight.outlineType = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).outlineType;
    highlight.priority = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).highlightPriority;
    highlight.isRevealed = true;
    effectInstance = GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).effectInstance;
    if(ToBool(effectInstance)) {
      effectInstance.Terminate();
    };
    GetFxMapper().GetAreaEffectDataByIndex(effectDataIDX).EffectInstanceClear();
    CancelForcedVisionAppearance(highlight);
    evt = new ForceVisionApperanceEvent();
    evt.apply = false;
    evt.forcedHighlight = highlight;
    evt.responseData = responseData;
    SendEventToBucket(highlight.sourceName, evt);
    RemoveBucket(highlight.sourceName);
  }

  protected final void ForceVisionAppearance(ref<FocusForcedHighlightData> data) {
    ref<ForceVisionApperanceEvent> evt;
    evt = new ForceVisionApperanceEvent();
    evt.forcedHighlight = data;
    evt.apply = true;
    GetPersistencySystem(GetOwner().GetGame()).QueueEntityEvent(GetOwner().GetEntityID(), evt);
  }

  protected final void CancelForcedVisionAppearance(ref<FocusForcedHighlightData> data) {
    ref<ForceVisionApperanceEvent> evt;
    evt = new ForceVisionApperanceEvent();
    evt.forcedHighlight = data;
    evt.apply = false;
    if(GetOwner().HasHighlight(data.highlightType, data.outlineType, GetOwner().GetEntityID(), data.sourceName)) {
      GetPersistencySystem(GetOwner().GetGame()).QueueEntityEvent(GetOwner().GetEntityID(), evt);
    };
  }

  protected cb Bool OnAddForceHighlightTarget(ref<AddForceHighlightTargetEvent> evt) {
    AddTargetToBucket(evt.effecName, evt.targetID);
  }

  protected cb Bool OnQHackWheelItemChanged(ref<QHackWheelItemChangedEvent> evt) {
    if(!evt.currentEmpty) {
      ResolveAreaQuickHacksVisibility(true, evt.commandData.m_action);
    } else {
      ResolveAreaQuickHacksVisibility(false, this.m_activeAction);
    };
  }

  protected cb Bool OnResponse(ref<ResponseEvent> evt) {
    ref<BaseScriptableAction> action;
    action = Cast(evt.responseData);
    if(ToBool(action)) {
      ResolveAreaQuickHacksVisibility(true, action);
    };
  }

  protected final void AddTargetToBucket(CName bucketName, EntityID entityID) {
    Int32 i;
    ref<GameEffectTargetVisualizationData> newBucket;
    i = 0;
    while(i < Size(this.m_forceHighlightTargetBuckets)) {
      if(this.m_forceHighlightTargetBuckets[i].GetBucketName() == bucketName) {
        this.m_forceHighlightTargetBuckets[i].AddTargetToBucket(entityID);
        return ;
      };
      i += 1;
    };
    newBucket = new GameEffectTargetVisualizationData();
    newBucket.SetBucketName(bucketName);
    newBucket.AddTargetToBucket(entityID);
    Push(this.m_forceHighlightTargetBuckets, newBucket);
    1 + 1;
  }

  protected final void SendEventToBucket(CName bucketName, ref<Event> evt) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forceHighlightTargetBuckets)) {
      if(this.m_forceHighlightTargetBuckets[i].GetBucketName() == bucketName) {
        this.m_forceHighlightTargetBuckets[i].SendEventToAll(GetOwner().GetGame(), evt);
        return ;
      };
      i += 1;
    };
  }

  protected final void RemoveBucket(CName bucketName) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forceHighlightTargetBuckets)) {
      if(this.m_forceHighlightTargetBuckets[i].GetBucketName() == bucketName) {
        Erase(this.m_forceHighlightTargetBuckets, i);
        return ;
      };
      i += 1;
    };
  }
}

public class GameEffectTargetVisualizationData extends IScriptable {

  private CName bucketName;

  private array<EntityID> m_forceHighlightTargets;

  public final const CName GetBucketName() {
    return this.bucketName;
  }

  public final void SetBucketName(CName _bucketName) {
    this.bucketName = _bucketName;
  }

  public final void AddTargetToBucket(EntityID entityID) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forceHighlightTargets)) {
      if(this.m_forceHighlightTargets[i] == entityID) {
        return ;
      };
      i += 1;
    };
    Push(this.m_forceHighlightTargets, entityID);
  }

  public final void ClearBucket() {
    Clear(this.m_forceHighlightTargets);
  }

  public final void SendEventToAll(GameInstance instance, ref<Event> evt) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_forceHighlightTargets)) {
      GetPersistencySystem(instance).QueueEntityEvent(this.m_forceHighlightTargets[i], evt);
      i += 1;
    };
  }
}
