
public class VirtualSystemPS extends MasterControllerPS {

  private wref<MasterControllerPS> m_owner;

  protected array<ref<DeviceComponentPS>> m_slaves;

  private Bool m_slavesCached;

  public final const Bool IsPartOfSystem(PersistentID targetID) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_slaves)) {
      if(targetID == this.m_slaves[i].GetID()) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const Bool IsPartOfSystem(ref<DeviceComponentPS> target) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_slaves)) {
      if(target == this.m_slaves[i]) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public SDeviceWidgetPackage GetDeviceWidget(GetActionsContext context) {
    SDeviceWidgetPackage widgetData;
    ref<TerminalSystemCustomData> customData;
    widgetData = GetDeviceWidget(context);
    if(widgetData.isValid) {
      customData = new TerminalSystemCustomData();
      customData.connectedDevices = Size(this.m_slaves);
      widgetData.customData = customData;
    };
    return widgetData;
  }

  public final void GetDeviceWidget(GetActionsContext context, out array<SDeviceWidgetPackage> data) {
    Push(data, GetDeviceWidget(context));
  }

  public const String GetDeviceStatus() {
    if(IsON()) {
      return "LocKey#51459";
    };
    return "LocKey#51460";
  }

  protected TweakDBID GetInkWidgetTweakDBID(GetActionsContext context) {
    return "DevicesUIDefinitions.DynamicSystemLayoutWidget";
  }

  protected ref<ThumbnailUI> ActionThumbnailUI() {
    ref<ThumbnailUI> action;
    action = ActionThumbnailUI();
    action.CreateThumbnailWidgetPackage("DevicesUIDefinitions.SystemDeviceThumnbnailWidget", "LocKey#42210");
    return action;
  }

  public final void Initialize(array<ref<DeviceComponentPS>> slaves, ref<MasterControllerPS> owner) {
    Int32 i;
    i = 0;
    while(i < Size(slaves)) {
      Push(this.m_slaves, slaves[i]);
      i += 1;
    };
    this.m_slavesCached = true;
    this.m_owner = RefToWeakRef(owner);
  }

  public Bool GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    Push(outActions, ActionToggleON());
    return true;
  }

  public EntityNotificationType OnToggleON(ref<ToggleON> evt) {
    ref<ScriptableDeviceAction> actionToSend;
    OnToggleON(evt);
    if(IsON()) {
      actionToSend = ActionSetDeviceON();
    } else {
      actionToSend = ActionSetDeviceOFF();
    };
    SendActionToAllSlaves(actionToSend);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected const void SendActionToAllSlaves(ref<ScriptableDeviceAction> action) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_slaves)) {
      ExecutePSAction(action, this.m_slaves[i]);
      i += 1;
    };
  }
}

public class SurveillanceSystemUIPS extends VirtualSystemPS {

  public Bool GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    GetActions(outActions, context);
    Push(outActions, ActionToggleTakeOverControl());
    return true;
  }

  protected EntityNotificationType OnToggleTakeOverControl(ref<ToggleTakeOverControl> evt) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_slaves)) {
      if(Cast(this.m_slaves[i]).CanPlayerTakeOverControl()) {
        QueuePSEvent(RefToWeakRef(this.m_slaves[i]), evt);
        return EntityNotificationType.DoNotNotifyEntity;
      };
      i += 1;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.CameraDeviceBackground";
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.CameraDeviceIcon";
  }
}

public class DoorSystemUIPS extends VirtualSystemPS {

  private persistent Bool m_isOpen;

  public Bool GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    Push(outActions, ActionToggleOpen());
    return true;
  }

  public final ref<ToggleOpen> ActionToggleOpen() {
    ref<ToggleOpen> action;
    action = new ToggleOpen();
    action.clearanceLevel = GetToggleOpenClearance();
    action.SetUp(this);
    action.SetProperties(this.m_isOpen);
    action.AddDeviceName(GetDeviceName());
    action.CreateInteraction();
    action.CreateActionWidgetPackage();
    return action;
  }

  protected final EntityNotificationType OnToggleOpen(ref<ToggleOpen> evt) {
    SendActionToAllSlaves(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }
}

public class MediaSystemUIPS extends VirtualSystemPS {

  public Bool GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    return true;
  }
}

public class CustomSystemUIPS extends VirtualSystemPS {

  public final void Initialize(array<ref<DeviceComponentPS>> slaves, ref<MasterControllerPS> owner, CName systemName, array<CName> actions) {
    Int32 i;
    Initialize(slaves, owner);
  }
}
