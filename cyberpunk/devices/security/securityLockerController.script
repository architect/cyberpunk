
public class UseSecurityLocker extends ActionBool {

  public final void SetProperties(Bool shouldDeposit) {
    this.actionName = "UseSecurityLocker";
    this.prop = SetUpProperty_Bool(this.actionName, shouldDeposit, "LocKey#286", "LocKey#287");
  }

  public String GetTweakDBChoiceRecord() {
    if(!FromVariant(this.prop.first)) {
      return "DepositWeapons";
    };
    return "RetrieveWeapons";
  }
}

public class SecurityLockerController extends ScriptableDC {

  protected const ref<SecurityLockerControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class SecurityLockerControllerPS extends ScriptableDeviceComponentPS {

  private SecurityLockerProperties m_securityLockerProperties;

  private persistent Bool m_isStoringPlayerEquipement;

  public final const Bool ShouldDisableCyberware() {
    return this.m_securityLockerProperties.disableCyberware;
  }

  public final const ESecurityAccessLevel GetAuthorizationLevel() {
    return this.m_securityLockerProperties.securityLevelAccessGranted;
  }

  public final const Bool GetIsEmpty() {
    return !this.m_isStoringPlayerEquipement;
  }

  public final const Bool GetIsStoringPlayerEquipement() {
    return this.m_isStoringPlayerEquipement;
  }

  public final const CName GetStoreSFX() {
    return this.m_securityLockerProperties.storeWeaponSFX;
  }

  public final const CName GetReturnSFX() {
    return this.m_securityLockerProperties.pickUpWeaponSFX;
  }

  protected void Initialize() {
    Initialize();
  }

  private final ref<UseSecurityLocker> ActionUseSecurityLocker(ref<GameObject> executor) {
    ref<UseSecurityLocker> action;
    action = new UseSecurityLocker();
    action.clearanceLevel = GetInteractiveClearance();
    action.SetProperties(this.m_isStoringPlayerEquipement);
    action.SetExecutor(RefToWeakRef(executor));
    action.SetUp(this);
    action.AddDeviceName(GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  public final EntityNotificationType OnUseSecurityLocker(ref<UseSecurityLocker> evt) {
    ref<TogglePersonalLink> togglePersonalLink;
    if(ShouldDisableCyberware()) {
      togglePersonalLink = ActionTogglePersonalLink(evt.GetExecutor());
      togglePersonalLink.SetIllegal(false);
      ExecutePSAction(togglePersonalLink, evt.GetInteractionLayer());
      return EntityNotificationType.DoNotNotifyEntity;
    };
    this.m_isStoringPlayerEquipement = !this.m_isStoringPlayerEquipement;
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected void ResolvePersonalLinkConnection(ref<TogglePersonalLink> evt, Bool abortOperation) {
    ResolvePersonalLinkConnection(evt, abortOperation);
    if(abortOperation) {
      return ;
    };
    QueueEntityEvent(GetMyEntityID(), ActionUseSecurityLocker(WeakRefToRef(evt.GetExecutor())));
    this.m_isStoringPlayerEquipement = !this.m_isStoringPlayerEquipement;
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    if(!GetActions(actions, context)) {
      return false;
    };
    if(IsON() && context.requestType == gamedeviceRequestType.Direct) {
      if(!IsPersonalLinkConnected() && !IsPersonalLinkConnecting()) {
        Push(actions, ActionUseSecurityLocker(WeakRefToRef(context.processInitiatorObject)));
      };
    };
    return true;
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceBackground";
  }
}
