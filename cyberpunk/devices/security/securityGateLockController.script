
public class SecurityGateLockController extends ScriptableDC {

  protected const ref<SecurityGateLockControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class SecurityGateLockControllerPS extends ScriptableDeviceComponentPS {

  public array<TrespasserEntry> m_tresspasserList;

  public EntityID m_entranceToken;

  public Bool m_isLeaving;

  [Default(SecurityGateLockControllerPS, true))]
  public Bool m_isLocked;

  public final const Bool IsLocked() {
    return this.m_isLocked;
  }

  public final void UpdateTrespassersList(ref<TriggerEvent> evt, Bool isEntering) {
    Int32 index;
    ref<ScriptedPuppet> trespasser;
    if(evt.componentName != "enteringArea" && evt.componentName != "centeredArea" && evt.componentName != "leavingArea") {
      return ;
    };
    trespasser = Cast(GetEntity(evt.activator));
    if(!ToBool(trespasser)) {
      return ;
    };
    if(!ToBool(Cast(trespasser))) {
      return ;
    };
    if(IsTrespasserOnTheList(trespasser, index)) {
      UpdateTrespasserEntry(index, isEntering, evt.componentName);
    } else {
      AddTrespasserEntry(trespasser, evt.componentName);
    };
  }

  private final Bool IsTrespasserOnTheList(ref<ScriptedPuppet> trespasser, out Int32 index) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_tresspasserList)) {
      if(WeakRefToRef(this.m_tresspasserList[i].trespasser) == trespasser) {
        index = i;
        return true;
      };
      i += 1;
    };
    index = -1;
    return false;
  }

  private final void UpdateTrespasserEntry(Int32 index, Bool isEntering, CName areaName) {
    TrespasserEntry t;
    t = this.m_tresspasserList[index];
    switch(areaName) {
      case "enteringArea":
        this.m_tresspasserList[index].isInsideA = isEntering;
        break;
      case "leavingArea":
        this.m_tresspasserList[index].isInsideB = isEntering;
        break;
      case "centeredArea":
        this.m_tresspasserList[index].isInsideScanner = isEntering;
        break;
      default:
        if(!IsFinal()) {
          LogDevices(this, NameToString(areaName) + "is not supported. Check if Security Gate entity has proper StaticAreaComponents", ELogType.WARNING);
        };
    };
    return ;
  }

  private final const Bool IsLegallyLeaving(TrespasserEntry t) {
    CName lastName;
    CName entrance;
    CName center;
    CName leaving;
    Int32 stackSize;
    entrance = "enteringArea";
    center = "centeredArea";
    leaving = "leavingArea";
    if(Size(t.areaStack) < 3) {
      return false;
    };
    if(t.areaStack[0] != entrance) {
      return false;
    };
    if(Last(t.areaStack) == leaving) {
      stackSize = Size(t.areaStack);
      if(t.areaStack[stackSize - 2] == center) {
        return true;
      };
      return false;
    };
    return false;
  }

  private final void AddTrespasserEntry(ref<ScriptedPuppet> trespasser, CName areaName) {
    TrespasserEntry newEntry;
    newEntry.trespasser = RefToWeakRef(trespasser);
    Push(this.m_tresspasserList, newEntry);
    UpdateTrespasserEntry(Size(this.m_tresspasserList) - 1, true, areaName);
  }

  private final void RemoveTrespasserEntry(Int32 index) {
    Erase(this.m_tresspasserList, index);
  }

  private final Bool IsTrespasserOutside(Int32 index) {
    if(this.m_tresspasserList[index].isInsideA || this.m_tresspasserList[index].isInsideB || this.m_tresspasserList[index].isInsideScanner) {
      return false;
    };
    return true;
  }

  private final EntityNotificationType OnForceUnlock(ref<SecurityGateForceUnlock> evt) {
    if(!IsPowered()) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    if(evt.shouldUnlock) {
      this.m_entranceToken = evt.entranceAllowedFor;
      UnlockGate();
    } else {
      LockGate(false);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  private final void UnlockGate() {
    if(!this.m_isLocked) {
      return ;
    };
    this.m_isLocked = false;
    UpdateGatePosition();
  }

  private final void LockGate(Bool expireToken) {
    EntityID emptyID;
    if(expireToken) {
      this.m_entranceToken = emptyID;
    };
    if(this.m_isLocked) {
      return ;
    };
    this.m_isLocked = true;
    UpdateGatePosition();
  }

  private final void UpdateGatePosition() {
    ref<UpdateGatePosition> updateGatePosition;
    updateGatePosition = new UpdateGatePosition();
    QueueEntityEvent(ExtractEntityID(GetID()), updateGatePosition);
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceBackground";
  }
}
