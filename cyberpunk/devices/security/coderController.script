
public class CoderController extends ScriptableDC {

  protected const ref<CoderControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class CoderControllerPS extends BasicDistractionDeviceControllerPS {

  [Attrib(tooltip, "Whoever uses this device is granted provided security access level")]
  [Default(CoderControllerPS, ESecurityAccessLevel.ESL_4))]
  private ESecurityAccessLevel m_providedAuthorizationLevel;

  protected ref<AuthorizeUser> ActionAuthorizeUser() {
    ref<AuthorizeUser> action;
    action = ActionAuthorizeUser();
    action.CreateInteraction();
    return action;
  }

  public EntityNotificationType OnAuthorizeUser(ref<AuthorizeUser> evt) {
    ref<ActionNotifier> notifier;
    ref<SecuritySystemControllerPS> secSys;
    secSys = GetSecuritySystem();
    if(ToBool(secSys)) {
      secSys.AuthorizeUser(WeakRefToRef(evt.GetExecutor()).GetEntityID(), this.m_providedAuthorizationLevel);
      return EntityNotificationType.SendThisEventToEntity;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public Bool GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    ref<SecuritySystemControllerPS> secSys;
    secSys = GetSecuritySystem();
    if(!ToBool(secSys)) {
      return false;
    };
    if(!secSys.IsUserAuthorized(WeakRefToRef(context.processInitiatorObject).GetEntityID(), this.m_providedAuthorizationLevel)) {
      if(!secSys.IsEntityBlacklistedForAtLeast(WeakRefToRef(context.processInitiatorObject).GetEntityID(), BlacklistReason.COMBAT)) {
        Push(outActions, ActionAuthorizeUser());
      };
    };
    return true;
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceBackground";
  }
}
