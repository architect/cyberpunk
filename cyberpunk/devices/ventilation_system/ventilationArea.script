
public class VentilationArea extends InteractiveMasterDevice {

  protected ref<TriggerComponent> m_areaComponent;

  [Default(VentilationArea, false))]
  protected Bool m_RestartGameEffectOnAttach;

  [Default(VentilationArea, Attacks.FragGrenade))]
  protected String m_AttackRecord;

  private edit EffectRef m_gameEffectRef;

  private ref<EffectInstance> m_gameEffect;

  private Bool m_highLightActive;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "area", "TriggerComponent", false);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    OnTakeControl(ri);
    this.m_areaComponent = Cast(GetComponent(ri, "area"));
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected cb Bool OnGameAttached() {
    OnGameAttached();
    if(GetDevicePS().IsAreaActive() && this.m_RestartGameEffectOnAttach) {
      PlayGameEffect();
    };
  }

  protected cb Bool OnDetach() {
    OnDetach();
    StopGameEffect();
  }

  private const ref<VentilationAreaController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<VentilationAreaControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected cb Bool OnActivateDevice(ref<ActivateDevice> evt) {
    PlayGameEffect();
  }

  protected cb Bool OnRevealDeviceRequest(ref<RevealDeviceRequest> evt) {
    OnRevealDeviceRequest(evt);
    if(!GetDevicePS().ShouldRevealDevicesGrid()) {
      return true;
    };
    ToggleHighlightOnTargets(evt.shouldReveal);
  }

  private final void ToggleHighlightOnTargets(Bool toogle) {
    Int32 i;
    array<ref<Entity>> entities;
    ref<ForceVisionApperanceEvent> evt;
    ref<FocusForcedHighlightData> highlight;
    entities = GetEntitiesInArea();
    this.m_highLightActive = toogle;
    i = 0;
    while(i < Size(entities)) {
      if(Cast(entities[i]) != null) {
        ToggleHighlightOnSingleTarget(toogle, entities[i].GetEntityID());
      };
      i += 1;
    };
  }

  private final void ToggleHighlightOnSingleTarget(Bool toogle, EntityID id) {
    ref<ForceVisionApperanceEvent> evt;
    ref<FocusForcedHighlightData> highlight;
    highlight = CreateHighlight(EFocusForcedHighlightType.DISTRACTION);
    evt = new ForceVisionApperanceEvent();
    evt.apply = toogle;
    evt.forcedHighlight = highlight;
    QueueEventForEntityID(id, evt);
  }

  private final ref<FocusForcedHighlightData> CreateHighlight(EFocusForcedHighlightType highlightType) {
    ref<FocusForcedHighlightData> highlight;
    highlight = new FocusForcedHighlightData();
    highlight.sourceID = GetEntityID();
    highlight.sourceName = GetClassName();
    highlight.highlightType = highlightType;
    highlight.inTransitionTime = 0;
    highlight.outTransitionTime = 0;
    highlight.priority = EPriority.VeryHigh;
    return highlight;
  }

  private final void PlayGameEffect() {
    ref<Attack_GameEffect> explosionAttack;
    AttackInitContext attackContext;
    array<hitFlag> hitFlags;
    array<ref<gameStatModifierData>> statMods;
    if(!ToBool(this.m_gameEffect)) {
      this.m_gameEffect = GetGameEffectSystem(GetGame()).CreateEffect(this.m_gameEffectRef, GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject(), this);
      attackContext.record = GetAttackRecord(Create(this.m_AttackRecord));
      attackContext.instigator = RefToWeakRef(GetPlayer(GetGame()));
      attackContext.source = RefToWeakRef(this);
      explosionAttack = Cast(Create(attackContext));
      explosionAttack.GetStatModList(statMods);
      Push(hitFlags, hitFlag.FriendlyFire);
      SetVector(this.m_gameEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, this.GetWorldPosition());
      SetVariant(this.m_gameEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.flags, ToVariant(hitFlags));
      SetVariant(this.m_gameEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(explosionAttack));
      SetVariant(this.m_gameEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackStatModList, ToVariant(statMods));
    };
    switch(GetDevicePS().GetAreaEffect()) {
      case ETrapEffects.SmokeScreen:
        ApplyStatusEffect("BaseStatusEffect.SmokeScreen");
        break;
      case ETrapEffects.Explosion:
        this.m_gameEffect.Run();
        break;
      case ETrapEffects.SmokeScreen:
        ApplyStatusEffect("BaseStatusEffect.SmokeScreen");
        break;
      case ETrapEffects.Bleeding:
        ApplyStatusEffect("BaseStatusEffect.Bleeding");
        break;
      case ETrapEffects.Burning:
        ApplyStatusEffect("BaseStatusEffect.Burning");
        break;
      case ETrapEffects.Blind:
        ApplyStatusEffect("BaseStatusEffect.Blind");
        break;
      case ETrapEffects.Stun:
        ApplyStatusEffect("BaseStatusEffect.Stun");
    };
  }

  private final void StopGameEffect() {
    if(ToBool(this.m_gameEffect)) {
      this.m_gameEffect.Terminate();
      this.m_gameEffect = null;
    };
  }

  public final const array<ref<Entity>> GetEntitiesInArea() {
    return this.m_areaComponent.GetOverlappingEntities();
  }

  protected final void ApplyStatusEffect(String effectTDBID) {
    TweakDBID statusEffectID;
    Int32 i;
    array<ref<Entity>> entities;
    statusEffectID = Create(effectTDBID);
    entities = GetEntitiesInArea();
    i = 0;
    while(i < Size(entities)) {
      GetStatusEffectSystem(GetGame()).ApplyStatusEffect(entities[i].GetEntityID(), statusEffectID);
      i += 1;
    };
  }

  protected cb Bool OnAreaEnter(ref<AreaEnteredEvent> evt) {
    wref<NPCPuppet> activator;
    OnAreaEnter(evt);
    activator = RefToWeakRef(Cast(GetEntity(evt.activator)));
    if(WeakRefToRef(activator) != null && this.m_highLightActive) {
      ToggleHighlightOnSingleTarget(true, WeakRefToRef(activator).GetEntityID());
    };
  }

  protected cb Bool OnAreaExit(ref<AreaExitedEvent> evt) {
    wref<NPCPuppet> activator;
    OnAreaExit(evt);
    activator = RefToWeakRef(Cast(GetEntity(evt.activator)));
    if(WeakRefToRef(activator) != null && this.m_highLightActive) {
      ToggleHighlightOnSingleTarget(false, WeakRefToRef(activator).GetEntityID());
    };
  }

  public const ref<FocusForcedHighlightData> GetDefaultHighlight() {
    return null;
  }

  public ref<GameObject> GetStimTarget() {
    array<ref<DeviceComponentPS>> children;
    wref<Entity> entity;
    Int32 i;
    GetDevicePS().GetChildren(children);
    i = 0;
    while(i < Size(children)) {
      if(children[i].GetDeviceName() == "ActivatedDevice") {
        entity = children[i].GetOwnerEntityWeak();
        return Cast(WeakRefToRef(entity));
      };
      i += 1;
    };
    return this;
  }

  public ref<Entity> GetDistractionControllerSource(ref<AreaEffectData> effectData?) {
    array<ref<DeviceComponentPS>> ancestors;
    Int32 i;
    GetDevicePS().GetAncestors(ancestors);
    i = 0;
    while(i < Size(ancestors)) {
      if(ancestors[i].GetDeviceName() == "Activator") {
        return WeakRefToRef(ancestors[i].GetOwnerEntityWeak());
      };
      i += 1;
    };
    return this;
  }
}

public class EffectObjectProvider_VentilationAreaEntities extends EffectObjectProvider_Scripted {

  public final void Process(EffectScriptContext ctx, EffectProviderScriptContext providerCtx) {
    Int32 i;
    array<ref<Entity>> entities;
    entities = Cast(GetWeapon(ctx)).GetEntitiesInArea();
    i = 0;
    while(i < Size(entities)) {
      AddTarget(ctx, providerCtx, entities[i]);
      i += 1;
    };
  }
}
