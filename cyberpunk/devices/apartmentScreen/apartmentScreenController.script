
public class ApartmentScreenController extends LcdScreenController {

  protected const ref<ApartmentScreenControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class ApartmentScreenControllerPS extends LcdScreenControllerPS {

  [Attrib(category, "UI")]
  private ERentStatus m_initialRentStatus;

  [Attrib(category, "UI")]
  [Attrib(customEditor, "TweakDBGroupInheritance;ScreenMessageData")]
  private edit TweakDBID m_overdueMessageRecordID;

  [Attrib(category, "UI")]
  [Attrib(customEditor, "TweakDBGroupInheritance;ScreenMessageData")]
  private edit TweakDBID m_paidMessageRecordID;

  [Attrib(category, "UI")]
  [Attrib(customEditor, "TweakDBGroupInheritance;ScreenMessageData")]
  private edit TweakDBID m_evictionMessageRecordID;

  [Attrib(category, "UI")]
  [Default(ApartmentScreenControllerPS, EPaymentSchedule.WEEKLY))]
  private EPaymentSchedule m_paymentSchedule;

  [Attrib(category, "UI")]
  [Default(ApartmentScreenControllerPS, true))]
  private Bool m_randomizeInitialOverdue;

  [Attrib(rangeMax, "90")]
  [Attrib(category, "UI")]
  [Attrib(rangeMin, "0")]
  private Int32 m_initialOverdue;

  [Attrib(category, "UI")]
  [Default(ApartmentScreenControllerPS, true))]
  private Bool m_allowAutomaticRentStatusChange;

  [Default(ApartmentScreenControllerPS, 90))]
  private const Int32 m_maxDays;

  private persistent Int32 m_currentOverdue;

  private persistent Bool m_isInitialRentStateSet;

  private persistent ERentStatus m_currentRentStatus;

  private persistent Int32 m_lastStatusChangeDay;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
    if(!IsStringValid(this.m_deviceName)) {
      this.m_deviceName = "Gameplay-Devices-DisplayNames-Screen";
    };
  }

  protected void GameAttached() {
    UpdateRentState();
  }

  public void GetQuestActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    GetQuestActions(outActions, context);
  }

  public final void UpdateRentState() {
    if(!this.m_isInitialRentStateSet) {
      InitializeRentState();
    } else {
      if(this.m_allowAutomaticRentStatusChange) {
        ReEvaluateRentStatus();
      };
      if(this.m_currentRentStatus == ERentStatus.OVERDUE) {
        UpdateCurrentOverdue();
      };
    };
    RefreshUI(GetBlackboard());
  }

  private final void InitializeRentState() {
    this.m_currentOverdue = GetInitialOverdueValue();
    if(this.m_allowAutomaticRentStatusChange) {
      if(this.m_currentOverdue > 0) {
        SetCurrentRentStatus(ERentStatus.OVERDUE);
      } else {
        SetCurrentRentStatus(ERentStatus.PAID);
      };
    } else {
      SetCurrentRentStatus(this.m_initialRentStatus);
    };
    this.m_isInitialRentStateSet = true;
  }

  private final Int32 GetInitialOverdueValue() {
    Int32 returnValue;
    Int32 randValue;
    randValue = RandRange(0, 2);
    if(this.m_randomizeInitialOverdue && randValue == 1) {
      returnValue = RandRange(0, this.m_maxDays);
    } else {
      if(this.m_initialRentStatus == ERentStatus.OVERDUE) {
        this.m_initialOverdue = 1;
      };
      returnValue = this.m_initialOverdue;
    };
    return returnValue;
  }

  private final void SetCurrentRentStatus(ERentStatus status) {
    TweakDBID messageID;
    this.m_currentRentStatus = status;
    this.m_lastStatusChangeDay = GetCurrentDay();
    if(this.m_currentRentStatus == ERentStatus.PAID) {
      this.m_currentOverdue = 0;
      if(IsValid(this.m_paidMessageRecordID)) {
        messageID = this.m_paidMessageRecordID;
      } else {
        messageID = "screen_messages.RentPaid";
      };
    } else {
      if(this.m_currentRentStatus == ERentStatus.OVERDUE) {
        if(IsValid(this.m_overdueMessageRecordID)) {
          messageID = this.m_overdueMessageRecordID;
        } else {
          messageID = "screen_messages.RentOverdue";
        };
      } else {
        if(this.m_currentRentStatus == ERentStatus.EVICTED) {
          if(IsValid(this.m_evictionMessageRecordID)) {
            messageID = this.m_evictionMessageRecordID;
          } else {
            messageID = "screen_messages.EvictionNotice";
          };
        };
      };
    };
    SetMessageRecordID(messageID);
  }

  private final void UpdateCurrentOverdue() {
    this.m_currentOverdue += 1;
  }

  private final GameTime GetGameTime() {
    return GetTimeSystem(GetGameInstance()).GetGameTime();
  }

  private final Int32 GetCurrentDay() {
    return Days(GetGameTime());
  }

  private final Int32 GetDaysPassed() {
    return GetCurrentDay() - this.m_lastStatusChangeDay;
  }

  private final void ReEvaluateRentStatus() {
    Int32 randValue;
    Int32 stateChangeProbablity;
    if(this.m_currentRentStatus == ERentStatus.PAID && GetDaysPassed() < GetPaymentScheduleValue()) {
      return ;
    };
    stateChangeProbablity = GetStateChangeProbabilityValue();
    randValue = RandRange(0, 100);
    if(randValue > stateChangeProbablity) {
      return ;
    };
    if(this.m_currentRentStatus == ERentStatus.OVERDUE) {
      randValue = RandRange(0, 2);
      if(randValue == 0) {
        SetCurrentRentStatus(ERentStatus.PAID);
      } else {
        SetCurrentRentStatus(ERentStatus.EVICTED);
      };
    } else {
      if(this.m_currentRentStatus == ERentStatus.PAID) {
        SetCurrentRentStatus(ERentStatus.OVERDUE);
      } else {
        if(this.m_currentRentStatus == ERentStatus.EVICTED) {
          SetCurrentRentStatus(ERentStatus.PAID);
        };
      };
    };
  }

  private final Int32 GetStateChangeProbabilityValue() {
    Float daysPassed;
    Float returnValue;
    if(this.m_currentRentStatus == ERentStatus.OVERDUE) {
      daysPassed = Cast(this.m_currentOverdue);
    } else {
      daysPassed = Cast(GetCurrentDay()) - Cast(this.m_lastStatusChangeDay);
    };
    if(daysPassed > Cast(this.m_maxDays)) {
      returnValue = 100;
    } else {
      returnValue = daysPassed / Cast(this.m_maxDays) * 100;
    };
    return Cast(returnValue);
  }

  public final const ERentStatus GetCurrentRentStatus() {
    return this.m_currentRentStatus;
  }

  public final const Int32 GetCurrentOverdueValue() {
    return this.m_currentOverdue;
  }

  public final const Int32 GetPaymentScheduleValue() {
    Int32 returnValue;
    if(this.m_paymentSchedule == EPaymentSchedule.WEEKLY) {
      returnValue = 7;
    } else {
      if(this.m_paymentSchedule == EPaymentSchedule.MONTHLY) {
        returnValue = 30;
      };
    };
    return returnValue;
  }
}
