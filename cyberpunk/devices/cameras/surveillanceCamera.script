
public class SurveillanceCamera extends SensorDevice {

  private ref<VirtualCameraComponent> m_virtualCam;

  private ref<IComponent> m_cameraHead;

  private ref<IComponent> m_cameraHeadPhysics;

  private ref<IComponent> m_verticalDecal1;

  private ref<IComponent> m_verticalDecal2;

  private edit Bool m_meshDestrSupport;

  [Default(SurveillanceCamera, true))]
  private Bool m_shouldRotate;

  [Default(SurveillanceCamera, false))]
  private Bool m_canStreamVideo;

  [Default(SurveillanceCamera, true))]
  private Bool m_canDetectIntruders;

  private Float m_currentAngle;

  private Bool m_rotateLeft;

  private Vector4 m_targetPosition;

  private CName m_factOnFeedReceived;

  private CName m_questFactOnDetection;

  private ref<LookAtAddEvent> m_lookAtEvent;

  public Float m_currentYawModifier;

  public Float m_currentPitchModifier;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "virtualcamera", "VirtualCameraComponent", false);
    RequestComponent(ri, "updateComponent", "UpdateComponent", false);
    RequestComponent(ri, "detectionAreaIndicator", "MeshComponent", false);
    RequestComponent(ri, "main_red", "gameLightComponent", false);
    RequestComponent(ri, "right_point", "gameLightComponent", false);
    RequestComponent(ri, "middle_point", "gameLightComponent", false);
    RequestComponent(ri, "stripes_points", "gameLightComponent", false);
    if(this.m_meshDestrSupport) {
      RequestComponent(ri, "cameraHead", "IComponent", true);
      RequestComponent(ri, "cameraHeadPhysics", "IComponent", true);
      RequestComponent(ri, "vertical_decal_1", "IComponent", false);
      RequestComponent(ri, "vertical_decal_2", "IComponent", false);
    };
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    ref<InteractionSetEnableEvent> interactionEvent;
    this.m_virtualCam = Cast(GetComponent(ri, "virtualcamera"));
    Push(this.m_lightScanRefs, Cast(GetComponent(ri, "main_red")));
    Push(this.m_lightAttitudeRefs, Cast(GetComponent(ri, "stripes_points")));
    Push(this.m_lightInfoRefs, Cast(GetComponent(ri, "middle_point")));
    Push(this.m_lightInfoRefs, Cast(GetComponent(ri, "right_point")));
    if(this.m_meshDestrSupport) {
      this.m_cameraHead = GetComponent(ri, "cameraHead");
      this.m_verticalDecal1 = GetComponent(ri, "vertical_decal_1");
      this.m_verticalDecal2 = GetComponent(ri, "vertical_decal_2");
      this.m_cameraHeadPhysics = GetComponent(ri, "cameraHeadPhysics");
    };
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected cb Bool OnGameAttached() {
    OnGameAttached();
    SetSenseObjectType(gamedataSenseObjectType.Camera);
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    RegisterToGameSessionDataSystem(GetDevicePS().CanTagEnemies());
    SetForcedSensesTracing();
  }

  protected cb Bool OnDetach() {
    OnDetach();
    RegisterToGameSessionDataSystem(false);
  }

  private const ref<SurveillanceCameraController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<SurveillanceCameraControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  public const Bool IsSurveillanceCamera() {
    return true;
  }

  protected const String GetScannerName() {
    return "LocKey#100";
  }

  public Bool SetAsIntrestingTarget(wref<GameObject> target) {
    return SetAsIntrestingTarget(target);
  }

  public void OnValidTargetAppears(wref<GameObject> target) {
    ref<ScriptedPuppet> puppet;
    OnValidTargetAppears(target);
    GetDevicePS().ThreatDetected(true);
    RequestAlarm();
    puppet = Cast(WeakRefToRef(target));
    if(GetDevicePS().CanTagEnemies() && WeakRefToRef(target).IsActive()) {
      if(ToBool(puppet) && puppet.IsAggressive()) {
        TagObject(RefToWeakRef(puppet));
      };
    };
  }

  public void OnCurrentTargetAppears(wref<GameObject> target) {
    ref<ScriptedPuppet> puppet;
    puppet = Cast(WeakRefToRef(target));
    if(GetDevicePS().ShouldRevealEnemies()) {
      RequestRevealOutline(puppet, true, GetEntityID());
    };
    RequestAlarm();
    PlaySoundEvent(this, "q001_sc_00a_before_mission_tbug_hack");
    if(!GetDevicePS().GetSecuritySystem().IsSystemInCombat()) {
      SetWarningMessage("LocKey#53158");
    };
    OnCurrentTargetAppears(target);
  }

  private final void SetWarningMessage(String lockey) {
    SimpleScreenMessage simpleScreenMessage;
    simpleScreenMessage.isShown = true;
    simpleScreenMessage.duration = 5;
    simpleScreenMessage.message = lockey;
    simpleScreenMessage.isInstant = true;
    GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().UI_Notifications).SetVariant(GetAllBlackboardDefs().UI_Notifications.WarningMessage, ToVariant(simpleScreenMessage), true);
  }

  public void OnValidTargetDisappears(wref<GameObject> target) {
    ref<ScriptedPuppet> puppet;
    puppet = Cast(WeakRefToRef(target));
    RequestRevealOutline(puppet, false, GetEntityID());
    if(GetDevicePS().CanTagEnemies()) {
      UntagObject(RefToWeakRef(puppet));
    };
  }

  public void OnAllValidTargetsDisappears() {
    OnAllValidTargetsDisappears();
    GetDevicePS().ThreatDetected(false);
    RequestAlarm();
  }

  protected cb Bool OnEnterShapeEvent(ref<EnterShapeEvent> evt)

  protected cb Bool OnExitShapeEvent(ref<ExitShapeEvent> evt)

  protected void PushPersistentData() {
    GetDevicePS().PushPersistentData();
    PushPersistentData();
  }

  protected void RestoreDeviceState() {
    RestoreDeviceState();
  }

  protected void DeactivateDevice() {
    DeactivateDevice();
    TurnOffDevice();
  }

  protected void ActivateDevice() {
    ActivateDevice();
    TurnOnDevice();
  }

  protected void TurnOffDevice() {
    TurnOffDevice();
    TurnOffCamera();
  }

  protected void TurnOnDevice() {
    TurnOnDevice();
    TurnOnCamera();
  }

  protected void CutPower() {
    CutPower();
    TurnOffCamera();
  }

  protected cb Bool OnToggleStreamFeed(ref<ToggleStreamFeed> evt) {
    UpdateDeviceState();
    ToggleFeed(GetDevicePS().ShouldStream());
  }

  protected cb Bool OnToggleCamera(ref<ToggleON> evt) {
    UpdateDeviceState();
    GetDevicePS().IsON() ? TurnOnCamera() : TurnOffCamera();
  }

  private final void TurnOnCamera() {
    RequestAlarm();
  }

  private final void TurnOffCamera() {
    ToggleFeed(false);
    ToggleAreaIndicator(false);
  }

  protected cb Bool OnDeath(ref<gameDeathEvent> evt) {
    OnDeath(evt);
    if(this.m_meshDestrSupport) {
      this.m_cameraHead.Toggle(false);
      this.m_verticalDecal1.Toggle(false);
      this.m_verticalDecal2.Toggle(false);
      this.m_cameraHeadPhysics.Toggle(true);
    };
  }

  protected cb Bool OnSetDeviceAttitude(ref<SetDeviceAttitude> evt) {
    OnSetDeviceAttitude(evt);
    SetForcedSensesTracing();
    RegisterToGameSessionDataSystem(true);
  }

  private final void SetForcedSensesTracing() {
    if(GetDevicePS().CanTagEnemies()) {
      GetSensesComponent().SetForcedSensesTracing(gamedataSenseObjectType.Npc, EAIAttitude.AIA_Neutral);
      GetSensesComponent().SetForcedSensesTracing(gamedataSenseObjectType.Npc, EAIAttitude.AIA_Hostile);
      GetSensesComponent().SetTickDistanceOverride(100);
    } else {
      GetSensesComponent().RemoveForcedSensesTracing(gamedataSenseObjectType.Npc, EAIAttitude.AIA_Neutral);
      GetSensesComponent().RemoveForcedSensesTracing(gamedataSenseObjectType.Npc, EAIAttitude.AIA_Hostile);
      GetSensesComponent().SetTickDistanceOverride(-1);
    };
  }

  protected cb Bool OnCameraTagLockEvent(ref<CameraTagLockEvent> evt) {
    GetDevicePS().SetTagLockFromSystem(evt.isLocked);
    SetForcedSensesTracing();
  }

  private final void RegisterToGameSessionDataSystem(Bool add) {
    ref<CameraTagLimitData> cameraTagLimitData;
    cameraTagLimitData = new CameraTagLimitData();
    cameraTagLimitData.add = add;
    cameraTagLimitData.object = RefToWeakRef(this);
    AddDataEntryRequest(GetGame(), EGameSessionDataType.CameraTagLimit, ToVariant(cameraTagLimitData));
  }

  private final void ToggleFeed(Bool shouldBeOn) {
    ref<FeedEvent> feedEvt;
    array<EntityID> feedReceivers;
    Int32 i;
    feedEvt = new FeedEvent();
    feedEvt.On = shouldBeOn;
    this.m_virtualCam.Toggle(shouldBeOn);
    if(shouldBeOn) {
      SetFactValue(GetGame(), this.m_factOnFeedReceived, 1);
    } else {
      SetFactValue(GetGame(), this.m_factOnFeedReceived, 0);
      GetDevicePS().ClearFeedReceivers();
    };
  }

  private final void RequestAlarm() {
    if(GetDevicePS().IsDetecting()) {
      SetFactValue(GetGame(), GetDevicePS().GetQuestFactOnDetection(), 1);
    } else {
      SetFactValue(GetGame(), GetDevicePS().GetQuestFactOnDetection(), 0);
    };
  }

  protected void OverrideLookAtSetupHor(out ref<LookAtAddEvent> lookAtEntityEvent) {
    lookAtEntityEvent.request.limits.softLimitDegrees = GetDevicePS().GetBehaviourMaxRotationAngle() * 2;
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.Alarm;
  }

  protected cb Bool OnActionEngineering(ref<ActionEngineering> evt) {
    UpdateDeviceState();
  }

  protected cb Bool OnTCSTakeOverControlActivate(ref<TCSTakeOverControlActivate> evt) {
    OnTCSTakeOverControlActivate(evt);
    StartEffectEvent(GetPlayerSystem(GetGame()).GetLocalPlayerControlledGameObject(), "fish_eye");
  }

  protected cb Bool OnTCSTakeOverControlDeactivate(ref<TCSTakeOverControlDeactivate> evt) {
    OnTCSTakeOverControlDeactivate(evt);
    StopEffectEvent(GetPlayerSystem(GetGame()).GetLocalPlayerControlledGameObject(), "fish_eye");
  }
}
