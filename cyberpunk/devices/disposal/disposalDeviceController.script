
public class ActionDisposal extends ActionBool {

  public void CompleteAction(GameInstance gameInstance) {
    Int32 i;
    array<wref<RewardBase_Record>> rewards;
    array<wref<ObjectActionEffect_Record>> actionEffects;
    MountingInfo mountingInfo;
    EntityID npcID;
    GetPersistencySystem(gameInstance).QueuePSDeviceEvent(this);
    mountingInfo = GetMountingFacility(gameInstance).GetMountingInfoSingleWithObjects(WeakRefToRef(GetExecutor()));
    WeakRefToRef(GetObjectActionRecord()).Rewards(rewards);
    i = 0;
    while(i < Size(rewards)) {
      GiveReward(gameInstance, WeakRefToRef(rewards[i]).GetID(), Cast(mountingInfo.childId));
      i += 1;
    };
    WeakRefToRef(GetObjectActionRecord()).CompletionEffects(actionEffects);
    ProcessStatusEffects(actionEffects, gameInstance);
  }
}

public class DisposeBody extends ActionDisposal {

  public final void SetProperties() {
    this.actionName = "Dispose";
    this.prop = SetUpProperty_Bool("Dispose", true, "Dispose", "Dispose");
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    return true;
  }
}

public class TakedownAndDisposeBody extends ActionDisposal {

  public final void SetProperties() {
    this.actionName = "TakedownAndDispose";
    this.prop = SetUpProperty_Bool("TakedownAndDispose", true, "TakedownAndDispose", "TakedownAndDispose");
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    return true;
  }
}

public class NonlethalTakedownAndDisposeBody extends ActionDisposal {

  public final void SetProperties() {
    this.actionName = "NonlethalTakedownAndDispose";
    this.prop = SetUpProperty_Bool("NonlethalTakedownAndDispose", true, "NonlethalTakedownAndDispose", "NonlethalTakedownAndDispose");
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    return true;
  }
}

public class SpiderbotDistractionPerformed extends ActionBool {

  public final void SetProperties(CName action_name) {
    this.actionName = action_name;
    this.prop = SetUpProperty_Bool(action_name, true, action_name, action_name);
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    return true;
  }
}

public class OverchargeDevice extends ActionBool {

  public final void SetProperties(CName action_name) {
    this.actionName = action_name;
    this.prop = SetUpProperty_Bool(action_name, true, action_name, action_name);
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    return true;
  }

  public String GetTweakDBChoiceRecord() {
    if(IsValid(this.m_objectActionID)) {
      return GetTweakDBChoiceRecord();
    };
    return "Overcharge";
  }
}

public class DisposalDeviceController extends ScriptableDC {

  protected const ref<DisposalDeviceControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class DisposalDeviceControllerPS extends ScriptableDeviceComponentPS {

  private persistent DisposalDeviceSetup m_DisposalDeviceSetup;

  private persistent DistractionSetup m_distractionSetup;

  private persistent DistractionSetup m_explosionSetup;

  private persistent Bool m_isDistractionDisabled;

  private persistent Bool m_wasActivated;

  private persistent Bool m_wasLethalTakedownPerformed;

  private Bool m_isPlayerCurrentlyPerformingDisposal;

  public final TweakDBID GetInteractionName() {
    return this.m_distractionSetup.m_alternativeInteractionName;
  }

  public final Bool WasActivated() {
    return this.m_wasActivated;
  }

  public final void SetIsPlayerCurrentlyPerformingDisposal(Bool value) {
    this.m_isPlayerCurrentlyPerformingDisposal = value;
  }

  public final const Bool WasLethalTakedownPerformed() {
    return this.m_wasLethalTakedownPerformed;
  }

  public final void SetWasLethalTakedownPerformed(Bool value) {
    this.m_wasLethalTakedownPerformed = value;
  }

  public final TweakDBID GetQuickHackName() {
    return this.m_distractionSetup.m_alternativeQuickHackName;
  }

  public final TweakDBID GetActionName() {
    return this.m_DisposalDeviceSetup.m_actionName;
  }

  public final TweakDBID GetTakedownActionName() {
    return this.m_DisposalDeviceSetup.m_takedownActionName;
  }

  public final TweakDBID GetNonlethalTakedownActionName() {
    return this.m_DisposalDeviceSetup.m_nonlethalTakedownActionName;
  }

  public final Float GetStimuliRange() {
    return this.m_distractionSetup.m_StimuliRange;
  }

  public final const Bool HasQuickHackDistraction() {
    return this.m_distractionSetup.m_hasQuickHack;
  }

  public final Bool HasSpiderbotInteraction() {
    return this.m_distractionSetup.m_hasSpiderbotInteraction;
  }

  public final Bool HasSpiderbotExplosionInteraction() {
    return this.m_explosionSetup.m_hasSpiderbotInteraction;
  }

  public final Bool HasComputerInteraction() {
    return this.m_explosionSetup.m_hasComputerInteraction;
  }

  public final array<ExplosiveDeviceResourceDefinition> GetExplosionDeinitionArray() {
    return this.m_explosionSetup.explosionDefinition;
  }

  public final Int32 GetNumberOfUses() {
    return this.m_DisposalDeviceSetup.m_numberOfUses;
  }

  protected void GameAttached() {
    GameAttached();
    InitializeSkillChecks(this.m_distractionSetup.m_skillChecks);
  }

  private final ref<DisposeBody> ActionDisposeBody(TweakDBID interactionTweak) {
    ref<DisposeBody> action;
    action = new DisposeBody();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction(interactionTweak);
    return action;
  }

  private final ref<TakedownAndDisposeBody> ActionTakedownAndDisposeBody(TweakDBID interactionTweak) {
    ref<TakedownAndDisposeBody> action;
    action = new TakedownAndDisposeBody();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction(interactionTweak);
    return action;
  }

  private final ref<NonlethalTakedownAndDisposeBody> ActionNonlethalTakedownAndDisposeBody(TweakDBID interactionTweak) {
    ref<NonlethalTakedownAndDisposeBody> action;
    action = new NonlethalTakedownAndDisposeBody();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction(interactionTweak);
    return action;
  }

  protected final ref<QuickHackDistraction> ActionQuickHackDistraction(TweakDBID interactionTweak) {
    ref<QuickHackDistraction> action;
    action = new QuickHackDistraction();
    action = ActionQuickHackDistraction();
    action.CreateInteraction(interactionTweak);
    action.CreateInteraction();
    action.SetDurationValue(GetDistractionDuration(action));
    return action;
  }

  protected final ref<SpiderbotDistraction> ActionSpiderbotDistraction(String interactionName) {
    ref<SpiderbotDistraction> action;
    action = new SpiderbotDistraction();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction(interactionName);
    return action;
  }

  protected final ref<SpiderbotExplodeExplosiveDevice> ActionSpiderbotExplosion(String interactionName) {
    ref<SpiderbotExplodeExplosiveDevice> action;
    action = new SpiderbotExplodeExplosiveDevice();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction(interactionName);
    return action;
  }

  protected final ref<SpiderbotExplodeExplosiveDevicePerformed> ActionSpiderbotExplodeExplosiveDevicePerformed() {
    ref<SpiderbotExplodeExplosiveDevicePerformed> action;
    action = new SpiderbotExplodeExplosiveDevicePerformed();
    action.clearanceLevel = GetSpiderbotClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  protected final ref<SpiderbotDistractionPerformed> ActionSpiderbotDistractionPerformed() {
    ref<SpiderbotDistractionPerformed> action;
    action = new SpiderbotDistractionPerformed();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties("Distract");
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  protected final ref<OverchargeDevice> ActionOverchargeDevice() {
    ref<OverchargeDevice> action;
    action = new OverchargeDevice();
    action.clearanceLevel = GetInteractiveClearance();
    action.SetUp(this);
    action.SetProperties("Overcharge");
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    return action;
  }

  protected final ref<ToggleActivation> ActionToggleActivation(TweakDBID interactionTweak) {
    ref<ToggleActivation> action;
    action = new ToggleActivation();
    action.SetUp(this);
    action.SetProperties(this.m_deviceState);
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction(interactionTweak);
    return action;
  }

  private final const ref<IBlackboard> GetPlayerSMBlackboard() {
    ref<IBlackboard> psmBlackboard;
    ref<PlayerPuppet> playerPuppet;
    playerPuppet = Cast(GetPlayerSystem(GetGameInstance()).GetLocalPlayerControlledGameObject());
    psmBlackboard = GetBlackboardSystem(GetGameInstance()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return psmBlackboard;
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> action;
    if(IsDisabled() || this.m_isPlayerCurrentlyPerformingDisposal) {
      return false;
    };
    if(HasComputerInteraction()) {
      Push(actions, ActionOverchargeDevice());
    };
    if(this.m_distractionSetup.m_hasSimpleInteraction && !this.m_wasActivated) {
      Push(actions, ActionToggleActivation(GetInteractionName()));
    };
    if(!IsPlayerCarrying() || IsEnemyGrappled()) {
      GetActions(actions, context);
    };
    if(IsNPCDisposalBlockedStatusEffect()) {
      return false;
    };
    if(IsPlayerDroppingBody()) {
      return false;
    };
    if(HasRestriction(context.processInitiatorObject, "NoWorldInteractions")) {
      return false;
    };
    if(IsEnemyGrappled()) {
      action = ActionTakedownAndDisposeBody(GetTakedownActionName());
      action.SetInactiveWithReason(GetNumberOfUses() > 0, "LocKey#2115");
      Push(actions, action);
      action = ActionNonlethalTakedownAndDisposeBody(GetNonlethalTakedownActionName());
      action.SetInactiveWithReason(GetNumberOfUses() > 0, "LocKey#2115");
      Push(actions, action);
    };
    if(IsPlayerCarrying()) {
      action = ActionDisposeBody(GetActionName());
      action.SetInactiveWithReason(GetNumberOfUses() > 0, "LocKey#2115");
      Push(actions, action);
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  protected final Bool IsNPCDisposalBlockedStatusEffect() {
    MountingInfo mountingInfo;
    wref<NPCPuppet> npc;
    ref<PlayerPuppet> playerPuppet;
    playerPuppet = Cast(GetPlayerSystem(GetGameInstance()).GetLocalPlayerControlledGameObject());
    mountingInfo = GetMountingFacility(GetGameInstance()).GetMountingInfoSingleWithObjects(playerPuppet);
    npc = RefToWeakRef(Cast(FindEntityByID(GetGameInstance(), mountingInfo.childId)));
    return HasStatusEffect(npc, "BaseStatusEffect.BlockBodyDisposal");
  }

  public final const Bool IsPlayerCarrying() {
    return GetPlayerSMBlackboard().GetBool(GetAllBlackboardDefs().PlayerStateMachine.Carrying);
  }

  public final const Bool IsEnemyGrappled() {
    return GetPlayerSMBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == ToInt(gamePSMTakedown.Grapple);
  }

  protected final Bool IsPlayerDroppingBody() {
    return GetPlayerSMBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.BodyCarrying) == ToInt(gamePSMBodyCarrying.Drop);
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    if(IsDisabled() || IsDistracting()) {
      return false;
    };
    if(HasQuickHackDistraction()) {
      return true;
    };
    return false;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> currentAction;
    if(IsDisabled() || IsDistracting()) {
      return ;
    };
    if(HasQuickHackDistraction()) {
      currentAction = ActionQuickHackDistraction();
      currentAction.SetObjectActionID("DeviceAction.MalfunctionClassHack");
      currentAction.SetDurationValue(GetDistractionDuration(currentAction));
      Push(actions, currentAction);
    };
    FinalizeGetQuickHackActions(actions, context);
  }

  protected Bool CanCreateAnySpiderbotActions() {
    return false;
  }

  protected void GetSpiderbotActions(out array<ref<DeviceAction>> actions, GetActionsContext context)

  public final EntityNotificationType OnDistraction(ref<Distraction> evt) {
    if(this.m_distractionSetup.m_disableOnActivation) {
      this.m_isDistractionDisabled = true;
    };
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public EntityNotificationType OnToggleActivation(ref<ToggleActivation> evt) {
    UseNotifier(evt);
    this.m_wasActivated = true;
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnSpiderbotDistraction(ref<SpiderbotDistraction> evt) {
    SendSpiderbotToPerformAction(ActionSpiderbotDistractionPerformed(), evt.GetExecutor());
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnSpiderbotExplosion(ref<SpiderbotExplodeExplosiveDevice> evt) {
    SendSpiderbotToPerformAction(ActionSpiderbotExplodeExplosiveDevicePerformed(), evt.GetExecutor());
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnSpiderbotExplosionPerformed(ref<SpiderbotExplodeExplosiveDevicePerformed> evt) {
    if(this.m_explosionSetup.m_disableOnActivation) {
      DisableDevice();
    };
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnSpiderbotDistractionPerformed(ref<SpiderbotDistractionPerformed> evt) {
    if(this.m_distractionSetup.m_disableOnActivation) {
      this.m_isDistractionDisabled = true;
    };
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnDisposeBody(ref<DisposeBody> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    this.m_DisposalDeviceSetup.m_numberOfUses = this.m_DisposalDeviceSetup.m_numberOfUses - 1;
    this.m_isPlayerCurrentlyPerformingDisposal = true;
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnTakedownAndDisposeBody(ref<TakedownAndDisposeBody> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    this.m_DisposalDeviceSetup.m_numberOfUses = this.m_DisposalDeviceSetup.m_numberOfUses - 1;
    this.m_isPlayerCurrentlyPerformingDisposal = true;
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnNonlethalTakedownAndDisposeBody(ref<NonlethalTakedownAndDisposeBody> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    this.m_DisposalDeviceSetup.m_numberOfUses = this.m_DisposalDeviceSetup.m_numberOfUses - 1;
    this.m_isPlayerCurrentlyPerformingDisposal = true;
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnOverchargeDevice(ref<OverchargeDevice> evt) {
    UseNotifier(evt);
    DisableDevice();
    return EntityNotificationType.SendThisEventToEntity;
  }
}
