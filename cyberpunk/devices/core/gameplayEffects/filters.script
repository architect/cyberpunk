
public class IsAccessPointFilter extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    if(ToBool(Cast(entity))) {
      return true;
    };
    return false;
  }
}

public class IsDeviceFilter extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    return ToBool(Cast(entity));
  }
}

public class IsPlayerFilter extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    return ToBool(Cast(entity));
  }
}

public class IsCoverDevice extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    return ToBool(Cast(entity)) || ToBool(Cast(entity));
  }
}

public class IsNotWeakspotFilter extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    if(ToBool(Cast(entity))) {
      return false;
    };
    return true;
  }
}

public class IsNotInstigatorWeakspotFilter extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    ref<Entity> instigator;
    ref<WeakspotObject> weakSpotObject;
    ref<Entity> weakSpotObjectOwner;
    instigator = GetInstigator(ctx);
    entity = GetEntity(filterCtx);
    weakSpotObject = Cast(entity);
    if(ToBool(weakSpotObject)) {
      weakSpotObjectOwner = WeakRefToRef(weakSpotObject.GetOwner());
    };
    if(ToBool(Cast(entity)) && weakSpotObjectOwner == instigator) {
      return false;
    };
    return true;
  }
}

public class EffectFilter_DamageOverTime extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    Float currentTime;
    Float lastTimeApplied;
    Float cycleDuration;
    currentTime = ToFloat(GetSimTime(GetGameInstance(ctx)));
    GetFloat(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.dotLastApplicationTime, lastTimeApplied);
    if(lastTimeApplied == 0) {
      SetFloat(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.dotLastApplicationTime, currentTime);
      return true;
    };
    GetFloat(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.dotCycleDuration, cycleDuration);
    if(currentTime - lastTimeApplied >= cycleDuration) {
      SetFloat(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.dotLastApplicationTime, currentTime);
      return true;
    };
    return false;
  }
}

public class OnlySingleStatusEffectFromInstigator extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<IAttack> attack;
    Variant variant;
    array<wref<StatusEffectAttackData_Record>> effects;
    Int32 i;
    ref<NPCPuppet> puppet;
    GetVariant(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.attack, variant);
    attack = FromVariant(variant);
    puppet = Cast(GetEntity(filterCtx));
    WeakRefToRef(attack.GetRecord()).StatusEffects(effects);
    if(ToBool(puppet)) {
      i = 0;
      while(i < Size(effects)) {
        if(HasStatusEffectFromInstigator(RefToWeakRef(puppet), WeakRefToRef(WeakRefToRef(effects[i]).StatusEffect()).GetID(), GetInstigator(ctx).GetEntityID())) {
          return false;
        };
        i += 1;
      };
    };
    return true;
  }
}

public class NotInDefeated extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<NPCPuppet> puppet;
    puppet = Cast(GetEntity(filterCtx));
    if(ToBool(puppet)) {
      return !HasStatusEffect(RefToWeakRef(puppet), "BaseStatusEffect.Defeated");
    };
    return true;
  }
}

public class IgnoreFriendlyTargets extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    wref<GameObject> target;
    ref<WeakspotObject> targetAsWeakspot;
    target = RefToWeakRef(Cast(GetEntity(filterCtx)));
    targetAsWeakspot = Cast(WeakRefToRef(target));
    if(ToBool(targetAsWeakspot)) {
      target = targetAsWeakspot.GetOwner();
    };
    if(GetAttitudeBetween(WeakRefToRef(target), Cast(GetInstigator(ctx))) == EAIAttitude.AIA_Friendly) {
      return false;
    };
    return true;
  }
}

public class PlayerIgnoreFriendlyAndAlive extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    Bool isFriendlyNPC;
    isFriendlyNPC = false;
    isFriendlyNPC = IsTargetFriendlyNPC(Cast(GetInstigator(ctx)), GetEntity(filterCtx));
    return !isFriendlyNPC;
  }
}

public class IgnorePlayerMountedVehicle extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    ref<VehicleObject> vehicle;
    Bool damage;
    entity = GetEntity(filterCtx);
    vehicle = Cast(entity);
    if(ToBool(vehicle)) {
      damage = vehicle.IsPlayerMounted();
      return !vehicle.IsPlayerMounted();
    };
    return true;
  }
}

public class IgnorePlayerIfMountedToVehicle extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    if(ToBool(Cast(entity))) {
      return !IsMountedToVehicle(GetGameInstance(ctx), entity.GetEntityID());
    };
    return true;
  }
}

public class IgnoreAlreadyAffectedEntities extends EffectObjectSingleFilter_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    Variant tempVariant;
    array<EntityID> affectedEntities;
    entity = GetEntity(filterCtx);
    GetVariant(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.targets, tempVariant);
    affectedEntities = FromVariant(tempVariant);
    if(ToBool(entity) && !Contains(affectedEntities, entity.GetEntityID())) {
      Push(affectedEntities, entity.GetEntityID());
      SetVariant(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.targets, ToVariant(affectedEntities));
      return true;
    };
    return false;
  }
}

public class IsLootContainer extends EffectObjectSingleFilter_Scripted {

  public edit Bool m_invert;

  public final Bool Process(EffectScriptContext ctx, EffectSingleFilterScriptContext filterCtx) {
    ref<Entity> entity;
    entity = GetEntity(filterCtx);
    if(ToBool(Cast(entity))) {
      return this.m_invert ? false : true;
    };
    return this.m_invert ? true : false;
  }
}
