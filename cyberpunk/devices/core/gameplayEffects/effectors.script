
public class RemotelyConnectToAccessPoint extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<Entity> entity;
    ref<DebugRemoteConnectionEvent> debugRemoteConnectionEvent;
    entity = GetTarget(applierCtx);
    debugRemoteConnectionEvent = new DebugRemoteConnectionEvent();
    entity.QueueEvent(debugRemoteConnectionEvent);
    return true;
  }
}

public class EffectExecutor_PuppetForceVisionAppearance extends EffectExecutor_Scripted {

  private final ref<PuppetForceVisionAppearanceData> GetForceVisionAppearanceData(EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> data;
    Variant dataVariant;
    GetVariant(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.forceVisionAppearanceData, dataVariant);
    data = FromVariant(dataVariant);
    if(data == null) {
      data = new PuppetForceVisionAppearanceData();
      data.m_isInvalid = true;
    };
    return data;
  }

  private final void SetForceVisionAppearanceData(EffectScriptContext ctx, ref<PuppetForceVisionAppearanceData> data) {
    SetVariant(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.forceVisionAppearanceData, ToVariant(data));
  }

  private final EFocusForcedHighlightType GetHighlightType(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_highlightType;
  }

  private final EFocusOutlineType GetOutlineType(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_outlineType;
  }

  private final wref<Stim_Record> GetStimRecord(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_stimRecord;
  }

  private final Float GetTransitionTime(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_transitionTime;
  }

  private final EPriority GetPriority(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_priority;
  }

  private final array<wref<ScriptedPuppet>> GetHighlightedTargets(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_highlightedTargets;
  }

  private final array<wref<ScriptedPuppet>> GetTargets(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_targets;
  }

  private final Int32 GetInvestigationSlots(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_investigationSlots;
  }

  private final Bool IsSourceHighlighted(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_sourceHighlighted;
  }

  private final String GetEffectName(EffectScriptContext ctx) {
    return GetForceVisionAppearanceData(ctx).m_effectName;
  }

  public final Bool Init(EffectScriptContext ctx) {
    ref<GameObject> source;
    gamedataStimType stimType;
    Int32 stimTypeValue;
    TweakDBID stimID;
    ref<CommunicationEvent> communicationEvent;
    ref<PuppetForceVisionAppearanceData> bbData;
    source = Cast(GetSource(ctx));
    GetInt(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.stimType, stimTypeValue);
    stimType = ToEnum(stimTypeValue);
    stimID = Create("stims." + EnumValueToString("gamedataStimType", Cast(ToInt(stimType))) + "Stimuli");
    bbData = GetForceVisionAppearanceData(ctx);
    if(IsValid(stimID)) {
      bbData.m_stimRecord = RefToWeakRef(GetStimRecord(stimID));
    };
    if(ToBool(source)) {
      communicationEvent = new CommunicationEvent();
      communicationEvent.name = "ResetInvestigators";
      bbData.m_investigationSlots = source.GetTotalCountOfInvestigationSlots();
      SetForceVisionAppearanceData(ctx, bbData);
      source.QueueEvent(communicationEvent);
    };
    return true;
  }

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<GameObject> source;
    source = Cast(GetSource(ctx));
    if(GetHighlightType(ctx) == EFocusForcedHighlightType.DISTRACTION || GetOutlineType(ctx) == EFocusOutlineType.DISTRACTION) {
      if(source != null) {
        if(IsSourceValid(source)) {
          EvaluateTargets(RefToWeakRef(source), ctx);
          UpdateDistractionHighlights(source, GetNearestTargets(source, GetInvestigationSlots(ctx), ctx), ctx);
        } else {
          ClearAllHighlights(source, ctx);
        };
      };
    };
    UpdateSourceHighlight(RefToWeakRef(source), ctx);
    return true;
  }

  private final void ClearAllHighlights(ref<GameObject> source, EffectScriptContext ctx) {
    Int32 i;
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    i = 0;
    while(i < Size(bbData.m_highlightedTargets)) {
      if(WeakRefToRef(bbData.m_highlightedTargets[i]) == null) {
      } else {
        SendForceVisionApperaceEvent(false, bbData.m_highlightedTargets[i], RefToWeakRef(source), ctx);
      };
      i += 1;
    };
  }

  public final void TargetAcquired(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<ScriptedPuppet> target;
    ref<GameObject> source;
    target = Cast(GetTarget(applierCtx));
    source = Cast(GetSource(ctx));
    if(source == null) {
      return ;
    };
    if(ToBool(target) && IsTargetValid(target)) {
      AddTarget(target, ctx);
      if(GetHighlightType(ctx) == EFocusForcedHighlightType.DISTRACTION || GetOutlineType(ctx) == EFocusOutlineType.DISTRACTION) {
        return ;
      };
      SendForceVisionApperaceEvent(true, RefToWeakRef(target), RefToWeakRef(source), ctx);
    };
  }

  public final void TargetLost(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<ScriptedPuppet> target;
    ref<ForceVisionApperanceEvent> evt;
    ref<FocusForcedHighlightData> highlight;
    ref<GameObject> source;
    target = Cast(GetTarget(applierCtx));
    source = Cast(GetSource(ctx));
    if(source == null) {
      return ;
    };
    if(ToBool(target)) {
      RemoveTarget(target, ctx);
      SendForceVisionApperaceEvent(false, RefToWeakRef(target), RefToWeakRef(source), ctx);
    };
  }

  private final Bool IsSourceValid(ref<GameObject> source) {
    ref<Device> device;
    device = Cast(source);
    if(device != null) {
      return device.GetCurrentGameplayRole() != EGameplayRole. && device.IsActive();
    };
    return true;
  }

  private final Bool IsTargetValid(ref<ScriptedPuppet> target) {
    gamedataReactionPresetType reactionPreset;
    wref<Character_Record> characterRecord;
    if(!IsActive(target) || !target.IsOnAutonomousAI()) {
      return false;
    };
    characterRecord = RefToWeakRef(GetCharacterRecord(target.GetRecordID()));
    if(!ToBool(characterRecord)) {
      return false;
    };
    reactionPreset = WeakRefToRef(WeakRefToRef(characterRecord).ReactionPreset()).Type();
    if(reactionPreset == gamedataReactionPresetType.Corpo_Aggressive || reactionPreset == gamedataReactionPresetType.Corpo_Passive || reactionPreset == gamedataReactionPresetType.Ganger_Aggressive || reactionPreset == gamedataReactionPresetType.Ganger_Passive || reactionPreset == gamedataReactionPresetType.Police_Aggressive || reactionPreset == gamedataReactionPresetType.Police_Passive || reactionPreset == gamedataReactionPresetType.Mechanical_Aggressive || reactionPreset == gamedataReactionPresetType.Mechanical_Passive || reactionPreset == gamedataReactionPresetType.Mechanical_NonCombat) {
      return true;
    };
    return false;
  }

  private final void AddTarget(ref<ScriptedPuppet> target, EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    if(target == null || HasAnyTargets(ctx) && Contains(bbData.m_targets, RefToWeakRef(target))) {
      return ;
    };
    Push(bbData.m_targets, RefToWeakRef(target));
    SetForceVisionAppearanceData(ctx, bbData);
  }

  private final void RemoveTarget(ref<ScriptedPuppet> target, EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    if(ToBool(target) && HasAnyTargets(ctx)) {
      Remove(bbData.m_targets, RefToWeakRef(target));
      SetForceVisionAppearanceData(ctx, bbData);
    };
  }

  private final void AddHighlightTarget(ref<ScriptedPuppet> target, EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    if(!Contains(bbData.m_highlightedTargets, RefToWeakRef(target))) {
      if(target.GetHudManager().IsQuickHackPanelOpened()) {
      };
      Push(bbData.m_highlightedTargets, RefToWeakRef(target));
      SetForceVisionAppearanceData(ctx, bbData);
    };
  }

  private final void RemoveHighlightTarget(ref<ScriptedPuppet> target, EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    if(ToBool(target) && HasHighlightedTargets(ctx)) {
      if(target.GetHudManager().IsQuickHackPanelOpened()) {
      };
      Remove(bbData.m_highlightedTargets, RefToWeakRef(target));
      SetForceVisionAppearanceData(ctx, bbData);
    };
  }

  private final Bool HasHighlightedTargets(EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    return Size(bbData.m_highlightedTargets) > 0;
  }

  private final Bool HasAnyTargets(EffectScriptContext ctx) {
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    return Size(bbData.m_targets) > 0;
  }

  private final void EvaluateTargets(wref<GameObject> source, EffectScriptContext ctx) {
    Int32 i;
    Bool shouldUpdate;
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    i = Size(bbData.m_targets) - 1;
    while(i >= 0) {
      if(WeakRefToRef(bbData.m_targets[i]) == null) {
        Erase(bbData.m_targets, i);
        shouldUpdate = true;
      } else {
        if(!IsActive(WeakRefToRef(bbData.m_targets[i])) || !CanReactOnStimType(WeakRefToRef(bbData.m_targets[i]), WeakRefToRef(GetStimRecord(ctx))) || !WeakRefToRef(bbData.m_targets[i]).IsOnAutonomousAI()) {
          SendForceVisionApperaceEvent(false, bbData.m_targets[i], source, ctx);
          Erase(bbData.m_targets, i);
          shouldUpdate = true;
        };
      };
      i -= 1;
    };
    if(shouldUpdate) {
      SetForceVisionAppearanceData(ctx, bbData);
    };
  }

  private final void UpdateSourceHighlight(wref<GameObject> source, EffectScriptContext ctx) {
    if(HasHighlightedTargets(ctx)) {
      if(!IsSourceHighlighted(ctx) || !WeakRefToRef(source).HasHighlight(GetHighlightType(ctx), GetOutlineType(ctx), WeakRefToRef(source).GetEntityID(), StringToName(GetEffectName(ctx))) && IsSourceValid(WeakRefToRef(source))) {
        SendForceVisionApperaceEvent(true, source, source, ctx);
      };
    } else {
      if(IsSourceHighlighted(ctx)) {
        SendForceVisionApperaceEvent(false, source, source, ctx);
      };
    };
  }

  private final void UpdateDistractionHighlights(ref<GameObject> source, array<wref<ScriptedPuppet>> targetsToHighlight, EffectScriptContext ctx) {
    Int32 i;
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    i = 0;
    while(i < Size(bbData.m_targets)) {
      if(Contains(targetsToHighlight, bbData.m_targets[i])) {
        if(!Contains(bbData.m_highlightedTargets, bbData.m_targets[i]) || !WeakRefToRef(bbData.m_targets[i]).HasHighlight(GetHighlightType(ctx), GetOutlineType(ctx), source.GetEntityID(), StringToName(GetEffectName(ctx)))) {
          SendForceVisionApperaceEvent(true, bbData.m_targets[i], RefToWeakRef(source), ctx);
        };
      } else {
        if(Contains(bbData.m_highlightedTargets, bbData.m_targets[i])) {
          SendForceVisionApperaceEvent(false, bbData.m_targets[i], RefToWeakRef(source), ctx);
        };
      };
      i += 1;
    };
  }

  private final Bool CanReactOnStimType(ref<ScriptedPuppet> target, ref<Stim_Record> stimRecord) {
    gamedataStimPropagation propagationType;
    Bool returnValue;
    returnValue = true;
    if(stimRecord != null) {
      propagationType = WeakRefToRef(stimRecord.Propagation()).Type();
      if(propagationType == gamedataStimPropagation.Audio && IsDeaf(RefToWeakRef(target))) {
        returnValue = false;
      };
    };
    return returnValue;
  }

  private final wref<ScriptedPuppet> GetNearestTarget(ref<GameObject> source, EffectScriptContext ctx) {
    Int32 i;
    Int32 j;
    ref<ScriptedPuppet> target;
    Float lastDistance;
    Float currentDistance;
    Vector4 targetPos;
    Vector4 sourcePos;
    ref<NavigationPath> path;
    array<Vector4> posSources;
    Float navDistance;
    Float closestNavDist;
    ref<PuppetForceVisionAppearanceData> bbData;
    posSources = Cast(source).GetNodePosition();
    bbData = GetForceVisionAppearanceData(ctx);
    i = 0;
    while(i < Size(bbData.m_targets)) {
      targetPos = WeakRefToRef(bbData.m_targets[i]).GetWorldPosition();
      j = 0;
      while(j < Size(posSources)) {
        navDistance = DistanceSquared(posSources[j], targetPos);
        if(navDistance < closestNavDist || closestNavDist == 0) {
          closestNavDist = navDistance;
          sourcePos = posSources[j];
        };
        j += 1;
      };
      path = GetNavigationSystem(source.GetGame()).CalculatePath(targetPos, sourcePos, NavGenAgentSize.Human, 0);
      currentDistance = path.CalculateLength();
      if(lastDistance == 0 || lastDistance > currentDistance && currentDistance != 0) {
        target = WeakRefToRef(bbData.m_targets[i]);
        lastDistance = currentDistance;
      };
      i += 1;
    };
    return RefToWeakRef(target);
  }

  private final wref<ScriptedPuppet> GetNearestTarget(ref<GameObject> source, array<wref<ScriptedPuppet>> targets, EffectScriptContext ctx) {
    Int32 i;
    Int32 j;
    ref<ScriptedPuppet> target;
    Float lastDistance;
    Float currentDistance;
    Vector4 targetPos;
    Vector4 sourcePos;
    ref<NavigationPath> path;
    array<Vector4> posSources;
    Float navDistance;
    Float closestNavDist;
    ref<PuppetForceVisionAppearanceData> bbData;
    posSources = Cast(source).GetNodePosition();
    bbData = GetForceVisionAppearanceData(ctx);
    i = 0;
    while(i < Size(bbData.m_targets)) {
      targetPos = WeakRefToRef(bbData.m_targets[i]).GetWorldPosition();
      j = 0;
      while(j < Size(posSources)) {
        navDistance = DistanceSquared(posSources[j], targetPos);
        path = GetNavigationSystem(source.GetGame()).CalculatePath(targetPos, posSources[j], NavGenAgentSize.Human, 0);
        if(!ToBool(path)) {
        } else {
          if(navDistance < closestNavDist || closestNavDist == 0) {
            closestNavDist = navDistance;
            sourcePos = posSources[j];
          };
        };
        j += 1;
      };
      path = GetNavigationSystem(source.GetGame()).CalculatePath(targetPos, sourcePos, NavGenAgentSize.Human, 0);
      currentDistance = path.CalculateLength();
      if(lastDistance == 0 || lastDistance > currentDistance && currentDistance != 0) {
        target = WeakRefToRef(targets[i]);
        lastDistance = currentDistance;
      };
      i += 1;
    };
    return RefToWeakRef(target);
  }

  private final array<wref<ScriptedPuppet>> GetNearestTargets(ref<GameObject> source, Int32 amount, EffectScriptContext ctx) {
    Int32 i;
    array<wref<ScriptedPuppet>> targets;
    array<wref<ScriptedPuppet>> foundTargets;
    ref<ScriptedPuppet> currentTarget;
    ref<PuppetForceVisionAppearanceData> bbData;
    bbData = GetForceVisionAppearanceData(ctx);
    targets = bbData.m_targets;
    i = 0;
    while(i < amount) {
      currentTarget = WeakRefToRef(GetNearestTarget(source, targets, ctx));
      Remove(targets, RefToWeakRef(currentTarget));
      Push(foundTargets, RefToWeakRef(currentTarget));
      i += 1;
    };
    return foundTargets;
  }

  private final Float GetFarestTargetDistance(ref<GameObject> source, array<wref<ScriptedPuppet>> targets, out Int32 index) {
    Int32 i;
    ref<ScriptedPuppet> target;
    Float lastDistance;
    Float currentDistance;
    Vector4 targetPos;
    Vector4 sourcePos;
    sourcePos = source.GetWorldPosition();
    i = 0;
    while(i < Size(targets)) {
      targetPos = WeakRefToRef(targets[i]).GetWorldPosition();
      currentDistance = Distance(sourcePos, targetPos);
      if(i == 0) {
        target = WeakRefToRef(targets[i]);
        lastDistance = currentDistance;
      } else {
        if(currentDistance > lastDistance) {
          target = WeakRefToRef(targets[i]);
          index = i;
          lastDistance = currentDistance;
        };
      };
      i += 1;
    };
    return lastDistance;
  }

  private final void SendForceVisionApperaceEvent(Bool enable, wref<GameObject> owner, wref<GameObject> source, EffectScriptContext ctx) {
    ref<ForceVisionApperanceEvent> evt;
    ref<FocusForcedHighlightData> highlight;
    ref<AddForceHighlightTargetEvent> addTargetEvent;
    ref<ScriptedPuppet> puppet;
    ref<UpdateWillingInvestigators> updateInvestEvt;
    ref<PuppetForceVisionAppearanceData> bbData;
    if(WeakRefToRef(owner) == null || WeakRefToRef(source) == null) {
      return ;
    };
    evt = new ForceVisionApperanceEvent();
    highlight = new FocusForcedHighlightData();
    updateInvestEvt = new UpdateWillingInvestigators();
    highlight.sourceID = WeakRefToRef(source).GetEntityID();
    highlight.sourceName = StringToName(GetEffectName(ctx));
    highlight.highlightType = GetHighlightType(ctx);
    highlight.outlineType = GetOutlineType(ctx);
    highlight.inTransitionTime = GetTransitionTime(ctx);
    highlight.priority = GetPriority(ctx);
    highlight.isRevealed = highlight.highlightType == EFocusForcedHighlightType.DISTRACTION || highlight.outlineType == EFocusOutlineType.DISTRACTION;
    evt.forcedHighlight = highlight;
    evt.apply = enable;
    WeakRefToRef(owner).QueueEvent(evt);
    if(WeakRefToRef(owner) == WeakRefToRef(source)) {
      bbData = GetForceVisionAppearanceData(ctx);
      bbData.m_sourceHighlighted = enable;
      SetForceVisionAppearanceData(ctx, bbData);
      return ;
    };
    puppet = Cast(WeakRefToRef(owner));
    if(enable) {
      if(ToBool(puppet)) {
        bbData = GetForceVisionAppearanceData(ctx);
        if(WeakRefToRef(owner) != WeakRefToRef(source) && !Contains(bbData.m_highlightedTargets, RefToWeakRef(puppet))) {
          updateInvestEvt.investigator = WeakRefToRef(owner).GetEntityID();
          WeakRefToRef(source).QueueEvent(updateInvestEvt);
        };
        AddHighlightTarget(puppet, ctx);
      };
      addTargetEvent = new AddForceHighlightTargetEvent();
      addTargetEvent.targetID = WeakRefToRef(owner).GetEntityID();
      addTargetEvent.effecName = highlight.sourceName;
      WeakRefToRef(source).QueueEvent(addTargetEvent);
    } else {
      if(ToBool(puppet)) {
        RemoveHighlightTarget(puppet, ctx);
      };
    };
  }
}

public class ApplyJammer extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<Entity> entity;
    ref<SetJammedEvent> evt;
    evt = new SetJammedEvent();
    entity = Cast(GetTarget(applierCtx));
    if(ToBool(entity)) {
      evt.newJammedState = true;
      evt.instigator = RefToWeakRef(Cast(GetInstigator(ctx)));
      entity.QueueEvent(evt);
      return true;
    };
    return false;
  }
}

public class ApplyJammerFromCw extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<SensorJammed> evt;
    ref<Entity> entity;
    evt = new SensorJammed();
    evt.sensor = RefToWeakRef(Cast(GetTarget(applierCtx)));
    entity = GetWeapon(ctx);
    entity.QueueEvent(evt);
    return true;
  }
}

public class EMP extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    return true;
  }

  public final void TargetAcquired(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<GameObject> target;
    ref<Device> device;
    ref<EMPHitEvent> unpowerEVT;
    target = Cast(GetTarget(applierCtx));
    if(target == null) {
      return ;
    };
    if(ToBool(Cast(target))) {
      ApplyStatusEffect(RefToWeakRef(target), "BaseStatusEffect.EMP");
    };
    device = Cast(target);
    if(device != null) {
      unpowerEVT = new EMPHitEvent();
      device.QueueEvent(unpowerEVT);
    };
  }
}

public class EMPExplosion extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<Device> device;
    ref<EMPHitEvent> unpowerEVT;
    device = Cast(GetTarget(applierCtx));
    if(device != null) {
      unpowerEVT = new EMPHitEvent();
      device.QueueEvent(unpowerEVT);
    };
    return true;
  }
}

public class EffectExecutor_PingNetwork extends EffectExecutor_Scripted {

  private edit FxResource m_fxResource;

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<RegisterPingNetworkLinkRequest> registerLinkRequest;
    SNetworkLinkData linkData;
    array<SNetworkLinkData> linksData;
    ref<GameObject> source;
    ref<GameObject> target;
    FxResource fxResource;
    Variant fxVariant;
    Float duration;
    GlobalNodeID node;
    Vector4 emptyVector;
    Vector4 networkDeviceMeshPosition;
    wref<GameObject> initialPingSource;
    if(GetNetworkSystem(ctx).IsPingLinksLimitReached() || !GetNetworkSystem(ctx).HasAnyActivePingWithRevealNetwork()) {
      return true;
    };
    source = Cast(GetSource(ctx));
    target = Cast(GetTarget(applierCtx));
    networkDeviceMeshPosition = GetHitPosition(applierCtx);
    if(!IsTargetValid(target, source, ctx)) {
      return true;
    };
    initialPingSource = GetNetworkSystem(ctx).GetInitialPingSource();
    GetVariant(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.fxResource, fxVariant);
    fxResource = FromVariant(fxVariant);
    if(!IsValid(fxResource)) {
      fxResource = this.m_fxResource;
    };
    GetFloat(GetSharedData(ctx), GetAllBlackboardDefs().EffectSharedData.duration, duration);
    registerLinkRequest = new RegisterPingNetworkLinkRequest();
    linkData.masterID = source.GetEntityID();
    linkData.drawLink = true;
    linkData.linkType = ELinkType.FREE;
    linkData.isDynamic = target.IsNetworkLinkDynamic() || source.IsNetworkLinkDynamic();
    linkData.revealMaster = false;
    linkData.revealSlave = false;
    linkData.fxResource = fxResource;
    linkData.isPing = true;
    linkData.permanent = GetNetworkSystem(ctx).GetPingType(source.GetEntityID()) == EPingType.SPACE;
    linkData.masterPos = source.GetNetworkBeamEndpoint();
    linkData.slavePos = target.GetNetworkBeamEndpoint();
    linkData.slaveID = target.GetEntityID();
    if(!GetNetworkSystem(ctx).HasNetworkLink(linkData)) {
      Push(registerLinkRequest.linksData, linkData);
    };
    if(Size(registerLinkRequest.linksData) > 0) {
      GetNetworkSystem(ctx).QueueRequest(registerLinkRequest);
    };
    return true;
  }

  public final void TargetLost(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx)

  private final Bool ShouldRevealObject(ref<GameObject> object) {
    ref<Device> device;
    device = Cast(object);
    if(ToBool(device)) {
      return device.GetDevicePS().HasNetworkBackdoor();
    };
    return false;
  }

  private final FxResource GetFxResource(ref<GameObject> object) {
    ref<Device> device;
    FxResource resource;
    device = Cast(object);
    if(ToBool(device)) {
      if(device.GetDevicePS().IsBreached()) {
        resource = device.GetFxResourceByKey("networkLinkBreached");
      } else {
        resource = device.GetFxResourceByKey("networkLinkDefault");
      };
    };
    return resource;
  }

  private final Bool IsTargetValid(ref<GameObject> target, ref<GameObject> source, EffectScriptContext ctx) {
    if(source == null || target == null) {
      return false;
    };
    if(Cast(target) == null && Cast(target) == null) {
      return false;
    };
    if(source == target) {
      return false;
    };
    if(ToBool(Cast(target)) || ToBool(Cast(source))) {
      return false;
    };
    if(ToBool(Cast(target)) || ToBool(Cast(source))) {
      return false;
    };
    if(ToBool(Cast(target)) && ToBool(Cast(source)) || ToBool(Cast(source)) && ToBool(Cast(target))) {
      return false;
    };
    if(ToBool(Cast(target)) && !Cast(target).ShouldRevealDevicesGrid() || ToBool(Cast(source)) && !Cast(source).ShouldRevealDevicesGrid()) {
      return false;
    };
    return true;
  }

  protected final const ref<NetworkSystem> GetNetworkSystem(EffectScriptContext ctx) {
    return Cast(GetScriptableSystemsContainer(GetGameInstance(ctx)).Get("NetworkSystem"));
  }
}

public class EffectExecutor_MuteBubble extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    return true;
  }

  public final void TargetAcquired(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    if(IsTargetValid(ctx, applierCtx)) {
      GetStatusEffectSystem(GetGameInstance(ctx)).ApplyStatusEffect(GetTarget(applierCtx).GetEntityID(), "BaseStatusEffect.MuteAudioStims");
      GetStatusEffectSystem(GetGameInstance(ctx)).ApplyStatusEffect(GetTarget(applierCtx).GetEntityID(), "BaseStatusEffect.JamCommuniations");
    };
  }

  public final void TargetLost(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    GetStatusEffectSystem(GetGameInstance(ctx)).RemoveStatusEffect(GetTarget(applierCtx).GetEntityID(), "BaseStatusEffect.MuteAudioStims");
    GetStatusEffectSystem(GetGameInstance(ctx)).RemoveStatusEffect(GetTarget(applierCtx).GetEntityID(), "BaseStatusEffect.JamCommuniations");
  }

  private final const Bool IsTargetValid(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    Bool statusEffect;
    statusEffect = GetStatusEffectSystem(GetGameInstance(ctx)).HasStatusEffect(GetTarget(applierCtx).GetEntityID(), "BaseStatusEffect.MuteAudioStims");
    return ToBool(GetTarget(applierCtx)) && !statusEffect;
  }
}

public abstract class EffectExecutor_Device extends EffectExecutor_Scripted {

  [Default(EffectExecutor_Device, 0.0f))]
  public edit Float m_maxDelay;

  protected final void QueueEventOnDevice(wref<InteractiveDevice> device, ref<ActionBool> evt) {
    Float delay;
    delay = this.m_maxDelay > 0 ? RandRangeF(0, this.m_maxDelay) : 0;
    if(delay > 0) {
      GetDelaySystem(WeakRefToRef(device).GetGame()).DelayPSEvent(WeakRefToRef(device).GetDevicePS().GetID(), WeakRefToRef(device).GetDevicePS().GetClassName(), evt, delay);
    } else {
      GetPersistencySystem(WeakRefToRef(device).GetGame()).QueuePSEvent(WeakRefToRef(device).GetDevicePS().GetID(), WeakRefToRef(device).GetDevicePS().GetClassName(), evt);
    };
  }
}

public class EffectExecutor_SetDeviceOFF extends EffectExecutor_Device {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    wref<InteractiveDevice> device;
    ref<DeactivateDevice> evt;
    device = RefToWeakRef(Cast(GetTarget(applierCtx)));
    if(ToBool(device)) {
      if(ToBool(Cast(WeakRefToRef(device)))) {
        return false;
      };
      evt = new DeactivateDevice();
      evt.SetProperties();
      QueueEventOnDevice(device, evt);
    };
    return true;
  }
}

public class EffectExecutor_SetDeviceON extends EffectExecutor_Device {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    wref<InteractiveDevice> device;
    ref<ActivateDevice> evt;
    ref<Entity> owner;
    owner = GetInstigator(ctx);
    device = RefToWeakRef(Cast(GetTarget(applierCtx)));
    if(ToBool(device)) {
      if(ToBool(Cast(WeakRefToRef(device)))) {
      } else {
        if(ToBool(owner) && Distance(owner.GetWorldPosition(), WeakRefToRef(device).GetWorldPosition()) < 3) {
          return true;
        };
      };
      evt = new ActivateDevice();
      evt.SetProperties();
      QueueEventOnDevice(device, evt);
    };
    return true;
  }
}

public class EffectExecutor_ToggleDevice extends EffectExecutor_Device {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    wref<InteractiveDevice> device;
    ref<ToggleActivate> evt;
    device = RefToWeakRef(Cast(GetTarget(applierCtx)));
    if(ToBool(device)) {
      evt = new ToggleActivate();
      QueueEventOnDevice(device, evt);
    };
    return true;
  }
}

public class EffectExecutor_ReactionStimuli extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<ScriptedPuppet> target;
    target = Cast(GetTarget(applierCtx));
    if(ToBool(target) && ToBool(target.GetStimReactionComponent())) {
      target.GetStimReactionComponent().StartProcess(true);
    };
    return true;
  }
}

public class EffectExecutor_GrenadeTargetTracker extends EffectExecutor_Scripted {

  public edit const array<CName> m_potentialTargetSlots;

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    return true;
  }

  public final void TargetAcquired(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<GrenadeTrackerTargetAcquiredEvent> targetAcquiredEvent;
    ref<NPCPuppet> target;
    CName targetSlot;
    target = Cast(GetTarget(applierCtx));
    if(IsTargetValid(target, ctx, applierCtx, targetSlot)) {
      targetAcquiredEvent = new GrenadeTrackerTargetAcquiredEvent();
      targetAcquiredEvent.target = RefToWeakRef(target);
      targetAcquiredEvent.targetSlot = targetSlot;
      GetSource(ctx).QueueEvent(targetAcquiredEvent);
    };
  }

  public final void TargetLost(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<GrenadeTrackerTargetLostEvent> targetLostEvent;
    ref<NPCPuppet> target;
    target = Cast(GetTarget(applierCtx));
    if(ToBool(target)) {
      targetLostEvent = new GrenadeTrackerTargetLostEvent();
      targetLostEvent.target = RefToWeakRef(target);
      GetSource(ctx).QueueEvent(targetLostEvent);
    };
  }

  private final const Bool IsTargetValid(ref<NPCPuppet> target, EffectScriptContext ctx, EffectExecutionScriptContext applierCtx, out CName targetSlot) {
    if(!ToBool(target)) {
      return false;
    };
    if(IsAlive(target) && !IsDefeated(target) && !IsUnconscious(target) && !IsPlayerCompanion(RefToWeakRef(target)) && target.GetAttitudeTowards(Cast(GetInstigator(ctx))) != EAIAttitude.AIA_Friendly && !target.IsCrowd() && IsTargetReachable(ctx, target, targetSlot)) {
      return true;
    };
    return false;
  }

  private final const Bool IsTargetReachable(EffectScriptContext ctx, ref<NPCPuppet> target, out CName targetSlot) {
    Vector4 sourcePosition;
    ref<SlotComponent> slotComponent;
    WorldTransform slotTransform;
    Vector4 startPoint;
    Vector4 endPoint;
    Int32 i;
    if(Size(this.m_potentialTargetSlots) == 0) {
      return true;
    };
    sourcePosition = GetSource(ctx).GetWorldPosition();
    slotComponent = target.GetSlotComponent();
    i = 0;
    while(i < Size(this.m_potentialTargetSlots)) {
      if(slotComponent.GetSlotTransform(this.m_potentialTargetSlots[i], slotTransform)) {
        if(GetAngleBetweenSourceUpAndTarget(sourcePosition, ToVector4(GetWorldPosition(slotTransform))) > 150) {
          startPoint = sourcePosition + new Vector4(0,0,-0.5,0);
        } else {
          startPoint = sourcePosition + new Vector4(0,0,0.5,0);
        };
        endPoint = ToVector4(GetWorldPosition(slotTransform)) + Normalize(startPoint - ToVector4(GetWorldPosition(slotTransform))) * 0.30000001192092896;
        if(IsPointReachable(ctx, startPoint, endPoint)) {
          targetSlot = this.m_potentialTargetSlots[i];
          return true;
        };
      };
      i += 1;
    };
    return false;
  }

  private final const Float GetAngleBetweenSourceUpAndTarget(Vector4 sourcePosition, Vector4 targetPosition) {
    Float angle;
    Vector4 vectorToTarget;
    vectorToTarget = targetPosition - sourcePosition;
    angle = GetAngleBetween(new Vector4(0,0,1,0), vectorToTarget);
    return angle;
  }

  private final const Bool IsPointReachable(EffectScriptContext ctx, Vector4 startPoint, Vector4 endPoint) {
    TraceResult raycastResult;
    GetSpatialQueriesSystem(GetGameInstance(ctx)).SyncRaycastByCollisionPreset(startPoint, endPoint, "World Static", raycastResult);
    return !IsValid(raycastResult);
  }
}

public class EffectExecutor_TrackTargets extends EffectExecutor_Scripted {

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    return true;
  }

  public final void TargetAcquired(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<TargetAcquiredEvent> targetAcquiredEvent;
    ref<GameObject> target;
    ref<Entity> source;
    target = Cast(GetTarget(applierCtx));
    source = GetSource(ctx);
    if(IsTargetValid(target, ctx, applierCtx)) {
      targetAcquiredEvent = new TargetAcquiredEvent();
      targetAcquiredEvent.target = RefToWeakRef(Cast(target));
      source.QueueEvent(targetAcquiredEvent);
    };
  }

  public final void TargetLost(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<TargetLostEvent> targetLostEvent;
    ref<GameObject> target;
    ref<Entity> source;
    target = Cast(GetTarget(applierCtx));
    source = GetSource(ctx);
    if(IsTargetValid(target, ctx, applierCtx)) {
      targetLostEvent = new TargetLostEvent();
      targetLostEvent.target = RefToWeakRef(Cast(target));
      source.QueueEvent(targetLostEvent);
    };
  }

  private final const Bool IsTargetValid(ref<GameObject> target, EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    if(IsAlive(Cast(target))) {
      return true;
    };
    return false;
  }
}

public class EffectExecutor_SendActionSignal extends EffectExecutor_Scripted {

  public edit CName m_signalName;

  [Default(EffectExecutor_SendActionSignal, 0.0f))]
  public edit Float m_signalDuration;

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<NPCPuppet> target;
    target = Cast(GetTarget(applierCtx));
    if(ToBool(target)) {
      SendActionSignal(RefToWeakRef(target), this.m_signalName, this.m_signalDuration);
    };
    return true;
  }
}

public class EffectExecutor_VisualEffectAtTarget extends EffectExecutor_Scripted {

  public edit FxResource m_effect;

  public edit Bool m_ignoreTimeDilation;

  public final Bool Process(EffectScriptContext ctx, EffectExecutionScriptContext applierCtx) {
    ref<Entity> target;
    WorldPosition worldPosition;
    WorldTransform transform;
    target = GetTarget(applierCtx);
    if(ToBool(target)) {
      SetVector4(worldPosition, target.GetWorldPosition());
      SetWorldPosition(transform, worldPosition);
      SetOrientation(transform, target.GetWorldOrientation());
      SpawnEffect(ctx, this.m_effect, transform, this.m_ignoreTimeDilation);
      return true;
    };
    return false;
  }

  public final void Preload(EffectPreloadScriptContext ctx) {
    PreloadFxResource(ctx, this.m_effect);
  }
}
