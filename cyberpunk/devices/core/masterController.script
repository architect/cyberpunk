
public class MasterController extends ScriptableDC {

  protected const ref<MasterControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class MasterControllerPS extends ScriptableDeviceComponentPS {

  protected ref<Clearance> m_clearance;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
  }

  protected void Initialize() {
    Initialize();
  }

  protected EntityNotificationType OnDeviceDynamicConnectionChange(ref<DeviceDynamicConnectionChange> evt) {
    OnDeviceDynamicConnectionChange(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected void CacheDevices()

  public const EDeviceStatus GetExpectedSlaveState() {
    return EDeviceStatus.INVALID;
  }

  protected const ref<Clearance> GetClearance() {
    return CreateClearance(1, 100);
  }

  protected Bool DetermineGameplayViability(GetActionsContext context, Bool hasActiveActions) {
    return Evaluate(this, hasActiveActions);
  }

  public CName GetWidgetTypeName() {
    return "GenericMasterDeviceWidget";
  }

  public final array<ref<DeviceComponentPS>> NetrunnerGiveConnectedDevices() {
    EntityID entityID;
    array<ref<DeviceComponentPS>> devices;
    entityID = ExtractEntityID(GetID());
    GetDeviceSystem(GetGameInstance()).GetChildren(entityID, devices);
    return devices;
  }

  public const void FillAgentsList(out array<PSOwnerData> agentsList)

  public const Bool IsMasterType() {
    return true;
  }

  public const ref<DeviceComponentPS> GetFirstAttachedSlave() {
    array<ref<DeviceComponentPS>> slaves;
    Int32 i;
    slaves = GetImmediateSlaves();
    i = 0;
    while(i < Size(slaves)) {
      if(slaves[i].IsAttachedToGame()) {
        return slaves[i];
      };
      i += 1;
    };
    return null;
  }

  public final const void GetAllDescendants(out array<ref<DeviceComponentPS>> outDevices) {
    GetDeviceSystem(GetGameInstance()).GetAllDescendants(ExtractEntityID(GetID()), outDevices);
  }

  public final const array<ref<PuppetDeviceLinkPS>> GetPuppets() {
    array<ref<DeviceComponentPS>> allSlaves;
    array<ref<PuppetDeviceLinkPS>> puppets;
    ref<PuppetDeviceLinkPS> puppetLink;
    Int32 i;
    GetAllDescendants(allSlaves);
    i = 0;
    while(i < Size(allSlaves)) {
      puppetLink = Cast(allSlaves[i]);
      if(ToBool(puppetLink)) {
        Push(puppets, puppetLink);
      };
      i += 1;
    };
    return puppets;
  }

  public const array<ref<DeviceComponentPS>> GetImmediateSlaves() {
    array<ref<DeviceComponentPS>> slaves;
    GetDeviceSystem(GetGameInstance()).GetChildren(GetMyEntityID(), slaves);
    return slaves;
  }

  public const Bool HasAnySlave() {
    array<ref<DeviceComponentPS>> immediateSlaves;
    immediateSlaves = GetImmediateSlaves();
    return Size(immediateSlaves) > 0;
  }

  public final const array<ref<DeviceComponentPS>> GetImmediateDescendants() {
    EntityID entityID;
    array<ref<DeviceComponentPS>> immediateDescendants;
    entityID = ExtractEntityID(GetID());
    GetDeviceSystem(GetGameInstance()).GetAllDescendants(entityID, immediateDescendants);
    return immediateDescendants;
  }

  protected final Bool ExtractActionFromSlave(ref<DeviceComponentPS> slave, CName actionName, out ref<DeviceAction> outAction) {
    outAction = Cast(slave).GetActionByName(actionName);
    if(ToBool(outAction)) {
      return true;
    };
    return false;
  }

  protected final const void SendActionsToAllSlaves(array<ref<ScriptableDeviceAction>> actions) {
    Int32 i;
    i = 0;
    while(i < Size(actions)) {
      SendActionToAllSlaves(actions[i]);
      i += 1;
    };
  }

  protected const void SendActionToAllSlaves(ref<ScriptableDeviceAction> action) {
    array<ref<DeviceComponentPS>> slaves;
    Int32 i;
    slaves = GetImmediateSlaves();
    i = 0;
    while(i < Size(slaves)) {
      ExecutePSAction(action, slaves[i]);
      i += 1;
    };
  }

  protected final const void SendEventToAllSlaves(ref<Event> evt) {
    array<ref<DeviceComponentPS>> slaves;
    Int32 i;
    slaves = GetImmediateSlaves();
    i = 0;
    while(i < Size(slaves)) {
      GetPersistencySystem().QueuePSEvent(slaves[i].GetID(), slaves[i].GetClassName(), evt);
      i += 1;
    };
  }

  protected final const void GetQuickHacksFromSlave(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    array<ref<DeviceComponentPS>> slaves;
    Int32 i;
    slaves = GetImmediateSlaves();
    i = 0;
    while(i < Size(slaves)) {
      Cast(slaves[i]).GetQuickHackActionsExternal(outActions, context);
      i += 1;
    };
  }

  public final void RequestAreaEffectVisualisationUpdateOnSlaves(CName areaEffectID, Bool show) {
    Int32 i;
    ref<AreaEffectVisualisationRequest> evt;
    array<ref<DeviceComponentPS>> devices;
    evt = new AreaEffectVisualisationRequest();
    evt.show = show;
    evt.areaEffectID = areaEffectID;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(GetID() == devices[i].GetID()) {
      } else {
        GetPersistencySystem().QueueEntityEvent(ExtractEntityID(devices[i].GetID()), evt);
      };
      i += 1;
    };
  }

  public void OnRequestThumbnailWidgetsUpdate(ref<RequestThumbnailWidgetsUpdateEvent> evt) {
    RequestThumbnailWidgetsUpdate(GetBlackboard());
  }

  public void OnRequestDeviceWidgetUpdate(ref<RequestDeviceWidgetUpdateEvent> evt) {
    RequestDeviceWidgetsUpdate(GetBlackboard(), evt.requester);
  }

  public const ref<MasterDeviceBaseBlackboardDef> GetBlackboardDef() {
    return GetAllBlackboardDefs().MasterDeviceBaseBlackboard;
  }

  public array<SThumbnailWidgetPackage> GetThumbnailWidgets() {
    Int32 i;
    array<ref<DeviceComponentPS>> devices;
    array<SThumbnailWidgetPackage> widgetsData;
    SThumbnailWidgetPackage currentWidget;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(devices[i].GetID() == GetID()) {
      } else {
        currentWidget = devices[i].GetThumbnailWidget();
        if(currentWidget.isValid) {
          Push(widgetsData, currentWidget);
        };
      };
      i += 1;
    };
    return widgetsData;
  }

  public array<SDeviceWidgetPackage> GetDeviceWidgets() {
    Int32 i;
    array<ref<DeviceComponentPS>> devices;
    array<SDeviceWidgetPackage> widgetsData;
    SDeviceWidgetPackage currentWidget;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      currentWidget = devices[i].GetDeviceWidget(GenerateContext(gamedeviceRequestType.External, GetClearance()));
      if(currentWidget.isValid) {
        Push(widgetsData, devices[i].GetDeviceWidget(GenerateContext(gamedeviceRequestType.External, GetClearance())));
      };
      i += 1;
    };
    return widgetsData;
  }

  public SDeviceWidgetPackage GetSlaveDeviceWidget(PersistentID deviceID) {
    Int32 i;
    array<ref<DeviceComponentPS>> devices;
    SDeviceWidgetPackage widgetData;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(devices[i].GetID() == GetID()) {
      } else {
        if(devices[i].GetID() == deviceID) {
          widgetData = devices[i].GetDeviceWidget(GenerateContext(gamedeviceRequestType.External, GetClearance()));
        } else {
          i += 1;
        };
      };
    };
    return widgetData;
  }

  public void RequestThumbnailWidgetsUpdate(ref<IBlackboard> blackboard) {
    array<SThumbnailWidgetPackage> widgetsData;
    widgetsData = GetThumbnailWidgets();
    if(ToBool(blackboard) && Size(widgetsData) > 0) {
      blackboard.SetVariant(GetBlackboardDef().ThumbnailWidgetsData, ToVariant(widgetsData));
      blackboard.SignalVariant(GetBlackboardDef().ThumbnailWidgetsData);
      blackboard.FireCallbacks();
    };
  }

  public void RequestDeviceWidgetsUpdate(ref<IBlackboard> blackboard, array<PersistentID> devices) {
    Int32 i;
    array<SDeviceWidgetPackage> widgetsData;
    i = 0;
    while(i < Size(devices)) {
      Push(widgetsData, GetSlaveDeviceWidget(devices[i]));
      i += 1;
    };
    if(ToBool(blackboard) && Size(widgetsData) > 0) {
      blackboard.SetVariant(GetBlackboardDef().DeviceWidgetsData, ToVariant(widgetsData));
      blackboard.SignalVariant(GetBlackboardDef().DeviceWidgetsData);
      blackboard.FireCallbacks();
    };
  }

  public void RequestDeviceWidgetsUpdate(ref<IBlackboard> blackboard, PersistentID deviceID) {
    array<SDeviceWidgetPackage> widgetsData;
    Push(widgetsData, GetSlaveDeviceWidget(deviceID));
    if(ToBool(blackboard) && Size(widgetsData) > 0) {
      blackboard.SetVariant(GetBlackboardDef().DeviceWidgetsData, ToVariant(widgetsData));
      blackboard.SignalVariant(GetBlackboardDef().DeviceWidgetsData);
      blackboard.FireCallbacks();
    };
  }

  public final void RequestAllDevicesWidgetsUpdate(ref<IBlackboard> blackboard) {
    array<SDeviceWidgetPackage> widgetsData;
    widgetsData = GetDeviceWidgets();
    if(ToBool(blackboard) && Size(widgetsData) > 0) {
      blackboard.SetVariant(GetBlackboardDef().DeviceWidgetsData, ToVariant(widgetsData));
      blackboard.SignalVariant(GetBlackboardDef().DeviceWidgetsData);
      blackboard.FireCallbacks();
    };
  }

  public final void RefreshSlaves_Event(Bool onInitialize?) {
    ref<RefreshSlavesEvent> evt;
    evt = new RefreshSlavesEvent();
    evt.onInitialize = onInitialize;
    GetPersistencySystem().QueuePSEvent(GetID(), GetClassName(), evt);
  }

  public final void RefreshDefaultHighlightOnSlaves() {
    array<ref<DeviceComponentPS>> slaves;
    ref<ForceUpdateDefaultHighlightEvent> evt;
    Int32 i;
    slaves = GetImmediateDescendants();
    evt = new ForceUpdateDefaultHighlightEvent();
    i = 0;
    while(i < Size(slaves)) {
      QueuePSEvent(RefToWeakRef(slaves[i]), evt);
      i += 1;
    };
  }

  public final void SetSlavesAsQuestImportant(Bool isImportant) {
    array<ref<DeviceComponentPS>> slaves;
    ref<SetAsQuestImportantEvent> evt;
    Int32 i;
    slaves = GetImmediateDescendants();
    evt = new SetAsQuestImportantEvent();
    evt.SetImportant(isImportant);
    i = 0;
    while(i < Size(slaves)) {
      QueuePSEvent(RefToWeakRef(slaves[i]), evt);
      i += 1;
    };
  }

  protected EntityNotificationType OnRefreshSlavesEvent(ref<RefreshSlavesEvent> evt) {
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnFillTakeOverChainBBoardEvent(ref<FillTakeOverChainBBoardEvent> evt) {
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final void FillTakeOverChainBB() {
    Int32 i;
    Int32 i1;
    Int32 validDeviceNumber;
    array<ref<DeviceComponentPS>> devices;
    ref<IBlackboard> chainBlackBoard;
    array<SWidgetPackage> deviceChain;
    SWidgetPackage newDeviceChainnStruct;
    ref<ControlledDeviceData> customData;
    Bool isDuplicated;
    chainBlackBoard = GetBlackboardSystem(GetGameInstance()).Get(GetAllBlackboardDefs().DeviceTakeControl);
    deviceChain = FromVariant(chainBlackBoard.GetVariant(GetAllBlackboardDefs().DeviceTakeControl.DevicesChain));
    devices = GetImmediateSlaves();
    validDeviceNumber = Size(deviceChain);
    i = 0;
    while(i < Size(devices)) {
      if(Cast(devices[i]).CanBeInDeviceChain()) {
        i1 = 0;
        while(i1 < Size(deviceChain)) {
          if(deviceChain[i1].ownerID == devices[i].GetID()) {
            isDuplicated = true;
          };
          i1 += 1;
        };
        if(isDuplicated) {
        } else {
          validDeviceNumber += 1;
          customData = new ControlledDeviceData();
          customData.m_isActive = Cast(devices[i]).IsControlledByPlayer();
          newDeviceChainnStruct.displayName = devices[i].GetDeviceName() + " " + "[" + IntToString(validDeviceNumber) + "]";
          newDeviceChainnStruct.ownerID = devices[i].GetID();
          newDeviceChainnStruct.ownerIDClassName = devices[i].GetClassName();
          newDeviceChainnStruct.customData = customData;
          newDeviceChainnStruct.libraryID = "device";
          Push(deviceChain, newDeviceChainnStruct);
        };
      };
      i += 1;
    };
    chainBlackBoard.SetVariant(GetAllBlackboardDefs().DeviceTakeControl.DevicesChain, ToVariant(deviceChain));
  }

  public void RevealDevicesGrid(Bool shouldDraw, Vector4 ownerEntityPosition?, FxResource fxDefault?, Bool isPing?, Float lifetime?, Bool revealSlave?, Bool revealMaster?, Bool ignoreRevealed?) {
    array<ref<DeviceComponentPS>> slaves;
    ref<ScriptableDeviceComponentPS> slave;
    ref<Device> slaveEntity;
    ref<RevealDeviceRequest> revealDeviceRequest;
    Vector4 slavePosition;
    ref<NetworkSystem> networkSystem;
    SNetworkLinkData linkData;
    array<SNetworkLinkData> linksData;
    ref<RegisterNetworkLinkRequest> registerLinkRequest;
    ref<UnregisterNetworkLinksByIDRequest> unregisterLinkRequest;
    Int32 i;
    if(!ShouldRevealDevicesGrid()) {
      return ;
    };
    networkSystem = GetNetworkSystem();
    if(!shouldDraw) {
      if(networkSystem.HasNetworkLink(ExtractEntityID(GetID()))) {
        unregisterLinkRequest = new UnregisterNetworkLinksByIDRequest();
        unregisterLinkRequest.ID = ExtractEntityID(GetID());
        networkSystem.QueueRequest(unregisterLinkRequest);
      };
      return ;
    };
    if(this.m_fullDepth) {
      slaves = GetImmediateDescendants();
    } else {
      slaves = GetImmediateSlaves();
    };
    if(Size(slaves) == 0) {
      return ;
    };
    if(IsUnpowered() && !CanRevealDevicesGridWhenUnpowered() || IsDisabled() || ignoreRevealed && WasRevealedInNetworkPing()) {
      return ;
    };
    linkData.masterID = ExtractEntityID(GetID());
    linkData.masterPos = ownerEntityPosition;
    linkData.linkType = ELinkType.GRID;
    linkData.isPing = isPing;
    linkData.lifetime = lifetime;
    linkData.revealSlave = revealSlave;
    linkData.revealMaster = revealMaster;
    if(isPing) {
      linkData.permanent = lifetime > 0;
    };
    linkData.fxResource = fxDefault;
    i = 0;
    while(i < Size(slaves)) {
      slave = Cast(slaves[i]);
      if(slave == null || !slave.ShouldRevealDevicesGrid() || ignoreRevealed && slave.WasRevealedInNetworkPing() || slave.IsUnpowered() && !slave.CanRevealDevicesGridWhenUnpowered()) {
      } else {
        slaveEntity = Cast(WeakRefToRef(slaves[i].GetOwnerEntityWeak()));
        if(!ToBool(slaveEntity)) {
          GetDeviceSystem(GetGameInstance()).GetNodePosition(ExtractEntityID(slaves[i].GetID()), slavePosition);
        } else {
          slavePosition = slaveEntity.GetNetworkBeamEndpoint();
        };
        linkData.slaveID = ExtractEntityID(slaves[i].GetID());
        linkData.slavePos = slavePosition;
        linkData.drawLink = this.m_drawGridLink && slave.ShouldDrawGridLink();
        linkData.isDynamic = IsLinkDynamic() || slave.IsLinkDynamic();
        if(isPing && !linkData.drawLink) {
        } else {
          Push(linksData, linkData);
        };
      };
      i += 1;
    };
    if(Size(linksData) > 0) {
      registerLinkRequest = new RegisterNetworkLinkRequest();
      registerLinkRequest.linksData = linksData;
      networkSystem.QueueRequest(registerLinkRequest);
    };
  }
}
