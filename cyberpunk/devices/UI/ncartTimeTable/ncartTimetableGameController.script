
public class NcartTimetableInkGameController extends DeviceInkGameControllerBase {

  private wref<inkCanvas> m_defaultUI;

  private wref<inkVideo> m_mainDisplayWidget;

  private wref<inkText> m_counterWidget;

  private Uint32 m_onGlitchingStateChangedListener;

  private Uint32 m_onTimeToDepartChangedListener;

  protected cb Bool OnUninitialize() {
    OnUninitialize();
    WeakRefToRef(this.m_mainDisplayWidget).Stop();
  }

  protected void SetupWidgets() {
    if(!this.m_isInitialized) {
      this.m_defaultUI = RefToWeakRef(Cast(WeakRefToRef(GetWidget("default_ui"))));
      this.m_counterWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("default_ui/counter_text"))));
      this.m_mainDisplayWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("main_display"))));
      WeakRefToRef(this.m_rootWidget).SetVisible(false);
    };
  }

  public void UpdateActionWidgets(array<SActionWidgetPackage> widgetsData)

  public void Refresh(EDeviceStatus state) {
    SetupWidgets();
    RequestActionWidgetsUpdate();
    switch(state) {
      case EDeviceStatus.ON:
        TurnOn();
        break;
      case EDeviceStatus.OFF:
        TurnOff();
        break;
      case EDeviceStatus.UNPOWERED:
        break;
      case EDeviceStatus.DISABLED:
        break;
      default:
    };
    Refresh(state);
  }

  protected void RegisterBlackboardCallbacks(ref<IBlackboard> blackboard) {
    RegisterBlackboardCallbacks(blackboard);
    if(ToBool(blackboard)) {
      this.m_onGlitchingStateChangedListener = blackboard.RegisterListenerVariant(GetOwner().GetBlackboardDef().GlitchData, this, "OnGlitchingStateChanged");
      this.m_onTimeToDepartChangedListener = blackboard.RegisterListenerInt(GetOwner().GetBlackboardDef().TimeToDepart, this, "OnTimeToDepartChanged");
    };
  }

  protected void UnRegisterBlackboardCallbacks(ref<IBlackboard> blackboard) {
    UnRegisterBlackboardCallbacks(blackboard);
    if(ToBool(blackboard)) {
      blackboard.UnregisterListenerVariant(GetOwner().GetBlackboardDef().GlitchData, this.m_onGlitchingStateChangedListener);
      blackboard.UnregisterListenerInt(GetOwner().GetBlackboardDef().TimeToDepart, this.m_onTimeToDepartChangedListener);
    };
  }

  protected ref<NcartTimetable> GetOwner() {
    return Cast(GetOwnerEntity());
  }

  protected cb Bool OnActionWidgetsUpdate(Variant value) {
    array<SActionWidgetPackage> widgets;
    widgets = FromVariant(value);
    UpdateActionWidgets(widgets);
  }

  protected cb Bool OnTimeToDepartChanged(Int32 value) {
    ref<inkTextParams> textParams;
    if(WeakRefToRef(this.m_counterWidget) != null) {
      textParams = new inkTextParams();
      textParams.AddTime("TIMER", value);
      WeakRefToRef(this.m_counterWidget).SetLocalizedTextScript("LocKey#48343", textParams);
    };
  }

  private void StartGlitchingScreen(GlitchData glitchData) {
    StopVideo();
    WeakRefToRef(this.m_defaultUI).SetVisible(false);
    if(glitchData.state == EGlitchState.DEFAULT) {
      PlayVideo("base\movies\misc\generic_noise_white.bk2", true, "");
    } else {
      PlayVideo("base\movies\misc\distraction_generic.bk2", true, "");
    };
  }

  private void StopGlitchingScreen() {
    StopVideo();
    WeakRefToRef(this.m_defaultUI).SetVisible(true);
  }

  public final void PlayVideo(ResRef videoPath, Bool looped, CName audioEvent) {
    WeakRefToRef(this.m_mainDisplayWidget).SetVideoPath(videoPath);
    WeakRefToRef(this.m_mainDisplayWidget).SetLoop(looped);
    if(IsNameValid(audioEvent)) {
      WeakRefToRef(this.m_mainDisplayWidget).SetAudioEvent(audioEvent);
    };
    WeakRefToRef(this.m_mainDisplayWidget).Play();
  }

  public final void StopVideo() {
    WeakRefToRef(this.m_mainDisplayWidget).Stop();
  }

  public final void TurnOff() {
    WeakRefToRef(this.m_rootWidget).SetVisible(false);
    WeakRefToRef(this.m_mainDisplayWidget).UnregisterFromCallback("OnVideoFinished", this, "OnVideoFinished");
  }

  public final void TurnOn() {
    WeakRefToRef(this.m_rootWidget).SetVisible(true);
  }
}
