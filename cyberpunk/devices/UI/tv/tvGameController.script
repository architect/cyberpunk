
public class TvChannelSpawnData extends IScriptable {

  public CName m_channelName;

  public String m_localizedName;

  public final void Initialize(CName channelName, String localizedName) {
    this.m_channelName = channelName;
    this.m_localizedName = localizedName;
  }
}

public class TvInkGameController extends DeviceInkGameControllerBase {

  private wref<inkCanvas> m_defaultUI;

  private wref<inkCanvas> m_securedUI;

  private wref<inkText> m_channellTextWidget;

  private wref<inkText> m_securedTextWidget;

  protected wref<inkVideo> m_mainDisplayWidget;

  private wref<inkWidget> m_actionsList;

  [Default(TvInkGameController, -1))]
  private Int32 m_activeChannelIDX;

  private array<SequenceVideo> m_activeSequence;

  [Default(TvInkGameController, 0))]
  private Int32 m_activeSequenceVideo;

  private array<wref<inkWidget>> m_globalTVChannels;

  protected wref<inkText> m_messegeWidget;

  protected wref<inkLeafWidget> m_backgroundWidget;

  [Default(TvInkGameController, -1))]
  private Int32 m_previousGlobalTVChannelID;

  [Default(TvInkGameController, -1))]
  private Int32 m_globalTVchanellsCount;

  private Int32 m_globalTVchanellsSpawned;

  private wref<inkWidget> m_globalTVslot;

  private CName m_activeAudio;

  private wref<ScreenMessageData_Record> m_activeMessage;

  private Uint32 m_onChangeChannelListener;

  private Uint32 m_onGlitchingStateChangedListener;

  protected cb Bool OnUninitialize() {
    OnUninitialize();
    if(ToBool(this.m_mainDisplayWidget)) {
      WeakRefToRef(this.m_mainDisplayWidget).Stop();
    };
  }

  protected void SetupWidgets() {
    if(!this.m_isInitialized) {
      WeakRefToRef(this.m_rootWidget).SetVisible(false);
      this.m_defaultUI = RefToWeakRef(Cast(WeakRefToRef(GetWidget("default_ui"))));
      this.m_securedUI = RefToWeakRef(Cast(WeakRefToRef(GetWidget("secured_ui"))));
      this.m_channellTextWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("default_ui/channel_text"))));
      this.m_securedTextWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("secured_ui/secured_text"))));
      this.m_mainDisplayWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("main_display"))));
      this.m_actionsList = RefToWeakRef(WeakRefToRef(GetWidget("default_ui/actions_list_slot")));
      this.m_messegeWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("messege_text"))));
      this.m_backgroundWidget = RefToWeakRef(Cast(WeakRefToRef(GetWidget("background"))));
      this.m_globalTVslot = RefToWeakRef(WeakRefToRef(GetWidget("global_tv_slot")));
      if(!GetOwner().IsInteractive()) {
        WeakRefToRef(this.m_rootWidget).SetInteractive(false);
        WeakRefToRef(this.m_defaultUI).SetInteractive(false);
        if(ToBool(this.m_actionsList)) {
          WeakRefToRef(this.m_actionsList).SetVisible(false);
        };
      };
    };
  }

  public void Refresh(EDeviceStatus state) {
    SetupWidgets();
    if(!IsGlobalTVInitialized()) {
      if(!WasGlobalTVinitalizationTrigered()) {
        InitializeGlobalTV();
      };
      return ;
    };
    switch(state) {
      case EDeviceStatus.ON:
        TurnOn();
        break;
      case EDeviceStatus.OFF:
        TurnOff();
        break;
      case EDeviceStatus.UNPOWERED:
        TurnOff();
        break;
      case EDeviceStatus.DISABLED:
        TurnOff();
        break;
      default:
    };
    Refresh(state);
  }

  protected void RegisterBlackboardCallbacks(ref<IBlackboard> blackboard) {
    RegisterBlackboardCallbacks(blackboard);
    if(ToBool(blackboard)) {
      this.m_onChangeChannelListener = blackboard.RegisterListenerInt(GetOwner().GetBlackboardDef().CurrentChannel, this, "OnChangeChannel");
      this.m_onGlitchingStateChangedListener = blackboard.RegisterListenerVariant(GetOwner().GetBlackboardDef().GlitchData, this, "OnGlitchingStateChanged");
    };
  }

  protected void UnRegisterBlackboardCallbacks(ref<IBlackboard> blackboard) {
    UnRegisterBlackboardCallbacks(blackboard);
    if(ToBool(blackboard)) {
      blackboard.UnregisterListenerVariant(GetOwner().GetBlackboardDef().GlitchData, this.m_onGlitchingStateChangedListener);
      blackboard.UnregisterListenerInt(GetOwner().GetBlackboardDef().CurrentChannel, this.m_onChangeChannelListener);
    };
  }

  protected ref<TV> GetOwner() {
    return Cast(GetOwnerEntity());
  }

  protected cb Bool OnChangeChannel(Int32 value) {
    if(this.m_cashedState != EDeviceStatus.ON) {
      Refresh(GetOwner().GetDeviceState());
    } else {
      SelectChannel(value);
    };
  }

  private void StartGlitchingScreen(GlitchData glitchData) {
    ResRef glitchVideoPath;
    StopVideo();
    HideAllGlobalTVChannels();
    if(glitchData.state == EGlitchState.DEFAULT) {
      glitchVideoPath = GetOwner().GetDefaultGlitchVideoPath();
    } else {
      glitchVideoPath = GetOwner().GetBroadcastGlitchVideoPath();
    };
    if(IsValid(glitchVideoPath)) {
      PlayVideo(glitchVideoPath, true, "");
    };
  }

  private void StopGlitchingScreen() {
    StopVideo();
    SelectChannel(this.m_activeChannelIDX, true);
  }

  private final void SelectChannel(Int32 value, Bool force?) {
    STvChannel channel;
    if(!this.m_isInitialized) {
      return ;
    };
    if(value == this.m_activeChannelIDX && !force) {
      return ;
    };
    channel = GetOwner().GetChannelData(value);
    this.m_activeSequence = channel.m_sequence;
    ResolveMessegeRecord(RefToWeakRef(GetMessageRecord(channel.m_messageRecordID)));
    if(value != this.m_activeChannelIDX && this.m_activeChannelIDX != -1) {
      StopVideo();
      HideAllGlobalTVChannels();
      this.m_activeSequenceVideo = 0;
    };
    if(IsGlobalTVChannel(channel)) {
      if(ShowGlobalTVChannel(channel.channelTweakID)) {
        this.m_activeChannelIDX = value;
        SetChannellText("");
      };
    } else {
      this.m_activeChannelIDX = value;
      SetChannellText(channel.channelName);
      PlayVideo(channel.videoPath, channel.looped, channel.audioEvent);
    };
  }

  public final void PlayVideo(ResRef videoPath, Bool looped, CName audioEvent) {
    RegisterTvChannel(-1);
    if(WeakRefToRef(this.m_mainDisplayWidget) == null) {
      return ;
    };
    WeakRefToRef(this.m_mainDisplayWidget).SetVideoPath(videoPath);
    WeakRefToRef(this.m_mainDisplayWidget).SetLoop(looped);
    if(IsNameValid(audioEvent)) {
      WeakRefToRef(this.m_mainDisplayWidget).SetAudioEvent(audioEvent);
    };
    if(IsValid(videoPath)) {
      WeakRefToRef(this.m_mainDisplayWidget).Play();
    };
  }

  public final void StopVideo() {
    ResRef invalidPath;
    if(WeakRefToRef(this.m_mainDisplayWidget) == null) {
      return ;
    };
    StopSound(GetOwner(), this.m_activeAudio);
    this.m_activeAudio = "";
    WeakRefToRef(this.m_mainDisplayWidget).Stop();
  }

  public final void SetChannellText(String channelName) {
    if(WeakRefToRef(this.m_channellTextWidget) != null) {
      WeakRefToRef(this.m_channellTextWidget).SetLocalizedTextScript(channelName);
    };
  }

  public final void SetSecuredText(String text) {
    if(WeakRefToRef(this.m_securedTextWidget) != null) {
      WeakRefToRef(this.m_securedTextWidget).SetText(text);
    };
  }

  public final void TurnOff() {
    WeakRefToRef(this.m_rootWidget).SetVisible(false);
    if(WeakRefToRef(this.m_mainDisplayWidget) != null && this.m_cashedState == EDeviceStatus.ON) {
      WeakRefToRef(this.m_mainDisplayWidget).UnregisterFromCallback("OnVideoFinished", this, "OnVideoFinished");
    };
    StopVideo();
    RegisterTvChannel(-1);
    this.m_activeSequenceVideo = 0;
    this.m_activeChannelIDX = -1;
    if(IsNameValid(this.m_activeAudio)) {
      StopSound(GetOwner(), this.m_activeAudio);
    };
  }

  public void TurnOn() {
    WeakRefToRef(this.m_rootWidget).SetVisible(true);
    WeakRefToRef(this.m_defaultUI).SetVisible(true);
    if(!GetOwner().IsDeviceSecured()) {
      SelectChannel(GetOwner().GetDevicePS().GetActiveStationIndex());
    };
    if(WeakRefToRef(this.m_mainDisplayWidget) != null && this.m_cashedState != EDeviceStatus.ON) {
      WeakRefToRef(this.m_mainDisplayWidget).RegisterToCallback("OnVideoFinished", this, "OnVideoFinished");
    };
  }

  protected cb Bool OnVideoFinished(wref<inkVideo> target) {
    Int32 index;
    if(Size(this.m_activeSequence) > 0 && this.m_activeSequenceVideo < Size(this.m_activeSequence)) {
      index = this.m_activeSequenceVideo;
      PlayVideo(this.m_activeSequence[index].videoPath, this.m_activeSequence[index].looped, this.m_activeSequence[index].audioEvent);
      this.m_activeSequenceVideo += 1;
    };
  }

  private final void RegisterTvChannel(Int32 id) {
    if(id == this.m_previousGlobalTVChannelID) {
      this.m_previousGlobalTVChannelID = -1;
    };
    GetGlobalTVSystem(GetOwner().GetGame()).RegisterTVChannelOnController(RefToWeakRef(this), this.m_previousGlobalTVChannelID, id);
    this.m_previousGlobalTVChannelID = id;
  }

  private final Bool IsGlobalTVInitialized() {
    return this.m_globalTVchanellsCount == this.m_globalTVchanellsSpawned;
  }

  private final Bool WasGlobalTVinitalizationTrigered() {
    return this.m_globalTVchanellsCount > -1;
  }

  private final void InitializeGlobalTV() {
    Int32 i;
    array<wref<ChannelData_Record>> channels;
    wref<inkWidget> channelWidget;
    ref<TvChannelSpawnData> spawnData;
    if(this.m_globalTVchanellsCount > -1) {
      return ;
    };
    channels = GetOwner().GetGlobalTVChannels();
    this.m_globalTVchanellsCount = Size(channels);
    i = 0;
    while(i < Size(channels)) {
      spawnData = new TvChannelSpawnData();
      spawnData.Initialize(WeakRefToRef(channels[i]).ChannelWidget(), WeakRefToRef(channels[i]).LocalizedName());
      if(HasLocalLibrary(WeakRefToRef(channels[i]).ChannelWidget())) {
        AsyncSpawnFromLocal(GetGlobalTVSlot(), WeakRefToRef(channels[i]).ChannelWidget(), this, "OnGLobalChannelSpawned", spawnData);
      } else {
        this.m_globalTVchanellsSpawned += 1;
      };
      i += 1;
    };
    if(IsGlobalTVInitialized()) {
      Refresh(GetOwner().GetDeviceState());
    };
  }

  private final wref<inkWidget> GetGlobalTVSlot() {
    if(WeakRefToRef(this.m_globalTVslot) != null) {
      return this.m_globalTVslot;
    };
    return GetRootWidget();
  }

  protected cb Bool OnGLobalChannelSpawned(ref<inkWidget> widget, ref<IScriptable> userData) {
    ref<TvChannelSpawnData> spawnData;
    spawnData = Cast(userData);
    if(ToBool(widget)) {
      this.m_globalTVchanellsSpawned += 1;
      widget.SetAnchor(inkEAnchor.Fill);
      widget.SetVisible(false);
      widget.SetName(spawnData.m_channelName);
      Push(this.m_globalTVChannels, RefToWeakRef(widget));
    };
    if(IsGlobalTVInitialized()) {
      Refresh(GetOwner().GetDeviceState());
    };
  }

  private final void HideAllGlobalTVChannels() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_globalTVChannels)) {
      WeakRefToRef(this.m_globalTVChannels[i]).SetVisible(false);
      i += 1;
    };
    if(IsNameValid(this.m_activeAudio)) {
      StopSound(GetOwner(), this.m_activeAudio);
    };
  }

  private final Bool ShowGlobalTVChannel(TweakDBID channelID) {
    Int32 i;
    wref<ChannelData_Record> channelRecord;
    CName audioEvent;
    Int32 realChannelID;
    channelRecord = RefToWeakRef(GetChannelDataRecord(channelID));
    if(ToBool(channelRecord)) {
      realChannelID = WeakRefToRef(channelRecord).OrderID();
      i = 0;
      while(i < Size(this.m_globalTVChannels)) {
        if(WeakRefToRef(this.m_globalTVChannels[i]).GetName() == WeakRefToRef(channelRecord).ChannelWidget()) {
          audioEvent = WeakRefToRef(channelRecord).AudioEvent();
          WeakRefToRef(this.m_globalTVChannels[i]).SetVisible(true);
          RegisterTvChannel(realChannelID);
          PlaySound(GetOwner(), audioEvent);
          this.m_activeAudio = audioEvent;
          SetChannellText("");
          return true;
        };
        i += 1;
      };
    };
    return false;
  }

  private final void HideGlobalTVChannel(TweakDBID channelID) {
    Int32 i;
    wref<ChannelData_Record> channelRecord;
    CName audioEvent;
    channelRecord = RefToWeakRef(GetChannelDataRecord(channelID));
    if(ToBool(channelRecord)) {
      i = 0;
      while(i < Size(this.m_globalTVChannels)) {
        if(WeakRefToRef(this.m_globalTVChannels[i]).GetName() == WeakRefToRef(channelRecord).ChannelWidget()) {
          audioEvent = WeakRefToRef(channelRecord).AudioEvent();
          StopSound(GetOwner(), audioEvent);
          this.m_activeAudio = "";
          WeakRefToRef(this.m_globalTVChannels[i]).SetVisible(false);
          SetChannellText("");
        };
        i += 1;
      };
    };
  }

  private final Bool IsGlobalTVChannel(STvChannel channel) {
    return IsValid(channel.channelTweakID);
  }

  protected void ResolveMessegeRecord(wref<ScreenMessageData_Record> record) {
    String fontPath;
    CName fontstyle;
    CName verticalAlignment;
    CName horizontalAlignment;
    if(WeakRefToRef(record) != null) {
      fontPath = WeakRefToRef(record).FontPath();
      fontstyle = WeakRefToRef(record).FontStyle();
      verticalAlignment = WeakRefToRef(record).TextVerticalAlignment();
      horizontalAlignment = WeakRefToRef(record).TextHorizontalAlignment();
      if(IsStringValid(fontPath)) {
        WeakRefToRef(this.m_messegeWidget).SetFontFamily(fontPath, fontstyle);
      } else {
        if(IsNameValid(fontstyle)) {
          WeakRefToRef(this.m_messegeWidget).SetFontStyle(fontstyle);
        };
      };
      if(IsNameValid(verticalAlignment)) {
        WeakRefToRef(this.m_messegeWidget).SetVerticalAlignment(WeakRefToRef(this.m_messegeWidget).GetVerticalAlignmentEnumValue(verticalAlignment));
      };
      if(IsNameValid(horizontalAlignment)) {
        WeakRefToRef(this.m_messegeWidget).SetHorizontalAlignment(WeakRefToRef(this.m_messegeWidget).GetHorizontalAlignmentEnumValue(horizontalAlignment));
      };
      WeakRefToRef(this.m_messegeWidget).UpdateMargin(WeakRefToRef(record).LeftMargin(), WeakRefToRef(record).TopMargin(), WeakRefToRef(record).RightMargin(), WeakRefToRef(record).BottomMargin());
      WeakRefToRef(this.m_messegeWidget).EnableAutoScroll(WeakRefToRef(record).AutoScroll());
      WeakRefToRef(this.m_messegeWidget).SetFontSize(WeakRefToRef(record).FontSize());
      WeakRefToRef(this.m_messegeWidget).SetLocalizedTextScript(WeakRefToRef(record).LocalizedDescription());
      WeakRefToRef(this.m_messegeWidget).SetTintColor(GetColorFromArray(WeakRefToRef(record).TextColor()));
      WeakRefToRef(this.m_messegeWidget).SetScrollTextSpeed(WeakRefToRef(record).ScrollSpeed());
      WeakRefToRef(this.m_backgroundWidget).SetTintColor(GetColorFromArray(WeakRefToRef(record).BackgroundColor()));
      WeakRefToRef(this.m_backgroundWidget).SetOpacity(WeakRefToRef(record).BackgroundOpacity());
      SetBackgroundTexture(RefToWeakRef(Cast(WeakRefToRef(this.m_backgroundWidget))), WeakRefToRef(record).BackgroundTextureID());
      this.m_activeMessage = record;
    } else {
      this.m_activeMessage = null;
      WeakRefToRef(this.m_messegeWidget).SetText("");
      WeakRefToRef(this.m_backgroundWidget).SetOpacity(0);
    };
  }

  private final Color GetColorFromArray(array<Int32> colorArray) {
    Int32 i;
    Color color;
    i = 0;
    while(i < Size(colorArray)) {
      if(i == 0) {
        color.Red = Cast(colorArray[i]);
      } else {
        if(i == 1) {
          color.Green = Cast(colorArray[i]);
        } else {
          if(i == 2) {
            color.Blue = Cast(colorArray[i]);
          } else {
            if(i == 3) {
              color.Alpha = Cast(colorArray[i]);
            };
          };
        };
      };
      i += 1;
    };
    return color;
  }

  private final const ref<ScreenMessageData_Record> GetMessageRecord(TweakDBID messageID) {
    wref<ScreenMessageData_Record> messageRecord;
    wref<ScreenMessagesList_Record> groupRecord;
    Int32 count;
    Int32 rand;
    if(!IsValid(messageID)) {
      return null;
    };
    messageRecord = RefToWeakRef(GetScreenMessageDataRecord(messageID));
    if(WeakRefToRef(messageRecord) != null) {
      groupRecord = WeakRefToRef(messageRecord).MessageGroup();
      if(WeakRefToRef(groupRecord) != null) {
        count = WeakRefToRef(groupRecord).GetMessagesCount();
        if(count > 0) {
          rand = RandRange(0, count);
          messageRecord = WeakRefToRef(groupRecord).GetMessagesItem(rand);
        };
      };
    };
    return WeakRefToRef(messageRecord);
  }

  private final void SetBackgroundTexture(wref<inkImage> imageWidget, TweakDBID textureID) {
    if(WeakRefToRef(imageWidget) != null && IsValid(textureID)) {
      RequestSetImage(this, imageWidget, textureID);
    };
  }

  private final void SetBackgroundTexture(wref<inkImage> imageWidget, wref<UIIcon_Record> textureRecord) {
    if(WeakRefToRef(imageWidget) != null && WeakRefToRef(textureRecord) != null) {
      WeakRefToRef(imageWidget).SetAtlasResource(WeakRefToRef(textureRecord).AtlasResourcePath());
      WeakRefToRef(imageWidget).SetTexturePart(WeakRefToRef(textureRecord).AtlasPartName());
    };
  }

  private final void SetBackgroundTexture(inkImageRef imageWidgetRef, wref<UIIcon_Record> textureRecord) {
    if(IsValid(imageWidgetRef) && WeakRefToRef(textureRecord) != null) {
      SetAtlasResource(imageWidgetRef, WeakRefToRef(textureRecord).AtlasResourcePath());
      SetTexturePart(imageWidgetRef, WeakRefToRef(textureRecord).AtlasPartName());
    };
  }

  protected cb Bool OnMessageTextureCallback(ref<iconAtlasCallbackData> e) {
    switch(e.loadResult) {
      case inkIconResult.Success:
        Log("TEST SUCCESS");
        break;
      case inkIconResult.AtlasResourceNotFound:
        Log("TEST FAIL");
        break;
      case inkIconResult.UnknownIconTweak:
        Log("TEST FAIL");
        break;
      case inkIconResult.PartNotFoundInAtlas:
        Log("TEST FAIL");
    };
  }
}
