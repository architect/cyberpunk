
public class NewsFeedMenuWidgetController extends inkLogicController {

  [Attrib(category, "OBSOLETE - Widget Paths")]
  [Default(NewsFeedMenuWidgetController, banners))]
  protected edit CName m_bannersListWidgetPath;

  [Attrib(category, "Widget Refs")]
  protected edit inkWidgetRef m_bannersListWidget;

  protected Bool m_isInitialized;

  private array<SBannerWidgetPackage> m_bannerWidgetsData;

  protected SBannerWidgetPackage m_fullBannerWidgetData;

  protected cb Bool OnInitialize() {
    this.m_fullBannerWidgetData.widgetTweakDBID = "DevicesUIDefinitions.FullBannerWidget";
  }

  public void InitializeBanners(ref<ComputerInkGameController> gameController, array<SBannerWidgetPackage> widgetsData) {
    Int32 i;
    ref<inkWidget> widget;
    HideBannerWidgets();
    i = 0;
    while(i < Size(widgetsData)) {
      widget = WeakRefToRef(GetBannerWidget(widgetsData[i], gameController));
      if(widget == null) {
        widget = WeakRefToRef(CreateBannerWidget(gameController, Get(this.m_bannersListWidget), widgetsData[i]));
        AddBannerWidget(widget, widgetsData[i], gameController);
      };
      InitializeBannerWidget(gameController, widget, widgetsData[i]);
      i += 1;
    };
    this.m_isInitialized = true;
  }

  public final wref<inkWidget> CreateBannerWidget(ref<ComputerInkGameController> gameController, wref<inkWidget> parentWidget, SBannerWidgetPackage widgetData) {
    ref<inkWidget> widget;
    ScreenDefinitionPackage screenDef;
    screenDef = gameController.GetScreenDefinition();
    widget = WeakRefToRef(gameController.FindWidgetInLibrary(parentWidget, GetWidgetDefinitionRecord(widgetData.widgetTweakDBID), WeakRefToRef(screenDef.screenDefinition.ComputerScreenType()), screenDef.style, widgetData.libraryID, widgetData.libraryPath));
    if(widget != null) {
      widget.SetSizeRule(inkESizeRule.Stretch);
    };
    return RefToWeakRef(widget);
  }

  protected final void InitializeBannerWidget(ref<ComputerInkGameController> gameController, ref<inkWidget> widget, SBannerWidgetPackage widgetData) {
    ref<ComputerBannerWidgetController> controller;
    controller = Cast(WeakRefToRef(widget.GetController()));
    if(controller != null) {
      controller.Initialize(gameController, widgetData);
    };
    widget.SetVisible(true);
  }

  protected final wref<inkWidget> GetBannerWidget(SBannerWidgetPackage widgetData, ref<ComputerInkGameController> gameController) {
    Int32 i;
    ScreenDefinitionPackage screenDef;
    screenDef = gameController.GetScreenDefinition();
    widgetData.libraryID = gameController.GetCurrentFullLibraryID(GetWidgetDefinitionRecord(widgetData.widgetTweakDBID), WeakRefToRef(screenDef.screenDefinition.ComputerScreenType()), screenDef.style);
    i = 0;
    while(i < Size(this.m_bannerWidgetsData)) {
      if(this.m_bannerWidgetsData[i].ownerID == widgetData.ownerID && this.m_bannerWidgetsData[i].widgetName == widgetData.widgetName && this.m_bannerWidgetsData[i].widgetTweakDBID == widgetData.widgetTweakDBID && this.m_bannerWidgetsData[i].libraryPath == widgetData.libraryPath && this.m_bannerWidgetsData[i].libraryID == widgetData.libraryID) {
        return this.m_bannerWidgetsData[i].widget;
      };
      i += 1;
    };
    return null;
  }

  protected final wref<inkWidget> AddBannerWidget(ref<inkWidget> widget, SBannerWidgetPackage widgetData, ref<ComputerInkGameController> gameController) {
    ScreenDefinitionPackage screenDef;
    screenDef = gameController.GetScreenDefinition();
    widgetData.libraryID = gameController.GetCurrentFullLibraryID(GetWidgetDefinitionRecord(widgetData.widgetTweakDBID), WeakRefToRef(screenDef.screenDefinition.ComputerScreenType()), screenDef.style);
    widgetData.widget = RefToWeakRef(widget);
    Push(this.m_bannerWidgetsData, widgetData);
    return widgetData.widget;
  }

  public final void HideBannerWidgets() {
    ref<ComputerBannerWidgetController> controller;
    Int32 i;
    i = 0;
    while(i < Size(this.m_bannerWidgetsData)) {
      if(WeakRefToRef(this.m_bannerWidgetsData[i].widget) != null) {
        WeakRefToRef(this.m_bannerWidgetsData[i].widget).SetVisible(false);
      };
      i += 1;
    };
  }

  public final void ShowFullBanner(ref<ComputerInkGameController> gameController, SBannerWidgetPackage widgetData) {
    ref<ComputerFullBannerWidgetController> controller;
    ScreenDefinitionPackage screenDef;
    TweakDBID widgetTweakDBID;
    ref<WidgetDefinition_Record> widgetRecord;
    CName libraryID;
    ResRef libraryPath;
    ResolveWidgetTweakDBData(this.m_fullBannerWidgetData.widgetTweakDBID, libraryID, libraryPath);
    widgetRecord = GetWidgetDefinitionRecord(this.m_fullBannerWidgetData.widgetTweakDBID);
    if(WeakRefToRef(this.m_fullBannerWidgetData.widget) == null || this.m_fullBannerWidgetData.libraryID != libraryID || this.m_fullBannerWidgetData.libraryPath != libraryPath) {
      if(WeakRefToRef(this.m_fullBannerWidgetData.widget) != null) {
        Cast(WeakRefToRef(gameController.GetMainLayoutController().GetMenuContainer())).RemoveChild(this.m_fullBannerWidgetData.widget);
      };
      screenDef = gameController.GetScreenDefinition();
      this.m_fullBannerWidgetData.widget = gameController.FindWidgetInLibrary(gameController.GetMainLayoutController().GetMenuContainer(), widgetRecord, WeakRefToRef(screenDef.screenDefinition.ComputerScreenType()), screenDef.style, widgetData.libraryID, widgetData.libraryPath);
      this.m_fullBannerWidgetData.libraryPath = libraryPath;
      this.m_fullBannerWidgetData.libraryID = libraryID;
    };
    controller = Cast(WeakRefToRef(WeakRefToRef(this.m_fullBannerWidgetData.widget).GetController()));
    if(ToBool(controller)) {
      SetVisible(this.m_bannersListWidget, false);
      WeakRefToRef(this.m_fullBannerWidgetData.widget).SetVisible(true);
      controller.Initialize(gameController, widgetData);
    };
  }

  public final void HideFullBanner() {
    WeakRefToRef(this.m_fullBannerWidgetData.widget).SetVisible(false);
    SetVisible(this.m_bannersListWidget, true);
  }
}
