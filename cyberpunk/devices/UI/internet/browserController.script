
public class BrowserGameController extends inkGameController {

  [Attrib(category, "Widget Refs")]
  public edit inkWidgetRef m_logicControllerRef;

  protected wref<JournalManager> m_journalManager;

  private array<CName> m_locationTags;

  protected cb Bool OnInitialize() {
    GameInstance gameInstance;
    ref<LocationManager> locationManager;
    wref<BrowserController> logicScript;
    logicScript = RefToWeakRef(Cast(WeakRefToRef(GetController(this.m_logicControllerRef))));
    WeakRefToRef(logicScript).Init(this);
    gameInstance = Cast(GetOwnerEntity()).GetGame();
    locationManager = GetLocationManager(gameInstance);
    locationManager.GetLocationTags(GetOwnerEntity().GetEntityID(), this.m_locationTags);
    PushWebsiteData();
    this.m_journalManager = RefToWeakRef(GetJournalManager(gameInstance));
    WeakRefToRef(this.m_journalManager).RegisterScriptCallback(this, "OnJournalEntryStateChanged", gameJournalListenerType.State);
  }

  protected cb Bool OnJournalEntryStateChanged(Uint32 entryHash, CName className, JournalNotifyOption notifyOption, JournalChangeType changeType) {
    if(className == "gameJournalInternetPage" || className == "gameJournalInternetSite") {
      PushWebsiteData();
    };
  }

  public final void PushWebsiteData() {
    JournalRequestContext context;
    array<wref<JournalEntry>> entries;
    wref<BrowserController> logicScript;
    context.stateFilter.active = true;
    if(ToBool(this.m_journalManager)) {
      WeakRefToRef(this.m_journalManager).GetInternetPages(context, entries);
    };
    logicScript = RefToWeakRef(Cast(WeakRefToRef(GetController(this.m_logicControllerRef))));
    WeakRefToRef(logicScript).SetWebsiteData(entries);
  }

  protected cb Bool OnUninitialize() {
    this.m_journalManager = RefToWeakRef(GetJournalManager(Cast(GetOwnerEntity()).GetGame()));
    WeakRefToRef(this.m_journalManager).UnregisterScriptCallback(this, "OnJournalEntryStateChanged");
  }

  public final ref<JournalManager> GetJournalManager() {
    return WeakRefToRef(this.m_journalManager);
  }
}

public class BrowserController extends inkLogicController {

  protected edit inkWidgetRef m_homeButton;

  protected edit ref<LinkController> m_homeButtonCoontroller;

  protected edit inkTextRef m_addressText;

  protected edit inkWidgetRef m_pageContentRoot;

  protected edit ResRef m_spinnerPath;

  protected edit CName m_webPageLibraryID;

  protected edit String m_defaultDevicePage;

  private ref<BrowserGameController> m_gameController;

  private array<wref<JournalInternetPage>> m_websiteData;

  private String m_currentRequestedPage;

  private wref<inkCompoundWidget> m_currentPage;

  private wref<inkWidget> m_spinner;

  public final void Init(ref<BrowserGameController> gameController) {
    this.m_gameController = gameController;
    this.m_homeButtonCoontroller = Cast(WeakRefToRef(GetController(this.m_homeButton)));
    if(ToBool(this.m_homeButtonCoontroller)) {
      this.m_homeButtonCoontroller.RegisterToCallback("OnRelease", this, "OnHomeButtonPressed");
    };
    this.m_spinner = RefToWeakRef(WeakRefToRef(SpawnFromExternal(Get(this.m_pageContentRoot), this.m_spinnerPath, "Root")));
    SetDefaultContent();
  }

  public final void SetWebsiteData(array<wref<JournalEntry>> pageEntries) {
    wref<JournalInternetPage> pageEntry;
    Int32 i;
    Clear(this.m_websiteData);
    i = 0;
    while(i < Size(pageEntries)) {
      pageEntry = RefToWeakRef(Cast(WeakRefToRef(pageEntries[i])));
      Push(this.m_websiteData, pageEntry);
      i += 1;
    };
  }

  public final void SetDefaultPage(String startingPage) {
    this.m_gameController.PushWebsiteData();
    this.m_defaultDevicePage = startingPage;
    SetDefaultContent();
  }

  public final const String GetDefaultpage() {
    return this.m_defaultDevicePage;
  }

  private final void SetDefaultContent() {
    this.m_homeButtonCoontroller.SetLinkAddress(this.m_defaultDevicePage);
    this.m_homeButtonCoontroller.SetColors(new Color(255,255,255,255), new Color(255,255,255,0));
    LoadWebPage(this.m_defaultDevicePage, this.m_gameController.GetJournalManager());
  }

  private final wref<JournalInternetPage> TryGetWebsiteData(String address) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_websiteData)) {
      if(WeakRefToRef(this.m_websiteData[i]).GetAddress() == address) {
        return this.m_websiteData[i];
      };
      i += 1;
    };
    i = 0;
    while(i < Size(this.m_websiteData)) {
      if(WeakRefToRef(this.m_websiteData[i]).GetAddress() == "NETdir://page_not_found") {
        return this.m_websiteData[i];
      };
      i += 1;
    };
    return null;
  }

  protected cb Bool OnProcessLinkPressed(wref<inkWidget> e) {
    ref<WebPage> page;
    page = Cast(WeakRefToRef(WeakRefToRef(e).GetController()));
    LoadWebPage(page.GetLastLinkClicked(), this.m_gameController.GetJournalManager());
  }

  private final void OnHomeButtonPressed(ref<inkPointerEvent> e) {
    ref<LinkController> linkController;
    if(e.IsAction("click")) {
      linkController = Cast(WeakRefToRef(WeakRefToRef(e.GetTarget()).GetController()));
      if(ToBool(linkController)) {
        LoadWebPage(linkController.GetLinkAddress(), this.m_gameController.GetJournalManager());
      };
    };
  }

  private final void LoadWebPage(String address, ref<JournalManager> journalManager) {
    wref<JournalInternetPage> page;
    array<CName> factsToSet;
    ref<WebPage> currentController;
    Vector2 scale;
    UnloadCurrentWebsite();
    this.m_currentRequestedPage = address;
    page = TryGetWebsiteData(this.m_currentRequestedPage);
    if(WeakRefToRef(page) == null) {
      WeakRefToRef(this.m_spinner).SetVisible(true);
      return ;
    };
    WeakRefToRef(this.m_spinner).SetVisible(false);
    SetText(this.m_addressText, WeakRefToRef(page).GetAddress());
    this.m_currentPage = RefToWeakRef(Cast(WeakRefToRef(SpawnFromExternal(Get(this.m_pageContentRoot), WeakRefToRef(page).GetWidgetPath(), this.m_webPageLibraryID))));
    WeakRefToRef(this.m_currentPage).SetAnchor(inkEAnchor.Fill);
    scale.X = WeakRefToRef(page).GetScale();
    scale.Y = WeakRefToRef(page).GetScale();
    WeakRefToRef(this.m_currentPage).SetScale(scale);
    currentController = Cast(WeakRefToRef(WeakRefToRef(this.m_currentPage).GetController()));
    if(ToBool(currentController)) {
      currentController.FillPage(page, journalManager);
      currentController.RegisterToCallback("OnLinkPressed", this, "OnProcessLinkPressed");
    };
    SetFacts(page);
  }

  private final void SetFacts(wref<JournalInternetPage> page) {
    array<JournalFactNameValue> factsToSet;
    Int32 i;
    factsToSet = WeakRefToRef(page).GetFactsToSet();
    i = 0;
    while(i < Size(factsToSet)) {
      if(factsToSet[i].factName != "") {
        SetFactValue(GetOwnerGameObject().GetGame(), factsToSet[i].factName, factsToSet[i].factValue);
      };
      i += 1;
    };
  }

  private final void UnloadCurrentWebsite() {
    ref<WebPage> currentController;
    if(WeakRefToRef(this.m_currentPage) != null) {
      currentController = Cast(WeakRefToRef(WeakRefToRef(this.m_currentPage).GetController()));
      currentController.UnregisterFromCallback("OnLinkPressed", this, "OnProcessLinkPressed");
      Cast(WeakRefToRef(Get(this.m_pageContentRoot))).RemoveChild(this.m_currentPage);
    };
  }

  private final ref<Computer> GetOwnerGameObject() {
    return Cast(this.m_gameController.GetOwnerEntity());
  }
}

public class LinkController extends inkButtonController {

  private String m_linkAddress;

  private HDRColor m_defaultColor;

  private HDRColor m_hoverColor;

  private HDRColor IGNORED_COLOR;

  protected cb Bool OnInitialize() {
    this.IGNORED_COLOR = new HDRColor(1,1,1,0);
    this.m_hoverColor = new HDRColor(1,1,1,1);
  }

  protected cb Bool OnButtonStateChanged(wref<inkButtonController> controller, inkEButtonState oldState, inkEButtonState newState) {
    ref<inkWidget> widget;
    if(StrLen(this.m_linkAddress) == 0) {
      return true;
    };
    if(this.m_hoverColor == new HDRColor(1,1,1,0)) {
      return true;
    };
    widget = WeakRefToRef(GetRootWidget());
    if(oldState == inkEButtonState.Normal) {
      this.m_defaultColor = widget.GetTintColor();
    };
    switch(newState) {
      case inkEButtonState.Normal:
        widget.SetTintColor(this.m_defaultColor);
        break;
      case inkEButtonState.Press:
      case inkEButtonState.Hover:
        widget.SetTintColor(this.m_hoverColor);
        break;
      case inkEButtonState.Disabled:
        widget.SetTintColor(this.m_defaultColor);
    };
  }

  public final String GetLinkAddress() {
    return this.m_linkAddress;
  }

  public final void SetLinkAddress(String link) {
    this.m_linkAddress = link;
  }

  public final void SetColors(Color color, Color hoverColor) {
    ref<inkWidget> widget;
    if(ToHDRColorDirect(color) == this.IGNORED_COLOR) {
      widget = WeakRefToRef(GetRootWidget());
      this.m_defaultColor = widget.GetTintColor();
    } else {
      this.m_defaultColor = ToHDRColorDirect(color);
    };
    this.m_hoverColor = ToHDRColorDirect(hoverColor);
  }
}

public class WebPage extends inkLogicController {

  protected edit const array<inkTextRef> m_textList;

  protected edit const array<inkRectangleRef> m_rectangleList;

  protected edit const array<inkImageRef> m_imageList;

  protected edit const array<inkVideoRef> m_videoList;

  private String m_lastClickedLinkAddress;

  [Default(WebPage, ImageLink))]
  private String HOME_IMAGE_NAME;

  [Default(WebPage, TextLink))]
  private String HOME_TEXT_NAME;

  public final void FillPage(wref<JournalInternetPage> page, ref<JournalManager> journalManager) {
    if(WeakRefToRef(page) != null) {
      FillPageFromJournal(page);
      if(WeakRefToRef(page).IsAdditionallyFilledFromScripts()) {
        FillPageFromScripts(WeakRefToRef(page).GetAddress(), journalManager);
      };
    };
  }

  private final void FillPageFromScripts(String address, ref<JournalManager> journalManager) {
    JournalRequestContext context;
    array<wref<JournalEntry>> entries;
    wref<JournalInternetSite> siteEntry;
    wref<JournalInternetPage> pageEntry;
    String pageAddress;
    ResRef iconAtlasPath;
    CName iconTexturePart;
    Int32 i;
    Int32 slotNumber;
    Int32 MAX_ICONS_COUNT;
    MAX_ICONS_COUNT = 60;
    slotNumber = 1;
    while(slotNumber < MAX_ICONS_COUNT) {
      if(!ClearSlot(slotNumber)) {
      } else {
        slotNumber += 1;
      };
    };
    if(address == "NETdir://ncity.pub") {
      context.stateFilter.active = true;
      journalManager.GetInternetSites(context, entries);
      slotNumber = 1;
      i = 0;
      while(i < Min(Size(entries), MAX_ICONS_COUNT)) {
        siteEntry = RefToWeakRef(Cast(WeakRefToRef(entries[i])));
        if(ToBool(siteEntry)) {
          if(!WeakRefToRef(siteEntry).IsIgnoredAtDesktop()) {
            pageEntry = journalManager.GetMainInternetPage(siteEntry);
            if(WeakRefToRef(pageEntry) != null) {
              pageAddress = WeakRefToRef(pageEntry).GetAddress();
            };
            iconAtlasPath = WeakRefToRef(siteEntry).GetAtlasPath();
            iconTexturePart = WeakRefToRef(siteEntry).GetTexturePart();
            SetSlot(slotNumber, WeakRefToRef(siteEntry).GetShortName(), pageAddress, iconAtlasPath, iconTexturePart);
            slotNumber += 1;
          };
        };
        i += 1;
      };
    };
  }

  private final void SetSlot(Int32 number, String shortName, String pageAddress, ResRef iconAtlasPath, CName iconTexturePart) {
    inkTextRef textRef;
    inkImageRef imageRef;
    textRef = GetTextRef(GetRefName(this.HOME_TEXT_NAME, number));
    if(IsValid(textRef)) {
      SetVisible(textRef, true);
      SetText(textRef, shortName);
      AddLink(textRef, pageAddress);
    };
    imageRef = GetImageRef(GetRefName(this.HOME_IMAGE_NAME, number));
    if(IsValid(imageRef)) {
      SetVisible(imageRef, true);
      SetAtlasResource(imageRef, iconAtlasPath);
      SetTexturePart(imageRef, iconTexturePart);
      AddLink(imageRef, pageAddress);
    };
  }

  private final Bool ClearSlot(Int32 number) {
    inkTextRef textRef;
    inkImageRef imageRef;
    textRef = GetTextRef(GetRefName(this.HOME_TEXT_NAME, number));
    imageRef = GetImageRef(GetRefName(this.HOME_IMAGE_NAME, number));
    if(!IsValid(textRef) && IsValid(imageRef)) {
      return false;
    };
    if(IsValid(textRef)) {
      SetVisible(textRef, false);
    };
    if(IsValid(imageRef)) {
      SetVisible(imageRef, false);
    };
    return true;
  }

  private final CName GetRefName(String prefix, Int32 number) {
    if(number < 9) {
      return StringToName(prefix + "0" + number);
    };
    return StringToName(prefix + number);
  }

  private final inkTextRef GetTextRef(CName instanceName) {
    inkTextRef dummy;
    Int32 i;
    i = 0;
    while(i < Size(this.m_textList)) {
      if(GetName(this.m_textList[i]) == instanceName) {
        return this.m_textList[i];
      };
      i += 1;
    };
    return dummy;
  }

  private final inkImageRef GetImageRef(CName instanceName) {
    inkImageRef dummy;
    Int32 i;
    i = 0;
    while(i < Size(this.m_imageList)) {
      if(GetName(this.m_imageList[i]) == instanceName) {
        return this.m_imageList[i];
      };
      i += 1;
    };
    return dummy;
  }

  private final void FillPageFromJournal(wref<JournalInternetPage> page) {
    array<ref<JournalInternetText>> texts;
    array<ref<JournalInternetRectangle>> rectangles;
    array<ref<JournalInternetImage>> images;
    array<ref<JournalInternetVideo>> videos;
    inkTextRef templateTextRef;
    inkRectangleRef templateRectangleRef;
    inkImageRef templateImageRef;
    inkVideoRef templateVideoRef;
    CName instanceName;
    Int32 i;
    Int32 t;
    Color IGNORED_COLOR;
    IGNORED_COLOR = new Color(255,255,255,0);
    texts = WeakRefToRef(page).GetTexts();
    i = 0;
    while(i < Size(texts)) {
      instanceName = texts[i].GetName();
      t = 0;
      while(t < Size(this.m_textList)) {
        templateTextRef = this.m_textList[t];
        if(GetName(templateTextRef) == instanceName) {
          SetText(templateTextRef, texts[i].GetText());
          if(texts[i].GetColor() != IGNORED_COLOR) {
            SetTintColor(templateTextRef, texts[i].GetColor());
          };
          AddLink(templateTextRef, texts[i]);
        } else {
          t += 1;
        };
      };
      i += 1;
    };
    rectangles = WeakRefToRef(page).GetRectangles();
    i = 0;
    while(i < Size(rectangles)) {
      instanceName = rectangles[i].GetName();
      t = 0;
      while(t < Size(this.m_rectangleList)) {
        templateRectangleRef = this.m_rectangleList[t];
        if(GetName(templateRectangleRef) == instanceName) {
          if(rectangles[i].GetColor() != IGNORED_COLOR) {
            SetTintColor(templateRectangleRef, rectangles[i].GetColor());
          };
          AddLink(templateRectangleRef, rectangles[i]);
        } else {
          t += 1;
        };
      };
      i += 1;
    };
    images = WeakRefToRef(page).GetImages();
    i = 0;
    while(i < Size(images)) {
      instanceName = images[i].GetName();
      t = 0;
      while(t < Size(this.m_imageList)) {
        templateImageRef = this.m_imageList[t];
        if(GetName(templateImageRef) == instanceName) {
          SetAtlasResource(templateImageRef, images[i].GetAtlasPath());
          SetTexturePart(templateImageRef, images[i].GetTexturePart());
          if(rectangles[i].GetColor() != IGNORED_COLOR) {
            SetTintColor(templateImageRef, images[i].GetColor());
          };
          AddLink(templateImageRef, images[i]);
        } else {
          t += 1;
        };
      };
      i += 1;
    };
    videos = WeakRefToRef(page).GetVideos();
    i = 0;
    while(i < Size(videos)) {
      instanceName = videos[i].GetName();
      t = 0;
      while(t < Size(this.m_videoList)) {
        templateVideoRef = this.m_videoList[t];
        if(GetName(templateVideoRef) == instanceName) {
          SetVideoPath(templateVideoRef, videos[i].GetVideoPath());
          Play(templateVideoRef);
          AddLink(templateVideoRef, videos[i]);
        } else {
          t += 1;
        };
      };
      i += 1;
    };
  }

  private final void AddLink(inkWidgetRef widget, String address) {
    String linkAddress;
    ref<LinkController> linkController;
    if(StrLen(address) > 0) {
      if(!IsInteractive(widget)) {
        Log("Widget [" + NameToString(GetName(widget)) + "] is not set as interactive, it cannot be clicked");
      };
      RegisterToCallback(widget, "OnRelease", this, "OnLinkCallback");
      linkController = Cast(WeakRefToRef(GetController(widget)));
      if(ToBool(linkController)) {
        linkController.SetLinkAddress(address);
      } else {
        Log("Missing LinkController for a widget [" + NameToString(GetName(widget)) + "]");
      };
    };
  }

  private final void AddLink(inkWidgetRef widget, ref<JournalInternetBase> baseElement) {
    String linkAddress;
    ref<LinkController> linkController;
    linkAddress = baseElement.GetLinkAddress();
    if(StrLen(linkAddress) > 0) {
      if(!IsInteractive(widget)) {
        Log("Widget [" + NameToString(GetName(widget)) + "] is not set as interactive, it cannot be clicked");
      };
      RegisterToCallback(widget, "OnRelease", this, "OnLinkCallback");
      linkController = Cast(WeakRefToRef(GetController(widget)));
      if(ToBool(linkController)) {
        linkController.SetLinkAddress(linkAddress);
        linkController.SetColors(baseElement.GetColor(), baseElement.GetHoverColor());
      } else {
        Log("Missing LinkController for a widget [" + NameToString(GetName(widget)) + "]");
      };
    };
  }

  private final void OnLinkCallback(ref<inkPointerEvent> e) {
    ref<LinkController> linkController;
    if(e.IsAction("click")) {
      linkController = Cast(WeakRefToRef(WeakRefToRef(e.GetTarget()).GetController()));
      if(ToBool(linkController)) {
        this.m_lastClickedLinkAddress = linkController.GetLinkAddress();
        CallCustomCallback("OnLinkPressed");
      } else {
        Log("Missing link controller");
      };
    };
  }

  public final String GetLastLinkClicked() {
    return this.m_lastClickedLinkAddress;
  }
}

public class WebsiteLoadingSpinner extends inkLogicController {

  protected cb Bool OnInitialize() {
    inkAnimOptions infiniteloop;
    infiniteloop.loopType = inkanimLoopType.Cycle;
    infiniteloop.loopInfinite = true;
    PlayLibraryAnimation("loadingLoop", infiniteloop);
  }
}
