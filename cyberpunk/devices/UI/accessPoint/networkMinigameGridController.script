
public class NetworkMinigameGridController extends inkLogicController {

  protected edit inkWidgetRef m_gridContainer;

  protected edit inkWidgetRef m_horizontalHoverHighlight;

  protected edit inkWidgetRef m_horizontalCurrentHighlight;

  protected edit inkWidgetRef m_verticalHoverHighlight;

  protected edit inkWidgetRef m_verticalCurrentHighlight;

  protected edit Vector2 m_gridVisualOffset;

  protected edit CName m_gridCellLibraryName;

  public array<CellData> m_gridData;

  public CellData m_lastSelected;

  public Vector2 m_currentActivePosition;

  public Bool m_isHorizontalHighlight;

  public CellData m_lastHighlighted;

  private ref<inkAnimProxy> m_animProxy;

  private ref<inkAnimProxy> m_animHighlightProxy;

  private Bool m_firstBoot;

  private Bool m_isHorizontal;

  protected cb Bool OnInitialize() {
    SetTranslation(this.m_gridContainer, this.m_gridVisualOffset);
    this.m_firstBoot = true;
  }

  private final void Clear() {
    ref<inkCompoundWidget> toClear;
    toClear = Cast(WeakRefToRef(Get(this.m_gridContainer)));
    toClear.RemoveAllChildren();
  }

  public final void SetUp(array<CellData> gridData) {
    SetGridData(gridData);
    this.m_isHorizontalHighlight = true;
    this.m_lastSelected = FindCellData(new Vector2(0,0));
    WeakRefToRef(this.m_lastSelected.assignedCell).Consume();
    HighlightCellSet(0, false, true);
  }

  public final void SetGridData(array<CellData> gridData) {
    Int32 i;
    Clear();
    this.m_gridData = gridData;
    i = 0;
    while(i < Size(this.m_gridData)) {
      this.m_gridData[i].assignedCell = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(AddCell(gridData[i])).GetController())));
      i += 1;
    };
  }

  private final wref<inkWidget> AddCell(CellData toAdd) {
    wref<NetworkMinigameGridCellController> cellLogic;
    wref<inkWidget> cell;
    cell = SpawnFromLocal(Get(this.m_gridContainer), this.m_gridCellLibraryName);
    cellLogic = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(cell).GetController())));
    WeakRefToRef(cellLogic).Spawn(toAdd, RefToWeakRef(this));
    return cell;
  }

  public final void SetCurrentActivePosition(Vector2 position, Bool isHorizontal) {
    this.m_currentActivePosition = position;
    this.m_isHorizontalHighlight = isHorizontal;
    HighlightCellSet(Cast(this.m_isHorizontalHighlight ? position.X : position.Y), false, this.m_isHorizontalHighlight);
  }

  public final void SetLastCellSelected(CellData cell) {
    if(!WeakRefToRef(cell.assignedCell).IsConsumed()) {
      this.m_lastSelected = cell;
      CallCustomCallback("OnCellSelected");
    };
  }

  public final CellData GetLastCellSelected() {
    return this.m_lastSelected;
  }

  public final array<CellData> GetGrid() {
    return this.m_gridData;
  }

  public final CellData FindCellData(Vector2 position) {
    Int32 i;
    CellData result;
    i = 0;
    while(i < Size(this.m_gridData)) {
      if(this.m_gridData[i].position.X == position.X && this.m_gridData[i].position.Y == position.Y) {
        result = this.m_gridData[i];
        return result;
      };
      i += 1;
    };
    return result;
  }

  public final void HighlightFromCellHover(Vector2 position) {
    if(IsOnCurrentCellSet(position)) {
      HighlightCellSet(Cast(this.m_isHorizontalHighlight ? position.Y : position.X), true, !this.m_isHorizontalHighlight);
    };
  }

  public final Bool IsOnCurrentCellSet(Vector2 position) {
    return this.m_isHorizontalHighlight ? position.X : position.Y == this.m_isHorizontalHighlight ? this.m_lastSelected.position.X : this.m_lastSelected.position.Y;
  }

  public final void RemoveHighlightFromCellHover() {
    if(ToBool(this.m_lastHighlighted.assignedCell)) {
      WeakRefToRef(this.m_lastHighlighted.assignedCell).SetHighlightStatus(false);
    };
  }

  private final void HighlightCellSet(Int32 index, Bool isHover, Bool isHorizontal) {
    inkWidgetRef highlightToMove;
    Vector2 cellSize;
    Vector2 cellToHighlightPos;
    wref<inkWidget> cellWidget;
    Vector2 newHorizontalPivot;
    Vector2 newVerticalPivor;
    Vector2 fullScale;
    if(Size(this.m_gridData) == 0) {
      return ;
    };
    cellWidget = WeakRefToRef(this.m_gridData[0].assignedCell).GetRootWidget();
    cellSize = WeakRefToRef(cellWidget).GetSize();
    this.m_isHorizontal = isHorizontal;
    if(isHorizontal) {
      highlightToMove = isHover ? this.m_horizontalHoverHighlight : this.m_horizontalCurrentHighlight;
      SetTranslation(highlightToMove, 0, Cast(index) * cellSize.Y + this.m_gridVisualOffset.Y);
      cellToHighlightPos = new Vector2(Cast(index),this.m_lastSelected.position.Y);
    } else {
      highlightToMove = isHover ? this.m_verticalHoverHighlight : this.m_verticalCurrentHighlight;
      SetTranslation(highlightToMove, Cast(index) * cellSize.X + this.m_gridVisualOffset.Y, 0);
      cellToHighlightPos = new Vector2(this.m_lastSelected.position.X,Cast(index));
    };
    if(!isHover) {
      if(ToBool(this.m_lastHighlighted.assignedCell)) {
        WeakRefToRef(this.m_lastHighlighted.assignedCell).SetHighlightStatus(false);
      };
    } else {
      this.m_lastHighlighted = FindCellData(cellToHighlightPos);
      WeakRefToRef(this.m_lastHighlighted.assignedCell).SetHighlightStatus(true);
    };
    SetSize(highlightToMove, cellSize);
    RefreshDimLevels(index, isHorizontal);
    if(!isHover && !this.m_firstBoot) {
      newHorizontalPivot.X = Cast(650 / 5 * index / 650);
      newVerticalPivor.Y = Cast(400 / 5 * index / 400);
      SetRenderTransformPivot(this.m_horizontalHoverHighlight, newHorizontalPivot);
      SetRenderTransformPivot(this.m_verticalHoverHighlight, newVerticalPivor);
      this.m_animProxy.Stop();
      if(isHorizontal) {
        this.m_animProxy = PlayLibraryAnimation("AnimationVerticalToHorizontal");
      } else {
        this.m_animProxy = PlayLibraryAnimation("AnimationHorizontalToVertical");
      };
    };
    if(isHover && !this.m_firstBoot) {
      if(ToBool(this.m_animHighlightProxy) && this.m_animHighlightProxy.IsPlaying()) {
        this.m_animHighlightProxy.Stop();
      };
      fullScale.X = 1;
      fullScale.Y = 1;
      this.m_animHighlightProxy.Stop();
      if(isHorizontal) {
        this.m_animHighlightProxy = PlayLibraryAnimation("horizonal_highlight");
        SetSize(this.m_horizontalCurrentHighlight, cellSize);
        SetScale(this.m_horizontalCurrentHighlight, fullScale);
        SetTranslation(this.m_horizontalCurrentHighlight, 0, Cast(index) * cellSize.Y + this.m_gridVisualOffset.Y);
      } else {
        this.m_animHighlightProxy = PlayLibraryAnimation("vertical_highlight");
        SetSize(this.m_verticalCurrentHighlight, cellSize);
        SetScale(this.m_verticalCurrentHighlight, fullScale);
        SetTranslation(this.m_verticalCurrentHighlight, Cast(index) * cellSize.X + this.m_gridVisualOffset.Y, 0);
      };
    };
    this.m_firstBoot = false;
  }

  public final void RefreshDimLevels(Int32 index, Bool isHorizontal) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_gridData)) {
      WeakRefToRef(this.m_gridData[i].assignedCell).SetElementActive(!IsOnCurrentCellSet(this.m_gridData[i].position));
      i += 1;
    };
  }
}

public class NetworkMinigameGridCellController extends inkButtonController {

  public CellData m_cellData;

  private wref<NetworkMinigameGridController> m_grid;

  protected edit inkWidgetRef m_slotsContainer;

  protected wref<NetworkMinigameElementController> m_slotsContent;

  protected edit CName m_elementLibraryName;

  private HDRColor m_defaultColor;

  protected cb Bool OnInitialize() {
    RegisterToCallback(this.m_slotsContainer, "OnRelease", this, "OnReleaseContainer");
  }

  public final void Spawn(CellData setUp, wref<NetworkMinigameGridController> grid) {
    wref<NetworkMinigameElementController> slotLogic;
    wref<inkWidget> slot;
    this.m_cellData = setUp;
    this.m_grid = grid;
    slot = SpawnFromLocal(Get(this.m_slotsContainer), this.m_elementLibraryName);
    this.m_slotsContent = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(slot).GetController())));
    WeakRefToRef(this.m_slotsContent).SetContent(setUp.element);
    if(this.m_cellData.consumed) {
      WeakRefToRef(this.m_slotsContent).Consume();
    };
    this.m_defaultColor = WeakRefToRef(slot).GetTintColor();
  }

  protected cb Bool OnButtonStateChanged(wref<inkButtonController> controller, inkEButtonState oldState, inkEButtonState newState) {
    switch(newState) {
      case inkEButtonState.Normal:
        WeakRefToRef(this.m_grid).RemoveHighlightFromCellHover();
        break;
      case inkEButtonState.Hover:
        WeakRefToRef(this.m_grid).HighlightFromCellHover(this.m_cellData.position);
        break;
      case inkEButtonState.Press:
        PlaySound("Button", "OnPress");
        break;
      case inkEButtonState.Disabled:
    };
  }

  public final void SetHighlightStatus(Bool isHighlighted) {
    WeakRefToRef(this.m_slotsContent).SetHighlightStatus(isHighlighted && !this.m_cellData.consumed);
  }

  protected cb Bool OnReleaseContainer(ref<inkPointerEvent> e) {
    if(!this.m_cellData.consumed) {
      WeakRefToRef(this.m_grid).SetLastCellSelected(this.m_cellData);
      WeakRefToRef(this.m_grid).RemoveHighlightFromCellHover();
    };
  }

  public final void Consume() {
    this.m_cellData.consumed = true;
    WeakRefToRef(this.m_slotsContent).Consume();
  }

  public final Bool IsConsumed() {
    return this.m_cellData.consumed;
  }

  public final void SetElementActive(Bool isDimmed) {
    wref<inkWidget> root;
    WeakRefToRef(this.m_slotsContent).SetElementActive(isDimmed);
    root = GetRootWidget();
    WeakRefToRef(root).SetInteractive(!isDimmed);
  }
}
