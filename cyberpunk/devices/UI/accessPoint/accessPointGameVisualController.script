
public class NetworkMinigameVisualController extends inkLogicController {

  protected edit inkCompoundRef m_gridContainer;

  protected edit inkVideoRef m_middleVideoContainer;

  protected edit inkWidgetRef m_sidesAnimContainer;

  protected edit ResRef m_sidesLibraryPath;

  protected edit CName m_introAnimationLibraryName;

  protected edit CName m_gridOutroAnimationLibraryName;

  protected edit CName m_endScreenIntroAnimationLibraryName;

  protected edit inkWidgetRef m_programsContainer;

  protected edit inkWidgetRef m_bufferContainer;

  protected edit inkWidgetRef m_endScreenContainer;

  protected edit const array<inkWidgetRef> m_FluffToHideContainer;

  protected edit const array<inkWidgetRef> m_DottedLinesList;

  protected edit inkWidgetRef m_basicAccessContainer;

  protected edit inkWidgetRef m_animationCallbackContainer;

  protected edit inkWidgetRef m_dotMask;

  protected edit Float m_linesToGridOffset;

  protected edit Float m_linesSeparationDistance;

  protected wref<NetworkMinigameAnimationCallbacksTransmitter> m_animationCallback;

  protected wref<NetworkMinigameGridController> m_grid;

  private edit inkWidgetRef m_gridController;

  private edit inkWidgetRef m_programListController;

  private edit inkWidgetRef m_bufferController;

  private edit inkWidgetRef m_endScreenController;

  protected wref<NetworkMinigameProgramListController> m_programList;

  protected wref<NetworkMinigameBufferController> m_buffer;

  protected wref<NetworkMinigameEndScreenController> m_endScreen;

  protected wref<NetworkMinigameBasicProgramController> m_basicAccess;

  protected wref<inkWidget> m_sidesAnim;

  private Int32 m_bufferFillCount;

  private ref<inkAnimProxy> m_bufferAnimProxy;

  private ref<inkAnimDef> m_fillProgress;

  protected cb Bool OnInitialize() {
    this.m_sidesAnim = SpawnFromExternal(Get(this.m_sidesAnimContainer), this.m_sidesLibraryPath, "Root");
    this.m_grid = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(this.m_gridContainer), "Grid")).GetController())));
    this.m_programList = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(Get(this.m_programListController)).GetController())));
    this.m_buffer = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(Get(this.m_bufferController)).GetController())));
    this.m_endScreen = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(Get(this.m_endScreenController)).GetController())));
    this.m_animationCallback = RefToWeakRef(Cast(WeakRefToRef(GetController(this.m_animationCallbackContainer))));
    this.m_basicAccess = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(SpawnFromLocal(Get(this.m_basicAccessContainer), "BasicAccessProgram")).GetController())));
    WeakRefToRef(this.m_grid).RegisterToCallback("OnCellSelected", this, "OnCellSelectCallback");
    WeakRefToRef(this.m_animationCallback).RegisterToCallback("OnStartSidesAnimation", this, "OnStartSidesAnimation");
    WeakRefToRef(this.m_animationCallback).RegisterToCallback("OnStartMinigameBGIntroAnimation", this, "OnStartMinigameBGIntroAnimation");
    WeakRefToRef(this.m_animationCallback).RegisterToCallback("OnIntroAnimationFinished", this, "OnIntroAnimationFinished");
    PlaySound("MiniGame", "OnOpen");
  }

  protected cb Bool OnUninitialize() {
    PlaySound("MiniGame", "OnClose");
  }

  public final void SetUp(NetworkMinigameData data) {
    Vector2 startingScale;
    startingScale.X = 0.0010000000474974513;
    startingScale.Y = 1;
    SetVisible(this.m_gridContainer, true);
    SetVisible(this.m_programsContainer, true);
    SetVisible(this.m_bufferContainer, true);
    SetVisible(this.m_endScreenContainer, false);
    SetVisible(this.m_basicAccessContainer, true);
    SetFluffVisibility(true);
    WeakRefToRef(this.m_sidesAnim).SetVisible(false);
    WeakRefToRef(this.m_grid).SetUp(data.gridData);
    WeakRefToRef(this.m_buffer).Spawn(data.playerBufferSize);
    WeakRefToRef(this.m_programList).Spawn(data.playerPrograms);
    WeakRefToRef(this.m_basicAccess).Spawn(data.basicAccess);
    StartIntroAnimation();
    SetScale(this.m_dotMask, startingScale);
    this.m_bufferFillCount = 0;
    InitializeFluffLines();
  }

  public final void SetGridElementPicked(NewTurnMinigameData newData) {
    CellData selectedCell;
    Vector2 oldScale;
    Vector2 newScale;
    ref<inkAnimScale> scaleInterpolator;
    ProgramData basicData;
    if(newData.doConsume) {
      selectedCell = WeakRefToRef(this.m_grid).FindCellData(newData.position);
      WeakRefToRef(selectedCell.assignedCell).Consume();
    };
    if(newData.doRegenerateGrid) {
      WeakRefToRef(this.m_grid).SetGridData(newData.regeneratedGridData);
    };
    WeakRefToRef(this.m_grid).SetCurrentActivePosition(newData.position, newData.nextHighlightMode == HighlightMode.Row);
    WeakRefToRef(this.m_buffer).SetEntries(newData.newPlayerBufferContent);
    WeakRefToRef(this.m_programList).ProcessListModified(newData.playerProgramsChange, newData.playerProgramsAdded, newData.playerProgramsRemoved);
    basicData = WeakRefToRef(this.m_basicAccess).GetData();
    if(newData.basicAccessCompletionState.isComplete && !basicData.wasCompleted) {
      WeakRefToRef(this.m_basicAccess).ShowCompleted(newData.basicAccessCompletionState.revealLocalizedName);
      WeakRefToRef(this.m_programList).PlaySideBarAnim();
    };
    WeakRefToRef(this.m_programList).UpdatePartialCompletionState(newData.playerProgramsCompletionState);
    WeakRefToRef(this.m_basicAccess).UpdatePartialCompletionState(newData.basicAccessCompletionState);
    this.m_bufferFillCount += 1;
    newScale.X = 0.18000000715255737 * Cast(this.m_bufferFillCount);
    newScale.Y = 1;
    oldScale = GetScale(this.m_dotMask);
    this.m_fillProgress = new inkAnimDef();
    scaleInterpolator = new inkAnimScale();
    scaleInterpolator.SetDuration(0.20000000298023224);
    scaleInterpolator.SetStartScale(oldScale);
    scaleInterpolator.SetEndScale(newScale);
    scaleInterpolator.SetType(inkanimInterpolationType.Linear);
    scaleInterpolator.SetMode(inkanimInterpolationMode.EasyIn);
    this.m_fillProgress.AddInterpolator(scaleInterpolator);
    this.m_bufferAnimProxy = PlayAnimation(this.m_dotMask, this.m_fillProgress);
  }

  public final void SetProgramCompleted(String id, Bool revealLocalizedName)

  public final void ShowEndScreen(EndScreenData endData) {
    ref<inkAnimProxy> animproxy;
    WeakRefToRef(this.m_endScreen).SetUp(endData);
    animproxy = PlayLibraryAnimation(this.m_gridOutroAnimationLibraryName);
    animproxy.RegisterToCallback(inkanimEventType.OnFinish, this, "OnGridOutroOver");
  }

  protected cb Bool OnGridOutroOver(ref<inkAnimProxy> e) {
    inkWidgetRef closeButtonRef;
    wref<inkLogicController> closeButton;
    ref<inkAnimProxy> animproxy;
    animproxy = PlayLibraryAnimation(this.m_endScreenIntroAnimationLibraryName);
    SetVisible(this.m_gridContainer, false);
    SetVisible(this.m_programsContainer, false);
    SetVisible(this.m_bufferContainer, false);
    SetVisible(this.m_basicAccessContainer, false);
    SetVisible(this.m_endScreenContainer, true);
    SetFluffVisibility(false);
    closeButtonRef = WeakRefToRef(this.m_endScreen).GetCloseButtonRef();
    closeButton = RefToWeakRef(WeakRefToRef(GetController(closeButtonRef)));
    WeakRefToRef(closeButton).RegisterToCallback("OnRelease", this, "OnCloseClicked");
  }

  public final CellData GetLastCellSelected() {
    return WeakRefToRef(this.m_grid).GetLastCellSelected();
  }

  public final void Close() {
    ref<inkCompoundWidget> toClear;
    WeakRefToRef(GetRootWidget()).SetVisible(false);
    ClearContainer(Cast(WeakRefToRef(Get(this.m_gridContainer))));
    ClearContainer(Cast(WeakRefToRef(Get(this.m_bufferContainer))));
    ClearContainer(Cast(WeakRefToRef(Get(this.m_programsContainer))));
    ClearContainer(Cast(WeakRefToRef(Get(this.m_endScreenContainer))));
    ClearContainer(Cast(WeakRefToRef(Get(this.m_basicAccessContainer))));
  }

  private final void StartIntroAnimation() {
    Play(this.m_middleVideoContainer);
    WeakRefToRef(this.m_sidesAnim).SetVisible(true);
    PlayLibraryAnimation(this.m_introAnimationLibraryName);
  }

  protected cb Bool OnStartSidesAnimation(wref<inkWidget> e) {
    wref<NetworkMinigameAnimationCallManager> controller;
    controller = RefToWeakRef(Cast(WeakRefToRef(WeakRefToRef(this.m_sidesAnim).GetController())));
    WeakRefToRef(controller).StartReveal();
  }

  protected cb Bool OnStartMinigameBGIntroAnimation(wref<inkWidget> e)

  protected cb Bool OnIntroAnimationFinished(wref<inkWidget> e)

  private final void InitializeFluffLines() {
    Int32 i;
    Float positionalIndex;
    wref<inkLinePattern> lineController;
    array<CellData> gridData;
    wref<inkWidget> cellWidget;
    Vector2 cellSize;
    Vector2 vertexToAdd;
    gridData = WeakRefToRef(this.m_grid).GetGrid();
    cellWidget = WeakRefToRef(gridData[0].assignedCell).GetRootWidget();
    cellSize = WeakRefToRef(cellWidget).GetSize();
    i = 0;
    while(i < Size(this.m_DottedLinesList)) {
      lineController = RefToWeakRef(Cast(WeakRefToRef(Get(this.m_DottedLinesList[i]))));
      positionalIndex = Cast(FloorF(Cast(i) + 1 / 2));
      vertexToAdd = new Vector2(this.m_linesToGridOffset + positionalIndex * cellSize.X - this.m_linesSeparationDistance * positionalIndex,-95);
      WeakRefToRef(lineController).AddVertex(vertexToAdd);
      i += 1;
    };
  }

  private final void SetFluffVisibility(Bool isVisible) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_FluffToHideContainer)) {
      SetVisible(this.m_FluffToHideContainer[i], isVisible);
      i += 1;
    };
  }

  private final void ClearContainer(ref<inkCompoundWidget> toClear) {
    toClear = Cast(WeakRefToRef(Get(this.m_gridContainer)));
    toClear.RemoveAllChildren();
  }

  protected cb Bool OnCellSelectCallback(wref<inkWidget> e) {
    CallCustomCallback("OnCellSelected");
  }

  protected cb Bool OnCloseClicked(ref<inkPointerEvent> e) {
    CallCustomCallback("OnEndClosed");
  }
}

public class NetworkMinigameAnimationCallManager extends inkLogicController {

  public final void StartReveal() {
    PlayLibraryAnimation("reveal");
  }
}

public class NetworkMinigameAnimationCallbacksTransmitter extends inkLogicController {

  protected cb Bool OnStartSidesAnimation() {
    CallCustomCallback("OnStartSidesAnimation");
  }

  protected cb Bool OnStartMinigameBGIntroAnimation() {
    CallCustomCallback("OnStartMinigameBGIntroAnimation");
  }

  protected cb Bool OnIntroAnimationFinished() {
    CallCustomCallback("OnIntroAnimationFinished");
  }
}
