
public class ElevatorInkGameController extends DeviceInkGameControllerBase {

  [Attrib(category, "Widget Refs")]
  private edit inkVerticalPanelRef m_verticalPanel;

  [Attrib(category, "Widget Refs")]
  private edit inkTextRef m_currentFloorTextWidget;

  [Attrib(category, "Widget Refs")]
  private edit inkCanvasRef m_openCloseButtonWidgets;

  [Attrib(category, "Widget Refs")]
  private edit inkFlexRef m_elevatorUpArrowsWidget;

  [Attrib(category, "Widget Refs")]
  private edit inkFlexRef m_elevatorDownArrowsWidget;

  [Attrib(category, "Widget Refs")]
  private edit inkCanvasRef m_waitingStateWidget;

  [Attrib(category, "Widget Refs")]
  private edit inkCanvasRef m_dataScanningWidget;

  [Attrib(category, "Widget Refs")]
  private edit inkCanvasRef m_elevatorStoppedWidget;

  protected Bool m_isPlayerScanned;

  protected Bool m_isPaused;

  protected Bool m_isAuthorized;

  protected ref<inkAnimProxy> m_animProxy;

  protected edit const array<Float> m_buttonSizes;

  private Uint32 m_onChangeFloorListener;

  private Uint32 m_onPlayerScannedListener;

  private Uint32 m_onPausedChangeListener;

  protected void SetupWidgets() {
    if(!this.m_isInitialized) {
      InitializeCurrentFloorName();
    };
  }

  protected final void InitializeCurrentFloorName() {
    ref<LiftDevice> lift;
    lift = GetOwner();
    if(ToBool(lift)) {
      SetCurrentFloorOnUI(lift.GetBlackboard().GetString(lift.GetBlackboardDef().CurrentFloor));
    };
  }

  public void UpdateActionWidgets(array<SActionWidgetPackage> widgetsData) {
    Int32 i;
    ref<ScriptableDeviceAction> action;
    ref<inkWidget> widget;
    Bool isListEmpty;
    Int32 isMoving;
    Bool isPowered;
    Bool isOn;
    Bool isAuthorized;
    inkAnimOptions animOptions;
    ref<inkAnimProxy> animProxy;
    HideActionWidgets();
    SetVisible(this.m_elevatorDownArrowsWidget, false);
    SetVisible(this.m_elevatorUpArrowsWidget, false);
    SetVisible(this.m_waitingStateWidget, false);
    SetVisible(this.m_dataScanningWidget, false);
    SetVisible(this.m_elevatorStoppedWidget, false);
    this.m_animProxy.Pause();
    animOptions.loopType = inkanimLoopType.Cycle;
    animOptions.loopInfinite = true;
    if(ToBool(GetOwner())) {
      isMoving = GetOwner().GetMovingMode();
      isPowered = GetOwner().GetDevicePS().IsPowered();
      isOn = GetOwner().GetDevicePS().IsON();
      isAuthorized = GetOwner().GetDevicePS().IsPlayerAuthorized();
    };
    if(this.m_isPaused) {
      SetVisible(this.m_elevatorStoppedWidget, true);
      this.m_animProxy = PlayLibraryAnimation("elevator_stopped", animOptions);
      return ;
    };
    if(!this.m_isPlayerScanned && isPowered && isOn) {
      this.m_isPlayerScanned = GetOwner().GetBlackboard().GetBool(GetAllBlackboardDefs().ElevatorDeviceBlackboard.isPlayerScanned);
      if(!this.m_isPlayerScanned) {
        SetVisible(this.m_dataScanningWidget, true);
        this.m_animProxy = PlayLibraryAnimation("data_scanning", animOptions);
        return ;
      };
    };
    i = 0;
    while(i < Size(widgetsData)) {
      widget = WeakRefToRef(GetActionWidget(widgetsData[i]));
      if(widget == null) {
        CreateActionWidgetAsync(Get(this.m_verticalPanel), widgetsData[i]);
      } else {
        RefreshFloor(widget, widgetsData[i], i, Size(widgetsData));
      };
      i += 1;
    };
    if(!isAuthorized) {
      Cast(WeakRefToRef(widget.GetController())).SetButtonSize(100, this.m_buttonSizes[0]);
      return ;
    };
    isListEmpty = Size(widgetsData) == 0;
    if(isListEmpty) {
      if(isMoving > 0) {
        SetVisible(this.m_elevatorUpArrowsWidget, true);
        Cast(WeakRefToRef(GetController(this.m_elevatorUpArrowsWidget))).PlayAnimations();
      } else {
        if(isMoving < 0) {
          SetVisible(this.m_elevatorDownArrowsWidget, true);
          Cast(WeakRefToRef(GetController(this.m_elevatorDownArrowsWidget))).PlayAnimations();
        } else {
          if(isMoving == 0) {
            HideActionWidgets();
            SetVisible(this.m_waitingStateWidget, true);
            this.m_animProxy = PlayLibraryAnimation("waiting_for_elevator", animOptions);
          };
        };
      };
    };
  }

  protected final void RefreshFloor(ref<inkWidget> widget, SActionWidgetPackage widgetData, Int32 floorNumber, Int32 maxFloors) {
    if(widgetData.isWidgetInactive) {
      widget.SetState("Inactive");
      widget.SetInteractive(false);
    } else {
      widget.SetState("Default");
      widget.SetInteractive(true);
    };
    InitializeActionWidget(widget, widgetData);
    switch(maxFloors) {
      case 1:
        Cast(WeakRefToRef(widget.GetController())).SetButtonSize(100, this.m_buttonSizes[0]);
        break;
      case 2:
        Cast(WeakRefToRef(widget.GetController())).SetButtonSize(100, this.m_buttonSizes[1]);
        break;
      case 3:
        Cast(WeakRefToRef(widget.GetController())).SetButtonSize(100, this.m_buttonSizes[2]);
        break;
      case 4:
        Cast(WeakRefToRef(widget.GetController())).SetButtonSize(100, this.m_buttonSizes[3]);
        break;
      case 5:
        Cast(WeakRefToRef(widget.GetController())).SetButtonSize(100, this.m_buttonSizes[4]);
    };
    ReorderChild(this.m_verticalPanel, RefToWeakRef(widget), floorNumber);
  }

  protected cb Bool OnActionWidgetSpawned(ref<inkWidget> widget, ref<IScriptable> userData) {
    OnActionWidgetSpawned(widget, userData);
    Refresh(GetOwner().GetDeviceState());
  }

  public final void SetCurrentFloorOnUI(String floorName) {
    SetLetterCase(this.m_currentFloorTextWidget, textLetterCase.UpperCase);
    SetLocalizedTextScript(this.m_currentFloorTextWidget, floorName);
  }

  protected ref<LiftDevice> GetOwner() {
    return Cast(GetOwnerEntity());
  }

  protected void RegisterBlackboardCallbacks(ref<IBlackboard> blackboard) {
    RegisterBlackboardCallbacks(blackboard);
    if(ToBool(blackboard)) {
      this.m_onChangeFloorListener = blackboard.RegisterListenerString(GetOwner().GetBlackboardDef().CurrentFloor, this, "OnChangeFloor");
      this.m_onPlayerScannedListener = blackboard.RegisterListenerBool(GetOwner().GetBlackboardDef().isPlayerScanned, this, "OnPlayerScanned");
      this.m_onPausedChangeListener = blackboard.RegisterListenerBool(GetOwner().GetBlackboardDef().isPaused, this, "OnPausedChange");
    };
  }

  protected void UnRegisterBlackboardCallbacks(ref<IBlackboard> blackboard) {
    UnRegisterBlackboardCallbacks(blackboard);
    if(ToBool(blackboard)) {
      blackboard.UnregisterListenerString(GetOwner().GetBlackboardDef().CurrentFloor, this.m_onChangeFloorListener);
      blackboard.UnregisterListenerBool(GetOwner().GetBlackboardDef().isPlayerScanned, this.m_onPlayerScannedListener);
      blackboard.UnregisterListenerBool(GetOwner().GetBlackboardDef().isPaused, this.m_onPausedChangeListener);
    };
  }

  protected cb Bool OnPlayerScanned(Bool value) {
    if(this.m_isPlayerScanned != value) {
      this.m_isPlayerScanned = value;
      Refresh(GetOwner().GetDeviceState());
    };
  }

  protected cb Bool OnPausedChange(Bool value) {
    if(this.m_isPaused != value) {
      this.m_isPaused = value;
      Refresh(GetOwner().GetDeviceState());
    };
  }

  protected cb Bool OnChangeFloor(String value) {
    SetCurrentFloorOnUI(value);
  }

  public void Refresh(EDeviceStatus state) {
    SetupWidgets();
    Refresh(state);
    RequestActionWidgetsUpdate();
  }
}

public class ElevatorTerminalFakeGameController extends DeviceInkGameControllerBase {

  private edit inkCanvasRef m_elevatorTerminalWidget;

  public void Refresh(EDeviceStatus state) {
    SDeviceWidgetPackage widgetPackage;
    Refresh(state);
    Cast(WeakRefToRef(GetController(this.m_elevatorTerminalWidget))).Initialize(this, widgetPackage);
  }
}
