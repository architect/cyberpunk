
public class CallActionWidgetController extends DeviceActionWidgetControllerBase {

  [Attrib(category, "Widget Refs")]
  protected edit inkTextRef m_statusText;

  [Attrib(category, "Animations")]
  [Default(CallActionWidgetController, calling_animation_maelstrom))]
  protected edit CName m_callingAnimName;

  [Attrib(category, "Animations")]
  [Default(CallActionWidgetController, talking_animation_maelstrom))]
  protected edit CName m_talkingAnimName;

  protected IntercomStatus m_status;

  public void Initialize(ref<DeviceInkGameControllerBase> gameController, SActionWidgetPackage widgetData) {
    Initialize(gameController, widgetData);
    SetLocalizedTextScript(this.m_statusText, "LocKey#279");
  }

  public void FinalizeActionExecution(ref<GameObject> executor, ref<DeviceAction> action) {
    ref<StartCall> contextAction;
    contextAction = Cast(action);
    if(ToBool(contextAction)) {
      CallStarted();
    };
  }

  public final void CallStarted() {
    this.m_status = IntercomStatus.CALLING;
    WeakRefToRef(this.m_targetWidget).SetInteractive(false);
    WeakRefToRef(this.m_targetWidget).SetState("Calling");
    SetLocalizedTextScript(this.m_statusText, "LocKey#2142");
    PlayLibraryAnimation(this.m_callingAnimName);
  }

  public final void CallPickedUp() {
    this.m_status = IntercomStatus.TALKING;
    WeakRefToRef(this.m_targetWidget).SetState("Talking");
    SetLocalizedTextScript(this.m_statusText, "LocKey#312");
    PlayLibraryAnimation(this.m_talkingAnimName);
  }

  public final void CallEnded() {
    this.m_status = IntercomStatus.CALL_ENDED;
    SetLocalizedTextScript(this.m_statusText, "LocKey#2143");
  }

  public final void CallMissed() {
    this.m_status = IntercomStatus.CALL_MISSED;
    SetLocalizedTextScript(this.m_statusText, "LocKey#2145");
  }

  public final void ResetIntercom() {
    this.m_status = IntercomStatus.DEFAULT;
    WeakRefToRef(this.m_targetWidget).SetInteractive(true);
    WeakRefToRef(this.m_targetWidget).SetState("Default");
    SetLocalizedTextScript(this.m_statusText, "LocKey#279");
  }

  protected cb Bool OnHoverOver(ref<inkPointerEvent> e) {
    if(this.m_status == IntercomStatus.DEFAULT) {
      WeakRefToRef(this.m_targetWidget).SetState("Hover");
    };
  }

  protected cb Bool OnHoverOut(ref<inkPointerEvent> e) {
    if(this.m_status == IntercomStatus.DEFAULT) {
      WeakRefToRef(this.m_targetWidget).SetState("Default");
    };
  }
}
