
public class SpeakerController extends ScriptableDC {

  protected const ref<SpeakerControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class SpeakerControllerPS extends ScriptableDeviceComponentPS {

  protected SpeakerSetup m_speakerSetup;

  private CName m_currentValue;

  private CName m_previousValue;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
    if(!IsStringValid(this.m_deviceName)) {
      this.m_deviceName = "LocKey#166";
    };
  }

  protected void Initialize() {
    Initialize();
  }

  protected void GameAttached() {
    this.m_currentValue = GetSoundName(this.m_speakerSetup.m_defaultMusic);
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    return true;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> currentAction;
    currentAction = ActionQuickHackDistraction();
    currentAction.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    Push(actions, currentAction);
    if(IsGlitching() || IsDistracting()) {
      SetActionsInactiveAll(ToScriptRef(actions), "LocKey#7004");
    };
    if(!IsPowered()) {
      SetActionsInactiveAll(ToScriptRef(actions), "LocKey#7013");
    };
    FinalizeGetQuickHackActions(actions, context);
  }

  public final CName GetGlitchSFX() {
    return this.m_speakerSetup.m_glitchSFX;
  }

  public final Bool UseOnlyGlitchSFX() {
    return this.m_speakerSetup.m_useOnlyGlitchSFX;
  }

  public final CName GetCurrentStation() {
    return this.m_currentValue;
  }

  public final void SetCurrentStation(CName station) {
    this.m_currentValue = station;
  }

  public final Float GetRange() {
    return this.m_speakerSetup.m_range;
  }

  protected ref<QuickHackDistraction> ActionQuickHackDistraction() {
    ref<QuickHackDistraction> action;
    action = new QuickHackDistraction();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    action.CreateInteraction();
    action.SetDurationValue(GetDistractionDuration(action));
    return action;
  }

  public EntityNotificationType OnQuickHackDistraction(ref<QuickHackDistraction> evt) {
    EntityNotificationType type;
    type = OnQuickHackDistraction(evt);
    if(type == EntityNotificationType.DoNotNotifyEntity) {
      return type;
    };
    if(IsOFF()) {
      ExecutePSAction(ActionToggleON());
    };
    if(evt.IsStarted()) {
      this.m_previousValue = this.m_currentValue;
      this.m_currentValue = GetSoundName(this.m_speakerSetup.m_distractionMusic);
    } else {
      this.m_currentValue = this.m_previousValue;
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnChangeMusicAction(ref<ChangeMusicAction> evt) {
    ref<ActionNotifier> notifier;
    if(IsPowered()) {
      notifier = new ActionNotifier();
      notifier.SetNone();
      if(IsOFF()) {
        ExecutePSAction(ActionToggleON());
      };
      Notify(notifier, evt);
      return EntityNotificationType.SendThisEventToEntity;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final ref<MusicSettings> CreateDeafeningMusic() {
    ref<PlayRadio> music;
    music = new PlayRadio();
    music.SetStatusEffect(ESoundStatusEffects.DEAFENED);
    music.SetSoundName(this.m_speakerSetup.m_distractionMusic);
    return music;
  }

  protected final CName GetSoundName(ERadioStationList music) {
    if(music == ERadioStationList.AGGRO_INDUSTRIAL) {
      return "radio_station_02_aggro_ind";
    };
    if(music == ERadioStationList.ELECTRO_INDUSTRIAL) {
      return "radio_station_03_elec_ind";
    };
    if(music == ERadioStationList.HIP_HOP) {
      return "radio_station_04_hiphop";
    };
    if(music == ERadioStationList.AGGRO_TECHNO) {
      return "radio_station_07_aggro_techno";
    };
    if(music == ERadioStationList.DOWNTEMPO) {
      return "radio_station_09_downtempo";
    };
    if(music == ERadioStationList.ATTITUDE_ROCK) {
      return "radio_station_01_att_rock";
    };
    if(music == ERadioStationList.POP) {
      return "radio_station_05_pop";
    };
    if(music == ERadioStationList.LATINO) {
      return "radio_station_10_latino";
    };
    if(music == ERadioStationList.METAL) {
      return "radio_station_11_metal";
    };
    return "station_none";
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.RadioDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.RadioDeviceBackground";
  }
}
