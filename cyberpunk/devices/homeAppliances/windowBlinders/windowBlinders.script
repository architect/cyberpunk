
public class WindowBlinders extends InteractiveDevice {

  private ref<AnimFeature_SimpleDevice> m_animFeature;

  private CName m_workspotSideName;

  protected ref<gameLightComponent> m_portalLight;

  protected edit const array<CName> m_sideTriggerNames;

  protected array<ref<TriggerComponent>> m_triggerComponents;

  public const CName GetDeviceStateClass() {
    return "WindowBlindersReplicatedState";
  }

  protected void ApplyReplicatedState(ref<DeviceReplicatedState> state) {
    const ref<WindowBlindersReplicatedState> blindersState;
    ApplyReplicatedState(state);
    blindersState = Cast(state);
    ApplyAnimState(blindersState.m_isOpen, blindersState.m_isTilted);
  }

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    Int32 i;
    OnRequestComponents(ri);
    i = 0;
    while(i < Size(this.m_sideTriggerNames)) {
      RequestComponent(ri, this.m_sideTriggerNames[i], "TriggerComponent", true);
      i += 1;
    };
    RequestComponent(ri, "portal_light", "gameLightComponent", false);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    Int32 i;
    OnTakeControl(ri);
    i = 0;
    while(i < Size(this.m_sideTriggerNames)) {
      Push(this.m_triggerComponents, Cast(GetComponent(ri, this.m_sideTriggerNames[i])));
      i += 1;
    };
    this.m_controller = Cast(GetComponent(ri, "controller"));
    this.m_portalLight = Cast(GetComponent(ri, "portal_light"));
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    UpdateDeviceState();
  }

  private const ref<WindowBlindersController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<WindowBlindersControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected Bool UpdateDeviceState(Bool isDelayed?) {
    if(UpdateDeviceState(isDelayed)) {
      UpdateAnimState();
      return true;
    };
    return false;
  }

  protected cb Bool OnToggleOpen(ref<ToggleOpen> evt) {
    UpdateDeviceState();
    PlaySoundEvent(this, "dev_doors_hidden_stop");
  }

  protected cb Bool OnQuickHackToggleOpen(ref<QuickHackToggleOpen> evt) {
    UpdateDeviceState();
    PlaySoundEvent(this, "dev_doors_hidden_stop");
  }

  protected cb Bool OnToggleTilt(ref<ToggleTiltBlinders> evt) {
    UpdateDeviceState();
    PlaySoundEvent(this, "dev_doors_hidden_stop");
  }

  protected cb Bool OnActionEngineering(ref<ActionEngineering> evt) {
    UpdateDeviceState();
  }

  protected cb Bool OnActionDemolition(ref<ActionDemolition> evt) {
    UpdateDeviceState();
    EnterWorkspot();
  }

  protected final void EnterWorkspot() {
    ref<WorkspotGameSystem> workspotSystem;
    ref<IBlackboard> playerStateMachineBlackboard;
    playerStateMachineBlackboard = GetBlackboardSystem(GetGame()).GetLocalInstanced(GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject().GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, true);
    workspotSystem = GetWorkspotSystem(GetGame());
    CheckCurrentSide();
    workspotSystem.PlayInDevice(this, GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject(), "lockedCamera", "playerWorkspot" + this.m_workspotSideName, "deviceWorkspot" + this.m_workspotSideName, "blinders", 0.5, WorkspotSlidingBehaviour.PlayAtResourcePosition);
  }

  protected cb Bool OnQuestStatusChange(ref<PSChangedEvent> evt) {
    UpdateDeviceState();
    PlaySoundEvent(this, "dev_doors_hidden_stop");
  }

  private final void UpdateAnimState() {
    ref<WindowBlindersReplicatedState> replicatedState;
    replicatedState = Cast(GetServerState());
    if(ToBool(replicatedState)) {
      replicatedState.m_isOpen = GetDevicePS().IsOpen();
      replicatedState.m_isTilted = GetDevicePS().IsTilted();
    };
    ApplyAnimState(GetDevicePS().IsOpen(), GetDevicePS().IsTilted());
    if(ToBool(this.m_portalLight) && this.m_portalLight.IsEnabled()) {
      this.m_portalLight.ToggleLight(GetDevicePS().IsOpen());
    };
    if(!this.m_wasAnimationFastForwarded) {
      FastForwardAnimations();
    };
  }

  private final void ApplyAnimState(Bool isOpen, Bool isTilted) {
    if(!ToBool(this.m_animFeature)) {
      this.m_animFeature = new AnimFeature_SimpleDevice();
    };
    this.m_animFeature.isOpen = isOpen;
    this.m_animFeature.isOpenLeft = isTilted;
    ApplyFeature(this, "DeviceWindowBlinders", this.m_animFeature);
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.OpenPath;
  }

  protected final void CheckCurrentSide() {
    String finalName;
    ref<GameObject> activator;
    Int32 i;
    Int32 j;
    array<ref<Entity>> overlappingEntities;
    i = 0;
    while(i < Size(this.m_triggerComponents)) {
      overlappingEntities = this.m_triggerComponents[i].GetOverlappingEntities();
      j = 0;
      while(j < Size(overlappingEntities)) {
        if(Cast(overlappingEntities[j]).IsPlayer()) {
          finalName = "Side" + ToString(i + 1);
          this.m_workspotSideName = StringToName(finalName);
        };
        j += 1;
      };
      i += 1;
    };
    if(this.m_workspotSideName == "") {
      this.m_workspotSideName = "Side1";
    };
  }
}
