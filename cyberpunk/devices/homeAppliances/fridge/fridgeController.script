
public class ToggleOpenFridge extends ActionBool {

  public final void SetProperties(Bool isOpen) {
    this.actionName = "ToggleOpenFridge";
    this.prop = SetUpProperty_Bool("Open", isOpen, "LocKey#273", "LocKey#274");
  }
}

public class FridgeController extends ScriptableDC {

  protected const ref<FridgeControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class FridgeControllerPS extends ScriptableDeviceComponentPS {

  private persistent Bool m_isOpen;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
    if(!IsStringValid(this.m_deviceName)) {
      this.m_deviceName = "LocKey#79";
    };
  }

  protected void Initialize() {
    Initialize();
  }

  protected ref<ToggleOpenFridge> ActionToggleOpenFridge() {
    ref<ToggleOpenFridge> action;
    action = new ToggleOpenFridge();
    action.clearanceLevel = GetToggleOpenClearance();
    action.SetUp(this);
    action.SetProperties(this.m_isOpen);
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetActions(actions, context);
    if(!IsUserAuthorized(WeakRefToRef(context.processInitiatorObject).GetEntityID())) {
      return false;
    };
    if(this.m_deviceState == EDeviceStatus.DISABLED) {
      return false;
    };
    if(IsInRange(context.clearance, GetToggleOpenClearance())) {
      Push(actions, ActionToggleOpenFridge());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  public final EntityNotificationType OnOpen(ref<ToggleOpenFridge> evt) {
    this.m_isOpen = !this.m_isOpen;
    evt.prop.first = ToVariant(this.m_isOpen);
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public const ref<Clearance> GetClearance() {
    return CreateClearance(2, 2);
  }

  public final Bool IsOpen() {
    return this.m_isOpen;
  }
}
