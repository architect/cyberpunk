
public class JukeboxControllerPS extends ScriptableDeviceComponentPS {

  protected JukeboxSetup m_jukeboxSetup;

  protected array<RadioStationsMap> m_stations;

  protected persistent Int32 m_activeStation;

  [Default(JukeboxControllerPS, true))]
  protected Bool m_isPlaying;

  protected void Initialize() {
    Initialize();
    InitializeStations();
  }

  protected void GameAttached() {
    this.m_activeStation = ToInt(this.m_jukeboxSetup.m_startingStation);
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetActions(actions, context);
    if(context.requestType == gamedeviceRequestType.Remote) {
      return false;
    };
    if(IsDefaultConditionMet(this, context)) {
      Push(actions, ActionTogglePlay());
    };
    if(IsDefaultConditionMet(this, context)) {
      Push(actions, ActionPreviousStation());
      Push(actions, ActionNextStation());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    return true;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> action;
    action = ActionQuickHackDistraction();
    action.SetInactiveWithReason(!IsDistracting(), "LocKey#7004");
    Push(actions, action);
    FinalizeGetQuickHackActions(actions, context);
  }

  public final const TweakDBID GetPaymentRecordID() {
    return this.m_jukeboxSetup.m_paymentRecordID;
  }

  protected final ref<TogglePlay> ActionTogglePlay() {
    ref<TogglePlay> action;
    action = new TogglePlay();
    action.SetUp(this);
    action.SetProperties(!this.m_isPlaying);
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    return action;
  }

  protected final ref<PreviousStation> ActionPreviousStation() {
    ref<PreviousStation> action;
    action = new PreviousStation();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.SetExecutor(RefToWeakRef(GetPlayer(GetGameInstance())));
    action.SetInkWidgetTweakDBID("DevicesUIDefinitions.JukeboxPreviousActionWidget");
    action.CreateActionWidgetPackage();
    if(IsValid(GetPaymentRecordID())) {
      action.SetObjectActionID(GetPaymentRecordID());
    };
    return action;
  }

  public final ref<NextStation> ActionNextStation() {
    ref<NextStation> action;
    action = new NextStation();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.SetExecutor(RefToWeakRef(GetPlayer(GetGameInstance())));
    action.SetInkWidgetTweakDBID("DevicesUIDefinitions.JukeboxNextActionWidget");
    action.CreateActionWidgetPackage();
    if(IsValid(GetPaymentRecordID())) {
      action.SetObjectActionID(GetPaymentRecordID());
    };
    return action;
  }

  protected ref<QuickHackDistraction> ActionQuickHackDistraction() {
    ref<QuickHackDistraction> action;
    action = ActionQuickHackDistraction();
    action.SetDurationValue(GetDistractionDuration(action));
    action.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    return action;
  }

  public final Int32 GetActiveStationIndex() {
    return this.m_activeStation;
  }

  public final CName GetActiveStationSoundEvent() {
    return this.m_stations[this.m_activeStation].soundEvent;
  }

  public final CName GetGlitchSFX() {
    return this.m_jukeboxSetup.m_glitchSFX;
  }

  public final Bool IsPlaying() {
    return this.m_isPlaying;
  }

  public final EntityNotificationType OnTogglePlay(ref<TogglePlay> evt) {
    if(!IsON()) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    this.m_isPlaying = FromVariant(evt.prop.first);
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnNextStation(ref<NextStation> evt) {
    if(!IsON() || !evt.CanPayCost(GetPlayer(GetGameInstance()))) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    if(this.m_activeStation + 1 == Size(this.m_stations)) {
      this.m_activeStation = 0;
    } else {
      this.m_activeStation += 1;
    };
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnPreviousStation(ref<PreviousStation> evt) {
    if(!IsON() || !evt.CanPayCost(GetPlayer(GetGameInstance()))) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    if(this.m_activeStation - 1 < 0) {
      this.m_activeStation = Size(this.m_stations) - 1;
    } else {
      this.m_activeStation -= 1;
    };
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public EntityNotificationType OnQuickHackDistraction(ref<QuickHackDistraction> evt) {
    EntityNotificationType type;
    type = OnQuickHackDistraction(evt);
    if(type == EntityNotificationType.DoNotNotifyEntity) {
      return type;
    };
    if(evt.IsStarted()) {
      if(IsOFF()) {
        ExecutePSAction(ActionSetDeviceON());
      };
      ExecutePSAction(ActionNextStation());
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  private final void InitializeStations() {
    Push(this.m_stations, CreateStation("radio_station_02_aggro_ind", "Gameplay-Devices-Radio-RadioStationAggroIndie"));
    Push(this.m_stations, CreateStation("radio_station_03_elec_ind", "Gameplay-Devices-Radio-RadioStationElectroIndie"));
    Push(this.m_stations, CreateStation("radio_station_04_hiphop", "Gameplay-Devices-Radio-RadioStationHipHop"));
    Push(this.m_stations, CreateStation("radio_station_07_aggro_techno", "Gameplay-Devices-Radio-RadioStationAggroTechno"));
    Push(this.m_stations, CreateStation("radio_station_09_downtempo", "Gameplay-Devices-Radio-RadioStationDownTempo"));
    Push(this.m_stations, CreateStation("radio_station_01_att_rock", "Gameplay-Devices-Radio-RadioStationAttRock"));
    Push(this.m_stations, CreateStation("radio_station_05_pop", "Gameplay-Devices-Radio-RadioStationPop"));
    Push(this.m_stations, CreateStation("radio_station_10_latino", "Gameplay-Devices-Radio-RadioStationLatino"));
    Push(this.m_stations, CreateStation("radio_station_11_metal", "Gameplay-Devices-Radio-RadioStationMetal"));
  }

  private final RadioStationsMap CreateStation(CName SoundEvt, String ChannelName) {
    RadioStationsMap station;
    station.soundEvent = SoundEvt;
    station.channelName = ChannelName;
    return station;
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.RadioDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.RadioDeviceBackground";
  }

  public const ref<JukeboxBlackboardDef> GetBlackboardDef() {
    return GetAllBlackboardDefs().JukeboxBlackboard;
  }
}
