
public class Radio extends InteractiveDevice {

  private array<RadioStationsMap> m_stations;

  private Int32 m_startingStation;

  private Bool m_isInteractive;

  private Bool m_isShortGlitchActive;

  private DelayID m_shortGlitchDelayID;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "audio", "soundComponent", false);
    RequestComponent(ri, "radio_ui", "worlduiWidgetComponent", false);
    OnRequestComponents(ri);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    OnTakeControl(ri);
    this.m_uiComponent = RefToWeakRef(Cast(GetComponent(ri, "radio_ui")));
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    if(IsUIdirty() && this.m_isInsideLogicArea) {
      RefreshUI();
    };
  }

  protected cb Bool OnToggleON(ref<ToggleON> evt) {
    OnToggleON(evt);
    TriggerArreaEffectDistraction(GetDefaultDistractionAreaEffectData(), WeakRefToRef(evt.GetExecutor()));
  }

  protected cb Bool OnTogglePower(ref<TogglePower> evt) {
    OnTogglePower(evt);
    TriggerArreaEffectDistraction(GetDefaultDistractionAreaEffectData(), WeakRefToRef(evt.GetExecutor()));
  }

  public Bool ResavePersistentData(ref<PersistentState> ps) {
    ref<RadioControllerPS> psDevice;
    MediaResaveData mediaData;
    RadioResaveData radioData;
    ResavePersistentData(ps);
    mediaData.m_mediaDeviceData.m_initialStation = this.m_startingStation;
    mediaData.m_mediaDeviceData.m_amountOfStations = Size(this.m_stations);
    mediaData.m_mediaDeviceData.m_activeChannelName = this.m_stations[this.m_startingStation].channelName;
    mediaData.m_mediaDeviceData.m_isInteractive = this.m_isInteractive;
    radioData.m_mediaResaveData = mediaData;
    radioData.m_stations = this.m_stations;
    psDevice.PushResaveData(radioData);
    return true;
  }

  protected void RestoreDeviceState() {
    RestoreDeviceState();
  }

  public const ref<RadioControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected const ref<RadioController> GetController() {
    return Cast(this.m_controller);
  }

  private final void PlayGivenStation() {
    RadioStationsMap station;
    Int32 stationIndex;
    Bool isMetal;
    stationIndex = GetDevicePS().GetActiveStationIndex();
    station = GetDevicePS().GetStationByIndex(stationIndex);
    AudioSwitch(this, "radio_station", station.soundEvent, "radio");
    isMetal = station.soundEvent == "radio_station_11_metal" ? true : false;
    MetalItUp(isMetal);
  }

  private final void MetalItUp(Bool isMetal) {
    if(GetDevicePS().GetDurabilityType() != EDeviceDurabilityType.INVULNERABLE) {
      if(isMetal) {
        GetDevicePS().SetDurabilityType(EDeviceDurabilityType.INDESTRUCTIBLE);
      } else {
        GetDevicePS().SetDurabilityType(EDeviceDurabilityType.DESTRUCTIBLE);
      };
    };
  }

  protected cb Bool OnNextStation(ref<NextStation> evt) {
    PlayGivenStation();
    UpdateDeviceState();
    RefreshUI();
    TriggerArreaEffectDistraction(GetDefaultDistractionAreaEffectData(), WeakRefToRef(evt.GetExecutor()));
  }

  protected cb Bool OnPreviousStation(ref<PreviousStation> evt) {
    PlayGivenStation();
    UpdateDeviceState();
    RefreshUI();
    TriggerArreaEffectDistraction(GetDefaultDistractionAreaEffectData(), WeakRefToRef(evt.GetExecutor()));
  }

  protected cb Bool OnQuestSetChannel(ref<QuestSetChannel> evt) {
    PlayGivenStation();
    RefreshUI();
  }

  protected cb Bool OnSpiderbotDistraction(ref<SpiderbotDistraction> evt) {
    OrderSpiderbot();
  }

  protected cb Bool OnSpiderbotOrderCompletedEvent(ref<SpiderbotOrderCompletedEvent> evt) {
    SendSetIsSpiderbotInteractionOrderedEvent(false);
    GetActivityLogSystem(GetGame()).AddLog("SPIDERBOT HAS FINISHED ACTIVATING THE DEVICE ... ");
    GetDevicePS().CauseDistraction();
  }

  protected void TurnOnDevice() {
    TurnOnDevice();
    if(ToBool(this.m_uiComponent)) {
      WeakRefToRef(this.m_uiComponent).Toggle(true);
    };
    PlayGivenStation();
    UpdateDeviceState();
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Start, "radio_idle");
    RefreshUI();
  }

  protected void TurnOffDevice() {
    TurnOffDevice();
    AudioSwitch(this, "radio_station", "station_none", "radio");
    UpdateDeviceState();
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.BreakLoop, "radio_idle");
    RefreshUI();
  }

  protected void CutPower() {
    CutPower();
    if(ToBool(this.m_uiComponent)) {
      WeakRefToRef(this.m_uiComponent).Toggle(false);
    };
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.BreakLoop, "radio_idle");
    UpdateDeviceState();
  }

  protected void DeactivateDevice() {
    DeactivateDevice();
    if(ToBool(this.m_uiComponent)) {
      WeakRefToRef(this.m_uiComponent).Toggle(false);
    };
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Kill, "radio_idle");
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.Distract;
  }

  protected void StartGlitching(EGlitchState glitchState, Float intensity?) {
    ref<AdvertGlitchEvent> evt;
    if(intensity == 0) {
      intensity = 1;
    };
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(intensity);
    QueueEvent(evt);
    UpdateDeviceState();
    PlaySound(this, GetDevicePS().GetGlitchSFX());
  }

  protected void StopGlitching() {
    ref<AdvertGlitchEvent> evt;
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(0);
    QueueEvent(evt);
  }

  private final void StartShortGlitch() {
    ref<StopShortGlitchEvent> evt;
    if(GetDevicePS().IsGlitching()) {
      return ;
    };
    if(!this.m_isShortGlitchActive) {
      evt = new StopShortGlitchEvent();
      StartGlitching(EGlitchState.DEFAULT, 1);
      this.m_shortGlitchDelayID = GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), evt, 0.25);
      this.m_isShortGlitchActive = true;
    };
  }

  protected cb Bool OnStopShortGlitch(ref<StopShortGlitchEvent> evt) {
    this.m_isShortGlitchActive = false;
    if(!GetDevicePS().IsGlitching()) {
      StopGlitching();
    };
  }
}
