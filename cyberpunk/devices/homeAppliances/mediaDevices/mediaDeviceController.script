
public class MediaDeviceController extends ScriptableDC {

  protected const ref<MediaDeviceControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class MediaDeviceControllerPS extends ScriptableDeviceComponentPS {

  protected Int32 m_previousStation;

  protected String m_activeChannelName;

  protected persistent Bool m_dataInitialized;

  protected persistent Int32 m_amountOfStations;

  protected persistent Int32 m_activeStation;

  protected final const ref<MediaDeviceStatus> ActionMediaDeviceStatus() {
    ref<MediaDeviceStatus> action;
    action = new MediaDeviceStatus();
    action.clearanceLevel = GetStatusClearance();
    action.SetUp(this);
    action.SetProperties(this);
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public ref<NextStation> ActionNextStation() {
    ref<NextStation> action;
    action = new NextStation();
    action.clearanceLevel = GetInteractiveClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    action.CreateInteraction();
    return action;
  }

  public ref<PreviousStation> ActionPreviousStation() {
    ref<PreviousStation> action;
    action = new PreviousStation();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    action.CreateInteraction();
    return action;
  }

  protected final ref<QuestSetChannel> ActionQuestSetChannel() {
    ref<QuestSetChannel> action;
    action = new QuestSetChannel();
    action.clearanceLevel = GetQuestClearance();
    action.SetUp(this);
    action.SetProperties(this.m_activeStation);
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetActions(actions, context);
    if(context.requestType == gamedeviceRequestType.Direct && !IsInteractive()) {
      return false;
    };
    if(!IsUserAuthorized(WeakRefToRef(context.processInitiatorObject).GetEntityID())) {
      return false;
    };
    if(context.requestType == gamedeviceRequestType.Remote) {
      return false;
    };
    if(IsDefaultConditionMet(this, context)) {
      Push(actions, ActionToggleON());
    };
    if(IsDefaultConditionMet(this, context) && context.requestType == gamedeviceRequestType.External) {
      Push(actions, ActionMediaDeviceStatus());
    };
    if(IsDefaultConditionMet(this, context)) {
      Push(actions, ActionPreviousStation());
      Push(actions, ActionNextStation());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  public void GetQuestActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    GetQuestActions(outActions, context);
    Push(outActions, ActionQuestEnableInteraction());
    Push(outActions, ActionQuestDisableInteraction());
    if(IsInRange(context.clearance, ActionQuestForcePower().clearanceLevel)) {
      Push(outActions, ActionQuestSetChannel());
    };
  }

  public EntityNotificationType OnNextStation(ref<NextStation> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    if(IsUnpowered() || IsDisabled() || !IsON()) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    this.m_previousStation = this.m_activeStation;
    if(this.m_activeStation + 1 == this.m_amountOfStations) {
      this.m_activeStation = 0;
    } else {
      this.m_activeStation += 1;
    };
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public EntityNotificationType OnPreviousStation(ref<PreviousStation> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    if(IsUnpowered() || IsDisabled() || !IsON()) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    this.m_previousStation = this.m_activeStation;
    if(this.m_activeStation - 1 < 0) {
      this.m_activeStation = this.m_amountOfStations - 1;
    } else {
      this.m_activeStation -= 1;
    };
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnQuestSetChannel(ref<QuestSetChannel> evt) {
    Int32 stationIDX;
    array<ref<DeviceActionProperty>> prop;
    prop = evt.GetProperties();
    GetProperty_Int(prop[0], stationIDX);
    SetActiveStationIndex(stationIDX);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final ref<QuestEnableInteraction> ActionQuestEnableInteraction() {
    ref<QuestEnableInteraction> action;
    action = new QuestEnableInteraction();
    action.clearanceLevel = GetQuestClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public EntityNotificationType OnQuestEnableInteraction(ref<QuestEnableInteraction> evt) {
    this.m_isInteractive = true;
    UseNotifier(evt);
    return EntityNotificationType.SendPSChangedEventToEntity;
  }

  protected final ref<QuestDisableInteraction> ActionQuestDisableInteraction() {
    ref<QuestDisableInteraction> action;
    action = new QuestDisableInteraction();
    action.clearanceLevel = GetQuestClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  public EntityNotificationType OnQuestDisableInteraction(ref<QuestDisableInteraction> evt) {
    this.m_isInteractive = false;
    UseNotifier(evt);
    return EntityNotificationType.SendPSChangedEventToEntity;
  }

  private final void GetQuickHackDistractionActions()

  public const ref<inkTextParams> GetDeviceStatusTextData() {
    ref<inkTextParams> textData;
    String channelName;
    Int32 channelAsNumber;
    textData = GetDeviceStatusTextData();
    if(IsON()) {
      channelName = GetActiveStationName();
      channelAsNumber = StringToInt(channelName);
      if(ToBool(textData)) {
        if(channelAsNumber > 0) {
          textData.AddString("TEXT_SECONDARY", channelName);
        } else {
          textData.AddLocalizedString("TEXT_SECONDARY", channelName);
        };
      };
    } else {
      if(ToBool(textData)) {
        textData.AddString("TEXT_SECONDARY", "");
      };
    };
    return textData;
  }

  protected ref<ThumbnailUI> ActionThumbnailUI() {
    ref<ThumbnailUI> action;
    action = ActionThumbnailUI();
    if(IsON()) {
      action.CreateThumbnailWidgetPackage("LocKey#42211");
    };
    return action;
  }

  public const ref<MediaDeviceStatus> GetDeviceStatusAction() {
    return ActionMediaDeviceStatus();
  }

  public final const Int32 GetPreviousStationIndex() {
    return this.m_previousStation;
  }

  public Int32 GetActiveStationIndex() {
    return this.m_activeStation;
  }

  public final const String GetActiveStationName() {
    return this.m_activeChannelName;
  }

  public final void SetActiveStationIndex(Int32 stationIDX) {
    this.m_previousStation = this.m_activeStation;
    this.m_activeStation = stationIDX;
  }

  public final void PassChannelName(String channelName) {
    this.m_activeChannelName = channelName;
  }

  public final void PushPersistentData(MediaDeviceData data) {
    if(IsInitialized()) {
      return ;
    };
    this.m_previousStation = -1;
    this.m_activeStation = data.m_initialStation;
    this.m_amountOfStations = data.m_amountOfStations;
    this.m_activeChannelName = data.m_activeChannelName;
    this.m_isInteractive = data.m_isInteractive;
  }
}
