
public class MediaDeviceStatus extends BaseDeviceStatus {

  public void SetProperties(ref<ScriptableDeviceComponentPS> deviceRef) {
    Int32 statusValue;
    SetProperties(deviceRef);
    statusValue = ToInt(deviceRef.GetDeviceState());
    this.prop.second = ToVariant(Cast(deviceRef).GetActiveStationName());
    this.prop = SetUpProperty_MediaStatus("MediaStatus", statusValue, Cast(deviceRef).GetActiveStationName());
  }

  public const String GetCurrentDisplayString() {
    String str;
    String channelName;
    Int32 baseStateValue;
    if(GetProperty_MediaStatus(this.prop, baseStateValue, channelName)) {
      if(baseStateValue > 1) {
        baseStateValue = 1;
      };
      if(baseStateValue > 0) {
        str = "LocKey#2256";
        return str;
      };
      return GetCurrentDisplayString();
    };
    Log("MediaDeviceStatus / Problem with acquiring station name");
    return str;
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device) && IsClearanceValid(context.clearance)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    return IsAvailable(device);
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    return IsClearanceValid(clearance);
  }

  public String GetTweakDBChoiceRecord() {
    return "wrong_action";
  }
}

public class NextStation extends ActionBool {

  public final void SetProperties() {
    this.actionName = "NextStation";
    this.prop = SetUpProperty_Bool("Next Station", true, "LocKey#252", "LocKey#252");
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device) && IsClearanceValid(context.clearance)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    return BasicAvailabilityTest(device);
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    if(IsInRange(clearance, GetInteractiveClearance())) {
      return true;
    };
    return false;
  }

  public String GetTweakDBChoiceRecord() {
    return "Next";
  }
}

public class PreviousStation extends ActionBool {

  public final void SetProperties() {
    this.actionName = "PreviousStation";
    this.prop = SetUpProperty_Bool("Previous Station", true, "LocKey#253", "LocKey#253");
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device) && IsClearanceValid(context.clearance)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    return BasicAvailabilityTest(device);
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    if(IsInRange(clearance, GetInteractiveClearance())) {
      return true;
    };
    return false;
  }

  public String GetTweakDBChoiceRecord() {
    return "Previous";
  }
}

public class QuestToggleInteractivity extends ActionBool {

  public final void SetProperties(Bool enable) {
    if(enable) {
      this.actionName = "QuestEnableInteractivity";
    } else {
      this.actionName = "QuestDisableInteractivity";
    };
    this.prop = SetUpProperty_Bool(this.actionName, enable, "QuestToggleInteractivity", "QuestToggleInteractivity");
  }
}

public class QuestMuteSounds extends ActionBool {

  public final void SetProperties(Bool mute) {
    if(mute) {
      this.actionName = "QuestMuteSounds";
    } else {
      this.actionName = "QuestUnMuteSounds";
    };
    this.prop = SetUpProperty_Bool(this.actionName, mute, "QuestMuteSounds", "QuestMuteSounds");
  }
}

public class QuestSetChannel extends ActionInt {

  public final void SetProperties(Int32 channel) {
    this.actionName = "SetChannel";
    this.prop = SetUpProperty_Int("SetChannel", channel);
  }
}

public class QuickHackDistraction extends ActionBool {

  public final void SetProperties() {
    this.actionName = "QuickHackDistraction";
    this.prop = SetUpProperty_Bool("QuickHackDistraction", true, "LocKey#6990", "LocKey#6990");
  }

  public final void SetProperties(CName interaction) {
    this.actionName = interaction;
    this.prop = SetUpProperty_Bool(interaction, true, interaction, interaction);
  }

  public const wref<ChoiceCaptionIconPart_Record> GetInteractionIcon() {
    return RefToWeakRef(GetChoiceCaptionIconPartRecord("ChoiceCaptionParts.DistractIcon"));
  }
}

public class GlitchScreen extends ActionBool {

  public final void SetProperties(Bool isGlitching, TweakDBID actionID, TweakDBID programID) {
    CName currentDisplayName;
    SetAttachedProgramTweakDBID(programID);
    SetObjectActionID(actionID);
    currentDisplayName = StringToName(LocKeyToString(WeakRefToRef(WeakRefToRef(GetObjectActionRecord()).ObjectActionUI()).Caption()));
    this.prop = SetUpProperty_Bool("GlitchScreen", isGlitching, currentDisplayName, currentDisplayName);
  }

  public const wref<ChoiceCaptionIconPart_Record> GetInteractionIcon() {
    return GetInteractionIcon();
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device) && IsClearanceValid(context.clearance)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    if(device.IsDisabled()) {
      return false;
    };
    if(device.IsUnpowered()) {
      return false;
    };
    if(device.IsDeviceSecured()) {
      return false;
    };
    if(!device.IsON()) {
      return false;
    };
    return true;
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    if(IsInRange(clearance, GetInteractiveClearance())) {
      return true;
    };
    return false;
  }

  public String GetTweakDBChoiceRecord() {
    return GetTweakDBChoiceRecord();
  }

  public TweakDBID GetAttachedProgramTweakDBID() {
    if(IsValid(this.m_attachedProgram)) {
      return this.m_attachedProgram;
    };
    return "QuickHack.DeviceSuicideHack";
  }
}
