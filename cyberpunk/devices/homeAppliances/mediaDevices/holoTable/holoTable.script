
public class HoloTable extends InteractiveDevice {

  public array<ref<MeshComponent>> m_meshTable;

  public edit Int32 componentCounter;

  public Int32 m_currentMesh;

  public ref<MeshComponent> m_glitchMesh;

  public const ref<HoloTableControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected const ref<MediaDeviceController> GetController() {
    return Cast(this.m_controller);
  }

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    Int32 i;
    String compName;
    i = 0;
    while(i < this.componentCounter) {
      compName = "mesh" + i;
      RequestComponent(ri, StringToName(compName), "MeshComponent", false);
      i += 1;
    };
    RequestComponent(ri, "GlitchMesh", "MeshComponent", false);
    OnRequestComponents(ri);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    Int32 i;
    String compName;
    i = 0;
    while(i < this.componentCounter) {
      compName = "mesh" + i;
      Push(this.m_meshTable, Cast(GetComponent(ri, StringToName(compName))));
      i += 1;
    };
    this.m_glitchMesh = Cast(GetComponent(ri, "GlitchMesh"));
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    GetDevicePS().SetMeshesAmount(this.componentCounter);
    SetActiveMesh();
  }

  protected final void SetActiveMesh() {
    ref<StimBroadcasterComponent> broadcaster;
    if(GetDeviceState() == EDeviceStatus.ON) {
      this.m_meshTable[this.m_currentMesh].Toggle(false);
      this.m_meshTable[GetDevicePS().GetActiveStationIndex()].Toggle(true);
      this.m_currentMesh = GetDevicePS().GetActiveStationIndex();
    } else {
      TurnOffMeshes();
    };
  }

  protected final void TurnOffMeshes() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_meshTable)) {
      this.m_meshTable[GetDevicePS().GetActiveStationIndex()].Toggle(false);
      i += 1;
    };
    StopEffectEvent(this, "light_cone_dust");
  }

  protected cb Bool OnNextStation(ref<NextStation> evt) {
    SetActiveMesh();
    UpdateDeviceState();
  }

  protected cb Bool OnPreviousStation(ref<PreviousStation> evt) {
    SetActiveMesh();
    UpdateDeviceState();
  }

  protected void TurnOnDevice() {
    StartEffectEvent(this, "light_cone_dust");
    SetActiveMesh();
    UpdateDeviceState();
  }

  protected void TurnOffDevice() {
    TurnOffDevice();
    TurnOffMeshes();
    UpdateDeviceState();
  }

  protected void CutPower() {
    CutPower();
    TurnOffMeshes();
    UpdateDeviceState();
  }

  protected void DeactivateDevice() {
    DeactivateDevice();
    TurnOffMeshes();
  }

  protected const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.Distract;
  }

  protected void StartGlitching(EGlitchState glitchState, Float intensity?) {
    TurnOffMeshes();
    if(ToBool(this.m_glitchMesh)) {
      this.m_glitchMesh.Toggle(true);
    };
  }

  protected void StopGlitching() {
    if(ToBool(this.m_glitchMesh)) {
      this.m_glitchMesh.Toggle(false);
    };
    if(GetDevicePS().IsON()) {
      SetActiveMesh();
    };
  }
}
