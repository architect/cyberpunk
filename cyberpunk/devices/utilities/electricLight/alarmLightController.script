
public class AlarmLightController extends ScriptableDC {

  protected const ref<AlarmLightControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class AlarmLightControllerPS extends BasicDistractionDeviceControllerPS {

  [Default(AlarmLightControllerPS, ESecuritySystemState.SAFE))]
  protected ESecuritySystemState m_securityAlarmState;

  public final const ESecuritySystemState GetAlarmState() {
    return this.m_securityAlarmState;
  }

  public EntityNotificationType OnQuestForceSecuritySystemSafe(ref<QuestForceSecuritySystemSafe> evt) {
    this.m_securityAlarmState = ESecuritySystemState.SAFE;
    return EntityNotificationType.SendThisEventToEntity;
  }

  public EntityNotificationType OnQuestForceSecuritySystemArmed(ref<QuestForceSecuritySystemArmed> evt) {
    this.m_securityAlarmState = ESecuritySystemState.COMBAT;
    WakeUpDevice();
    return EntityNotificationType.SendThisEventToEntity;
  }

  public EntityNotificationType OnSecurityAlarmBreachResponse(ref<SecurityAlarmBreachResponse> evt) {
    this.m_securityAlarmState = evt.GetSecurityState();
    if(this.m_securityAlarmState == ESecuritySystemState.COMBAT) {
      WakeUpDevice();
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public EntityNotificationType OnSecuritySystemOutput(ref<SecuritySystemOutput> evt) {
    OnSecuritySystemOutput(evt);
    this.m_securityAlarmState = evt.GetCachedSecurityState();
    if(this.m_securityAlarmState == ESecuritySystemState.COMBAT) {
      WakeUpDevice();
    };
    return EntityNotificationType.SendThisEventToEntity;
  }
}
