
public class ElectricLight extends Device {

  private array<ref<gameLightComponent>> m_lightComponents;

  private const array<LightPreset> m_lightDefinitions;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_lightDefinitions)) {
      if(IsNameValid(this.m_lightDefinitions[i].lightSourcesName)) {
        RequestComponent(ri, this.m_lightDefinitions[i].lightSourcesName, "gameLightComponent", true);
      };
      i += 1;
    };
    OnRequestComponents(ri);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_lightDefinitions)) {
      Push(this.m_lightComponents, Cast(GetComponent(ri, this.m_lightDefinitions[i].lightSourcesName)));
      ApplyPreset(this.m_lightComponents[i], this.m_lightDefinitions[i].preset);
      i += 1;
    };
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  private const ref<ElectricLightController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<ElectricLightControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected const Bool ShouldRegisterToHUD() {
    if(this.m_forceRegisterInHudManager) {
      return true;
    };
    return false;
  }

  private final void ApplyPreset(ref<gameLightComponent> light, TweakDBID preset) {
    ref<EnvLight_Record> envLightRecord;
    Color presetColor;
    gameLightSettings lightSettings;
    envLightRecord = GetEnvLightRecord(preset);
    light.SetTemperature(envLightRecord.Temperature());
    lightSettings = light.GetDefaultSettings();
    lightSettings.intensity = envLightRecord.Intensity();
    lightSettings.radius = envLightRecord.Radius();
    CreateColorFromIntArray(envLightRecord.Color(), presetColor);
    lightSettings.color = presetColor;
    light.SetParameters(lightSettings);
  }

  private final Bool CreateColorFromIntArray(array<Int32> ints, out Color color) {
    if(Size(ints) != 3) {
      if(!IsFinal()) {
        LogDevices(this, "ElectricLight \ CreateColorFromIntArray \ WRONG PRESET PROVIDED - Light for " + GetDeviceName() + " has wrong color", ELogType.WARNING);
      };
      color = new Color(255,0,0,255);
      return false;
    };
    color.Red = Cast(ints[0]);
    color.Green = Cast(ints[1]);
    color.Blue = Cast(ints[2]);
    color.Alpha = 255;
    return true;
  }

  protected void CutPower() {
    TurnOffLights();
  }

  protected void RestorePower() {
    RestoreDeviceState();
  }

  protected void TurnOnDevice() {
    TurnOnLights();
  }

  protected void TurnOffDevice() {
    TurnOffLights();
  }

  private final void TurnOnLights() {
    ref<ToggleLightEvent> evt;
    if(GetDevicePS().GetDurabilityState() == EDeviceDurabilityState.BROKEN) {
      return ;
    };
    if(GetDevicePS().GetDurabilityType() == EDeviceDurabilityType.INDESTRUCTIBLE) {
      StartEffectEvent(this, "light_on_destr");
    };
    evt = new ToggleLightEvent();
    evt.toggle = true;
    QueueEvent(evt);
  }

  private final void TurnOffLights() {
    ref<ToggleLightEvent> evt;
    evt = new ToggleLightEvent();
    if(GetDevicePS().GetDurabilityType() == EDeviceDurabilityType.INDESTRUCTIBLE) {
      StopEffectEvent(this, "light_on_destr");
    };
    evt.toggle = false;
    QueueEvent(evt);
  }

  protected cb Bool OnHitEvent(ref<gameHitEvent> hit) {
    if(IsBullet(hit.attackData.GetAttackType()) || IsExplosion(hit.attackData.GetAttackType())) {
      ReactToHit(hit);
    };
  }

  protected void ReactToHit(ref<gameHitEvent> hit) {
    if(GetDevicePS().GetDurabilityType() == EDeviceDurabilityType.INDESTRUCTIBLE) {
      StartEffectEvent(this, "light_on_destr");
    };
    if(GetDevicePS().GetDurabilityType() == EDeviceDurabilityType.DESTRUCTIBLE) {
      GetDevicePS().SetDurabilityState(EDeviceDurabilityState.BROKEN);
      TurnOffDevice();
    };
  }

  protected cb Bool OnEMPHitEvent(ref<EMPHitEvent> evt) {
    ref<EMPEnded> empEnded;
    if(IsActive()) {
      ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Start, "emp_hit");
      ExecuteAction(GetDevicePS().ActionSetDeviceUnpowered());
      empEnded = new EMPEnded();
      GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), empEnded, evt.lifetime);
    };
  }

  protected cb Bool OnEMPEnded(ref<EMPEnded> evt) {
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.BreakLoop, "emp_hit");
    ExecuteAction(GetDevicePS().ActionSetDevicePowered());
  }

  protected cb Bool OnPhysicalDestructionEvent(ref<PhysicalDestructionEvent> evt) {
    GetDevicePS().ForceDisableDevice();
  }

  protected void ActivateDevice() {
    TurnOnDevice();
  }

  protected Bool IncludeLightsInVisibilityBoundsScript() {
    return true;
  }

  public const Bool IsGameplayRelevant() {
    return false;
  }

  public const Bool ShouldSendGameAttachedEventToPS() {
    return false;
  }
}
