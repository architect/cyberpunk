
public class C4Controller extends ExplosiveDeviceController {

  protected const ref<C4ControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class C4ControllerPS extends ExplosiveDeviceControllerPS {

  [Default(C4ControllerPS, C4))]
  private persistent CName m_itemTweakDBString;

  protected void GameAttached()

  private final ref<ActivateC4> ActionActivate() {
    ref<ActivateC4> action;
    action = new ActivateC4();
    action.clearanceLevel = 1;
    action.SetUp(this);
    action.SetProperties();
    action.CreateInteraction();
    return action;
  }

  private final ref<DeactivateC4> ActionDeactivate() {
    ref<DeactivateC4> action;
    action = new DeactivateC4();
    action.clearanceLevel = 1;
    action.SetUp(this);
    action.SetProperties();
    action.CreateInteraction();
    return action;
  }

  private final ref<DetonateC4> ActionDetonate() {
    ref<DetonateC4> action;
    action = new DetonateC4();
    action.clearanceLevel = 1;
    action.SetUp(this);
    action.SetProperties();
    action.CreateInteraction();
    return action;
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetActions(actions, context);
    if(IsOFF() && GetTransactionSystem(GetGameInstance()).HasItem(GetPlayerMainObject(), GetInventoryItemID())) {
      Push(actions, ActionActivate());
    };
    if(IsON()) {
      Push(actions, ActionDeactivate());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    return true;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    ref<ScriptableDeviceAction> currentAction;
    currentAction = ActionDetonate();
    currentAction.SetObjectActionID("DeviceAction.OverloadClassHack");
    currentAction.SetInactiveWithReason(IsON(), "LocKey#7005");
    Push(outActions, currentAction);
    FinalizeGetQuickHackActions(outActions, context);
  }

  protected void PushInactiveInteractionChoice(GetActionsContext context, out array<InteractionChoice> choices) {
    ref<ActivateC4> baseAction;
    InteractionChoice inactiveChoice;
    baseAction = ActionActivate();
    inactiveChoice.choiceMetaData.tweakDBName = baseAction.GetTweakDBChoiceRecord();
    inactiveChoice.caption = "DEBUG: Reason Unhandled";
    SetType(inactiveChoice.choiceMetaData.type, gameinteractionsChoiceType.Inactive);
    if(IsOFF()) {
      inactiveChoice.caption = "[NEED C4]";
      Push(choices, inactiveChoice);
      return ;
    };
  }

  public final EntityNotificationType OnActivateC4(ref<ActivateC4> evt) {
    wref<GameObject> executor;
    executor = evt.GetExecutor();
    if(ToBool(executor)) {
      GetTransactionSystem(GetGameInstance()).RemoveItem(WeakRefToRef(executor), GetInventoryItemID(), 1);
    };
    UseNotifier(evt);
    SetDeviceState(EDeviceStatus.ON);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnDeactivateC4(ref<DeactivateC4> evt) {
    wref<GameObject> executor;
    executor = evt.GetExecutor();
    if(ToBool(executor)) {
      GetTransactionSystem(GetGameInstance()).GiveItem(WeakRefToRef(executor), GetInventoryItemID(), 1);
    };
    UseNotifier(evt);
    SetDeviceState(EDeviceStatus.OFF);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnDetonateC4(ref<DetonateC4> evt) {
    SetDeviceState(EDeviceStatus.DISABLED);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final const CName GetItemTweakDBString() {
    return this.m_itemTweakDBString;
  }

  public final const ItemID GetInventoryItemID() {
    return FromTDBID(Create("Items." + ToString(this.m_itemTweakDBString)));
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.ExplosionDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.ExplosionDeviceBackground";
  }
}

public class ActivateC4 extends ActionBool {

  public ItemID itemID;

  public final void SetProperties() {
    this.actionName = "ActivateC4";
    this.prop = SetUpProperty_Bool(this.actionName, true, "LocKey#562", "LocKey#562");
  }

  public String GetTweakDBChoiceRecord() {
    if(IsValid(this.m_objectActionID)) {
      return GetTweakDBChoiceRecord();
    };
    return "PlaceC4";
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    if(device.IsOFF()) {
      return true;
    };
    return false;
  }
}

public class DeactivateC4 extends ActionBool {

  public ItemID itemID;

  public final void SetProperties() {
    this.actionName = "DeactivateC4";
    this.prop = SetUpProperty_Bool(this.actionName, true, "LocKey#563", "LocKey#563");
  }

  public String GetTweakDBChoiceRecord() {
    return "DeactivateC4";
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    if(device.IsON()) {
      return true;
    };
    return false;
  }
}

public class DetonateC4 extends ActionBool {

  public ItemID itemID;

  public final void SetProperties() {
    this.actionName = "DetonateC4";
    this.prop = SetUpProperty_Bool(this.actionName, true, "LocKey#564", "LocKey#564");
  }

  public String GetTweakDBChoiceRecord() {
    if(IsValid(this.m_objectActionID)) {
      return GetTweakDBChoiceRecord();
    };
    return "DetonateC4";
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    if(device.IsON()) {
      return true;
    };
    return false;
  }
}
