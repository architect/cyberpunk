
public class RetractableAd extends BaseAnimatedDevice {

  protected ref<OffMeshConnectionComponent> m_offMeshConnection;

  protected ref<TriggerComponent> m_areaComponent;

  protected ref<IComponent> m_advUiComponent;

  protected Bool m_isPartOfTheTrap;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    OnRequestComponents(ri);
    RequestComponent(ri, "offMeshConnection", "OffMeshConnectionComponent", false);
    RequestComponent(ri, "ui", "AdvertisementWidgetComponent", false);
    RequestComponent(ri, "area", "TriggerComponent", false);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    OnTakeControl(ri);
    this.m_offMeshConnection = Cast(GetComponent(ri, "offMeshConnection"));
    this.m_advUiComponent = GetComponent(ri, "ui");
    this.m_areaComponent = Cast(GetComponent(ri, "area"));
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected cb Bool OnActivateDevice(ref<ActivateDevice> evt) {
    OnActivateDevice(evt);
    if(GetDevicePS().IsConnected()) {
      this.m_isPartOfTheTrap = true;
    };
  }

  protected void ToggleState() {
    array<ref<Entity>> count;
    count = GetEntitiesInArea();
    if(GetDevicePS().IsActive() && Size(count) > 0) {
      ApplyImpulse(count);
    };
    if(this.m_animationType == EAnimationType.REGULAR) {
      Animate();
    } else {
      if(this.m_animationType == EAnimationType.TRANSFORM) {
        TransformAnimate();
      };
    };
  }

  protected void TransformAnimate() {
    ref<gameTransformAnimationPlayEvent> playEvent;
    playEvent = new gameTransformAnimationPlayEvent();
    playEvent.looping = false;
    playEvent.timesPlayed = 1;
    if(this.m_isPartOfTheTrap) {
      playEvent.timeScale = 0.699999988079071;
    } else {
      if(GetDevicePS().Randomize()) {
        playEvent.timeScale = RandRangeF(0.800000011920929, 1.2000000476837158);
      } else {
        playEvent.timeScale = 1;
      };
    };
    if(GetDevicePS().IsNotActive()) {
      playEvent.animationName = "closing";
      this.m_gameplayRoleComponent.ToggleMappin(gamedataMappinVariant.HazardWarningVariant, false);
      ToggleOffMeshConnection(true);
      ToggleLights(false);
    } else {
      playEvent.animationName = "opening";
      this.m_gameplayRoleComponent.ToggleMappin(gamedataMappinVariant.HazardWarningVariant, true, true);
      ToggleOffMeshConnection(false);
      ToggleLights(true);
    };
    QueueEvent(playEvent);
  }

  protected final void ToggleOffMeshConnection(Bool toggle) {
    if(ToBool(this.m_offMeshConnection)) {
      if(toggle) {
        this.m_offMeshConnection.EnableOffMeshConnection();
        this.m_offMeshConnection.EnableForPlayer();
      } else {
        this.m_offMeshConnection.DisableOffMeshConnection();
        this.m_offMeshConnection.DisableForPlayer();
      };
    };
  }

  protected final void ToggleLights(Bool toggle) {
    ref<ToggleLightEvent> lightEvent;
    lightEvent = new ToggleLightEvent();
    lightEvent.toggle = toggle;
    QueueEvent(lightEvent);
  }

  protected cb Bool OnPhysicalDestructionEvent(ref<PhysicalDestructionEvent> evt) {
    if(evt.componentName == "destructible_glass" && evt.levelOfDestruction == 1) {
      this.m_gameplayRoleComponent.ToggleMappin(gamedataMappinVariant.HazardWarningVariant, false);
      DisableTrap();
    } else {
      GetDevicePS().ForceDisableDevice();
    };
  }

  protected final void DisableTrap() {
    ref<RoadBlockTrapControllerPS> trap;
    if(GetDevicePS().IsConnected()) {
      trap = GetDevicePS().GetTrapController();
      trap.ForceDisableDevice();
    };
  }

  public final const array<ref<Entity>> GetEntitiesInArea() {
    return this.m_areaComponent.GetOverlappingEntities();
  }

  protected void StartGlitching(EGlitchState glitchState, Float intensity?) {
    ref<AdvertGlitchEvent> evt;
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(1);
    QueueEvent(evt);
    UpdateDeviceState();
  }

  protected void StopGlitching() {
    ref<AdvertGlitchEvent> evt;
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(0);
    QueueEvent(evt);
  }

  private final void ApplyImpulse(array<ref<Entity>> activators) {
    ref<PSMImpulse> ev;
    Int32 i;
    Float impulseDirection;
    Vector4 impulseInLocalSpace;
    Vector4 devicePosition;
    Vector4 activatorPosition;
    Vector4 direction;
    Vector4 deviceForward;
    Float impulsMultiplier;
    if(this.m_isPartOfTheTrap) {
      return ;
    };
    devicePosition = GetWorldPosition();
    deviceForward = GetWorldForward();
    impulsMultiplier = GetFloat("player.externalImpules.bilboard");
    i = 0;
    while(i < Size(activators)) {
      ev = new PSMImpulse();
      ev.id = "impulse";
      activatorPosition = activators[i].GetWorldPosition();
      direction = activatorPosition - devicePosition;
      if(Dot(direction, deviceForward) < 0) {
        deviceForward = -deviceForward;
      };
      impulseInLocalSpace = deviceForward * impulsMultiplier;
      ev.impulse = impulseInLocalSpace;
      activators[i].QueueEvent(ev);
      i += 1;
    };
  }

  protected cb Bool OnAreaEnter(ref<AreaEnteredEvent> evt) {
    wref<GameObject> activator;
    if(GetDevicePS().IsDisabled() || GetDevicePS().IsUnpowered()) {
      return false;
    };
    activator = RefToWeakRef(Cast(GetEntity(evt.activator)));
    if(WeakRefToRef(activator).IsPlayer() && GetDevicePS().IsActivated() && evt.componentName == "areaTop") {
      GetPersistencySystem(GetGame()).QueuePSEvent(GetDevicePS().GetID(), GetDevicePS().GetClassName(), GetDevicePS().ActionDeactivateDevice());
    };
  }

  private const ref<RetractableAdController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<RetractableAdControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }
}
