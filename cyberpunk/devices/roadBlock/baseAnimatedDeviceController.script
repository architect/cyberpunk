
public class BaseAnimatedDeviceController extends ScriptableDC {

  protected const ref<BaseAnimatedDeviceControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class BaseAnimatedDeviceControllerPS extends ScriptableDeviceComponentPS {

  protected persistent Bool m_isActive;

  protected Bool m_hasInteraction;

  protected Bool m_randomizeAnimationTime;

  [Attrib(customEditor, "TweakDBGroupInheritance;Interactions.InteractionChoice;Interactions.MountChoice")]
  protected TweakDBID m_nameForActivation;

  [Attrib(customEditor, "TweakDBGroupInheritance;Interactions.InteractionChoice;Interactions.MountChoice")]
  protected TweakDBID m_nameForDeactivation;

  public final const Bool IsActive() {
    return this.m_isActive;
  }

  public final const Bool IsNotActive() {
    return !this.m_isActive;
  }

  public final const Bool Randomize() {
    return this.m_randomizeAnimationTime;
  }

  protected void GameAttached() {
    if(this.m_isActive) {
      this.m_activationState = EActivationState.ACTIVATED;
    } else {
      this.m_activationState = EActivationState.DEACTIVATED;
    };
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetActions(actions, context);
    if(this.m_hasInteraction || context.requestType == gamedeviceRequestType.External) {
      Push(actions, ActionToggleActivate());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    return true;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> currentAction;
    currentAction = ActionQuickHackToggleActivate();
    currentAction.SetObjectActionID("DeviceAction.ToggleStateClassHack");
    Push(actions, currentAction);
    FinalizeGetQuickHackActions(actions, context);
  }

  public void GetQuestActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    GetQuestActions(actions, context);
    Push(actions, ActionQuestForceActivate());
    Push(actions, ActionQuestForceDeactivate());
  }

  protected ref<ToggleActivate> ActionToggleActivate() {
    ref<ToggleActivate> action;
    action = new ToggleActivate();
    action.clearanceLevel = GetToggleOpenClearance();
    action.SetUp(this);
    action.SetProperties(IsActive(), this.m_nameForActivation, this.m_nameForDeactivation);
    action.AddDeviceName(GetDeviceName());
    action.CreateInteraction();
    action.CreateActionWidgetPackage();
    return action;
  }

  protected ref<QuickHackToggleActivate> ActionQuickHackToggleActivate() {
    ref<QuickHackToggleActivate> action;
    action = new QuickHackToggleActivate();
    action.clearanceLevel = GetToggleOpenClearance();
    action.SetUp(this);
    action.SetProperties(IsActive(), this.m_nameForActivation, this.m_nameForDeactivation);
    action.AddDeviceName(GetDeviceName());
    if(IsActive()) {
      action.CreateInteraction(this.m_nameForActivation);
    } else {
      action.CreateInteraction(this.m_nameForDeactivation);
    };
    action.CreateActionWidgetPackage();
    return action;
  }

  public EntityNotificationType OnToggleActivate(ref<ToggleActivate> evt) {
    OnToggleActivate(evt);
    this.m_isActive = IsActive() ? false : true;
    NotifyParents();
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnQuickHackToggleActivate(ref<QuickHackToggleActivate> evt) {
    this.m_isActive = IsActive() ? false : true;
    this.m_activationState = IsActive() ? EActivationState.ACTIVATED : EActivationState.DEACTIVATED;
    NotifyParents();
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected EntityNotificationType OnActivateDevice(ref<ActivateDevice> evt) {
    if(this.m_activationState != EActivationState.ACTIVATED && IsON()) {
      OnActivateDevice(evt);
      this.m_isActive = true;
      NotifyParents();
      return EntityNotificationType.SendThisEventToEntity;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnDeactivateDevice(ref<DeactivateDevice> evt) {
    if(this.m_activationState != EActivationState.DEACTIVATED && IsON()) {
      OnDeactivateDevice(evt);
      this.m_isActive = false;
      NotifyParents();
      return EntityNotificationType.SendThisEventToEntity;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }
}
