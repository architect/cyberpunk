
public class JumpPod extends GameObject {

  private ref<IVisualComponent> m_activationLight;

  private ref<IComponent> m_activationTrigger;

  public edit Float impulseForward;

  public edit Float impulseRight;

  public edit Float impulseUp;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "activationLight", "entLightComponent", true);
    RequestComponent(ri, "activator", "gameStaticTriggerAreaComponent", true);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.m_activationLight = Cast(GetComponent(ri, "activationLight"));
    this.m_activationTrigger = GetComponent(ri, "activator");
    this.m_activationLight.Toggle(false);
  }

  protected cb Bool OnAreaEnter(ref<AreaEnteredEvent> trigger) {
    ApplyImpulse(trigger.activator);
  }

  protected cb Bool OnAreaExit(ref<AreaExitedEvent> trigger)

  private final void ApplyImpulse(EntityGameInterface activator) {
    ref<PSMImpulse> ev;
    Vector4 impulseInLocalSpace;
    ev = new PSMImpulse();
    ev.id = "impulse";
    impulseInLocalSpace = GetWorldForward() * this.impulseForward;
    impulseInLocalSpace += GetWorldRight() * this.impulseRight;
    impulseInLocalSpace += GetWorldUp() * this.impulseUp;
    ev.impulse = impulseInLocalSpace;
    GetEntity(activator).QueueEvent(ev);
  }
}
