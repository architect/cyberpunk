
public class DataTermController extends ScriptableDC {

  protected const ref<DataTermControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class FastTravelDeviceAction extends ActionBool {

  private ref<FastTravelPointData> m_fastTravelPointData;

  public final void SetProperties(ref<FastTravelPointData> data) {
    CName displayName;
    this.m_fastTravelPointData = data;
    displayName = StringToName(GetFastTravelPointRecord(data.GetPointRecord()).DisplayName());
    this.actionName = "FastTravel";
    this.prop = SetUpProperty_Bool("FastTravel", true, displayName, displayName);
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device) && IsClearanceValid(context.clearance)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    if(device.IsUnpowered() || device.IsDisabled()) {
      return false;
    };
    return true;
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    if(IsInRange(clearance, GetQuestClearance())) {
      return true;
    };
    return false;
  }

  public void CreateActionWidgetPackage(array<ref<DeviceAction>> actions?) {
    String widgetName;
    widgetName = GetFastTravelPointRecord(this.m_fastTravelPointData.GetPointRecord()).DisplayName();
    CreateActionWidgetPackage(actions);
    this.m_actionWidgetPackage.widgetName = widgetName;
  }

  public final const ref<FastTravelPointData> GetFastTravelPointData() {
    return this.m_fastTravelPointData;
  }
}

public class OpenWorldMapDeviceAction extends ActionBool {

  private ref<FastTravelPointData> m_fastTravelPointData;

  public final void SetProperties() {
    this.actionName = "OpenWorldMapDeviceAction";
    this.prop = SetUpProperty_Bool("OpenWorldMapDeviceAction", true, "LocKey#2057", "LocKey#2057");
  }

  public final static Bool IsDefaultConditionMet(ref<ScriptableDeviceComponentPS> device, GetActionsContext context) {
    if(IsAvailable(device) && IsClearanceValid(context.clearance)) {
      return true;
    };
    return false;
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    if(device.IsUnpowered() || device.IsDisabled()) {
      return false;
    };
    return true;
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    if(IsInRange(clearance, GetQuestClearance())) {
      return true;
    };
    return false;
  }

  public String GetTweakDBChoiceRecord() {
    return "SellectDestination";
  }
}

public class DataTermControllerPS extends ScriptableDeviceComponentPS {

  private ref<FastTravelPointData> m_linkedFastTravelPoint;

  [Default(DataTermControllerPS, EFastTravelTriggerType.Manual))]
  private edit EFastTravelTriggerType m_triggerType;

  public final const EFastTravelTriggerType GetFastravelTriggerType() {
    return this.m_triggerType;
  }

  public Bool GetActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    if(!GetActions(actions, context)) {
      return false;
    };
    if(ToBool(context.processInitiatorObject) && !IsUserAuthorized(WeakRefToRef(context.processInitiatorObject).GetEntityID())) {
      return false;
    };
    if(GetFastravelTriggerType() == EFastTravelTriggerType.Manual && GetFastTravelSystem().IsFastTravelEnabled()) {
      Push(actions, ActionOpenWorldMap());
    };
    SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }

  public const ref<DataTermDeviceBlackboardDef> GetBlackboardDef() {
    return GetAllBlackboardDefs().DataTermDeviceBlackboard;
  }

  protected final ref<FastTravelDeviceAction> ActionFastTravel(ref<FastTravelPointData> actionData) {
    ref<FastTravelDeviceAction> action;
    action = new FastTravelDeviceAction();
    action.SetUp(this);
    action.SetProperties(actionData);
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    return action;
  }

  public EntityNotificationType OnFastTravelAction(ref<FastTravelDeviceAction> evt) {
    ref<BaseDeviceStatus> cachedStatus;
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    cachedStatus = GetDeviceStatusAction();
    if(IsUnpowered() || IsDisabled()) {
      return SendActionFailedEvent(evt, evt.GetRequesterID(), "Unpowered or Disabled");
    };
    if(!IsFinal()) {
      LogActionDetails(evt, cachedStatus);
    };
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final ref<OpenWorldMapDeviceAction> ActionOpenWorldMap() {
    ref<OpenWorldMapDeviceAction> action;
    action = new OpenWorldMapDeviceAction();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    action.CreateInteraction();
    return action;
  }

  public EntityNotificationType OnOpenWorldMapAction(ref<OpenWorldMapDeviceAction> evt) {
    ref<BaseDeviceStatus> cachedStatus;
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    cachedStatus = GetDeviceStatusAction();
    if(IsUnpowered() || IsDisabled()) {
      return SendActionFailedEvent(evt, evt.GetRequesterID(), "Unpowered or Disabled");
    };
    if(!IsFinal()) {
      LogActionDetails(evt, cachedStatus);
    };
    Notify(notifier, evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final void SetLinkedFastTravelPoint(ref<FastTravelPointData> point) {
    this.m_linkedFastTravelPoint = point;
  }

  private final ref<FastTravelSystem> GetFastTravelSystem() {
    return Cast(GetScriptableSystemsContainer(GetGameInstance()).Get("FastTravelSystem"));
  }
}
