
public class BasicDistractionDeviceController extends ScriptableDC {

  protected const ref<BasicDistractionDeviceControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class BasicDistractionDeviceControllerPS extends ScriptableDeviceComponentPS {

  [Attrib(category, "Distraction properties")]
  [Default(BasicDistractionDeviceControllerPS, EPlaystyleType.NETRUNNER))]
  protected edit EPlaystyleType m_distractorType;

  protected inline ref<EngDemoContainer> m_basicDistractionDeviceSkillChecks;

  [Attrib(category, "Distraction properties")]
  protected edit const array<CName> m_effectOnSartNames;

  [Attrib(category, "Distraction properties")]
  [Default(BasicDistractionDeviceControllerPS, EAnimationType.TRANSFORM))]
  protected edit EAnimationType m_animationType;

  [Attrib(category, "Tech design")]
  protected edit Bool m_forceAnimationSystem;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
  }

  protected void Initialize() {
    Initialize();
  }

  protected void GameAttached() {
    InitializeSkillChecks(this.m_basicDistractionDeviceSkillChecks);
  }

  public final const EAnimationType GetAnimationType() {
    return this.m_animationType;
  }

  public final const Bool GetForceAnimationSystem() {
    return this.m_forceAnimationSystem;
  }

  public final const array<CName> GetEffectOnStartNames() {
    return this.m_effectOnSartNames;
  }

  protected ref<QuickHackDistraction> ActionQuickHackDistraction() {
    ref<QuickHackDistraction> action;
    action = new QuickHackDistraction();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    action.SetDurationValue(GetDistractionDuration(action));
    action.CreateInteraction();
    return action;
  }

  protected final ref<SpiderbotDistractDevice> ActionSpiderbotDistractDevice() {
    ref<SpiderbotDistractDevice> action;
    action = new SpiderbotDistractDevice();
    action.clearanceLevel = GetSpiderbotClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction();
    return action;
  }

  protected final ref<SpiderbotDistractDevicePerformed> ActionSpiderbotDistractDevicePerformed() {
    ref<SpiderbotDistractDevicePerformed> action;
    action = new SpiderbotDistractDevicePerformed();
    action.clearanceLevel = GetSpiderbotClearance();
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    return action;
  }

  protected Bool CanCreateAnySpiderbotActions() {
    if(this.m_distractorType == EPlaystyleType.TECHIE) {
      return true;
    };
    return false;
  }

  protected void GetSpiderbotActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    if(this.m_distractorType == EPlaystyleType.NETRUNNER) {
      return ;
    };
    if(!IsDistracting()) {
      Push(actions, ActionSpiderbotDistractDevice());
    };
  }

  protected const Bool CanCreateAnyQuickHackActions() {
    if(this.m_distractorType == EPlaystyleType.NETRUNNER) {
      return true;
    };
    return false;
  }

  protected void GetQuickHackActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    ref<ScriptableDeviceAction> currentAction;
    GetQuickHackActions(actions, context);
    if(this.m_distractorType == EPlaystyleType.TECHIE || this.m_distractorType == EPlaystyleType.NONE) {
      return ;
    };
    currentAction = ActionQuickHackDistraction();
    currentAction.SetObjectActionID("DeviceAction.MalfunctionClassHack");
    currentAction.SetInactiveWithReason(!IsDistracting(), "LocKey#7004");
    Push(actions, currentAction);
    FinalizeGetQuickHackActions(actions, context);
  }

  public final EntityNotificationType OnSpiderbotDistractExplosiveDevice(ref<SpiderbotDistractDevice> evt) {
    ref<ScriptableDeviceAction> action;
    this.m_distractExecuted = true;
    action = ActionSpiderbotDistractDevicePerformed();
    action.SetDurationValue(GetDistractionDuration(action));
    SendSpiderbotToPerformAction(action, evt.GetExecutor());
    UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final EntityNotificationType OnSpiderbotDistractExplosiveDevicePerformed(ref<SpiderbotDistractDevicePerformed> evt) {
    ref<BaseDeviceStatus> cachedStatus;
    cachedStatus = GetDeviceStatusAction();
    if(evt.IsStarted()) {
      this.m_distractExecuted = true;
      evt.SetCanTriggerStim(true);
      ExecutePSActionWithDelay(evt, this, evt.GetDurationValue());
    } else {
      this.m_distractExecuted = false;
      evt.SetCanTriggerStim(false);
    };
    UseNotifier(evt);
    if(!IsFinal()) {
      LogActionDetails(evt, cachedStatus);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }
}

public class SpiderbotDistractDevice extends ActionBool {

  public final void SetProperties() {
    this.actionName = "SpiderbotDistractDevice";
    this.prop = SetUpProperty_Bool(this.actionName, true, "LocKey#596", "LocKey#596");
  }

  public String GetTweakDBChoiceRecord() {
    return "SpiderbotDistraction";
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    return true;
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    if(IsInRange(clearance, GetInteractiveClearance())) {
      return true;
    };
    return false;
  }

  public final static Bool IsContextValid(GetActionsContext context) {
    if(context.requestType == gamedeviceRequestType.Remote) {
      return true;
    };
    return false;
  }
}

public class SpiderbotDistractDevicePerformed extends ActionBool {

  public final void SetProperties() {
    this.actionName = "SpiderbotDistractDevicePerformed";
    this.prop = SetUpProperty_Bool(this.actionName, true, "SpiderbotDistractDevicePerformed", "SpiderbotDistractDevicePerformed");
  }

  public final static Bool IsAvailable(ref<ScriptableDeviceComponentPS> device) {
    return true;
  }

  public final static Bool IsClearanceValid(ref<Clearance> clearance) {
    return true;
  }

  public final static Bool IsContextValid(GetActionsContext context) {
    return true;
  }
}
