
public class Activator extends InteractiveMasterDevice {

  private ref<AnimFeature_SimpleDevice> m_animFeature;

  private Int32 hitCount;

  private ref<MeshComponent> m_meshComponent;

  [Default(Activator, default))]
  public CName meshAppearence;

  [Default(Activator, Yellow))]
  public CName meshAppearenceBreaking;

  [Default(Activator, red))]
  public CName meshAppearenceBroken;

  [Default(Activator, 2.98f))]
  public Float defaultDelay;

  [Default(Activator, 1.68f))]
  public Float yellowDelay;

  [Default(Activator, 4.03f))]
  public Float redDelay;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "mesh", "MeshComponent", true);
    OnRequestComponents(ri);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.m_meshComponent = Cast(GetComponent(ri, "mesh"));
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  private const ref<ActivatorController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<ActivatorControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected void EnterWorkspot(ref<GameObject> activator, Bool freeCamera?, CName componentName?, CName deviceData?) {
    ref<WorkspotGameSystem> workspotSystem;
    workspotSystem = GetWorkspotSystem(activator.GetGame());
    workspotSystem.PlayInDeviceSimple(this, activator, freeCamera, componentName, "deviceWorkspot");
  }

  protected cb Bool OnDisassembleDevice(ref<DisassembleDevice> evt) {
    ref<IBlackboard> playerStateMachineBlackboard;
    ref<PlayerPuppet> playerPuppet;
    playerPuppet = Cast(GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject());
    EnterWorkspot(playerPuppet, false, "disassembleWorkspot");
    playerStateMachineBlackboard = GetBlackboardSystem(GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, true);
    UpdateDeviceState();
    DelayApperanceSwitchEvent(this.meshAppearenceBreaking, this.yellowDelay);
    DelayApperanceSwitchEvent(this.meshAppearence, this.defaultDelay);
    DelayApperanceSwitchEvent(this.meshAppearenceBroken, this.redDelay);
  }

  protected cb Bool OnSpiderbotActivateActivator(ref<SpiderbotActivateActivator> evt) {
    ref<SpiderbotOrderDeviceEvent> spiderbotOrderDeviceEvent;
    NodeRef locationOverrideRef;
    EntityID locationOverrideID;
    wref<GameObject> locationOverrideObject;
    GlobalNodeRef locationOverrideGlobalRef;
    SendSetIsSpiderbotInteractionOrderedEvent(true);
    spiderbotOrderDeviceEvent = new SpiderbotOrderDeviceEvent();
    spiderbotOrderDeviceEvent.target = RefToWeakRef(this);
    locationOverrideRef = GetDevicePS().GetSpiderbotInteractionLocationOverride();
    locationOverrideGlobalRef = ResolveNodeRefWithEntityID(locationOverrideRef, GetEntityID());
    if(IsDefined(ResolveNodeRef(locationOverrideRef, Cast(GetRoot())))) {
      locationOverrideID = Cast(locationOverrideGlobalRef);
      locationOverrideObject = RefToWeakRef(Cast(FindEntityByID(GetGame(), locationOverrideID)));
      spiderbotOrderDeviceEvent.overrideMovementTarget = locationOverrideObject;
    };
    WeakRefToRef(evt.GetExecutor()).QueueEvent(spiderbotOrderDeviceEvent);
  }

  protected cb Bool OnSpiderbotOrderCompletedEvent(ref<SpiderbotOrderCompletedEvent> evt) {
    SendSetIsSpiderbotInteractionOrderedEvent(false);
    GetActivityLogSystem(GetGame()).AddLog("SPIDERBOT HAS FINISHED ACTIVATING THE DEVICE ... ");
    GetDevicePS().ActivateConnectedDevices();
    SetGameplayRoleToNone();
  }

  protected cb Bool OnToggleActivation(ref<ToggleActivation> evt) {
    UpdateDeviceState();
    SetGameplayRoleToNone();
  }

  protected cb Bool OnDelayApperanceSwitchEvent(ref<panelApperanceSwitchEvent> evt) {
    SetMeshAppearanceEvent(this, evt.newApperance);
  }

  protected cb Bool OnWorkspotFinished(CName componentName) {
    OnWorkspotFinished(componentName);
    if(componentName == "disassembleWorkspot") {
      this.m_disassemblableComponent.ObtainParts();
      GetDevicePS().ActivateConnectedDevices();
      SetGameplayRoleToNone();
      UpdateAnimState();
    };
  }

  private final void UpdateAnimState() {
    if(!ToBool(this.m_animFeature)) {
      this.m_animFeature = new AnimFeature_SimpleDevice();
    };
    this.m_animFeature.isOpen = true;
    this.m_interaction.Toggle(false);
    ApplyFeature(this, "DeviceMaintenancePanel", this.m_animFeature);
  }

  private final void DelayApperanceSwitchEvent(CName newApperance, Float time) {
    ref<panelApperanceSwitchEvent> evt;
    evt = new panelApperanceSwitchEvent();
    evt.newApperance = newApperance;
    GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), evt, time);
  }

  protected cb Bool OnHit(ref<gameHitEvent> evt) {
    if(GetDevicePS().GetDurabilityType() == EDeviceDurabilityType.DESTRUCTIBLE) {
      this.hitCount = this.hitCount + 1;
      if(this.hitCount > 1) {
        GetDevicePS().ActivateConnectedDevices();
        SetGameplayRoleToNone();
        this.m_meshComponent.Toggle(false);
      };
    };
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.ControlOtherDevice;
  }

  public const EGameplayRole GetCurrentGameplayRole() {
    EGameplayRole gameplayRole;
    if(gameplayRole != EGameplayRole.) {
      return gameplayRole;
    };
    return this.m_gameplayRoleComponent.GetCurrentGameplayRole();
  }
}
