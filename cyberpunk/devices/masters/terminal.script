
public class Terminal extends InteractiveMasterDevice {

  protected ref<ScriptableVirtualCameraViewComponent> m_cameraFeed;

  private Bool m_isShortGlitchActive;

  private DelayID m_shortGlitchDelayID;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    RequestComponent(ri, "terminalui", "worlduiWidgetComponent", false);
    RequestComponent(ri, "disassemblableComponent", "DisassemblableComponent", true);
    OnRequestComponents(ri);
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    this.m_uiComponent = RefToWeakRef(Cast(GetComponent(ri, "terminalui")));
    this.m_disassemblableComponent = Cast(GetComponent(ri, "disassemblableComponent"));
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  protected void ResolveGameplayState() {
    ResolveGameplayState();
    ResetScreenToDefault();
  }

  protected cb Bool OnLogicReady(ref<SetLogicReadyEvent> evt) {
    OnLogicReady(evt);
    if(IsUIdirty() && this.m_isInsideLogicArea) {
      RefreshUI();
    };
  }

  protected cb Bool OnDetach() {
    OnDetach();
    GetGlobalTVSystem(GetGame()).UnregisterTVChannelFromEntity(RefToWeakRef(this));
  }

  protected cb Bool OnPersitentStateInitialized(ref<GameAttachedEvent> evt) {
    OnPersitentStateInitialized(evt);
  }

  protected Bool UpdateDeviceState(Bool isDelayed?) {
    if(UpdateDeviceState(isDelayed)) {
      if(GetDevicePS().HasActiveContext(gamedeviceRequestType.Direct) || GetDevicePS().IsAdvancedInteractionModeOn() || ShouldAlwasyRefreshUIInLogicAra() && this.m_isInsideLogicArea) {
        RefreshUI(isDelayed);
      };
      return true;
    };
    return false;
  }

  private const ref<TerminalController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<TerminalControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected void CreateBlackboard() {
    this.m_blackboard = Create(GetAllBlackboardDefs().MasterDeviceBaseBlackboard);
  }

  public const ref<MasterDeviceBaseBlackboardDef> GetBlackboardDef() {
    return GetDevicePS().GetBlackboardDef();
  }

  protected void RestoreDeviceState() {
    RestoreDeviceState();
  }

  protected void DeactivateDevice() {
    TurnOffDevice();
  }

  protected void ActivateDevice() {
    TurnOnDevice();
  }

  protected void CutPower() {
    TurnOffScreen();
    this.m_cameraFeed.Toggle(false);
  }

  protected void TurnOffDevice() {
    TurnOffScreen();
    this.m_cameraFeed.Toggle(false);
  }

  protected void TurnOnDevice() {
    TurnOnScreen();
  }

  protected cb Bool OnActionEngineering(ref<ActionEngineering> evt) {
    ref<IBlackboard> playerStateMachineBlackboard;
    EnterWorkspot(GetPlayerSystem(GetGame()).GetLocalPlayerControlledGameObject(), true, "playerEngineeringWorkspot");
    playerStateMachineBlackboard = GetBlackboardSystem(GetGame()).Get(GetAllBlackboardDefs().PlayerStateMachine);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, true);
  }

  protected cb Bool OnDisassembleDevice(ref<DisassembleDevice> evt) {
    ref<PlayerPuppet> player;
    player = Cast(GetPlayerSystem(GetGame()).GetLocalPlayerMainGameObject());
    this.m_disassemblableComponent.ObtainParts();
    UpdateDeviceState();
  }

  protected cb Bool OnWorkspotFinished(CName componentName) {
    OnWorkspotFinished(componentName);
    if(componentName == "disassembleWorkspot") {
      this.m_disassemblableComponent.ObtainParts();
    };
  }

  public final const Bool IsAuthorizationModuleOn() {
    return GetDevicePS().IsAuthorizationModuleOn();
  }

  private final void UnsecureTerminal() {
    ExecuteAction(GetDevicePS().GetActionByName("ToggleSecure"));
  }

  protected final void StartHacking(ref<GameObject> executor) {
    if(executor != null) {
    };
  }

  protected final void ResetScreenToDefault() {
    if(GetDevicePS().IsON()) {
      return ;
    };
    if(GetDevicePS().IsON()) {
      ShowScreenSaver();
    } else {
      TurnOffScreen();
    };
  }

  protected final void ShowScreenSaver()

  protected final void TurnOffScreen() {
    WeakRefToRef(this.m_uiComponent).Toggle(false);
    GetGlobalTVSystem(GetGame()).UnregisterTVChannelFromEntity(RefToWeakRef(this));
  }

  protected final void TurnOnScreen() {
    WeakRefToRef(this.m_uiComponent).Toggle(true);
  }

  protected final void SetState(gameinteractionsReactionState state) {
    ref<TerminalSetState> evt;
    evt = new TerminalSetState();
    evt.state = state;
    GetPersistencySystem(GetGame()).QueuePSEvent(GetController().GetPersistentID(), GetController().GetPSName(), evt);
  }

  private void InitializeScreenDefinition() {
    if(!IsValid(this.m_screenDefinition.screenDefinition)) {
      this.m_screenDefinition.screenDefinition = "DevicesUIDefinitions.Terminal_9x16";
    };
    if(!IsValid(this.m_screenDefinition.style)) {
      this.m_screenDefinition.style = "DevicesUIStyles.None";
    };
  }

  public const Bool ShouldShowTerminalTitle() {
    return GetDevicePS().ShouldShowTerminalTitle();
  }

  protected void BreakDevice() {
    TurnOffScreen();
  }

  public const EGameplayRole DeterminGameplayRole() {
    return EGameplayRole.ControlOtherDevice;
  }

  public const ResRef GetDefaultGlitchVideoPath() {
    return GetDevicePS().GetDefaultGlitchVideoPath();
  }

  public const ResRef GetBroadcastGlitchVideoPath() {
    return GetDevicePS().GetBroadcastGlitchVideoPath();
  }

  protected void StartGlitching(EGlitchState glitchState, Float intensity?) {
    GlitchData glitchData;
    ref<AdvertGlitchEvent> evt;
    glitchData.state = glitchState;
    glitchData.intensity = intensity;
    if(intensity == 0) {
      intensity = 1;
    };
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(intensity);
    QueueEvent(evt);
    GetBlackboard().SetVariant(GetBlackboardDef().GlitchData, ToVariant(glitchData), true);
    GetBlackboard().FireCallbacks();
  }

  protected void StopGlitching() {
    GlitchData glitchData;
    ref<AdvertGlitchEvent> evt;
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(0);
    QueueEvent(evt);
    glitchData.state = EGlitchState.NONE;
    GetBlackboard().SetVariant(GetDevicePS().GetBlackboardDef().GlitchData, ToVariant(glitchData));
    GetBlackboard().FireCallbacks();
  }

  protected cb Bool OnHitEvent(ref<gameHitEvent> hit) {
    OnHitEvent(hit);
    StartShortGlitch();
  }

  private final void StartShortGlitch() {
    ref<StopShortGlitchEvent> evt;
    if(GetDevicePS().IsGlitching()) {
      return ;
    };
    if(!this.m_isShortGlitchActive) {
      evt = new StopShortGlitchEvent();
      StartGlitching(EGlitchState.DEFAULT, 1);
      this.m_shortGlitchDelayID = GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), evt, 0.25);
      this.m_isShortGlitchActive = true;
    };
  }

  protected cb Bool OnStopShortGlitch(ref<StopShortGlitchEvent> evt) {
    this.m_isShortGlitchActive = false;
    if(!GetDevicePS().IsGlitching()) {
      StopGlitching();
    };
  }
}
