
public class FuseController extends MasterController {

  protected const ref<FuseControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class FuseControllerPS extends MasterControllerPS {

  [Attrib(category, "City Light System")]
  protected inline persistent ref<DeviceTimeTableManager> m_timeTableSetup;

  [Attrib(category, "City Light System")]
  [Default(FuseControllerPS, 5))]
  protected persistent Int32 m_maxLightsSwitchedAtOnce;

  [Attrib(rangeMax, "1.0f")]
  [Attrib(category, "City Light System")]
  [Attrib(rangeMin, "0.0f")]
  [Default(FuseControllerPS, 1.0f))]
  protected persistent Float m_timeToNextSwitch;

  [Attrib(category, "City Light System")]
  [Default(FuseControllerPS, ELightSwitchRandomizerType.RANDOM_PROGRESSIVE))]
  protected persistent ELightSwitchRandomizerType m_lightSwitchRandomizerType;

  [Attrib(customEditor, "TweakDBGroupInheritance;Interactions.InteractionChoice")]
  private TweakDBID m_alternativeNameForON;

  [Attrib(customEditor, "TweakDBGroupInheritance;Interactions.InteractionChoice")]
  private TweakDBID m_alternativeNameForOFF;

  private Bool m_isCLSInitialized;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
    if(!IsStringValid(this.m_deviceName)) {
      this.m_deviceName = "LocKey#116";
    };
  }

  protected void Initialize() {
    Initialize();
    InitializeCLS();
    if(IsUnpowered()) {
      RefreshPowerOnSlaves_Event();
    } else {
      if(this.m_timeTableSetup == null || !this.m_timeTableSetup.IsValid()) {
        RefreshSlaves_Event();
      };
    };
  }

  public const EDeviceStatus GetExpectedSlaveState() {
    return GetDeviceState();
  }

  public final const ref<DeviceTimeTableManager> GetTimetableSetup() {
    return this.m_timeTableSetup;
  }

  protected const ref<Clearance> GetClearance() {
    return CreateClearance(29, 29);
  }

  public ref<ToggleON> ActionToggleON() {
    ref<ToggleON> action;
    action = new ToggleON();
    action.clearanceLevel = GetToggleOnClearance();
    action.SetUp(this);
    action.SetProperties(this.m_deviceState, this.m_alternativeNameForON, this.m_alternativeNameForOFF);
    action.AddDeviceName(this.m_deviceName);
    action.CreateActionWidgetPackage();
    return action;
  }

  public Bool GetActions(out array<ref<DeviceAction>> outActions, GetActionsContext context) {
    GetActions(outActions, context);
    if(IsDisabled()) {
      return false;
    };
    if(IsDefaultConditionMet(this, context) && context.requestType == gamedeviceRequestType.External) {
      Push(outActions, ActionToggleON());
    };
    SetActionIllegality(outActions, this.m_illegalActions.regularActions);
    return true;
  }

  protected EntityNotificationType OnQuestForceON(ref<QuestForceON> evt) {
    OnQuestForceON(evt);
    RefreshSlaves();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnQuestForceOFF(ref<QuestForceOFF> evt) {
    OnQuestForceOFF(evt);
    RefreshSlaves();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnSetDeviceON(ref<SetDeviceON> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    if(IsDisabled() || IsUnpowered()) {
      return SendActionFailedEvent(evt, evt.GetRequesterID(), "Unpowered or Disabled");
    };
    SetDeviceState(EDeviceStatus.ON);
    RefreshSlaves();
    Notify(notifier, evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnSetDeviceOFF(ref<SetDeviceOFF> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    if(IsDisabled() || IsUnpowered()) {
      return SendActionFailedEvent(evt, evt.GetRequesterID(), "Unpowered or Disabled");
    };
    SetDeviceState(EDeviceStatus.OFF);
    RefreshSlaves();
    Notify(notifier, evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnSetDeviceUnpowered(ref<SetDeviceUnpowered> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    if(IsDisabled()) {
      return SendActionFailedEvent(evt, evt.GetRequesterID(), "Unpowered or Disabled");
    };
    UnpowerDevice();
    Notify(notifier, evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected EntityNotificationType OnSetDevicePowered(ref<SetDevicePowered> evt) {
    ref<ActionNotifier> notifier;
    notifier = new ActionNotifier();
    notifier.SetNone();
    if(IsDisabled()) {
      return SendActionFailedEvent(evt, evt.GetRequesterID(), "Disabled");
    };
    PowerDevice();
    Notify(notifier, evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public EntityNotificationType OnToggleON(ref<ToggleON> evt) {
    OnToggleON(evt);
    RefreshSlaves();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected void PowerDevice() {
    EDeviceStatus stateFromCLS;
    stateFromCLS = EDeviceStatus.DISABLED;
    if(IsConnectedToCLS()) {
      stateFromCLS = GetCityLightSystem().GetFuseStateByID(GetID());
    };
    if(stateFromCLS != EDeviceStatus.DISABLED) {
      SetDeviceState(stateFromCLS);
    } else {
      SetDeviceState(EDeviceStatus.ON);
    };
    RefreshPowerOnSlaves();
  }

  public void UnpowerDevice() {
    UnpowerDevice();
    RefreshPowerOnSlaves();
  }

  private final void RefreshSlaves() {
    Int32 i;
    CName actionToGet;
    ref<DeviceAction> action;
    array<ref<DeviceComponentPS>> devices;
    GetActionsContext context;
    if(IsUnpowered()) {
      RefreshPowerOnSlaves();
      return ;
    };
    if(IsConnectedToCLS()) {
      RefreshCLSoNslaves(GetDeviceState(), false, GetImmediateSlaves());
      return ;
    };
    context = GenerateContext(gamedeviceRequestType.Internal, GetTotalClearanceValue(), null, ExtractEntityID(GetID()));
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(devices[i] == this) {
      } else {
        if(IsON()) {
          action = Cast(devices[i]).ActionSetDeviceON();
        } else {
          action = Cast(devices[i]).ActionSetDeviceOFF();
        };
        if(ToBool(action)) {
          Cast(action).RegisterAsRequester(ExtractEntityID(GetID()));
          GetPersistencySystem().QueuePSDeviceEvent(action);
        };
      };
      i = i + 1;
    };
  }

  private final void RefreshPowerOnSlaves() {
    Bool restorePower;
    if(IsConnectedToCLS()) {
      restorePower = IsON();
      RefreshCLSoNslaves(GetDeviceState(), restorePower, GetImmediateSlaves());
      return ;
    };
    if(!IsON()) {
      CutPowerOnSlaveDevices();
    } else {
      RestorePowerOnSlaveDevices();
    };
  }

  private final void RestorePowerOnSlaveDevices() {
    Int32 i;
    ref<ScriptableDeviceAction> action;
    array<ref<DeviceComponentPS>> devices;
    ref<ScriptableDeviceComponentPS> device;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(devices[i] == this) {
      } else {
        device = Cast(devices[i]);
        if(ToBool(device)) {
          action = device.ActionSetDevicePowered();
        };
        if(ToBool(action)) {
          ExecutePSAction(action, device);
        };
      };
      i = i + 1;
    };
  }

  private final void CutPowerOnSlaveDevices() {
    Int32 i;
    ref<ScriptableDeviceAction> action;
    array<ref<DeviceComponentPS>> devices;
    ref<ScriptableDeviceComponentPS> device;
    devices = GetImmediateSlaves();
    i = 0;
    while(i < Size(devices)) {
      if(devices[i] == this) {
      } else {
        device = Cast(devices[i]);
        if(ToBool(device)) {
          action = device.ActionSetDeviceUnpowered();
        };
        if(ToBool(action)) {
          ExecutePSAction(action, device);
        };
      };
      i = i + 1;
    };
  }

  protected EntityNotificationType OnRefreshSlavesEvent(ref<RefreshSlavesEvent> evt) {
    RefreshSlaves();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final EntityNotificationType OnRefreshPowerOnSlavesEvent(ref<RefreshPowerOnSlavesEvent> evt) {
    RefreshPowerOnSlaves();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final void RefreshPowerOnSlaves_Event() {
    ref<RefreshPowerOnSlavesEvent> evt;
    evt = new RefreshPowerOnSlavesEvent();
    GetPersistencySystem().QueuePSEvent(GetID(), GetClassName(), evt);
  }

  public final const Bool IsCLSInitialized() {
    return this.m_isCLSInitialized;
  }

  protected final EntityNotificationType OnInitializeCLSEvent(ref<InitializeCLSEvent> evt) {
    InitializeCLS();
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final EntityNotificationType OnDeviceTimetableEvent(ref<DeviceTimetableEvent> evt) {
    if(evt.state == EDeviceStatus.ON) {
      ExecutePSAction(ActionSetDeviceON(), this);
    } else {
      if(evt.state == EDeviceStatus.OFF) {
        ExecutePSAction(ActionSetDeviceOFF(), this);
      } else {
        if(evt.state == EDeviceStatus.UNPOWERED) {
          ExecutePSAction(ActionSetDeviceUnpowered(), this);
        };
      };
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final EntityNotificationType OnRefreshCLSoNslaves(ref<RefreshCLSOnSlavesEvent> evt) {
    if(evt.state != GetDeviceState()) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    RefreshCLSoNslaves(evt.state, evt.restorePower, evt.slaves);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  private final void RefreshCLSoNslaves(EDeviceStatus state, Bool restorePower, array<ref<DeviceComponentPS>> devices) {
    Int32 i;
    EntityID id;
    Int32 totalEventsSent;
    array<ref<DeviceComponentPS>> excessDevices;
    Float delay;
    i = 0;
    while(i < Size(devices)) {
      if(devices[i] == null || !devices[i].IsAttachedToGame()) {
      } else {
        if(totalEventsSent >= this.m_maxLightsSwitchedAtOnce) {
          Push(excessDevices, devices[i]);
        } else {
          id = ExtractEntityID(devices[i].GetID());
          if(IsDefined(id)) {
            SendDeviceTimeTableEvent(id, state, restorePower);
            totalEventsSent += 1;
          };
        };
      };
      i += 1;
    };
    if(Size(excessDevices) > 0) {
      if(this.m_lightSwitchRandomizerType == ELightSwitchRandomizerType.NONE) {
        delay = this.m_timeToNextSwitch;
      } else {
        if(this.m_lightSwitchRandomizerType == ELightSwitchRandomizerType.RANDOM_PROGRESSIVE) {
          delay += GetLightSwitchDelayValue();
        } else {
          delay = GetLightSwitchDelayValue();
        };
      };
      SendCLSRefreshByEvent(excessDevices, state, restorePower, delay);
    };
  }

  private final Float GetLightSwitchDelayValue() {
    Float delay;
    if(this.m_timeToNextSwitch == 0) {
      delay = 0;
    } else {
      delay = RandRangeF(0, this.m_timeToNextSwitch);
    };
    return delay;
  }

  private final void SendCLSRefreshByEvent(array<ref<DeviceComponentPS>> devices, EDeviceStatus state, Bool restorePower, Float delay) {
    ref<RefreshCLSOnSlavesEvent> evt;
    evt = new RefreshCLSOnSlavesEvent();
    evt.state = state;
    evt.slaves = devices;
    evt.restorePower = restorePower;
    if(delay > 0) {
      QueuePSEventWithDelay(RefToWeakRef(this), evt, delay);
    } else {
      QueuePSEvent(RefToWeakRef(this), evt);
    };
  }

  private final void SendDeviceTimeTableEvent(EntityID targetID, EDeviceStatus state, Bool restorePower) {
    ref<DeviceTimetableEvent> evt;
    evt = new DeviceTimetableEvent();
    evt.state = state;
    evt.requesterID = ExtractEntityID(GetID());
    evt.restorePower = restorePower;
    GetPersistencySystem().QueueEntityEvent(targetID, evt);
  }

  private final void SendDeviceTimeTableEventWithDelay(EntityID targetID, EDeviceStatus state, Bool restorePower, Float delay) {
    ref<DeviceTimetableEvent> evt;
    evt = new DeviceTimetableEvent();
    evt.state = state;
    evt.requesterID = ExtractEntityID(GetID());
    evt.restorePower = restorePower;
    QueuePSEventWithDelay(RefToWeakRef(this), evt, delay);
  }

  protected final EntityNotificationType OnDealyedTimetableEvent(ref<DelayedTimetableEvent> evt) {
    EntityID id;
    if(evt.eventToForward != null && WeakRefToRef(evt.targetPS) != null) {
      if(WeakRefToRef(evt.targetPS).IsAttachedToGame()) {
        id = ExtractEntityID(WeakRefToRef(evt.targetPS).GetID());
        if(IsDefined(id)) {
          GetPersistencySystem().QueueEntityEvent(id, evt);
        };
      };
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final const EDeviceStatus GetDeviceStateByCLS() {
    ref<CityLightSystem> cls;
    ECLSForcedState clsState;
    EDeviceStatus deviceState;
    if(!IsDisabled() && !IsUnpowered()) {
      deviceState = this.m_deviceState;
    } else {
      if(IsConnectedToCLS() && !this.m_isCLSInitialized) {
        cls = GetCityLightSystem();
        if(ToBool(cls)) {
          clsState = cls.GetState();
          if(clsState == ECLSForcedState.DEFAULT) {
            deviceState = this.m_timeTableSetup.GetDeviceStateForActiveEntry(GetGameInstance());
          } else {
            if(clsState == ECLSForcedState.ForcedON) {
              deviceState = EDeviceStatus.ON;
            } else {
              if(clsState == ECLSForcedState.ForcedOFF) {
                deviceState = EDeviceStatus.OFF;
              };
            };
          };
        } else {
          deviceState = this.m_deviceState;
        };
      } else {
        deviceState = this.m_deviceState;
      };
    };
    return deviceState;
  }

  private final void InitializeCLS() {
    ref<RegisterTimetableRequest> request;
    PSOwnerData requesterData;
    array<ref<DeviceComponentPS>> lights;
    ref<CityLightSystem> cls;
    ECLSForcedState clsState;
    if(this.m_isCLSInitialized) {
      return ;
    };
    if(this.m_timeTableSetup != null && this.m_timeTableSetup.IsValid()) {
      request = new RegisterTimetableRequest();
      request.timeTable = this.m_timeTableSetup.GetTimeTable();
      requesterData.id = GetID();
      requesterData.className = GetClassName();
      request.requesterData = requesterData;
      if(!IsFinal()) {
        GetChildren(lights);
        request.lights = Size(lights);
      };
      cls = GetCityLightSystem();
      if(ToBool(cls)) {
        cls.QueueRequest(request);
        clsState = cls.GetState();
      };
      if(!IsDisabled() && !IsUnpowered()) {
        if(clsState == ECLSForcedState.DEFAULT) {
          SetDeviceState(this.m_timeTableSetup.GetDeviceStateForActiveEntry(GetGameInstance()));
        } else {
          if(clsState == ECLSForcedState.ForcedON) {
            SetDeviceState(EDeviceStatus.ON);
          } else {
            if(clsState == ECLSForcedState.ForcedOFF) {
              SetDeviceState(EDeviceStatus.OFF);
            };
          };
        };
      };
      this.m_isCLSInitialized = true;
    };
  }

  public const Bool IsConnectedToCLS() {
    return this.m_timeTableSetup != null && this.m_timeTableSetup.IsValid();
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.GeneratorDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.GeneratorDeviceBackground";
  }
}
