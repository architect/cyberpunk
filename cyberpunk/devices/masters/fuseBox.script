
public class FuseBox extends InteractiveMasterDevice {

  private Bool m_isShortGlitchActive;

  private DelayID m_shortGlitchDelayID;

  [Default(FuseBox, 0))]
  protected edit Int32 m_numberOfComponentsToON;

  [Default(FuseBox, 0))]
  protected edit Int32 m_numberOfComponentsToOFF;

  protected edit const array<Int32> m_indexesOfComponentsToOFF;

  public ref<MeshComponent> m_mesh;

  private array<ref<IPlacedComponent>> m_componentsON;

  private array<ref<IPlacedComponent>> m_componentsOFF;

  protected cb Bool OnRequestComponents(EntityRequestComponentsInterface ri) {
    Int32 i;
    String componentName;
    RequestComponent(ri, "stand_generator", "MeshComponent", false);
    OnRequestComponents(ri);
    i = 0;
    while(i < this.m_numberOfComponentsToON) {
      componentName = "componentON_" + i;
      RequestComponent(ri, StringToName(componentName), "IPlacedComponent", true);
      i += 1;
    };
    i = 0;
    while(i < this.m_numberOfComponentsToOFF) {
      componentName = "componentOFF_" + i;
      RequestComponent(ri, StringToName(componentName), "IPlacedComponent", true);
      i += 1;
    };
  }

  protected cb Bool OnTakeControl(EntityResolveComponentsInterface ri) {
    Int32 i;
    String componentName;
    this.m_mesh = Cast(GetComponent(ri, "stand_generator"));
    i = 0;
    while(i < this.m_numberOfComponentsToON) {
      componentName = "componentON_" + i;
      Push(this.m_componentsON, Cast(GetComponent(ri, StringToName(componentName))));
      i += 1;
    };
    i = 0;
    while(i < this.m_numberOfComponentsToOFF) {
      componentName = "componentOFF_" + i;
      Push(this.m_componentsOFF, Cast(GetComponent(ri, StringToName(componentName))));
      i += 1;
    };
    OnTakeControl(ri);
    this.m_controller = Cast(GetComponent(ri, "controller"));
  }

  private const ref<FuseBoxController> GetController() {
    return Cast(this.m_controller);
  }

  public const ref<FuseBoxControllerPS> GetDevicePS() {
    ref<DeviceComponentPS> ps;
    ps = GetControllerPersistentState();
    return Cast(ps);
  }

  protected void TurnOffDevice() {
    TurnOffDevice();
    ToggleComponentsON_OFF(false);
  }

  protected void TurnOnDevice() {
    TurnOnDevice();
    ToggleComponentsON_OFF(true);
  }

  protected final void ToggleComponentsON_OFF(Bool visible) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_indexesOfComponentsToOFF)) {
      this.m_componentsOFF[this.m_indexesOfComponentsToOFF[i]].Toggle(visible);
      i += 1;
    };
  }

  protected void ResolveGameplayState() {
    Int32 i;
    array<ExplosiveDeviceResourceDefinition> dataArray;
    if(GetDevicePS().IsOverloaded()) {
      ToggleVisibility(false);
    };
    ResolveGameplayState();
  }

  protected cb Bool OnOverloadDevice(ref<OverloadDevice> evt) {
    if(evt.IsStarted()) {
      StartOverloading(evt.GetClassName());
    } else {
      StopOverloading();
    };
  }

  private final void StartOverloading(CName effectName) {
    ref<EffectInstance> empEffect;
    ref<AreaEffectData> effectData;
    Float duration;
    Vector4 position;
    Int32 actionIndex;
    ref<AreaEffectData> areaEffect;
    actionIndex = GetFxResourceMapper().GetAreaEffectDataIndexByName(effectName);
    if(actionIndex < 0) {
      return ;
    };
    areaEffect = GetFxResourceMapper().GetAreaEffectDataByIndex(actionIndex);
    duration = areaEffect.stimLifetime;
    position = GetAcousticQuerryStartPoint();
    empEffect = GetGameEffectSystem(GetGame()).CreateEffectStatic("emp", "emp", this);
    SetVector(empEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, position);
    SetFloat(empEffect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, areaEffect.stimRange);
    empEffect.Run();
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Start, "emp");
    StartGlitching(EGlitchState.DEFAULT, 1);
    ToggleVisibility(false);
  }

  private final void StopOverloading() {
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.BreakLoop, "smoke");
    if(!GetDevicePS().IsGlitching()) {
      StopGlitching();
    };
  }

  protected void ToggleVisibility(Bool visible) {
    Int32 i;
    Bool off;
    this.m_mesh.Toggle(visible);
    i = 0;
    while(i < this.m_numberOfComponentsToON) {
      this.m_componentsON[i].Toggle(!visible);
      i += 1;
    };
    i = 0;
    while(i < this.m_numberOfComponentsToOFF) {
      this.m_componentsOFF[i].Toggle(visible);
      i += 1;
    };
    SetGameplayRoleToNone();
    GetDevicePS().ForceDisableDevice();
  }

  public const EGameplayRole DeterminGameplayRole() {
    if(GetDevicePS().IsGenerator()) {
      return EGameplayRole.Distract;
    };
    if(GetDevicePS().IsOFF() || GetDevicePS().IsDisabled()) {
      return EGameplayRole.;
    };
    return EGameplayRole.CutPower;
  }

  protected void StartGlitching(EGlitchState glitchState, Float intensity?) {
    ref<AdvertGlitchEvent> evt;
    if(intensity == 0) {
      intensity = 1;
    };
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(intensity);
    QueueEvent(evt);
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Start, "smoke");
  }

  protected void StopGlitching() {
    ref<AdvertGlitchEvent> evt;
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(0);
    QueueEvent(evt);
    ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.BreakLoop, "smoke");
  }

  protected cb Bool OnHitEvent(ref<gameHitEvent> hit) {
    OnHitEvent(hit);
    StartShortGlitch();
  }

  private final void StartShortGlitch() {
    ref<StopShortGlitchEvent> evt;
    if(GetDevicePS().IsGlitching()) {
      return ;
    };
    if(!this.m_isShortGlitchActive) {
      evt = new StopShortGlitchEvent();
      StartGlitching(EGlitchState.DEFAULT, 1);
      this.m_shortGlitchDelayID = GetDelaySystem(GetGame()).DelayEvent(RefToWeakRef(this), evt, 0.25);
      this.m_isShortGlitchActive = true;
    };
  }

  protected cb Bool OnStopShortGlitch(ref<StopShortGlitchEvent> evt) {
    this.m_isShortGlitchActive = false;
    if(!GetDevicePS().IsGlitching()) {
      StopGlitching();
      ActivateEffectAction(RefToWeakRef(this), gamedataFxActionType.Kill, "smoke");
    };
  }

  protected const Bool HasAnyDirectInteractionActive() {
    if(IsDead()) {
      return false;
    };
    return true;
  }
}
