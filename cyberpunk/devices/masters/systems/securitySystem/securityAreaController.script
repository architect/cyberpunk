
public class SecurityAreaEvent extends ActionBool {

  private SecurityAreaData m_securityAreaData;

  private wref<GameObject> m_whoBreached;

  public final const ref<GameObject> GetWhoBreached() {
    return WeakRefToRef(this.m_whoBreached);
  }

  protected final void SetWhoBreached(ref<GameObject> whoBreached) {
    this.m_whoBreached = RefToWeakRef(whoBreached);
  }

  public final void SetAreaData(SecurityAreaData areaData) {
    this.m_securityAreaData = areaData;
  }

  public final SecurityAreaData GetSecurityAreaData() {
    return this.m_securityAreaData;
  }

  public final PersistentID GetSecurityAreaID() {
    return this.m_securityAreaData.id;
  }

  public final void ModifyAreaTypeHack(ESecurityAreaType modifiedAreaType) {
    this.m_securityAreaData.securityAreaType = modifiedAreaType;
  }
}

public class SecurityAreaCrossingPerimeter extends SecurityAreaEvent {

  private Bool m_entered;

  public final void SetProperties(ref<GameObject> whoBreached, Bool didEnter) {
    SetWhoBreached(whoBreached);
    this.m_entered = didEnter;
  }

  public final Bool GetEnteredState() {
    return this.m_entered;
  }
}

public class SecurityAreaController extends MasterController {

  protected const ref<SecurityAreaControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public struct OutputPersistentData {

  public persistent ESecuritySystemState m_currentSecurityState;

  public persistent EBreachOrigin m_breachOrigin;

  public persistent Bool m_securityStateChanged;

  public persistent Vector4 m_lastKnownPosition;

  public persistent ESecurityNotificationType m_type;

  public persistent ESecurityAreaType m_areaType;

  public persistent EntityID m_objectOfInterest;

  public persistent EntityID m_whoBreached;

  public persistent PersistentID m_reporter;

  public persistent Int32 m_id;

  public final static Bool IsValid(OutputPersistentData self) {
    if(IsDefined(self.m_whoBreached) || IsDefined(self.m_objectOfInterest)) {
      return true;
    };
    return false;
  }
}

public class SecurityAreaControllerPS extends MasterControllerPS {

  private ref<SecuritySystemControllerPS> m_system;

  private persistent array<AreaEntry> m_usersInPerimeter;

  private persistent Bool m_isPlayerInside;

  [Attrib(tooltip, "Utilized during Authorization only. Determines the list of passwords and keycards that are working in this area. Passwords & Keycards are specified in Security System")]
  private ESecurityAccessLevel m_securityAccessLevel;

  [Attrib(tooltip, "This determines what actions are legal inside this area as well what type of response will be initiated as a countermeasure")]
  [Default(SecurityAreaControllerPS, ESecurityAreaType.DANGEROUS))]
  private persistent ESecurityAreaType m_securityAreaType;

  [Attrib(tooltip, "[ OPTIONAL ] This determines what kind of events can get out of given area or get inside given area and its agents. By default all events are received and broadcasted")]
  private EventsFilters m_eventsFilters;

  [Attrib(tooltip, "[ OPTIONAL ] If you want your Security Area to change its type regularly (i.e: Store / Alley (day/night) / Arasaka Lobby) here you can control it. TIP: You can perform transitions using SecurityAreaManager Quest Block :) TIP2: UPON TRANSITION, UI PROMPT ABOUT BEING IN AREA X MAY SHOW UP! TIP3: ORDER IS NOT IMPORANT!")]
  private array<AreaTypeTransition> m_areaTransitions;

  private persistent Bool m_pendingDisableRequest;

  private persistent OutputPersistentData m_lastOutput;

  private Bool m_questPlayerHasTriggeredCombat;

  private Bool m_hasThisAreaReceivedCombatNotification;

  private Bool m_pendingNotifyPlayerAboutTransition;

  protected void Initialize() {
    Initialize();
  }

  protected cb Bool OnInstantiated() {
    OnInstantiated();
  }

  protected void GameAttached() {
    UpdateMiniMapRepresentation();
  }

  public final const Bool HasPlayerBeenSpottedAndTriggeredCombat() {
    return this.m_questPlayerHasTriggeredCombat;
  }

  public final const Bool HasThisAreaReceivedCombatNotification() {
    return this.m_hasThisAreaReceivedCombatNotification;
  }

  public final const EFilterType GetIncomingFilter() {
    return this.m_eventsFilters.incomingEventsFilter;
  }

  public final const EFilterType GetOutgoingFilter() {
    return this.m_eventsFilters.outgoingEventsFilter;
  }

  public final void RegisterTimeSystemListeners(ref<Entity> entity) {
    ref<Transition> transitionEvent;
    GameTime hour;
    Int32 i;
    UnregisterTimeSystemListeners();
    i = 0;
    while(i < Size(this.m_areaTransitions)) {
      transitionEvent = new Transition();
      hour = MakeGameTime(0, this.m_areaTransitions[i].transitionHour, 0, 0);
      this.m_areaTransitions[i].listenerID = GetTimeSystem(GetGameInstance()).RegisterListener(RefToWeakRef(entity), transitionEvent, hour, -1, true);
      transitionEvent.listenerID = this.m_areaTransitions[i].listenerID;
      i += 1;
    };
  }

  public final void UnregisterTimeSystemListeners() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_areaTransitions)) {
      GetTimeSystem(GetGameInstance()).UnregisterListener(this.m_areaTransitions[i].listenerID);
      this.m_areaTransitions[i].listenerID = 0;
      i += 1;
    };
  }

  private final void ResolveSecurityAreaType() {
    Int32 hourOfTheDay;
    Int32 i;
    Int32 cachedTransitionHour;
    Uint32 mostRelevantTransition;
    cachedTransitionHour = 25;
    hourOfTheDay = Hours(GetGameTime(GetGameInstance()));
    mostRelevantTransition = 0;
    i = 0;
    while(i < Size(this.m_areaTransitions)) {
      if(this.m_areaTransitions[i].transitionHour < hourOfTheDay) {
        if(cachedTransitionHour > this.m_areaTransitions[i].transitionHour) {
          cachedTransitionHour = this.m_areaTransitions[i].transitionHour;
          mostRelevantTransition = this.m_areaTransitions[i].listenerID;
        };
      };
      i += 1;
    };
    if(mostRelevantTransition == 0) {
      return ;
    };
    ApplyTransition(mostRelevantTransition);
  }

  public final Bool ApplyTransition(Uint32 listenerIndex) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_areaTransitions)) {
      if(this.m_areaTransitions[i].listenerID == listenerIndex) {
        return ApplyTransition(this.m_areaTransitions[i]);
      };
      i += 1;
    };
    return false;
  }

  private final Bool ApplyTransition(AreaTypeTransition transition) {
    ref<PendingSecuritySystemDisable> evt;
    array<ref<SecurityTurretControllerPS>> turrets;
    Int32 i;
    if(this.m_securityAreaType == transition.transitionTo) {
      return false;
    };
    if(transition.transitionTo == ESecurityAreaType.DISABLED) {
      if(!IsDisableAllowed(ToScriptRef(turrets))) {
        PostponeAreaDisabling(turrets);
        return false;
      };
    } else {
      if(this.m_pendingDisableRequest) {
        GetTurrets(ToScriptRef(turrets));
        i = 0;
        while(i < Size(turrets)) {
          evt = new PendingSecuritySystemDisable();
          evt.isPending = false;
          QueuePSEvent(RefToWeakRef(turrets[i]), evt);
          i += 1;
        };
        this.m_pendingDisableRequest = false;
      };
    };
    if(transition.transitionMode == ETransitionMode.FORCED) {
      SetSecurityAreaType(transition.transitionTo);
      if(IsPlayerInside()) {
        if(!GetSecuritySystem().IsSystemInCombat()) {
          NotifySystemAboutCrossingPerimeter(GetLocalPlayerControlledGameObject(), true);
        } else {
          this.m_pendingNotifyPlayerAboutTransition = true;
        };
      };
      return true;
    };
    if(GetSecuritySystem().IsSystemSafe()) {
      SetSecurityAreaType(transition.transitionTo);
      if(IsPlayerInside()) {
        NotifySystemAboutCrossingPerimeter(GetLocalPlayerControlledGameObject(), true);
      };
      return true;
    };
    return false;
  }

  private final void PostponeAreaDisabling(array<ref<SecurityTurretControllerPS>> turrets) {
    ref<PendingSecuritySystemDisable> evt;
    Int32 i;
    i = 0;
    while(i < Size(turrets)) {
      evt = new PendingSecuritySystemDisable();
      evt.isPending = true;
      QueuePSEvent(RefToWeakRef(turrets[i]), evt);
      i += 1;
    };
    this.m_pendingDisableRequest = true;
  }

  private final EntityNotificationType OnSecurityTurretOffline(ref<SecurityTurretOffline> evt) {
    array<ref<SecurityTurretControllerPS>> turrets;
    AreaTypeTransition transition;
    if(this.m_pendingDisableRequest) {
      if(IsDisableAllowed(ToScriptRef(turrets))) {
        this.m_pendingDisableRequest = false;
        transition.transitionTo = ESecurityAreaType.DISABLED;
        transition.transitionMode = ETransitionMode.FORCED;
        ApplyTransition(transition);
      } else {
        PostponeAreaDisabling(turrets);
      };
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  private final const void GetTurrets(ref<array<ref<SecurityTurretControllerPS>>> turrets) {
    array<ref<DeviceComponentPS>> slaves;
    ref<SecurityTurretControllerPS> turret;
    Int32 i;
    slaves = GetImmediateSlaves();
    i = 0;
    while(i < Size(slaves)) {
      turret = Cast(slaves[i]);
      if(ToBool(turret)) {
        Push(FromScriptRef(turrets), turret);
      };
      i += 1;
    };
    if(Size(FromScriptRef(turrets)) == 0) {
      GetSecuritySystem().GetTurrets(this, turrets);
    };
  }

  private final const Bool IsDisableAllowed(ref<array<ref<SecurityTurretControllerPS>>> turrets) {
    Int32 i;
    array<ref<DeviceComponentPS>> slaves;
    ref<SecurityTurretControllerPS> turret;
    Bool isAllowed;
    isAllowed = true;
    GetTurrets(turrets);
    i = 0;
    while(i < Size(FromScriptRef(turrets))) {
      if(FromScriptRef(turrets)[i].IsTurretOperationalUnderSecuritySystem()) {
        isAllowed = false;
      };
      i += 1;
    };
    return isAllowed;
  }

  private final void UpdateMiniMapRepresentation() {
    ref<SecuritySystemControllerPS> secSys;
    secSys = GetSecuritySystem();
    if(ToBool(secSys) && secSys.IsHidden()) {
      GetMappinSystem(GetGameInstance()).OnAreaTypeChanged(ExtractEntityID(GetID()), SecurityAreaTypeEnumToName(ESecurityAreaType.DISABLED));
      return ;
    };
    if(Size(this.m_areaTransitions) > 0) {
      ResolveSecurityAreaType();
    } else {
      GetMappinSystem(GetGameInstance()).OnAreaTypeChanged(ExtractEntityID(GetID()), SecurityAreaTypeEnumToName(this.m_securityAreaType));
    };
  }

  public const String GetDeviceName() {
    if(IsStringValid(this.m_deviceName)) {
      return this.m_deviceName;
    };
    return "";
  }

  public final void AreaEntered(ref<AreaEnteredEvent> evt) {
    ref<GameObject> obj;
    obj = Cast(GetEntity(evt.activator));
    if(!IsActive() || !IsDefined(obj.GetEntityID())) {
      return ;
    };
    ProcessOnEnterRequest(obj);
  }

  public final void AreaExited(ref<GameObject> obj) {
    Bool isAuthorized;
    String dbgString;
    Int32 userIndex;
    if(!IsDefined(obj.GetEntityID())) {
      return ;
    };
    userIndex = FindEntryIndex(obj.GetEntityID());
    if(userIndex == -1 && !IsActive()) {
      return ;
    };
    Erase(this.m_usersInPerimeter, userIndex);
    if(obj.IsPlayerControlled()) {
      this.m_isPlayerInside = false;
      this.m_system = null;
    };
    NotifySystemAboutCrossingPerimeter(obj, false);
  }

  private final void ProcessOnEnterRequest(ref<GameObject> objectToProcess) {
    AreaEntry newEntry;
    if(!ToBool(objectToProcess)) {
      return ;
    };
    newEntry.user = objectToProcess.GetEntityID();
    if(IsUserInside(newEntry.user)) {
    } else {
      PushUniqueEntry(newEntry);
    };
    if(objectToProcess.IsPlayerControlled()) {
      this.m_isPlayerInside = true;
      if(this.m_securityAreaType != ESecurityAreaType.DISABLED) {
        this.m_system = GetSecuritySystem();
      };
      if(this.m_securityAreaType != ESecurityAreaType.DISABLED) {
        NotifySystemAboutCrossingPerimeter(objectToProcess, true);
      };
    };
  }

  private final ref<SecurityAreaCrossingPerimeter> ActionSecurityAreaCrossingPerimeter(ref<GameObject> whoEntered, Bool entered) {
    ref<SecurityAreaCrossingPerimeter> action;
    action = new SecurityAreaCrossingPerimeter();
    action.SetUp(this);
    action.SetProperties(whoEntered, entered);
    action.AddDeviceName(GetDeviceName());
    action.SetAreaData(GetSecurityAreaData());
    return action;
  }

  private final void NotifySystemAboutCrossingPerimeter(ref<GameObject> tresspasser, Bool entering) {
    ref<SecurityAreaCrossingPerimeter> tresspassingEvent;
    if(entering) {
      if(!IsFinal()) {
        LogDevices(this, "Entity: " + ToDebugString(tresspasser.GetEntityID()) + " entered");
      };
    } else {
      if(!IsFinal()) {
        LogDevices(this, "Entity: " + ToDebugString(tresspasser.GetEntityID()) + " left");
      };
    };
    if(!entering && tresspasser.IsPlayerControlled()) {
      this.m_pendingNotifyPlayerAboutTransition = false;
    };
    if(GetSecuritySystem().IsUserAuthorized(tresspasser.GetEntityID(), GetSecurityAccessLevel())) {
      return ;
    };
    tresspassingEvent = ActionSecurityAreaCrossingPerimeter(tresspasser, entering);
    SendActionToAllSlaves(tresspassingEvent);
    NotifySecuritySystem(tresspassingEvent);
  }

  public final EntityNotificationType OnQuestAddTransition(ref<QuestAddTransition> evt) {
    ref<RegisterTimeListeners> registerEvent;
    Int32 i;
    i = 0;
    while(i < Size(this.m_areaTransitions)) {
      if(this.m_areaTransitions[i].transitionHour == evt.transition.transitionHour) {
        return EntityNotificationType.DoNotNotifyEntity;
      };
      i += 1;
    };
    Push(this.m_areaTransitions, evt.transition);
    registerEvent = new RegisterTimeListeners();
    QueueEntityEvent(GetMyEntityID(), registerEvent);
    if(!IsFinal()) {
      LogDevices(this, "SECURITY AREA MANAGER: New Transition added");
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final EntityNotificationType OnQuestRemoveTransition(ref<QuestRemoveTransition> evt) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_areaTransitions)) {
      if(this.m_areaTransitions[i].transitionHour == evt.removeTransitionFrom) {
        GetTimeSystem(GetGameInstance()).UnregisterListener(this.m_areaTransitions[i].listenerID);
        Erase(this.m_areaTransitions, i);
        if(!IsFinal()) {
          LogDevices(this, "SECURITY AREA MANAGER: Transition removed");
        };
      };
      i += 1;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final EntityNotificationType OnQuestExecuteTransition(ref<QuestExecuteTransition> evt) {
    ApplyTransition(evt.transition);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final EntityNotificationType OnQuestIllegalActionAreaNotification(ref<QuestIllegalActionAreaNotification> evt) {
    ref<SecuritySystemInput> actionSecuritySystemInput;
    Vector4 LKP;
    ref<GameObject> playerPup;
    Int32 i;
    if(evt.revealPlayerSettings.revealPlayer == ERevealPlayerType.REVEAL_ONCE) {
      playerPup = GetPlayerMainObject();
      if(ToBool(playerPup)) {
        LKP = playerPup.GetWorldPosition();
      };
    };
    actionSecuritySystemInput = ActionSecurityBreachNotification(LKP, playerPup, ESecurityNotificationType.ILLEGAL_ACTION);
    ExecutePSAction(actionSecuritySystemInput, this);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final const EntityNotificationType OnQuestCombatActionAreaNotification(ref<QuestCombatActionAreaNotification> evt) {
    ref<SecuritySystemInput> actionSecuritySystemInput;
    Vector4 LKP;
    ref<GameObject> playerPup;
    Int32 i;
    if(evt.revealPlayerSettings.revealPlayer == ERevealPlayerType.REVEAL_ONCE) {
      playerPup = GetLocalPlayerControlledGameObject();
      if(ToBool(playerPup)) {
        LKP = playerPup.GetWorldPosition();
      };
    };
    actionSecuritySystemInput = ActionSecurityBreachNotification(LKP, playerPup, ESecurityNotificationType.COMBAT);
    ExecutePSAction(actionSecuritySystemInput, this);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final EntityNotificationType OnQuestModifyFilter(ref<QuestModifyFilters> evt) {
    if(evt.incomingFilters != EQuestFilterType.DONT_CHANGE) {
      this.m_eventsFilters.incomingEventsFilter = ToEnum(ToInt(evt.incomingFilters) - 1);
    };
    if(evt.outgoingFilters != EQuestFilterType.DONT_CHANGE) {
      this.m_eventsFilters.outgoingEventsFilter = ToEnum(ToInt(evt.outgoingFilters) - 1);
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public const void FillAgentsList(out array<PSOwnerData> agentsList)

  private final void NotifySecuritySystem(ref<SecurityAreaCrossingPerimeter> tresspassingEvent) {
    QueuePSEvent(RefToWeakRef(GetSecuritySystem()), tresspassingEvent);
  }

  public const ESecurityAccessLevel GetSecurityAccessLevel() {
    return this.m_securityAccessLevel;
  }

  public final const ESecurityAreaType GetSecurityAreaType() {
    return this.m_securityAreaType;
  }

  public const Bool IsConnectedToSystem() {
    return IsPartOfSystem(ESystems.SecuritySystem);
  }

  public final const array<AreaEntry> GetUsersInPerimeter() {
    return this.m_usersInPerimeter;
  }

  protected final const Bool IsPlayerInside() {
    Int32 i;
    i = 0;
    while(i < Size(this.m_usersInPerimeter)) {
      if(this.m_usersInPerimeter[i].user == GetLocalPlayerControlledGameObject().GetEntityID()) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const Uint32 GetSecurityAreaTypeAsUint32() {
    if(ToBool(GetSecuritySystem())) {
      return GetSecuritySystem().IsHidden() ? 0 : ToInt(GetSecurityAreaType());
    };
    return 0;
  }

  public const Bool IsConnectedToSecuritySystem(out ESecurityAccessLevel level) {
    Int32 i;
    array<ref<DeviceComponentPS>> parents;
    GetParents(parents);
    level = GetSecurityAccessLevel();
    i = 0;
    while(i < Size(parents)) {
      if(ToBool(Cast(parents[i]))) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final void SetSecurityAreaType(ESecurityAreaType newType) {
    ref<SecurityAreaTypeChangedNotification> notification;
    ref<ManageAreaComponent> manageAreaComponent;
    if(this.m_securityAreaType == newType) {
      return ;
    };
    if(newType == ESecurityAreaType.DISABLED) {
      this.m_system = null;
      manageAreaComponent = new ManageAreaComponent();
      manageAreaComponent.enable = false;
      QueueEntityEvent(GetMyEntityID(), manageAreaComponent);
    };
    if(this.m_securityAreaType == ESecurityAreaType.DISABLED) {
      this.m_system = GetSecuritySystem();
      manageAreaComponent = new ManageAreaComponent();
      manageAreaComponent.enable = true;
      QueueEntityEvent(GetMyEntityID(), manageAreaComponent);
    };
    notification = new SecurityAreaTypeChangedNotification();
    notification.area = RefToWeakRef(this);
    notification.previousType = this.m_securityAreaType;
    this.m_securityAreaType = newType;
    notification.currentType = this.m_securityAreaType;
    QueuePSEvent(RefToWeakRef(GetSecuritySystem()), notification);
    if(GetSecuritySystem().IsHidden()) {
      GetMappinSystem(GetGameInstance()).OnAreaTypeChanged(ExtractEntityID(GetID()), SecurityAreaTypeEnumToName(ESecurityAreaType.DISABLED));
    } else {
      GetMappinSystem(GetGameInstance()).OnAreaTypeChanged(ExtractEntityID(GetID()), SecurityAreaTypeEnumToName(this.m_securityAreaType));
    };
  }

  private final void ProcessOnExitRequest(AreaEntry entryToProcess)

  public final const Bool IsActive() {
    return this.m_securityAreaType != ESecurityAreaType.DISABLED;
  }

  public final const Bool IsUserInside(EntityID userToBeChecked) {
    Int32 index;
    index = FindEntryIndex(userToBeChecked);
    if(index < 0) {
      return false;
    };
    return true;
  }

  public final const Bool IsAreaCompromised() {
    Int32 i;
    ref<SecuritySystemControllerPS> secSys;
    secSys = GetSecuritySystem();
    if(!ToBool(secSys)) {
      if(!IsFinal()) {
        LogDevices(this, "No security system found by security area. This setup is not supported", ELogType.ERROR);
      };
      return false;
    };
    i = 0;
    while(i < Size(this.m_usersInPerimeter)) {
      if(secSys.IsEntityBlacklisted(this.m_usersInPerimeter[i].user) || !secSys.IsUserAuthorized(this.m_usersInPerimeter[i].user, GetSecurityAccessLevel())) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final void PushUniqueEntry(AreaEntry entryToPush) {
    Push(this.m_usersInPerimeter, entryToPush);
  }

  private final const Int32 FindEntryIndex(EntityID userToFind) {
    Int32 i;
    i = 0;
    while(i < Size(this.m_usersInPerimeter)) {
      if(this.m_usersInPerimeter[i].user == userToFind) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final const array<ref<CommunityProxyPS>> ExtractSquadProxies() {
    array<ref<DeviceComponentPS>> devices;
    array<ref<CommunityProxyPS>> proxies;
    EntityID myEntityID;
    Int32 i;
    myEntityID = ExtractEntityID(GetID());
    GetDeviceSystem(GetGameInstance()).GetChildren(myEntityID, devices);
    i = 0;
    while(i < Size(devices)) {
      if(ToBool(Cast(devices[i]))) {
        Push(proxies, Cast(devices[i]));
      };
      i += 1;
    };
    return proxies;
  }

  public final const SecurityAreaData GetSecurityAreaData() {
    SecurityAreaData data;
    data.securityArea = RefToWeakRef(this);
    data.securityAreaType = GetSecurityAreaType();
    data.accessLevel = GetSecurityAccessLevel();
    data.zoneName = GetDeviceName();
    data.id = GetID();
    data.incomingFilters = this.m_eventsFilters.incomingEventsFilter;
    data.outgoingFilters = this.m_eventsFilters.outgoingEventsFilter;
    return data;
  }

  public final const array<EntityID> GetSecurityAreaAgents() {
    array<EntityID> agents;
    array<EntityID> npcs;
    array<EntityID> devices;
    Int32 i;
    npcs = GetNPCs();
    devices = GetDevices();
    i = 0;
    while(i < Size(npcs)) {
      Push(agents, npcs[i]);
      i += 1;
    };
    i = 0;
    while(i < Size(devices)) {
      Push(agents, devices[i]);
      i += 1;
    };
    return agents;
  }

  public final const array<EntityID> GetNPCs() {
    array<ref<CommunityProxyPS>> proxies;
    array<EntityID> proxyNPCs;
    array<EntityID> npcs;
    Int32 i;
    proxies = ExtractSquadProxies();
    i = 0;
    while(i < Size(proxies)) {
      proxyNPCs = proxies[i].ExtractEntityIDs();
      i += 1;
    };
    return npcs;
  }

  public const ref<SecuritySystemControllerPS> GetSecuritySystem() {
    if(ToBool(this.m_system)) {
      return this.m_system;
    };
    return GetSecuritySystem();
  }

  public final const array<EntityID> GetDevices() {
    array<ref<DeviceComponentPS>> slaves;
    array<EntityID> devicesIDs;
    Int32 i;
    slaves = GetImmediateSlaves();
    i = 0;
    while(i < Size(slaves)) {
      if(ToBool(Cast(slaves[i]))) {
      } else {
        Push(devicesIDs, ExtractEntityID(slaves[i].GetID()));
      };
      i += 1;
    };
    return devicesIDs;
  }

  public final const ref<SecuritySystemOutput> GetLastOutput() {
    return RestoreLastOutput();
  }

  private final const ref<SecuritySystemOutput> RestoreLastOutput() {
    ref<SecuritySystemOutput> recreatedOutput;
    ref<GameObject> whoBreached;
    ref<GameObject> reporter;
    if(IsValid(this.m_lastOutput)) {
      if(IsDefined(this.m_lastOutput.m_whoBreached)) {
        whoBreached = Cast(FindEntityByID(GetGameInstance(), this.m_lastOutput.m_whoBreached));
      } else {
        if(IsDefined(this.m_lastOutput.m_objectOfInterest)) {
          whoBreached = Cast(FindEntityByID(GetGameInstance(), this.m_lastOutput.m_objectOfInterest));
        };
      };
      reporter = Cast(FindEntityByID(GetGameInstance(), ExtractEntityID(this.m_lastOutput.m_reporter)));
      recreatedOutput = new SecuritySystemOutput();
      recreatedOutput = GetSecuritySystem().ActionSecuritySystemBreachResponse(ActionSecurityBreachNotificationStatic(this.m_lastOutput.m_lastKnownPosition, whoBreached, RefToWeakRef(reporter), this.m_lastOutput.m_type));
      recreatedOutput.SetCachedSecuritySystemState(this.m_lastOutput.m_currentSecurityState);
      recreatedOutput.SetBreachOrigin(this.m_lastOutput.m_breachOrigin);
      recreatedOutput.SetSecurityStateChanged(this.m_lastOutput.m_securityStateChanged);
      recreatedOutput.GetOriginalInputEvent().ModifyNotificationType(this.m_lastOutput.m_type);
      recreatedOutput.GetOriginalInputEvent().ModifyAreaTypeHack(this.m_lastOutput.m_areaType);
      recreatedOutput.GetOriginalInputEvent().SetID(this.m_lastOutput.m_id);
    };
    return recreatedOutput;
  }

  public final EntityNotificationType OnGameEntitySpawnerEvent(ref<gameEntitySpawnerEvent> evt) {
    ref<SecuritySystemOutput> recreatedOutput;
    recreatedOutput = RestoreLastOutput();
    if(ToBool(recreatedOutput)) {
      QueueEntityEvent(evt.spawnedEntityId, recreatedOutput);
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public EntityNotificationType OnSecuritySystemOutput(ref<SecuritySystemOutput> breachEvent) {
    String debugMessage;
    ESecuritySystemState systemState;
    systemState = GetSecuritySystem().GetSecurityState();
    if(this.m_pendingNotifyPlayerAboutTransition && systemState != ESecuritySystemState.COMBAT) {
      NotifySystemAboutCrossingPerimeter(GetLocalPlayerControlledGameObject(), true);
      this.m_pendingNotifyPlayerAboutTransition = false;
    };
    if(!IsActive()) {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    debugMessage = "Input received";
    if(ToBool(breachEvent.GetOriginalInputEvent()) && ToBool(breachEvent.GetOriginalInputEvent().GetWhoBreached()) && IsUserInside(breachEvent.GetOriginalInputEvent().GetWhoBreached().GetEntityID())) {
      debugMessage += " User Inside - Breach = LOCAL";
      breachEvent.SetBreachOrigin(EBreachOrigin.LOCAL);
      if(systemState == ESecuritySystemState.COMBAT && breachEvent.GetOriginalInputEvent().GetWhoBreached() == GetLocalPlayerControlledGameObject()) {
        this.m_questPlayerHasTriggeredCombat = true;
        this.m_hasThisAreaReceivedCombatNotification = true;
      };
    } else {
      if(breachEvent.GetOriginalInputEvent().GetNotificationType() == ESecurityNotificationType.ALARM && IsUserInside(breachEvent.GetOriginalInputEvent().GetObjectOfInterest().GetEntityID())) {
        debugMessage += " ALARM ORIGINATED IN THIS AREA - BREACH - LOCAL";
        breachEvent.SetBreachOrigin(EBreachOrigin.LOCAL);
      } else {
        if(this.m_eventsFilters.incomingEventsFilter == EFilterType.ALLOW_NONE) {
          debugMessage += " Area does not accept external events. Ignoring Event";
          if(!IsFinal()) {
            LogDevices(this, breachEvent.GetOriginalInputEvent().GetID(), debugMessage);
          };
          return EntityNotificationType.DoNotNotifyEntity;
        };
        if(this.m_eventsFilters.incomingEventsFilter == EFilterType.ALLOW_COMBAT_ONLY) {
          if(breachEvent.GetCachedSecurityState() != ESecuritySystemState.COMBAT) {
            debugMessage += " Area accepts only COMBAT events. Ignoring event.";
            if(!IsFinal()) {
              LogDevices(this, breachEvent.GetOriginalInputEvent().GetID(), debugMessage);
            };
            return EntityNotificationType.DoNotNotifyEntity;
          };
          this.m_hasThisAreaReceivedCombatNotification = true;
        };
        debugMessage += " User NOT Inside - Breach = EXTERNAL";
        breachEvent.SetBreachOrigin(EBreachOrigin.EXTERNAL);
      };
    };
    debugMessage += " Forwarding event to slaves";
    if(!IsFinal()) {
      LogDevices(this, breachEvent.GetOriginalInputEvent().GetID(), debugMessage);
    };
    StoreLastOutputPersistentData(breachEvent);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  private final void StoreLastOutputPersistentData(ref<SecuritySystemOutput> breachEvent) {
    this.m_lastOutput.m_currentSecurityState = breachEvent.GetCachedSecurityState();
    this.m_lastOutput.m_breachOrigin = breachEvent.GetBreachOrigin();
    this.m_lastOutput.m_securityStateChanged = breachEvent.GetSecurityStateChanged();
    this.m_lastOutput.m_lastKnownPosition = breachEvent.GetOriginalInputEvent().GetLastKnownPosition();
    this.m_lastOutput.m_type = breachEvent.GetOriginalInputEvent().GetNotificationType();
    this.m_lastOutput.m_areaType = GetSecurityAreaType();
    if(ToBool(breachEvent.GetOriginalInputEvent().GetObjectOfInterest())) {
      this.m_lastOutput.m_objectOfInterest = breachEvent.GetOriginalInputEvent().GetObjectOfInterest().GetEntityID();
    };
    if(ToBool(breachEvent.GetOriginalInputEvent().GetWhoBreached())) {
      this.m_lastOutput.m_whoBreached = breachEvent.GetOriginalInputEvent().GetWhoBreached().GetEntityID();
    };
    if(ToBool(breachEvent.GetOriginalInputEvent().GetNotifierHandle())) {
      this.m_lastOutput.m_reporter = WeakRefToRef(breachEvent.GetOriginalInputEvent().GetNotifierHandle()).GetID();
    };
    this.m_lastOutput.m_id = breachEvent.GetOriginalInputEvent().GetID();
  }

  public EntityNotificationType OnSecuritySystemForceAttitudeChange(ref<SecuritySystemForceAttitudeChange> evt) {
    SendActionToAllSlaves(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public EntityNotificationType OnTargetAssessmentRequest(ref<TargetAssessmentRequest> evt) {
    SendActionToAllSlaves(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public EntityNotificationType OnFullSystemRestart(ref<FullSystemRestart> evt) {
    SendActionToAllSlaves(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final static CName SecurityAreaTypeEnumToName(ESecurityAreaType type) {
    switch(type) {
      case ESecurityAreaType.DISABLED:
        return "DISABLED";
      case ESecurityAreaType.SAFE:
        return "SAFE";
      case ESecurityAreaType.RESTRICTED:
        return "RESTRICTED";
      case ESecurityAreaType.DANGEROUS:
        return "DANGEROUS";
    };
    return "";
  }

  public const String GetDebugTags() {
    String playerStatus;
    Bool isPlayerInside;
    Bool isPlayerAuthorized;
    String tags;
    Int32 authorizedUsers;
    Int32 unauthorizedUsers;
    Int32 i;
    tags = GetDebugTags();
    return tags;
  }

  public void OnMaraudersMapDeviceDebug(ref<MaraudersMapDevicesSink> sink) {
    sink.BeginCategory("securityAreaControllerPS Specific");
    sink.PushString("AREA TYPE", ToString(this.m_securityAreaType));
    sink.PushString("PLAYER INSIDE: ", BoolToString(this.m_isPlayerInside));
    sink.PushString("ACCESS LEVEL", ToString(this.m_securityAccessLevel));
    sink.PushString("RECEIVED COMBAT OUTPUT: ", BoolToString(this.m_hasThisAreaReceivedCombatNotification));
    sink.EndCategory();
  }

  protected TweakDBID GetDeviceIconTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected TweakDBID GetBackgroundTextureTweakDBID() {
    return "DeviceIcons.SecuritySystemDeviceBackground";
  }
}
