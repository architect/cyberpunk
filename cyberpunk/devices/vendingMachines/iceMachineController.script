
public class IceMachineController extends VendingMachineController {

  protected const ref<IceMachineControllerPS> GetPS() {
    return Cast(GetBasePS());
  }
}

public class IceMachineControllerPS extends VendingMachineControllerPS {

  private TweakDBID m_vendorTweakID;

  private IceMachineSFX m_iceMachineSFX;

  protected cb Bool OnInstantiated() {
    OnInstantiated();
    if(!IsStringValid(this.m_deviceName)) {
      this.m_deviceName = "Gameplay-Devices-DisplayNames-IceMachine";
    };
    this.m_vendorTweakID = "Vendors.IceMachine";
  }

  public final TweakDBID GetVendorTweakID() {
    return this.m_vendorTweakID;
  }

  public final CName GetProcessingSFX() {
    return this.m_iceMachineSFX.m_processing;
  }

  public CName GetGlitchStartSFX() {
    return this.m_iceMachineSFX.m_glitchingStart;
  }

  public CName GetGlitchStopSFX() {
    return this.m_iceMachineSFX.m_glitchingStop;
  }

  public final CName GetIceFallSFX() {
    return this.m_iceMachineSFX.m_iceFalls;
  }

  public Float GetTimeToCompletePurchase() {
    return 2.5;
  }

  public Int32 GetHackedItemCount() {
    return 25;
  }

  protected void PushShopStockActions(out array<ref<DeviceAction>> actions, GetActionsContext context) {
    array<SItemStack> shopStock;
    Int32 i;
    if(IsDefaultConditionMet(this, context) && this.m_isReady) {
      shopStock = GetVendorItemsForSale(RefToWeakRef(Cast(WeakRefToRef(GetOwnerEntityWeak()))), false);
      if(Size(shopStock) > 0) {
        Push(actions, ActionDispenceIceCube(shopStock[0].itemID));
      };
    };
  }

  protected final ref<DispenceItemFromVendor> ActionDispenceIceCube(ItemID item) {
    ref<DispenceItemFromVendor> action;
    Int32 price;
    action = new DispenceItemFromVendor();
    price = GetBuyPrice(GetGameInstance(), GetMyEntityID(), item);
    action.clearanceLevel = GetInteractiveClearance();
    action.SetUp(this);
    action.SetProperties(item, price);
    action.AddDeviceName(this.m_deviceName);
    action.SetDurationValue(GetTimeToCompletePurchase());
    action.CreateActionWidgetPackage();
    return action;
  }

  public EntityNotificationType OnDispenceItemFromVendor(ref<DispenceItemFromVendor> evt) {
    ref<TransactionSystem> transactionSys;
    transactionSys = GetTransactionSystem(GetGameInstance());
    if(evt.IsStarted()) {
      ExecutePSActionWithDelay(evt, this, evt.GetDurationValue());
    } else {
      if(evt.CanPay(GetPlayer(GetGameInstance()))) {
        this.m_isReady = false;
        transactionSys.RemoveItem(GetPlayer(GetGameInstance()), Money(), evt.GetPrice());
        UseNotifier(evt);
      } else {
        return EntityNotificationType.DoNotNotifyEntity;
      };
    };
    return EntityNotificationType.SendThisEventToEntity;
  }
}
